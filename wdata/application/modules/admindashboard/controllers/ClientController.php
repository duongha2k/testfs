<?php

class Admindashboard_ClientController extends Core_Controller_Action {

    public function init() {
        
        parent::init();

        $request = $this->getRequest();

        $auth = new Zend_Session_Namespace('Auth_User');
        $this->view->isPM = $auth->isPM;
        $this->view->name = $auth->name;
        $company = $request->getParam('company', NULL);

        $deductCompanies = array('HXC', 'HXWN', 'HXXO', 'HXST', 'BW', 'suss', 'CBD', 'RHOM', 'TECF', 'SIS', 'BLUE', 'CPI', 'FDSV', 'PCS', 'DATA', 'BBLC');

        $this->view->company = !empty($company) ? $company : null;
        $this->view->showDeduct = in_array($this->view->company, $deductCompanies);
    }

    public function createAction() {
        $errorMessages = new Core_Form_ErrorHandler('createClient');
        $search = new Core_Api_AdminClass;
        //get states
        $states = $search->getStates();
        $this->view->states = $states->data;
        //get countries
        $countries = $search->getCountries();
        $this->view->countries = $countries->data;
        //get pricing rules
        $pricingRules = $search->getPricingRulesDescriptionOrder($this->_login, $this->_password);
        $this->view->pricingRules = $pricingRules->data;

        $this->view->messages = $errorMessages->getMessages();
        $errorMessages->clearMessages();
        
        //622
        $CommonClass = new Core_Api_CommonClass();
        $WoTagClass = new Core_Api_WoTagClass(); 
        $this->view->StatusList = $WoTagClass->getClientStatusList(); 
        $this->view->RelationShipList = $WoTagClass->getClientRelationShipList();
        $this->view->SegmentList = $WoTagClass->getSegmentList();
        $this->view->OriginalSellerList = $WoTagClass->getOriginalSellerList();
        $this->view->ExecutiveList = $WoTagClass->getExecutiveList();
        $this->view->EntityTypeList = $WoTagClass->getEntityTypeList();
        $this->view->ServiceTypeList = $WoTagClass->getServiceTypeList();
        $this->view->AccountManagers = $CommonClass->getAccountManagers();
        $this->view->ServiceDirectors = $CommonClass->getClientServiceDirectors();

    }

    public function doCreateAction() {
        $this->_helper->viewRenderer->setNoRender();
        $errorMessages = new Core_Form_ErrorHandler('createClient');
        $createForm = $this->getRequest()->getParam('CreateForm', null);
        $params = $this->getRequest()->getParams();

        if (empty($createForm))
            $errorMessages->addMessage('Create Client Form is empty');

      
        if(isset($createForm['UserType']) &&  $createForm['UserType']=='Admin') 
        {
            $createForm['Admin']=1;
        }            
        if(isset($createForm['Admin']) &&  $createForm['Admin']==1) 
        {
            $createForm['UserType']='Admin';
        }                
        $createForm['CustomSignOff_Enabled']='1';
        //--- create client
        $admin = new Core_Api_AdminClass();
        $result = $admin->createClient($this->_login, $this->_password, $createForm);
        
        //999
        $user = new Core_Api_User();
        $data = $result->data;
        $clientId = $data[0]->ClientID;
        $user->SetAccessDefaultValuesForClient($clientId);
        //end 999

           //13870           
            $companyId = $createForm['Company_ID'];       
            $csdId = $params["FSClientServiceDirectorId"];
            $entityTypeId = $params["EntityTypeId"];
            $serviceTypeId = $params["ServiceTypeId"];                       
            $resultProject = $admin->createDefaultProject($companyId,$csdId,$entityTypeId,$serviceTypeId);            
            //13870
        if (!$result->success) {
            foreach ($result->errors as $error) {
                $errorMessages->addMessage($error->message);
            }
        } else {
            // save tag
            $WoTagClass = new Core_Api_WoTagClass();
            $arrayTab = array();
            foreach($WoTagClass->tagCodeIdArray as $tagCodeId){
                if(isset($params[$tagCodeId]) && !empty($params[$tagCodeId])){
                    $arrayTab[] = array('TagCodeId'=>$tagCodeId, 'TagItemId'=>$params[$tagCodeId]);
                }
            }
            if(!empty($arrayTab)){
               $WoTagClass->saveClientTagValues($result->data[0]->ClientID,$arrayTab);
            }
            $id = $result->data[0]->ClientID;
            if (empty($_FILES))
                break;
            $msgs = array();

            foreach ($_FILES as $k => $fileStruc) {
                if (empty($fileStruc['tmp_name']))
                    continue;
                if ($fileStruc['error'] == UPLOAD_ERR_OK &&
                        is_uploaded_file($fileStruc['tmp_name']) &&
                        filesize($fileStruc['tmp_name']) > 0
                ) {
                    $fname = $fileStruc['name'];
                    if (strrpos($fname, '/') !== FALSE) {
                        $fname = substr($fname, strrpos($fname, '/'));
                    }
                    $info = pathinfo($fname);
                    $fname = uniqid($info['filename'] . '_');
                    $fname = $fname . '.' . $info['extension'];
                    $fdata = file_get_contents($fileStruc['tmp_name']);

                    $result = $admin->uploadFileAWS($this->_login, $this->_password, $id, $fname, $fdata, $k);
                    if (!$result->success)
                        $msgs[] = $result->errors[0]->message;
                } else {
                    if ($fileStruc['error'] != UPLOAD_ERR_NO_FILE) {
                        $errorMessages->addMessage('File "' . $fileStruc['name'] . '" upload failed');
                    } else if (!is_uploaded_file($fileStruc['tmp_name'])) {
                        $errorMessages->addMessage('File "' . $fileStruc['name'] . '" not uploaded.');
                    } else if (filesize($fileStruc['tmp_name']) == 0) {
                        $errorMessages->addMessage('Uploaded "' . $fileStruc['name'] . '" file is empty.');
                    }
                    $errorsDetails = $errorMessages->getMessages();
                    error_log('FileUploading: (admin:fWUA, client:' . $id . ') ' . implode('|', $errorsDetails));
                }
            }
            if (!empty($msgs)) {
                $msgs = array_unique($msgs);
                foreach ($msgs as $msg)
                    $errorMessages->addMessage($msg);
            }
            // send email
                $toEmail = $createForm['Email1']; //$techEmail;
                $fromName = "support@fieldsolutions.com";
                $fromEmail = "support@fieldsolutions.com";

                $subject = "Welcome to FieldSolutions!";
                $body = "Welcome to FieldSolutions, ".$createForm['ContactName']."!

                        Your login information is:

                        Username: ".$createForm['UserName']."
                        Temporary Password: ".$createForm['Password']."

                        As a safety precaution, please update your temporary password when you first log into the FieldSolutions site. To update your password, click on 'Client Settings' in the blue Navigation Bar at the upper right of your screen, click the 'Edit' button, and scroll down to the bottom of the page.

                        We're looking forward to working with you!

                        Thank you,

                        Your FieldSolutions Team
                        ";
                $mail = new Core_Mail();
                $mail->setBodyText($body);
                $mail->setFromName($fromName);
                $mail->setFromEmail($fromEmail);
                $mail->setToEmail($toEmail);
                $mail->setSubject($subject);
                $mail->send();  
        }

        $this->_redirect($_SERVER['HTTP_REFERER']);
    }

    public function searchAction() {
        $search = new Core_Api_AdminClass;
        //get companies
        $fieldsList = array('Company_ID', 'CompanyName');
        $companies = $search->getCompanies($this->_login, $this->_password, $fieldsList, 'CompanyName', 'CompanyName');

        $this->view->companies = $companies->data;
    }

    public function listAction() {
        $request = $this->getRequest();
        $params = $request->getParams();

        $size = ($request->getParam('size', 10)) + 0;
        $size = ( $size <= 10 ) ? 10 : ( ( $size >= 100 ) ? 100 : $size );
        $page = ($request->getParam('page', 1)) + 0;
        $page = ( $page < 1 ) ? 1 : $page;

        $sortBy = $request->getParam('sortBy', 'UserName');
        $sortDir = $request->getParam('sortDir', 'asc');

        $this->view->sortBy = $sortBy;
        $this->view->sortDir = $sortDir;

        $search = new Core_Api_AdminClass;

        $countRows = 0;

        // Save workorders into CSV file
        /* if ( $request->getParam('download', false) === 'csv' ) {
          try {
          ob_start();

          $projects = $search->getClientsList(
          $this->_login,
          $this->_password,
          $params,
          'Project_ID ASC'
          );

          $res = array( "Project_ID","Project_Name","Headline","Client_Company_Name");
          $this->putcsv($res);

          if ($projects->success )
          foreach ($projects->data as $project) {
          $row = array(
          $project->Project_ID,
          $project->Project_Name,
          $project->Headline,
          $project->Client_Name
          );
          $this->putcsv($row);
          }
          $csv = ob_get_contents();
          ob_end_clean();

          return $this->_outputCsv($csv);
          } catch (Exception $e) {
          print 'error occurred';
          return false;
          }
          } */

        $clients = $search->getClientsList(
                $this->_login,
				$this->_password, 
				$params,
				(!empty($sortBy) && !empty($sortDir) ) ? "{$sortBy} {$sortDir}" : null,
				($page - 1) * $size, 
				$size,
				$countRows
        );

        if ($clients->success) {
            $paginator = new Zend_Paginator(new Core_Paginator_Sliced_Array($clients->data, $countRows));
            $paginator->setCurrentPageNumber($page);
            $this->view->paginator = $paginator;
        }
        $this->view->success = $clients->success;
        $this->view->errors = $clients->errors;
        // 576
        $Company_ID = $this->getRequest()->getParam('Company_ID', null);
        $WoTagClass = new Core_Api_WoTagClass();
        $clientStatus = $WoTagClass->getCompanyTagValues($Company_ID);
        if(strtolower($clientStatus["ClientStatusId"]["TagItemName"]) =="inactive")
        {
            $this->view->success = 0 ;
        }
        // 576
    }

    public function listPricingRuleAction() {
        $request = $this->getRequest();
        $params = $request->getParams();

        $size = ($request->getParam('size', 25)) + 0;
        $size = ( $size <= 10 ) ? 10 : ( ( $size >= 100 ) ? 100 : $size );
        $page = ($request->getParam('page', 1)) + 0;
        $page = ( $page < 1 ) ? 1 : $page;

        $sortBy = $request->getParam('sortBy', 'UserName');
        $sortDir = $request->getParam('sortDir', 'asc');

//	$this->view->company = '';

        $params = $request->getParams();

        $params['Company_ID'] = $this->view->company;
        $params['assignedPricingRule'] = true;

        $companyId = $this->view->company;

        $this->view->sortBy = $sortBy;
        $this->view->sortDir = $sortDir;

        $search = new Core_Api_AdminClass;

        $countRows = 0;

        $rules = $search->getPricingRules($this->_login, $this->_password);

        $pricingRules = array();

        foreach ($rules->data as $rule) {
            $pricingRules[$rule->PricingRuleId] = $rule->Description;
        }

//	ini_set('error_log', '/home/p3559/error.log');

        // Save workorders into CSV file
        if ($request->getParam('download', false) === 'csv') {
            try {
                ob_start();

                $allRules = $request->getParam('all', false);
                $allRules = !empty($allRules);
                if ($allRules) {
                    $rules = Core_Pricing::getRules();
                    $res = array('Rule ID', 'Description', 'Parent ID', 'Clone Date');
                } else {
                    $clients = $search->getClientsList(
                            $this->_login,
							$this->_password,
							$params,
							'ClientID ASC'
                    );

                    $res = array("Client_ID", "Company_ID", "Company", "PricingRuleID", "Description");
                }

                $this->putcsv($res);

                if ($allRules) {
                    foreach ($rules as $id => $rule) {
                        $row = array($id, $rule['Description'], $rule['ParentID'], $rule['CloneDate']);
                        $this->putcsv($row);
                    }
                } else if ($clients->success) {
                    foreach ($clients->data as $client) {
                        $row = array(
                            $client->ClientID,
                            $client->Company_ID,
                            $client->CompanyName,
                            $client->PricingRuleID,
                            $pricingRules[$client->PricingRuleID]
                        );
                        $this->putcsv($row);
                    }
                }
                $csv = ob_get_contents();
                ob_end_clean();

                return $allRules ? $this->_outputCsv($csv, $this->_company . '-' . ucfirst('pricingrules') . '-' . date('mdY') . '.csv') : $this->_outputCsv($csv);
            } catch (Exception $e) {
                print 'error occurred';
                return false;
            }
        }

        $clients = $search->getClientsList(
                $this->_login, 
				$this->_password, 
				$params,
				(!empty($sortBy) && !empty($sortDir) ) ? "{$sortBy} {$sortDir}" : null, 
				($page - 1) * $size, 
				$size, 
				$countRows
        );

        if ($clients->success) {
            $paginator = new Zend_Paginator(new Core_Paginator_Sliced_Array($clients->data, $countRows));
            $paginator->setCurrentPageNumber($page);
            $this->view->paginator = $paginator;
        }
        $this->view->pricingRules = $pricingRules;
        $this->view->success = $clients->success;
        $this->view->errors = $clients->errors;
    }

    public function updateAction() {
        $client_id = $this->getRequest()->getParam('id', null);
        $user = new Core_Api_User($client_id);
        $client = $user->loadById($client_id);

        $errorMessages = new Core_Form_ErrorHandler('updateClient_' . $client_id);

        $this->view->client = $client;
        $this->view->success = $client !== false;

        $search = new Core_Api_AdminClass;

        //get pricing rules
        $pricingRules = $search->getPricingRulesDescriptionOrder($this->_login, $this->_password);
        $this->view->pricingRules = $pricingRules->data;

        $errors = Core_Api_Error::getInstance();
        $er = $errors->getErrorArray();
        foreach ($er as $k => $v)
            $errorMessages->addMessage($error->message);

        //--- 13622   
        $WoTagClass = new Core_Api_WoTagClass();
        $companyTagValues = $WoTagClass->getCompanyTagValues($client->Company_ID);
        $clientTagValues = $WoTagClass->getClientTagValues($client_id);
        if(empty($clientTagValues['EntityTypeId']['TagItemId'])) {
            $clientTagValues['EntityTypeId']['TagItemId'] = $companyTagValues['EntityTypeId']['TagItemId'];
            $clientTagValues['EntityTypeId']['TagItemName'] = $companyTagValues['EntityTypeId']['TagItemName'];            
        }
        if(empty($clientTagValues['ServiceTypeId']['TagItemId'])) 
        {
            $clientTagValues['ServiceTypeId']['TagItemId'] = $companyTagValues['ServiceTypeId']['TagItemId'];
            $clientTagValues['ServiceTypeId']['TagItemName'] = $companyTagValues['ServiceTypeId']['TagItemName'];
        }
        //---    
        $this->view->messages = $errorMessages->getMessages();
        $errorMessages->clearMessages();
        $this->view->ClientTagValues = $clientTagValues;
    }

    public function doUpdateAction() {
        $updateForm = $this->getRequest()->getParam('UpdateForm', null);
        $id = $this->getRequest()->getParam('ClientID', null);
        $removefiles = $this->getRequest()->getParam('removefiles', null);
        $errorMessages = new Core_Form_ErrorHandler('updateClient_' . $id);

        if (!empty($removefiles))
            foreach ($removefiles as $rmf)
                $updateForm[$rmf] = '';

        if (empty($updateForm) || empty($id))
            $errorMessages->addMessage('Update Client Form is empty');
        $er = $errorMessages->getMessages();
        if (empty($er)) {
            $search = new Core_Api_AdminClass;
            $checkboxes = array('AccountEnabled', 'ShowOnReports', 'ProjectManager', 'Admin', 'AcceptTerms');
            foreach ($checkboxes as $c)
                $updateForm[$c] = isset($updateForm[$c]) ? 1 : 0;

            
            if(isset($updateForm['UserType']) &&  $updateForm['UserType']=='Admin') 
            {
                $updateForm['Admin']=1;
            }            
            if(isset($updateForm['Admin']) &&  $updateForm['Admin']==1) 
            {
                $updateForm['UserType']='Admin';
            }                
            //echo("updateForm: <pre>");print_r($updateForm);die();//test
            
            //--- update                
            $result = $search->updateClient($this->_login, $this->_password, $id, $updateForm);
            $errors = Core_Api_Error::getInstance();
            if (!$result->success) {
                foreach ($result->errors as $error) {
                    $errorMessages->addMessage($error->message);
                }
            } else {
                // save tag
                
                if (!empty($_FILES)) {
                    $msgs = array();

                    foreach ($_FILES as $k => $fileStruc) {
                        if (empty($fileStruc['tmp_name']))
                            continue;
                        if ($fileStruc['error'] == UPLOAD_ERR_OK &&
                                is_uploaded_file($fileStruc['tmp_name']) &&
                                filesize($fileStruc['tmp_name']) > 0
                        ) {
                            $fname = $fileStruc['name'];
                            if (strrpos($fname, '/') !== FALSE) {
                                $fname = substr($fname, strrpos($fname, '/'));
                            }
                            $info = pathinfo($fname);
                            $fname = uniqid($info['filename'] . '_');
                            $fname = $fname . '.' . $info['extension'];
                            $fdata = file_get_contents($fileStruc['tmp_name']);

                            $result = $search->uploadFileAWS($this->_login, $this->_password, $id, $fname, $fdata, $k);
                           // print_r("update:"); echo("<pre>");print_r($result);echo("</pre>");die();

                            if (!$result->success)
                                $msgs[] = $result->errors[0]->message;
                        } else {
                            if ($fileStruc['error'] != UPLOAD_ERR_NO_FILE) {
                                $errorMessages->addMessage('File "' . $fileStruc['name'] . '" upload failed');
                            } else if (!is_uploaded_file($fileStruc['tmp_name'])) {
                                $errorMessages->addMessage('File "' . $fileStruc['name'] . '" not uploaded.');
                            } else if (filesize($fileStruc['tmp_name']) == 0) {
                                $errorMessages->addMessage('Uploaded "' . $fileStruc['name'] . '" file is empty.');
                            }
                            $errorsDetails = $errorMessages->getMessages();
                            error_log('FileUploading: (admin:fWUA, client:' . $id . ') ' . implode('|', $errorsDetails));
                        }
                    }

                    if (!empty($removefiles)) {
                        $files = new Core_File();
                        foreach ($removefiles as $rmf) {
                            $files->removeFile($result->data[0]->$rmf, S3_CLIENTS_LOGO_DIR);
                        }
                    }
                    if (!empty($msgs)) {
                        $msgs = array_unique($msgs);
                        foreach ($msgs as $msg)
                            $errorMessages->addMessage($msg);
                    }
                }
            }
        }
        $this->_redirect($_SERVER['HTTP_REFERER']);
    }

    public function removeAction() {
        $this->_helper->viewRenderer->setNoRender();
        $response = $this->getResponse();
        $id = $this->getRequest()->getParam('id', null);
        $errors = array();
        if (!empty($id)) {
            $admin = new Core_Api_AdminClass();
            $res = $admin->removeClient($this->_login, $this->_password, $id);
            if (!$res->success) {
                foreach ($res->errors as $error)
                    $errors[] = $error->message;
            }
        } else {
            $errors[] = 'Client ID empty bu reqired.';
        }
        $json_output = array('success' => empty($errors), 'error' => $errors);
        $response->clearAllHeaders()->clearBody();
        $response->setHeader('Content-type', 'application/json');
        $response->setBody(json_encode($json_output));
    }

    private function putcsv($row, $fd = ',', $quot = '"') {
        $str = '';
        foreach ($row as $cell) {
            $cell = str_replace($quot, $quot . $quot, $cell);
            if (strchr($cell, $fd) !== FALSE || strchr($cell, $quot) !== FALSE || strchr($cell, "\n") !== FALSE) {
                $str .= $quot . $cell . $quot . $fd;
            } else {
                $str .= $cell . $fd;
            }
        }
        echo substr($str, 0, -1) . "\n";
        return strlen($str);
    }

    private function _outputCsv($csvText, $fname = null) {
        if (empty($fname))
            $fname = $this->_company . '-' . ucfirst('clients') . '-' . date('mdY') . '.csv';
        $this->_helper->viewRenderer->setNoRender();
        $response = $this->getResponse();
        $response->clearHeaders();
        $response->clearBody();
        $response->setHeader('Pragma', 'public')
                ->setHeader('Expires', '0')
                ->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0')
                ->setHeader('Cache-Control', 'private')
                ->setHeader('Content-Type', 'text/csv')
                ->setHeader('Content-Disposition', 'attachment; filename=' . $fname, true)
                ->setHeader('Content-Transfer-Encoding', 'binary');
        $response->sendResponse();
        //Flush buffers
        ob_end_flush();
        flush();
        echo $csvText;
        exit;
    }

    //--------------------------------------------------------
    public function updateclientAction() {
        
        $request = $this->getRequest();
        $mode = $request->getParam('mode') == "edit" ? true : false;

        $user = new Core_Api_User();
        $client_id = $this->getRequest()->getParam('client_id', null);
             
        $clientInfo = $user->loadById($client_id);
        
        $clientCountry = $clientInfo->Country;

        $apiClass = new Core_Api_Class();
        $apiCommon = new Core_Api_CommonClass();   

        $this->view->PcntDeductPercent =$user->GetDeductPercent_FSSFeeFrTech($clientInfo->Company_ID);
        
        $this->view->countries = $apiClass->getCountriesList();
         
        $clientCountryCode = $apiCommon->getCountryCode($clientCountry); 
        $this->view->states = $apiCommon->getStatesArray($clientCountryCode);   
        $this->view->client = $clientInfo;
        $this->view->success = $clientInfo !== false;
        $this->view->mode = $mode;
        //---- ClientServiceDirectors
        $serviceDirectors = $apiCommon->getClientServiceDirectors();
        $this->view->serviceDirectors = $serviceDirectors;
        $accountManagers = $apiCommon->getAccountManagers();
        $this->view->accountManagers = $accountManagers;
        
        $errors = Core_Api_Error::getInstance();
          
        $this->view->messages = $error->message;

        $clientExt = $user->getClientExt($client_id);
       
        $_clientExt = array();
        if (!empty($clientExt)) {
             
            $_clientExt["FSClientServiceDirectorId"] = $clientExt[0]["FSClientServiceDirectorId"];
            $_clientExt["FSAccountManagerId"] = $clientExt[0]["FSAccountManagerId"];
            $_clientExt["File1URL"] = $clientExt[0]["File1URL"];
            $_clientExt["File2URL"] = $clientExt[0]["File2URL"];
            $_clientExt["File3URL"] = $clientExt[0]["File3URL"];
            $_clientExt["Upload1Date"] = $clientExt[0]["Upload1Date"];
            $_clientExt["Upload2Date"] = $clientExt[0]["Upload2Date"];
            $_clientExt["Upload3Date"] = $clientExt[0]["Upload3Date"];
            $_clientExt["DeniedTech_NoShowGreater"] = $clientInfo->DeniedTech_NoShowGreater;
            $_clientExt["DeniedTech_NoShowPeriod"] = $clientInfo->DeniedTech_NoShowPeriod;
            $_clientExt["DeniedTech_BackOutGreater"] = $clientInfo->DeniedTech_BackOutGreater;
            $_clientExt["DeniedTech_BackOutPeriod"] = $clientInfo->DeniedTech_BackOutPeriod;
            $_clientExt["DeniedTech_PreferenceRatingSmaller"] = $clientInfo->DeniedTech_PreferenceRatingSmaller;
            $_clientExt["DeniedTech_PreferenceRatingPeriod"] = $clientInfo->DeniedTech_PreferenceRatingPeriod;
            $_clientExt["DeniedTech_PerformanceRatingSmaller"] = $clientInfo->DeniedTech_PerformanceRatingSmaller;
            $_clientExt["DeniedTech_PerformanceRatingPeriod"] = $clientInfo->DeniedTech_PerformanceRatingPeriod;
        } else {
             //die("updateclientAction1");  
            $_clientExt["FSClientServiceDirectorId"] = 0;
            $_clientExt["FSAccountManagerId"] = 0;
            $_clientExt["File1URL"] = "";
            $_clientExt["File2URL"] = "";
            $_clientExt["File3URL"] = "";
            $_clientExt["Upload1Date"] = "";
            $_clientExt["Upload2Date"] = "";
            $_clientExt["Upload3Date"] = "";
            $_clientExt["DeniedTech_NoShowGreater"] = 0;
            $_clientExt["DeniedTech_NoShowPeriod"] = "";
            $_clientExt["DeniedTech_BackOutGreater"] = 0;
            $_clientExt["DeniedTech_BackOutPeriod"] = "";
            $_clientExt["DeniedTech_PreferenceRatingSmaller"] = 0;
            $_clientExt["DeniedTech_PreferenceRatingPeriod"] = "";
            $_clientExt["DeniedTech_PerformanceRatingSmaller"] = 0;
            $_clientExt["DeniedTech_PerformanceRatingPeriod"] = "";
        }
        $this->view->clientExt = $_clientExt;
        // get 
        
        //13622
        //
        $WoTagClass = new Core_Api_WoTagClass(); 
        $companyTagValues = $WoTagClass->getCompanyTagValues($clientInfo->Company_ID);
        $clientTagValues = $WoTagClass->getClientTagValues($client_id);
        //echo("<pre>");print_r($clientTagValues); echo("</pre>");
        //die("updateclientAction");  
        if(!empty($clientTagValues)){
            if(empty($clientTagValues['EntityTypeId']['TagItemId'])) {
               
                $clientTagValues['EntityTypeId']['TagItemId'] = $companyTagValues['EntityTypeId']['TagItemId'];
                $clientTagValues['EntityTypeId']['TagItemName'] = $companyTagValues['EntityTypeId']['TagItemName'];            
            }
            if(empty($clientTagValues['ServiceTypeId']['TagItemId'])) 
            {
                 
                $clientTagValues['ServiceTypeId']['TagItemId'] = $companyTagValues['ServiceTypeId']['TagItemId'];
                $clientTagValues['ServiceTypeId']['TagItemName'] = $companyTagValues['ServiceTypeId']['TagItemName'];
            }
            //die("updateclientAction3");  

        }  
       

        //---         
        $this->view->StatusList = $WoTagClass->getClientStatusList(); 
       // print_r($this->view->StatusList) ;
        
        $this->view->RelationShipList = $WoTagClass->getClientRelationShipList();
            
        $this->view->SegmentList = $WoTagClass->getSegmentList();
        
        $this->view->OriginalSellerList = $WoTagClass->getOriginalSellerList();
        
        $this->view->ExecutiveList = $WoTagClass->getExecutiveList();
          
        $this->view->ClientTagValues = $clientTagValues;
         
        $this->view->ClientCompanyRestricted = $user->isClientCompanyRestricted($clientInfo->Company_ID);
        
        $this->view->SystemStatusList = $WoTagClass->getSystemStatusList();      
       // die("updateclientAction3");     
    }

    //--------------------------------------------------------
    public function ajaxUploadFileTempAction() {
        $this->_helper->viewRenderer->setNoRender(true);

        $maxFileSize = 52428800; //50Mb

        $id = $this->getRequest()->getParam('file', "");


        $return = array('success' => 0);

        if (!empty($_FILES["{$id}"])) {
            if (
                    is_readable($_FILES[$id]['tmp_name']) &&
                    $_FILES[$id]['error'] == UPLOAD_ERR_OK &&
                    filesize($_FILES[$id]['tmp_name']) <= $maxFileSize
            ) {

                $fname = $_FILES[$id]['name'];
                if (strrpos($fname, '/') !== FALSE) {
                    $fname = substr($fname, strrpos($fname, '/'));
                }

                $info = pathinfo($fname);
                $fname = uniqid($info['filename'] . '_');
                $fname = $fname . '.' . $info['extension'];
                $fdata = file_get_contents($_FILES[$id]['tmp_name']);

                $error = "";
                $fname = ltrim(trim($fname), '/');

                $file = new Core_File();

                $uploadStatus = $file->uploadFile($fname, $fdata, S3_CLIENTS_LOGO_DIR, true);
                if (!$uploadStatus) {
                    $error = 'Upload file ' . $fname . ' failed';
                }

                $return['success'] = 0;

                if (!empty($error)) {
                    $return['success'] = 0;
                    $return['errors'] = array();
                    $return['errors'][] = $error;
                } else {
                    $return['success'] = 1;
                    $return['file'] = $fname;
                }
            } else {
                $return['success'] = 0;
                $return['errors'] = array();
                if (empty($id)) {
                    $return['errors'][] = 'Incorrect parameters: ID is required and cannot be empty';
                } else if (empty($_FILES["{$id}"])) {
                    //$return['errors'][]= 'File not uploaded';
                } else if (!is_readable($_FILES["{$id}"]['tmp_name'])) {
                    $return['success'] = -1;
                    $return['errors'][] = 'Uploaded file not readabale';
                } else if (filesize($_FILES["{$id}"]['tmp_name']) > $maxFileSize) {
                    $return['errors'][] = 'Uploaded file size too big';
                } else if ($_FILES["{$id}"]['error'] != UPLOAD_ERR_OK) {
                    switch ($_FILES["{$id}"]['error']) {
                        case UPLOAD_ERR_FORM_SIZE :
                            $return['errors'][] = 'Uploaded file size too big';
                            break;
                        case UPLOAD_ERR_PARTIAL :
                            $return['errors'][] = 'The uploaded file was only partially uploaded';
                            break;
                        case UPLOAD_ERR_NO_FILE :
                        case UPLOAD_ERR_NO_TMP_DIR :
                        case UPLOAD_ERR_CANT_WRITE :
                        case UPLOAD_ERR_EXTENSION :
                            $return['errors'][] = 'No file was uploaded';
                    }
                }
            }
        }

       // die("updateclientAction3");    
        $response = $this->getResponse();
        $response->clearHeaders()->clearBody();
        $return = json_encode($return);

        $response->setHeader('Content-Length', strlen($return), true);
        $response->setBody($return);
    }

//--------------------------------------------------------
    public function ajaxUploadFileAction() {
        $this->_helper->viewRenderer->setNoRender(true);
        $company = $this->view->company;

        $maxFileSize = 52428800; //50Mb

        $request = $this->getRequest();
        $id = 'Company_Logo';
        //$id         = $request->getParam('file', NULL);
        $descr = $request->getParam('descr', NULL);
        $client_id = $this->getRequest()->getParam('client_id', null);
        $return = array('success' => 0);

        if (!empty($_FILES["{$id}"])) {
            if (
                    is_readable($_FILES[$id]['tmp_name']) &&
                    $_FILES[$id]['error'] == UPLOAD_ERR_OK &&
                    filesize($_FILES[$id]['tmp_name']) <= $maxFileSize
            ) {
                $uploader = new Core_Api_User();

                $fname = $_FILES[$id]['name'];
                if (strrpos($fname, '/') !== FALSE) {
                    $fname = substr($fname, strrpos($fname, '/'));
                }

                $info = pathinfo($fname);
                $fname = uniqid($info['filename'] . '_');
                $fname = $fname . '.' . $info['extension'];
                $fdata = file_get_contents($_FILES[$id]['tmp_name']);

                $result = $uploader->uploadFileForClient($client_id, $fname, $fdata);
                $return['success'] = 0;
                $return['errors'] = array();

                if (!empty($result->errors)) {
                    $return['success'] = 0;
                    $return['errors'] = array();
                    foreach ($result->errors as $e) {
                        $return['errors'][] = $e->message;
                    }
                } else {
                    $return['success'] = 1;
                }
            } else {
                $return['success'] = 0;
                $return['errors'] = array();
                if (empty($id)) {
                    $return['errors'][] = 'Incorrect parameters: ID is required and cannot be empty';
                } else if (empty($_FILES["{$id}"])) {
                    //$return['errors'][]= 'File not uploaded';
                } else if (!is_readable($_FILES["{$id}"]['tmp_name'])) {
                    $return['success'] = -1;
                    $return['errors'][] = 'Uploaded file not readabale';
                } else if (filesize($_FILES["{$id}"]['tmp_name']) > $maxFileSize) {
                    $return['errors'][] = 'Uploaded file size too big';
                } else if ($_FILES["{$id}"]['error'] != UPLOAD_ERR_OK) {
                    switch ($_FILES["{$id}"]['error']) {
                        case UPLOAD_ERR_FORM_SIZE :
                            $return['errors'][] = 'Uploaded file size too big';
                            break;
                        case UPLOAD_ERR_PARTIAL :
                            $return['errors'][] = 'The uploaded file was only partially uploaded';
                            break;
                        case UPLOAD_ERR_NO_FILE :
                        case UPLOAD_ERR_NO_TMP_DIR :
                        case UPLOAD_ERR_CANT_WRITE :
                        case UPLOAD_ERR_EXTENSION :
                            $return['errors'][] = 'No file was uploaded';
                    }
                }
            }
        }

        $response = $this->getResponse();
        $response->clearHeaders()->clearBody();
        $return = json_encode($return);

        $response->setHeader('Content-Length', strlen($return), true);
        $response->setBody($return);
    }

    //-------------------------------------------------------
    public function doUpdateclientAction() {
       // die("updateclientAction3");    
        $this->_helper->viewRenderer->setNoRender(true);
        $return = array('success' => 0);
        $needUpdate = true;
        $client_id = $this->getRequest()->getParam('client_id', null);
        $params = $this->getRequest()->getParams();
        $user = new Core_Api_User();
        $Company_ID = $this->getRequest()->getParam('Company_ID', null);
        $FSAccountManagerId = $this->getRequest()->getParam('FSAccountManagerId', null);
        $updateForm = $this->getRequest()->getParam('UpdateForm', null);

        $params = $this->getRequest()->getParams();

        $formFields = array();
        foreach ($params as $key => $value) {
            if (strstr($key, 'EditRecord') !== null) {
                $formFields[str_replace('EditRecord', '', $key)] = $value;
            }
        }
         $clientInfo = $user->loadById($client_id);
         
        if (!empty($formFields['PasswordCurrent'])) {

            $cid = $clientInfo->ClientID;
            if (empty($cid)) {
                // wrong password confirmation
                $return['success'] = 0;
                $needUpdate = false;
                $return['errors'][] = 'Wrong Password';
            }

            $errorMessages = new Core_Form_ErrorHandler('updateClient_' . $cid);
            $errorMessages->clearMessages();


            if (empty($formFields['Password']) || $formFields['Password'] != $formFields['PasswordConfirm']) {

                $return['success'] = 0;
                $needUpdate = false;
                if (empty($return['errors']))
                    $return['errors'] = array();
                $return['errors'][] = 'Incorrect Password Confirmation';
            }
        }

        if (empty($formFields['PasswordCurrent'])
                || empty($formFields['Password'])
                || empty($formFields['PasswordConfirm'])
                || $formFields['Password'] != $formFields['PasswordConfirm']
        ) {
            // dont update password fields
            unset($formFields['PasswordCurrent']);
            unset($formFields['Password']);
            unset($formFields['PasswordConfirm']);
        }

        $allowedToUpdate = array(
            'CompanyName',
            'EIN',
            'CompanyDescription',
            'ContactName',
            'BusinessAddress',
            'BusinessCity',
            'BusinessState',
            'BusinessZip',
            'BusinessZip',
            'Country',
            'ContactPhone1',
            'ContactPhone2',
            'Email1',
            'Email2',
            'WebsiteURL',
            'Password',
            'DeniedTech_NoShowGreater',
            'DeniedTech_NoShowPeriod',
            'DeniedTech_BackOutGreater',
            'DeniedTech_BackOutPeriod',
            'DeniedTech_PreferenceRatingSmaller',
            'DeniedTech_PreferenceRatingPeriod',
            'DeniedTech_PerformanceRatingSmaller',
            'DeniedTech_PerformanceRatingPeriod'
        );
        $allowedToUpdateExt = array(
            'FSClientServiceDirectorId',
            'FSAccountManagerId',
            'File1URL',
            'File2URL',
            'File3URL',
            'Upload1Date',
            'Upload2Date',
            'Upload3Date'
        );
        $updateFields = array();
        $updateFieldsExt = array();
        foreach ($formFields as $key => $value) {
            if (in_array($key, $allowedToUpdate)) {
                $updateFields[$key] = $value;
            }
            if (in_array($key, $allowedToUpdateExt)) {
                $updateFieldsExt[$key] = $value;
            }
        }
        if($updateFieldsExt['Upload1Date'] != "")
        {
            $updateFieldsExt['Upload1Date'] = 
                date_format(date_create($updateFieldsExt['Upload1Date'])
                        , 'Y-m-d');
        }
        if($updateFieldsExt['Upload2Date'] != "")
        {
            $updateFieldsExt['Upload2Date'] = 
                date_format(date_create($updateFieldsExt['Upload2Date'])
                        , 'Y-m-d');
        }
        if($updateFieldsExt['Upload3Date'] != "")
        {
            $updateFieldsExt['Upload3Date'] = 
                date_format(date_create($updateFieldsExt['Upload3Date'])
                        , 'Y-m-d');
        }
        $file = new Core_File();
        if ($formFields["chkAgreement"] == "true") {
            $file->removeFile($updateFieldsExt['File1URL'], S3_CLIENTS_LOGO_DIR);
            $updateFieldsExt['File1URL'] = "";
        }
        if ($formFields["chkInformation"] == "true") {
            $file->removeFile($updateFieldsExt['File2URL'], S3_CLIENTS_LOGO_DIR);
            $updateFieldsExt['File2URL'] = "";
        }
        if ($formFields["chkService"] == "true") {
            $file->removeFile($updateFieldsExt['File3URL'], S3_CLIENTS_LOGO_DIR);
            $updateFieldsExt['File3URL'] = "";
        }
        if (!empty($formFields['Company_Logo_Rm']) && $formFields['Company_Logo_Rm'] == 1) {
            // remove client logo
            $clientInfo = $user->loadById($client_id);

            $file = new Core_File();
            $file->removeFile($clientInfo->Company_Logo, S3_CLIENTS_LOGO_DIR); // Remove old file
            $errors = Core_Api_Error::getInstance();
            $errors->resetErrors();
            $updateFields['Company_Logo'] = '';
        }
        //14144
        $WoTagClass = new Core_Api_WoTagClass(); //14144
        $clientTagValues = $WoTagClass->getClientTagValues($client_id);    //14144
       

        $search = new Core_Api_Class();

        if ($needUpdate) {
            $result = $search->updateProfileForClientFromAdminSide($updateFields, $updateFieldsExt, $client_id, $this->_login,1,1);
         //  echo("<pre>"); print_r($result); echo("</pre>"); die();

            //print_r($updateFieldsExt);die();
            $errors = Core_Api_Error::getInstance();
            if (!$result->success) {
                foreach ($result->errors as $error) {
                    $return['success'] = 0;
                    $return['errors'] = array($error->message);
                }
            } else {
                $search->updateProfileForRepClientFromAdminSide($updateFields, $updateFieldsExt, $client_id, $this->_login);
                $user->SaveDeductPercent_FSSFeeFrTech($clientInfo->Company_ID,$params['PcntDeductPercent'],$this->_login,$client_id);
                $return['success'] = 1;
            }
        }
        // save tag
        $WoTagClass = new Core_Api_WoTagClass();
        $arrayTab = array();
        foreach($WoTagClass->tagCodeIdArray as $tagCodeId){
            if(isset($params[$tagCodeId])){
                $arrayTab[] = array('TagCodeId'=>$tagCodeId, 'TagItemId'=>$params[$tagCodeId]);
            }
        }
        if(!empty($arrayTab)){
          
            //$WoTagClass->saveClientTagValues($result->data[0]->ClientID,$arrayTab,$this->_login); //14136
            
           
             $WoTagClass->saveClientTagValues($client_id,$arrayTab,$this->_login); //14136 
             
               
            
        }
        $WosClass  = new Core_Api_WosClass();
        $WosClass->updateAM_AllNonInvoiceWOs_ofCompany($Company_ID,$FSAccountManagerId);
         $WoTagClass = new Core_Api_WoTagClass();
        $clientTagValues = $WoTagClass->getClientTagValues($client_id);
    //echo("<pre>"); print_r($clientTagValues); echo("</pre>");  die();
    //14144  send mail
        if(strtolower($clientTagValues["ClientStatusId"]["TagItemName"]) =="inactive" || strtolower($clientTagValues["ClientStatusId"]["TagItemName"]) =="lost" || strtolower($clientTagValues["ClientStatusId"]["TagItemName"]) =="dead")
       
        {
                             $Core_Api_User= new Core_Api_User();              
                             $updateFields["cliaccess_atrib_value"] = $Core_Api_User->setAllAccessAttribToRestricted($Company_ID);
                             $apiemailrestricted = new Core_EmailForRestricted();
                             $user = $Core_Api_User->loadById($client_id);
                              // $companyID = $user->getCompanyId();
                               
                                $clientInfo = $Core_Api_User->loadWithExt(
                                array(
                                    'login' => $clients[0]['UserName'],
                                    'password' => $clients[0]['Password']
                                ));
                                $CSD =$clientInfo->FSClientServiceDirectorEmail;
                                 $AM =$clientInfo->FSAccountManagerEmail;
                                $CompanyName = $user->getCompanyName();
                                $apiemailrestricted->setCompanyId($Company_ID);
                                $apiemailrestricted->setCompanyName($CompanyName);
                                $apiemailrestricted->settoMail($AM.",".$CSD);
                                $apiemailrestricted->setusername($this->_login);
                                $apiemailrestricted->setrestrictedtype(2);
                                $apiemailrestricted->sendMail();       
        }
                //14144    
        // print_r($WosClass);  die();   
        //echo($WosClass);
        $response = $this->getResponse();
        $response->clearHeaders()->clearBody();
        $return = json_encode($return);

        $response->setHeader('Content-Length', strlen($return), true);
        $response->setBody($return);
    }

    //--------------------------------------------------------
    public function getclientcompanyAction() {
        $this->_helper->viewRenderer->setNoRender(true);
        $company_id = $this->getRequest()->getParam('company_id', null);
        $return = array();
        $return['client_id'] = null;
        $return['options'] = "";
        $user = new Core_Api_User;
        //14046
        $repClient = $user->getRepClientforCompanyOnyOne($company_id);
        if(!empty($repClient) && count($repClient)>0) {
            $return['client_id'] = $repClient['ClientID'];   
        }
        //end 14046
        $response = $this->getResponse();
        $response->clearHeaders()->clearBody();
        $return = json_encode($return);

        $response->setHeader('Content-Length', strlen($return), true);
        $response->setBody($return);
    }

    //--- 13457
    public function accesssettingprofileAction() 
    {
        //--- enable 21-Day Restiction Tool ?
        //---    
        $Core_Api_CommonClass = new Core_Api_CommonClass;
        $Attributes = $Core_Api_CommonClass->getAllClientAccessAttributes();
        $Core_Api_Class = new Core_Api_Class;
        $Core_Api_User = new Core_Api_User();

        $client_id = $this->getRequest()->getParam('client_id', null);
        $_mode = $this->getRequest()->getParam('mode', null);
        $mode1 = ($_mode == 'edit') ? true : false;
        $client = $Core_Api_User->loadById($client_id);
        $companyID = $client->getCompanyId();
        $this->view->mode = $mode1;
        if (!$mode1) {
            
            $Core_Api_User->SetAccessDefaultValuesForClient($client_id);
        }
        //--- enable 21-Day Restiction Tool ?
        $additionalValues = $Core_Api_User->getClientCompanyAdditionalFields($companyID);
        $enableAuto21DayRestricTool=1;
        if(empty($additionalValues))
        {
            $Core_Api_User->saveClientCompanyAdditionalFields($companyID,
                array('EnableAuto21DayRestricTool',1)
            );
        }
        else
        {
            $enableAuto21DayRestricTool = $additionalValues['EnableAuto21DayRestricTool'];
        }
        //--- is client restricted ?
        
        //14144
        $WoTagClass = new Core_Api_WoTagClass();
        $clientTagValues = $WoTagClass->getClientTagValues($client_id);
        if(strtolower($clientTagValues["ClientStatusId"]["TagItemName"]) =="inactive" || strtolower($clientTagValues["ClientStatusId"]["TagItemName"]) =="lost" || strtolower($clientTagValues["ClientStatusId"]["TagItemName"]) =="dead")
       
        {
          
          $isRestricted=true;     
        
        }
        else
        {
            $isRestricted = $Core_Api_User->isClientCompanyRestricted($companyID);  
          
        }  
          //14144  
         
        $this->view->isRestricted = $isRestricted;
      //  print_r("Retrixc:");print_r($this->view->isRestricted);
        //--- lastAssignedBeforeDays , $remainDays, $lastRestrictedDate
        $lastAssignedDate = API_Timestamp::getLastWinAssignedTimestamp_ByCompany($companyID);
        $lastAssignedBeforeDays = -1;
        
        $today = new DateTime();
        
        if(empty($lastAssignedDate)) $lastAssignedBeforeDays=-1;
        else
        {
            $tmpDate = new DateTime($lastAssignedDate);
            $diff = $tmpDate->diff($today);
            $lastAssignedBeforeDays = $diff->days;
        }
        //--- 13571
        $companyAdditionalInfo = $Core_Api_User->getClientCompanyAdditionalFields($companyID);
        $beginDate = $companyAdditionalInfo['BeginDateForCountingNDaysRule'];        
        $beginDateBeforeDays = -1;
        if(!empty($beginDate))
        {
            $tmpDate_1 = new DateTime($beginDate);
            $diff_1 = $tmpDate_1->diff($today);
            $beginDateBeforeDays = $diff_1->days;            
        }
        //--- remainDays
        $remainDays = 0;
        $remainDays_2 = 0;
        
        $remainDays_1 = 0;
        if($beginDateBeforeDays!=-1 && $beginDateBeforeDays<21)
        {            
            $remainDays_1 = 21-$beginDateBeforeDays;
        }
        
        $remainDays_2 = 0;
        if($lastAssignedBeforeDays !=-1 && $lastAssignedBeforeDays<21)
        {
            $remainDays_2 = 21-$lastAssignedBeforeDays;
        }
        $remainDays = $remainDays_1 <= $remainDays_2? $remainDays_2: $remainDays_1;
                
        $lastRestrictedDate = $Core_Api_User->getLastRestrictedDate($companyID);
        $days_sinceRestricted = '';
        if(!empty($lastRestrictedDate))
        {
            $tmpDate = new DateTime($lastRestrictedDate);
            $diff = $tmpDate->diff($today);
            $days_sinceRestricted = $diff->days;
        }
        $this->view->enableAuto21DayRestricTool = $enableAuto21DayRestricTool;
        $this->view->isRestricted = $isRestricted;    
        $this->view->lastAssignedBeforeDays = $lastAssignedBeforeDays;
        $this->view->remainDays = $remainDays;
        $this->view->days_sinceRestricted = $days_sinceRestricted;           
        //--- 
        $Values = $Core_Api_Class->getClientAccessControlValues($client_id);
        $WoTagClass = new Core_Api_WoTagClass(); //14144
        $clientTagValues = $WoTagClass->getClientTagValues($client_id);    //14144
       
        $_Attributes = $Attributes;
        if (!empty($Attributes)) {
            foreach ($Attributes as $i => $Attribute) {
                $_Attributes[$i]['cliaccess_atrib_value'] = 0;
                $_Attributes[$i]['open'] = true;
                $_Attributes[$i]['restricted'] = false;
                if (!empty($Values)) {
                    foreach ($Values as $j => $Value) {
                     
                        //14144
                         if(strtolower($clientTagValues["ClientStatusId"]["TagItemName"]) =="inactive" || strtolower($clientTagValues["ClientStatusId"]["TagItemName"]) =="lost" || strtolower($clientTagValues["ClientStatusId"]["TagItemName"]) =="dead")
       
                            {
                               
                               $Value['cliaccess_atrib_value']=2;  
                              
                            }
                        //14144  
                        if ($Value['cliaccess_atrib_id'] == $Attribute['id']) {
                            $_Attributes[$i]['cliaccess_atrib_value'] = $Value['cliaccess_atrib_value'];
                            if ($Value['cliaccess_atrib_value'] == 1) {
                                $_Attributes[$i]['open'] = true;
                            } else {
                                $_Attributes[$i]['open'] = false;
                            }
                            if ($Value['cliaccess_atrib_value'] == 2) {
                                $_Attributes[$i]['restricted'] = true;
                            } else {
                                $_Attributes[$i]['restricted'] = false;
                            }
                            if (empty($Value['cliaccess_atrib_value'])) {
                                $_Attributes[$i]['restricted'] = false;
                                $_Attributes[$i]['open'] = true;
                            }
                            break;
                        }
                    }
                }
            }
        }
        $this->view->clientAccessAttributes = $_Attributes;
        
    }

    //--- 13457
    public function updateAccesssettingprofileAction() {
        $this->_helper->viewRenderer->setNoRender(true);
        $params = $this->getRequest()->getParams();
        $Core_Api_User = new Core_Api_User();
        $client_id = $params['client_id'];
        $enableAuto21DayRestricTool = $params['EnableAuto21DayRestricTool'];
        
        $user = $Core_Api_User->loadById($client_id);
        $companyID = $user->getCompanyId();
        //743
        $isFullRestricted_old = $Core_Api_User->AreFullRestriction($companyID);
        $isPartialRestricted_old = $Core_Api_User->ArePartialRestriction($companyID);
        $isFullOpened_old = $Core_Api_User->AreFullorPartialRestriction($companyID);
        //end 743
        $json = base64_decode($params['json']);
        $datas = json_decode($json);
        $Core_Api_Class = new Core_Api_Class();
        $oldData = $Core_Api_Class->getClientAccessControlValues($client_id);
        if (count($datas) > 0 && !empty($datas)) {
            foreach ($datas as $i => $item) {
                $Core_Api_User->editClientAccessValue($client_id, $item->id, $item->val);
            }
            // for bac duong
            $Core_Api_User->WriteAccessForRepClientFromAdmin($client_id, $this->_login, $oldData);
            //--- 13457 update 'BeginDateForCountingNDaysRule' field
            if(!$Core_Api_User->isClientCompanyRestricted($companyID) && !$enableAuto21DayRestricTool)
            {
                $beginDateForCountingNDaysRule = date("Y-m-d");
            }
            else
            {
                $beginDateForCountingNDaysRule = '1970-01-01';
            }            
            $fieldvals=array('BeginDateForCountingNDaysRule'=>$beginDateForCountingNDaysRule);
            $Core_Api_User->saveClientCompanyAdditionalFields($companyID,$fieldvals);
        }
        //--- update EnableAuto21DayRestricTool
        $Core_Api_User->saveClientCompanyAdditionalFields($companyID,
            array('EnableAuto21DayRestricTool'=>$enableAuto21DayRestricTool)
        );
        //743
        $apiemailrestricted = new Core_EmailForRestricted();
        $clients = $Core_Api_User->getRepClientforCompany($companyID);

        $clientInfo = $Core_Api_User->loadWithExt(
                        array(
                            'login' => $clients[0]['UserName'],
                            'password' => $clients[0]['Password']
                        ));
        $CSD =$clientInfo->FSClientServiceDirectorEmail;
        $AM =$clientInfo->FSAccountManagerEmail;
        $isFullRestricted = $Core_Api_User->AreFullRestriction($companyID);
        $isPartialRestricted = $Core_Api_User->ArePartialRestriction($companyID);
        $isFullOpened=$Core_Api_User->AreFullorPartialRestriction($companyID);

        $restrictedtype = 0;
        if($isFullRestricted)
        {
            $restrictedtype = 1;
        }
        else if($isPartialRestricted)
        {
            $restrictedtype = 2;
        }
        else if(!$isFullOpened)
        {
            $restrictedtype = 4;
        }
        $issendmail=FALSE;
        if(($isFullRestricted != $isFullRestricted_old)
                || ($isPartialRestricted != $isPartialRestricted_old)
                || ($isFullOpened!=$isFullOpened_old))
        {
            $issendmail = TRUE;
        }
        
        if($issendmail)
        {
            $CompanyName = $user->getCompanyName();
            $apiemailrestricted->setCompanyId($companyID);
            $apiemailrestricted->setCompanyName($CompanyName);
            $apiemailrestricted->settoMail($AM.",".$CSD);
            $apiemailrestricted->setusername($this->_login);
            $apiemailrestricted->setrestrictedtype($restrictedtype);
            $apiemailrestricted->sendMail();
        }
        //end 743
        
        //---
        $return['success'] = 1;
        $response = $this->getResponse();
        $response->clearHeaders()->clearBody();
        $return = json_encode($return);

        $response->setHeader('Content-Length', strlen($return), true);
        $response->setBody($return);
    }

    //--------------------------------------------------------
    public function activitylogprofileAction() {
        $request = $this->getRequest();
        $params = $request->getParams();
        $size = (int) ($request->getParam('size', 10));
        $size = ( $size <= 10 ) ? 10 : ( ( $size >= 100 ) ? 100 : $size );
        $page = ($request->getParam('page', 1)) + 0;
        $page = ( $page < 1 ) ? 1 : $page;
        $offset = ($page * $size) - $size;
        $client_id = $request->getParam('client_id', 0);

        $Core_Api_User = new Core_Api_User();
        $countRows = $Core_Api_User->GetTotalOfAccessLogForClient($client_id);
        $Logs = $Core_Api_User->GetListOfAccessLogForClient($client_id, $size, $offset);


        if ($countRows > 0) {

            $paginator = new Zend_Paginator(new Core_Paginator_Sliced_Array($Logs, $countRows));
            $paginator->setCurrentPageNumber($page);
            $this->view->paginator = $paginator;
        }

        $this->view->Logs = $Logs;
    }

    //--------------------------------------------------------
    public function getstatisAction() {
        $this->_helper->viewRenderer->setNoRender(true);
        $contry_id = $this->getRequest()->getParam('contry_id', null);
        $return = array();
        $client = new Core_Api_Class();

        $countrycodes = array("US","CA","MX","BR","UK","KY","BS");
        $apiCommon = new Core_Api_CommonClass();   
        $states = $apiCommon->getStatesArray("$contry_id");
        $options = '<option data="0"  statecode="" contrycode="" value="">Select State</option>';
        if(!empty($states->data) && in_array($contry_id,$countrycodes))
        {
            $states = $apiCommon->getStatesArray("$contry_id");
            foreach ($states->data as $stateCode => $stateVal) {
                $options .= '<option 
                        contrycode="'.$contry_id.'"  value="' . $stateCode . '">'
                        . $stateVal . '</option>';
            }
            }
        $options .= '<option data="2" value="Other">Other</option>';
        
        //----
        $response = $this->getResponse();
        $response->clearHeaders()->clearBody();

        $response->setHeader('Content-Length', strlen($return), true);
        $response->setBody($options);
    }
    public function userCreateAction() {
        $errorMessages = new Core_Form_ErrorHandler('createClient');
        $this->view->messages = $errorMessages->getMessages();
        $company_id = $this->getRequest()->getParam('Company_ID', null);
        $this->view->Company_ID = (!empty($company_id) ) ? $company_id : $this->view->company;
        $errorMessages->clearMessages();
    }

    public function doUserCreateAction() {
        $this->_helper->viewRenderer->setNoRender(true);
        $errorMessages = new Core_Form_ErrorHandler('createClient');
        $request = $this->getRequest();
        $client_id = $this->getRequest()->getParam('client_id', 0);	
        //$createForm = $request->getParam('CreateForm', null);
        $createForm = $this->getRequest()->getParams();
        @$this->_company = $createForm['Company_ID'];
		$filterArr = array("module", "controller", "action","client_id");
		foreach($createForm as $k=>$v){
			if(in_array($k,$filterArr)){
				unset($createForm[$k]);
}
		}
        
        if (empty($createForm))
            $errorMessages->addMessage('Create Client Form is empty');
        $errorMessages->clearMessages();

        $clientClass = new Core_Api_AdminClass();
        //13832
        $Core_Api_User = new Core_Api_User;        
        $repClientInfo = $Core_Api_User->getRepClientforCompany($createForm["Company_ID"],1);
        if(!empty($repClientInfo) && count($repClientInfo)>0){
            $createForm["CompanyName"] = $repClientInfo["CompanyName"];
        } else {
            $createForm["CompanyName"] = $createForm["Company_ID"];            
        }        
        //end 13832
        if(isset($createForm['UserType']) &&  $createForm['UserType']=='Admin') 
        {
            $createForm['Admin']=1;
        }            
        if(isset($createForm['Admin']) &&  $createForm['Admin']==1) 
        {
            $createForm['UserType']='Admin';
        }                
        //--- create client    
        $result = $clientClass->createClient_NoCheck($createForm);
        if (!$result->success) {
            foreach ($result->errors as $error){
                $errorMessages->addMessage($error->message);
            }
        } else {
            $id = $result->data[0]->ClientID;
  
            $Apiuser = new Core_Api_User();
            $msgAccountEnabled = "User Added: ".$createForm["UserName"];
            $Apiuser->add_profile_action_log($client_id,$this->_login,$msgAccountEnabled);
         
            
            // send email
            $toEmail = $createForm['Email1']; //$techEmail;
            $fromName = "FieldSolutions Support";
            $fromEmail = "support@fieldsolutions.com";
           
            $subject = "Welcome to FieldSolutions!";
            $body = "Welcome to FieldSolutions, ".$createForm['ContactName']."!
                
                    Your login information is:
                    
                    Username: ".$createForm['UserName']."
                    Temporary Password: ".$createForm['Password']."
                        
                    As a safety precaution, please update your temporary password when you first log into the FieldSolutions site. To update your password, click on 'Client Settings' in the blue Navigation Bar at the upper right of your screen, click the 'Edit' button, and scroll down to the bottom of the page.
                    
                    We're looking forward to working with you!
                    
                    Thank you,
                    
                    Your FieldSolutions Team
                    ";
            $mail = new Core_Mail();
            $mail->setBodyText($body);
            $mail->setFromName($fromName);
            $mail->setFromEmail($fromEmail);
            $mail->setToEmail($toEmail);
            $mail->setSubject($subject);
            $mail->send();
        }
        $err = $errorMessages->getMessages();
        if (empty($err)){
        	$this->_helper->json(array("success" => true, ""));
            //$this->_redirect('../clients/profile.php?v=' . $createForm['Company_ID']);
            
        }else{
        	$this->_helper->json(array("success" => false, ""));
        }
        //$this->_redirect($_SERVER['HTTP_REFERER']);
       
    }
    public function userListAction() {
        $request = $this->getRequest();
        $params = $request->getParams();

        $size = ($request->getParam('size', 20)) + 0;
        $size = ( $size <= 10 ) ? 10 : ( ( $size >= 100 ) ? 100 : $size );
        $page = ($request->getParam('page', 1)) + 0;
        $page = ( $page < 1 ) ? 1 : $page;

        $sortBy = $request->getParam('sortBy', 'UserName');
        $sortDir = $request->getParam('sortDir', 'asc');
        $this->view->client_id = $this->getRequest()->getParam('ClientID', 0);
        $this->view->sortBy = $sortBy;
        $this->view->sortDir = $sortDir;

        $company_id = $this->getRequest()->getParam('Company_ID', null);
        $this->view->Company_ID = (!empty($company_id) ) ? $company_id : $auth->companyId;

        $search = new Core_Api_Class;
        
        $countRows = 0;
       
        $this->view->accountEnabled = $request->getParam('AccountEnabled', NULL);
        
        if($params['AccountEnabled'] == '0')unset($params['AccountEnabled']);
        
        $clients = $search->getClientsList_NoCheck($params, 
            (!empty($sortBy) && !empty($sortDir) ) ? "{$sortBy} {$sortDir}" : null, 
            ($page - 1) * $size, 
            $size, $countRows
        );
        
        
        //remove account enabled filter
        if(isset($params['AccountEnabled']))unset($params['AccountEnabled']);

        $allClients = $search->getClientsList_NoCheck($params, 
            (!empty($sortBy) && !empty($sortDir) ) ? "{$sortBy} {$sortDir}" : null, 
            ($page - 1) * $size
        );

        if ($clients->success) {
            $paginator = new Zend_Paginator(new Core_Paginator_Sliced_Array($clients->data, $countRows));
            $paginator->setItemCountPerPage(20)->setCurrentPageNumber($page);
            $this->view->paginator = $paginator;
            $this->view->fullClientList = $allClients->data;
        }
        $this->view->success = $clients->success;
        $this->view->errors = $clients->errors;
    }
    
    public function userRemoveAction(){
    	$this->_helper->viewRenderer->setNoRender(true);
    	$request = $this->getRequest();
        $params = $request->getParams();
        $clientId = $this->getRequest()->getParam('clientID', null);
        $client_id = $this->getRequest()->getParam('client_id', 0);           
        
        $user = new Core_Api_User($clientId);
        $Apiuser = new Core_Api_User();
        $userinfo = $Apiuser->loadById($clientId); 
        $return = array();
        if($user->remove()){
            $Apiuser = new Core_Api_User();
            $msgAccountEnabled = "User Deleted: ".$userinfo->UserName;
            $Apiuser->add_profile_action_log($client_id,$this->_login,$msgAccountEnabled); 
            $return['success'] = 0;
            $return['errors'] = array();
        }
        
         $response = $this->getResponse();
         $response->clearAllHeaders()->clearBody();
         $return = json_encode($return);
         $response->setHeader('Content-type', 'application/json');
         $response->setHeader('Content-Length', strlen($return), true)
                  ->setBody($return);
    }

    public function userUpdateAction() {
        $client_id = $this->getRequest()->getParam('id', null);
        $user = new Core_Api_User($client_id);
        $client = $user->loadById($client_id);
        $this->view->client_id = $this->getRequest()->getParam('client_id', 0);
        
        $errorMessages = new Core_Form_ErrorHandler('updateClient_' . $client_id);

        $this->view->client = $client;
        $this->view->success = $client !== false;

        $search = new Core_Api_Class;

        //get states
        $states = $search->getStates();
        $this->view->states = $states->data;

        $errors = Core_Api_Error::getInstance();
        $er = $errors->getErrorArray();
        foreach ($er as $k => $v)
            $errorMessages->addMessage($error->message);

        $this->view->messages = $errorMessages->getMessages();
        $errorMessages->clearMessages();
    }

    public function doUserUpdateAction() {
        $this->_helper->viewRenderer->setNoRender(true);
        $updateForm = $this->getRequest()->getParam('UpdateForm', null);
        $id = $this->getRequest()->getParam('ClientID', null);
        $removefiles = $this->getRequest()->getParam('removefiles', null);
        $this->_company = $this->getRequest()->getParam('company', null);
        $errorMessages = new Core_Form_ErrorHandler('updateClient_' . $id);
        $client_id = $this->getRequest()->getParam('client_id', 0);
        
        $errorMessages->clearMessages();
        if (!empty($removefiles))
            foreach ($removefiles as $rmf)
                $updateForm[$rmf] = '';

        if (empty($updateForm) || empty($id))
            $errorMessages->addMessage('Update Client Form is empty');
        $er = $errorMessages->getMessages();
        if (empty($er)) {
            $search = new Core_Api_AdminClass;
            $checkboxes = array('AccountEnabled', 'Admin');
            foreach ($checkboxes as $c)
                $updateForm[$c] = isset($updateForm[$c]) ? 1 : 0;
            $Apiuser = new Core_Api_User();
            $user = $Apiuser->loadById($id);
         
            if(isset($updateForm['UserType']) &&  $updateForm['UserType']=='Admin') 
            {
                $updateForm['Admin']=1;
            }            
            if(isset($updateForm['Admin']) &&  $updateForm['Admin']==1) 
            {
                $updateForm['UserType']='Admin';
            }                
            //echo("updateForm: <pre>");print_r($updateForm);//test
            $result = $search->updateClient($this->_login, $this->_password, $id, $updateForm);
            
            $errors = Core_Api_Error::getInstance();
            if (!$result->success) {
                foreach ($result->errors as $error) {
                    $errorMessages->addMessage($error->message);
                }
            } else {
  
                if ($updateForm['AccountEnabled'] != $user->AccountEnabled) {
                    $msgAccountEnabled = "";
                    if ($updateForm['AccountEnabled']) {
                        $msgAccountEnabled = "User Activated: " . $user->UserName;
                    } else {
                        $msgAccountEnabled = "User Marked Inactive: " . $user->UserName;
                    }
                    $Apiuser->add_profile_action_log($client_id, $this->_login, $msgAccountEnabled);
                }
                $fieldChanges = array(
                    'ContactName' => 'Contact Name',
                    'ContactPhone1' => 'Primary Phone',
                    'ContactPhone2' => 'Secondary Phone',
                    'Email1' => 'Primary Email',
                    'Email2' => 'Secondary Email',
                    'Password' => 'Password',
                    'UserType' => 'User Type');
                $field = "";
                foreach ($fieldChanges as $key => $value) {
                    if (trim($updateForm[$key]) != trim($user->$key)) {
                        $field .= $value . ", ";
                    }
                }
                if (!empty($field)) {
                    $field = substr($field, 0, count($field) - 3);
                    $msgAccountEnabled = "Edits to " . $user->UserName . " Contact Information: " . $field;
                    $Apiuser->add_profile_action_log($client_id, $this->_login, $msgAccountEnabled);
                }
            }
        }
        $err = $errorMessages->getMessages();
        if (empty($err)){
           $this->_redirect('../admin/admin_client_profile.php?type=Client&client_id='.$this->getRequest()->getParam('client_id', 0).'&tabid=clientUsers');
        }    
        $this->_redirect($_SERVER['HTTP_REFERER']);
    }
    
    public function validusernameAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $return = array();
        
        $username = $this->getRequest()->getParam('UserName', null);
        if(!empty($username))
        {
            $core = new Core_Api_User();
            if($core->isDuplicated_Username($username)==1)
            {
                $return['ok'] = 0;
                $return['errors'] = array('Desired Username must be unique');
}
            else
            {
                $return['ok'] = 1;
                $return['errors'] = array();
            }
        }
        else
        {
             $return['ok'] = 0;
             $return['errors'] = array('Desired Username is empty but required');
        }
        
        $response = $this->getResponse();
        $response->clearAllHeaders()->clearBody();
        $return = json_encode($return);
        $response->setHeader('Content-type', 'application/json');
        $response->setHeader('Content-Length', strlen($return), true)
                  ->setBody($return);
                  
    }
    //13809
    function clientProfileUpdateEntityAction(){
        $pro = $this->getRequest()->getParam('pro', "");
        $WoTagClass = new Core_Api_WoTagClass(); 
        $ProjectClass = new Core_Api_ProjectClass();
        
        if($pro == "update"){
            $this->_helper->viewRenderer->setNoRender(true);
            $params = $this->getRequest()->getParams();
            // save tag
            $WoTagClass = new Core_Api_WoTagClass();
            $arrayTab = array();
            $arrayTab[] = array('TagCodeId'=>'EntityTypeId', 'TagItemId'=>$params['EntityTypeId']);
            $WoTagClass->saveClientTagValues($params['client_id'],$arrayTab,$this->_login);
            $projects = $params['Project'];
            $projectTagArray = array();
            //13809
            if(!empty($projects) && is_array($projects)){
                foreach($projects as $projectID){
                    $newValue = $params['EntityType_'.$projectID];
                    $projectInfo = $ProjectClass->getProjectById($projectID);
                    $oldValue = $projectInfo['EntityTypeId'];
                    if($newValue != $oldValue)
                    {
                        $projectTagArray[] = array('Project_ID'=>$projectID,'TagCodeId'=>'EntityTypeId', 'TagCodeValue'=>$newValue);
                    }
                }
            }
            if(count($projectTagArray) > 0)
            {
            $WoTagClass->saveProjectTagArrayValues($projectTagArray,$params["Applyupdate"]);
            }
            //end 13809
            
            $this->_helper->json(array("success" => true,$projectTagArray));
        }else{
            $this->view->EntityTypeId = $this->getRequest()->getParam('EntityTypeId', "");
            $this->view->EntityTypeList = $WoTagClass->getEntityTypeList();

            $client_id = $this->getRequest()->getParam('client_id', null);
            $this->view->client_id = $client_id;
            $user = new Core_Api_User($client_id);
            $client = $user->loadById($client_id);
            $this->view->Projects  = $ProjectClass->getProjectList_ForCompany($client->Company_ID);
        }
    }
    //13809
    function clientProfileUpdateServiceyypeAction(){
        $pro = $this->getRequest()->getParam('pro', "");
        $WoTagClass = new Core_Api_WoTagClass(); 
        $ProjectClass = new Core_Api_ProjectClass();
        
        if($pro == "update"){
            $this->_helper->viewRenderer->setNoRender(true);
            $params = $this->getRequest()->getParams();
            // save tag
            $WoTagClass = new Core_Api_WoTagClass();
            $arrayTab = array();
            $arrayTab[] = array('TagCodeId'=>'ServiceTypeId', 'TagItemId'=>$params['ServiceTypeId']);
            $WoTagClass->saveClientTagValues($params['client_id'],$arrayTab,$this->_login);
            $projects = $params['Project'];
            $projectTagArray = array();
            //13809
            if(!empty($projects) && is_array($projects)){
                foreach($projects as $projectID){
                    $newValue = $params['ServiceType_'.$projectID];
                    $projectInfo = $ProjectClass->getProjectById($projectID);
                    $oldValue = $projectInfo['ServiceTypeId'];
                    if($newValue != $oldValue){                        
                        $projectTagArray[] = array('Project_ID'=>$projectID,'TagCodeId'=>'ServiceTypeId', 'TagCodeValue'=>$newValue);
                    }
                }
            }
            if(count($projectTagArray) > 0){
            $WoTagClass->saveProjectTagArrayValues($projectTagArray,$params["Applyupdate"]);
            }
            //end 13809
            $this->_helper->json(array("success" => true,$projectTagArray));
        }else{
            $this->view->ServiceTypeId = $this->getRequest()->getParam('ServiceTypeId', "");
            $this->view->ServiceTypeList = $WoTagClass->getServiceTypeList();

            $client_id = $this->getRequest()->getParam('client_id', null);
            $this->view->client_id = $client_id;
            $user = new Core_Api_User($client_id);
            $client = $user->loadById($client_id);
            $this->view->Projects  = $ProjectClass->getProjectList_ForCompany($client->Company_ID);
        }
    }
    function clientProfileUpdateCsdAction(){
        $pro = $this->getRequest()->getParam('pro', "");
        $WoTagClass = new Core_Api_WoTagClass(); 
        $CommonClass = new Core_Api_CommonClass();
        $ProjectClass = new Core_Api_ProjectClass();

        if($pro == "update"){
            $this->_helper->viewRenderer->setNoRender(true);
            $params = $this->getRequest()->getParams();
    
            // save tag
            $arrayTab = array();
            $arrayTab[] = array('TagCodeId'=>'FSClientServiceDirectorId', 'TagItemId'=>$params['CSDId']);
            $WoTagClass->saveClientTagValues($params['client_id'],$arrayTab,$this->_login);
            
            $projects = $params['Project'];
            $projectTagArray = array();
            //13809
            if(!empty($projects) && is_array($projects)){
                foreach($projects as $projectID){
                    $newCSDID = $params['CSD_'.$projectID];
                    $projectInfo = $ProjectClass->getProjectById($projectID);
                    $oldCSDID = $projectInfo['FSClientServiceDirectorId'];
                    if($newCSDID != $oldCSDID)
                    {
                        $projectTagArray[] = array('Project_ID'=>$projectID,'TagCodeId'=>'FSClientServiceDirectorId', 'TagCodeValue'=>$newCSDID);
                    }
                }
            }
            if(count($projectTagArray) > 0) {
            $WoTagClass->saveProjectTagArrayValues($projectTagArray,$params["Applyupdate"]);
            }
            //end 13809
            
            $this->_helper->json(array("success" => true,$projectTagArray));
        }else{
            $this->view->ClientServiceDirectorsId = $this->getRequest()->getParam('CSDId', "");
            $this->view->ClientServiceDirectors = $CommonClass->getClientServiceDirectors();
            $client_id = $this->getRequest()->getParam('client_id', null);
            $this->view->client_id = $client_id;
            $user = new Core_Api_User($client_id);
            $client = $user->loadById($client_id);
            $this->view->Projects  = $ProjectClass->getProjectList_ForCompany($client->Company_ID);
        }
    }
    
    //233
    function clientnotupdatepasswordAction()
    {
        $this->view->NotUpdatedPassword=$NotUpdatedPassword;
        
        $request = $this->getRequest();
        $params = $request->getParams();
        $size = (int) ($request->getParam('size', 10));
        $size = ( $size <= 10 ) ? 10 : ( ( $size >= 100 ) ? 100 : $size );
        $page = ($request->getParam('page', 1)) + 0;
        $page = ( $page < 1 ) ? 1 : $page;
        $offset = ($page * $size) - $size;
        
        $PasswordUpdateClass = new Core_Api_PasswordUpdateClass();
        $total = $PasswordUpdateClass->Count_ClientList_NotUpdatedPassword();
        $NotUpdatedPassword = $PasswordUpdateClass->getClientList_NotUpdatedPassword($size,$offset);

        if ($total > 0) {

            $paginator = new Zend_Paginator(new Core_Paginator_Sliced_Array($NotUpdatedPassword, $total));
            $paginator->setCurrentPageNumber($page);
            $this->view->paginator = $paginator;
        }

        $this->view->NotUpdatedPassword = $NotUpdatedPassword;
    }
    function technotupdatepasswordAction()
    {
        $this->view->NotUpdatedPassword=$NotUpdatedPassword;
        
        $request = $this->getRequest();
        $params = $request->getParams();
        $size = (int) ($request->getParam('size', 10));
        $size = ( $size <= 10 ) ? 10 : ( ( $size >= 100 ) ? 100 : $size );
        $page = ($request->getParam('page', 1)) + 0;
        $page = ( $page < 1 ) ? 1 : $page;
        $offset = ($page * $size) - $size;
        
        $PasswordUpdateClass = new Core_Api_PasswordUpdateClass();
        $total = $PasswordUpdateClass->Count_TechList_NotUpdatedPassword();
        $NotUpdatedPassword = $PasswordUpdateClass->getTechList_NotUpdatedPassword($size,$offset);


        if ($total > 0) {

            $paginator = new Zend_Paginator(new Core_Paginator_Sliced_Array($NotUpdatedPassword, $total));
            $paginator->setCurrentPageNumber($page);
            $this->view->paginator = $paginator;
        }

        $this->view->NotUpdatedPassword = $NotUpdatedPassword;
        
    }
    //end 233
    function clientMasterListAction(){
        $params = $this->getRequest()->getParams();
        $search = new Core_Api_AdminClass;
        $this->view->CompanyMasterList = $search->getCompanyMasterList($this->_login, $this->_password);
        
        $CommonClass = new Core_Api_CommonClass();
        $WoTagClass = new Core_Api_WoTagClass(); 
        $this->view->StatusList = $WoTagClass->getClientStatusList(); 
        $this->view->RelationShipList = $WoTagClass->getClientRelationShipList();
        $this->view->SegmentList = $WoTagClass->getSegmentList();
        $this->view->OriginalSellerList = $WoTagClass->getOriginalSellerList();
        $this->view->ExecutiveList = $WoTagClass->getExecutiveList();
        $this->view->EntityTypeList = $WoTagClass->getEntityTypeList();
        $this->view->ServiceTypeList = $WoTagClass->getServiceTypeList();
        $this->view->AccountManagers = $CommonClass->getAccountManagers();
        $this->view->ServiceDirectors = $CommonClass->getClientServiceDirectors();
        $this->view->export = $params["export"];
    }
    function saveClientMasterAction(){
        $this->_helper->viewRenderer->setNoRender(true);
        $request = $this->getRequest();
        $params = $request->getParams();
        $CompanyIDs = $params["CompanyID"];
        $search = new Core_Api_AdminClass;
        if(is_array($CompanyIDs) && !empty($CompanyIDs)){
            foreach($CompanyIDs as $companyID){
                $tagFields = array(
                    'ClientStatusId'=>$params["ClientStatus_".$companyID],
                    'ClientRelationshipId'=>$params["ClientRelationship_".$companyID],
                    'SegmentId'=>$params["Segment_".$companyID],
                    'OriginalSellerId'=>$params["OriginalSeller_".$companyID],
                    'ExecutiveId'=>$params["Executive_".$companyID],
                    'FSClientServiceDirectorId'=>$params["CSD_".$companyID],
                    'FSAccountManagerId'=>$params["AM_".$companyID]);
                $search->updateCompanyTagsInfo($companyID,$tagFields,$this->_login);    
            }
        }
        
        $this->_helper->json(array("success" => true));
    }
    function clientActivityLogAction(){
        $request = $this->getRequest();
        $params = $this->getRequest()->getParams();
        $this->view->search = $params["search"];
        $search = new Core_Api_AdminClass;
        if(!empty($params["search"])){
            $size = 100;
            $page = ($request->getParam('page', 1)) + 0;
            $page = ( $page < 1 ) ? 1 : $page;
            $offset = ($page * $size) - $size;
            
            $filterFields = array(
                'Company_ID'=>$params["clientid"],
                'username'=>$params["username"],
                'logdateGreater'=>(!empty($params["dateGea"])?date_format(new DateTime($params["dateGea"]), 'Y-m-d'):""),
                'logdateLess'=>(!empty($params["DateLe"])?date_format(new DateTime($params["DateLe"]), 'Y-m-d'):""));
            if(!empty($params["sortfield"]) && !empty($params["sortdir"])){
                $sort = $params["sortfield"]." ".$params["sortdir"];
            }
            print_r($sort);
            $ClientActivityLog = $search->getClientActivityLog($filterFields,0,$sort,$size,$offset);
            $total = $search->getClientActivityLog($filterFields,1);
            
            if ($total > 0) {

                $paginator = new Zend_Paginator(new Core_Paginator_Sliced_Array($ClientActivityLog, $total));
                $paginator->setCurrentPageNumber($page);
                $paginator->setItemCountPerPage($size);
                $this->view->paginator = $paginator;
            }
            $this->view->clientid = $params["clientid"];
            $this->view->username = $params["username"];
            $this->view->dateGea = $params["dateGea"];
            $this->view->DateLe = $params["DateLe"];
            $this->view->sortfield = $params["sortfield"];
            $this->view->sortdir = $params["sortdir"];
            $this->view->ClientActivityLog = $ClientActivityLog;
        }
        $fieldsList = array('Company_ID', 'CompanyName');
        $companies = $search->getCompanies($this->_login, $this->_password
        , $fieldsList, 'CompanyName', 'CompanyName');
        $this->view->CompanyMasterList = $companies->data;
        
    }
    //624
    
    //931
    function uploadtagAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        //874
        $request = $this->getRequest();
        $params = $request->getParams();
        
        $fname = $params['fname'];
        $multipletaglevels = $params['multipletaglevels'];
        $hidNumberItem = $params['hidNumberItem'];
        
        if (empty($_FILES))
            break;
        $msgs = array();
        $itemno = 0;
        $errstr ="";
        
        foreach($_FILES as $filestr)
        {
            $itemno++;
            list($width, $height, $type, $attr) = getimagesize($filestr['tmp_name']);
            if ($height > 26)
            {
                $errstr .=  'Tag level '.$itemno.' Art Max height: 26px.\n';
            }
        }
            
        if($errstr!="")
        {
            echo "<script>alert('".$errstr."');</script>";
            die;
        }
        
        $artname="";
        $itemno = 0;

        foreach($_FILES as $filestr)
        {
            $itemno++;
            if($itemno<=$hidNumberItem)
            {
                $fname = $filestr['name'];

                if(empty($fname) || $fname=="")
                {
                    if(isset($params["TagArtName_$itemno"]))
                        $artname .= $params["TagArtName_$itemno"]."|";
                }
                else
                {
                    if (strrpos($fname, '/') !== FALSE)
                    {
                        $fname = substr($fname, strrpos($fname, '/'));
                    }
                    $info = pathinfo($fname);
                    //931
                    $fname = uniqid($info['filename'] . '_') . '.' . $info['extension'];
                    //end 931
                    $fdata = file_get_contents($filestr['tmp_name']);
                    $error = "";
                    $fname = ltrim(trim($fname), '/');

                    $file = new Core_File();
                    $file->uploadFile($fname, $fdata, 'wos-docs/tags', true);
                    $artname .= $fname."|";
                }
            }
        }
        
        echo "<script>window.parent.callPopupSubmit('$artname')</script>";
    }
    //end 931
    function deltagAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $request = $this->getRequest();
        $params = $request->getParams();
        $fname = $params['fname'];
        $file = new Core_File();
        $file->removeFile($fname, 'wos-docs/tags');
    }

    function createfstagAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $request = $this->getRequest();
        $params = $request->getParams();
        $tagTitle = $params['tagtitle'];
        $visibililyID = $params['cbxSystemVisibility'];
        $companyIDArray = $params['Client'];
        
        $tagName = ($params['TagName'] == "")?preg_replace( '/\s+/', '', $tagTitle):$params['TagName'];
        $tagSource = ($params['TagSource'] == "")?"":$params['TagSource'];
        $addByUser = $_SESSION['UserName'];
        
        //13912
        $hideFromTechs = $params['techHide'];                
         if(isset($params['canNonGPMEdit'])) { 
            $canNonGPMEdit = 1; 
         }else { 
            $canNonGPMEdit = 0; 
         }
         if(isset($params['canGPMEdit'])) { 
            $canGPMEdit = 1; 
         }else { 
            $canGPMEdit = 0; 
         }
         if(isset($params['canTechEdit'])) { 
            $canTechEdit = 1; 
         }else { 
            $canTechEdit = 0; 
         }
         //931
        if(isset($params['multipletaglevels']))
        { 
            $multipletaglevels = 1; 
            $fname="";
            $tagLevels = explode("|",$params['fname']);
            $i=1;
            foreach($tagLevels as $tag)
            {
                if($i<=$params['hidNumberItem'])
                {
                    $tagLevelsData[]=array('Title'=>$params["tagTitleLevel_$i"],'TagArt'=>$tag,'DisplayOrder'=>$i);
                }
                $i++;
            }
        }
        else
        { 
            $tagLevels = explode("|",$params['fname']);
            $fname = $tagLevels[0];
            $multipletaglevels = 0; 
            $tagLevelsData=null;
        }
         if(isset($params['multipletaglevels'])) { 
            $multipletaglevels = 1; 
         }else { 
            $multipletaglevels = 0; 
         }
         
        $FSTagClass = new Core_Api_FSTagClass();
        $err = $FSTagClass->createFSTag($tagTitle,$visibililyID,$fname,$companyIDArray,$addByUser,$tagName,$tagSource,0,$hideFromTechs,$canNonGPMEdit,$canGPMEdit,$canTechEdit,
                $multipletaglevels,$tagLevelsData);
        //end 931
        //End 13912
        Core_TimeStamp::createTimeStamp("", $addByUser, "Add FS tag: ".$tagName." for client: ".$companyIDArray , "", "", "", "", "", "");
        
        if(count($err)>0)
        {
            $this->_helper->json(array("success" => FALSE,"err"=>$err[0]));
        }
        else
        {
            $this->_helper->json(array("success" => TRUE, "err"=>""));
        }
        
    }
    public function searchtagAction()
    {
        $request = $this->getRequest();
        $params = $request->getParams();
        $size = (int) ($request->getParam('size', 100));
        $size = ( $size <= 10 ) ? 10 : ( ( $size >= 100 ) ? 100 : $size );
        $page = ($request->getParam('page', 1)) + 0;
        $page = ( $page < 1 ) ? 1 : $page;
        $offset = ($page * $size) - $size;
        
        $visibility = $params['SystemVisibility'];
        $companyID = $params['client'];
        $addByUsername = $params['adminusername'];
        $tagTitleLike = $params['tagtitle'];
        $activity = $params['activity'];
        $sortfield = $params['sortfield'];
        $sortdir = $params['sortdir'];
        //13912    
        $extFilters = array();
        if(($params['techHide'])!="") { 
            $extFilters['HideFromTechs']=$params['techHide'];        
        }
        if(($params['canNonGPMEdit'])!="") { 
            
            $extFilters['CanNonGPMEdit']= $params['canNonGPMEdit'];   
        }
        if(($params['canGPMEdit'])!="") { 
            $extFilters['CanGPMEdit']= $params['canGPMEdit'];
        }
        if(($params['canTechEdit'])!="") { 
            $extFilters['CanTechEdit']= $params['canTechEdit'];
        }    
        if(($params['multipletaglevels'])!="") { 
            $extFilters['HasMultiTagLevels']= $params['multipletaglevels'];
        }  
        $sort= $sortfield .' '.$sortdir;
        $FSTagClass = new Core_Api_FSTagClass();
        $total = $FSTagClass->getTagSearchResult($visibility, $companyID, $tagTitleLike, $addByUsername, $activity,1,$size,0,'',$extFilters);     

        $TagSearchResultlist = $FSTagClass->getTagSearchResult($visibility, $companyID, $tagTitleLike, $addByUsername, $activity,0,$size,$offset,$sort,$extFilters);        
        //End 13912  
        if ($total > 0) {
            $paginator = new Zend_Paginator(new Core_Paginator_Sliced_Array($TagSearchResultlist, $total));
            $paginator->setCurrentPageNumber($page);
            $paginator->setItemCountPerPage($size);
            $this->view->paginator = $paginator;
        }
        $this->view->sortfield = $params["sortfield"];
        $this->view->sortdir = $params["sortdir"];
        $this->view->TagSearchResultlist = $TagSearchResultlist;
        //$errorMessages->clearMessages();
    }
    
    public function edittagAction()
    {
        $request = $this->getRequest();
        $params = $request->getParams();
        $FSTagClass = new Core_Api_FSTagClass();
        
        if(!isset($params['tagtitle']))
        {
        $fsTagID = $params['fsTagID'];
        $sortdir = $params['sortdir'];
        $sortfield = $params['sortfield'];
            
            $FSTagInfo = $FSTagClass->getFSTagInfo($fsTagID);
            $FSTagCompanyList = $FSTagClass->getFSTagCompanyList($fsTagID);
        $this->view->sortdir = $sortdir;
        $this->view->sortfield = $sortfield;
    }
        else
        {
            $tagtitle=$params['tagtitle'];
            $tagfrom=$params['tagfrom'];
            
            $FSTagInfo = $FSTagClass->getFSTagInfo_byTitle($tagtitle);
            $FSTagCompanyList = $FSTagClass->getFSTagCompanyList($FSTagInfo['id']);
            $this->view->tagfrom = $tagfrom;
        }
        $this->view->FSTagInfo = $FSTagInfo;
        $this->view->FSTagCompanyList = $FSTagCompanyList;
    }
    
    function updatefstagAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $request = $this->getRequest();
        $params = $request->getParams();
        
        $tagTitle = $params['tagtitle'];
        $visibililyID = $params['cbxSystemVisibility'];
        $fname = $params['fname'];
        $companyIDArray = $params['Client'];
        //$Active = $params['Active'];//13994
        $fsTagID = $params['fsTagID'];
        
        $tagName = ($params['TagName'] == "")?preg_replace( '/\s+/', '', $tagTitle):$params['TagName'];
        $tagSource = ($params['TagSource'] == "")?"":$params['TagSource'];
        $addByUser = $_SESSION['UserName'];
        $companyIDlist = "";
        if(!empty($companyIDArray))
        {
            foreach($companyIDArray as $companyIDlist)
            {
                $companyIDlist = $companyIDlist.$companyIDlist.", ";
            }
        }
        //13994
        if(isset($params['Active'])) { 
            
            $Active = 1;    
                
        }else
        {
            $Active = 0;  
        }
            
        
        //13912
        $hideFromTechs = $params['techHide'];                
         if(isset($params['canNonGPMEdit'])) { 
            $canNonGPMEdit = 1; 
         }else { 
            $canNonGPMEdit = 0; 
         }
         if(isset($params['canGPMEdit'])) { 
            $canGPMEdit = 1; 
         }else { 
            $canGPMEdit = 0; 
         }
         if(isset($params['canTechEdit'])) { 
            $canTechEdit = 1; 
         }else { 
            $canTechEdit = 0; 
         }
          //End 13912
        //931
        if(isset($params['multipletaglevels']))
        { 
            $multipletaglevels = 1; 
            $fname="";
            $tagLevels = explode("|",$params['fname']);
            $i=1;
            foreach($tagLevels as $tag)
            {
                if($i<=$params['hidNumberItem'])
                {
                    $tagLevelsData[]=array('id'=>$params["TagArtID_$i"],'Title'=>$params["tagTitleLevel_$i"],'TagArt'=>$tag,'DisplayOrder'=>$i);
                }
                $i++;
            }
        }
        else
        { 
            $multipletaglevels = 0; 
            $tagLevelsData=null;
        }
         if(isset($params['multipletaglevels'])) { 
            $multipletaglevels = 1; 
         }else { 
            $multipletaglevels = 0; 
         }

        $FSTagClass = new Core_Api_FSTagClass();
        //            $err = $FSTagClass->createFSTag($tagTitle,$visibililyID,$fname,        $companyIDArray,$addByUser,$tagName,$tagSource,0,$hideFromTechs,$canNonGPMEdit,$canGPMEdit,$canTechEdit,$multipletaglevels,$tagLevelsData);
        $err = $FSTagClass->updateFSTag_ByID($fsTagID,$tagTitle,$visibililyID,$fname,$Active,$companyIDArray,$hideFromTechs,$canNonGPMEdit,$canGPMEdit,$canTechEdit,$multipletaglevels,$tagLevelsData);
        //end 931
        $FSTagClass = new Core_Api_FSTagClass();
            
        
       
        Core_TimeStamp::createTimeStamp("", $addByUser, "Update FS tag: ".$tagName." for client: ".$companyIDlist, "", "", "", "", "", "");
        
        if(count($err)>0)
        {
            $this->_helper->json(array("success" => FALSE,"err"=>$err[0]));
        }
        else
        {
            $this->_helper->json(array("success" => TRUE, "err"=>""));
        }
        
    }
    
    function isduplicatetagAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $request = $this->getRequest();
        $params = $request->getParams();
        $tagtitle = $params['tagtitle'];
        $FSTagClass = new Core_Api_FSTagClass();

        $isDuplicateTag = $FSTagClass->isDuplicate_Title($tagtitle);
        $this->_helper->json(array("success" => TRUE, "duplicate" => $isDuplicateTag));
    }
    
    function isduplicatetagnameAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $request = $this->getRequest();
        $params = $request->getParams();
        $TagName = $params['TagName'];
        $TagTitle = $params['TagTitle'];
        $TagSource = $params['TagSource'];
        
        $FSTagClass = new Core_Api_FSTagClass();

        $isDuplicateTag = $FSTagClass->isDuplicate_Name($TagName);
        if(!$isDuplicateTag)
        {
            $FSTagClass->createFSTag_FromExistingTag($TagName,$TagTitle,$TagSource,$this->_login);
            $isDuplicateTag = true;
        }
        $this->_helper->json(array("success" => TRUE, "duplicate" => $isDuplicateTag));
    }
    //end 624
}