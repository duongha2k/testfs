<?php
class Admindashboard_ProjectController extends Core_Controller_Action
{
    public function init()
    {
        parent::init();

        $request = $this->getRequest();

        $company = $request->getParam('company',NULL);

        $this->view->company = !empty($company)?$company:NULL;
        $this->view->showDeduct = $company == 'HXC' || $company == 'HXWN' || $company == 'HXXO' || $company == 'HXST' || $company == 'BW' || $company == 'suss' || $company == 'CBD' || $company == 'RHOM' || $company == 'TECF';
    }


    
    public function listAction()
    {
        $request = $this->getRequest();
        
        $size    = ($request->getParam('size', 10)) + 0;
        $size    = ( $size <= 10 ) ? 10 : ( ( $size >= 100 ) ? 100 : $size );
        $page    = ($request->getParam('page', 1)) + 0;
        $page    = ( $page < 1 ) ? 1 : $page;
        
        $sortBy = $request->getParam('sortBy', 'Project_ID');
        $sortDir = $request->getParam('sortDir', 'asc');
        
        $this->view->sortBy = $sortBy;
        $this->view->sortDir = $sortDir;
        
        //print "$sortBy} {$sortDesc}";  exit;
        /*$sortStr = $this->_assembleSortStr(array(
            's1' => $request->getParam('sort1', NULL), 'd1' => $request->getParam('dir1', NULL)
        ));*/
        $params = $request->getParams();
        
        $params['companyId'] = $this->view->company;
        
        $companyId = $this->view->company;

        $search = new Core_Api_AdminClass;

        $countRows = 0;
        
        
            // Save workorders into CSV file
        if ( $request->getParam('download', false) === 'csv' ) {
            try {
                ob_start();
                
                $projects = $search->getProjectsList(
                    $this->_login,
                    $this->_password,
                    $params,
					'Project_ID ASC'
                );
				//798
                //$res = array( "Project_ID","Project_Name","Headline","Client_Company_Name");
				$res = array( "Project_ID","Project_Name","Headline","Company Name","Company Code","Service Type","AM","CSD","Legal Entity");
				
                $this->putcsv($res);
                
                if ($projects->success )
                foreach ($projects->data as $project) {
                    $row = array(
                        $project->Project_ID,
                        $project->Project_Name,
                        $project->Headline, 
                        //$project->Client_Name 
						$project->CompanyName, 
						$project->CompanyCode,
						$project->ServiceTypeName,
						$project->AMName,
						$project->CSDName,
						$project->EntityTypeName
                    );
                    $this->putcsv($row);
                }
                $csv = ob_get_contents();
                ob_end_clean();
                
                return $this->_outputCsv($csv);
            } catch (Exception $e) {
                print 'error occurred';
                return false;
            }
        }
        
        //echo
        $projects = $search->getProjectsList(
                $this->_login,
                $this->_password,
                $params,
                (!empty($sortBy) && !empty($sortDir) ) ? "{$sortBy} {$sortDir}" : null,
                ($page-1)*$size,
                $size,
                $countRows
        );



        if ( $projects->success ) {
            $paginator = new Zend_Paginator(new Core_Paginator_Sliced_Array($projects->data, $countRows));
            $paginator->setCurrentPageNumber($page);
            $this->view->paginator = $paginator;
        }
    
        //$this->view->projects = $projects;
        $this->view->success    = $projects->success;
        $this->view->errors     = $projects->errors;
        
    }
    
    public function listPricingRuleAction()
    {
        $request = $this->getRequest();
        
        $size    = ($request->getParam('size', 25)) + 0;
        $size    = ( $size <= 10 ) ? 10 : ( ( $size >= 100 ) ? 100 : $size );
        $page    = ($request->getParam('page', 1)) + 0;
        $page    = ( $page < 1 ) ? 1 : $page;
        
        $sortBy = $request->getParam('sortBy', 'Project_ID');
        $sortDir = $request->getParam('sortDir', 'asc');
        
        $this->view->sortBy = $sortBy;
        $this->view->sortDir = $sortDir;
        
        $params = $request->getParams();
        
        $params['companyId'] = $this->view->company;
	$params['assignedPricingRule'] = true;
        
        $companyId = $this->view->company;

        $search = new Core_Api_AdminClass;

        $countRows = 0;
        
        $rules = $search->getPricingRules($this->_login,
            $this->_password);

	$pricingRules = array();


	foreach ($rules->data as $rule) {
		$pricingRules[$rule->PricingRuleId] = $rule->Description;
	}

            // Save workorders into CSV file
        if ( $request->getParam('download', false) === 'csv' ) {
            try {
                ob_start();
                
                $projects = $search->getProjectsList(
                    $this->_login,
                    $this->_password,
                    $params,
					'Project_ID ASC'
                );

                $res = array( "Project_ID","Project_Company_ID","Project_Name","PricingRuleID","Description");
                $this->putcsv($res);
                
                if ($projects->success )
                foreach ($projects->data as $project) {
                    $row = array(
                        $project->Project_ID,
                        $project->Project_Company_ID,
                        $project->Project_Name,
                        $project->PricingRuleID,
			$pricingRules[$project->PricingRuleID]
                    );
                    $this->putcsv($row);
                }
                $csv = ob_get_contents();
                ob_end_clean();
                
                return $this->_outputCsv($csv);
            } catch (Exception $e) {
                print 'error occurred';
                return false;
            }
        }
        
        //getProjectsList($user, $pass, $filters, $sort, $offset = 0, $limit = 0, &$count)
        $projects = $search->getProjectsList(
                $this->_login,
                $this->_password,
                $params,
                (!empty($sortBy) && !empty($sortDir) ) ? "{$sortBy} {$sortDir}" : null,
                ($page-1)*$size,
                $size,
                $countRows
        );

        if ( $projects->success ) {
            $paginator = new Zend_Paginator(new Core_Paginator_Sliced_Array($projects->data, $countRows));
            $paginator->setCurrentPageNumber($page);
            $this->view->paginator = $paginator;
        }

    
	$this->view->pricingRules = $pricingRules;
        //$this->view->projects = $projects;
        $this->view->success    = $projects->success;
        $this->view->errors     = $projects->errors;
        
    }
    
    private function putcsv($row, $fd=',', $quot='"')
    {
       $str='';
       foreach ($row as $cell)
       {
          $cell = str_replace($quot, $quot.$quot, $cell);
          if (strchr($cell, $fd) !== FALSE || strchr($cell, $quot) !== FALSE || strchr($cell, "\n") !== FALSE)
          {
             $str .= $quot.$cell.$quot.$fd;
          }
          else
          {
             $str .= $cell.$fd;
          }
       }
       echo substr($str, 0, -1)."\n";
       
       return strlen($str);
    }
    
    
    private function _outputCsv($csvText)
    {
        $this->_helper->viewRenderer->setNoRender();
        $response = $this->getResponse();
        $response->clearHeaders();
        $response->clearBody();
        $response->setHeader('Pragma', 'public')
                ->setHeader('Expires', '0')
                ->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0')
                ->setHeader('Cache-Control', 'private')
                ->setHeader('Content-Type', 'text/csv')
                ->setHeader('Content-Disposition', 'attachment; filename='.$this->_company.'-'.ucfirst('projects').'-'.date('mdY').'.csv', true)
                ->setHeader('Content-Transfer-Encoding', 'binary');
        $response->sendResponse();

        //Flush buffers
        ob_end_flush();
        flush();
        
        echo $csvText;

        exit;
    }
    
    
    public function doUpdateAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        
        $params    = $this->getRequest()->getParams();
        $prjClass  = new Core_Api_AdminProjectClass();
        $return    = array('success'=>0);
        $companyId = $params['Project_Company_ID'];

        $prj = $prjClass->getProjects(
            $this->_login,
            $this->_password,
            $params['Project_ID']
        );
        if ($prj->success && !empty($prj->data[0]->Project_ID)) {
            $prj = $prj->data[0];
            $className = get_class($prj);

            $currentProject = $prj->Project_ID;

            foreach ($params as $key => $value) {
                if(property_exists($className, $key)) $prj->$key = $value;
            }
            $apiResponse = $prjClass->updateProject(
                $this->_login,
                $this->_password,
                $prj->Project_ID,
                $prj
             );
            if ( empty($apiResponse->errors) ) {
                Core_Cache_Manager::factory()->clean('all', array('prj'));
                $return['success'] = 1;
                $return['project_id'] = $prj->Project_ID;
            } else {
                $return['success'] = 0;
                $return['errors'] = array();
                foreach ($apiResponse->errors as $e) {
                    $return['errors'][]=$e->message;
                }
            }
        } else {
            $return['success'] = 0;
            $return['errors'] = array('There is no such Project');
            foreach ($prj->errors as $e) {
                $return['errors'][]=$e->message;
            }
        }
        $response = $this->getResponse();
        $response->clearAllHeaders()->clearBody();
        $return = json_encode($return);

        $response->setHeader('Content-type', 'application/json');
        $response->setHeader('Content-Length', strlen($return), true)
                 ->setBody($return);
    }
    
    
    public function pricingRuleAction()
    {
        $request   = $this->getRequest();
        $companyId = $this->view->company;
        $prj_id    = $request->getParam('project_id', null);
        $search    = new Core_Api_AdminProjectClass();

        $prj = $search->getProjects(
            $this->_login,
            $this->_password,
            $prj_id
        );
        $this->view->project = $prj->data[0];
        $this->view->success = $prj->success;
        $this->view->errors  = $prj->errors;

        
        $admin = new Core_Api_AdminClass();
        $pricingRules = $admin->getPricingRulesDescriptionOrder($this->_login,
            $this->_password);
//print_r($pricingRules->data);
        $this->view->pricingRules = $pricingRules->data;
        
    }
    
}

