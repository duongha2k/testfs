<?php
class Admindashboard_JsController extends Core_Controller_Action
{
    public function init()
    {
        parent::init();
        $this->_helper->viewRenderer->setNoRender();
    }
    public function getTechInfoAction()
    {
        $response = $this->getResponse();
        $response->setHeader('Content-Type', 'text/javascript', true);

        $request = $this->getRequest();
        $tech    = $request->getParam('id', NULL);
        
        if(empty ($tech) || !is_numeric($tech)){
            $jsonContext = 'techInfo = null;';
        } else {
            $search  = new Core_Api_Class;
            $techInfo = $search->techLookup($this->_login, $this->_password, $tech);     
            if($techInfo->success) {
                $jsonContext = 'techInfo = '.json_encode((array)$techInfo->data[0]).';';
            } else {
                $jsonContext = 'techInfo = null;';
            }
        }
        $response->setHeader('Content-Length', strlen($jsonContext), true)
                 ->setBody($jsonContext);
    }

    public function prepareWoAction()
    {
        require_once 'Core/Api/TechClass.php';

        $response = $this->getResponse();
        $request = $this->getRequest();
        $win = $request->getParam('win', NULL);
        $key = $request->getParam('key', NULL);
        $get = $request->getParam('get', 0) + 0;
        $output = '';
        $search = new Core_Api_TechClass();
        
        if ( NULL === $win || NULL === $key ) {
            if ( $get ) {
                $output = '{}';
            } else {
                $output = 'var _prepared = false;';
            }
        } else {
            $cache = $this->getInvokeArg('bootstrap')->getResource('Cache');

            if ( FALSE === ($wo = $cache->load($key)) ) {
                $paths = $this->getFrontController()->getControllerDirectory();
                if ( !empty($paths['dashboard']) && file_exists($paths['dashboard'].'/IndexController.php') ) {
                    require_once $paths['dashboard'].'/IndexController.php';
                    $fields = Dashboard_IndexController::FIELDS;
                } else {
                    $fields = NULL;
                }

                $wo = $search->getWorkOrder($this->_login, $this->_password, $win, null);
                $cache->save($wo, $key, array(), 120);
            }
            if ( FALSE === ($tech = $cache->load($key.'_tech')) ) {
                $tech = new API_Tech();
                $id = ( !empty($_SESSION['FLSID']) ) ? $_SESSION['FLSID'] : ( ( !empty($_SESSION['TechID']) ) ? $_SESSION['TechID'] : NULL );
                if ( $id ) {
                    $tech->lookupID($id);
                    $cache->save($tech, $key.'_tech', array(), 120);
                }
            }

            if ( FALSE === ($project = $cache->load($key.'_project')) ) {
                if($wo->success) {
                    $prj_id = $wo->data[0]->Project_ID;
                    if ($prj_id){
                        $project = $search->getProjects($this->_login,$this->_password,array($prj_id),'');
                        $cache->save($project, $key.'_project', array(), 120);
                    }
                }
            }

            if ( $get ) {
                if ( $wo->success ) {
                    $wo->data[0]->Status = ucwords(strtolower($wo->data[0]->Status));
                    $output = json_encode((array)$wo->data[0]);
                } else {
                    $output = '{}';
                }
            } else {
                $output = 'var _prepared = true;';
            }
        }

        $response
            ->clearAllHeaders()
            ->clearBody();
        $response
            ->setHeader('Content-Type', 'text/javascript')
            ->setHeader('Content-Length', strlen($output))
            ->setBody($output);
    }

    public function projectInfoAction()
    {
        $id = $this->getRequest()->getParam('id',NULL);
        if ($id) {
            $search = new Core_Api_Class;
            $projectInfo = $search->getProjects($this->_login,$this->_password,array((int)$id));
            if ($projectInfo->success) {
                $projectInfo = $projectInfo->data[0];
            }else{
                $projectInfo = null;
            }
        }else{
            $projectInfo = null;
        }

        //filter Project Info
        $return = array();
        $rename_array = array(
            'Project_ID' => 'Project_ID',
            'Project_Name' => 'Project_Name',
            'Project_Manager' => 'ProjectManagerName',
            'Project_Manager_Email' => 'ProjectManagerEmail',
            'Project_Manager_Phone' => 'ProjectManagerPhone',
            'Resource_Coordinator' => 'ResourceCoordinatorName',
            'Resource_Coord_Email' => 'ResourceCoordinatorEmail',
            'Resource_Coord_Phone' => 'ResourceCoordinatorPhone',
            'Qty_Devices'=>'Qty_Devices',
            'Amount_Per'=>'Amount_Per',
            'Description'=>'Description',
            'Requirements'=>'Requirements',
            'Tools'=>'Tools',
            'SpecialInstructions'=>'SpecialInstructions',
            'AutoBlastOnPublish'=>'AutoBlastOnPublish',
            'RecruitmentFromEmail'=>'RecruitmentFromEmail',
            'AutoSMSOnPublish'=>'AutoSMSOnPublish',
            'WO_Category_ID'=>'WO_Category_ID',
            'General1'=>'General1',
            'General2'=>'General2',
            'General3'=>'General3',
            'General4'=>'General4',
            'General5'=>'General5',
            'General6'=>'General6',
            'General7'=>'General7',
            'General8'=>'General8',
            'General9'=>'General9',
            'General10'=>'General10',
            'Headline'=>'Headline',
            'CheckInOutEmail'=>'CheckInOutEmail',
            'CheckInOutName'=>'CheckInOutName',
            'CheckInOutNumber'=>'CheckInOutNumber',
            'EmergencyName'=>'EmergencyName',
            'EmergencyPhone'=>'EmergencyPhone',
            'EmergencyEmail'=>'EmergencyEmail',
            'TechnicalSupportName'=>'TechnicalSupportName',
            'TechnicalSupportPhone'=>'TechnicalSupportPhone',
            'TechnicalSupportEmail'=>'TechnicalSupportEmail',
            'Push2Tech'=>'Push2Tech',
            'Reminder24Hr'=>'Reminder24Hr',
            'Reminder1Hr'=>'Reminder1Hr',
            'ReminderAcceptance'=>'ReminderAcceptance',
            'ReminderNotMarkComplete'=>'ReminderNotMarkComplete',
            'P2TPreferredOnly'=>'P2TPreferredOnly',
            'isWorkOrdersFirstBidder'=>'isWorkOrdersFirstBidder',
            'MaximumAllowableDistance'=>'MaximumAllowableDistance',
            'MinimumSelfRating'=>'MinimumSelfRating',
            'ReminderIncomplete'=>'ReminderIncomplete',
            'CheckOutall'=>'CheckOutCall',
            'CheckInCall'=>'CheckInCall',
            'SignOffSheet_Required'=>'SignOffSheet_Required',
            'P2TNotificationEmail'=>'P2TNotificationEmail',
            'NotifyOnBid'=>'NotifyOnBid',
            'NotifyOnAssign'=>'NotifyOnAssign',
            'AssignmentNotificationEmail'=>'AssignmentNotificationEmail',
            'NotifyOnAutoAssign'=>'NotifyOnAutoAssign',
            'FixedBid'=>'FixedBid',
            'PcntDeduct'=>'PcntDeduct'
        );
        if ($projectInfo) {
            foreach ($rename_array as $key=>$value) {
                $return[$value] = $projectInfo->$key;
            }
        }
        
        if ($projectInfo->ReminderAll!=0) {
            //7 checkboxes
            $rename_array = array(
                        'Reminder24Hr'=>'Reminder24Hr',
                        'Reminder1Hr'=>'Reminder1Hr',
                        'ReminderAcceptance'=>'ReminderAcceptance',
                        'ReminderNotMarkComplete'=>'ReminderNotMarkComplete',
                        'ReminderIncomplete'=>'ReminderIncomplete',
                        'CheckOutall'=>'CheckOutCall',
                        'CheckInCall'=>'CheckInCall'
                    );
            foreach ($rename_array as $k->$v) {
                $return[$v] = true;
            }

        }
        //$return = $projectInfo;


        $response = $this->getResponse();
        $response->clearAllHeaders()->clearBody();
        $return = json_encode($return);
        $response->setHeader('Content-type', 'application/json');
        $response->setHeader('Content-Length', strlen($return), true)
                 ->setBody($return);
    }

    public function projectInfoExAction()
    {
        $id = $this->getRequest()->getParam('id',NULL);
        $company = $this->getRequest()->getParam('v',NULL);
        
        if ($id) {
            $search = new Core_Api_Class;
            $projectInfo = $search->getProjectByID(array((int)$id));
            $projectInfo = $projectInfo[0];
        }else{
            $projectInfo = null;
        }
        //filter Project Info
        $return = array();
        $rename_array = array(
            'PcntDeduct'=>'PcntDeduct',
            'PcntDeductPercent'=>'PcntDeductPercent'
        );

        if ($projectInfo) {
            foreach ($rename_array as $key=>$value) {
                $return[$value] = $projectInfo[$key];
            }
        }
        $siteCount = Core_Site::getSiteCount($projectInfo['Project_Company_ID'], (int)$id);
		$return['SiteCount'] = empty($siteCount[$id]) ? 0 : $siteCount[$id];
        $response = $this->getResponse();
        $response->clearAllHeaders()->clearBody();
        $return = json_encode($return);
        $response->setHeader('Content-type', 'application/json');
        $response->setHeader('Content-Length', strlen($return), true)
                 ->setBody($return);
    }
    public function companyProjectsAction()
    {
        require_once 'Core/Cache/Manager.php';
        require_once 'Core/Api/Class.php';

        $request    = $this->getRequest();
        $response   = $this->getResponse();

        $company        = $request->getParam('company', NULL);
        $projectsState  = $request->getParam('pstate', 'all');

        $key    = Core_Cache_Manager::makeKey('all_projects', $company);
        $cache  = $this->getInvokeArg('bootstrap')->getResource('Cache');
        $search = new Core_Api_Class;

        if ( FALSE === ($projects = $cache->load($key)) ) {
            $projects = $search->getAllProjects($this->_login.'|pmContext='.$company, $this->_password);
            if ( $projects->success ) {
                $cache->save($projects, $key, array('projects'), LIFETIME_15MIN);
            }
        }

        $output = 'var projects = ';
        if ( $projects->success ) {

            if ( !empty($projects->data) ) {
                usort($projects->data, 'Core_Controller_Action::_cmpProjects');
            }

            $obj = array();
            switch ( strtolower(trim($projectsState)) ) {
                case 'all':
                    foreach ( $projects->data as &$project )
                        $obj[] = array('id' => 0 + $project->Project_ID, 'name' => $project->Project_Name);
                    break;
                case 'active':
                    foreach ( $projects->data as &$project )
                        if ( $project->Active )
                            $obj[] = array('id' => 0 + $project->Project_ID, 'name' => $project->Project_Name);
                    break;
                case 'notactive':
                    foreach ( $projects->data as &$project )
                        if ( !$project->Active )
                            $obj[] = array('id' => 0 + $project->Project_ID, 'name' => $project->Project_Name);
                    break;
            }

            if ( $obj ) {
                $output .= json_encode($obj);
            } else {
                $output .= '[];';
            }
        } else {
            $output .= '[];';
        }

        $response
            ->clearAllHeaders()
            ->clearBody();
        $response
            ->setHeader('Content-Type', 'text/javascript')
            ->setHeader('Content-Length', strlen($output))
            ->setBody($output);
    }

    public function statesAction()
    {
        require_once 'Core/Cache/Manager.php';
        require_once 'Core/Api/Class.php';

        $request    = $this->getRequest();
        $response   = $this->getResponse();

        $search     = new Core_Api_Class;

        $output = 'var states = ';
        $result = $search->getStates();
        if ( $result->success ) {
            $obj = array();
            foreach ( $result->data as $id => $name )
                $obj[] = array('id' => $id, 'name' => $name);

            if ( $obj ) {
                $output .= json_encode($obj);
            } else {
                $output .= '[];';
            }
        } else {
            $output .= '[];';
        }

        $response
            ->clearAllHeaders()
            ->clearBody();
        $response
            ->setHeader('Content-Type', 'text/javascript')
            ->setHeader('Content-Length', strlen($output))
            ->setBody($output);
    }

    public function getProjectsAction()
    {
        require_once 'Core/Cache/Manager.php';
        require_once 'Core/Api/AdminClass.php';

        $request    = $this->getRequest();
        $response   = $this->getResponse();

        $company        = $request->getParam('company', NULL);
        $projectsState  = $request->getParam('pstate', 'all');
        $fieldsList     = array('Project_ID','Project_Name','Active');

        $key    = Core_Cache_Manager::makeKey('projects_admin', $projectsState);
        $cache  = $this->getInvokeArg('bootstrap')->getResource('Cache');
        $search = new Core_Api_AdminClass;

        if ( FALSE === ($projects = $cache->load($key)) ) {
            $projects = $search->getProjects($this->_login,$this->_password,$fieldsList,$company,null,$projectsState,'Project_Name');
            if ( $projects->success ) $cache->save($projects, $key, array('projects'), LIFETIME_15MIN);
        }

        $output = 'var projects = ';
        if ( $projects->success ) {
            $obj = array();
            foreach ( $projects->data as &$project )
                $obj[] = array('id' => 0 + $project->Project_ID, 'name' => $project->Project_Name);

            $output .= ( $obj ) ? json_encode($obj).';' : '[];';
        } else {
            $output .= '[];';
        }

        $response
            ->clearAllHeaders()
            ->clearBody();
        $response
            ->setHeader('Content-Type', 'text/javascript')
            ->setHeader('Content-Length', strlen($output))
            ->setBody($output);
    }
    public function getCompaniesAction()
    {
        require_once 'Core/Cache/Manager.php';
        require_once 'Core/Api/AdminClass.php';

        $response   = $this->getResponse();

        $fieldsList = array('Company_ID','CompanyName');

        $key    = Core_Cache_Manager::makeKey('companies_admin', $this->_login);
        $cache  = $this->getInvokeArg('bootstrap')->getResource('Cache');
        $search = new Core_Api_AdminClass;

        if ( FALSE === ($companies = $cache->load($key)) ) {
            $companies = $search->getCompanies($this->_login,$this->_password,$fieldsList,'CompanyName','CompanyName');
            if ( $companies->success ) $cache->save($companies, $key, array('companies'), LIFETIME_15MIN);
        }

        $output = 'var companies = ';
        if ( $companies->success ) {
            $obj = array();
            foreach ( $companies->data as $company )
                $obj[] = array('id' => $company->Company_ID, 'name' => $company->CompanyName);

            $output .= ( $obj ) ? json_encode($obj).';' : '[];';
        } else {
            $output .= '[];';
        }

        $response
            ->clearAllHeaders()
            ->clearBody();
        $response
            ->setHeader('Content-Type', 'text/javascript')
            ->setHeader('Content-Length', strlen($output))
            ->setBody($output);
    }

    public function getCategoriesAction()
    {
        require_once 'Core/Cache/Manager.php';
        require_once 'Core/Api/AdminClass.php';

        $response   = $this->getResponse();

        $fieldsList = array('Category_ID','Category');
        
        $keyType = $this->getRequest()->getParam('keytype', 'id');
        
        $key    = Core_Cache_Manager::makeKey('categories_admin_' . $keyType, $this->_login);
        $cache  = $this->getInvokeArg('bootstrap')->getResource('Cache');
        $search = new Core_Api_AdminClass;
        
        

        if ( FALSE === ($categories = $cache->load($key)) ) {
            $categories = $search->getWOCategories($this->_login,$this->_password,$fieldsList);
            if ( $categories->success ) $cache->save($categories, $key, array('categories'), LIFETIME_15MIN);
        }

        $output = 'var categories = ';
        if ( $categories->success ) {
            //if ( !empty($companies->data) )
            //    usort($companies->data, 'Core_Controller_Action::_cmpProjects');

            $obj = array();
            if ($keyType == 'name') {
                foreach ( $categories->data as $category ) {
                    $obj[] = array('id' => $category->Category, 'name' => $category->Category);
                }
            } else {
                foreach ( $categories->data as $category ) {
                    $obj[] = array('id' => 0 + $category->Category_ID, 'name' => $category->Category);
                }
            }

            $output .= ( $obj ) ? json_encode($obj).';' : '[];';
        } else {
            $output .= '[];';
        }

        $response
            ->clearAllHeaders()
            ->clearBody();
        $response
            ->setHeader('Content-Type', 'text/javascript')
            ->setHeader('Content-Length', strlen($output))
            ->setBody($output);
    }
    public function getCalltypesAction()
    {
        require_once 'Core/Cache/Manager.php';
        require_once 'Core/Api/AdminClass.php';

        $response   = $this->getResponse();

        $key    = Core_Cache_Manager::makeKey('call_types_admin', $this->_login);
        $cache  = $this->getInvokeArg('bootstrap')->getResource('Cache');
        $search = new Core_Api_AdminClass;

        if ( FALSE === ($call_types = $cache->load($key)) ) {
            $call_types = $search->getCallTypes();
            if ( $call_types->success ) $cache->save($call_types, $key, array('call_types'), LIFETIME_15MIN);
        }

        $output = 'var call_types = ';
        if ( $call_types->success ) {
            $obj = array();
            foreach ( $call_types->data as $k=>$ctype )
                $obj[] = array('id' => 0+$k, 'name' => $ctype);
            $output .= ( $obj ) ? json_encode($obj).';' : '[];';
        } else {
            $output .= '[];';
        }

        $response
            ->clearAllHeaders()
            ->clearBody();
        $response
            ->setHeader('Content-Type', 'text/javascript')
            ->setHeader('Content-Length', strlen($output))
            ->setBody($output);
    }
}