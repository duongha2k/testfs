<?php
class Admindashboard_SitelistController extends Core_Controller_Action
{
    public function init()
    {
		try {
			parent::init();
		} catch (Core_Auth_Exception $e) {
			if ($this->getRequest()->getActionName() != 'state-info') throw $e;
		}
        $request = $this->getRequest();
        $auth = new Zend_Session_Namespace('Auth_User');
        $this->view->name = $auth->name;
        $updateForm = $this->getRequest()->getParam('UpdateForm', null);
        $company = $request->getParam('company',NULL);
		if (empty($company)) $company = $request->getParam('Company_ID', NULL);
		if (empty($company)) $company = $updateForm["Company_ID"];
		
        $this->view->company = !empty($company)?$company:$_SESSION['Company_ID'];
        $this->view->Project_ID = $request->getParam('Project_ID', NULL);
	}

	public function siteSearchAction() {
		$this->_helper->viewRenderer->setNoRender();
		$request = $this->getRequest();
		$api = new Core_Api_Class;
		$params = array(
			"AutoComplete" => $request->getParam('term', NULL),
			"Project_ID" => $request->getParam('project_id', NULL)
		);
        $results = $api->getSiteList(
                $this->_login.'|pmContext='.$this->view->company,
                $this->_password,
                $params,
                "SiteNumber ASC",
                0,
                50,
                $countRows
        );
		$matches = array();
		foreach ($results->data as $site) {
			$matches[] = array("label" => "{$site['SiteNumber']}, {$site['SiteName']}, {$site['Address']}, {$site['City']}, {$site['State']}, {$site['Zipcode']}", "value" => $site['SiteNumber']);  
    }
		$this->_helper->json($matches);
	}
	
    public function saveSiteAction()
    {
		$this->_helper->viewRenderer->setNoRender();
		
        $response = $this->getResponse();
        $response->setHeader('Content-Type', 'text/javascript', true);

        $request = $this->getRequest();
        $params = $request->getParams();

		$api = new Core_Api_Class;
		$result = $api->updateSite($this->_login.'|pmContext='.$this->view->company, $this->_password, $params);
		$result = $result->success ? $result->data[0] : false;

	$jsonContext = json_encode($result);

        $response->setHeader('Content-Length', strlen($jsonContext), true)
                 ->setBody($jsonContext);
    }

    //13433
    public function saveTechToSiteAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        
        $response = $this->getResponse();
        $response->setHeader('Content-Type', 'text/javascript', true);

        $request = $this->getRequest();
        $params = $request->getParams();
 
        //--- siteNumber, projectID
        $siteNumber = $params['SiteNumber'];
        $projectID = $params['Project_ID'];
        $techID = $params['Tech_ID'];
        
        //--- get site info
        $api = new Core_Api_Class;
        $result = $api->getSite($this->_login.'|pmContext='.$this->view->company, $this->_password,$siteNumber, $projectID);
        $siteInfo = $result->success ? $result->data[0] : false;
        
        //--- insert site if not exists
        if(empty($siteInfo)) 
        {
            $result = $api->updateSite($this->_login.'|pmContext='.$this->view->company, $this->_password, $params);
            
        }
        //--- attach tech to site
        $api_1 = new Core_Api_WosClass();
        $api_1->saveTechToSite($techID,$siteNumber,$projectID);
        
        //--- return
        $ret = true;  
        $jsonContext = json_encode($ret);
        $response->setHeader('Content-Length', strlen($jsonContext), true)
                 ->setBody($jsonContext);
    }

    
    public function getSiteAction()
    {
		$this->_helper->viewRenderer->setNoRender();

        $response = $this->getResponse();
        $response->setHeader('Content-Type', 'text/javascript', true);

        $request = $this->getRequest();
		$siteNumber = $request->getParam('SiteNumber', NULL);
		$Project_ID = $request->getParam('Project_ID', NULL);

		$api = new Core_Api_Class;
		$result = $api->getSite($this->_login.'|pmContext='.$this->view->company, $this->_password, $siteNumber, $Project_ID);
		$result = $result->success ? $result->data[0] : false;
                
        $Core_Api_WosClass = new Core_Api_WosClass;
        $result['TechsInSite'] = $Core_Api_WosClass->Exists_TechsInSite($siteNumber,$Project_ID);
        
	$jsonContext = json_encode($result);

        $response->setHeader('Content-Length', strlen($jsonContext), true)
                 ->setBody($jsonContext);
    }
	
    public function createAction()
    {
        $errorMessages = new Core_Form_ErrorHandler( 'createSitelist' );      
        $this->view->messages = $errorMessages->getMessages();
        $company_id = $this->getRequest()->getParam('Company_ID', null);
        $this->view->Company_ID = ( !empty($company_id) ) ? $company_id : $this->view->company;
        $errorMessages->clearMessages();

		$common = new Core_Api_CommonClass;
		// country list
		$countries = $common->getCountries();
        $this->view->countriesSuccess = $countries->success;
        if ($countries->success)
            $this->view->countries = $countries->data;

        // states list
        $states = $common->getStatesArray('US');
        $this->view->statesSuccess = $states->success;
        if ($states->success)
            $this->view->states = $states->data;
}

    public function doCreateAction()
    {
        $errorMessages = new Core_Form_ErrorHandler( 'createSite' );
        $request       = $this->getRequest();
        $createForm    = $request->getParam('CreateForm', null);

        if(empty($createForm)) $errorMessages->addMessage( 'Create Site List Form is empty' );
        $errorMessages->clearMessages();

        $api = new Core_Api_Class();
        $result = $api->updateSite($this->_login.'|pmContext='.$this->view->company, $this->_password, $createForm);

        if( !$result->success ) {
            foreach ( $result->errors as $error ) $errorMessages->addMessage($error->message);
        } else {
        }
        $err = $errorMessages->getMessages();
        if (empty($err))
            $this->_redirect('../clients/siteView.php?project_id='.$createForm['Project_ID'].'&v='.$createForm['Company_ID']);
		$this->_redirect($_SERVER['HTTP_REFERER']);
    }

    public function listAction()
    {
        $request = $this->getRequest();
        $params  = $request->getParams();
		$params["company"] = $this->view->company;

        $size    = ($request->getParam('size', 10)) + 0;
        $size    = ( $size <= 10 ) ? 10 : ( ( $size >= 100 ) ? 100 : $size );
        $page    = ($request->getParam('page', 1)) + 0;
        $page    = ( $page < 1 ) ? 1 : $page;

        $sortBy  = $request->getParam('sortBy', 'SiteNumber');
        $sortDir = $request->getParam('sortDir', 'asc');

        $this->view->sortBy = $sortBy;
        $this->view->sortDir = $sortDir;
        
        $search = new Core_Api_Class;

        $countRows = 0;
        $clients = $search->getSiteList(
                $this->_login.'|pmContext='.$this->view->company,
                $this->_password,
                $params,
                (!empty($sortBy) && !empty($sortDir) ) ? "{$sortBy} {$sortDir}" : null,
                ($page-1)*$size,
                $size,
                $countRows
        );
		
        if ( $clients->success ) {
            $paginator = new Zend_Paginator(new Core_Paginator_Sliced_Array($clients->data, $countRows));
            $paginator->setCurrentPageNumber($page);
            $this->view->paginator = $paginator;
        }
        $this->view->success    = $clients->success;
        $this->view->errors     = $clients->errors;
        $this->view->Project_ID = $request->getParam('Project_ID', NULL);
        $this->view->select_mode = $request->getParam('select_mode', NULL);
    }


    public function updateAction()
    {
        $request = $this->getRequest();
		$siteNumber = base64_decode($request->getParam('id', null));
		$project_id = $request->getParam('Project_ID', null);
		$api = new Core_Api_Class;
        $site      = $api->getSite($this->_login.'|pmContext='.$this->view->company, $this->_password, $siteNumber, $project_id);

        $errorMessages = new Core_Form_ErrorHandler( 'updateSite_'.$siteNumber . '_' . $project_id);

        //--- 13433: Count_TechsInSite
        $apiWos = new Core_Api_WosClass();
        $countTechsinSite= $apiWos->Count_TechsInSite($siteNumber,$project_id);
        $this->view->countTechsInSite  = $countTechsinSite;  
        $this->view->Project_ID  = $project_id;        
        //---
        $this->view->site  = $site->data[0];
        $this->view->success = $site->success !== false;

        $search  = new Core_Api_Class;
		$common = new Core_Api_CommonClass;

		// country list
		$countries = $common->getCountries();
        $this->view->countriesSuccess = $countries->success;
        if ($countries->success)
            $this->view->countries = $countries->data;

		$selectedCountry = $site->success ? $site->data[0]['Country'] : 'US';
        // states list
        $states = $common->getStatesArray($selectedCountry);
        $this->view->statesSuccess = $states->success;
        if ($states->success)
            $this->view->states = $states->data;

        //13433 
        
            
        $errors = Core_Api_Error::getInstance();
        $er = $errors->getErrorArray();
        foreach ( $er as $k=>$v ) $errorMessages->addMessage($error->message);

        $this->view->messages = $errorMessages->getMessages();
        $errorMessages->clearMessages();
    }

    public function doUpdateAction()
    {
        $updateForm = $this->getRequest()->getParam('UpdateForm', null);
		$id = empty($updateForm) ? NULL : $updateForm['SiteNumber'];
		$company = $updateForm["Company_ID"];
		$errorMessages = new Core_Form_ErrorHandler( 'updateSite_'.$id );
        $errorMessages->clearMessages();

        if(empty($updateForm) || empty($id)) $errorMessages->addMessage( 'Update Site List Form is empty' );
        $er = $errorMessages->getMessages();
        if(empty($er)) {
	        $api = new Core_Api_Class();
	        $result = $api->updateSite($this->_login.'|pmContext='.$company, $this->_password, $updateForm);
            
            $errors = Core_Api_Error::getInstance();
            if( !$result->success ) {
                foreach ( $result->errors as $error ) {
                    $errorMessages->addMessage($error->message);
                }
            } else {
            }
        }
        if (empty($err))
            $this->_redirect('../clients/siteView.php?project_id='.$updateForm['Project_ID'].'&v='.$updateForm['Company_ID']);
        $this->_redirect($_SERVER['HTTP_REFERER']);      
    }

	public function doDeleteAction()
    {
    	$request = $this->getRequest();
		$company = $request->getParam('v', null);
    	$siteNumber = base64_decode($request->getParam('id', null));
		$project_id = $request->getParam('project_id', null);

		$api = new Core_Api_Class();
		$result = $api->deleteSite($this->_login.'|pmContext='.$company, $this->_password, $siteNumber, $project_id);

        $this->_redirect($_SERVER['HTTP_REFERER']);      
    }
    //330
    public function listcustomerAction()
    {
        $request = $this->getRequest();
        $params  = $request->getParams();
	$params["company"] = $this->view->company;

        $size    = ($request->getParam('size', 10)) + 0;
        $size    = ( $size <= 10 ) ? 10 : ( ( $size >= 100 ) ? 100 : $size );
        $page    = ($request->getParam('page', 1)) + 0;
        $page    = ( $page < 1 ) ? 1 : $page;

        $sortBy  = $request->getParam('sortBy', 'SiteNumber');
        $sortDir = $request->getParam('sortDir', 'asc');

        $this->view->sortBy = $sortBy;
        $this->view->sortDir = $sortDir;
        
        $search = new Core_Api_Class;

        $countRows = 0;
        $clients = $search->getSiteListByProject(
                $this->view->Project_ID, 
                $sortBy." ".$sortDir, 
                ($page-1)*$size, 
                $size = 0, 
                $countRows);	
       $this->view->success = false; 
       $this->view->errors     = true;
        if ( count($clients) > 0 ) {
            $paginator = new Zend_Paginator(new Core_Paginator_Sliced_Array($clients, $countRows));
            $paginator->setCurrentPageNumber($page);
            $this->view->paginator = $paginator;
            $this->view->success = true;
            $this->view->errors     = false;
}
        
        $this->view->Project_ID = $request->getParam('Project_ID', NULL);
        $this->view->select_mode = $request->getParam('select_mode', NULL);
    }   
    
    public function getSiteCustomerAction()
    {
		$this->_helper->viewRenderer->setNoRender();

        $response = $this->getResponse();
        $response->setHeader('Content-Type', 'text/javascript', true);

        $request = $this->getRequest();
		$siteNumber = $request->getParam('SiteNumber', NULL);
		$Project_ID = $request->getParam('Project_ID', NULL);

		$api = new Core_Api_Class;
		$result = $api->getSiteWOCheck($siteNumber, $Project_ID);
		//$result = $result->success ? $result->data[0] : false;

	$jsonContext = json_encode($result);

        $response->setHeader('Content-Length', strlen($jsonContext), true)
                 ->setBody($jsonContext);
    }
}