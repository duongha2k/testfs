<?php
class Admindashboard_TechController extends Core_Controller_Action
{
    public function init()
    {
        parent::init();
    }

    public function createAction()
    {		
        $request = $this->getRequest();
		$params  = $request->getParams();

		$common = new Core_Api_CommonClass;
		$statesOptions = $common->getStatesArray('US');
		$countryOptions = $common->getCountries();
        $TechSource=$common->getTechSourceArray(); //14145
				
		$this->view->states = $statesOptions->data;
		$this->view->countries = $countryOptions->data;
        $this->view->TechSource=$TechSource[18];
       // print_r("test:");print_r($this->view->TechSource);die();
								
		$bankStates = $common->getStatesArray();
		$this->view->bankStates = $bankStates->data;
		
		$db = Core_Database::getInstance();
		$select = $db->select();
		$select->from(Core_Database::TABLE_COUNTRIES, array('id', 'Name'))->order('DisplayPriority', 'Name');
		
		$bankCountries = $db->fetchPairs($select);
		$this->view->bankCountries = $bankCountries;
		
		$select = $db->select();
		$select->from(Core_Database::TABLE_TECH_DEACTIVATION_REASONS, array('id', 'Reason'));
		$this->view->techDeactivationReason = $db->fetchPairs($select);
		
		$isoOptions = $common->getISOsArray();
		$this->view->isos = $isoOptions->data;
		
		$this->view->create = true;
		$this->render('update');
	}

    public function updateAction()
    {
        $request = $this->getRequest();
		$params  = $request->getParams();

		$common = new Core_Api_CommonClass;
        //--- tech. info        
        $techId = $params['techID'];
        $api = new Core_Api_TechClass();
        $techInfo = $api->getProperties($techId);
        $techCountryCode = $techInfo['Country'];
        if(empty($techCountryCode)) $techCountryCode='US';                    
        //---		
		$countryOptions = $common->getCountries();
        $statesOptions = $common->getStatesArray($techCountryCode);//US
		
		$this->view->techID = $params['techID'];
		
		$this->view->states = $statesOptions->data;
		$this->view->countries = $countryOptions->data;

		$bankStates = $common->getStatesArray();
		$this->view->bankStates = $bankStates->data;
		
		$db = Core_Database::getInstance();
		$select = $db->select();
		$select->from(Core_Database::TABLE_COUNTRIES, array('id', 'Name'))->order('DisplayPriority', 'Name');
		
		$bankCountries = $db->fetchPairs($select);
		$this->view->bankCountries = $bankCountries;
		
		$select = $db->select();
		$select->from(Core_Database::TABLE_TECH_DEACTIVATION_REASONS, array('id', 'Reason'));
		$this->view->techDeactivationReason = $db->fetchPairs($select);
		
		$isoOptions = $common->getISOsArray();
		$this->view->isos = $isoOptions->data;

		$certMapping = new Core_TechCertifications();
		$this->view->certMapping = $certMapping->getMapping();
                $core = new Core_TechLanguages();
                $this->view->languegaSkill = $core->getTechLanguages($params['techID']);
        //--- 13630
        $core = new Core_Tech();
        $core->Reset_AdjustableBedExpert_Tag($params['techID']);
    }
	
	public function getW9InfoAction() {
        $id = $this->getRequest()->getParam('id', null);
        $auth = new Zend_Session_Namespace('Auth_W9');
		$pw = $auth->password;
		$w9Info = null;
		try {
			if (!empty($pw) && $w9Info = Core_Tech::getW9Info($id, $pw)){
				$success = "success";
			}else{
				$success = "error";
			}
		}catch(Exception $e){
//			print_r($e);
		}
		$this->_helper->json(array('success'=>$success, 'w9Info' => $w9Info));	
	}
	
	public function w9PrintAction() {
        $request = $this->getRequest();
		$params  = $request->getParams();

		$this->view->techID = $params['techID'];
		
        $auth = new Zend_Session_Namespace('Auth_W9');
//		var_dump($auth->password);die();
		$this->view->askPassword = empty($auth->password);
		
		$common = new Core_Api_CommonClass;
		$statesOptions = $common->getStatesArray('US');
		
		$statesHtml = "<option value=\"\">Select State</option>";
		foreach ($statesOptions->data as $code => $name) {
				$statesHtml .= "<option value=\"$code\">$name</option>";
		}
		$this->view->statesHtml = $statesHtml;
	}
	
	public function w9AuthenticateAction() {
        $auth = new Zend_Session_Namespace('Auth_W9');
        $pw = $this->getRequest()->getParam('pw', null);
        $auth = new Zend_Session_Namespace('Auth_W9');
		if (!Core_Tech::W9Authenticate($pw)) $this->_helper->json(array('success'=>false));
		$auth->password = $pw;

		$this->_helper->json(array('success'=>true));	
	}
	public function getphotoauditAction() {

        $request = $this->getRequest();
        $params = $request->getParams();
        $size = 18;
        $page = ($request->getParam('page', 1)) + 0;
        $page = ( $page < 1 ) ? 1 : $page;
        $offset = ($page * $size) - $size;
         
        $uploadedDateGreater = $params['txtDateUpdatedForm'] != ""?
                date_format(date_create($params['txtDateUpdatedForm']),'Y-m-d'):"";
        $uploadedDateLess = $params['txtDateUpdatedTo'] != ""?
                date_format(date_create($params['txtDateUpdatedTo']),'Y-m-d'):"";

        $pending = $params['chkPending'] == ""?0:1;
        
        $Core_Api_TechClass = new Core_Api_TechClass();
  
        $countRows = $Core_Api_TechClass->searchPhotos_Total(
                $uploadedDateGreater,
                $uploadedDateLess,
                $pending);
        $Values = $Core_Api_TechClass->searchPhotos(
                $uploadedDateGreater,
                $uploadedDateLess,
                $pending,
                $offset,
                $size
                );
        
        if ($countRows > 0) {

            $paginator = new Zend_Paginator(new Core_Paginator_Sliced_Array($Values, $countRows));
            $paginator->setCurrentPageNumber($page);
            $paginator->setItemCountPerPage($size);
            $this->view->paginator = $paginator;
}
  
        $this->view->Values = $Values;
        $this->view->DateSearch = ($params['txtDateUpdatedForm'] == "" && 
               $params['txtDateUpdatedTo'] == "" )?"All" :
               (($params['txtDateUpdatedForm'] == "" && 
               $params['txtDateUpdatedTo'] != "")? ' -> '.$params['txtDateUpdatedTo']:
               (($params['txtDateUpdatedForm'] != "" && 
               $params['txtDateUpdatedTo'] == "" )?$params['txtDateUpdatedForm'].' -> '
               :$params['txtDateUpdatedForm'].' -> '.$params['txtDateUpdatedTo']));
        
    }
    public function doapproveallAction()
    {
        $request = $this->getRequest();
        $params = $request->getParams();
        $uploadedDateGreater = $params['txtDateUpdatedForm'] != ""?
                date_format(date_create($params['txtDateUpdatedForm']),'Y-m-d'):"";
        $uploadedDateLess = $params['txtDateUpdatedTo'] != ""?
                date_format(date_create($params['txtDateUpdatedTo']),'Y-m-d'):"";

        $pending = $params['chkPending'] == ""?0:1;
        $success = 'error';
        
        try {
             $Core_Api_TechClass =  new  Core_Api_TechClass;
             $Core_Api_TechClass->approveBatchPhoto($uploadedDateGreater,
                     $uploadedDateLess,
                     $pending);   
             $success = "success";
        } catch (Exception $e) {
            $success = "error";
        }

        $this->_helper->json(array('success' => $success));
    }
    //-----------------------------------------------
    function getauditphoneAction() {
        $params = $_POST;
        
        $request = $this->getRequest();
        $size = (int) ($request->getParam('size', 10));
        $size = ( $size <= 10 ) ? 10 : ( ( $size >= 100 ) ? 100 : $size );
        $page = ($request->getParam('page', 1)) + 0;
        $page = ( $page < 1 ) ? 1 : $page;
        $offset = ($page * $size) - $size;

        $datainput = Array("ReportedStartDate" => empty($params['ReportedStartDate'])?"":date_format(new DateTime($params['ReportedStartDate']),'Y-m-d')
            , "ReportedEndDate" => empty($params['ReportedEndDate'])?"":date_format(new DateTime($params['ReportedEndDate']),'Y-m-d')
            , "IncludeValidatedPhone" => $params['IncludeValidatedPhone'] == "on" ? 1 : 0
            , "PhoneNumber" => $params['PhoneNumber']
            , "PhoneType" => $params['PhoneType']
            , "TechID" => $params['TechID']
            , "TechLastName" => $params['TechLastName']
            , "TechFisrtName" => $params['TechFisrtName']
            , "ReportingClientId" => $params['ReportingClientId']
            , "ReportingUser" => $params['ReportingUser']
        );
        $columnsort = $params['columnsort'];
        $typesort = $params['typesort'];
        $sort = $columnsort . ' ' . $typesort;

        $Core_Api_TechClass = new Core_Api_TechClass;
        
        $countRows =count($Core_Api_TechClass->getPhoneAuditLog($datainput, $sort));
        
        $values = $Core_Api_TechClass->getPhoneAuditLog(
                $datainput, $sort, $size, $offset);

        if ($countRows > 0) {

            $paginator = new Zend_Paginator(new Core_Paginator_Sliced_Array($values, $countRows));
            $paginator->setCurrentPageNumber($page);
            $this->view->paginator = $paginator;
        }

        $this->view->values = $values;
        $this->view->columnsort = $columnsort;
        $this->view->typesort = $typesort;
        $this->view->page = $page;
    }
    /**
     *  
     */
    function updaterequitephoneAction() {
        $this->_helper->viewRenderer->setNoRender(true);
        $params = $this->getRequest()->getParams();
        $id = $params['id'];
        $phonetype = $params['phonetype'];
        $usertype = $params['usertype'];
        $Core_Api_TechClass = new Core_Api_TechClass;
        $aClient = null;
        $requite = 0;
        switch ($phonetype) {
            case 1:
                $aClient = $Core_Api_TechClass->setPrimaryPhoneValid($id,$usertype, $this->_login);
                if (!empty($aClient)) {
                    $requite = $aClient[0]['PrimaryPhoneStatus'];
                }
                break;
            case 2:
                $aClient = $Core_Api_TechClass->setSecondaryPhoneValid($id,$usertype, $this->_login);
                if (!empty($aClient)) {
                    $requite = $aClient[0]['SecondaryPhoneStatus'];
                }
                break;
        }

        $response = $this->getResponse();
        $response->clearHeaders()->clearBody();
        $response->setHeader('Content-Length', strlen($requite), true);
        $response->setBody($requite);
    }
    
    function blackboxDenyAction() {
        
        $this->_helper->viewRenderer->setNoRender(true);
        $params = $this->getRequest()->getParams();
        $return = array ("success" => 0);
        $CertID = $params['CertID'];
        $TechID = $params['TechID'];
        $apiTechClass = new Core_Api_TechClass();
        $result = $apiTechClass->denyBBCert($CertID,$TechID);
        if(!empty($result))
        {
            $return = array ("success" => $result);
}
        $response = $this->getResponse();
        $response->clearHeaders()->clearBody();
        $return = json_encode($return);

        $response->setHeader('Content-Length', strlen($return), true);
        $response->setBody($return);
    }
    function blackboxConfirmAction() {
        $this->_helper->viewRenderer->setNoRender(true);
        $params = $this->getRequest()->getParams();
        $return = array ("success" => 0);
        $CertID = $params['CertID'];
        $TechID = $params['TechID'];
        $apiTechClass = new Core_Api_TechClass();
        $result = $apiTechClass->confirmBBCert($CertID,$TechID);
        
        if(empty($result))
        {
            $return = array ("success" => 0);
        }
        else
        {
            $strDateBTTS =  date_format(new DateTime($result["date"]), "m/d/Y");
        
            $return = array ("success" => 1, "strDate"=>$strDateBTTS);
        }
        
        $response = $this->getResponse();
        $response->clearHeaders()->clearBody();
        $return = json_encode($return);

        $response->setHeader('Content-Length', strlen($return), true);
        $response->setBody($return);
    }
}
