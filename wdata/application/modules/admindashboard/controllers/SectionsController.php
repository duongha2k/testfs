<?php
class Admindashboard_SectionsController extends Core_Admin_Controller_Action
{

	/**
     * _wo
     *
     * @var API_Response
     * @access private
     */
    private $_wo;

    public function init()
    {
		parent::init();

        $benchmark = Core_Benchmark::getInstance('Start benchmark on sections pop-ups');
        //  This is constroller can be used for AJAX only!
        $request = $this->getRequest();
        if ( !$request->isXmlHttpRequest() ) {
            $this->_redirect('/');
        }

        //  Check authentication
        $auth = new Zend_Session_Namespace('Auth_User');
        $this->_login = $auth->login;
        $this->_password = $auth->password;
        session_write_close();

        if ( !$this->_login || !$this->_password ) {
            throw new Core_Auth_Exception('Authentication required');
        }
        //  Check authentication end block

        $request    = $this->getRequest();
        $win        = $request->getParam('win', false);
        $companyId  = $request->getParam('companyId', false);

        if ( !$win ) {
            $this->view->success = true;
            if ($request->getParam('action') == 'quickassigntool') return;
            $this->view->success = false;
            $this->view->errors  = "Sorry, but Work Order ID or Company ID are incorrect! Request skiped.";
            return;
        }

        $cacheKey = Core_Cache_Manager::makeKey($this->_login, 'sections', 'win', $win); //  1 key for whole controller!
        $cacheKeyCategories = Core_Cache_Manager::makeKey('wo_categiries', 'sections');
        $cache = $this->getInvokeArg('bootstrap')->getResource('Cache');

        $benchmark->point('Initialization complete');

        if ( $request->getParam('action') == 'final' || false === ($this->_wo = $cache->load($cacheKey)) ) {
            $benchmark->point('Load WO #'.$win.' from Caspio');
            $search = new Core_Api_AdminClass;

            $this->_wo = $search->getWorkOrder($this->_login, $this->_password, $win);
            
            $benchmark->point('WO #'.$win.' loaded');
            if ( $this->_wo->success ) {   //  save if operation success

                //  load Call Type
                $result = $search->getCallTypes();
                if ( $result->success && array_key_exists($this->_wo->data[0]->Type_ID, $result->data) ) {
                    $this->_wo->data[0]->Type_ID = $result->data[$this->_wo->data[0]->Type_ID];
                } else {
                    $this->_wo->data[0]->Type_ID = NULL;
                }

                //  load Category
                if ( $this->_wo->data[0]->WO_Category_ID ) {
                    if ( FALSE === ($categories = $cache->load($cacheKeyCategories)) ) {
                        $categories = $search->getWOCategories($this->_login.'|pmContext='.$companyId, $this->_password);
                        if ( $categories->success ) {
                            $cache->save($categories, $cacheKeyCategories, array('filter'), LIFETIME_15MIN);
                        }
                    }
                    if ( $categories->success ) {
                        foreach ( $categories->data as &$c ) {
                            if ( $c->Category_ID == $this->_wo->data[0]->WO_Category_ID ) {
                                $this->_wo->data[0]->WO_Category_ID = $c->Category;
                                break;
                            }
                        }
                    }
                }

                $cache->save($this->_wo, $cacheKey, array('wokr_orders'), LIFETIME_15MIN);
            }
        } else {
            $benchmark->point('WO #'.$win.' was loaded from cache');
        }

        $benchmark->point('View is rendering...');
//var_dump($this->_wo);exit;
        $this->view->wo         = $this->_wo->data[0];
        $this->view->success    = $this->_wo->success;
        $this->view->errors     = $this->_wo->errors;
        $this->view->win        = $win;
        $this->view->companyId  = $companyId;
        
    }


    public function documentsAction()
    {
        $this->view->core = new Core_Api_Class();
        $this->view->user_login = $this->_login;
        $this->view->user_password = $this->_password;
        $this->view->side = $this->getRequest()->getParam('side',null);
    }

    
    public function startTimeAction()
    {

    }

}

