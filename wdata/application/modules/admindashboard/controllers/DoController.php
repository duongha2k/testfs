<?php
class Admindashboard_DoController extends Core_Controller_Action
{
    public function init()
    {
        parent::init();
        //  This is constroller can be used for AJAX only!
        $request = $this->getRequest();
        if ( !$request->isXmlHttpRequest() ) {
            if ( $this->getRequest()->getParam('download', false) !== 'csv' ) {
                // ok
            } else if ( $this->getRequest()->getParam('download', false) !== 'file' ) {
                // ok
            } else {
                $this->_redirect('/');
            }
        }
    }

    public function fileDownloadAction()
    {
        $this->_helper->viewRenderer->setNoRender();

        $request  = $this->getRequest();

        $filename = $request->getParam('filename', false);
        if ( $filename !== false ) {
            $filename = base64_decode($filename);
        }

        $_f = preg_replace('/[^-.a-z0-9_\/]+/i', '_', $filename);
        $response = $this->getResponse();
        $response->clearHeaders();
        $response->clearBody();
        $response
            ->setHeader('Content-Type', 'application/octet-stream', true)
            ->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0')
            ->setHeader('Content-Disposition', 'attachment; filename='.substr($_f, strrpos($_f, '/')+1), true)
            ->setHeader('Last-Modified', date('D, d M Y H:i:s T'), true);

        $getter = new Core_Api_AdminClass;
        $side = $request->getParam('side',null);
        $file   = $getter->getFile($this->_login, $this->_password, $filename, $side);

        if ( $file === NULL || $file instanceof API_Response ) {
            $response->setHeader('Content-Length', 0, true)
                ->setBody('');
        } else {
            //$file = base64_decode($file);
            $response->setHeader('Content-Length', strlen($file), true)
                ->setBody($file);
        }
    }

    /**
     * Download WOS documents
     * @deprecated Use in dashboard module
     */
    public function fileWosDocumentsDownloadAction()
    {
        $this->fileDownloadS3('wo_docs');
    }
    
    
    /**
     * Download project documents
     * @deprecated Use in dashboard module
     */
    public function fileProjectDocumentsDownloadAction()
    {
        $this->fileDownloadS3('project');
    }
    
    
    /**
     * Dwonload file from s3
     * @param string $type
     * @throws Zend_Exception
     */
    private function fileDownloadS3($type)
    {
        if (empty($this->_login) || empty($this->_password) ) {
            // no authentification
            throw new Core_Auth_Exception();
        }
        // fix IE SSL download bug
        if (isset($_SERVER['HTTP_USER_AGENT']) &&  (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== false)
            && !empty($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) == 'on') {
                header('Location: http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI']);
                exit;
        }
        $this->_helper->viewRenderer->setNoRender();

        $showZero = false;
        
        $request  = $this->getRequest();

        $filename = $request->getParam('filename', false);
        $encType = $request->getParam('type', 'base');
        $showAction = $request->getParam('sact', 'down');

        $mimeType = 'application/octet-stream';
        if ( $filename !== false && $encType == 'base' ) {
            $filename = urldecode(base64_decode($filename));
        }
        if ($showAction == 'image') {
            $fileImageType = Core_Image::getImageTypeByFileName($filename);
            if (!empty($fileImageType) ) {
                $mimeType = Core_Image::getMimeByImgType($fileImageType);
            } else {
                $showZero = true;
            }
        }
        $response = $this->getResponse();
        $response->clearHeaders();
        $response->clearBody();
        
        $filename = ltrim($filename, '/');
        $file = null;
        
        switch ($type) {
            case 'wo_docs':
                $file = API_WorkOrder::getFile($filename);
                break;
            case 'project':
                $getter = new Core_Api_ProjectClass();
                $file = $getter->getFile($filename);
                break;
            default:
                throw new Zend_Exception('Wrong type');
                break;
        }

        if ( $file === NULL || $file instanceof API_Response || $showZero) {
            // not found
            if (0 && $encType == 'base') { /*disables caspio logic*/
                // check caspio for file
                $this->fileDownloadAction();
            } else {
                // not found
                $this->getResponse()->setHeader('HTTP/1.1','404 Not Found');
                $this->getResponse()->setHeader('Status','404 File not found');
                $this->getResponse()->setBody('<h1>404 File not found</h1>');
            }
        } else {
            if (isset($_SERVER['HTTP_USER_AGENT']) &&  (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== false) ) {
                $filename = rawurlencode($filename);
            }
            $response
            //->setHeader('Content-Type', $mimeType, true)
            ->setHeader("Content-Type", "content=".$mimeType."; charset=utf-8", true)
            ->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0')
            ->setHeader("Content-Disposition", "attachment; filename=\"$filename\"", true)
            ->setHeader('Last-Modified', date('D, d M Y H:i:s T'), true)
            ->setHeader('Content-Length', strlen($file), true);

            $response->setBody($file);
        }
    }
    
    
    public function progressBarAction()
    {
        $this->_helper->viewRenderer->setNoRender();

        $failJson = '{"percent":0,"label":"","done":true}';

        $response = $this->getResponse();
        $response->clearAllHeaders()->clearBody();
        $response->setHeader('Content-type', 'application/json');

        $key        = $this->getRequest()->getParam('key', null);
        $progress   = Core_ProgressBar::getInstance()->setUserLogin($this->_login);

        $out = trim($progress->read($key));
        if ( empty($out) )  return $response->setBody($failJson);
        else                return $response->setBody($out);
    }

}
