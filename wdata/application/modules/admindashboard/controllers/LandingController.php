<?php
class Admindashboard_LandingController extends Core_Controller_Action
{
    const FIELDS = 'TB_UNID,WO_ID,DateEntered,PO,ShowTechs,SourceByDate,ShortNotice,StartDate,StartTime,EndDate,EndTime,StartRange,Duration,Region,Type_ID,Project_ID,WO_Category_ID,Headline,Site_Contact_Name,SiteName,SiteNumber,Project_Name,Address,City,State,Zipcode,SitePhone,SiteFax,SiteEmail,Update_Requested,Update_Reason,Description,Requirements,Tools,SpecialInstructions,Lead,Assist,SignOffSheet_Required,ProjectManagerName,ProjectManagerPhone,ProjectManagerEmail,ResourceCoordinatorName,ResourceCoordinatorPhone,ResourceCoordinatorEmail,EmergencyName,EmergencyPhone,EmergencyEmail,TechnicalSupportName,TechnicalSupportPhone,TechnicalSupportEmail,CheckInOutName,CheckInOutNumber,CheckInOutEmail,Tech_ID,Tech_FName,Tech_LName,NoShow_Tech,BackOut_Tech,FLS_ID,BU1FLSID,BU1Info,Backup_FLS_ID,Ship_Contact_Info,PayMax,Amount_Per,Qty_Devices,Tech_Bid_Amount,WorkOrderReviewed,TechCheckedIn_24hrs,CheckedIn,Checkedby,PreCall_1,PreCall_2,CheckInNotes,StoreNotified,Notifiedby,DateNotified,NotifyNotes,TechMarkedComplete,Date_In,Time_In,Date_Out,Time_Out,ContactHD,HDPOC,TechMiles,TechComments,AttachedFile,FLS_OOS,Unexpected_Steps,Unexpected_Desc,AskedBy,AskedBy_Name,Paperwork_Received,Incomplete_Paperwork,MissingComments,Site_Complete,CallClosed,PartsProcessed,ClientComments,Work_Out_of_Scope,OutofScope_Amount,OutofScope_Reason,TripCharge,MileageReimbursement,MaterialsReimbursement,Additional_Pay_Amount,Penalty_Reason,AbortFee,AbortFeeAmount,Add_Pay_AuthBy,Add_Pay_Reason,calculatedTechHrs,baseTechPay,PayAmount,Approved,TechPaid,DatePaid,Deactivated,DeactivationCode,Deactivated_Reason,General1,General2,General3,General4,General5,General6,General7,General8,General9,General10,FS_InternalComments,Username,Invoiced,Company_Name,Company_ID,TechEmail,TechPhone,TechPhoneExt,Date_Assigned,lastModBy,lastModDate,approvedBy,DateApproved,DeactivatedDate,DeactivatedBy,Latitude,Longitude,PricingRan,WO_Category,DateIncomplete,ReminderAcceptance,Reminder24Hr,Reminder1Hr,CheckInCall,CheckOutCall,ReminderNotMarkComplete,ReminderIncomplete,ReminderAll,SMSBlast,MinutesRemainPublished,MinimumSelfRating,MaximumAllowableDistance,isProjectAutoAssign,isWorkOrdersFirstBidder,P2TPreferredOnly,P2TAlreadyAssigned,SATRecommended,SATPerformance,ReturnPartCount,NewPartCount,Qty_Applicants,Status,DateInvoiced,Work_Out_of_Scope,OutOfScope_Reason';

    public function init()
    {
        parent::init();
        ini_set('error_reporting', E_ALL | E_STRICT);
        ini_set('display_errors', 1);

        $request = $this->getRequest();

        //  This is constroller can be used for AJAX only!
        if ( !$this->getRequest()->isXmlHttpRequest() ) {
            $this->_redirect('/');
        }

        $this->_sortStr = $this->_assembleSortStr(array(
            's1' => $request->getParam('sort1', NULL), 'd1' => $request->getParam('dir1', NULL),
            's2' => $request->getParam('sort2', NULL), 'd2' => $request->getParam('dir2', NULL),
            's3' => $request->getParam('sort3', NULL), 'd3' => $request->getParam('dir3', NULL)
        ));

    }

    public function navigationAction()
    {
        require_once 'Core/Api/Class.php';

        $request    = $this->getRequest();
        $company    = $request->getParam('company', NULL);
        $tab        = $request->getParam('tab', NULL);

        $search = new Core_Api_Class;

        $cacheKeyFilters = Core_Cache_Manager::makeKey('filters', $company, $tab, $this->_login);
        $cache = $this->getInvokeArg('bootstrap')->getResource('Cache');

        if ( FALSE == ($projectList = $cache->load($cacheKeyFilters)) ) {
            $projectList = $search->getAllProjects($this->_login.'|pmContext='.$company, $this->_password);
            if ( $projectList->success ) {
                $cache->save($projectList, $cacheKeyFilters, array('filter'), LIFETIME_15MIN);
            }
        }
        if ( !empty($projectList->data) ) {
            usort($projectList->data, 'Core_Controller_Action::_cmpProjects');
        }

        $this->view->projects   = $projectList->data;
        $this->view->company    = $company;
    }

    public function publishedAction()
    {
        require_once 'Core/Api/Class.php';
        require_once 'API/WorkOrderFilter.php';

        $request        = $this->getRequest();

        $company        = $request->getParam('company', NULL);
        $projects       = $request->getParam('projects', NULL);
        $win            = $request->getParam('win', NULL);
        $state          = $request->getParam('state', NULL);
        $zip            = $request->getParam('zip', NULL);
        $startDate      = $request->getParam('start_date', NULL);
        $endDate        = $request->getParam('end_date', NULL);
        $tech           = $request->getParam('tech', NULL);
        $role           = $request->getParam('role', NULL);
        $region         = $request->getParam('region', NULL);
        $callType       = $request->getParam('call_type', NULL);
        $bid            = $request->getParam('bid', NULL);

        $size           = $request->getParam('size', 6);
        $size           = ( is_int($size) && $size >= 6 && $size <= 100 ) ? $size : 6;
        $page           = $request->getParam('page', 1);

        //  Next 7 days
        if ( $startDate === NULL && $endDate === NULL ) {
            $startDate = date('Y-m-d H:i:00');
            $endDate = date('Y-m-d H:i:59', time()+7*24*60*60);
        }

        $filters = new API_WorkOrderFilter;
        $filters->WO_State          = WO_STATE_PUBLISHED;
        $filters->Project_ID        = ( is_array($projects) ) ? ( (count($projects) > 1) ? $projects : $projects[0] ) : $projects;
        $filters->TB_UNID           = $win;
        $filters->State             = $state;
        $filters->Zipcode           = $zip;
        $filters->StartDateFrom     = $startDate;
        $filters->StartDateTo       = $endDate;
        $filters->Tech              = $tech;
        $filters->Region            = $region;
        $filters->Call_Type         = $callType;

        $search = new Core_Api_Class;

        $wo = $search->getWorkOrders(
            $this->_login.'|pmContext='.$company,
            $this->_password,
            self::FIELDS,
            '',
            '',
            '',
            $filters,
            ($page-1)*$size,
            $page*$size,
            $countRows,
            $this->_sortStr
        );
        
        $this->view->wo         = $wo->data;
        $this->view->success    = $wo->success;
        $this->view->errors     = $wo->errors;
        $this->view->company    = $company;
    }

    public function assignedAction()
    {
	    require_once 'Core/Api/Class.php';
        require_once 'API/WorkOrderFilter.php';

        $request        = $this->getRequest();

        $company        = $request->getParam('company', NULL);
        $projects       = $request->getParam('projects', NULL);
        $win            = $request->getParam('win', NULL);
        $state          = $request->getParam('state', NULL);
        $zip            = $request->getParam('zip', NULL);
        $startDate      = $request->getParam('start_date', NULL);
        $endDate        = $request->getParam('end_date', NULL);
        $tech           = $request->getParam('tech', NULL);
        $role           = $request->getParam('role', NULL);
        $region         = $request->getParam('region', NULL);
        $callType       = $request->getParam('call_type', NULL);
        $bid            = $request->getParam('bid', NULL);
        $size           = $request->getParam('size', 6);
        $size           = ( is_int($size) && $size >= 6 && $size <= 100 ) ? $size : 6;
        $page           = $request->getParam('page', 1);

        $filters = new API_WorkOrderFilter;
        $filters->WO_State          = WO_STATE_ASSIGNED;
        $filters->Project_ID        = ( is_array($projects) ) ? ( (count($projects) > 1) ? $projects : $projects[0] ) : $projects;
        $filters->TB_UNID           = $win;
        $filters->State             = $state;
        $filters->Zipcode           = $zip;
        $filters->StartDateFrom     = $startDate;
        $filters->StartDateTo       = $endDate;
        $filters->Tech              = $tech;
        $filters->Region            = $region;
        $filters->Call_Type         = $callType;

        $search = new Core_Api_Class;

        $wo = $search->getWorkOrders(
            $this->_login.'|pmContext='.$company,
            $this->_password,
            self::FIELDS,
            '',
            '',
            '',
            $filters,
            ($page-1)*$size,
            $page*$size,
            $countRows,
            $this->_sortStr
        );

        $techs = array();
        if ( $wo->success && $countRows ) {
            foreach ( $wo->data as &$worder ) {
                $techs[] = $worder->Tech_ID;
            }
        }

        $tech = new API_Tech;

        $this->view->wo         = $wo->data;
        $this->view->techs      = $tech->lookupID(array_unique($techs));
        $this->view->success    = $wo->success;
        $this->view->errors     = $wo->errors;
        $this->view->company    = $company;
    }

    public function workdoneAction()
    {
    }

    public function workdoneSmallAction()
    {
        require_once 'Core/Api/Class.php';
        require_once 'API/WorkOrderFilter.php';

        $request        = $this->getRequest();

        $company        = $request->getParam('company', NULL);
        $projects       = $request->getParam('projects', NULL);
        $win            = $request->getParam('win', NULL);
        $state          = $request->getParam('state', NULL);
        $zip            = $request->getParam('zip', NULL);
        $startDate      = $request->getParam('start_date', NULL);
        $endDate        = $request->getParam('end_date', NULL);
        $tech           = $request->getParam('tech', NULL);
        $role           = $request->getParam('role', NULL);
        $region         = $request->getParam('region', NULL);
        $callType       = $request->getParam('call_type', NULL);
        $bid            = $request->getParam('bid', NULL);

        $size           = $request->getParam('size', 6);
        $size           = ( is_int($size) && $size >= 6 && $size <= 100 ) ? $size : 6;
        $page           = $request->getParam('page', 1);

        $filters = new API_WorkOrderFilter;
        $filters->WO_State          = WO_STATE_WORK_DONE;
        $filters->Project_ID        = ( is_array($projects) ) ? ( (count($projects) > 1) ? $projects : $projects[0] ) : $projects;
        $filters->TB_UNID           = $win;
        $filters->State             = $state;
        $filters->Zipcode           = $zip;
        $filters->StartDateFrom     = $startDate;
        $filters->StartDateTo       = $endDate;
        $filters->Tech              = $tech;
        $filters->Region            = $region;
        $filters->Call_Type         = $callType;

        $search = new Core_Api_Class;

        $wo = $search->getWorkOrders(
            $this->_login.'|pmContext='.$company,
            $this->_password,
            self::FIELDS,
            '',
            '',
            '',
            $filters,
            ($page-1)*$size,
            $page*$size,
            $countRows,
            $this->_sortStr
        );

        $techs = array();
        if ( $wo->success && $countRows ) {
            foreach ( $wo->data as &$worder ) {
                $techs[] = $worder->Tech_ID;
            }
        }

        $tech = new API_Tech;

        $this->view->wo         = $wo->data;
        $this->view->success    = $wo->success;
        $this->view->errors     = $wo->errors;
        $this->view->company    = $company;
    }

    public function alertemailsAction()
    {
    }
}

