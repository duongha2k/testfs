<?php
class Admindashboard_IsoController extends Core_Controller_Action
{
    public function init()
    {
        parent::init();

        $request = $this->getRequest();

        $auth = new Zend_Session_Namespace('Auth_User');
        $this->view->isPM = $auth->isPM;
        $this->view->name = $auth->name;
        $company = $request->getParam('company',NULL);		
    }

    public function createAction()
    {		
        $errorMessages = new Core_Form_ErrorHandler('createIso' );
        $search = new Core_Api_AdminClass;
        //get states
        $states = $search->getStatesArray('US');
        $this->view->states = $states->data;
        //get countries
        $countries = $search->getCountries();
        $this->view->countries = $countries->data;
		
		$search = new Core_Api_AdminClass;
        $fieldsList = array('Company_ID','CompanyName');
        $companies = $search->getCompanies($this->_login,$this->_password,$fieldsList,'CompanyName','CompanyName');
		$this->view->companies = $companies->data;

        $this->view->messages = $errorMessages->getMessages();
        $errorMessages->clearMessages();
    }

    public function doCreateAction()
    {
        $errorMessages = new Core_Form_ErrorHandler( 'createIso' );
        $createForm    = $this->getRequest()->getParam('CreateForm', null);

        if(empty($createForm)) $errorMessages->addMessage( 'Create Iso Form is empty' );

        $admin = new Core_Api_AdminClass();
        $result = $admin->createISO($this->_login, $this->_password, $createForm);

        if( !$result->success ) {
            foreach ( $result->errors as $error ) {
                $errorMessages->addMessage($error->message);
            }
        } else {
/*            $id = $result->data[0]->UNID;
            $msgs = array();

            if ( !empty($msgs) ) {
                $msgs = array_unique($msgs);
                foreach ( $msgs as $msg ) $errorMessages->addMessage($msg);
            }*/
        }

        $this->_redirect($_SERVER['HTTP_REFERER']);
    }

    public function searchAction()
    {
        $search = new Core_Api_AdminClass;
        //get companies
        $fieldsList = array('Company_ID','CompanyName');
        $companies = $search->getCompanies($this->_login,$this->_password,$fieldsList,'CompanyName','CompanyName');

        $this->view->companies = $companies->data;
    }
	
    public function listAction()
    {
        $request = $this->getRequest();
        $params  = $request->getParams();

        //13960
        //$size    = ($request->getParam('size', 10)) + 0; 
        $size = 100;        
        //end 13960
        $page    = ($request->getParam('page',1)) + 0;
        $page    = ( $page < 1 ) ? 1 : $page;

        $sortBy  = $request->getParam('sortBy', 'Company_Name');
        $sortDir = $request->getParam('sortDir', 'asc');

        $this->view->sortBy = $sortBy;
        $this->view->sortDir = $sortDir;
        
        $search  = new Core_Api_AdminClass;

        $countRows =0;

        // Save workorders into CSV file
        /*if ( $request->getParam('download', false) === 'csv' ) {
            try {
                ob_start();

                $projects = $search->getClientsList(
                    $this->_login,
                    $this->_password,
                    $params,
					'Project_ID ASC'
                );

                $res = array( "Project_ID","Project_Name","Headline","Client_Company_Name");
                $this->putcsv($res);

                if ($projects->success )
                foreach ($projects->data as $project) {
                    $row = array(
                        $project->Project_ID,
                        $project->Project_Name,
                        $project->Headline,
                        $project->Client_Name
                    );
                    $this->putcsv($row);
                }
                $csv = ob_get_contents();
                ob_end_clean();

                return $this->_outputCsv($csv);
            } catch (Exception $e) {
                print 'error occurred';
                return false;
            }
        }*/

        $iso = $search->getISOsList(
                $this->_login,
                $this->_password,
                $params,
                (!empty($sortBy) && !empty($sortDir) ) ? "{$sortBy} {$sortDir}" : null,
                ($page-1)*$size,
                $size,
                $countRows
        );

        if ( $iso->success ) {
           $paginator = new Zend_Paginator(new Core_Paginator_Sliced_Array($iso->data, $countRows));
            
           $paginator->setCurrentPageNumber($page); 
           $paginator->setItemCountPerPage($size);//13960
           $this->view->paginator = $paginator;
        }
        $this->view->success    = $iso->success;
        $this->view->errors     = $iso>errors;
    }

    public function updateAction()
    {
        $id = $this->getRequest()->getParam('id', null);
        $iso      = new Core_ISO();
        $iso->load($id);

        $errorMessages = new Core_Form_ErrorHandler( 'updateIso_'.$client_id );
        
        $this->view->iso  = $iso;
        $this->view->success = $iso !== false;
        
        $search  = new Core_Api_AdminClass;
        
        $errors = Core_Api_Error::getInstance();
        $er = $errors->getErrorArray();
        foreach ( $er as $k=>$v ) $errorMessages->addMessage($error->message);
        
        //get states
        $states = $search->getStatesArray($iso->Country);
        $this->view->states = $states->data;
        //get countries
        $countries = $search->getCountries();
        $this->view->countries = $countries->data;
		
		$search = new Core_Api_AdminClass;
        $fieldsList = array('Company_ID','CompanyName');
        $companies = $search->getCompanies($this->_login,$this->_password,$fieldsList,'CompanyName','CompanyName');
		$this->view->companies = $companies->data;

		$this->view->messages = $errorMessages->getMessages();
        $errorMessages->clearMessages();
        
        $isRequiredZip = true;
        $isRequiredPhone = true;
        if($iso->Country != "US" && $iso->Country != "CA") $isRequiredPhone = false;

        if(!empty($iso))
        {
            $Country = Core_Country::getCountryInfoByCode($iso->Country);
            if(!empty($Country))
            {
                if($Country["ZipRequired"] != "1")
                {
                    $isRequiredZip = false;
    }
            }
        }
        $this->view->isRequiredPhone = $isRequiredPhone;
        $this->view->isRequiredZip = $isRequiredZip;
    }

    public function doUpdateAction()
    {
        $updateForm    = $this->getRequest()->getParam('UpdateForm', null);
        $id            = $this->getRequest()->getParam('UNID', null);
        $errorMessages = new Core_Form_ErrorHandler( 'updateIso_'.$id );
        
        $errorMessages->clearMessages();
        $er = $errorMessages->getMessages();
        if(empty($updateForm) || empty($id)) $errorMessages->addMessage( 'Update Iso Form is empty' );
        $er = $errorMessages->getMessages();

        if(empty($er) || sizeof($er) == 0) {
            $search = new Core_Api_AdminClass;

            $result = $search->updateISO($this->_login, $this->_password, $id, $updateForm);
            $errors = Core_Api_Error::getInstance();
            if( !$result->success ) {
                foreach ( $result->errors as $error ) {
                    $errorMessages->addMessage($error->message);
                }
            } else {
                if ( !empty($msgs) ) {
                    $msgs = array_unique($msgs);
                    foreach ( $msgs as $msg ) $errorMessages->addMessage($msg);
                }
            }
        }
        $this->_redirect($_SERVER['HTTP_REFERER']);
    }

/*    public function removeAction(){
        $this->_helper->viewRenderer->setNoRender();
        $response = $this->getResponse();
        $id = $this->getRequest()->getParam('id', null);
        $errors = array();
        if(!empty($id)) {
            $admin = new Core_Api_AdminClass();
            $res = $admin->removeClient($this->_login,$this->_password,$id);
            if( !$res->success ) {
                foreach ( $res->errors as $error ) $errors[] = $error->message;
            }
        } else {
            $errors[] = 'Client ID empty bu reqired.';
        }
        $json_output = array('success' => empty($errors), 'error' => $errors);
        $response->clearAllHeaders()->clearBody();
        $response->setHeader('Content-type', 'application/json');
        $response->setBody( json_encode($json_output) );
    }

    private function putcsv($row, $fd=',', $quot='"')
    {
       $str='';
       foreach ($row as $cell) {
          $cell = str_replace($quot, $quot.$quot, $cell);
          if (strchr($cell, $fd) !== FALSE || strchr($cell, $quot) !== FALSE || strchr($cell, "\n") !== FALSE) {
             $str .= $quot.$cell.$quot.$fd;
          } else {
             $str .= $cell.$fd;
          }
       }
       echo substr($str, 0, -1)."\n";
       return strlen($str);
    }
    
    
    private function _outputCsv($csvText, $fname = null)
    {
		if (empty($fname)) $fname = $this->_company.'-'.ucfirst('clients').'-'.date('mdY').'.csv';
        $this->_helper->viewRenderer->setNoRender();
        $response = $this->getResponse();
        $response->clearHeaders();
        $response->clearBody();
        $response->setHeader('Pragma', 'public')
                ->setHeader('Expires', '0')
                ->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0')
                ->setHeader('Cache-Control', 'private')
                ->setHeader('Content-Type', 'text/csv')
                ->setHeader('Content-Disposition', 'attachment; filename='.$fname, true)
                ->setHeader('Content-Transfer-Encoding', 'binary');
        $response->sendResponse();
        //Flush buffers
        ob_end_flush();
        flush();
        echo $csvText;
        exit;
    }*/
}