<?php
class Admindashboard_CommentsController extends Core_Controller_Action
{
    public function init()
    {
        parent::init();

    }

	public function clearAbuseAction() {
		$this->_helper->viewRenderer->setNoRender(true);
		$c = new Core_Comments;
		$id = $this->getRequest()->getParam('id', null);
		if (!empty($id)) $c->clearAbuse($id);
		echo "<script type='text/javascript'>window.location='/reports/reportviewer/report.html?name=AdminCommentsAbuse.prpt';</script>";
	}
	public function deleteAction() {
		$this->_helper->viewRenderer->setNoRender(true);
		$c = new Core_Comments;
		$id = $this->getRequest()->getParam('id', null);
//		if (!empty($id)) $c->delete($id);
		echo "<script type='text/javascript'>window.location='/reports/reportviewer/report.html?name=AdminCommentsAbuse.prpt';</script>";
	}
}
