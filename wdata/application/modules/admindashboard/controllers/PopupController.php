<?php

class Admindashboard_PopupController extends Core_Controller_Action
{
    public function init()
    {
        parent::init();
        //  This is constroller can be used for AJAX only!
        $request = $this->getRequest();
        if ( !$request->isXmlHttpRequest() ) {
            $this->_redirect('/');
        }
    }

    public function incommentsAction()
    {
        $mcomments = $this->_getParam('MissingComments');
        $mcomments = htmlspecialchars(trim($mcomments));
        if ( empty($mcomments) ) $mcomments = 'No comments';
        $this->view->MissingComments = $mcomments;
    }

    public function netAmountAction()
    {
        require_once 'Core/Api/TechClass.php';
        require_once 'API/WorkOrderFilter.php';

        $win = $this->_getParam('win');

        if ( empty($win) ) {
            $this->_helper->viewRenderer->setNoRender(true);

            $response = $this->getResponse();
            $response->clearAllHeaders();
            $response->clearBody();

            $response
                ->setHttpResponseCode(406)
                ->setBody('WIN # is required and cannot be empty');
            return;
        }
        $search = new Core_Api_TechClass;
        $wo = $search->getWorkOrder($this->_login, $this->_password, $win);

        if ( !$wo->success ) {
            $this->_helper->viewRenderer->setNoRender(true);

            $response = $this->getResponse();
            $response->clearAllHeaders();
            $response->clearBody();

            $response
                ->setHttpResponseCode(406)
                ->setBody($wo->errors[0]->message);
            return;
        }

        $this->view->wo = $wo->data[0];
    }

    public function bidSearchWoAction()
    {
        require_once 'Core/Api/TechClass.php';
        require_once 'API/WorkOrderFilter.php';

        $request = $this->getRequest();
        $win     = $request->getParam('win', NULL);
        $techId  = $request->getParam('tech_id', NULL);

        if ( empty($win) ) {
            $this->_helper->viewRenderer->setNoRender(true);

            $response = $this->getResponse();
            $response->clearAllHeaders();
            $response->clearBody();

            $response
                ->setHttpResponseCode(406)
                ->setBody('WIN # is required and cannot be empty');
            return;
        }

        $paths = $this->getFrontController()->getControllerDirectory();
        if ( !empty($paths['dashboard']) && file_exists($paths['dashboard'].'/IndexController.php') ) {
            require_once $paths['dashboard'].'/IndexController.php';
            $fields = Dashboard_IndexController::FIELDS;
        } else {
            $fields = NULL;
        }

        $filters = new API_WorkOrderFilter;
        $filters->Deactivated   = FALSE;
        $filters->ShowTechs     = TRUE;
        $filters->Sourced       = FALSE;
        $filters->Tech          = NULL;
        $filters->TB_UNID       = $win;

        $search  = new Core_Api_TechClass;

        $wo = $search->getWorkOrders($this->_login, $this->_password, $fields, $filters);

        if ( !$wo->success ) {
            $this->_helper->viewRenderer->setNoRender(true);

            $response = $this->getResponse();
            $response->clearAllHeaders();
            $response->clearBody();

            $response
                ->setHttpResponseCode(406)
                ->setBody($wo->errors[0]->message);
            return;
        } else if ( !$wo->errors && empty($wo->data) ) {
            $this->_helper->viewRenderer->setNoRender(true);

            $response = $this->getResponse();
            $response->clearAllHeaders();
            $response->clearBody();

            $response
                ->setHttpResponseCode(406)
                ->setBody(Core_Api_TechClass::ERROR_WO_INVALID_WIN);
            return;
        }

        $this->view->wo = $wo->data[0];
    }

    public function filtersAction()
    {
        require_once 'Core/Api/Class.php';

        $request    = $this->getRequest();
        $companyId  = $request->getParam('company', NULL);
        $tab        = $request->getParam('tab', NULL);

        $search = new Core_Api_AdminClass;

        $cacheKeyFilters = Core_Cache_Manager::makeKey('filters', $companyId, $tab, $this->_login);
        $cache = $this->getInvokeArg('bootstrap')->getResource('Cache');

        $projectListRes = array();
        if ($companyId) {
        	if ( FALSE == ($projectList = $cache->load($cacheKeyFilters)) ) {
        		$projectList = $search->getAllProjects($this->_login, $this->_password, $companyId);
        		if ( $projectList->success ) {
        			$cache->save($projectList, $cacheKeyFilters, array('filter'), LIFETIME_15MIN);
        		}
        		$projectListRes = $projectList->data;
        	}

        	if ( !empty($projectList->data) ) {
        		usort($projectList->data, 'Core_Controller_Action::_cmpProjects');
        	}
        }

        $result = $search->getStates();
        if ( $result->success ) {
            $states = $result->data;
        }

        $this->view->states     = $states;
        $this->view->projects   = $projectList->data;//13849
        $this->view->callTypes  = $search->getCallTypes()->data;
        $this->view->companyId  = $companyId;
    }

    public function findAvailableWorkAction()
    {
        require_once 'Core/Api/Class.php';

        $request    = $this->getRequest();
        $companyId  = $request->getParam('company', NULL);
        $tab        = $request->getParam('tab', NULL);

        $search = new Core_Api_Class;

        $cacheKeyFilters = Core_Cache_Manager::makeKey('filters', $companyId, $tab, $this->_login);
        $cache = $this->getInvokeArg('bootstrap')->getResource('Cache');

        if ( FALSE == ($projectList = $cache->load($cacheKeyFilters)) ) {
            $projectList = $search->getAllProjects($this->_login.'|pmContext='.$companyId, $this->_password);
            if ( $projectList->success ) {
                $cache->save($projectList, $cacheKeyFilters, array('filter'), LIFETIME_15MIN);
            }
        }

        $result = $search->getStates();
        if ( $result->success ) {
            $states = $result->data;
        }

        if ( !empty($projectList->data) ) {
            usort($projectList->data, 'Core_Controller_Action::_cmpProjects');
        }

        $this->view->states     = $states;
        $this->view->projects   = $projectList->data;
        $this->view->companyId  = $companyId;
    }

    public function techFiltersAction()
    {
        $request    = $this->getRequest();
        $tab        = $request->getParam('tab', NULL);

        $search = new Core_Api_Class;

        $result = $search->getStates();
        if ( $result->success ) {
            $states = $result->data;
        }
        $this->view->states     = $states;
        $this->view->callTypes  = $search->getCallTypes()->data;
    }

    public function findworkorderAction()
    {
        require_once 'Core/Api/Class.php';

        $request    = $this->getRequest();
        $companyId  = $request->getParam('company', NULL);

        $search = new Core_Api_Class;

        $cacheKeyFilters = Core_Cache_Manager::makeKey('findworkorder', $companyId, $this->_login);
        $cache = $this->getInvokeArg('bootstrap')->getResource('Cache');

        if ( FALSE == ($projectList = $cache->load($cacheKeyFilters)) ) {
            $projectList = $search->getAllProjects($this->_login.'|pmContext='.$companyId, $this->_password);
            if ( $projectList->success ) {
                $cache->save($projectList, $cacheKeyFilters, array('filter'), LIFETIME_15MIN);
            }
        }

        $this->view->projects   = $projectList->data;
        $this->view->companyId  = $companyId;
    }

    public function techInfoAction()
    {
        require_once 'Core/Api/Class.php';

        $request = $this->getRequest();
        $tech    = $request->getParam('tech', NULL);
        $search  = new Core_Api_Class;

        $techInfo = $search->techLookup($this->_login, $this->_password, $tech);

        $this->view->tech = $techInfo;
    }

    public function getTechInfoAction()
    {
        require_once 'Core/Api/Class.php';

        $this->_helper->viewRenderer->setNoRender();
        $response = $this->getResponse();
        $response->setHeader('Content-Type', 'text/json', true);

        $request = $this->getRequest();
        $tech    = $request->getParam('tech', NULL);
        if(empty ($tech) || !is_numeric($tech)){
            $jsonContext = json_encode(array('error'=>1, 'msg'=>'Tech Id is empty or not numeric'));
        } else {
        $search  = new Core_Api_Class;

        $techInfo = $search->techLookup($this->_login, $this->_password, $tech);

        if($techInfo->success && !empty($techInfo->data[0]->TechID)) {
        	$info = array('fName'=>$techInfo->data[0]->Firstname,
                          'lName'=>$techInfo->data[0]->Lastname,
        				  'email'=>$techInfo->data[0]->PrimaryEmail,
        				  'email2'=>$techInfo->data[0]->SecondaryEmail,
        				  'phone'=>$techInfo->data[0]->PrimaryPhone,
        				  'phone2'=>$techInfo->data[0]->SecondaryPhone);
            $jsonContext = json_encode(array('error'=>0, 'techInfo'=>$info));
        } else {
            $jsonContext = json_encode(array('error'=>1, 'msg'=>'There is no such tech'));
        }
        }
        $response->setHeader('Content-Length', strlen($jsonContext), true)
                 ->setBody($jsonContext);
    }

    public function getWoInfoAction()
    {
        require_once 'Core/Api/Class.php';

        $this->_helper->viewRenderer->setNoRender();

        $request = $this->getRequest();

        $woId    = $request->getParam('WO_ID', NULL);
        $winNum  = intval($request->getParam('WIN_NUM', NULL));
        $company = trim($request->getParam('company', NULL));
        $search  = new Core_Api_Class;

        if (!empty ($winNum) && is_numeric($winNum)) {
            $woInfo = $search->getWorkOrder($this->_login.'|pmContext='.$company, $this->_password, (int)$winNum);
        } elseif (!empty ($woId)) {
            $filters = new API_WorkOrderFilter;
            $filters->WO_ID = trim($woId);

            $woInfo = $search->getWorkOrders(
                $this->_login.'|pmContext='.$company,
                $this->_password,
                'WIN_NUM,WO_ID,Tech_Bid_Amount,Amount_Per,Deactivated,DeactivationCode,Deactivated_Reason,PayMax',
                '','','',
                $filters,
                0,0,$countRows,''
            );
        }

        $response = $this->getResponse();
        $response->setHeader('Content-Type', 'text/json', true);

        if ($woInfo->success) {
            if (!empty($woInfo->data[0]->WIN_NUM)) {
                $jsonContext = json_encode(array('error'=>0,
                                                 'woInfo'=>array('WIN_NUM'=>$woInfo->data[0]->WIN_NUM,
                												 'WO_ID'=>$woInfo->data[0]->WO_ID,
                                                                 'Tech_Bid_Amount'=>$woInfo->data[0]->Tech_Bid_Amount,
                                                                 'PayMax'=>$woInfo->data[0]->PayMax,
                                                                 'Amount_Per'=>$woInfo->data[0]->Amount_Per,
                												 'Deactivated'=>$woInfo->data[0]->Deactivated,
                												 'DeactivationCode'=>$woInfo->data[0]->DeactivationCode,
                												 'Deactivated_Reason'=>$woInfo->data[0]->Deactivated_Reason)));
            } else
                $jsonContext = json_encode(array('error'=>1, 'msg'=>'There is no such Work Order'));
        } else
            $jsonContext = json_encode(array('error'=>1, 'msg'=>'There is no such Work Order'));

        $response->setHeader('Content-Length', strlen($jsonContext), true)
                 ->setBody($jsonContext);
    }

    public function getFullWoInfoAction() {
        require_once 'Core/Api/Class.php';

        $this->_helper->viewRenderer->setNoRender();

        $request = $this->getRequest();

        $woId    = $request->getParam('WO_ID', NULL);
        $winNum  = intval($request->getParam('WIN_NUM', NULL));
        $company = trim($request->getParam('company', NULL));

        $response = $this->getResponse();
        $response->setHeader('Content-Type', 'text/json', true);

        $search  = new Core_Api_Class;
        if (!empty ($winNum) && is_numeric($winNum)) {
            $woInfo = $search->getWorkOrder($this->_login.'|pmContext='.$company, $this->_password, (int)$winNum);
        } elseif (!empty ($woId)) {
            $filters = new API_WorkOrderFilter;
            $filters->WO_ID = trim($woId);

            $woInfo = $search->getWorkOrders(
                $this->_login.'|pmContext='.$company,
                $this->_password,
                'WIN_NUM',
                '','','',
                $filters,
                0,0,$countRows,''
            );
            if ($woInfo->success && !empty($woInfo->data[0]->WIN_NUM)) {
            	$woInfo = $search->getWorkOrder($this->_login.'|pmContext='.$company, $this->_password, (int)$woInfo->data[0]->WIN_NUM);
            } else {
                $jsonContext = json_encode(array('error'=>1, 'msg'=>'There is no such Work Order'));
        		$response->setHeader('Content-Length', strlen($jsonContext), true)
                ->setBody($jsonContext);
                return;
            }
        }

        if ($woInfo->success) {
            if (!empty($woInfo->data[0]->WIN_NUM)) {
                $jsonContext = json_encode(array('error'=>0, 'woInfo'=> $woInfo->data[0]));
            } else
                $jsonContext = json_encode(array('error'=>1, 'msg'=>'There is no such Work Order'));
        } else
            $jsonContext = json_encode(array('error'=>1, 'msg'=>'There is no such Work Order'));

        $response->setHeader('Content-Length', strlen($jsonContext), true)
            ->setBody($jsonContext);
    }

    public function activityLogAction()
    {
        require_once 'API/Timestamp.php';
        require_once 'Core/Api/Class.php';
        require_once 'API/WorkOrderFilter.php';

        $this->_helper->viewRenderer->setNoRender();

        $request    = $this->getRequest();
        $response   = $this->getResponse();

        $win        = intval($request->getParam('win', ''));
        $woid       = $request->getParam('woid', '');
        $company    = $request->getParam('company', NULL);

        if ( !empty($win) ) {
            $filter = new API_WorkOrderFilter();
            $filter->TB_UNID = $win;

            $result = API_WorkOrderFilter::filter($company, 'WO_ID', '', '', '', $filter, '')->toArray();
            if ( !empty($result) && is_array($result) ) {
                $woid = $result[0]['WO_ID'];
            }

            $log = API_Timestamp::getTimestamps($win, $company, 0, 0, 'DateTime_Stamp DESC');
        } else if ( !empty($woid) ) {

            $filter = new API_WorkOrderFilter();
            $filter->WO_ID = $woid;

            $result = API_WorkOrderFilter::filter($company, 'TB_UNID', '', '', '', $filter, '')->toArray();
            if ( !empty($result) && is_array($result) ) {
                $win = $result[0]['TB_UNID'];
                $log = API_Timestamp::getTimestamps($win, $company, 0, 0, 'DateTime_Stamp DESC');
            } else {
                $log = null;
            }
        } else {
            $log = null;
        }

        if ( !empty($log) && is_array($log) )
            foreach ( $log as &$ts )
                $ts->description = str_replace(array(',', ';', ':', '.', '-'), array(', ', '; ', ': ', '- '), $ts->description);

        $this->view->log = $log;

        $data = array('win' => $win, 'woid' => $woid, 'html' => $this->view->render('popup/activity-log.phtml'));

        $response->clearBody();
        $response
            ->clearAllHeaders()
            ->setHeader('Content-Type', 'application/json')
            ->setBody(json_encode($data));
    }

    public function approveAction()
    {

    }
}

