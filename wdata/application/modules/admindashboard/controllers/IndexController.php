<?php
//require_once '/home/cheshchevik/work_copy/fieldsolution_saturn/includes/classes/API/AdminWorkOrderFilter.php';
//require_once 'Core/Api/AdminClass.php';

class Admindashboard_IndexController extends Core_Admin_Controller_Action
{
    const FIELDS = 'WIN_NUM,WO_ID,DateEntered,PO,ShowTechs,SourceByDate,ShortNotice,StartDate,StartTime,EndDate,EndTime,StartRange,Duration,Region,Route,Type_ID,Project_ID,WO_Category_ID,Headline,Site_Contact_Name,SiteName,SiteNumber,Project_Name,Address,City,State,Zipcode,SitePhone,SiteFax,SiteEmail,Update_Requested,Update_Reason,Description,Requirements,Tools,SpecialInstructions,Lead,Assist,SignOffSheet_Required,ProjectManagerName,ProjectManagerPhone,ProjectManagerEmail,ResourceCoordinatorName,ResourceCoordinatorPhone,ResourceCoordinatorEmail,EmergencyName,EmergencyPhone,EmergencyEmail,TechnicalSupportName,TechnicalSupportPhone,TechnicalSupportEmail,CheckInOutName,CheckInOutNumber,CheckInOutEmail,Tech_ID,Tech_FName,Tech_LName,NoShow_Tech,BackOut_Tech,FLS_ID,BU1FLSID,BU1Info,Backup_FLS_ID,Ship_Contact_Info,PayMax,Amount_Per,Qty_Devices,Tech_Bid_Amount,WorkOrderReviewed,TechCheckedIn_24hrs,CheckedIn,Checkedby,PreCall_1,PreCall_2,CheckInNotes,StoreNotified,Notifiedby,DateNotified,NotifyNotes,TechMarkedComplete,Date_In,Time_In,Date_Out,Time_Out,ContactHD,HDPOC,TechMiles,TechComments,AttachedFile,FLS_OOS,Unexpected_Steps,Unexpected_Desc,AskedBy,AskedBy_Name,Paperwork_Received,Incomplete_Paperwork,MissingComments,Site_Complete,CallClosed,PartsProcessed,ClientComments,Work_Out_of_Scope,OutofScope_Amount,OutofScope_Reason,TripCharge,MileageReimbursement,MaterialsReimbursement,Additional_Pay_Amount,Penalty_Reason,AbortFee,AbortFeeAmount,Add_Pay_AuthBy,Add_Pay_Reason,calculatedTechHrs,baseTechPay,PayAmount,Approved,TechPaid,DatePaid,Deactivated,DeactivationCode,Deactivated_Reason,General1,General2,General3,General4,General5,General6,General7,General8,General9,General10,FS_InternalComments,Username,Invoiced,Company_Name,Company_ID,TechEmail,TechPhone,TechPhoneExt,Date_Assigned,lastModBy,lastModDate,approvedBy,DateApproved,DeactivatedDate,DeactivatedBy,Latitude,Longitude,PricingRan,WO_Category,DateIncomplete,ReminderAcceptance,Reminder24Hr,Reminder1Hr,CheckInCall,CheckOutCall,ReminderNotMarkComplete,ReminderIncomplete,ReminderAll,SMSBlast,MinutesRemainPublished,MinimumSelfRating,MaximumAllowableDistance,isProjectAutoAssign,isWorkOrdersFirstBidder,P2TPreferredOnly,P2TAlreadyAssigned,SATRecommended,SATPerformance,ReturnPartCount,NewPartCount,Qty_Applicants,Status,DateInvoiced,Work_Out_of_Scope,OutOfScope_Reason,PricingRuleApplied,calculatedInvoice,PcntDeduct,PcntDeductPercent,Net_Pay_Amount,Qty_Visits';

    private $_filter;
    private $_size;
    private $_page;
    private $_company;
    private $_tab;
   
    
    public function init()
    {
        parent::init();
        //  This is constroller can be used for AJAX only!
        $request = $this->getRequest();

        /* Check for ajax requests only, except 'csv' download */
        if ( !$request->isXmlHttpRequest() ) {
            if ( $this->getRequest()->getParam('download', false) !== 'csv' ) {
                $this->_redirect('/');
            }
        }
        /* Check for ajax requests only, except 'csv' download */

        $this->_company     = $request->getParam('company', NULL);


        $this->_sortStr = $this->_assembleSortStr(array(
            's1' => $request->getParam('sort1', NULL), 'd1' => $request->getParam('dir1', NULL),
            's2' => $request->getParam('sort2', NULL), 'd2' => $request->getParam('dir2', NULL),
            's3' => $request->getParam('sort3', NULL), 'd3' => $request->getParam('dir3', NULL)
        ));

        /* Get Method when user download a CSV file */
        if ( $request->isGet() ) {
            if ( NULL !== ($projects = $request->getParam('projects', NULL)) ) {
                if ( is_string($projects) && strpos($projects, '|', 1) ) {
                    $projects = explode('|', $projects);
                    $request->setParam('projects', $projects);
                }
            }
            if ( NULL !== ($startdate = $request->getParam('start_date', NULL)) ) {
                $request->setParam('start_date', str_replace('_', '/', $startdate));
            }
            if ( NULL !== ($enddate = $request->getParam('end_date', NULL)) ) {
                $request->setParam('end_date', str_replace('_', '/', $enddate));
            }
            if ( NULL !== ($date_approved_from = $request->getParam('date_approved_from', NULL)) ) {
                $request->setParam('date_approved_from', str_replace('_', '/', $date_approved_from));
            }
            if ( NULL !== ($date_approved_to = $request->getParam('date_approved_to', NULL)) ) {
                $request->setParam('date_approved_to', str_replace('_', '/', $date_approved_to));
            }
            if ( NULL !== ($date_entered_from = $request->getParam('date_entered_from', NULL)) ) {
                $request->setParam('date_entered_from', str_replace('_', '/', $date_entered_from));
            }
            if ( NULL !== ($date_entered_to = $request->getParam('date_entered_to', NULL)) ) {
                $request->setParam('date_entered_to', str_replace('_', '/', $date_entered_to));
            }
            if ( NULL !== ($date_invoiced_from = $request->getParam('date_invoiced_from', NULL)) ) {
                $request->setParam('date_invoiced_from', str_replace('_', '/', $date_invoiced_from));
            }
            if ( NULL !== ($date_invoiced_to = $request->getParam('date_invoiced_to', NULL)) ) {
                $request->setParam('date_invoiced_to', str_replace('_', '/', $date_invoiced_to));
            }
            if ( NULL !== ($date_paid_from = $request->getParam('date_paid_from', NULL)) ) {
                $request->setParam('date_paid_from', str_replace('_', '/', $date_paid_from));
            }
            if ( NULL !== ($date_paid_to = $request->getParam('date_paid_to', NULL)) ) {
                $request->setParam('date_paid_to', str_replace('_', '/', $date_paid_to));
            }
        }
        /* Get Method when user download a CSV file */

        $projects           = $request->getParam('projects', NULL);
        $show_to_tech       = $request->getParam('show_to_tech', NULL);
        $update_req         = $request->getParam('update_req', NULL);
        $store_notify       = $request->getParam('store_notify', NULL);
        $checked_in         = $request->getParam('checked_in', NULL);
        $reviewed_wo        = $request->getParam('reviewed_wo', NULL);
        $tech_confirmed     = $request->getParam('tech_confirmed', NULL);
        $tech_complete      = $request->getParam('tech_complete', NULL);
        $site_complete      = $request->getParam('site_complete', NULL);
        $approved           = $request->getParam('approved', NULL);
        $out_of_scope       = $request->getParam('out_of_scope', NULL);
        $tech_paid          = $request->getParam('tech_paid', NULL);
        $show_sourced       = $request->getParam('show_sourced', NULL);
        $show_not_sourced   = $request->getParam('show_not_sourced', NULL);
        $paper_received     = $request->getParam('paper_received', NULL);
        $lead               = $request->getParam('lead', NULL);
        $assist             = $request->getParam('assist', NULL);
        $deactivated        = $request->getParam('deactivated', NULL);
        $tbPay              = $request->getParam('tb_pay', NULL);
        $tech_called        = $request->getParam('tech_called', NULL);
        $pcnt_deduct        = $request->getParam('pcnt_deduct', NULL);
        $invoiced           = $request->getParam('invoiced', NULL);
        $devry              = $request->getParam('devry', NULL);   
        $route              = $request->getParam('route', NULL);   
        if ( $show_not_sourced === NULL && $show_sourced === NULL )
            $sourced = NULL;
        else if ( $show_sourced + 0 > 0 && $show_not_sourced + 0 > 0 )
            $sourced = NULL;
        else if ( $show_not_sourced + 0 > 0  )
            $sourced = 'False';
        else
            $sourced = 'True';

        $this->_filters = new API_AdminWorkOrderFilter;
        $this->_filters->Sourced                = $sourced;
        $this->_filters->TB_UNID                = $request->getParam('win', NULL);
        $this->_filters->Company_ID             = $this->_company;
        $this->_filters->Call_Type              = $request->getParam('call_type', NULL);
        $this->_filters->Project_ID             = ( is_array($projects) ) ? ( (count($projects) > 1) ? $projects : $projects[0] ) : $projects;
        $this->_filters->StartDateFrom          = $request->getParam('start_date', NULL);
        $this->_filters->StartDateTo            = $request->getParam('end_date', NULL);
        $this->_filters->WO_ID                  = $request->getParam('client_wo', NULL);
        $this->_filters->PO                     = $request->getParam('po', NULL);
        $this->_filters->ShowTechs              = ( NULL === $show_to_tech) ? NULL : ( ( $show_to_tech + 0 ) ? true : false );
        $this->_filters->Region                 = $request->getParam('region', NULL);
        $this->_filters->Route                  = $request->getParam('route', NULL);
        $this->_filters->SiteName               = $request->getParam('site_name', NULL);
        $this->_filters->SiteNumber             = $request->getParam('site_number', NULL);
        $this->_filters->City                   = $request->getParam('city', NULL);
        $this->_filters->State                  = $request->getParam('state', NULL);
        $this->_filters->Zipcode                = $request->getParam('zip', NULL);
        $this->_filters->Update_Requested       = ( NULL === $update_req ) ? NULL : ( ( $update_req + 0 ) ? true : false );
        $this->_filters->CheckedIn              = ( NULL === $checked_in ) ? NULL : ( ( $checked_in + 0 ) ? true : false );
        $this->_filters->WorkOrderReviewed      = ( NULL === $reviewed_wo ) ? NULL : ( ( $reviewed_wo + 0 ) ? true : false );
        $this->_filters->TechCheckedIn_24hrs    = ( NULL === $tech_confirmed ) ? NULL : ( ( $tech_confirmed + 0 ) ? true : false );
        $this->_filters->TechMarkedComplete     = ( NULL === $tech_complete ) ? NULL : ( ( $tech_complete + 0 ) ? true : false );
        $this->_filters->TechCalled             = ( NULL === $tech_called ) ? NULL : ( ( $tech_called + 0 ) ? true : false );
        $this->_filters->Site_Complete          = ( NULL === $site_complete ) ? NULL : ( ( $site_complete + 0 ) ? true : false );
        $this->_filters->Approved               = ( NULL === $approved ) ? NULL : ( ( $approved + 0 ) ? true : false );

        $this->_filters->Additional_Pay_Amount  = $request->getParam('extra_pay', NULL);
        $this->_filters->PricingRan             = $request->getParam('pricing_ran', NULL);
        $this->_filters->Work_Out_of_Scope      = ( NULL === $out_of_scope ) ? NULL : ( ( $out_of_scope + 0 ) ? true : false );
        $this->_filters->TechPaid               = ( NULL === $tech_paid ) ? NULL : ( ( $tech_paid + 0 ) ? true : false );
        $this->_filters->Tech_FName             = $request->getParam('tech_fname', NULL);
        $this->_filters->Tech_LName             = $request->getParam('tech_lname', NULL);
        $this->_filters->TechEmail              = $request->getParam('tech_email', NULL);
        $this->_filters->TechEmailContain       = $request->getParam('tech_email_contain', NULL);
        $this->_filters->Username               = $request->getParam('username', NULL);
        $this->_filters->Deactivated            = ( NULL === $deactivated ) ? NULL : ( ( $deactivated + 0 ) ? true : false );
        $this->_filters->StoreNotified          = ( NULL === $store_notify) ? NULL : ( ( $store_notify + 0 ) ? true : false );
        $this->_filters->Paperwork_Received     = ( NULL === $paper_received ) ? NULL : ( ( $paper_received + 0 ) ? true : false );
        $this->_filters->Lead                   = ( NULL === $lead ) ? NULL : ( ( $lead + 0 ) ? true : false );
        $this->_filters->Assist                 = ( NULL === $assist ) ? NULL : ( ( $assist + 0 ) ? true : false );
        $this->_filters->PcntDeduct             = ( NULL === $pcnt_deduct ) ? NULL : ( ( $pcnt_deduct + 0 ) ? true : false );
        $this->_filters->Invoiced               = ( NULL === $invoiced ) ? NULL : ( ( $invoiced + 0 ) ? true : false );
        
        $this->_filters->FLS_ID                 = $request->getParam('fls_id', NULL);
        $this->_filters->AbortFee                 = $request->getParam('AbortFee', NULL);

        
        $this->_filters->Headline               = $request->getParam('headline', NULL);
        $this->_filters->WO_Category            = str_replace("_", "/",$request->getParam('wo_category', NULL));
        $this->_filters->DateEnteredFrom        = $request->getParam('date_entered_from', NULL);
        $this->_filters->DateEnteredTo          = $request->getParam('date_entered_to', NULL);
        $this->_filters->DateApprovedFrom       = $request->getParam('date_approved_from', NULL);
        $this->_filters->DateApprovedTo         = $request->getParam('date_approved_to', NULL);
        $this->_filters->DateInvoicedFrom       = $request->getParam('date_invoiced_from', NULL);
        $this->_filters->DateInvoicedTo         = $request->getParam('date_invoiced_to', NULL);
	
        $this->_filters->DatePaidFrom           = $request->getParam('date_paid_from', NULL);
        $this->_filters->DatePaidTo             = $request->getParam('date_paid_to', NULL);
        $this->_filters->PayMaxFrom             = $request->getParam('pay_max_from', NULL);
        $this->_filters->PayMaxTo               = $request->getParam('pay_max_to', NULL);
        $this->_filters->Net_Pay_AmountFrom     = $request->getParam('net_pay_amount_from', NULL);
        $this->_filters->Net_Pay_AmountTo       = $request->getParam('net_pay_amount_to', NULL);
        $this->_filters->Tech_ID                = $request->getParam('tech', NULL);
        $this->_filters->Deactivated_Reason     = $request->getParam('deactivated_reason', NULL);
        $this->_filters->DeactivationCode       = $request->getParam('deactivation_code', NULL);
        
        $this->_filters->SATRecommended_From    = $request->getParam('satrecommended_from', NULL);
        $this->_filters->SATPerformance_From    = $request->getParam('satperformance_from', NULL);
        $this->_filters->TBPay                  = ( NULL === $tbPay ) ? NULL : ( ( $tbPay + 0 ) ? 1 : 0 );
        $this->_filters->Amount_Per     = $request->getParam('Amount_Per', NULL);
        
        //------------------------------------
        $this->_filters->AM     = $request->getParam('AM', "");
        $this->_filters->CSD     = $request->getParam('CSD', "");
        $this->_filters->Client_Relationship     = $request->getParam('Client_Relationship', "");
        $this->_filters->Client_Status     = $request->getParam('Client_Status', "");
        $this->_filters->Entity     = $request->getParam('Entity', "");
        $this->_filters->Executive     = $request->getParam('Executive', "");
        $this->_filters->Original_Seller     = $request->getParam('Original_Seller', "");
        $this->_filters->Segment     = $request->getParam('Segment', "");
        $this->_filters->Service_Type     = $request->getParam('Service_Type', "");
        $this->_filters->System_Status     = $request->getParam('System_Status', "");
        
        $this->_filters->ISO_ID     = $request->getParam('ISO_ID', "");
        $this->_filters->Pay_ISO     = $request->getParam('Pay_ISO', "");
        $this->_filters->ISO_Company_Name     = $request->getParam('ISO_Company_Name', "");
        
        $this->_filters->wm_assignment_id     = $request->getParam('wm_assignment_id', "");
        $this->_filters->wm_tech_id     = $request->getParam('wm_tech_id', "");
        $this->_filters->wm_tech_select     = $request->getParam('wm_tech_select', "");
		
        //--- DeVry_Tech_ID_List 
        if($devry==1)
        {
            $api = new Core_Api_TechClass;
            $deVryTechIdList=$api->getDeVryTechIdList();
            if(!empty($deVryTechIdList))
            {
                $this->_filters->DeVry_Tech_ID_List = $deVryTechIdList;
            }
            else
            {
                $this->_filters->DeVry_Tech_ID_List = -1;
            }
        }
        //--- DeVry_Tech_ID_List: end  
      
        $this->_size    = ($request->getParam('size', empty($_COOKIE['page_size']) ? 25 : $_COOKIE['page_size'] )) + 0;
        $this->_size    = ( $this->_size <= 10 ) ? 10 : ( ( $this->_size >= 100 ) ? 100 : $this->_size );
        
        setcookie("page_size", $this->_size, time() + 3600 * 24 * 30, "/");
        
        $this->_page    = ($request->getParam('page', 1)) + 0;
        $this->_page    = ( $this->_page < 1 ) ? 1 : $this->_page;
        
        $this->view->pageCurrent = $this->_page;
        $this->view->pageSize = $this->_size;

        $this->view->tab = $this->getRequest()->getActionName();
    }

    public function createdAction()
    {
        $request = $this->getRequest();
        $search = new Core_Api_AdminClass;
        $this->_filters->WO_State = WO_STATE_CREATED;

        /** Save workorders into CSV file **/
        if ( $request->getParam('download', false) === 'csv' ) {
            try {
                $csvAdapter = new Core_CSV_Adapter_Admin(
                    $this->_login,
                    $this->_password,
                    $this->_filters,
                    self::FIELDS,
                    $this->_sortStr
                );

                $csvExport = new Core_CSV_Builder('admin', 'created', $csvAdapter);
                return $this->_outputCsv($csvExport);
            } catch (Core_CSV_Exception $e) {
                print 'error occurred';
                return false;
            }
        }
        /**  End section: Save created into CSV file  **/

        $wo = $search->getWorkOrders(
            $this->_login,
            $this->_password,
            self::FIELDS,
            '',
            '',
            '',
            $this->_filters,
            ($this->_page-1)*$this->_size,
            $this->_page*$this->_size,
            $countRows,
            $this->_sortStr
        );

        if ( $wo->success && $countRows ) {
            $_wo = range(0, $countRows-1);
            for ( $j = 0, $i = ($this->_page-1)*$this->_size; $i < $this->_page*$this->_size && $i <= $countRows-1; ++$i, ++$j ) {
                $_wo[$i] = $wo->data[$j];
            }
            $wo->data = $_wo;
            unset($_wo);
        }

        if ( $wo->success ) {
            $paginator = new Zend_Paginator(new Zend_Paginator_Adapter_Array($wo->data));
            $paginator->setCurrentPageNumber($this->_page);
            $paginator->setItemCountPerPage($this->_size);
            $this->view->paginator = $paginator;
        }
        $this->view->success    = $wo->success;
        $this->view->errors     = $wo->errors;
    }

    public function publishedAction()
    {
        $request = $this->getRequest();
        $search = new Core_Api_AdminClass;

        $this->_filters->WO_State = WO_STATE_PUBLISHED;

        /** Save workorders into CSV file **/
        if ( $request->getParam('download', false) === 'csv' ) {
            try {
                $csvAdapter = new Core_CSV_Adapter_Admin(
                    $this->_login,
                    $this->_password,
                    $this->_filters,
                    self::FIELDS,
                    $this->_sortStr
                );

                $csvExport = new Core_CSV_Builder('admin', 'published', $csvAdapter);
                return $this->_outputCsv($csvExport);
            } catch (Core_CSV_Exception $e) {
                print 'error occurred';
                return false;
            }
        }
        /**  End section: Save created into CSV file  **/

        $wo = $search->getWorkOrders(
            $this->_login,
            $this->_password,
            self::FIELDS,
            '',
            '',
            '',
            $this->_filters,
            ($this->_page-1)*$this->_size,
            $this->_page*$this->_size,
            $countRows,
            $this->_sortStr
        );

        if ( $wo->success && $countRows ) {
            $_wo = range(0, $countRows-1);
            for ( $j = 0, $i = ($this->_page-1)*$this->_size; $i < $this->_page*$this->_size && $i <= $countRows-1; ++$i, ++$j ) {
                $_wo[$i] = $wo->data[$j];
            }
            $wo->data = $_wo;
            unset($_wo);
        }

        if ( $wo->success ) {
            $paginator = new Zend_Paginator(new Zend_Paginator_Adapter_Array($wo->data));

            $paginator->setCurrentPageNumber($this->_page);
            $paginator->setItemCountPerPage($this->_size);
            $this->view->paginator = $paginator;
        }
        $this->view->success    = $wo->success;
        $this->view->errors     = $wo->errors;
    }

    public function assignedAction()
    {
        $request = $this->getRequest();

        $search = new Core_Api_AdminClass;
        $this->_filters->WO_State = WO_STATE_ASSIGNED;

		/** Save workorders into CSV file **/
        if ( $request->getParam('download', false) === 'csv' ) {
            try {
                $csvAdapter = new Core_CSV_Adapter_Admin(
                    $this->_login,
                    $this->_password,
                    $this->_filters,
                    self::FIELDS,
                    $this->_sortStr
                );

                $csvExport = new Core_CSV_Builder('admin', 'assigned', $csvAdapter);
                return $this->_outputCsv($csvExport);
            } catch (Core_CSV_Exception $e) {
                print 'error occurred';
                return false;
            }
        }
        /**  End section: Save created into CSV file  **/

        $wo = $search->getWorkOrders(
            $this->_login,
            $this->_password,
            self::FIELDS,
            '',
            '',
            '',
            $this->_filters,
            ($this->_page-1)*$this->_size,
            $this->_page*$this->_size,
            $countRows,
            $this->_sortStr
        );

        $techs = array();
        if ( $wo->success && $countRows ) {
            $_wo = range(0, $countRows-1);
            for ( $j = 0, $i = ($this->_page-1)*$this->_size; $i < $this->_page*$this->_size && $i <= $countRows-1; ++$i, ++$j ) {
                $_wo[$i] = $wo->data[$j];
                if ( !defined('FLSVersion') )   $techs[] = $_wo[$i]->Tech_ID;
                else                            $techs[] = $_wo[$i]->FLS_ID;
            }
            $wo->data = $_wo;
            unset($_wo);
        }

        $tech = new API_Tech;
        $techs = $tech->lookupID(array_unique($techs));

        if ( $wo->success ) {
            $paginator = new Zend_Paginator(new Zend_Paginator_Adapter_Array($wo->data));
            $paginator->setCurrentPageNumber($this->_page);
            $paginator->setItemCountPerPage($this->_size);
            $this->view->paginator = $paginator;
        }
        $this->view->techs      = $techs;
        $this->view->success    = $wo->success;
        $this->view->errors     = $wo->errors;
    }

    public function approvedAction()
    {
        $request = $this->getRequest();

        $search = new Core_Api_AdminClass;
        $this->_filters->WO_State = WO_STATE_APPROVED;

		/** Save workorders into CSV file **/
        if ( $request->getParam('download', false) === 'csv' ) {
            try {
                $csvAdapter = new Core_CSV_Adapter_Admin(
                    $this->_login,
                    $this->_password,
                    $this->_filters,
                    self::FIELDS,
                    $this->_sortStr
                );

                $csvExport = new Core_CSV_Builder('admin', 'approved', $csvAdapter);
                return $this->_outputCsv($csvExport);
            } catch (Core_CSV_Exception $e) {
                print 'error occurred';
                return false;
            }
        }
        /**  End section: Save created into CSV file  **/

        $wo = $search->getWorkOrders(
            $this->_login,
            $this->_password,
            self::FIELDS,
            '',
            '',
            '',
            $this->_filters,
            ($this->_page-1)*$this->_size,
            $this->_page*$this->_size,
            $countRows,
            $this->_sortStr
        );

        $techs = array();
        if ( $wo->success && $countRows ) {
            $_wo = range(0, $countRows-1);
            for ( $j = 0, $i = ($this->_page-1)*$this->_size; $i < $this->_page*$this->_size && $i <= $countRows-1; ++$i, ++$j ) {
                $_wo[$i] = $wo->data[$j];
                if ( !defined('FLSVersion') )   $techs[] = $_wo[$i]->Tech_ID;
                else                            $techs[] = $_wo[$i]->FLS_ID;
            }
            $wo->data = $_wo;
            unset($_wo);
        }

        $tech = new API_Tech;
        $techs = $tech->lookupID(array_unique($techs));

        if ( $wo->success ) {
            $paginator = new Zend_Paginator(new Zend_Paginator_Adapter_Array($wo->data));
            $paginator->setCurrentPageNumber($this->_page);
            $paginator->setItemCountPerPage($this->_size);
            $this->view->paginator = $paginator;
        }
        $this->view->techs      = $techs;
        $this->view->success    = $wo->success;
        $this->view->errors     = $wo->errors;
    }



    public function inaccountingAction()
    {
        $request = $this->getRequest();

        $search = new Core_Api_AdminClass;
        $this->_filters->WO_State = WO_STATE_IN_ACCOUNTING;

		/** Save workorders into CSV file **/
        if ( $request->getParam('download', false) === 'csv' ) {
            try {
                $csvAdapter = new Core_CSV_Adapter_Admin(
                    $this->_login,
                    $this->_password,
                    $this->_filters,
                    self::FIELDS,
                    $this->_sortStr
                );

                $csvExport = new Core_CSV_Builder('admin', 'inaccounting', $csvAdapter);
                return $this->_outputCsv($csvExport);
            } catch (Core_CSV_Exception $e) {
                print 'error occurred';
                return false;
            }
        }
        /**  End section: Save created into CSV file  **/

        $wo = $search->getWorkOrders(
            $this->_login,
            $this->_password,
            self::FIELDS,
            '',
            '',
            '',
            $this->_filters,
            ($this->_page-1)*$this->_size,
            $this->_page*$this->_size,
            $countRows,
            $this->_sortStr
        );

        $techs = array();
        if ( $wo->success && $countRows ) {
            $_wo = range(0, $countRows-1);
            for ( $j = 0, $i = ($this->_page-1)*$this->_size; $i < $this->_page*$this->_size && $i <= $countRows-1; ++$i, ++$j ) {
                $_wo[$i] = $wo->data[$j];
                if ( !defined('FLSVersion') )   $techs[] = $_wo[$i]->Tech_ID;
                else                            $techs[] = $_wo[$i]->FLS_ID;
            }
            $wo->data = $_wo;
            unset($_wo);
        }

        $tech = new API_Tech;
        $techs = $tech->lookupID(array_unique($techs));

        if ( $wo->success ) {
            $paginator = new Zend_Paginator(new Zend_Paginator_Adapter_Array($wo->data));
            $paginator->setCurrentPageNumber($this->_page);
            $paginator->setItemCountPerPage($this->_size);
            $this->view->paginator = $paginator;
        }
        $this->view->techs      = $techs;
        $this->view->success    = $wo->success;
        $this->view->errors     = $wo->errors;
    }


    /**
     * workdoneAction
     *
     * Render data for 'workdone' tab
     *
     * @access public
     * @return void
     */
    public function workdoneAction()
    {
        $request = $this->getRequest();
        if($request->getParam('loaddasboard', false) === '1' )
        {
            $this->view->isloaddasboard = false;
            return true;
        }
        else
        {
            $this->view->isloaddasboard = true;
        }
        
        $search = new Core_Api_AdminClass;
        $this->_filters->WO_State = WO_STATE_WORK_DONE;
        
		/** Save workorders into CSV file **/
        if ( $request->getParam('download', false) === 'csv' ) {
            try {
                $csvAdapter = new Core_CSV_Adapter_Admin(
                    $this->_login,
                    $this->_password,
                    $this->_filters,
                    self::FIELDS,
                    $this->_sortStr
                );

                $csvExport = new Core_CSV_Builder('admin', 'workdone', $csvAdapter);
                return $this->_outputCsv($csvExport);
            } catch (Core_CSV_Exception $e) {
                print 'error occurred';
                return false;
            }
        }
        /**  End section: Save created into CSV file  **/

        $wo = $search->getWorkOrders(
            $this->_login,
            $this->_password,
            self::FIELDS,
            '',
            '',
            '',
            $this->_filters,
            ($this->_page-1)*$this->_size,
            $this->_page*$this->_size,
            $countRows,
            $this->_sortStr
        );

        $techs = array();
        if ( $wo->success && $countRows ) {
            $_wo = range(0, $countRows-1);
            for ( $j = 0, $i = ($this->_page-1)*$this->_size; $i < $this->_page*$this->_size && $i <= $countRows-1; ++$i, ++$j ) {
                $_wo[$i] = $wo->data[$j];
                if ( !defined('FLSVersion') )   $techs[] = $_wo[$i]->Tech_ID;
                else                            $techs[] = $_wo[$i]->FLS_ID;
            }
            $wo->data = $_wo;
            unset($_wo);
        }

        $tech = new API_Tech;
        $techs = $tech->lookupID(array_unique($techs));

        if ( $wo->success ) {
            $paginator = new Zend_Paginator(new Zend_Paginator_Adapter_Array($wo->data));
            $paginator->setCurrentPageNumber($this->_page);
            $paginator->setItemCountPerPage($this->_size);
            $this->view->paginator = $paginator;
        }
        $this->view->techs      = $techs;
        $this->view->success    = $wo->success;
        $this->view->errors     = $wo->errors;
    }

    public function incompleteAction()
    {
        $request = $this->getRequest();

        $search = new Core_Api_AdminClass;

        $this->_filters->WO_State = WO_STATE_INCOMPLETE;


        /** Save workorders into CSV file **/
        if ( $request->getParam('download', false) === 'csv' ) {
            try {
                $csvAdapter = new Core_CSV_Adapter_Admin(
                    $this->_login,
                    $this->_password,
                    $this->_filters,
                    self::FIELDS,
                    $this->_sortStr
                );

                $csvExport = new Core_CSV_Builder('admin', 'incomplete', $csvAdapter);
                return $this->_outputCsv($csvExport);
            } catch (Core_CSV_Exception $e) {
                print 'error occurred';
                return false;
            }
        }
        /**  End section: Save created into CSV file  **/

        $wo = $search->getWorkOrders(
            $this->_login,
            $this->_password,
            self::FIELDS,
            '',
            '',
            '',
            $this->_filters,
            ($this->_page-1)*$this->_size,
            $this->_page*$this->_size,
            $countRows,
            $this->_sortStr
        );

        $techs = array();
        if ( $wo->success && $countRows ) {
            $_wo = range(0, $countRows-1);
            for ( $j = 0, $i = ($this->_page-1)*$this->_size; $i < $this->_page*$this->_size && $i <= $countRows-1; ++$i, ++$j ) {
                $_wo[$i] = $wo->data[$j];
                if ( !defined('FLSVersion') )   $techs[] = $_wo[$i]->Tech_ID;
                else                            $techs[] = $_wo[$i]->FLS_ID;
            }
            $wo->data = $_wo;
            unset($_wo);
        }

        $tech = new API_Tech;
        $techs = $tech->lookupID(array_unique($techs));

        if ( $wo->success ) {
            $paginator = new Zend_Paginator(new Zend_Paginator_Adapter_Array($wo->data));
            $paginator->setCurrentPageNumber($this->_page);
            $paginator->setItemCountPerPage($this->_size);
            $this->view->paginator = $paginator;
        }
        $this->view->techs      = $techs;
        $this->view->success    = $wo->success;
        $this->view->errors     = $wo->errors;
    }

    public function completedAction()
    {
        $request = $this->getRequest();
        /* Completed state */
        $this->_filters->WO_State = 'completed';
//        $this->_filters->Approved          = 1;
//        $this->_filters->Deactivated       = 0;
        /* Completed state */

        $search = new Core_Api_AdminClass;

        /** Save workorders into CSV file **/
        if ( $request->getParam('download', false) === 'csv' ) {
            try {
                $csvAdapter = new Core_CSV_Adapter_Admin(
                    $this->_login,
                    $this->_password,
                    $this->_filters,
                    self::FIELDS,
                    $this->_sortStr
                );

                $csvExport = new Core_CSV_Builder('admin', 'completed', $csvAdapter);
                return $this->_outputCsv($csvExport);
            } catch (Core_CSV_Exception $e) {
                print 'error occurred';
                return false;
            }
        }
        /**  End section: Save created into CSV file  **/

        $wo = $search->getWorkOrders(
            $this->_login,
            $this->_password,
            self::FIELDS,
            '',
            '',
            '',
            $this->_filters,
            ($this->_page-1)*$this->_size,
            $this->_page*$this->_size,
            $countRows,
            $this->_sortStr
        );

        $techs = array();
        if ( $wo->success && $countRows ) {
            $_wo = range(0, $countRows-1);
            for ( $j = 0, $i = ($this->_page-1)*$this->_size; $i < $this->_page*$this->_size && $i <= $countRows-1; ++$i, ++$j ) {
                $_wo[$i] = $wo->data[$j];
                if ( !defined('FLSVersion') )   $techs[] = $_wo[$i]->Tech_ID;
                else                            $techs[] = $_wo[$i]->FLS_ID;
            }
            $wo->data = $_wo;
            unset($_wo);
        }

        $tech = new API_Tech;
        $techs = $tech->lookupID(array_unique($techs));

        if ( $wo->success ) {
            $paginator = new Zend_Paginator(new Zend_Paginator_Adapter_Array($wo->data));
            $paginator->setCurrentPageNumber($this->_page);
            $paginator->setItemCountPerPage($this->_size);
            $this->view->paginator = $paginator;
        }
        $this->view->techs      = $techs;
        $this->view->success    = $wo->success;
        $this->view->errors     = $wo->errors;
    }

    public function allAction()
    {
    	$request = $this->getRequest();
       
        $search = new Core_Api_AdminClass();
        $WoTagClass = new  Core_Api_WoTagClass();
        
		$rules = $search->getPricingRules($this->_login, $this->_password);

        $pricingRules = array();

        foreach ($rules->data as $rule) {
            $pricingRules[$rule->PricingRuleId] = $rule->Description;
        }
		
		$this->view->pricingRules = $pricingRules;
		
		//--- for client-tags condition 13622
        $clientStatusID = $this->_filters->Client_Status;
        $clientRelationshipID = $this->_filters->Client_Relationship;
        $segmentID = $this->_filters->Segment;
        $originalSellerID = $this->_filters->Original_Seller;
        $executiveID = $this->_filters->Executive;
        $systemStatusID = $this->_filters->System_Status;
                
        $companyIDArray = $WoTagClass->getCompanyArray_ByClientTagsInfo($clientStatusID,
            $clientRelationshipID,$segmentID,$originalSellerID,
            $executiveID,$systemStatusID
        );
        
        if(!empty($companyIDArray) && count($companyIDArray) > 0)
        {
            $this->_filters->InCompanyIDArray = $companyIDArray;
        }
        // 13760  
        $pay_ISO = $this->_filters->Pay_ISO;
        if(!empty($pay_ISO))
                {
                    $api = new Core_Api_WoTagClass;
                    $pay_ISOList=$api->getTechList_HasPayISO();
                    $this->_filters->Pay_ISO = $pay_ISOList;           
                }
               
        //--- for project/win-tags condition 13622
        if( $this->_filters->CSD != ""
            || $this->_filters->AM != ""
            || $this->_filters->Entity != ""
            || $this->_filters->Service_Type != ""
        )
        {
            $winext = $WoTagClass->getWinArray_ByTagsInfo(
                    $this->_filters->CSD,
                    $this->_filters->AM,
                    $this->_filters->Entity,
                    $this->_filters->Service_Type);
                    

            if(is_array($winext)){
                if(!empty($this->_filters->TB_UNID) && is_array($this->_filters->TB_UNID )){
                    $this->_filters->TB_UNID = array_merge($winext,$this->_filters->TB_UNID);
                }else{
                    if(count($winext) <= 0){
                        $this->_filters->TB_UNID = array("-1");
                    }else{
                        $this->_filters->TB_UNID = $winext;
                    }
                }
            }else{
                $this->_filters->TB_UNID = array("-1");
            }
                     
        }
        //--- for ISO tags condition 13622
//        if($this->_filters->ISO_ID != ""
//            || $this->_filters->Pay_ISO != ""
//            || $this->_filters->ISO_Company_Name != "")
//        {
//          $winext = $WoTagClass->getWinArray_ByISOInfo($this->_filters->Pay_ISO,$this->_filters->ISO_ID,$this->_filters->ISO_Company_Name);
////
//           if(is_array($winext)){
//                if(!empty($this->_filters->TB_UNID) && is_array($this->_filters->TB_UNID )){
//                    $this->_filters->TB_UNID = array_merge($winext,$this->_filters->TB_UNID);
//                }else{
//                    if(count($winext) <= 0){
//                        $this->_filters->TB_UNID = array("-1");
//                    }else{
//                        $this->_filters->TB_UNID = $winext;
//                    }
//                }
//            }else{
//                $this->_filters->TB_UNID = array("-1");
//            }
//        }
        
        if ( !$request->getParam('full_seach', false) ) {
            //  Do not use this filter for full search mode
            $this->_filters->WO_State = WO_STATE_ALL_ACTIVE;
        }

        /** Save workorders into CSV file **/
        if ( $request->getParam('download', false) === 'csv' ) {

            $typeImport = $request->getParam('version', 'all');

            try {
                $csvAdapter = new Core_CSV_Adapter_Admin(
                    $this->_login,
                    $this->_password,
                    $this->_filters,
                    self::FIELDS,
                    $this->_sortStr
                );

                $csvExport = new Core_CSV_Builder('admin', $typeImport, $csvAdapter);
                return $this->_outputCsv($csvExport);
            } catch (Core_CSV_Exception $e) {
                print 'error occurred';
                return false;
            }
        }
        /**  End section: Save created into CSV file  **/   
     
        $wo = $search->getWorkOrders(
            $this->_login,
            $this->_password,
            self::FIELDS,
            '',
            '',
            '',
            $this->_filters,
            ($this->_page-1)*$this->_size,
            $this->_page*$this->_size,
            $countRows,
            $this->_sortStr
        );

        $techs = array();
        if ( $wo->success && $countRows ) {
            $_wo = range(0, $countRows-1);
            for ( $j = 0, $i = ($this->_page-1)*$this->_size; $i < $this->_page*$this->_size && $i <= $countRows-1; ++$i, ++$j ) {
                $_wo[$i] = $wo->data[$j];
                if ( !defined('FLSVersion') )   $techs[] = $_wo[$i]->Tech_ID;
                else                            $techs[] = $_wo[$i]->FLS_ID;
                $woTagsInfo = $search->getWorkOrderTagsInfo($_wo[$i]->WIN_NUM);
                if(!empty($woTagsInfo))
                {
                    foreach($woTagsInfo as $k=>$v)
                    {
                        if($k!='WINNUM') $_wo[$i]->$k = $v;
            }
                }
            }
            $wo->data = $_wo;
            unset($_wo);
        }
 
        $tech = new API_Tech;
        $techs = $tech->lookupID(array_unique($techs));

        if ( $wo->success ) {
            $paginator = new Zend_Paginator(new Zend_Paginator_Adapter_Array($wo->data));
            $paginator->setCurrentPageNumber($this->_page);
            $paginator->setItemCountPerPage($this->_size);
            $this->view->paginator = $paginator;
        }

        $this->view->techs      = $techs;
        $this->view->success    = $wo->success;
        $this->view->errors     = $wo->errors;
    }

    public function deactivatedAction()
    {
        $search = new Core_Api_AdminClass;

        $request = $this->getRequest();
        $this->_filters->WO_State = WO_STATE_DEACTIVATED;

        /** Save workorders into CSV file **/
        if ( $request->getParam('download', false) === 'csv' ) {
            try {
                $csvAdapter = new Core_CSV_Adapter_Admin(
                    $this->_login,
                    $this->_password,
                    $this->_filters,
                    self::FIELDS,
                    $this->_sortStr
                );

                $csvExport = new Core_CSV_Builder('admin', 'deactivated', $csvAdapter);
                return $this->_outputCsv($csvExport);
            } catch (Core_CSV_Exception $e) {
                print 'error occurred';
                return false;
            }
        }
        /**  End section: Save created into CSV file  **/

        $wo = $search->getWorkOrders(
            $this->_login,
            $this->_password,
            self::FIELDS,
            '',
            '',
            '',
            $this->_filters,
            ($this->_page-1)*$this->_size,
            $this->_page*$this->_size,
            $countRows,
            $this->_sortStr
        );

        $techs = array();
        if ( $wo->success && $countRows ) {
            $_wo = range(0, $countRows-1);
            for ( $j = 0, $i = ($this->_page-1)*$this->_size; $i < $this->_page*$this->_size && $i <= $countRows-1; ++$i, ++$j ) {
                $_wo[$i] = $wo->data[$j];
                if ( !defined('FLSVersion') )   $techs[] = $_wo[$i]->Tech_ID;
                else                            $techs[] = $_wo[$i]->FLS_ID;
            }
            $wo->data = $_wo;
            unset($_wo);
        }

        $tech = new API_Tech;
        $techs = $tech->lookupID(array_unique($techs));

        if ( $wo->success ) {
            $paginator = new Zend_Paginator(new Zend_Paginator_Adapter_Array($wo->data));
            $paginator->setCurrentPageNumber($this->_page);
            $paginator->setItemCountPerPage($this->_size);
            $this->view->paginator = $paginator;
        }

        $this->view->techs      = $techs;
        $this->view->success    = $wo->success;
        $this->view->errors     = $wo->errors;
    }

    private function _outputCsv(Core_CSV_Builder $csvBuilder)
    {
        $this->_helper->viewRenderer->setNoRender();
        $response = $this->getResponse();
        $response->clearHeaders();
        $response->clearBody();
        $response->setHeader('Pragma', 'public')
                ->setHeader('Expires', '0')
                ->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0')
                ->setHeader('Cache-Control', 'private')
                ->setHeader('Content-Type', 'text/csv')
                ->setHeader('Content-Disposition', 'attachment; filename='.$this->_company.'-'.ucfirst($csvBuilder->getTabName()).'-'.date('mdY').'.csv', true)
                ->setHeader('Content-Transfer-Encoding', 'binary');
        $response->sendResponse();

        //Flush buffers
        ob_end_flush();
        flush();

        $csvBuilder->printCSV();
        exit;
    }

}

