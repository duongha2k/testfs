<?php
//ini_set("error_log", "/home/p3559/error.log");
class Admindashboard_DetailsController extends Core_Admin_Controller_Action
{
    public function init()
    {
        parent::init();

        $request = $this->getRequest();

        //  This is constroller can be used for AJAX only!
        if ( !$this->getRequest()->isXmlHttpRequest() ) {
            //$this->_redirect('/');
        }

        $this->_sortStr = $this->_assembleSortStr(array(
            's1' => $request->getParam('sort1', NULL), 'd1' => $request->getParam('dir1', NULL),
            's2' => $request->getParam('sort2', NULL), 'd2' => $request->getParam('dir2', NULL),
            's3' => $request->getParam('sort3', NULL), 'd3' => $request->getParam('dir3', NULL)
        ));

        $auth = new Zend_Session_Namespace('Auth_User');
        $this->view->isPM = $auth->isPM;
        $this->view->name = $auth->name;
        $this->view->company = $request->getParam('company',NULL);

    }

//    public function listfilesAction()
//    {
//        echo Core_Caspio::caspioListFiles(1630);
//        exit;
//    }

    public function navigationAction()
    {
        $request    = $this->getRequest();
        $win        = $request->getParam('id', NULL);
        //$tab        = $request->getParam('tab', NULL);
        $search = new Core_Api_Class;
        $filters = new API_WorkOrderFilter;
        $company = $this->view->company;

        $result = $search->getWorkOrders($this->_login."|pmContext=$company", $this->_password,'TB_UNID,Status',NULL,NULL,NULL,$filters);

        $this->view->counters = array('total' => count($result->data));

        /* initializing $this->view->counters */
        $constants = get_defined_constants(true);
        $constants = $constants['user'];
        foreach ( $constants as $key => $val ) {
            if ( strpos($key, 'WO_STATE_', 0) === 0 && is_string($val) && !empty($val) ) {
                $this->view->counters[$val] = 0;
            }
        }
        /* initializing $this->view->counters */

        foreach($result->data as $wo) {
            ++$this->view->counters[$wo->Status];
        }

        $authData = array('login' => $this->_login.'|pmContext='.$company, 'password' => $this->_password);
        $client = new Core_Api_User;
        $client->checkAuthentication($authData);

        $this->view->client     = $client;
        if ($win) {
            $this->view->wo         = $search->getWorkOrder($this->_login.'|pmContext='.$company, $this->_password, $win);
        }
        $this->view->company    = $company;
        $this->view->win        = $win;
    }

    private function createOpenTabs($tab) {
        $this->view->opentabs = array();

        $tab_options = array('created'=>array(0,1,2,5,8),
            'published'=>array(),
            'assigned'=>array(),
            'workdone'=>array(),
            'incomplete'=>array(),
            'completed'=>array(),
            'all'=>array(),
            'deactivated'=>array(),
            'full'=>array(),
            'new'=>array(0,1,2,4));
        if (isset($tab_options[$tab])){
            $this->view->opentabs = $tab_options[$tab];
        }
    }



    public function indexAction()
    {

        $request = $this->getRequest();
        $companyId = $this->view->company;

        $this->createOpenTabs($request->getParam('tab',null));


        $authData = array('login' => $this->_login, 'password' => $this->_password);
        $client = new Core_Api_AdminUser;
        $client->checkAuthentication($authData);
        $this->view->client     = $client;

        $search = new Core_Api_AdminClass;
        $winnum = $request->getParam('win', null);
        if(!empty($winnum))
        {
            $projectclass = new Core_Api_ProjectClass();
            $projectclass->updateServiceTypeWO_FromItsProject($winnum);
        }
        // wo data
        $wo = $search->getWorkOrder(
            $this->_login,
            $this->_password,
            $request->getParam('win', null)
        );
        $this->view->wo      = $wo->data[0];
        $this->view->success = $wo->success;
        $this->view->errors  = $wo->errors;
        

        if(!$wo->success || !$wo->data[0]) return;
        $wo = $wo->data[0];

        $cache = $this->getInvokeArg('bootstrap')->getResource('Cache');

        // projects list
        $cacheKeyPrj = Core_Cache_Manager::makeKey('projectsadmin', $wo->Company_ID);
        if (1 || FALSE == ($projects = $cache->load($cacheKeyPrj)) ) {
            $projects = $search->getAllProjects(
                $this->_login,
                $this->_password,
                $wo->Company_ID
            );
            if ( $projects->success ) {
                $cache->save($projects, $cacheKeyPrj, array('prj'), LIFETIME_15MIN);
            }
        }

        /*if ( !empty($projects->data) ) {
            usort($projects->data, 'Core_Controller_Action::_cmpProjects');
        }*/
        $this->view->projectsSuccess = $projects->success;
        if ($projects->success)
            $this->view->projects = $projects->data;

        // deactivation codes list
        $reasons = $search->getDeactivationCodes();
        if ($reasons->success)
            $this->view->deactivationCodes = $reasons->data;

        // penalty reasons list
        $reasons = $search->getPenaltyReasons();
        if ($reasons->success)
            $this->view->penaltyReasons = $reasons->data;

        // call types list
        $callTypes = $search->getCallTypes();
        $this->view->callTypesSuccess = $callTypes->success;
        if ($callTypes->success)
            $this->view->callTypes = $callTypes->data;
// country list
		$countries = $search->getCountries();
        $this->view->countriesSuccess = $countries->success;
        if ($countries->success)
            $this->view->countries = $countries->data;

        // states list
        $states = $search->getStatesArray($this->view->wo->Country);
        $this->view->statesSuccess = $states->success;
        if ($states->success)
            $this->view->states = $states->data;
//        // states list
//        $states = $search->getStatesArray();
//        $this->view->statesSuccess = $states->success;
//        if ($states->success)
//            $this->view->states = $states->data;

        // categories list
        $categories = $search->getWOCategories($this->_login, $this->_password);
        $this->view->categoriesSuccess = $categories->success;
        if ($categories->success)
            $this->view->categories = $categories->data;

        $this->view->project_info = null;
        if (is_array($projects) ) {
            foreach ($projects as $project) {
                if ($project->Project_ID == $wo->Project_ID) {
                    $this->view->project_info = $project;
                    break;
                }
            }
        }

        // prepare js pricing info
        $pricingRules = $search->getPricingRulesDescriptionOrder($this->_login, $this->_password);
        if ($pricingRules->success == true) {
            $this->view->pricingRules = $pricingRules->data;
        } else {
            $this->view->pricingRules = array();
        }
        
        $files = new Core_Files();
        $select = $files->select();
        $select->where('WIN_NUM = ?', $request->getParam('win'));
        $filesList = $files->fetchAll($select);

        $this->view->files = $filesList;
        $this->view->isdisable = true;
        
        $Wosapi = new  Core_Api_WosClass();
        $this->view->EnableColspanTechPay = $Wosapi->enableExpenseReporting($request->getParam('win'));
        
        if($this->view->EnableColspanTechPay)
        {
            $this->view->WosTurnedOnBCatsinfo = $Wosapi->getWorkOrderTurnedOnBCats_insertFromProjectIfNotExists($request->getParam('win'));
        }
        
        //---  num of visits
        //$numOfVisits = $Wosapi->getNumOfVisits($request->getParam('win'));
		$numOfVisits = $wo->Qty_Visits;
        $this->view->numOfVisits = $numOfVisits;
        $WoTagClass = new Core_Api_WoTagClass(); 
        $this->view->EntityTypeList = $WoTagClass->getEntityTypeList();
        //14112                 
        $this->view->ISOInformation = "";    
        if (!empty($wo->Tech_ID)) {
            $Core_Api_TechClass = new Core_Api_TechClass;
            $resultISO = $Core_Api_TechClass->getTechInfo_ForAPIUsing($wo->Tech_ID);
            if (!empty($resultISO['ISO_Affiliation_ID'])&& $resultISO['ISO_Affiliation_ID'] > 0) {             
                $ISO_ID = $resultISO['ISO_Affiliation_ID'];
                $ISO_Name = $resultISO['ISO_Affiliation'];  
                $this->view->ISOInformation = $ISO_Name."(ISO ID# ".$ISO_ID.")";            
            } 
        }
    }



    public function updateAction()
    {
        $this->_helper->viewRenderer->setNoRender();

        $params = $this->getRequest()->getParams();

        $search = new Core_Api_AdminClass;

        $return = array('success'=>0);

        $wo = $search->getWorkOrder(
            $this->_login,
            $this->_password,
            $params['WIN_NUM']
        );
        if ($wo->success && !empty($wo->data[0]->WIN_NUM)) {
            $wo = $wo->data[0];
            $className = get_class($wo);

            $currentProject = $wo->Project_ID;
            $OldQty_Visits = $wo->Qty_Visits;
            $oldInvoiced = $wo->Invoiced; // 13622
            foreach ($params as $key => $value) {
                if(property_exists($className, $key)) $wo->$key = $value;
            }

            if (!empty($params["Project_ID"]) && $currentProject != $params["Project_ID"])
                unset($wo->Project_Name);

            //Special handler for checkboxes.
            $checkboxes = array('ShowTechs','Update_Requested','Lead','Assist','SignOffSheet','AutoBlastOnPublish','SMSBlast','ReminderAcceptance','Reminder24Hr','Reminder1Hr','CheckInCall','CheckOutCall','ReminderNotMarkComplete','ReminderIncomplete','PcntDeduct','WorkOrderReviewed','CheckedIn_24hrs','StoreNotified','Paperwork_Received','Incomplete_Paperwork','TechMarkedComplete','Site_Complete','CallClosed','AbortFee','Approved', 'TechPaid', 'Invoiced','PricingRan','Deactivated');
            foreach ($checkboxes as $c) {
                if (!property_exists($className, $c) || isset($params[$c]))
                    continue;

                //Checkbox wasn't checked.
                $wo->$c = false;
            }

            //Special handlers for virtual fields
            if (isset($params['MarkIncompleteV'])) {
                $wo->TechMarkedComplete = false;
            }
            if (isset($params['Route'])) {
                $wo->Route = $params['Route'];
            }
            
            //--- 13541 for QtyVisits
            if (isset($params['Qty_Devices_7'])) {
                if(!empty($params['Qty_Devices_7']))
                {
                    $wo->Qty_Devices = $params['Qty_Devices_7'];    
                }                
            }
            
            //--- call update function
            $apiResponse = $search->updateWorkOrder($this->_login, $this->_password, $wo->WIN_NUM, $wo);

            if (empty($apiResponse->errors)) {
                
                $status = 0;
                if (!empty($params['PreferTechV'])) {
                    $status = 1;
                }elseif(!empty($params['DenyTechV'])) {
                    $status = 2;
                }

                // calculate invoice
                if (!empty($params['RunPricingV']) && $wo->Approved) {
                    require_once realpath(APPLICATION_PATH . '/../../htdocs/library') . '/pricingNew.php';
                    runPricingCalculation($wo->WIN_NUM);
                }
                if($OldQty_Visits < $params['Qty_Visits']){
                    $Core_Api_WosClass = new Core_Api_WosClass;

                    for($i = 1; $i <= ((int)$params['Qty_Visits'] - (int)$OldQty_Visits);$i++){
                        $Core_Api_WosClass->saveWOVisit(
                                            0,
                                            $wo->WIN_NUM,
                                            1,
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "");
                    }
                }
                //-- 13622
                $updateInvoiced = $wo->Invoiced;
                if($updateInvoiced==true && $oldInvoiced==false)
                {
                    //-- update AM of wo from AM of company
                    $WoTagClass = new Core_Api_WoTagClass();
                    $WoTagClass->updateAM_ofWO_fromAM_ofCompany($wo->WIN_NUM,$wo->Company_ID);
                }
                //-- end 13622
                $return['success'] = 1;

            } else {
                $return['success'] = 0;
                $return['errors'] = array();
                foreach ($apiResponse->errors as $e) {
                    $return['errors'][]=$e->message;
                }
            }
        $ProjectClass = new Core_Api_ProjectClass();
        $ProjectClass->updateServiceTypeWO_FromServiceTypeProject($wo->WIN_NUM,$currentProject);
        } else {
            $return['success'] = 0;
            $return['errors'] = array('There is no such Work Order');
            foreach ($apiResponse->errors as $e) {
                $return['errors'][]=$e->message;
            }
        }
        $response = $this->getResponse();
        $response->clearAllHeaders()->clearBody();
        $return = json_encode($return);

        $response->setHeader('Content-type', 'application/json');
        $response->setHeader('Content-Length', strlen($return), true)
                 ->setBody($return);
    }

    public function ajaxUploadFileAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);

        $maxFileSize = 52428800; //50Mb

        $request    = $this->getRequest();

        $win = $request->getParam('win', NULL);
        $id  = $request->getParam('file', NULL);

        $return = array('success'=>0);
        if (
            !empty($id)                                                 &&
            !empty($_FILES["Pic{$id}"])                        &&
            is_readable($_FILES["Pic{$id}"]['tmp_name'])       &&
            $_FILES["Pic{$id}"]['error'] == UPLOAD_ERR_OK      &&
            filesize($_FILES["Pic{$id}"]['tmp_name']) <= $maxFileSize
        ) {
            $uploader = new Core_Api_AdmninClass();

            $saveField = 'Pic'. $id;
            $fname     = $_FILES[$saveField]['name'];
            if ( strrpos($fname, '/') !== FALSE ) {
                $fname = substr($fname, strrpos($fname, '/'));
            }
            //$fdata     = base64_encode(file_get_contents($_FILES[$saveField]['tmp_name']));
            $fdata     = file_get_contents($_FILES[$saveField]['tmp_name']);
            $fmime     = $_FILES[$saveField]['type'];

            $result = $uploader->uploadFile($this->_login, $this->_password, $win, $fname, $fdata, $fmime, $saveField);

            if ( !empty($result->errors) ) {
                $return['success'] = 0;
                $return['errors'] = array();
                foreach ($result->errors as $e) {
                    $return['errors'][]=$e->message;
                }
            } else {
                $return['success']=1;
            }
        } else {
            $return['success']=0;
            $return['errors']=array();
            if ( empty($id) ) {
                $return['errors'][]='Incorrect parameters: ID is required and cannot be empty';
            } else if ( empty($_FILES["Pic{$id}"]) ) {
                $return['errors'][]= 'File not uploaded';
            } else if ( !is_readable($_FILES["Pic{$id}"]['tmp_name']) ) {
                $return['errors'][]= 'Uploaded file not readabale';
            } else if ( filesize($_FILES["Pic{$id}"]['tmp_name']) > $maxFileSize ) {
                $return['errors'][]= 'Uploaded file size too big';
            } else if ( $_FILES["Pic{$id}"]['error'] != UPLOAD_ERR_OK ) {
                switch ( $_FILES["Pic{$id}"]['error'] ) {
                    case UPLOAD_ERR_FORM_SIZE :
                        $return['errors'][]= 'Uploaded file size too big';
                        break;
                    case UPLOAD_ERR_PARTIAL :
                        $return['errors'][]= 'The uploaded file was only partially uploaded';
                        break;
                    case UPLOAD_ERR_NO_FILE :
                    case UPLOAD_ERR_NO_TMP_DIR :
                    case UPLOAD_ERR_CANT_WRITE :
                    case UPLOAD_ERR_EXTENSION :
                        $return['errors'][]= 'No file was uploaded';
                }
            }
        }
        $response = $this->getResponse();
        $response->clearHeaders()->clearBody();
        $return = json_encode($return);

        $response->setHeader('Content-Length', strlen($return), true);
        $response->setBody($return);
    }

}

