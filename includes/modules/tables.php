<?php

define('TABLE_WORK_ORDER_TIMESTAMPS', 'Work_Order_TimeStamps');
define('TABLE_WORK_ORDERS', 'Work_Orders');
define('TABLE_CLIENT_LIST', 'TR_Client_List');

define('TABLE_DEACTIVATION_CODES', 'Deactivation_Codes');
define('TABLE_WO_CATEGORIES', 'WO_Categories');
define('TABLE_MASTER_LIST', 'TR_Master_List');
define('TABLE_MASTER_LIST_CABLING', 'Tech_Cabling_Skills_View');
define('TABLE_MASTER_LIST_TELEPHONY', 'Tech_Telephony_Skills_View');

define('TABLE_CLIENT_PROJECTS', 'TR_Client_Projects');
define('TABLE_STATES_LIST', 'States_List');
define('TABLE_SHIPPING_CARRIERS', 'Shipping_Carriers');
define('TABLE_CALL_TYPE', 'Call_Type');

define('TABLE_ADMIN_LIST', 'TR_Admin');

define('TABLE_PART_ENTRIES', 'Part_Entries');
define('TABLE_PARTS', 'Parts');

define('TABLE_FLS_USERS_ADMIN', 'FLS_Admin');
define('TABLE_FLS_PROJECTS', 'FLS_Projects');
define('TABLE_FLS_WORK_ORDERS', 'FLS_Work_Orders');
define('TABLE_FLS_WORK_ORDER_TIMESTAMPS', 'FLS_Work_Order_TimeStamps');

define('TABLE_ADMINS', 'TR_Admin');
define('TABLE_CUSTOMERS', 'customers');
define('TABLE_STAFF', 'TR_Staff');
define('TABLE_ISO', 'ISO');
define('TABLE_CLIENT_DENIED_TECHS', 'Deactivated_Techs_By_Clients');
define('TABLE_CLIENT_PREFERRED_TECHS', 'Client_Preferred_Techs');

define('TABLE_PRICING_RULES', 'Pricing_Rules');
