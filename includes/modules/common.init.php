<?php

// Define path to application directory
defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../../wdata/application'));

defined('DOCUMENT_INCLUDES')
	|| define('DOCUMENT_INCLUDES', realpath(dirname(__FILE__) . '/..'));

// Define application environment
defined('APPLICATION_ENV')
    || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));

// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
    realpath(APPLICATION_PATH . '/../library'),
    realpath(APPLICATION_PATH . '/../../includes/classes'),
    get_include_path(),
)));


/** Zend_Application */
require_once 'Zend/Application.php';

// Create application, bootstrap, and run
$application = new Zend_Application(
    APPLICATION_ENV,
    APPLICATION_PATH . '/configs/application.ini'
);


$autoloader = Zend_Loader_Autoloader::getInstance();
$autoloader->registerNamespace('Core_');
$autoloader->registerNamespace('API_');
$autoloader->registerNamespace('Salesforce_');
$autoloader->registerNamespace('PHPExcel_');
$autoloader->registerNamespace('WorkMarket');
$autoloader->registerNamespace('Browscap');
$application->bootstrap(array('Cache', 'Caspio', 'Configs', 'Defines'));

