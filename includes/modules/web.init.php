<?php
require_once dirname(__file__) . '/common.init.php';

@define('BASE_URL', $_SERVER['HTTP_HOST'].dirname($_SERVER['SCRIPT_NAME']));
define('HTTP_URL', 'http://'.BASE_URL);
define('HTTPS_URL', 'https://'.BASE_URL);

//$phpSessionHandler = new Core_PHPSessionHandler();
