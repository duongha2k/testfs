<?php
// init evironment
require_once dirname(__FILE__).'/../modules/common.init.php';

//open file for logs
$logger = (file_exists(dirname(__FILE__).'/migrationLogger.txt')) ? fopen(dirname(__FILE__).'/migrationLogger.txt', 'a+') :
          fopen(dirname(__FILE__).'/migrationLogger.txt', 'w');
//open file for moved files names
$wosFiles = (file_exists(dirname(__FILE__).'/wosFiles.txt')) ? fopen(dirname(__FILE__).'/wosFiles.txt', 'a+') :
          fopen(dirname(__FILE__).'/wosFiles.txt', 'w');
$errors = Core_Api_Error::getInstance(); //Caspio error hendler

// signals handler
function sig_handler($signo) {
    switch($signo) {
        case SIGUSR1:
            $f =fopen(dirname(__FILE__).'/migrationLogger.txt', 'a+');
            fwrite($f, '\r\n Finished by user SIGUSR1');
            fclose($f);
            exit(); // quit after user signal
            break;
        default:
            break;
    }
}

$fieldList = API_WorkOrder::uploadFileFields(); // fields with docs names
$fieldList[] = 'WIN_NUM';

$pid = getmypid(); // id of current process
file_put_contents(dirname(__FILE__).'/fileMigrPID.txt', $pid); // put PID in file

// get WO
$db = Core_Database::getInstance();
$select = $db->select();
$select->from(Core_Database::TABLE_WORK_ORDERS, $fieldList)
       ->forUpdate()
       ->where('is_relocated= ?', 0)
       ->where('WIN_NUM <= 50000')
       ->limit(1, 0);
$result = $db->fetchRow($select);

if (!empty($result)){
    $win = $result['WIN_NUM']+0;
    $flag = true; // is WO processed completely
    fwrite($logger, "\r\n [".date('r')."] -- Start process wo #$win");
    // proccess each file from WO
    foreach ($result as $f=>$file_name) {
        $file_name = trim($file_name);
        if (empty ($file_name) || $f=='WIN_NUM' || $file_name=='NULL') continue;
        $caspioFileData = Core_Caspio::caspioDownloadFile( $file_name ); // get file from Caspio
        if(Core_File::fileExists($file_name, S3_CLIENTS_DOCS_DIR) == true){
            fwrite($logger, "\r\n [".date('r')."] -- File $file_name has already uploaded on AWS");
            continue;
        }
        
        $caspioErrors = $errors->getErrors();
        if (!empty($caspioErrors)){
            foreach ($caspioErrors as $ce) fwrite($logger, $ce);
        }
        // put file on S3
        if(!Core_File::uploadFile( $file_name , $caspioFileData, S3_CLIENTS_DOCS_DIR )){
            $flag = false;
            fwrite($logger, "\r\n [".date('r')."] -- File $file_name has not uploaded on AWS");
        } else {
        
            fwrite($logger, "\r\n [".date('r')."] -- File $file_name has uploaded on AWS");
            fwrite($wosFiles, "\r\n $file_name");
        }
    }
    if ($flag) {
        //update is_relocated flag
        $db->update(Core_Database::TABLE_WORK_ORDERS, array('is_relocated'=>1), "WIN_NUM = '$win'");
        fwrite($logger, "\r\n [".date('r')."] -- WO #$win successfuly processed");

    } else {
        fwrite($logger,"\r\n [".date('r')."] -- WO #$win didnt marked as relocated");
    }
} else {
    fwrite($logger, "\r\n [".date('r')."] -- Finished!!!");
    exit;
}

// init signals
declare(ticks = 1);
if(function_exists('pcntl_signal_dispatch'))
    register_tick_function('pcntl_signal_dispatch');
pcntl_signal(SIGUSR1, "sig_handler");

fclose($logger); //cloce logger file
fclose($wosFiles); //cloce logger file

//create AT jobusleep(10);
$command = 'at -f '.dirname(__FILE__).'/runScript.txt now';
shell_exec($command);

exit;
?>