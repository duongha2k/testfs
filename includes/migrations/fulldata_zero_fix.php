<?php
// init evironment
require_once dirname(__FILE__).'/../modules/common.init.php';
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 'On');


$fstr = file_get_contents('zero_fix_sizeO.txt');
$fstr = trim($fstr, '|');
$fstr = explode('|', $fstr);
$fstr = array_unique($fstr);
$fstr = array_diff($fstr, array(''));


$fieldsToList = array('WIN_NUM', 'StartDate', 'Company_ID', 'Company_Name', 'Tech_ID', 'Tech_FName', 'Tech_LName');
$picFieldList = API_WorkOrder::uploadFileFields(); // fields with docs names
$fieldList = array_merge($fieldsToList, $picFieldList);

$db = Zend_Db::factory('MYSQLI', array(
    'host'       => 'fieldsolutionsdb.cd3kywrhh0ei.us-east-1.rds.amazonaws.com',
    'username'   => 'fsolutions',
    'password'   => 'volume55nourish',
    'dbname'     => 'gbailey_technicianbureau'
));
Zend_Db_Table::setDefaultAdapter($db);


$db_files = new Core_Files();
$db_files_select = $db_files->select()
        ->where('id IN(?)', $fstr);
$db_files_res = $db_files->fetchAll($db_files_select);
$db_files_res = $db_files_res->toArray();

if(empty($db_files_res)) exit('Empty files result');

///////////////////////
$resultArray = array();
///////////////////////

$winIDs = array();
foreach($db_files_res as $fr){
    $winIDs[] = trim($fr['WIN_NUM']);

    $resultArray[$fr['id']]['id'] = $fr['id'];
    $resultArray[$fr['id']]['win'] = $fr['WIN_NUM'];
    $resultArray[$fr['id']]['path'] = $fr['path'];
}

if(empty($winIDs)) exit('Empty wos result');


$db = new Core_WorkOrders();
$select = $db->select();
$select->from(Core_Database::TABLE_WORK_ORDERS, $fieldList)
       ->where('WIN_NUM in(?)', $winIDs);
$wos = $db->fetchAll($select);

$wosRes = array();
foreach ($wos as $wo){
    foreach($fieldList as $wo_field){
         $wosRes[$wo['WIN_NUM']][$wo_field] = $wo[$wo_field];
    }
}


$info = (file_exists(dirname(__FILE__).'/fullinfo.csv')) ? fopen(dirname(__FILE__).'/fullinfo.csv', 'a+') :
          fopen(dirname(__FILE__).'/fullinfo.csv', 'w');
fwrite($info, "FileID,WIN_NUM,FileName,Field,StartDate,CompanyID,CompanyName,TechID,TechFName,TechLName \n");


foreach($resultArray as &$r){
    $r['field'] = 'undefined';
    foreach($picFieldList as $picf){
        if(ltrim($wosRes[$r['win']][$picf],'/') == ltrim($r['path'],'/')) {$r['field'] = $picf;}
    }

    foreach($fieldsToList as $f){
        if($f != 'WIN_NUM') $r[$f] = $wosRes[$r['win']][$f];
    }
    
    $str = putcsv($r);
    fwrite($info, $str);
}

fclose($info);

function putcsv($row, $fd=',', $quot='"')
{
   $str='';
   foreach ($row as $cell)
   {
      $cell = str_replace($quot, $quot.$quot, $cell);
      if (strchr($cell, $fd) !== FALSE || strchr($cell, $quot) !== FALSE || strchr($cell, "\n") !== FALSE)
      {
         $str .= $quot.$cell.$quot.$fd;
      }
      else
      {
         $str .= $cell.$fd;
      }
   }
   return substr($str, 0, -1)."\n";
}

exit;