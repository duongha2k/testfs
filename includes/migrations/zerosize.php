<?php
// init evironment
require_once dirname(__FILE__).'/../modules/common.init.php';
//open file for logs
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 'On');

$logger = (file_exists(dirname(__FILE__).'/zerosize_suc.txt')) ? fopen(dirname(__FILE__).'/zerosize_suc.txt', 'a+') :
          fopen(dirname(__FILE__).'/zerosize_suc.txt', 'w');
$logger1 = (file_exists(dirname(__FILE__).'/zerosize_err.txt')) ? fopen(dirname(__FILE__).'/zerosize_err.txt', 'a+') :
          fopen(dirname(__FILE__).'/zerosize_err.txt', 'w');
$s3_err = (file_exists(dirname(__FILE__).'/zerosize_s3_err.txt')) ? fopen(dirname(__FILE__).'/zerosize_s3_err.txt', 'a+') :
          fopen(dirname(__FILE__).'/zerosize_s3_err.txt', 'w');

$db = Zend_Db::factory('MYSQLI', array(
    'host'       => 'fieldsolutionsdb.cd3kywrhh0ei.us-east-1.rds.amazonaws.com',
    'username'   => 'fsolutions',
    'password'   => 'volume55nourish',
    'dbname'     => 'gbailey_technicianbureau'
));
Zend_Db_Table::setDefaultAdapter($db);


$db_files = new Core_Files();
$db_files_select = $db_files->select()
        ->from(Core_Database::TABLE_FILES, array('id','path'))
        ->where('processed = 1')
        ->limit(1, 0);
$db_files_res = $db_files->fetchRow($db_files_select);

$fff= new Core_File();

$id = $db_files_res['id'];
$path = ltrim($db_files_res['path'], '/');

try{
    $on_s3 = $fff->getInfo(S3_CLIENTS_DOCS_DIR.'/'.$path);

    if($on_s3['size'] <=0 ){

        $on_ca = Core_Caspio::caspioDownloadFile($path);
        $on_ca = trim($on_ca);
        if(empty($on_ca) || $on_ca==null || $on_ca=='NULL' || strlen($on_ca)==0 || $on_ca == ''){

            fwrite($logger1, $id.'|');

        } else {

            $r = $fff->uploadFile($path, $on_ca, S3_CLIENTS_DOCS_DIR);
            if($r != false) fwrite($logger, $id.'|');
            else fwrite($logger1, $id.'|');

        }

    }


}catch(Exception $e){
    fwrite($s3_err, $id.'|');
}

if(!empty($id)) $db_files->update(array('processed'=>0), 'id = '.$id);
//create AT
$command = 'at -f '.dirname(__FILE__).'/runzerosize.txt now';
shell_exec($command);

exit;
?>