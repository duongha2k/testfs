<?php
// init evironment
require_once dirname(__FILE__).'/../modules/common.init.php';

//open file for logs
$logger = (file_exists(dirname(__FILE__).'/migrationPrjLogger.txt')) ? fopen(dirname(__FILE__).'/migrationPrjLogger.txt', 'a+') :
          fopen(dirname(__FILE__).'/migrationPrjLogger.txt', 'w');
//open file for moved files names
$prjFiles = (file_exists(dirname(__FILE__).'/prjFiles.txt')) ? fopen(dirname(__FILE__).'/prjFiles.txt', 'a+') :
          fopen(dirname(__FILE__).'/prjFiles.txt', 'w');
$errors = Core_Api_Error::getInstance(); //Caspio error hendler

// signals handler
function sig_handler($signo) {
    switch($signo) {
        case SIGUSR1:
            $f =fopen(dirname(__FILE__).'/migrationPrjLogger.txt', 'a+');
            fwrite($f, '\r\n Finished by user');
            fclose($f);
            exit(); // quit after user signal
            break;
        default:
            break;
    }
}

$fieldList = array('Project_ID','Project_Docs','Project_Other_Files'); // fields with docs names

$pid = getmypid(); // id of current process
file_put_contents(dirname(__FILE__).'/fileMigrPrjPID.txt', $pid); // put PID in file

// get WO
$db = Core_Database::getInstance();
$select = $db->select();
$select->from(Core_Database::TABLE_PROJECTS, $fieldList)
       ->forUpdate()
       ->where('is_relocated= ?', 0)
       //->where('Project_ID = ?', 3153)
       ->limit(1, 0);
$result = $db->fetchRow($select);

if (!empty($result)) {
    $prj_id = $result['Project_ID']+0;
    $flag = true; // is Project processed completely
    fwrite($logger, "\r\n [".date('r')."] -- Start process Project #$prj_id");
    // proccess each file from WO
    foreach ($result as $f=>$file_name) {
        $file_name = trim($file_name);
        if (empty ($file_name) || $f=='Project_ID' || $file_name=='NULL') continue;
        $caspioFileData = Core_Caspio::caspioDownloadFile( $file_name ); // get file from Caspio
        $caspioErrors = $errors->getErrors();
        if (!empty($caspioErrors)){
            foreach ($caspioErrors as $ce) fwrite($logger, $ce);
        }
        // put file on S3
        if(!Core_File::uploadFile( trim($file_name, '/') , $caspioFileData, S3_PROJECTS_DOCS_DIR )){
            $flag = false;
            fwrite($logger, "\r\n [".date('r')."] -- File $file_name has not uploaded on AWS");
        } else {
            fwrite($logger, "\r\n [".date('r')."] -- File $file_name has uploaded on AWS");
            fwrite($prjFiles, "\r\n $file_name");
        }
    }
    if ($flag) {
        //update is_relocated flag
        $db->update(Core_Database::TABLE_PROJECTS, array('is_relocated'=>1), "Project_ID = '$prj_id'");
        fwrite($logger, "\r\n [".date('r')."] -- Project #$prj_id successfuly processed");
    } else {
        $err = Core_Api_Error::getInstance();
        fwrite($logger,"\r\n [".date('r')."] -- Project #$prj_id didnt marked as relocated");
    }
} else {
    fwrite($logger, "\r\n [".date('r')."] -- Finished!!!");
    exit;
}

// init signals
declare(ticks = 1);
if(function_exists('pcntl_signal_dispatch'))
    register_tick_function('pcntl_signal_dispatch');
pcntl_signal(SIGUSR1, "sig_handler");
sleep(1);

fclose($logger); //cloce logger file
fclose($file_name); //cloce logger file

//create AT jobusleep(10);
$command = 'at -f '.dirname(__FILE__).'/runPrjScript.txt now';
shell_exec($command);

exit;
?>