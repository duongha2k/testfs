<?php
// init evironment
require_once dirname(__FILE__).'/../modules/common.init.php';
require_once dirname(__FILE__).'/../../wdata/application/Bootstrap.php';

$fieldList = array('Company_Logo', 'ClientID'); // fields with docs names

// get WO
$db = Core_Database::getInstance();
$select = $db->select();
$select->from(Core_Database::TABLE_CLIENTS, $fieldList);
$select->where('Company_Logo != ?', '');
$resultAll = $db->fetchAll($select);

$uploader = new Core_File();

foreach ($resultAll as $result) {
    echo "Client: ", $result['ClientID'], "\n";
    // proccess each file from WO
    foreach ($result as $f=>$file_name) {
        $file_name = trim($file_name);
        if (empty ($file_name) || $f=='ClientID' || $file_name=='NULL') continue;
        
        if (!fileExistsS3($file_name, S3_CLIENTS_LOGO_DIR)) {
            
            if ($fileContent = fileExistsCaspio($file_name, S3_CLIENTS_LOGO_DIR) ) {
                // to upload
                echo "\tFile ", $file_name, " was uploaded ";
                if(!$uploader->uploadFile( trim($file_name, '/') , $fileContent, S3_CLIENTS_LOGO_DIR, true )){
                    echo ' error';
                } else {
                    echo ' successfuly';
                }
                echo "\n";
                
            } else {
                echo "\tFile : ", $file_name , " empty on Caspio\n";
            }
            
        } else {
            echo "\tFile : ", $file_name , " exisyts on S3\n";
        }
    }
} 


function fileExistsS3($fileNameSource,$bucketName)
{
    $fileName = trim($fileNameSource,'/');

    try {
        $s3 = new Zend_Service_Amazon_S3( S3_ACCESS_KEY, S3_SICRET_KEY);
        $res =  $s3->getInfo($bucketName.'/'.$fileName);
        if (empty($res) || $res['size'] == 0) {
            // check Caspio
            return false;
        }

        return true;
    } catch (Zend_Service_Amazon_S3_Exception $s3e) {
        print 'exception';
    }
}


function fileExistsCaspio($fileNameSource,$bucketName)
{
    $caspioFileData = Core_Caspio::caspioDownloadFile( $fileNameSource ); // get file from Caspio

    if (empty($caspioFileData) ) {
        return false;
    }

    return $caspioFileData;
        
}


?>