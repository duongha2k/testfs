<?php

class IndexController extends Core_Controller
{
    public function init()
    {
        parent::init();
        $this->smarty->assign('pageTitle', 'Index page');
    }

    public function indexAction()
    {
    echo "123";
    }

    public function versionAction()
    {
        $file = '../../htdocs/version.txt';
        if (file_exists($file)) {
            $ver = file_get_contents($file);
            echo $ver;
        } else {
            echo 0;
        }
    }

    public function getwotimestampsAction()
    {
     	$woId = (int)$this->_getParam('winnum');

		$result = Core_Caspio::caspioSelectAdv(TABLE_WORK_ORDER_TIMESTAMPS, 'TB_UNID,DateTime_Stamp,Description', 'TB_UNID='.$woId, '');
		echo "<html><body> timestamps for " . $woId . "\n";
		var_dump($result);
		echo "</body></html>";
    }
    
    public function testAction()
    {
		echo "bongo";
    }

}