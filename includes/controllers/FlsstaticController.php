<?php
define("FLSVersion", true);

class FlsstaticController extends Core_Controller
{
    public function init()
    {
        parent::init();
    }

    public function indexAction()
    {
		echo "fls-static index-land";
    }

    public function soapAction()
    {
    	ini_set("soap.wsdl_cache_enabled", "0");
		ini_set('soap.wsdl_cache_ttl', '0');		//cache wsdl for an hour

		$classmap = array('API_WorkOrder' => 'API_WorkOrder', 'API_Project' => 'API_Project', 'API_WorkOrder' => 'API_WorkOrder', 'API_PartEntry' => 'API_PartEntry', 'API_NewPart' => 'API_NewPart', 'API_ReturnPart' => 'API_ReturnPart', 'API_WorkOrderFilter' => 'API_WorkOrderFilter');

		try {
		        $server = new Zend_Soap_Server(HTTPS_URL.'/flsstatic/wsdl', array('cache_wsdl'=>WSDL_CACHE_NONE, 'classmap' => $classmap) );
			$server->setClass('Core_Api_Class');
			$server->handle();
		} catch (Exception $e) {
			echo $e;
		}
    }

    public function wsdlAction()
    {
//		$wsdl = new Zend_Soap_AutoDiscover_Wrapper('Zend_Soap_Wsdl_Strategy_ArrayOfTypeComplex');
//		$wsdl->setUri(HTTPS_URL.'/fls/soap/');
//		$wsdl->setClass('Core_Api_Class');
		
/*		$wsdl = $wsdl->getWsdl();
		
		$wsdl->addComplexType("API_WorkOrder");
		$wsdl->addComplexType("API_Project");
		$wsdl->addComplexType("API_PartEntry");		
		$wsdl->addComplexType("API_NewPart");
		$wsdl->addComplexType("API_ReturnPart");
		$wsdl->addComplexType("API_Response");
		$wsdl->addComplexType("API_Error");
		$wsdl->addComplexType("API_WorkOrderFilter");*/
		if (!headers_sent()) {
			header('Content-Type: text/xml');
		}
//		$wsdl->dump();
//		$wsdl->handle();

		$wsdl = file_get_contents(dirname(__FILE__) . "/fls-static.wsdl");
		$wsdl = str_replace('<!--SOAP URL-->', HTTPS_URL.'/flsstatic/soap/', $wsdl);
		echo $wsdl;
    }

    public function resetAction()
    {
        $cache = Core_Cache_Manager::factory();
        $cache->clean();
        echo "cache cleaned";
    }
}

class Zend_Soap_AutoDiscover_Wrapper extends Zend_Soap_AutoDiscover {
	public function getWsdl() {
		return $this->_wsdl;
	}
}
