<?php
class TechController extends Core_Controller
{
    public function init()
    {
        parent::init();
    }

    public function indexAction()
    {
		echo "tech index-land";
    }

    public function soapAction()
    {
    	ini_set("soap.wsdl_cache_enabled", "0");
		ini_set('soap.wsdl_cache_ttl', '0');		//cache wsdl for an hour
		$classmap = array('API_TechWorkOrder' => 'API_TechWorkOrder', 'API_Project' => 'API_Project', 'API_PartEntry' => 'API_PartEntry', 'API_NewPart' => 'API_NewPart', 'API_ReturnPart' => 'API_ReturnPart', 'API_WorkOrderFilter' => 'API_WorkOrderFilter', 'API_Tech' => 'API_Tech');

		$v = $this->_getParam('version', NULL);
		if (empty($v)) $v = 1; // default version
		$wsdlFileName = dirname(__FILE__) . '/wsdl/fieldsolutions/techv' . $v . '.wsdl';

        $server = new Zend_Soap_Server(HTTPS_URL.'/tech/wsdl/' . ($v == 1 ? '' : "$v"), array('cache_wsdl'=>WSDL_CACHE_NONE, 'classmap'=>$classmap) );
		$server->setClass('Core_Api_TechClassPublic');
		$server->handle();
    }

    public function wsdlAction()
    {
		$v = $this->_getParam('version', NULL);
		if (empty($v)) $v = 1; // default version
		$wsdlFileName = dirname(__FILE__) . '/wsdl/fieldsolutions/techv' . $v . '.wsdl';
		
		if (!headers_sent()) {
			header('Content-Type: text/xml');
		}

		$wsdl = file_get_contents($wsdlFileName);
		$wsdl = str_replace('<!--SOAP URL-->', HTTPS_URL.'/tech/soap/' . ($v == 1 ? '' : "$v"), $wsdl);
		echo $wsdl;
//		$wsdl = new Zend_Soap_AutoDiscover('Zend_Soap_Wsdl_Strategy_ArrayOfTypeComplex');
//	    $wsdl->setUri(HTTPS_URL.'/tech/soap/');
//		$wsdl->setComplexTypeStrategy('Zend_Soap_Wsdl_Strategy_ArrayOfTypeComplex');
//        $wsdl->setClass('Core_Api_TechClassPublic');

//		$wsdl = $wsdl->getWsdl();
		
//		require_once("API/TechWorkOrder.php");
//		$wsdl->addComplexType("API_TechWorkOrder");
/*		$wsdl->addComplexType("API_Project");
		$wsdl->addComplexType("API_PartEntry");		
		$wsdl->addComplexType("API_NewPart");
		$wsdl->addComplexType("API_ReturnPart");
		$wsdl->addComplexType("API_Response");
		$wsdl->addComplexType("API_Error");
		$wsdl->addComplexType("API_WorkOrderFilter");*/
//		if (!headers_sent()) {
//			header('Content-Type: text/xml');
//		}
//		$wsdl->dump();
//		$wsdl->handle();
		
    }

    public function resetAction()
    {
        $cache = Core_Cache_Manager::factory();
        $cache->clean();
        echo "cache cleaned";
    }

}

class Zend_Soap_AutoDiscover_Wrapper extends Zend_Soap_AutoDiscover {
	public function getWsdl() {
		return $this->_wsdl;
	}
}
