<?php
class ApiController extends Core_Controller
{
    public function init()
    {
        parent::init();
    }

    public function indexAction()
    {
    }

    public function soapAction()
    {
    	ini_set("soap.wsdl_cache_enabled", "0");
		ini_set('soap.wsdl_cache_ttl', '0');
        $server = new Zend_Soap_Server(HTTPS_URL.'/client/wsdl', array('cache_wsdl'=>WSDL_CACHE_NONE));
		$server->setClass('Core_Api_Class');
		$server->handle();
    }

    public function wsdlAction()
    {
	    $wsdl = new Zend_Soap_AutoDiscover();
	    $wsdl->setUri(HTTPS_URL.'/client/soap/');
        $wsdl->setClass('Core_Api_Class');
		$wsdl->handle();
		
    }

    public function resetAction()
    {
        $cache = Core_Cache_Manager::factory();
        $cache->clean();
    }

}