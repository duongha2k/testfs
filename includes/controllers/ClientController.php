<?php
class ClientController extends Core_Controller
{
    public function init()
    {
        parent::init();
    }

    public function indexAction()
    {
		echo "client index-land";
    }

    public function soapAction()
    {
    	ini_set("soap.wsdl_cache_enabled", "0");
		ini_set('soap.wsdl_cache_ttl', '0');		//cache wsdl for an hour
		$v = $this->_getParam('version', NULL);
		if (empty($v)) $v = 2; // default version
		$wsdlFileName = dirname(__FILE__) . '/wsdl/fieldsolutions/clientv' . $v . '.wsdl';
		$classmap = array('API_WorkOrder' => 'API_WorkOrder', 'API_Project' => 'API_Project', 'API_WorkOrder' => 'API_WorkOrder', 'API_PartEntry' => 'API_PartEntry', 'API_NewPart' => 'API_NewPart', 'API_ReturnPart' => 'API_ReturnPart', 'API_WorkOrderFilter' => 'API_WorkOrderFilter', 'API_Tech' => 'API_Tech', 'API_WOCategory' => 'API_WOCategory', 'API_Timestamp' => 'API_Timestamp');

        $server = new Zend_Soap_Server(HTTPS_URL.'/client/wsdl/' . ($v == 2 ? '' : "$v"), array('cache_wsdl'=>WSDL_CACHE_NONE, 'classmap'=>$classmap) );
		$server->setClass('Core_Api_Class');
		$server->handle();
    }

    public function wsdlAction()
    {
		$v = $this->_getParam('version', NULL);
		if (empty($v)) $v = 2; // default version
		$wsdlFileName = dirname(__FILE__) . '/wsdl/fieldsolutions/clientv' . $v . '.wsdl';

		if (!headers_sent()) {
			header('Content-Type: text/xml');
		}
		
		$wsdl = file_get_contents($wsdlFileName);
		$wsdl = str_replace('<!--SOAP URL-->', HTTPS_URL.'/client/soap/' . ($v == 2 ? '' : "$v"), $wsdl);
		echo $wsdl;
    }

    public function resetAction()
    {
        $cache = Core_Cache_Manager::factory();
        $cache->clean();
        echo "cache cleaned";
    }

}

class Zend_Soap_AutoDiscover_Wrapper extends Zend_Soap_AutoDiscover {
	public function getWsdl() {
		return $this->_wsdl;
	}
}
