<?php
class AdminController extends Core_Controller
{
    public function init()
    {
        parent::init();
    }

    public function indexAction()
    {
		echo "admin index-land";
    }

    public function soapAction()
    {
    	ini_set("soap.wsdl_cache_enabled", "0");
		ini_set('soap.wsdl_cache_ttl', '0');		//cache wsdl for an hour
		$classmap = array('API_AdminWorkOrder' => 'API_AdminWorkOrder', 'API_Project' => 'API_Project', 'API_AdminWorkOrder' => 'API_AdminWorkOrder', 'API_PartEntry' => 'API_PartEntry', 'API_NewPart' => 'API_NewPart', 'API_ReturnPart' => 'API_ReturnPart', 'API_AdminWorkOrderFilter' => 'API_AdminWorkOrderFilter', 'API_Tech' => 'API_Tech', 'API_WOCategory' => 'API_WOCategory', 'API_Timestamp' => 'API_Timestamp');

        $server = new Zend_Soap_Server(HTTPS_URL.'/admin/wsdl', array('cache_wsdl'=>WSDL_CACHE_NONE, 'classmap'=>$classmap) );
		$server->setClass('Core_Api_AdminClass');
		$server->handle();
    }

    public function wsdlAction()
    {
		$wsdl = new Zend_Soap_AutoDiscover_Wrapper('Zend_Soap_Wsdl_Strategy_ArrayOfTypeComplex');
		$classmap = array('API_AdminWorkOrder' => 'API_AdminWorkOrder', 'API_Project' => 'API_Project', 'API_AdminWorkOrder' => 'API_AdminWorkOrder', 'API_PartEntry' => 'API_PartEntry', 'API_NewPart' => 'API_NewPart', 'API_ReturnPart' => 'API_ReturnPart', 'API_AdminWorkOrderFilter' => 'API_AdminWorkOrderFilter', 'API_Tech' => 'API_Tech', 'API_WOCategory' => 'API_WOCategory', 'API_Timestamp' => 'API_Timestamp');
	    $wsdl->setUri(HTTPS_URL.'/admin/soap/');
		$wsdl->setComplexTypeStrategy('Zend_Soap_Wsdl_Strategy_ArrayOfTypeComplex');
        $wsdl->setClass('Core_Api_AdminClass');
		$wsdl = $wsdl->getWsdl();
		
		$wsdl->addComplexType("API_Tech");
		$wsdl->addComplexType("API_WOCategory");
		$wsdl->addComplexType("API_AdminWorkOrderFilter");
		$wsdl->addComplexType("API_Timestamp");
/*		$wsdl->addComplexType("API_Project");
		$wsdl->addComplexType("API_PartEntry");		
		$wsdl->addComplexType("API_NewPart");
		$wsdl->addComplexType("API_ReturnPart");
		$wsdl->addComplexType("API_Response");
		$wsdl->addComplexType("API_Error");
		$wsdl->addComplexType("API_WorkOrderFilter");*/
		if (!headers_sent()) {
			header('Content-Type: text/xml');
		}
		$wsdl->dump();
//		$wsdl->handle();
		
    }

    public function resetAction()
    {
        $cache = Core_Cache_Manager::factory();
        $cache->clean();
        echo "cache cleaned";
    }

}

class Zend_Soap_AutoDiscover_Wrapper extends Zend_Soap_AutoDiscover {
	public function getWsdl() {
		return $this->_wsdl;
	}
}
