<?php
class SalesforceController extends Core_Controller
{
    public function init()
    {
        parent::init();
    }

    public function indexAction()
    {
	echo "sf index-land";
    }

    public function soapAction()
    {
    }

	public function wsdlAction()
	{
		$companyID = $this->_getParam('companyID');
		if (!headers_sent()) {
			header('Content-Type: text/xml');
		}
		$wsdl = file_get_contents(dirname(__FILE__) . "/wsdl/salesforce/$companyID/workflowOutboundMessage.wsdl.xml");
		//	$wsdl = str_replace('<!--SOAP URL-->', HTTPS_URL.'/flsstatic/soap/', $wsdl);

		echo $wsdl;
	}

	public function syncAction() {
    		ini_set("soap.wsdl_cache_enabled", "0");
		ini_set('soap.wsdl_cache_ttl', '0');		//cache wsdl for an hour
		$token = $this->_getParam('token');
		$companyID = $this->_getParam('companyID');
		$db = Core_Database::getInstance();
		$sql = "SELECT organization_id, company_id, api_username, token_key FROM salesforce_organization WHERE company_id = ? LIMIT 1";
		$organization = $db->fetchAll($sql, array($companyID));

		if (!$organization || sizeof($organization) == 0 || $token != md5($organization[0]["token_key"] . $companyID)) {
			$key = empty($organization[0]["token_key"]) ? "sf-default-key" : $organization[0]["token_key"];
			error_log("**** SF sync: $key $companyID " . md5($key . $companyID));
			header("HTTP/1.0 403 Forbidden");
		}

		try {
			$classmap = array('sObject' => 'Salesforce_WorkOrder_Abstract', 'Work_Order__c' => 'Salesforce_WorkOrder_' . $companyID, 'Work_Order__cNotification' => 'Salesforce_WorkOrder_Notification');

		        $server = new Zend_Soap_Server(HTTPS_URL."/salesforce/wsdl/$companyID", array('cache_wsdl'=>WSDL_CACHE_NONE, 'classmap' => $classmap) );
		} catch (Exception $e) {
			error_log("*** SF sync: " . $e);
		}

		$server->setClass('Salesforce_Api_Class');
		$server->handle();
	}

}

