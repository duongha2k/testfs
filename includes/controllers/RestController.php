<?php
define("RESTVersion", true);
ini_set('display_errors', 0);

class RestController extends Zend_Controller_Action
{
    public function init()
    {
    }

	public function clientAccessAction()
  	{
		// lists features a client has access to. Returns list of features in json format.
		$user = $this->_getParam('user');
		$pass = $this->_getParam('pass');
		$response = array();
		$authData = array('login'=>$user, 'password'=>$pass);
		$u = new Core_Api_User();
		$u->checkAuthentication($authData);
		if (!$u->isAuthenticate()) $response = array('error' => 'invalid login');
		else {
			$access = Core_ClientFeatures_Access::getAccessListForClientId($u->getClientId());
			$response = array('success' => $access);
		}
		$response = json_encode($response);
  		$this->_response->setHeader("Content-Type", "text/plain")->setBody($response);
    }

	public function woAction()
  	{
  		 $winNum = $this->_getParam('winNum');
  		 $user = $this->_getParam('user');
  		 $pass = $this->_getParam('pass');

         $w = new Core_Api_Class();

		 if ($this->getRequest()->getMethod() == 'GET')
  		 {
			$response = $w->getWorkOrder($user, $pass, $winNum);

			$responseXML = $response->toXML(array("single" => true));

			if ($responseXML == "") $responseXML = "<WO/>";

	  		$this->_response->setHeader("Content-Type", "text/xml")->setBody($responseXML);

  		 }
  		 else if ($this->getRequest()->getMethod() == 'POST')
  		 {
			$postData = $this->getRequest()->getRawBody(); // for some reason getPost doesn't work with Pentaho POST
			$postParts = explode("=", $postData);

			$response = $w->updateWorkOrder($user, $pass, $winNum, $postParts[1]);
			$responseXML = $response->toXML(array("single" => true, "fieldList" => "WIN_NUM"));

  		 	$this->_response->setHeader("Content-Type", "text/xml")->setBody($responseXML);
  		 }
    }

	public function winnumAction()
  	{
  		$woID = $this->_getParam('woID');
  		$user = $this->_getParam('user');
  		$pass = $this->_getParam('pass');
        $w = new Core_Api_Class();

 		$response = $w->getWinNum($user, $pass, urldecode($woID));
        // var_dump($response);

		$responseXML = $response->toXML(array("single" => true, "fieldList" => "WIN_NUM"));
		// var_dump($responseXML);
		// exit;
		if ($responseXML == "") $responseXML = "<WO/>";

	  	$this->_response->setHeader("Content-Type", "text/xml")->setBody($responseXML);
    }

    public function publishAction()
  	{
		$winNum = $this->_getParam('winNum');
		$user = $this->_getParam('user');
		$pass = $this->_getParam('pass');

        $w = new Core_Api_Class();

 		$response = $w->publishWorkOrder($user, $pass, $winNum);
		$responseXML = $response->toXML(array("fieldList" => "WIN_NUM", "single" => true));
		if ($responseXML == "") $responseXML = "<WO/>";

	  	$this->_response->setHeader("Content-Type", "text/xml")->setBody($responseXML);
    }

    public function assignAction()
  	{ 
		$winNum = $this->_getParam('winNum');
		$user = $this->_getParam('user');
		$pass = $this->_getParam('pass');
		$techID = $this->_getParam('techID');
		$bidAmount = $this->_getParam('bidAmount');
        $bidComments = $this->_getParam('bidComments');
        $amountper = $this->_getParam('amountPer');

       	$logger = Core_Api_Logger::getInstance();
		$logger->setUrl('assignWorkOrder' . $winNum);
		$logger->setLogin($user);
		$logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');


		// $user = array('login'=>$user, 'password'=>$pass);

		// $userObj = new Core_Api_User();
		// $userObj->checkAuthentication($authData);

		// if (!$userObj->isAuthenticate())
		//	return Core_Api_Error::getInstance()->getErrors();

        $w = new Core_Api_Class();

		$response = $w->assignWorkOrder($user, $pass, $winNum, $techID, $bidAmount, $bidComments, $amountper);
		$responseXML = $response->toXML(array("fieldList" => "WIN_NUM", "single" => true));
		if ($responseXML == "") $responseXML = "<WO/>";

	  	$this->_response->setHeader("Content-Type", "text/xml")->setBody($responseXML);
    }

	public function approveAction()
  	{
		$winNum = $this->_getParam('winNum');
  		$user = $this->_getParam('user');
  		$pass = $this->_getParam('pass');

        $w = new Core_Api_Class();

 		$response = $w->approveWorkOrder($user, $pass, $winNum);
		$responseXML = $response->toXML(array("fieldList" => "WIN_NUM", "single" => true));
		if ($responseXML == "") $responseXML = "<WO/>";

	  	$this->_response->setHeader("Content-Type", "text/xml")->setBody($responseXML);
    }

	public function deactivateAction()
  	{
		$winNum = $this->_getParam('winNum');
		$user = $this->_getParam('user');
		$pass = $this->_getParam('pass');
		$code = $this->_getParam('code');
		$reason = $this->_getParam('reason');

        $w = new Core_Api_Class();

 		$response = $w->deactivateWorkOrder($user, $pass, $winNum, $code, $reason);
		$responseXML = $response->toXML(array("fieldList" => "WIN_NUM", "single" => true));
		if ($responseXML == "") $responseXML = "<WO/>";

	  	$this->_response->setHeader("Content-Type", "text/xml")->setBody($responseXML);
    }

	public function incompleteAction()
  	{
  		 $winNum = $this->_getParam('winNum');
  		 $user = $this->_getParam('user');
  		 $pass = $this->_getParam('pass');
  		 $comments = $this->_getParam('comments');

  		$w = new Core_Api_Class();

 		$response = $w->markWorkOrderIncomplete($user, $pass, $winNum, $comments);
		$responseXML = $response->toXML(array("fieldList" => "WIN_NUM", "single" => true));
		if ($responseXML == "") $responseXML = "<WO/>";

	  	$this->_response->setHeader("Content-Type", "text/xml")->setBody($responseXML);
    }
	
    public function wovisitAction()
    {
        $Core_Api_WosClass = new Core_Api_WosClass;
        $Core_Api_Class = new Core_Api_Class;
        $request    = $this->getRequest();
        $user       = $request->getParam('user', NULL);
        $pass       = $request->getParam('pass', NULL);
        $woid        = $request->getParam('woid', NULL);
		$order = $request->getParam('order', NULL);
		$woVisitID	= $request->getParam('woVisitID', NULL); // NULL means create new
		$startType	= $request->getParam('startType', NULL);
		$startDate	= $request->getParam('startDate', NULL);
		$startTime	= $request->getParam('startTime', NULL);
		$endDate	= $request->getParam('endDate', NULL);
		$endTime	= $request->getParam('endTime', NULL);
		$estimatedDuration	= $request->getParam('estimatedDuration', NULL);
		$techArrivalInstructions	= $request->getParam('techArrivalInstructions', NULL);
		$company	= $request->getParam('company', NULL);
		$order	= $request->getParam('order', NULL);
		$woVisitID = NULL;
		error_log("19806a:  $woVisitID, $woid, $startType, $startDate, $startTime, $endDate, $endTime, $estimatedDuration, $techArrivalInstructions, $company, $order");

		$authData = array('login'=>$user, 'password'=>$pass);

		$userObj = new Core_Api_User();
		$userObj->checkAuthentication($authData);
		$responseXML = "";
		if (!empty($company) && !empty($woid)) {
			$duplicateStartDate = $Core_Api_WosClass->getWOVisitIDWithCompanyAndWOID($company,$woid,NULL,$startDate);
			error_log("19806a $company $woid $order");
			if (!empty($duplicateStartDate)) $responseXML = "<error id=\"2\">A visit with start date of $startDate already exists</error>";
			if (!empty($order) && empty($responseXML)) {
				$visit = $Core_Api_WosClass->getWOVisitIDWithCompanyAndWOID($company,$woid,$order); // match visit # for use with wo import
				error_log("19806a " . print_r($visit, true));
				
				if (!empty($visit)) {
					// override provisional wo visits (visits w/o WIN_NUM)
					$woVisitID = $visit["WoVisitID"];
					error_log("19806a overrider " . $woVisitID);
				}
			}
		}
		if (!$userObj->isAuthenticate()) {
			$responseXML = "<error id=\"1\">Login invalid</error>";
		}
		else if (empty($responseXML)){
			// check valid
			$errors = $Core_Api_WosClass->validWOVisit(
				$woVisitID,
				"",
				$startType,
				!empty($startDate)?date_format(new DateTime($startDate), "Y-m-d"):"",
				$startTime,
				!empty($endDate)?date_format(new DateTime($endDate), "Y-m-d"):"",
				$endTime,
				$estimatedDuration,
				$techArrivalInstructions);
				
			if(empty($errors)){
				// create visit
				$errors = $Core_Api_WosClass->saveWOVisit(
					$woVisitID,
					NULL,
					$startType,
					!empty($startDate)?date_format(new DateTime($startDate), "Y-m-d"):"",
					$startTime,
					!empty($endDate)?date_format(new DateTime($endDate), "Y-m-d"):"",
					$endTime,
					$estimatedDuration,
					$techArrivalInstructions,
					$company,
					$woid,
					$order);
			}
		}
		if (!empty($errors)) {
			$responseXML = "<error id=\"6\">" . $errors->errors[0]->message . "</error>";
		}
		else if (empty($responseXML))
			$responseXML = "<API_Response/>";
		$this->getResponse()
			->setHeader("Content-Type", "text/xml")
			->setBody($responseXML);
    }

	public function techwosAction()
    {
        $request    = $this->getRequest();
        $user       = $request->getParam('user', NULL);
        $pass       = $request->getParam('pass', NULL);
        $win        = $request->getParam('win', NULL);

        $tech = new Core_Api_TechClass;

        if ( $request->isGet() ) {
            if ( is_numeric($win) ) {
                $wos = $tech->getWorkOrder($user, $pass, $win);
            } else {
                $offset = $request->getParam('offset', 0);
                $limit  = $request->getParam('limit', 0);
                $count  = -1;
                $sort   = $request->getParam('sort', '');
                $filter = $this->prepareFilter();

                $wos = $tech->getWorkOrders($user, $pass, NULL, $filter, $offset, $limit, $count, $sort);
            }

            $responseXML = $wos->toXML();
            if ($responseXML == "") $responseXML = "<WOS/>";

            $this->getResponse()
                ->setHeader("Content-Type", "text/xml")
                ->setBody($responseXML);
        }
    }

    public function techupwoAction()
    {
        require_once 'Core/Api/TechClass.php';

        $request    = $this->getRequest();
        $user       = $request->getParam('user', NULL);
        $password   = $request->getParam('password', NULL);
        $win        = $request->getParam('win', NULL);
        $worder     = $request->getParam('wo', NULL);

        $tech = new Core_Api_TechClass;

        if ( $request->isPost() ) {
            $wos = $tech->updateWorkOrder($user, $password, $win, $worder);

            $responseXML = $wos->toXML();
            if ($responseXML == "")
                $responseXML = "<WOS/>";

            $this->getResponse()
                ->setHeader("Content-Type", "text/xml")
                ->setBody($responseXML);
        }
    }

    public function techUploadFileAction()
    {
        require_once 'Core/Api/TechClass.php';

        $request    = $this->getRequest();
        $response   = $this->getResponse();
        $user       = $request->getParam('user', NULL);
        $password   = $request->getParam('password', NULL);
        $win        = $request->getParam('winNum', NULL);
        $fname      = '';
        $fmime      = '';
        $saveField  = '';
        $fdata      = '';
        $fsize      = '';

        $tech = new Core_Api_TechClass;

        $result = '';

        $body = $request->getRawBody();
        if ( $body && strpos($body, '<File>') !== FALSE ) {
            $dom = new DOMDocument('1.0', 'UTF-8');
            $dom->loadXML($body);
            $fileNodes = $dom->getElementsByTagName('File');
            for ( $fileNum = 0; $fileNum < $fileNodes->length; ++$fileNum ) {
                $fileChilderen = $fileNodes->item($fileNum)->childNodes;
                if ( $fileChilderen->length ) {
                    for ( $fileFields = 0; $fileFields < $fileChilderen->length; ++$fileFields ) {
                        $fileField = $fileChilderen->item($fileFields);
                        switch ( strtolower($fileField->nodeName) ) {
                            case 'name'         : $fname     = $fileField->nodeValue; break;
                            case 'data'         : $fdata     = $fileField->nodeValue; break;
                            case 'contenttype'  : $fmime     = $fileField->nodeValue; break;
                            case 'size'         : $fsize     = $fileField->nodeValue; break;
                            case 'field'        : $saveField = $fileField->nodeValue; break;
                            default             : trigger_error('Unexpected node "'.$fileField.'"', E_USER_NOTICE);
                        }
                    }
                    $result = $tech->uploadFile($user, $password, $win, $fname, $fdata, $fmime, $saveField);
                    $fname = $fdata = $fmime = $fsize = $saveField = '';
                    if ( !$result->success ) {
                        $response->clearHeaders();
                        $response->clearBody();
                        $response
                            ->setHeader('Content-Type', 'text/xml')
                            ->setBody($result->toXML());
                        return;
                    }
                }
            }
        }

        if ( $result ) {
            $XML = $result->toXML(array("fieldList" => 'WIN_NUM'), true);
        } else {
            $XML = "<WOS/>";
        }
        $response
            ->setHeader("Content-Type", "text/xml")
            ->setBody($XML);
        return;
    }

    private function prepareFilter()
    {
        $request = $this->getRequest();

  		$filter                         = new API_WorkOrderFilter;
        $filter->Company_ID             = $request->getParam('Company_ID', NULL);
  		$filter->WO_ID                  = $request->getParam('WO_ID', NULL);
  		$filter->WO_ID_Mode                  = 1; // use exact search for import
  		$filter->Project_ID             = $request->getParam('Project_ID', NULL);
  		$filter->StartDate              = $request->getParam('StartDate', NULL);
  		$filter->EndDate                = $request->getParam('EndDate', NULL);
  		$filter->PO                     = $request->getParam('PO', NULL);
  		$filter->ShowTechs              = $request->getParam('ShowTechs', NULL);
  		$filter->Region                 = $request->getParam('Region', NULL);
  		$filter->SiteName               = $request->getParam('SiteName', NULL);
  		$filter->SiteNumber             = $request->getParam('SiteNumber', NULL);
  		$filter->City                   = $request->getParam('City', NULL);
  		$filter->State                  = $request->getParam('State', NULL);
  		$filter->Zipcode                = $request->getParam('Zipcode', NULL);
  		$filter->Update_Requested       = $request->getParam('Update_Requested', NULL);
  		$filter->CheckedIn              = $request->getParam('CheckedIn', NULL);
  		$filter->WorkOrderReviewed      = $request->getParam('WorkOrderReviewed', NULL);
  		$filter->TechCheckedIn_24hrs    = $request->getParam('TechCheckedIn_24hrs', NULL);
  		$filter->TechMarkedComplete     = $request->getParam('TechMarkedComplete', NULL);
  		$filter->Site_Complete          = $request->getParam('Site_Complete', NULL);
  		$filter->Approved               = $request->getParam('Approved', NULL);
  		$filter->Additional_Pay_Amount  = $request->getParam('Additional_Pay_Amount', NULL);
  		$filter->Work_Out_of_Scope      = $request->getParam('Work_Out_of_Scope', NULL);
  		$filter->TechPaid               = $request->getParam('TechPaid', NULL);
  		$filter->Tech_FName             = $request->getParam('Tech_FName', NULL);
  		$filter->Tech_LName             = $request->getParam('Tech_LName', NULL);
  		$filter->Tech_ID                = $request->getParam('Tech_ID', NULL);
  		$filter->Sourced                = $request->getParam('Sourced', NULL);
  		$filter->Deactivated            = $request->getParam('Deactivated', NULL);
        $filter->WO_State               = $request->getParam('WO_State', NULL);
        $filter->Distance               = $request->getParam('Distance', NULL);

        return $filter;
    }

    public function wosAction()
  	{                
  		 if ($this->getRequest()->getMethod() == 'GET')
  		 {
  		 	$this->queryWorkOrders();
  		 }
  		 else if ($this->getRequest()->getMethod() == 'POST')
  		 {
  		 	$this->createWorkOrder();
  		 }
  	}

  	private function queryWorkOrders()
  	{
  		$timeStampFrom = $this->_hasParam('TimeStampFrom') ?
  			urldecode( $this->_getParam('TimeStampFrom') ) : NULL;

  		$timeStampTo = $this->_hasParam('TimeStampTo') ?
  			urldecode( $this->_getParam('TimeStampTo') ) : NULL;

  		$timeStampType = $this->_hasParam('TimeStampDescription') ?
  		  			$this->_getParam('TimeStampDescription') :
				  		($this->_hasParam('TimeStampType') ?
  							$this->_getParam('TimeStampType') : NULL);

  		$user = $this->_getParam('user');
  		$pass = $this->_getParam('pass');

  		$filters    = $this->prepareFilter();
		$fieldList  = 'WIN_NUM,WO_ID';
		$w          = new Core_Api_Class();

		$response = $w->getWorkOrders($user, $pass, $fieldList, $timeStampFrom, $timeStampTo, $timeStampType, $filters);

		$responseXML = $response->toXML(array("fieldList" => $fieldList));
		if ($responseXML == "") $responseXML = "<WOS/>";

  		$this->_response->setHeader("Content-Type", "text/xml")->setBody($responseXML);
  	}

  	private function createWorkOrder()
  	{
  		define('NO_CREATE_WORK_ORDER_EMAIL', 1);

		error_log("19806a: create");
  		$user = $this->_getParam('user');
  		$pass = $this->_getParam('pass');
  		$publish = $this->_getParam('publish');
		$wo = $this->_getParam('wo');
		$file = $this->_getParam('file');
		if (!empty($file)) {
			$db = Core_Database::getInstance();
			$db->delete(Core_Database::TABLE_WORK_ORDER_IMPORT, "date <= DATE_SUB(NOW(), INTERVAL 72 HOUR)");
			$select = $db->select();
			$select->from(Core_Database::TABLE_WORK_ORDER_IMPORT, array('id'))->where("file = ?", $file)->order('id DESC')->limit(1);
			$importId = $db->fetchOne($select);
			if (empty($importId)) {
				Core_Database::insert(Core_Database::TABLE_WORK_ORDER_IMPORT, array('file' => $file, 'date' => new Zend_Db_Expr('NOW()')));
				$importId = $db->lastInsertId('work_order_import');
			}

			define('IMPORTID', $importId);
		}
		
  		$w = new Core_Api_Class();

		$publish = $publish == "True";
  		$response = $w->createWorkOrder($user, $pass, $publish, $wo);
		$responseXML = $response->toXML(array("single" => true, "fieldList" => "WIN_NUM"));
		if ($responseXML == "") $responseXML = "<WO/>";
		error_log("19806a: $responseXML");

		$this->_response->setHeader("Content-Type", "text/xml")->setBody($responseXML);
  	}

	public function projectsAction()
  	{
  		$user = $this->_getParam('user');
  		$pass = $this->_getParam('pass');

  		$w = new Core_Api_Class();

  		$response = $w->getAllProjects($user, $pass);
		$responseXML = $response->toXML();
		if ($responseXML == "") $responseXML = "<Projects/>";

	  	$this->_response->setHeader("Content-Type", "text/xml")->setBody($responseXML);

    }

	public function partsAction() {
		$method = $this->getRequest()->getMethod();
  		$w = new Core_Api_Class();
		$winNum = $this->_getParam('winNum');
		$woID = $this->_getParam('woID', null);
		$user = $this->_getParam('user');
		$pass = $this->_getParam('pass');
		$partID = $this->_getParam('partID', null);
		
		$shortResponse = false;
		$single = false;
		$dom = new DOMDocument("1.0");
		
		$postData = $this->getRequest()->getRawBody();
		
		$response = null;
		
		switch ($method) {
			case "POST":
				// create
			    $parts = $this->_getParam('parts', null);

				if (empty($partID)) {
				    if ($winNum == 0 && !empty($woID) ) {
				        // search $winNum
                        $result = $w->getWinNum($user, $pass, $woID);
                        if ($result->success) {
                            $result = $result->data[0]->WIN_NUM;
                            
                            $partsRes = $w->getParts($user, $pass, $result);
                            if (empty($partsRes->data)) {
                                $winNum = $result;
                            }
                        }
                        
                        $errors = Core_Api_Error::getInstance();
                        $errors->resetErrors();
                        
				    }
				    if (!empty($winNum) ) {
			  		    $response = $w->createPartEntry($user, $pass, $winNum, $parts);
				    } else {
				        //not adding part, part is already exists
				        $response = API_Response::success(array());
				    }
				} else {
			  		$response = $w->updatePartEntry($user, $pass, $winNum, $partID, $parts);
				}
				$single = true;
				$shortResponse = true;

				break;
			case "DELETE":
				// delete
		  		$response = $w->deletePart($user, $pass, $winNum, $partID);
				$single = true;
				$shortResponse = true;

			default:
				// get
				if (empty($partID)) {
			  		$response = $w->getParts($user, $pass, $winNum);
				}
				else {
			  		$response = $w->getPartEntry($user, $pass, $winNum, $partID);
					$single = true;
				}
				break;
		}

		$responseXML = $response->toXML(array("single" => $single, "short" => $shortResponse));
		if ($responseXML == "")
			$responseXML = $single ? "<PartEnry/>" : "<Parts/>";

		$this->_response->setHeader("Content-Type", "text/xml")->setBody($responseXML);
	}

	public function techpartsAction() {
		$method = $this->getRequest()->getMethod();
  		$w = new Core_Api_TechClass();
		$winNum = $this->_getParam('winNum');
		$user = $this->_getParam('user');
		$pass = $this->_getParam('pass');
		$partID = $this->_getParam('partID');
		$shortResponse = false;
		$single = false;
		$dom = new DOMDocument("1.0");
		switch ($method) {
			case "POST":
				// create
				$postData = $this->getRequest()->getRawBody(); // for some reason getPost doesn't work with Pentaho POST
				$postParts = explode("=", $postData);

				if ($postParts[0] == "parts")
					$postData = $postParts[1];
				else
					$postData = "";

		  		$response = $w->updateTechPartEntry($user, $pass, $winNum, $partID, $postData);
				$shortResponse = true;

				break;
			default:
				// get
				if (empty($partID)) {
			  		$response = $w->getTechParts($user, $pass, $winNum);
				}
				else {
			  		$response = $w->getTechPartEntry($user, $pass, $winNum, $partID);
					$single = true;
				}
				break;
		}

		$responseXML = $response->toXML(array("single" => $single, "short" => $shortResponse));
		if ($responseXML == "")
			$responseXML = $single ? "<PartEnry/>" : "<Parts/>";

		$this->_response->setHeader("Content-Type", "text/xml")->setBody($responseXML);
	}
    /*
    public function testservercreateAction() {
        if($this->getRequest()->getMethod() == 'POST'){
            $user = $this->_getParam('user');
            $pass = $this->_getParam('pass');
            $publish = $this->_getParam('publish');

            $postData = $this->getRequest()->getRawBody(); // for some reason getPost doesn't work with Pentaho POST

            $postParts = explode("=", $postData);
            //var_dump(urldecode($postParts[1]));exit();
            //$postParts[1] = '<?xml version="1.0" encoding="UTF-8"?><WO>'
            //.'<WO_ID>qweasdqwe</WO_ID>'
            //.'<Zipcode>55456</Zipcode>'
            //.'<Project_ID>PRJ3</Project_ID>'
            //.'<Headline>Headline</Headline>'
            //.'<WO_Category_ID>1</WO_Category_ID>'

            //.'</WO>';

            $w = new Core_Factory();
            $t = '<?xml version="1.0" encoding="UTF-8"?><WO>'.urldecode($postParts[1]).'</WO>';
            $publish = $publish == "True";
            $response = $w->createWorkOrder($user, $pass, $publish, $t);
            $responseXML = $response->toXML(array("single" => true, "fieldList" => "WIN_NUM"));
            if ($responseXML == "") $responseXML = "<WO/>";
            $this->_response->setHeader("Content-Type", "text/xml")->setBody('');
            $this->_response->setHeader("Content-Type", "text/xml")->setBody($responseXML);
        }
    }

    public function testcreateAction() {
        $user = $this->_getParam('user');
        $pass = $this->_getParam('pass');
        $publish = $this->_getParam('publish');

        echo '<form action="/api/rest/testservercreate?user='.$user.'&pass='.$pass.'&publish='.$publish.'" method="POST">'
        .'<textarea name="asdf" cols="40" rows="20">'

        .'<WO_ID>qweasdqwe</WO_ID>'
        .'<Zipcode>55456</Zipcode>'
        .'<Project_ID>PRJ3</Project_ID>'
        .'<Headline>Headline</Headline>'
        .'<WO_Category_ID>1</WO_Category_ID>'

        .'</textarea><input type="submit" value="send"></form>';
    }


    public function testupdateAction() {
        echo '
        <form method="post" action="?user=berazouski&pass=123456&publish=false">
            <textarea name="xmlcantext"><WO_ID>qweasdqwe</WO_ID><Project_ID>ADSFSA<Project_ID></textarea>
            <input type="submit" value="send" />
        </form>
        ';

        if($this->getRequest()->getMethod() == 'POST'){
            $user = $this->_getParam('user');
            $pass = $this->_getParam('pass');
            //$winNum = '10300';//$this->_getParam('winNum');
            $winNum = '55283';//$this->_getParam('winNum');
            $postParts[1] = '<?xml version="1.0" encoding="UTF-8"?><WO><WO_ID>qweasdqwe</WO_ID><Zipcode>55456</Zipcode><Project_ID>128</Project_ID><Headline>Headline</Headline><WO_Category_ID>1</WO_Category_ID>'
            .'<Type_ID>2</Type_ID><Tech_ID>36807</Tech_ID><PayMax>33.3</PayMax><SitePhone>(123) 123-1233</SitePhone>'
            .'<Tech_Bid_Amount>33.3</Tech_Bid_Amount><ProjectManagerPhone>(123) 123-1233</ProjectManagerPhone><ResourceCoordinatorPhone>(123) 123-1233</ResourceCoordinatorPhone>'
            .'<State>NY</State></WO>';

            $w = new Core_Factory();

            $response = $w->updateWorkOrder($user, $pass, $winNum, $postParts[1]);
            $responseXML = $response->toXML(array("single" => true, "fieldList" => "WIN_NUM"));
            if ($responseXML == "") $responseXML = "<WO/>";
        }
    }

    */

}
