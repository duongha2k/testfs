<?php
class WorkMarket_Assignment {
	private $title;
	private $description;
	private $industry_id;
	private $scheduled_start;
	private $scheduled_start_date;
	private $scheduled_end;
	private $scheduled_end_date;
	private $location_country;
	private $location_type;
	private $send_to_groups;
	private $send_radius;
	private $support_contact_email;
	
	private $pricing_type;
	private $pricing_flat_price;
	private $pricing_per_hour_price;
	private $pricing_per_unit_price;
	private $pricing_max_number_of_units;
	
	private $template_id;
	
	private $instructions;
	private $private_instructions;
	private $location_contacts;
	private $location_name;
	private $location_number;
	
	public function __construc($fields) {
		if (!is_array($fields)) return;
		foreach ($fields as $k=>$v) {
			$this->$k = $v;
		}
	}
	public function __set($name, $value) {
		$this->$name = $value;
	}
	public function __get($name) {
		return $this->$name;
	}
	public function toArray() {
		$ret = array();
		foreach ($this as $k=>$v) {
			if ($k == 'location_contacts' || $k == 'send_to_groups') {
				$kOrig = $k;
				foreach ($v as $vk=>$vv) {
					$k = $kOrig;
					$k .= "[$vk]";
					if (is_array($vv)) {
						$vkOrig = $k;
						foreach ($vv as $vvk=>$vvv) {
							$k = $vkOrig;
							$k .= "[$vvk]";
							$ret[$k] = $vvv;
						}
					}
					else
						$ret[$k] = $vv;
				}
			}
			else
				$ret[$k] = $v;
		}
		return $ret;
	}
}