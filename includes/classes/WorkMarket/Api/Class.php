<?php
class WorkMarket_Api_Class {
		const URL_TEST = "https://api-dev.workmarket.com/api/v1";
		const URL_PROD = "https://www.workmarket.com/api/v1";
		const MODE_TEST = 0;
		const MODE_PROD = 1;
        private $token;
        private $secret;
        private $access_token;
        private $mode;
        public function __construct($token, $secret, $mode = self::MODE_PROD) {
                $this->token = $token;
                $this->secret = $secret;
		$this->mode = $mode;
        }

	private function qsToken() {
		return "access_token=" . $this->access_token;
	}

	public function connect() {
		$auth = $this->call("/authorization/request", 
			array("token" => $this->token, "secret" => $this->secret));
		return $this->access_token = $auth->response->access_token;
	}
	public function assignmentList() {
		return $this->call("/assignments/list?");
	}
	public function assignmentListUpdated($modifiedUnixTs, $start = 0) {
		return $this->call("/assignments/list_updated?modified_since=$modifiedUnixTs&start=$start&");
	}
	public function locationTypesList() {
		return $this->call("/constants/location_types?");
	}
	public function assignmentCreate(WorkMarket_Assignment $assignment) {
		return $this->call("/assignments/create?", $assignment->toArray());
	}
	public function assignmentGet($id) {
		return $this->call("/assignments/get?id=$id&");
	}
	public function assignmentVoid($id) {
		return $this->call("/assignments/void?", array('id' => $id));
	}
	public function assignmentNotesAdd($id, $content, $is_private = false) {
		return $this->call("/assignments/add_note?id=$id&", array('content' => $content, 'is_private' => $is_private ? 1 : 0));
	}
	public function assignmentAttachmentAdd($id, $filename, $data, $description) {
		return $this->call("/assignments/attachments/add?id=$id&", array('attachment' => base64_encode($data), 'filename' => $filename, 'description' => $description));
	}
	public function assignmentAttachmentRemove($id, $uuid) {
		return $this->call("/assignments/attachments/remove?id=$id&", array('uuid' => $uuid));
	}
	public function assignmentAttachmentList($id, $closeOutOnly = false) {
		return $this->call("/assignments/attachments/list?id=$id&closeout=" . ($closeOutOnly ? 1 : 0) . "&");
	}
	public function assignmentAttachmentGet($uuid) {
		return $this->call("/assignments/attachments/get?uuid=$uuid&");
	}
	public function assignmentRejectForPayment($id, $note) {
		return $this->call("/assignments/reject_payment?id=$id&", array('note' => $note));
	}
	public function assignmentApproveForPayment($id) {
		return $this->call("/assignments/approve_payment?", array('id' => $id));
	}
	public function assignmentCancel($id, $pay, $note) {
		return $this->call("/assignments/cancel?id=$id&", array('note' => $note, 'reason' => 'end_user_cancelled', 'amount' => $pay));
	}
	public function assignmentQuestionAnswer($id, $answer) {
		return $this->call("/assignments/questions/answer?id=$id&", array('answer' => $answer));
	}
	public function assignmentOffersAccept($id) {
		return $this->call("/assignments/offers/accept?", array('id' => $id));
	}
	public function groupsList() {
		return $this->call("/groups/list?");
	}
	public function labelsList() {
		return $this->call("/labels/list?");
	}
	public function statusList() {
		return $this->call("/statuses/list?");
	}

	private function call($url, $post = NULL) {
		if ($this->mode == self::MODE_PROD) 
			$url = self::URL_PROD . $url;
		else
			$url = self::URL_TEST . $url;
		if (!empty($this->access_token)) $url .= $this->qsToken();
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_FAILONERROR, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1); // return into a variable
		curl_setopt($ch, CURLOPT_TIMEOUT, 10);
		if (!empty($post)) {
			curl_setopt($ch, CURLOPT_POST, 1); // set POST method
			curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));
		}
		$result = curl_exec($ch); // run the whole process
		curl_close($ch);
		$result = json_decode($result);
		return $result;
	}
}

/*$wm = new WorkMarket_Api_Class("5l7IuedcT1ctNAMCWh8v", "DK9glQ09Le19qyODi7njZzoPljCbpdt0dEDXrqGC", WorkMarket_Api_Class::MODE_TEST);
$z = $wm->connect();
$wo = new WorkMarket_Assignment();
$wo->title = "API Test " . strtotime("+1 sec");
$wo->description = "Fix a printer";
$wo->industry_id = 1000;
/*$wo->scheduled_start = strtotime("+1 hour");
$wo->scheduled_start_date = date("Y-m-d h:i A", $wo->scheduled_start) . ' CDT';
$wo->scheduled_end = strtotime("+2 hour");
$wo->scheduled_end_date = date("Y-m-d h:i A", $wo->scheduled_end) . ' CDT';*/

/*$wo->scheduled_start_date = '2013/04/15 4:00 PM EST';
$wo->scheduled_start = strtotime($wo->scheduled_start_date);
$wo->scheduled_end_date = '2013/04/16 4:00 PM EST';
$wo->scheduled_end = strtotime($wo->scheduled_end_date);
$wo->pricing_type = 'internal';
$wo->location_country = 'USA';
$wo->location_type = 1;
$wo->location_address1 = "123 ABC St";
$wo->location_city = "Henderson";
$wo->location_state = "MT";
$wo->location_zip = "59866";

//$r = $wm->assignmentList();
//$r = $wm->locationTypesList();
//$r = $wm->assignmentCreate($wo);
$r = $wm->assignmentListUpdated(strtotime('04/16/2013 12:00 AM CDT'));
var_dump($r);*/
