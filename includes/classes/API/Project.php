<?php
/**
 * class API_Project
 * @package API_Project
 */
class API_Project {
	/**
	 * Project_ID
	 * @var int */
	var $Project_ID;

	/*
	 * Project_Name
	 * @var string */
	var $Project_Name;

	/*
	 * Client Purchase Order Number
	 * @var string */
	var $PO;

	/*
	 * Project_Manager
	 * @var string */
	var $Project_Manager;

	/*
	 * Project_Manager_Email
	 * @var string */
	var $Project_Manager_Email;

	/*
	 * Project_Manager_Phone
	 * @var string */
	var $Project_Manager_Phone;

	/*
	 * Project_Docs
	 * @var string */
	var $Project_Docs;

	/*
	 * Project_Other_Files
	 * @var string */
	var $Project_Other_Files;

	/*
	 * Project_Company_ID
	 * @var string */
	var $Project_Company_ID;

	/*
	 * CreatedBy
	 * @var string */
	var $CreatedBy;

	/*
	 * Resource_Coordinator
	 * @var string */
	var $Resource_Coordinator;

	/*
	 * Resource_Coord_Email
	 * @var string */
	var $Resource_Coord_Email;

	/*
	 * Resource_Coord_Phone
	 * @var string */
	var $Resource_Coord_Phone;

	/**
	 * Active
	 * @var boolean */
	var $Active;

	/*
	 * External_Files
	 * @var string */
	var $External_Files;

	/*
	 * From_Email
	 * @var string */
	var $From_Email;

	/*
	 * To_Email
	 * @var string */
	var $To_Email;

	/**
	 * Default_Amount
	 * @var int */
	var $Default_Amount;

	/**
	 * Qty_Devices
	 * @var int */
	var $Qty_Devices;

	/*
	 * Amount_Per
	 * @var string */
	var $Amount_Per;

	/**
	 * ShowOnReports
	 * @var boolean */
	var $ShowOnReports;

	/*
	 * Client_Name
	 * @var string */
	var $Client_Name;

	/*
	 * Client_Email
	 * @var string */
	var $Client_Email;

	/**
	 * Client_ID
	 * @var int */
	var $Client_ID;

	/*
	 * Client_Company_Name
	 * @var string */
	var $Client_Company_Name;

	/*
	 * Description
	 * @var string */
	var $Description;

	/*
	 * Requirements
	 * @var string */
	var $Requirements;

	/*
	 * Tools
	 * @var string */
	var $Tools;

	/*
	 * SpecialInstructions
	 * @var string */
	var $SpecialInstructions;

    /*
	 * SpecialInstructions
	 * @var string */
	var $SpecialInstructions_AssignedTech;

	/**
	 * UseMyRecruitEmailSettings
	 * @var boolean */
	var $UseMyRecruitEmailSettings;

	/**
	 * AutoBlastOnPublish
	 * @var boolean */
	var $AutoBlastOnPublish;

	/*
	 * RecruitmentFromEmail
	 * @var string */
	var $RecruitmentFromEmail;

	/**
	 * PricingRuleID
	 * @var int */
	var $PricingRuleID;

	/**
	 * AutoSMSOnPublish
	 * @var boolean */
	var $AutoSMSOnPublish;

	/**
	 * WO_Category_ID
	 * @var int */
	var $WO_Category_ID;

	/*
	 * SMS_BlastRadius
	 * @var string */
	var $SMS_BlastRadius;

	/**
	 * Push2Tech
	 * @var boolean */
	var $Push2Tech;

	/**
	 * Reminder24Hr
	 * @var boolean */
	var $Reminder24Hr;

	/**
	 * Reminder1Hr
	 * @var boolean */
	var $Reminder1Hr;

	/**
	 * ReminderAcceptance
	 * @var boolean */
	var $ReminderAcceptance;

	/**
	 * ReminderNotMarkComplete
	 * @var boolean */
	var $ReminderNotMarkComplete;

	/*
	 * General1
	 * @var string */
	var $General1;

	/*
	 * General2
	 * @var string */
	var $General2;

	/*
	 * General3
	 * @var string */
	var $General3;

	/*
	 * General4
	 * @var string */
	var $General4;

	/*
	 * General5
	 * @var string */
	var $General5;

	/*
	 * General6
	 * @var string */
	var $General6;

	/*
	 * General7
	 * @var string */
	var $General7;

	/*
	 * General8
	 * @var string */
	var $General8;

	/*
	 * General9
	 * @var string */
	var $General9;

	/*
	 * General10
	 * @var string */
	var $General10;

	/*
	 * Headline
	 * @var string */
	var $Headline;

	/*
	 * SubHeadline
	 * @var string */
	var $SubHeadline;

	/*
	 * CheckInOutEmail
	 * @var string */
	var $CheckInOutEmail;

	/*
	 * EmergencyName
	 * @var string */
	var $EmergencyName;

	/*
	 * EmergencyPhone
	 * @var string */
	var $EmergencyPhone;

	/*
	 * EmergencyEmail
	 * @var string */
	var $EmergencyEmail;

	/*
	 * TechnicalSupportName
	 * @var string */
	var $TechnicalSupportName;

	/*
	 * TechnicalSupportEmail
	 * @var string */
	var $TechnicalSupportEmail;

	/*
	 * TechnicalSupportPhone
	 * @var string */
	var $TechnicalSupportPhone;

	/*
	 * CheckInOutName
	 * @var string */
	var $CheckInOutName;

	/*
	 * CheckInOutNumber
	 * @var string */
	var $CheckInOutNumber;

	/**
	 * isProjectAutoAssign
	 * @var boolean */
	var $isProjectAutoAssign;

	/**
	 * isWorkOrdersFirstBidder
	 * @var boolean */
	var $isWorkOrdersFirstBidder;

	/**
	 * MinutesRemainPublished
	 * @var int */
	var $MinutesRemainPublished;

	/**
	 * MinimumSelfRating
	 * @var int */
	var $MinimumSelfRating;

	/**
	 * MaximumAllowableDistance
	 * @var int */
	var $MaximumAllowableDistance;

	/**
	 * ReminderIncomplete
	 * @var boolean */
	var $ReminderIncomplete;

	/**
	 * ReminderAll
	 * @var boolean */
	var $ReminderAll;

	/**
	 * CheckInCall
	 * @var boolean */
	var $CheckInCall;

	/**
	 * IVRPriceEach
	 * @var int */
	var $IVRPriceEach;

	/**
	 * IVRPriceBundle
	 * @var int */
	var $IVRPriceBundle;

	/**
	 * CheckOutCall
	 * @var boolean */
	var $CheckOutCall;

	/*
	 * ACSAuthBy
	 * @var string */
	var $ACSAuthBy;

	/*
	 * ACSAuthDate
	 * @var string */
	var $ACSAuthDate;

	/**
	 * ACSApplyTo
	 * @var int */
	var $ACSApplyTo;

	/**
	 * P2TPreferredOnly
	 * @var boolean */
	var $P2TPreferredOnly;

	/**
	 * NotifyOnBid
	 * @var boolean */
	var $NotifyOnBid;

	/**
	 * OOS_Trigger
	 * @var boolean */
	var $OOS_Trigger;

	/**
	 * InstallDesk_Trigger
	 * @var boolean */
	var $InstallDesk_Trigger;

	/**
	 * SignOffSheet_Required
	 * @var boolean */
	var $SignOffSheet_Required;

	/*
	 * P2TNotificationEmail
	 * @var string */
	var $P2TNotificationEmail;

	/**
	 * NotifyOnAssign
	 * @var boolean */
	var $NotifyOnAssign;

	/*
	 * AssignmentNotificationEmail
	 * @var string */
	var $AssignmentNotificationEmail;

	/**
	 * NotifyOnAutoAssign
	 * @var boolean */
	var $NotifyOnAutoAssign;

	/**
	 * FixedBid
	 * @var boolean */
	var $FixedBid;

	/**
	 * PcntDeduct
	 * @var boolean */
	var $PcntDeduct;
	
    var $StartDate;
    var $StartTime;
    var $EndDate;
    var $EndTime;
    var $StartRange;
    var $Duration;

	var $CredentialCertificationId;
	
    var $BidNotificationEmails;
    var $NotifyOnWorkDone;
    var $WorkDoneNotificationEmails;
    var $PartManagerUpdate_Required;
	
    var $EmailBlastRadius; /*327*/
    var $EmailBlastRadiusPreferredOnly; /*327*/
    var $PcntDeductPercent; /*13334*/
	var $Project_Logo;
	
	var $ClientCredential;

	var $CustomSignOff;
	var $CustomSignOff_Enabled;
	var $SignOff_Disabled;

    var $Type_ID;//13353
	
	var $Owner;
	
	var $AllowCustomCommSettings;
    

    
    var $ReceiveScheduleConflictNotification;
    var $ReceiveScheduleConflictNotificationEmails;
    
    var $StartType;
    var $WorkStartTime;
    var $WorkEndTime;
    var $EstimatedDuration;
    var $TechArrivalInstructions;
    var $ReceiveReVisitNotification;
    var $ReceiveReVisitNotificationEmails;
    
    var $ReminderCustomHr; //13483
    var $ReminderCustomHrChecked; //13483
    var $ReminderCustomHr_2; //13483
    var $ReminderCustomHrChecked_2; //13483
    var $ReminderCustomHr_3; //13483
    var $ReminderCustomHrChecked_3; //13483
    var $Qty_Visits;
    var $ReceiveTechQAMessageNotification;//260
    var $ReceiveTechQAMessageNotificationEmails;//620
    var $AllowDeVryInternstoObserveWork; 
    var $AllowDeVryInternstoPerformField; 
    var $ServiceTypeId; //13622
    var $EntityTypeId; //13622
    var $FSClientServiceDirectorId; //13622
    var $FSAccountManagerId; //13622
    
    var $EnableSiteStatusReporting; //13676
    var $ACSNotifiPOC; //13691
    var $ACSNotifiPOC_Phone; //13691
    var $ACSNotifiPOC_Email; //13691

    var $WorkAcceptedByTechEmailTo; //13763
    var $WorkConfirmedByTechEmailTo; //13763
    var $TechCheckedInEmailTo; //13763
    var $TechCheckedOutEmailTo; //13763
    var $ReceiveWorkAcceptedByTechEmail; //13763
    var $ReceiveWorkConfirmedByTechEmail; //13763
    var $ReceiveTechCheckedInEmail; //13763
    var $ReceiveTechCheckedOutEmail; //13763
	
	var $SiteContactReminderCall; //13732
    var $SiteContactReminderCallHours; //13732
    var $PushWM;

    var $RequiredStartDateTime; //13969
    var $RequiredEndDateTime; //13969
    
    public function __construct($id = null) {
		$this->Project_ID = $id;
	}

	public function toXMLElement($dom) {

		$pj = $dom->createElement('Project');
		$id = $dom->createAttribute('ID');
		$name = $dom->createAttribute('NAME');

		$id->appendChild($dom->createTextNode($this->Project_ID));
		$name->appendChild($dom->createTextNode($this->Project_Name));

		$pj->appendChild($id);
		$pj->appendChild($name);

		return $pj;
	}

    public function toXMLElementTect($dom) {
		$fieldList = $this;
		$root = $dom->createElement('Project');
		foreach ($this as $key => $value) {
			if (!array_key_exists($key, $fieldList) || $value === NULL) continue;
			if ( $key == "ClientCredential") {
				if (is_array($value)) $value = implode(",", $value);
			}
			
			if (is_bool($value))
				$value = $value === true ? "True" : "False";
			$parameter = $dom->createElement($key, htmlspecialchars($value, ENT_QUOTES));
			$root->appendChild($parameter);
		}
		$dom->appendChild($root);
		return $root;
	}

	public static function toXMLWithArray($array, $notUsed) {
		$dom = new DOMDocument("1.0");
		$root = $dom->createElement('Projects');
		foreach ($array as $data) {
			$root->appendChild($data->toXMLElement($dom));
		}
		$dom->appendChild($root);
		return $dom->saveXML();
	}
	
	public static function projectWithResult($result) {
		$project = new API_Project();
		if (!empty($result)) {
		    foreach ($result as $key=>$value) {
                if (!array_key_exists($key, $project)) continue;
				
				if ( $key == "ClientCredential") {
					$project->ClientCredential = $value;
					if (is_string($project->ClientCredential)) $project->ClientCredential = explode(",", $project->ClientCredential);
				}

                if ( strtolower(trim($key)) === 'active' ) {
                    if ( is_string($value) && strtolower(trim($value)) === 'false' ) {
                        $project->Active = FALSE;
                        continue;
                    }
                    if ( $value )   $project->Active = TRUE;
                    else            $project->Active = FALSE;
                    continue;
                }

				$project->$key = $value;
		    }
		}
		return $project;
	}

    public static function uploadFileFields()
    {
        return array('Project_Docs', 'Project_Other_Files', 'Project_Logo');
    }
       
}
