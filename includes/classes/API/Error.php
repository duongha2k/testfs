<?php
class API_Error {
    /**
	 * error code
     * @var int */
    var $code;

    /**
     * error type
     * @var string */
    var $type;

    /**
     * error message
     * @var string */
    var $message;
	
	public function __construct($code, $type, $message) {
		$this->code = $code;
		$this->type = $type;
		$this->message = $message;
	}

	public function toXMLElement($dom) {
		$id = $dom->createAttribute('id');
		$id->appendChild($dom->createTextNode($this->code));

		$type = $dom->createElement('type');
		$type->appendChild($dom->createTextNode($this->type));

		$description = $dom->createElement('description');
		$description->appendChild($dom->createTextNode($this->message));

		$error = $dom->createElement('error');
		$error->appendChild($id);
		$error->appendChild($type);
		$error->appendChild($description);
		return $error;
	}

	public static function toXMLWithArray($array) {
		$dom = new DOMDocument("1.0");
		$root = $dom->createElement('errors');
		foreach ($array as $data) {
			$root->appendChild($data->toXMLElement($dom));
		}
		$dom->appendChild($root);
		return $dom->saveXML();
	}
	
	public static function getAPIErrors() {
		return self::errorsToAPIErrors(Core_Api_Error::getInstance()->getErrorArray());
	}

	private static function errorsToAPIErrors($errors) {
		$ret = array();
		foreach ($errors as $key => $val) {
			$ret[] = new API_Error( $val["error_id"], $val["id"], self::humaniseAPIErrorMessage($val["description"]) );
		}
		return $ret;
	}

    /**
     * Translate Caspio data fields name to human message
     * @param string $message - original message
     * @return string
     * @see https://tickets.fieldsolutions.com/issues/12099
     * @author Artem Sukharev
     */
    private static function humaniseAPIErrorMessage( $message )
    {
        $search_and_replace = array(
            "WO_ID" => "Client Work Order ID #",
            "StartDate" => "Start Date",
            "StartTime" => "Start Time",
            "EndDate" => "End Date",
            "EndTime" => "End Time",
            "WO_Category_ID" => "Skill Category",
            "Headline" => 'Headline',
            "Call_Type" => 'Call Type',
            'PayMax' => 'Client Offer',
            'Tech_Bid_Amount' => 'Agreed Amount',
            'Qty_Devices' => 'Device Quantity',
            'Project_ID' => 'Project',
        );

        $message = str_replace(array_keys($search_and_replace), array_values($search_and_replace), $message);
        return $message;
    }
}