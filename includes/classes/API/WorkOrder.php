<?php

if (!defined("FLSVersion")):

/**
 * class API_WorkOrder
 * @package API_WorkOrder
 */
class API_WorkOrder extends API_Abstract_ClientWorkOrder {
}

else:

/**
 * class API_WorkOrder
 * @package API_WorkOrder
 */
class API_WorkOrder extends API_Abstract_ClientWorkOrder {
	/** 
	 * 
	 * @var boolean	 */
	var $ShortNotice;
	/** 
	 * 
	 * @var boolean	 */
	var $PreCall_1;
	/** 
	 * 
	 * @var boolean	 */
	var $PreCall_2;
	/** 
	 * 
	 * @var boolean	 */
	var $FLS_OOS;
	/** 
	 * 
	 * @var boolean	 */
	var $Unexpected_Steps;
	/** 
	 * 
	 * @var string	 */
	var $Unexpected_Desc;
	/** 
	 * 
	 * @var string	 */
	var $AskedBy;
	/** 
	 * 
	 * @var string	 */
	var $AskedBy_Name;
	/** 
	 * TODO What field in DB referrenced to?
	 * @var string	 */
	var $BU2FLSID;
	/** 
	 * 
	 * @var string	 */
	var $BU2Info;
	/** 
	 * 
	 * @var string	 */
	var $FLS_ID;
	/** 
	 * 
	 * @var string	 */
	var $SourceByDate;
    /**
     *
     * @var string
     */
    var $Penalty_Reason;
}

endif;
