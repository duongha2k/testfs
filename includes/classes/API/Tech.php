<?php
/**
 * class API_Tech
 * @package API_Tech
 */
class API_Tech {
	const MODE_TECH = 1;
	const MODE_ADMIN = 2;
	static private $readOnlyFields = NULL;
    //13700, 14018
	private static $fieldList = "TechID,FLSID,FirstName,LastName,PrimaryPhone,PrimPhoneType,SecondaryPhone,SecondPhoneType,PrimaryEmail,SecondaryEmail,City,State,Country,ZipCode,Latitude,Longitude,Address1,Address2,W9,Bg_Test_Pass_Lite,DrugTest_Pass,BG_Test_ResultsDate_Lite,Skills,Certifications,Cert_Hallmark_POS,NCR_Basic_Cert,Starbucks_Cert,FLSCSP_Rec,FS_Cert_Test_Pass,Cert_Maurices_POS,CORE_Cert,FLSstatus,HP_CertNum,SamsungCert,HP_CertProof,ISO_Affiliation,AcceptTerms,SMS_Number,PaymentMethod,BankCountry,BankAddress1,BankCity,BankState,BankZip,BankName,AccountName,RoutingNum,AccountNum,DepositType,BusinessStatement,MaskedAccountNum,MaskedRoutingNum,RegDate,TechSource,Date_TermsAccepted,No_Shows,Back_Outs,Deactivated,Deactivate_Reason,UserName,Password,FreeAccess,ISO_Affiliation_ID,Private,Pay_Company,Dispatch,MRA_Compliant,MRA_Date,CDM,L1,L2,L3,L4,DELL_DCSE_Reg,DCSE_Soft_Skills,DCSE_Lead,VA,SSA,Secret,Top_Secret,NACI,General1,General2,General3,General4,General5,General6,W2_Tech,StaffComments,FSPlusOneReg,FSPlusOneDateReg,BackgroundNotes,GovClear,InsuranceNotes,ExperienceNotes,CertificationsNotes,SkillsNotes,ToolsNotes,InterimSecClear,TopSecClear,FullSecClear,WillYouPassDrug,WillYouPassBG,WillYouPayDrugBG,PayPalRcvd,DisclRcvd,InfoLinkNotified,DrugPassed,DatePassDrug,DrugRefNo,Bg_Test_Req_Lite,Bg_Test_ReqDate_Lite,BG_Test_ResultsBy_Lite,Bg_Test_Req_Full,Bg_Test_ReqDate_Full,BG_Test_ResultsDate_Full,Bg_Test_Pass_Full,BG_Test_ResultsBy_Full,FLSCSP_RecDate,FLSCSP_RecBy,SATRecommendedAvg,SATRecommendedTotal,SATPerformanceAvg,SATPerformanceTotal,Qty_IMAC_Calls,Qty_FLS_Service_Calls,EmailContactOptOut,AllowText,PerformancePercent,PreferencePercent,FS_Mobile_Date,SilverDrugTest_Pass,SilverDrugPassed,DatePassedSilverDrug,HideBids,HideBidsDate,HideBidsByUserName,WMID";

	
/*	private static $writeable = array(
		'FirstName','LastName','PrimaryPhone','PrimPhoneType','SecondaryPhone','SecondPhoneType','PrimaryEmail','SecondaryEmail','City','State','Country','ZipCode','Address1','Address2','UserName','Password','AcceptTerms','SMS_Number','PaymentMethod','BankCountry','BankAddress1','BankCity','BankState','BankZip','BankName','AccountName','RoutingNum','AccountNum','DepositType','BusinessStatement','MaskedAccountNum','MaskedRoutingNum'
	);*/
	
	private static $internalFields = array('FREEACCESS','ISO_AFFILIATION_ID','ISO_AFFILIATION','PRIVATE','PAY_COMPANY','DISPATCH','MRA_COMPLIANT','MRA_DATE','CDM','L1','L2','L3','L4','DELL_DCSE_REG','DCSE_SOFT_SKILLS','DCSE_LEAD','VA','SSA','SECRET','TOP_SECRET','NACI','GENERAL1','GENERAL2','GENERAL3','GENERAL4','GENERAL5','GENERAL6','W2_TECH','STAFFCOMMENTS','FSPLUSONEREG','FSPLUSONEDATEREG','BACKGROUNDNOTES','GOVCLEAR','INSURANCENOTES','EXPERIENCENOTES','CERTIFICATIONSNOTES','SKILLSNOTES','TOOLSNOTES','INTERIMSECCLEAR','TOPSECCLEAR','FULLSECCLEAR','WILLYOUPASSDRUG','WILLYOUPASSBG','WILLYOUPAYDRUGBG','PAYPALRCVD','DISCLRCVD','INFOLINKNOTIFIED','DRUGPASSED
','DATEPASSDRUG','DRUGREFNO','BG_TEST_REQ_LITE','BG_TEST_REQDATE_LITE','BG_TEST_RESULTSBY_LITE','BG_TEST_REQ_FULL','BG_TEST_REQDATE_FULL','BG_TEST_RESULTSDATE_FULL','BG_TEST_RESULTSBY_FULL','FLSCSP_RECDATE','FLSCSP_RECBY','DEACTIVATE_REASON','FS_MOBILE_DATE');
	/** 
	 *
	 * @var int	 */
	public $TechID;

	/**
	 *
	 * @var int	 */
	public $FLSID;

	/** 
	 * 
	 * @var string	 */
	public $Firstname;

	/** 
	 * 
	 * @var string	 */
	public $Lastname;

	/** 
	 * 
	 * @var string	 */
    public $PrimaryPhone;

    /**
     * SecondaryPhone  
     * 
     * @var string
     * @access public
     */
    public $SecondaryPhone;
    

	/** 
	 * 
	 * @var string	 */
	public $PrimaryEmail;

    /**
     * SecondaryEmail
     *
     * @var string
     * @access public
     */
    public $SecondaryEmail;

    /**
     * Address1
     * 
     * @var string
     * @access public
     */
    public $Address1;
    /**
     * Address2 
     * 
     * @var string
     * @access public
     */
    public $Address2;
	
	/** 
	 * 
	 * @var string	 */
	public $City;

	/** 
	 * 
	 * @var string	 */
	public $State;

	/** 
	 * 
	 * @var string	 */
	public $Zipcode;

	/** 
	 * 
	 * @var float	 */
	public $Latitude;

	/** 
	 * 
	 * @var float	 */
	public $Longitude;
		
	/** 
	 * 
	 * @var boolean	 */
	public $W9;

	/** 
	 * 
	 * @var boolean	 */
	public $Bg_Test_Pass_Lite;
        
        /** 
	 * 
	 * @var boolean	 */
        public $DrugTest_Pass;
        /** 
	 * 
	 * @var string	 */
	public $Bg_Test_ResultsDate_Lite;
        public $BG_Test_ResultsDate_Lite;

	/** 
	 * 
	 * @var string	 */
	public $Skills;

	/** 
	 * 
	 * @var string	 */
	public $Certifications;

	/** 
	 * 
	 * @var boolean	 */
	public $Cert_Hallmark_POS;

	/** 
	 * 
	 * @var boolean	 */
	public $NCR_Basic_Cert;

	/** 
	 * 
	 * @var boolean	 */
	public $Starbucks_Cert;

	/** 
	 * 
	 * @var boolean	 */
	public $FLSCSP_Rec;

	/** 
	 * 
	 * @var boolean	 */
	public $FS_Cert_Test_Pass;

	/** 
	 * 
	 * @var boolean	 */
	public $Cert_Maurices_POS;

	/** 
	 * 
	 * @var boolean	 */
	public $CORE_Cert;

	/** 
	 * 
	 * @var string	 */
	public $FLSstatus;

	/** 
	 * 
	 * @var boolean	 */
	public $HP_Cert;

	/** 
	 * 
	 * @var boolean	 */
	public $SamsungCert;

	/** 
	 * 
	 * @var boolean	 */
	public $DirectDeposit;

	/** 
	 * 
	 * @var boolean	 */
	public $isDenied;

	/** 
	 * 
	 * @var boolean	 */
	public $isPreferred;
	
	/** 
	 * 
	 * @var string	 */
	public $PaymentMethod;
	
    public $BankCountry;
    public $BankAddress1;
    public $BankCity;
    public $BankState;
    public $BankZip;
    public $BankName;
    public $AccountName;
    public $RoutingNum;
    public $AccountNum;
    public $DepositType;
	
	
	
	/** 
	 * 
	 * @var int	 */
	public $Qty_IMAC_Calls;
	
	public $AcceptTerms;
	
	public $Qty_FLS_Service_Calls;
	
	public $No_Shows;
	
	public $SATRecommendedAvg;
	
	public $SATRecommendedTotal;
	
	public $SATPerformanceAvg;
	
	public $SATPerformanceTotal;
	public $PerformancePercent;
	public $PreferencePercent;
	
	public $PrimPhoneType;
	
	public $SecondPhoneType;
	
	public $Back_Outs;
	
	public $ISO_Affiliation;

	public $SMS_Number;

	public $FSPlusOneLifetimePoints;
	public $FSPlusOneAvailablePoint;
	public $BusinessStatement;
	public $MaskedRoutingNum;
	public $MaskedAccountNum;
	public $Country;

	public $RegDate;
	public $TechSource;
	public $Date_TermsAccepted;
	public $Deactivated;
	
	public $UserName;
	public $Password;

	public $FreeAccess;

	public $ISO_Affiliation_ID;
	public $Private;
	public $Pay_Company;
	public $Dispatch;

	public $MRA_Compliant;
	public $MRA_Date;
	public $CDM;
	public $L1;
	public $L2;
	public $L3;
	public $L4;
	public $DELL_DCSE_Reg;
	public $DCSE_Soft_Skills;
	public $DCSE_Lead;
	public $VA;
	public $SSA;
	public $Secret;
	public $Top_Secret;
	public $NACI;
	public $General1;
	public $General2;
	public $General3;
	public $General4;
	public $General5;
	public $General6;
	
	public $StaffComments;
	
	public $FSPlusOneReg;
	public $FSPlusOneDateReg;
	
	public $BackgroundNotes;
	public $GovClear;
	public $InsuranceNotes;
	public $ExperienceNotes;
	public $CertificationsNotes;
	public $SkillsNotes;
	public $ToolsNotes;
	
	public $DellInterest;
	public $TechApproval;

	public $FLSStatus;
	public $FLStitle;
	public $FLSBadge;
	
	public $InterimSecClear;
	public $TopSecClear;
	public $FullSecClear;
	
	public $WillYouPassDrug;
	public $WillYouPassBG;
	public $WillYouPayDrugBG;
	public $PayPalRcvd;
	public $DisclRcvd;
	public $InfoLinkNotified;
	public $DrugPassed;
	public $DatePassDrug;
	public $DrugRefNo;
	public $Bg_Test_Req_Lite;
	public $Bg_Test_ReqDate_Lite;
	public $BG_Test_ResultsBy_Lite;
	public $Bg_Test_Req_Full;
	public $Bg_Test_ReqDate_Full;
	public $Bg_Test_Pass_Full;
	public $BG_Test_ResultsDate_Full;
	public $BG_Test_ResultsBy_Full;	

	public $FS_Mobile_Date;

	public $FLSCSP_RecDate;
	public $FLSCSP_RecBy;

	public $Deactivate_Reason;
	public $AllowText;
	public $EmailContactOptOut;

	public $CertNumber;

    public $SilverDrugTest_Pass;//13700
    public $SilverDrugPassed;//13700
    public $DatePassedSilverDrug;//13700

    public $HideBids;//14018
    public $HideBidsDate;//14018
    public $HideBidsByUserName;//14018
	
	public $isWMTech;
	public $WMID;

	protected $mode = 1;
	
	public function setMode($mode) {
		$this->mode = $mode;
	}

	public function lookupID($id, $companyID = null, $returnDeactivated = false, $certNumberByID = false) {
		if (empty($id)) return false;
		$d = $returnDeactivated ? "" : "AND Deactivated = '0'";

        if ( is_array($id) ) {
        	foreach ($id as $k=>$v) {
                if (empty($v)) unset($id[$k]);
            }

        	$idSet = join("','", $id);
			if (count($id) == 0) return array();

        	if (defined("FLSVersion") && FLSVersion == "FLS") {
				return $this->doTechLookupList("FLSID IN ('".$idSet."') OR TechID IN ('".$idSet."') $d", $companyID);
        	}
        	else {
				return $this->doTechLookupList("TechID IN ('".$idSet."') $d", $companyID, $certNumberByID);
				//return $this->doTechLookupList(($certNumberByID !== false ? "c.TechID" : "TechID") . " IN ('".$idSet."') $d", $companyID, $certNumberByID);
        	}
        } else {
        	if (defined("FLSVersion") && FLSVersion == "FLS") {
	            return $this->doTechLookup("(FLSID = '$id' OR TechID = '$id') $d", $companyID);
        	}else {
	            return $this->doTechLookup("TechID = '$id' $d", $companyID);
        	}
        }
	}
    //13458
    public function lookupFLSID($id, $companyID = null) {
        if (empty($id)) return false;        
        return $this->doTechLookup("FLSID ='$id'",$companyID);      
    }
	/**
     * doTechLookupList
     * 
     * @param mixed $criteria 
     * @param string $companyID
     * @access private
     * @return array of Tech objects
     */
    private function doTechLookupList( $criteria, $companyID = null, $certNumberByID = false) {
//var_dump ($criteria); exit;

	$fieldListMap = explode(",", API_Tech::$fieldList);
	$fieldListMap = array_combine($fieldListMap, $fieldListMap);

	if (!empty($companyID)) {
//		$fieldListMap["isDenied"] = "ISNULL((SELECT TOP 1 1 FROM Deactivated_Techs_By_Clients WHERE Company_ID = '$companyID' AND Deactivated_Techs_By_Clients.TechID = TR_Master_List.TechID), 0)";
	//	$fieldListMap["isPreferred"] = "ISNULL((SELECT TOP 1 PreferLevel FROM Client_Preferred_Techs WHERE CompanyID = '$companyID' AND TechID = Tech_ID), 0)";
	}

	//$list = Core_Caspio::caspioSelectWithFieldListMap(TABLE_MASTER_LIST, $fieldListMap, $criteria, "");
		$db = Core_Database::getInstance();
    	try{
			$select = $db->select();
			$select->from(Core_Database::TABLE_TECH_BANK_INFO, $fieldListMap);
			$select->where($criteria);
		
			$list = $db->fetchAll($select);

			if ($certNumberByID) {
				$where = explode (" AND ", $criteria);
				$select = $db->select();
				$select->from("tech_certification");
				$select->where($where[0] . " AND certification_id = ?", $certNumberByID);
				$cert_records = $db->fetchAll($select);
				/*
				$select = $db->select();
				$select->from(Core_Database::TABLE_TECH_BANK_INFO, $fieldListMap);
				$select->joinLeft (array ("c" => "tech_certification"), "TechBankInfo.TechID = c.TechID", "c.number");
				$select->where($criteria . " AND c.certification_id = ?", $certNumberByID);
		    
				$list = $db->fetchAll($select);
				*/
			}
		}catch(Exception $e){
    		error_log($e->getMessage());
    	}

        if ( empty($list) )
            return false;

		$certs = array ();
		if (!empty ($cert_records)) {
			foreach ($cert_records as $cert) {
				$certs[$cert["TechID"]] = $cert["number"];
			}
		}

		$techList = array();
        foreach ( $list as $_tech ) {
			$add_record = false;

			if ($certNumberByID) {
				if ($_tech["number"] == $certNumberByID)
					$add_record = true;
				else
					$add_record = !isset ($techList[$_tech['TechID']]);
			}
			else {
				$add_record = !isset ($techList[$_tech['TechID']]);
			}

			if ($add_record) {
            $tech = new self();
            $tech->TechID           = $_tech['TechID'];
            $tech->FLSID           = $_tech['FLSID'];
            $tech->Firstname        = $_tech['FirstName'];
            $tech->Lastname         = $_tech['LastName'];
            $tech->PrimaryPhone     = $_tech['PrimaryPhone'];
            $tech->SecondaryPhone   = $_tech['SecondaryPhone'];
            $tech->PrimaryEmail     = $_tech['PrimaryEmail'];
            $tech->SecondaryEmail   = $_tech['SecondaryEmail'];
            $tech->Address1         = $_tech['Address1'];
            $tech->Address2         = $_tech['Address2'];
            $tech->City             = $_tech['City'];
            $tech->State            = $_tech['State'];
            $tech->Country			= $_tech['Country'];
            $tech->Zipcode          = $_tech['ZipCode'];
            $tech->Latitude         = $_tech['Latitude'];
            $tech->Longitude        = $_tech['Longitude'];
            $tech->W9        = $_tech['W9'];
            $tech->Bg_Test_Pass_Lite        = strtolower($_tech['Bg_Test_Pass_Lite']) === "pass";
            $tech->DrugTest_Pass        = strtolower($_tech['DrugTest_Pass']) === "pass";
			$tech->Bg_Test_Pass_Full        = strtolower($_tech['Bg_Test_Pass_Full']) === "pass";
            $tech->BG_Test_ResultsDate_Lite        = $_tech['BG_Test_ResultsDate_Lite'];
            $tech->Bg_Test_ResultsDate_Lite        = $_tech['BG_Test_ResultsDate_Lite'];
            $tech->FS_Mobile_Date = $_tech['FS_Mobile_date'];
            $tech->Skills        = $_tech['Skills'];
            $tech->Certifications = $_tech['Certifications'];
            $tech->Cert_Hallmark_POS = strtolower($_tech['Cert_Hallmark_POS']) === "true";
            $tech->NCR_Basic_Cert = strtolower($_tech['NCR_Basic_Cert']) === "true";
            $tech->Starbucks_Cert = strtolower($_tech['Starbucks_Cert']) === "true";
            $tech->FLSCSP_Rec = strtolower($_tech['FLSCSP_Rec']) === "true";
            $tech->FS_Cert_Test_Pass = strtolower($_tech['FS_Cert_Test_Pass']) === "true";
            $tech->Cert_Maurices_POS = strtolower($_tech['Cert_Maurices_POS']) === "true";
            $tech->CORE_Cert = strtolower($_tech['CORE_Cert']) === "true";
            $tech->FLSstatus = $_tech['FLSstatus'];
			$tech->HP_Cert = !empty($_tech['HP_CertNum']) && !empty($_tech['HP_CertProof']);
            $tech->SamsungCert = strtolower($_tech['SamsungCert']) === "true";
            $tech->AcceptTerms = strtolower($_tech['AcceptTerms']) === "yes";
			$tech->isWMTech = $_tech['AcceptTerms'] === 'WM';
            $tech->PrimPhoneType = $_tech['PrimPhoneType'];
            $tech->SecondPhoneType = $_tech['SecondPhoneType'];

            $tech->SMS_Number = $_tech['SMS_Number'];
            $tech->PaymentMethod = $_tech['PaymentMethod'];
            $tech->BankCountry = $_tech['BankCountry'];
    		$tech->BankAddress1 = $_tech['BankAddress1'];
    		$tech->BankCity = $_tech['BankCity'];
    		$tech->BankState = $_tech['BankState'];
    		$tech->BankZip = $_tech['BankZip'];
    		$tech->BankName = $_tech['BankName'];
    		$tech->AccountName = $_tech['AccountName'];
		    $tech->RoutingNum = $_tech['RoutingNum'];
		    $tech->AccountNum = $_tech['AccountNum'];
		    $tech->DepositType = $_tech['DepositType'];
		    $tech->BusinessStatement = $_tech['BusinessStatement'];
		    $tech->MaskedRoutingNum = $_tech['MaskedRoutingNum'];
		    $tech->MaskedAccountNum = $_tech['MaskedAccountNum'];

		    $tech->SATRecommendedAvg = $_tech['SATRecommendedAvg'];
		    $tech->SATRecommendedTotal = $_tech['SATRecommendedTotal'];
		    $tech->SATPerformanceAvg = $_tech['SATPerformanceAvg'];
		    $tech->SATPerformanceTotal  = $_tech['SATPerformanceTotal'];
            
            $tech->PerformancePercent = $_tech['PerformancePercent'];
            $tech->PreferencePercent = $_tech['PreferencePercent'];

		    $tech->No_Shows  = $_tech['No_Shows'];
		    $tech->Back_Outs  = $_tech['Back_Outs'];
		    $tech->Qty_IMAC_Calls = $_tech['Qty_IMAC_Calls'];
		    $tech->Qty_FLS_Service_Calls  = $_tech['Qty_FLS_Service_Calls '];
		    $tech->EmailContactOptOut = $_tech['EmailContactOptOut'];
		    $tech->AllowText = $_tech['AllowText'];
            //13700
            $tech->SilverDrugTest_Pass = $_tech['SilverDrugTest_Pass'];
            $tech->SilverDrugPassed = $_tech['SilverDrugPassed'];
            $tech->DatePassedSilverDrug = $_tech['DatePassedSilverDrug'];
            //end 13700

		$points = Core_FSPlusOne::getTechPointSummary($tech->TechID);
		if ($points) {
			$tech->FSPlusOneLifetimePoints = $points['allPoints'];
			$tech->FSPlusOneAvailablePoint = $points['currentPoints'];
		}
		else {
			$tech->FSPlusOneLifetimePoints = 0;
			$tech->FSPlusOneAvailablePoint = 0;
		}

		$tech->isDenied = empty($companyID) ? false : Core_Tech::isClientDenied($tech->TechID, $companyID);
		$tech->isPreferred = empty($companyID) ? false : Core_Tech::isTechPreferred($companyID, $tech->TechID);

			$tech->CertNumber = isset ($certs[$tech->TechID]) ? $certs[$tech->TechID] : "";

            $techList[$tech->TechID]= $tech;
			}
        }

        return $techList;
    }
	
	public function doTechLookup($criteria, $companyID = null) {
		$fieldListMap = explode(",", API_Tech::$fieldList);
		$fieldListMap = array_combine($fieldListMap, $fieldListMap);

		if (!empty($companyID)) {
//			$fieldListMap["isDenied"] = "ISNULL((SELECT TOP 1 1 FROM Deactivated_Techs_By_Clients WHERE Company_ID = '$companyID' AND Deactivated_Techs_By_Clients.TechID = TR_Master_List.TechID), 0)";
		//	$fieldListMap["isPreferred"] = "ISNULL((SELECT TOP 1 PreferLevel FROM Client_Preferred_Techs WHERE CompanyID = '$companyID' AND TechID = Tech_ID), 0)";
		}

		//$user = Core_Caspio::caspioSelectWithFieldListMap(TABLE_MASTER_LIST, $fieldListMap, $criteria, "");
		
		$db = Core_Database::getInstance();
    	try{
		    $select = $db->select();
		    $select->from(Core_Database::TABLE_TECH_BANK_INFO, $fieldListMap)
		    		->where($criteria);
		    		
		    $user = $db->fetchAll($select);
    	}catch(Exception $e){
    		error_log($e->getMessage());
    	}
		
		if (empty($user)) return false;
		$info = $user[0];
		$this->TechID = $info['TechID'];
		$this->FLSID = $info['FLSID'];
		$this->Firstname = $info['FirstName'];
		$this->Lastname = $info['LastName'];
		$this->PrimaryPhone = $info['PrimaryPhone'];
		$this->SecondaryPhone = $info['SecondaryPhone'];
		$this->PrimaryEmail = $info['PrimaryEmail'];
		$this->SecondaryEmail = $info['SecondaryEmail'];
		$this->Address1 = $info['Address1'];
		$this->Address2 = $info['Address2'];
		$this->City = $info['City'];
		$this->State = $info['State'];
		$this->Country = $info['Country'];
		$this->Zipcode = $info['ZipCode'];
		$this->Latitude = $info['Latitude'];
		$this->Longitude = $info['Longitude'];
		$this->W9        = $info['W9'];
		$this->Bg_Test_Pass_Lite        = strtolower($info['Bg_Test_Pass_Lite']) === "pass";
                $this->DrugTest_Pass        = strtolower($info['DrugTest_Pass']) === "pass";
		$this->Bg_Test_Pass_Full        = strtolower($info['Bg_Test_Pass_Full']) === "pass";
		$this->BG_Test_ResultsDate_Lite        = $info['BG_Test_ResultsDate_Lite'];
		$this->Bg_Test_ResultsDate_Lite        = $info['BG_Test_ResultsDate_Lite'];
		if (isset ($info['FS_Mobile_date'])) $this->FS_Mobile_Date = $info['FS_Mobile_date'];
		$this->Skills        = $info['Skills'];
		$this->Certifications = $info['Certifications'];
		$this->Cert_Hallmark_POS = strtolower($info['Cert_Hallmark_POS']) === "true";
		$this->NCR_Basic_Cert = strtolower($info['NCR_Basic_Cert']) === "true";
		$this->Starbucks_Cert = strtolower($info['Starbucks_Cert']) === "true";
		$this->FLSCSP_Rec = strtolower($info['FLSCSP_Rec']) === "true";
		$this->FS_Cert_Test_Pass = strtolower($info['FS_Cert_Test_Pass']) === "true";
		$this->Cert_Maurices_POS = strtolower($info['Cert_Maurices_POS']) === "true";
		$this->CORE_Cert = strtolower($info['CORE_Cert']) === "true";
		$this->FLSstatus = $info['FLSstatus'];
		//$this->HP_Cert = !empty($info['HP_CertNum']);
		$this->HP_Cert = !empty($info['HP_CertNum']) && !empty($info['HP_CertProof']);
		$this->SamsungCert = strtolower($info['SamsungCert']) === "true";
		$this->AcceptTerms = strtolower($info['AcceptTerms']) === "yes";
		$this->isWMTech = $info['AcceptTerms'] === 'WM';
		$this->PrimPhoneType = $info['PrimPhoneType'];
		$this->SecondPhoneType = $info['SecondPhoneType'];

		$this->SMS_Number = $info['SMS_Number'];
		$this->PaymentMethod = $info['PaymentMethod'];
		$this->BankCountry = $info['BankCountry'];
    	$this->BankAddress1 = $info['BankAddress1'];
    	$this->BankCity = $info['BankCity'];
    	$this->BankState = $info['BankState'];
    	$this->BankZip = $info['BankZip'];
    	$this->BankName = $info['BankName'];
    	$this->AccountName = $info['AccountName'];
		$this->RoutingNum = $info['RoutingNum'];
		$this->AccountNum = $info['AccountNum'];
		$this->DepositType = $info['DepositType'];
		$this->BusinessStatement = $info['BusinessStatement'];
		$this->MaskedRoutingNum = $info['MaskedRoutingNum'];
		$this->MaskedAccountNum = $info['MaskedAccountNum'];

		$this->SATRecommendedAvg = $info['SATRecommendedAvg'];
		$this->SATRecommendedTotal = $info['SATRecommendedTotal'];
		$this->SATPerformanceAvg = $info['SATPerformanceAvg'];
		$this->SATPerformanceTotal  = $info['SATPerformanceTotal'];
		$this->No_Shows  = $info['No_Shows'];
		$this->Back_Outs  = $info['Back_Outs'];
		$this->Qty_IMAC_Calls = $info['Qty_IMAC_Calls'];
		if (isset ($info['Qty_FLS_Service_Calls'])) $this->Qty_FLS_Service_Calls  = $info['Qty_FLS_Service_Calls'];

		$points = Core_FSPlusOne::getTechPointSummary($this->TechID);
		if ($points) {
			$this->FSPlusOneLifetimePoints = $points['allPoints'];
			$this->FSPlusOneAvailablePoint = $points['currentPoints'];
		}
		else {
			$this->FSPlusOneLifetimePoints = 0;
			$this->FSPlusOneAvailablePoint = 0;
		}

		$this->isDenied = empty($companyID) ? false : Core_Tech::isClientDenied($this->TechID, $companyID);
		$this->isPreferred = empty($companyID) ? false : Core_Tech::isTechPreferred($companyID, $this->TechID);

		$otherInfo = $this->getTechMysqlInfo($this->TechID);

		$this->DirectDeposit = strtolower($otherInfo["PaymentMethod"]) === "direct deposit";
		
		$date = new Zend_Date($info['RegDate'], "yyyy-MM-dd HH:mm:ss");
		$this->RegDate = $date->toString('yyyy-MM-dd HH:mm');
		$this->TechSource = $info['TechSource'];
		
		$date = new Zend_Date($info['Date_TermsAccepted'], "yyyy-MM-dd HH:mm:ss");
		$this->Date_TermsAccepted = $date->toString('yyyy-MM-dd HH:mm');

/*		$this->No_Shows = $info['No_Shows'];
		$this->UserName = $info['UserName'];
		$this->Password = $info['Password'];

		$this->Deactivated = $info['Deactivated'];
		$this->Deactivate_Reason = $info['Deactivate_Reason'];

		$this->FreeAccess = $info['FreeAccess'];

		$this->ISO_Affiliation_ID = $info['ISO_Affiliation_ID'];
		$this->Private = $info['Private'];
		$this->Pay_Company = $info['Pay_Company'];
		$this->Dispatch = $info['Dispatch'];
		
		$this->MRA_Compliant = $info['MRA_Compliant'];
		$this->MRA_Date = $info['MRA_Date'];
		$this->CDM = $info['CDM'];
		$this->L1 = $info['L1'];
		$this->L2 = $info['L2'];
		$this->L3 = $info['L3'];
		$this->L4 = $info['L4'];
		$this->DELL_DCSE_Reg = $info['DELL_DCSE_Reg'];
		$this->DCSE_Soft_Skills = $info['DCSE_Soft_Skills'];
		$this->DCSE_Lead = $info['DCSE_Lead'];
		$this->VA = $info['VA'];
		$this->SSA = $info['SSA'];
		$this->Secret = $info['Secret'];
		$this->Top_Secret = $info['Top_Secret'];
		$this->NACI = $info['NACI'];
		$this->General1 = $info['General1'];
		$this->General2 = $info['General2'];
		$this->General3 = $info['General3'];
		$this->General4 = $info['General4'];
		$this->General5 = $info['General5'];
		$this->General6 = $info['General6'];
		$this->W2_Tech = $info['W2_Tech'];
		$this->StaffComments = $info['StaffComments'];
		$this->FSPlusOneReg = $info['FSPlusOneReg'];*/
		$date = new Zend_Date($info['FSPlusOneDateReg']);
		$this->FSPlusOneDateReg = $date->toString('yyyy-MM-dd hh:mm');
		
/*		$this->BackgroundNotes = $info['BackgroundNotes'];
		$this->GovClear = $info['GovClear'];
		$this->InsuranceNotes = $info['InsuranceNotes'];
		$this->ExperienceNotes = $info['ExperienceNotes'];
		$this->CertificationsNotes = $info['CertificationsNotes'];
		$this->SkillsNotes = $info['SkillsNotes'];
		$this->ToolsNotes = $info['ToolsNotes'];
		
		$this->InterimSecClear = $info['InterimSecClear'];
		$this->TopSecClear = $info['InterimSecClear'];
		$this->FullSecClear = $info['InterimSecClear'];*/
	
		$vars = get_class_vars(__CLASS__);
		foreach ($vars as $field => $v) {
			if ($field == "fieldList" || $field == "internalFields" || $field == "readOnlyFields") continue;
			if (!empty($this->$field)) continue;
			if (isset ($info[$field])) $this->$field = $info[$field];
		}

		if ($this->mode == self::MODE_TECH) {
			$this->excludeFields();
		}
		
		return true;
	}

	private function getTechMysqlInfo($id) {
		$db = Zend_Registry::get('DB');
		$select = $db->select();
		$select->from("TechBankInfo", "PaymentMethod")
			->where("TechID = (?)", $this->TechID)
			->limit(1,0);

		$result = false;
		try {
			$result = $select->query()->fetchAll();
		} catch (Exception $e) {
			$result = false;
		}
		
		return $result ? $result[0] : false;
		
		
	}
	
	
	/**
	 * Make result array of objects item
	 * @param array $result
	 * @param object $object
	 * @author Alexander Cheshchevik 
	 * @return array <multitype: API_Tech>
	 */
	public static function techOrderArray($result, $object) 
	{
	    // TODO Better rewrite this functions to one abstract class
	    $techs = array();
	    if (is_array($result) && !empty($result[0])) {
			foreach ($result as $key => $value) {
				$clone = clone $object;
				$techs[] = self::techWithResult($value, $clone);
			}
	    }
		return $techs;
	}
	

	/**
	 * Prepare object for result array
	 * @param array $result
	 * @param object $object
	 * @author Alexander Cheshchevik 
	 * @return API_Tech
	 */
	public static function techWithResult($result, $object = NULL) 
	{
	    if ($object == NULL)
            $object = new API_Tech(NULL);
	    if (!empty($result)) {
		    foreach ($result as $k => $val) {
		        $field = $k;
		        
		        if ($field == 'FirstName') {
				    $field = 'Firstname';
				} else if ($field == 'LastName') {
				    $field = 'Lastname';
				} else if ($field == 'ZipCode') {
				    $field = 'Zipcode';
				}
		        
				if ( !property_exists($object, $field )) continue;
				if (strtolower($result[$k]) == "true") $result[$k] = true;
				if (strtolower($result[$k]) == "false") $result[$k] = false;
				
			    $object->$field = $result[$k];
		    }
	    }
	    return $object;
	}
	
	
	public static function getFiledList()
	{
	    return self::$fieldList;
	}
	
    public static function getWritableFields()
    {
        return self::$writeable;
    }
	
	public function excludeFields() {
		$vars = get_class_vars(__CLASS__);
		foreach ($vars as $field => $v) {
			if (in_array(strtoupper($field), self::$internalFields))
				unset($this->$field);
		}
	}
	
}
