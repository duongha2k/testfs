<?php
/**
 * class API_Project
 * @package API_Project
 */
class API_Country {

	/*
	 * Code
	 * @var string */
	var $Code;

	/*
	 * Name
	 * @var string */
	var $Name;

	
	public static function countryWithResult($result) {
		$country = new API_Country();
		if (!empty($result)) {
		    foreach ($result as $key => $value) {
                if (!property_exists($country, $key)) continue;
				$country->$key = $value;
		    }
		}
		return $country;
	}
}
