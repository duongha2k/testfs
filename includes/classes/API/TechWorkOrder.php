<?php
/**
 * class API_TechWorkOrder
 * @package API_TechWorkOrder
 */
class API_TechWorkOrder extends API_Abstract_WorkOrder {
	static private $readOnlyFields = NULL;

	public static function internalFields() {
		return array('USERNAME','PASSWORD','PAID','PAYAPPROVE','CHECK_NUMBER','MOVEDTOMYSQL','ATTACHEDFILE',
    	                        'USEMYRECRUITEMAILSETTINGS','PRICINGRULEID','CALCULATEDINVOICE','PRICINGCALCULATIONTEXT',
    	                        'TECHNICIANFEE','PRICINGRULEAPPLIED','URGENT','FSSOURCINGSUPPORT','PRICINGRAN',
    	                        'REMINDERUSEWOSETTING','CALCULATEDTOTALTECHPAY','PAYAMOUNT_FINAL',
    	                        'PRICINGACCUMULATOR', 'PRICINGACCUMULATOR2', 'FS_INTERNALCOMMENTS', "DEACTIVATED_DUMMYTECHFLAG", "TBPAY","FLS_ID");
	}

    static private $writeable = array(
        'CheckedIn',
        'calculatedTechHrs',
        'Unexpected_Steps',
        'Unexpected_Desc',
        'AskedBy',
        'AskedBy_Name',
        'WorkOrderReviewed',    // bool;        accept
        'TechCheckedIn_24hrs',  // bool;        confirm
        'TechMarkedComplete',   // bool;        complete
        'Date_In',              // date/time;   check in date
        'Date_Out',             // date/time;   check out date
        'Time_In',              // text(255);   check in time
        'Time_Out',             // text(255);   check out date
        'TechComments',         // text(64000); comments
        'TechMiles',            // int;         mileage
        'ContactHD',            // bool;        helpdesk contacted
        'HDPOC'                // text(255);   who was contacted
    );
    
    /**
     * Status
     * 
     * @var float
     * @access public
     */
    public $Status;
    /**
     * Pic1
     *
     * @var float
     * @access public
     */
   // public $Pic1;
    /**
     * Pic2
     *
     * @var float
     * @access public
     */
    public $Pic2;
    /**
     * Pic3
     *
     * @var float
     * @access public
     */
    public $Pic3;
    /**
     * ReminderAcceptance
     *
     * @var boolean
     * @access public
     */
    public $ReminderAcceptance;
    /**
     * Reminder24Hr
     *
     * @var boolean
     * @access public
     */
    public $Reminder24Hr;
    /**
     * CheckedIn
     *
     * @var boolean
     * @access public
     */
    public $ReminderAll;
    /**
     * CheckedIn
     *
     * @var boolean
     * @access public
     */
    public $CheckInCall;
    /**
     * CheckedIn
     *
     * @var boolean
     * @access public
     */
    public $CheckOutCall;
    /**
     * CheckedIn
     *
     * @var boolean
     * @access public
     */
    public $CheckedIn;
    /**
     * Site_Complete
     *
     * @var boobooleanl
     * @access public
     */
    public $Site_Complete;
    /**
     * Distance
     *
     * @var float
     * @access public
     */
    public $Distance;
    /**
     * Unexpected_Steps
     *
     * @var mixed
     * @access public
     */
    public $Unexpected_Steps;
    /**
     * Unexpected_Desc 
     * 
     * @var mixed
     * @access public
     */
    public $Unexpected_Desc;
    /**
     * AskedBy 
     * 
     * @var mixed
     * @access public
     */
    public $AskedBy;
    /**
     * AskedBy_Name 
     * 
     * @var mixed
     * @access public
     */
    public $AskedBy_Name;
    /**
     * ContactHD 
     * 
     * @var mixed
     * @access public
     */
    public $ContactHD;
    /**
     * HDPOC 
     * 
     * @var mixed
     * @access public
     */
    public $HDPOC;
    /**
     * Company_Name 
     * 
     * @var mixed
     * @access public
     */
    public $Company_Name;
    /**
     * Company_ID 
     * 
     * @var mixed
     * @access public
     */
    public $Company_ID;

    /**
     * Date_Completed
     *
     * @var mixed
     * @access public
     */
    public $Date_Completed;

    /**
     * calculatedTechHrs
     *
     * @var mixed
     * @access public
     */
    public $calculatedTechHrs;

    /**
     * DateIncomplete
     *
     * @var mixed
     * @access public
     */
    public $DateIncomplete;
    /**
     * DateApproved
     *
     * @var mixed
     * @access public
     */
    public $DateApproved;
    /**
     * Net_Pay_Amount
     *
     * @var mixed
     * @access public
     */
    public $Net_Pay_Amount;
    /**
     * FLS_ID
     *
     * @var mixed
     * @access public
     */
    public $FLS_ID;

    public $MinimumSelfRating;

    public $MaximumAllowableDistance;

    /**
     * __construct
     *
     * @param mixed $id
     * @access public
     * @return void
     */
    public function __construct( $id = null )
    {
        if ( !empty($id) ) {
            parent::__construct($id);
        }
    }
    /**
     * getWritableFields 
     * 
     * @access public
     * @return void
     */
    static public function getWritableFields()
    {
        return self::$writeable;
    }
    /**
     * techUploadFileFields 
     * 
     * Return fields' names for file upload
     *
     * @static
     * @access public
     * @return array
     */
    static public function techUploadFileFields()
    {
        return array();
    }
    /**
     * readOnlyFields 
     * 
     * @static
     * @access public
     * @return void
     */
	static public function readOnlyFields() {
        if ( !self::$readOnlyFields ) {
            $r  = new ReflectionClass(__CLASS__);
            $readOnlyFields = array();

            foreach ($r->getProperties(ReflectionProperty::IS_PUBLIC) as $prop)
                if ( !in_array($prop->getName(), $wo->getWritableFields()) )
                    $readOnlyFields[$prop->getName()] = strtoupper($prop->getName());

            self::$readOnlyFields = $readOnlyFields;
        }

		return self::$readOnlyFields;
	}
}

