<?php
/**
 * class API_WOCategory
 * @package API_WOCategory
 */
class API_WOCategory {
	/** 
	 * id
	 * @var int	 */
	var $Category_ID;

	/** 
	 * name
	 * @var string	 */
	var $Category;

	public function toXMLElement($dom) {
		$pj = $dom->createElement('WOCategory');
		$id = $dom->createAttribute('ID');
		$name = $dom->createAttribute('NAME');

		$id->appendChild($dom->createTextNode($this->Category_ID));
		$name->appendChild($dom->createTextNode($this->Category));

		$pj->appendChild($id);
		$pj->appendChild($name);

		return $pj;
	}

	public static function toXMLWithArray($array, $notUsed) {
		$dom = new DOMDocument("1.0");
		$root = $dom->createElement('WOCategory');
		foreach ($array as $data) {
			$root->appendChild($data->toXMLElement($dom));
		}
		$dom->appendChild($root);
		return $dom->saveXML();
	}
	
	public static function woCategoryWithResult($result) {
		$WOCategory = new API_WOCategory();
		if (!empty($result)) {
		    foreach ($result as $key=>$value) {
				if (!array_key_exists($key, $WOCategory)) continue;
//				if ($key == "Category_ID") $value = intval($value);
				$WOCategory->$key = $value;
		    }
		}
		return $WOCategory;
	}
	public function getWOCategoryById($categoryId)
    {
        $db = Core_Database::getInstance();
        $select = $db->select();
        $select->from(Core_Database::TABLE_WO_CATEGORIES);
        $select->where("Category_ID = '$categoryId'");
        $records = Core_Database::fetchAll($select);
        if(!empty($records)) return $records[0];
        else return null;
    }
    
	public function getWOCategoryIdByTRCol($techselfRatingColumn)
    {
        $db = Core_Database::getInstance();
        $select = $db->select();
        $select->from(Core_Database::TABLE_WO_CATEGORIES);
        $select->where("TechSelfRatingColumn = '$techselfRatingColumn'");
        $records = Core_Database::fetchAll($select);
        if(!empty($records)) return $records[0]['Category_ID'];
        else return 0;
    }
}