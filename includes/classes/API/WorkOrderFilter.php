<?php
/**
 * class API_WorkOrderFilter
 * @package API_WorkOrderFilter
 */
class API_WorkOrderFilter extends API_Abstract_ClientWorkOrderFilter {

    const numDaysContainsFilterLimit1 = 240;
    const numDaysContainsFilterLimit2 = 480;
    private static function booleanToInt($val) {
		return $val === true || $val === 1 ? 1 : 0;
	}

    /**
     * assembleCriteria
     *
     * @param string $companyID
     * @param string $timeStampFrom
     * @param string $timeStampTo
     * @param string $timeStampType
     * @param API_WorkOrderFilter $filters
     * @static
     * @access public
     * @return string
     */
    static public function assembleCriteria($companyID, $timeStampFrom, $timeStampTo, $timeStampType, API_WorkOrderFilter $filters)
    {
		$criteria = $filters->toArray();

		$criteriaArr  = array();
		$db = Core_Database::getInstance();
		$select = $db->select();

		if (!empty($timeStampFrom) || !empty($timeStampTo) || !empty($timeStampType) || !empty($criteria["UpdateTS_AND"]) || !empty($criteria["UpdateTS_OR"]))
		{
			$tsSelect = $db->select();
			$tsSelect->from(array("ts" => Core_Database::TABLE_TIMESTAMPS), "WIN_NUM")->distinct();
			$tsSelect->join(array("wo" => Core_Database::TABLE_WORK_ORDERS), "wo.WIN_NUM = ts.WIN_NUM");
			$tsSelect->where("wo.Company_ID = ?", $companyID);

			if (!empty($timeStampType)) {
				$tsSelect->where("ts.Description LIKE ?", "%" . $timeStampType . "%");
			}

			if (!empty($timeStampFrom)) {
				$ts = strtotime($timeStampFrom);
				if ($ts)
					$tsSelect->where("ts.DateTime_Stamp >= ?", date("Y-m-d H:i:s",$ts));
			}
			if (!empty($timeStampTo)) {
				$ts = strtotime($timeStampTo);
				if ($ts)
					$tsSelect->where("ts.DateTime_Stamp <= ?", date("Y-m-d H:i:s",$ts));
			}

			// Created - ShowTechs = '0' AND ISNULL(Tech_ID, 0) = 0 AND Deactivated = '0'
			// Published - ShowTechs = '1' AND ISNULL(Tech_ID, 0) = 0 AND Deactivated = '0' AND ISNULL(FLS_ID,'') = ''
			// Assigned - ISNULL(Tech_ID, 0) != 0 AND Deactivated = '0' AND TechMarkedComplete = '0' AND ISNULL(MissingComments,'') = ''
			// Work Done - TechMarkedComplete = '1' AND Approved = '0' AND TechPaid = '0' AND Invoiced = '0' AND Deactivated = '0'
			// Approved - Approved = '1' AND Invoiced = '0' AND Deactivated = '0'
			// Incomplete - TechMarkedComplete = '0' AND Approved = '0' AND TechPaid = '0' AND Invoiced = '0' AND ISNULL(MissingComments,'') = '' AND Deactivated = '0'
			// In Accounting - Invoiced = '1' AND TechPaid = '0' AND Deactivated = '0'
			// Completed - TechPaid = '1' AND Deactivated = '0'
			// All - Deactivated = '0'
			// Deactivated - Deactivated = '1'

			if (!empty($criteria["UpdateTS_AND"])) {

				$updatedFieldList = str_replace(' ', '', $criteria["UpdateTS_AND"]);
				$updatedFieldList = explode(",", $updatedFieldList);
				$singleField = array();

				foreach ($updatedFieldList as $value) {
					$tsSelect->where("ts.Description LIKE ?", "%$value%");
				}
			}

			if (!empty($criteria["UpdateTS_OR"])) {
				$updatedFieldList = str_replace(' ', '', $criteria["UpdateTS_OR"]);
				$updatedFieldList = explode(",", $updatedFieldList);
				$singleField = array();
				foreach ($updatedFieldList as $value) {
					$singleField[] = "ts.Description LIKE '%$value%'";
				}

				if (sizeof($singleField) > 0)
					$tsSelect->where("((" . implode(") OR (", $singleField) . "))");
			}

			$timeStamps = false;
			try {
				$result = $tsSelect->query()->fetchAll();
				$timeStamps = array();
				foreach ($result as $row=>$values) {
					$timeStamps[] = $values["WIN_NUM"];
				}
				unset($result);
			} catch (Exception $e) {
				error_log($e);
				$timeStamps = false;
			}

			if (!empty($timeStamps)) {
				$select->where("WIN_NUM IN (?)", $timeStamps);
			} else {
				return 'WIN_NUM != WIN_NUM'; // force empty result
			}
		}

		if (!empty($criteria["WO_State"])) {
			$stateQuery = Core_WorkOrder_Status::getStateConditionsQueryWithName($criteria["WO_State"]);
			if (empty($stateQuery))
				$select->where("Status = ?", $criteria["WO_State"]);
			else
				$select->where("$stateQuery");
		}

        /** @author Roman Gabrusenok Begin **/
        if ( !empty($criteria['TB_UNID']) ) {
			$select->where("WIN_NUM IN (?)", explode(',', $criteria['TB_UNID']));
        }

        if ( !empty($criteria['Call_Type']) ) {
			$select->where("Type_ID = ?", $criteria['Call_Type']);
        }
//622
        if ( !empty($criteria['AbortFee']) ) {
			$select->where("AbortFee = ?", $criteria['AbortFee']);
        }

        if ( !empty($criteria['Parts_Presents']) ) {
			$select->where("ReturnPartCount > 0");
        }

        if ( !empty($criteria['Docs_Presents']) ) {
			/*$select->where("Pic1_FromTech IS NOT NULL OR Pic2_FromTech IS NOT NULL OR Pic3_FromTech IS NOT NULL");*/
        }

        /* Make filter by Tech: all should be grupped in ane place */
		$criteriaTech = $db->select();
        if ( !empty($criteria['Tech']) ) {
            if ( $criteria['Tech'] ) {
				$criteriaTech->where("Tech_ID = ?", $criteria['Tech']);
            } else {
                $criteria['Tech'] = trim($criteria['Tech']);
                if ( FALSE !== strpos($criteria['Tech'], ' ') ) {
                    $_tech = explode(' ', $criteria['Tech'], 2);
					$criteriaTech->where("Tech_FName LIKE ?", "%" . trim($_tech[0]) . "%");
					$criteriaTech->where("Tech_LName LIKE ?", "%" . trim($_tech[1]) . "%");
                    unset($_tech);
                } else {
					$_tech = "%" . trim($criteria['Tech']) . "%";
					$select->where("Tech_FName LIKE $_tech OR Tech_LName LIKE $_tech");
                    unset($_tech);
                }
            }
        }
        if ( !empty($criteria['Tech_FName']) ) {
			$criteriaTech->where("Tech_FName LIKE ?", "%" . $criteria['Tech_FName'] . "%");
        }
        if ( !empty($criteria['Tech_LName']) ) {
			$criteriaTech->where("Tech_LName LIKE ?", "%" . $criteria['Tech_LName'] . "%");
        }
        if ( !empty($criteria['Tech_ID']) ) {
            $criteriaTech->where("Tech_ID = ?", $criteria['Tech_ID']);
        }
        if ( !empty($criteria['Contractor_ID']) ) {
            $criteriaTech->where("c.number = ?", $criteria['Contractor_ID']);
        }
        if ( !empty($criteria['FLS_ID']) ) {
			$criteriaTech->where("FLS_ID = ?", $criteria['FLS_ID']);
        }

		$criteriaTech = implode(" ", $criteriaTech->getPart(Zend_Db_Select::WHERE));

        if ( NULL !== $criteria['Sourced'] ) {
            /* any tech filters */
            if ( !empty($criteriaTech) ) {
				$techFilter = '((' . $criteriaTech . ') OR ';
                $bracket = ' )';
            } else {
                $techFilter = '';
                $bracket = '';
            }

            if ( self::strtobool($criteria['Sourced']) ) {
				$select->where($techFilter."((IFNULL(Tech_ID,0) != 0) OR (IFNULL(FLS_ID,'') != ''))".$bracket);
            } else {
				$select->where($techFilter."((IFNULL(Tech_ID,0) = 0) AND (IFNULL(FLS_ID,'') = ''))".$bracket);
            }
        } else if ( !empty($criteriaTech) ) {
			$select->where($criteriaTech);
        }
        unset($criteriaTech);
        /* Make filter by Tech: all should be grupped in ane place */


        if ( !empty($criteria['Project_ID']) ) {
            if ( !is_array($criteria['Project_ID']) ) {
				$select->where("Project_ID = ?", $criteria['Project_ID']);
            } else {
				$select->where("Project_ID IN (?)", $criteria['Project_ID']);
            }
        }
        /** @author Roman Gabrusenok Finish **/

		$techViewMode = defined("WO_FILTER_TECH_VIEW_MODE") ? WO_FILTER_TECH_VIEW_MODE : false;
		if (!empty($criteria['dateRange']) && $criteria['dateRange'] != 'start' && $criteria['dateRange'] != 'invoiced') {
			// search date range based on timestamp
			$tsSelect = $db->select();
			$tsSelect->from(array("ts" => Core_Database::TABLE_TIMESTAMPS), "WIN_NUM")->distinct();
			$tsSelect->join(array("wo" => Core_Database::TABLE_WORK_ORDERS), "wo.WIN_NUM = ts.WIN_NUM");
			$tsSelect->where("wo.Company_ID = ?", $companyID);

			switch ($criteria['dateRange']) {
				case 'published':
					$descr = 'Work Order Published';
					break;
				case 'assigned':
					$descr = 'Work Order Assigned';
					break;
				case 'approved':
					$descr = 'Work Order Approved';
					break;
				case 'workdone':
					$descr = 'Work Order Marked: Completed';
					break;
			}
			$tsSelect->where("ts.Description LIKE ?", "%" . $descr . "%");

			if (!empty($criteria['StartDateFrom'])) {
				$ts = strtotime($criteria['StartDateFrom']);
				if ($ts)
					$tsSelect->where("ts.DateTime_Stamp >= ?", date("Y-m-d",$ts));
			}
			if (!empty($criteria['StartDateTo'])) {
				$ts = strtotime($criteria['StartDateTo']);
				if ($ts)
					$tsSelect->where("ts.DateTime_Stamp <= ?", date("Y-m-d",$ts));
			}

			$timestamps = false;
			try {
				$result = $tsSelect->query()->fetchAll();
				$timeStamps = array();
				foreach ($result as $row=>$values) {
					$timeStamps[] = $values["WIN_NUM"];
				}
				unset($result);
			} catch (Exception $e) {
				error_log($e);
				$timeStamps = false;
			}

			if (!empty($timeStamps)) {
				$select->where("WIN_NUM IN (?)", $timeStamps);
			} else {
				return 'WIN_NUM != WIN_NUM'; // force empty result
			}
		}
		else {
			$dateRangeField = $criteria['dateRange'] == 'invoiced' ? 'DateInvoiced' : 'StartDate';
			if (!empty($criteria['StartDateFrom'])) {
				$date = self::dateFormat("Y-m-d", $criteria['StartDateFrom']);
				$time = self::dateFormat("h:i A", $criteria['StartDateFrom']);
				if ($dateRangeField == 'StartDate') {
					if ($date)
						$select->where("work_orders.StartDate >= ?" . ($techViewMode ? " OR work_orders.StartDate IS NULL" : "" ), $date);
					if ($time && $time != '12:00 AM')
						$select->where("STR_TO_DATE(CONCAT(work_orders.StartDate,' ',work_orders.StartTime),'%Y-%m-%d %h:%i %p') >= STR_TO_DATE(?,'%Y-%m-%d %h:%i %p')" . ($techViewMode ? " OR work_orders.StartTime IS NULL" : "" ), $date . ' ' .$time);
				}
				else {
					if ($date) {
						$select->where("DateInvoiced >= ?", $date);
						$select->where("DateInvoiced < ?", date('Y-m-d')); // only show matches for one day after invoice
					}
				}
			}
			if (!empty($criteria['StartDateTo'])) {
				$date = self::dateFormat("Y-m-d", $criteria['StartDateTo']);
				$time = self::dateFormat("h:i A", $criteria['StartDateTo']);
				if ($dateRangeField == 'StartDate') {
					if ($date)
						$select->where("work_orders.StartDate <= ?" . ($techViewMode ? " OR work_orders.StartDate IS NULL" : "" ), $date);
					if ($time && $time != '12:00 AM')
						$select->where("STR_TO_DATE(CONCAT(work_orders.StartDate,' ',work_orders.StartTime),'%Y-%m-%d %h:%i %p') <= STR_TO_DATE(?,'%Y-%m-%d %h:%i %p')" . ($techViewMode ? " OR work_orders.StartTime IS NULL" : "" ), $date . ' ' .$time);
				}
				else {
					if ($date) {
						$select->where("DateInvoiced <= ?", $date);
						$select->where("DateInvoiced < ?", date('Y-m-d')); // only show matches for one day after invoice
					}
				}
			}
		}

		if (!empty($criteria['EndDateFrom'])) {
			$date = self::dateFormat("Y-m-d", $criteria['EndDateFrom']);
			if ($date)
				$select->where("EndDate >= ?", $date);
		}
		if (!empty($criteria['EndDateTo'])) {
			$date = self::dateFormat("Y-m-d", $criteria['EndDateTo']);
			if ($date)
				$select->where("EndDate <= ?", $date);
		}

        if (!empty($criteria['WO_ID'])) {
			if (!empty($criteria['WO_ID_Mode']) && $criteria['WO_ID_Mode'] == 1)
				// exact
				$select->where("WO_ID = ?", $criteria['WO_ID']);
			else
				// like
				$select->where("WO_ID LIKE ?", "%" . $criteria['WO_ID'] . "%");
        }

        if (!empty($criteria['PO'])) {
			$select->where("PO LIKE ?", "%" . $criteria['PO'] . "%");
        }

        // True/False/NULL
        if ( NULL !== $criteria['ShowTechs']) {
			$select->where("ShowTechs = ?", self::booleanToInt($criteria['ShowTechs']));
        }

        if (!empty($criteria['Region'])) {
			$select->where("Region LIKE ?", "%" . $criteria['Region'] . "%");
        }

        if (!empty($criteria['Route'])) {
			$select->where("Route LIKE ?", "%" . $criteria['Route'] . "%");
        }
        
        if (!empty($criteria['SiteName'])) {
			$select->where("SiteName LIKE ?", "%" . $criteria['SiteName'] . "%");
        }

        if (!empty($criteria['SiteNumber'])) {
			$select->where("SiteNumber LIKE ?", $criteria['SiteNumber']);
        }

        if (!empty($criteria['City'])) {
			$select->where("City LIKE ?", "%" . $criteria['City'] . "%");
        }

        if (!empty($criteria['State'])) {
			$select->where("State LIKE ?", $criteria['State']);
        }

        if (!empty($criteria['Zipcode'])) {
            $select->where("work_orders.Zipcode LIKE ?", "%" . $criteria['Zipcode'] . "%");
        }

        // True/False/NULL
        if ( NULL !== ($criteria['Update_Requested']) ) {
            $select->where("Update_Requested = ?", $criteria['Update_Requested']);
        }

        // True/False/NULL
        if (NULL !== $criteria['CheckedIn']) {
            $select->where("CheckedIn = ?", $criteria['CheckedIn']);
        }

        // 1/0/NULL
        if (NULL !== $criteria['CheckedOut']) {
            if($criteria['CheckedOut']==1)
            {
               // $select->where("(Date_Out IS NOT NULL) OR (Time_Out IS NOT NULL)");
            }
            else if($criteria['CheckedOut']==0)
            {
               // $select->where("(Date_Out IS NULL) AND (Time_Out IS NULL)");
            }
        }

        //  True/False/NULL
        if ( NULL !== $criteria['StoreNotified'] ) {
            $select->where("StoreNotified = ?", $criteria['StoreNotified']);
        }
        //  True/False/NULL
        if ( NULL !== $criteria['Incomplete_Paperwork'] ) {
            $select->where("Incomplete_Paperwork = ?", $criteria['Incomplete_Paperwork']);
        }
        //  True/False/NULL
        if ( NULL !== $criteria['Paperwork_Received'] ) {
            $select->where("Paperwork_Received = ?", $criteria['Paperwork_Received']);
        }
        //  True/False/NULL
        if ( NULL !== $criteria['Lead'] ) {
			$select->where("Lead = ?", $criteria['Lead']);
		}
        //  True/False/NULL
        if ( NULL !== $criteria['Assist'] ) {
            $select->where("Assist = ?", $criteria['Assist']);
        }
        // True/False/NULL
        if ( NULL !== $criteria['WorkOrderReviewed']) {
            $select->where("WorkOrderReviewed = ?", $criteria['WorkOrderReviewed']);
        }
        // True/False/NULL
        if ( NULL !== $criteria['TechCheckedIn_24hrs']) {
            $select->where("TechCheckedIn_24hrs = ?", $criteria['TechCheckedIn_24hrs']);
        }
        // True/False/NULL
        if ( NULL !== $criteria['TechMarkedComplete']) {
            $select->where("TechMarkedComplete = ?", $criteria['TechMarkedComplete']);
        }
        // True/False/NULL
        if ( NULL !== $criteria['Site_Complete'] ) {
            $select->where("Site_Complete = ?", $criteria['Site_Complete']);
        }

        // True/False/NULL
        if ( NULL !== $criteria['Approved']) {
            $select->where("Approved = ?", $criteria['Approved']);
        }

        if (!empty($criteria['Additional_Pay_Amount'])) {
            $select->where("Additional_Pay_Amount = 'True'");
        }
        // True/False/NULL
        if ( NULL !== $criteria['Work_Out_of_Scope']) {
            $select->where("Work_Out_of_Scope = ?", $criteria['Work_Out_of_Scope']);
        }
        // True/False/NULL
        if ( NULL !== $criteria['TechPaid']) {
            $select->where("TechPaid = ?", $criteria['TechPaid']);
        }
        //017
        if ( NULL !== $criteria['Short_Notice'] && !empty($criteria['Short_Notice'])) {
			$v = is_string($criteria['Short_Notice']) ? strtolower($criteria['Short_Notice']) == 'true' : $criteria['Short_Notice'] === true;
			$select->where("ShortNotice = ?", $v ? 1 : 0);
        }
        //end 017

        /**
        * ###########################
        * defaults to 'any' instead of 'false' as the Find Work Orders screen does
        * ###########################
        */
        //  True/False/NULL
        if (NULL !== $criteria['Deactivated']) {
			$select->where("Deactivated = ?", self::booleanToInt($criteria['Deactivated']));
		}

        if (NULL !== $criteria['isProjectAutoAssign']) {
            if ( $criteria['isProjectAutoAssign'] ) $criteriaArr[] = 'isProjectAutoAssign = 1';
            else                                    $criteriaArr[] = 'isProjectAutoAssign = 0';
        }

        if (!empty($criteria['WO_Category'])) {
			$select->where("WO_Category_ID = ?", $criteria['WO_Category']);
        }

        /**
         * withOutCompanyID criteria was added to get all WO from all companies
         * it used to get All Techs WO from client side (Tech Schedule module)
         * @author Artem Sukharev
         * @since 2010-10-22
         */
        if ( !isset($criteria['withOutCompanyID']) || !$criteria['withOutCompanyID'] ) {
        	if( !empty($criteria['Company_ID']) ) {
				$select->where(Core_Database::TABLE_WORK_ORDERS.".Company_ID = ?", $criteria['Company_ID']);
            }else if ( !empty($companyID) ) {
            	$select->where(Core_Database::TABLE_WORK_ORDERS.".Company_ID = ?", $companyID);
            }
        }

        if (isset($criteria['Tech_ID_IS_NULL']) && NULL !== $criteria['Tech_ID_IS_NULL'] ) {
            $select->where('Tech_ID IS NULL');
        }

        if (!empty($criteria['withCoordinates']) ) {
            $select->where('Latitude IS NOT ?', new Zend_Db_Expr('NULL') );
            $select->where('Longitude IS NOT ?', new Zend_Db_Expr('NULL') );
        }

		if (!empty($criteria['availableOrAssignedToTech'])) {
			$select->where("IFNULL(Tech_ID,'') = '' OR Tech_ID = ?", $criteria['availableOrAssignedToTech']);
		}

		if (!empty($criteria['Country'])) {
			$select->where("Country = ?", $criteria['Country']);
		}
		
		if (!empty($criteria['hideAvaiableIfClientDeniedForTech'])) {
			$companyQuery = "SELECT Company_ID FROM " . Core_Database::TABLE_CLIENT_DENIED_TECHS . " WHERE TechID = ?";
			$select->where("(Status IN ('" . WO_STATE_ASSIGNED . "', '" . WO_STATE_INCOMPLETE . "', '" . WO_STATE_WORK_DONE . "', '" . WO_STATE_APPROVED . "') OR Company_ID NOT IN ($companyQuery))", $criteria['hideAvaiableIfClientDeniedForTech']);
		}
		
		if (!empty($criteria['QBID'])) {
			$select->where("QBID = ?", $criteria['QBID']);
		}
		if (!empty($criteria['QBIDIsNotEmpty'])) {
			$select->where("QBID IS NOT NULL");
			$select->where("QBID != ''");
		}

		if (!empty($criteria['deactivatedTechLimitedAccess'])) {
			$select->where("Status IN ('" . WO_STATE_ASSIGNED . "', '" . WO_STATE_INCOMPLETE . "', '" . WO_STATE_WORK_DONE . "', '" . WO_STATE_APPROVED . "')");
		}
		
        //--- Audit 360
        $apiClass = new Core_Api_Class();
        $apiTechClass = new Core_Api_TechClass();//13728
        if (!empty($criteria['Audit'])) {            
            if($criteria['Audit']==1) // with AuditNeeded and w/o AuditComplete 
            {
                $wins=$apiClass->getWinsWithAuditNeededAndWOComplete();
                $select->where("WIN_NUM IN ($wins)");                
            }
            else if($criteria['Audit']==2)// with AuditComplete 
            {
                $wins=$apiClass->getWinsWithAuditComplete();
                $select->where("WIN_NUM IN ($wins)");      
            }
            else if($criteria['Audit']==3)// w/o AuditComplete and w/o AuditNeeded 
            {
                $wins=$apiClass->getWinsWithAuditCompleteORNeeded();
                $select->where("WIN_NUM NOT IN ($wins)");                      
            }            
        }
        
        if (!empty($criteria['AuditNeeded'])) {
            $wins=$apiClass->getWinsWithAuditNeededAndWOComplete();
            $select->where("WIN_NUM IN ($wins)");                            
        }
        
        if (!empty($criteria['AuditComplete'])) {
            $wins=$apiClass->getWinsWithAuditComplete();
            $select->where("WIN_NUM IN ($wins)");                            
        }

        //--- 13713
        if (!empty($criteria['DeVryObserveORPerform'])) {
            $wins=$apiClass->getWinsWithDeVryObserveORDeVryPerform();
            $select->where("WIN_NUM IN ($wins)");                            
        }        
        
        //--- end 13713

 //--- 13728
        if (!empty($criteria['ViewOnlyGroupWOForTechID'])) {
            $techID = $criteria['ViewOnlyGroupWOForTechID'];
            $wins=$apiTechClass->getWins_TechNeedToViewOnlyOnDashboard($techID);
            $select->where("WIN_NUM IN ($wins)");                           
        }                
        //--- end 13728

        //--- 13732
        if (isset($criteria['SiteContactBackedOutChecked'])) {
            $wins=$apiClass->getWinsWithSiteContactBackedOutChecked();
            if($criteria['SiteContactBackedOutChecked']==1){
                $select->where("WIN_NUM IN ($wins)");    
            } else if($criteria['SiteContactBackedOutChecked']==0) {
                $select->where("WIN_NUM NOT IN ($wins)");    
            }            
        }                
        //--- end 13732

        if (!empty($criteria['Owner'])) {
            $select->where("WorkOrderOwner = ? OR (WorkOrderOwner IS NULL AND Username = ?)", $criteria['Owner'], $criteria['Owner']);
        }

		if (!empty($criteria['DisplayProjectByClientUser'])) {
			$setting = Core_DashboardSettings::getSettings($criteria['DisplayProjectByClientUser']);
			$setting = $setting['filter'];
			$list = Core_DashboardSettings_Project::getSettings($criteria['DisplayProjectByClientUser']);
			if (!empty($list)) {
				$list = array_keys($list);
				if ($setting == Core_DashboardSettings::FILTER_PROJECT_MY) {
					// Show only project user owns
					$selectMyProject = $db->select();
					$user = new Core_Api_User;
					$user->loadById($criteria['DisplayProjectByClientUser']);
					$selectMyProject->from(Core_Database::TABLE_PROJECTS, array('Project_ID'))
						->where("CreatedBy = ?", $user->getLogin());
					$myProjects = $db->fetchCol($selectMyProject);
					if (empty($myProjects)) $myProjects = array(0);
					$select->where("Project_ID IN (?)", $myProjects);
				}
				else {
					// Show set of project
					$select->where("Project_ID IN (?)", $list);
				}
			}
		}

		if (!empty($criteria['DisplayByClientUserAction'])) {
			// Show only work order that user performed certain action on
			$select2 = $db->select();
			$client_id = $criteria['DisplayByClientUserAction'];
			$action = Core_DashboardSettings_WorkOrder::getSettings($client_id);
			$action = empty($action) ? Core_DashboardSettings_WorkOrder::FILTER_NONE : $action["filter"];
			$user = new Core_Api_User;
			$user->loadById($client_id);
			$subquery = "EXISTS (SELECT WIN_NUM FROM " . Core_Database::TABLE_TIMESTAMPS . " AS ts WHERE " . Core_Database::TABLE_WORK_ORDERS . ".WIN_NUM = ts.WIN_NUM AND ts.Username = " . $db->quoteInto('?', $user->getLogin()) . " AND ts.Description LIKE ?)";
			if (Core_DashboardSettings_WorkOrder::isFilterActive($action, Core_DashboardSettings_WorkOrder::FILTER_CREATED))
				$select2->orWhere("Username = ?", $user->getLogin());
			if (Core_DashboardSettings_WorkOrder::isFilterActive($action, Core_DashboardSettings_WorkOrder::FILTER_PUBLISHED))
				$select2->orWhere($subquery, "Work Order Published");
			if (Core_DashboardSettings_WorkOrder::isFilterActive($action, Core_DashboardSettings_WorkOrder::FILTER_ASSIGNED))
				$select2->orWhere($subquery, "Work Order Assigned");
			if (Core_DashboardSettings_WorkOrder::isFilterActive($action, Core_DashboardSettings_WorkOrder::FILTER_APPROVED))
				$select2->orWhere($subquery, "Work Order Approved");
/*			if (Core_DashboardSettings_WorkOrder::isFilterActive($action, Core_DashboardSettings_WorkOrder::FILTER_OWNED))
				$select2->orWhere("WorkOrderOwner = ? OR (WorkOrderOwner IS NULL AND UserName = ?)", $user->getLogin(), $user->getLogin());*/
			$crit = $select2->getPart(Zend_Db_Select::WHERE);
			if (!empty($crit)) {
				$crit = implode(' ', $crit);
				$select->where($crit);
			}
		}
        //13892
        if (!empty($criteria['StandardOffset'])) {
            $StandardOffset = $criteria['StandardOffset'];
            $select->where("z.StandardOffset = '$StandardOffset'");
        }
        //end 13892
        //--- return
        return implode(" ", $select->getPart(Zend_Db_Select::WHERE));
    }

    /**
     * assemble sort
     *
     * @param string $sort
     * @static
     * @access public
     * @return array
     */
	public static function assembleSort($sort, $select, $optimize = false) {
		$sort = strtolower($sort);
		$sortParts = explode(',', $sort);

        // fix for sorting text that should be float
        $techViewMode = defined("WO_FILTER_TECH_VIEW_MODE") ? WO_FILTER_TECH_VIEW_MODE : false;
        foreach ($sortParts as $key=>$part) {
            $part = trim($part);
            if ($part == "calculated_distance asc" || $part == "calculated_distance desc") {
                    $p = explode(" ", $part);
                    $sortParts[$key] = "Distance " . $p[1];
            }
            if ($part == "tb_unid asc" || $part == "tb_unid desc") {
                    $p = explode(" ", $part);
                    $sortParts[$key] = "win_num " . $p[1];
            }
            if ($part == "payamount asc" || $part == "payamount desc") {
                    $p = explode(" ", $part);
                    $sortParts[$key] = "(CASE WHEN payamount IS NOT NULL THEN payamount ELSE 99999 END) " . $p[1];
            }
            if ($part == "paymax asc" || $part == "paymax desc") {
                    $p = explode(" ", $part);
                    $sortParts[$key] = "(CASE WHEN paymax IS NOT NULL THEN paymax ELSE 99999 END) " . $p[1];
            }
            //14005
            else if(!$techViewMode && ($part == "startdate asc" || $part == "startdate desc")) {
                    $p = explode(" ", $part);
                    $direction = $p[1];
                    $position = $direction == "asc" ? "1980-01-01" : "1980-01-01";

					if (!$optimize) {
						$sortParts[$key] = "(SELECT (CASE WHEN (StartDate IS NOT NULL AND  StartDate < '2199-01-01' AND StartDate > '1980-01-01') THEN StartDate ELSE $position END)
FROM work_order_visits wov
WHERE (wov.Type = 1 OR Type = 3) AND (wov.WIN_NUM = work_orders.WIN_NUM)
ORDER BY (CASE WHEN (StartDate IS NULL OR StartDate='0000-00-00' OR StartDate > '2199-01-01' OR StartDate < '1980-01-01' ) THEN 4000 WHEN (DATEDIFF(CURDATE(),StartDate)) = 0 THEN 1000 WHEN (DATEDIFF(CURDATE(),StartDate) < 0) THEN 2000 - DATEDIFF(CURDATE(),StartDate) WHEN (DATEDIFF(CURDATE(),StartDate) > 0) THEN 3000 + DATEDIFF(CURDATE(),StartDate) END)
LIMIT 0,1) " .$direction;
					}
					else {
						$sortParts[$key] = "(CASE WHEN (StartDate IS NOT NULL AND  StartDate < '2199-01-01' AND StartDate > '1980-01-01') THEN StartDate ELSE 1980-01-01 END)" . $direction;
					}
                    //echo("<br/>CASE WHEN work_orders.startdate.2123....".$p[1]);//test
            } 
            //end 14005

            else if ($techViewMode && $part == "startdate asc" || $part == "startdate desc") {
                    $p = explode(" ", $part);
                    //$position = $p[1] == "desc" ? "2079-06-06" : "1980-01-01";
                    $position = $p[1] == "desc" ? "1980-01-01" : "1980-01-01";
                    $sortParts[$key] = "(CASE WHEN work_orders.startdate IS NOT NULL THEN work_orders.startdate ELSE $position END) " . $p[1];
            }
	    else if ($techViewMode && $part == "enddate asc" || $part == "enddate desc") {
                    $p = explode(" ", $part);
                    $position = $p[1] == "desc" ? "2079-06-06" : "1980-01-01";
                    $sortParts[$key] = "(CASE WHEN enddate IS NOT NULL THEN enddate ELSE $position END) " . $p[1];
            }
            else if ($part == "startdate asc" ||
                    $part == "enddate asc") {
                    $p = explode(" ", $part);
                    $position = $p[1] == "asc" ? "2079-06-06" : "startdate";
                    $sortParts[$key] = "(CASE WHEN work_orders.startdate IS NOT NULL THEN work_orders.startdate ELSE 999999 END) " . $p[1];
            }
            else if ($part == "datepaid asc" || $part == "datepaid desc") {
                    $p = explode(" ", $part);
                    $sortParts[$key] = "(CASE WHEN status = 'in accounting' THEN 999999 ELSE datepaid END) " . $p[1];
            }
            else if ($part == "docs asc" || $part == "docs desc") {
                $filesCrit = Core_Database::TABLE_WORK_ORDERS.'.WIN_NUM='.Core_Database::TABLE_FILES.'.WIN_NUM';
                $select->joinLeft(Core_Database::TABLE_FILES, $filesCrit, array('files_count'=>'count('.Core_Database::TABLE_FILES.'.id)'));
                $select->group(Core_Database::TABLE_WORK_ORDERS.'.WIN_NUM');
                $p = explode(" ", $part);
                $sortParts[$key] = "files_count " . $p[1];
            }
            else if ($part == "qty_applicants asc" || $part == "qty_applicants desc") {
                $filesCrit = Core_Database::TABLE_WORK_ORDERS.'.WIN_NUM='.Core_Database::TABLE_WORK_ORDER_BIDS.'.WorkOrderID';
                $select->joinLeft(Core_Database::TABLE_WORK_ORDER_BIDS, $filesCrit, array('bids_count'=>'count('.Core_Database::TABLE_WORK_ORDER_BIDS.'.id)'));
                //14018
                $select->joinLeft(array('techbid'=>Core_Database::TABLE_TECH_BANK_INFO),
                    'techbid.TechID='.Core_Database::TABLE_WORK_ORDER_BIDS.'.TechID',
                    array()
                );
                $select->group(Core_Database::TABLE_WORK_ORDERS.'.WIN_NUM');
                $select->where('techbid.HideBids IS NULL OR techbid.HideBids=0');
                //end 14018
                $p = explode(" ", $part);
                $sortParts[$key] = "bids_count " . $p[1];
            }else if($part == "logbookstartdate desc" || $part == "logbookstartdate asc" ){
            		$p = explode(" ", $part);
                    $position = $p[1] == "desc" ? "1969-01-01" : "1969-01-01";
                    $sortParts["StartDate"] = "(CASE WHEN work_orders.startdate IS NOT NULL THEN work_orders.startdate ELSE $position END) " . $p[1];
                    $sortParts[0] = "work_orders.StartDate ".$p[1];
            }
            else if($part == "auditneeded desc" || $part == "auditneeded asc")
            {
                $p = explode(" ", $part);
                $crit = Core_Database::TABLE_WORK_ORDERS.'.WIN_NUM=woaf.WINNUM';
                $select->joinLeft(array("woaf"=>"work_orders_additional_fields"),
                            $crit,
                            array('Audit'=>'(CASE WHEN (AuditNeeded=1 AND AuditComplete=0)  THEN 1 WHEN AuditComplete=1 THEN 2 ELSE 3 END)')
                );
                $sortParts[$key] = "Audit ".$p[1];
            }
            else if ($part == "date_completed asc" || $part == "date_completed desc") {
                    $p = explode(" ", $part);
                    $sortParts[$key] = "(case when Date_Completed is not null then Date_Completed else now() end) " . $p[1];
            }
            else if ($part == "starttime asc" || $part == "starttime desc") {
                    $p = explode(" ", $part);
                    $sortParts[$key] = "(case when instr(work_orders.StartTime,'AM')>0 then CONCAT('0',LPAD(work_orders.StartTime,8,'0')) else CONCAT('1',LPAD(work_orders.StartTime,8,'0') ) end) " . $p[1];
	        }
            else if($part == "techcheckedin_24hrs asc" || $part == "techcheckedin_24hrs desc")
            {
                $p = explode(" ", $part);
                $sortParts[$key] = "TechCheckedIn_24hrs  ".$p[1];
            }
            else if($part == "checkedin asc" || $part == "checkedin desc")
            {
                $p = explode(" ", $part);
                $sortParts[$key] = "CheckedIn ".$p[1];
            }
            else if($part == "checkedout asc" || $part == "checkedout desc")
            {
                $p = explode(" ", $part);
                $sortParts[$key] = "Date_Out ".$p[1];
            }
            //13892
            else if($part == "standardoffset asc" || $part == "standardoffset desc")
            {
                $p = explode(" ", $part);
                $sortParts[$key] = "StandardOffset ".$p[1];
            }            
            //end 13892
             //13959
            else if($part == "zipcode asc" || $part == "zipcode desc")
            {
                $p = explode(" ", $part);
                $sortParts[$key] = "work_orders.Zipcode ".$p[1];
            }            
            //end 13959
        }
        $select->order($sortParts);
		return $select;
	}

    /**
     * filter
     *
     * @param string $companyID
     * @param string $fields
     * @param string $timeStampFrom
     * @param string $timeStampTo
     * @param string $timeStampType
     * @param API_WorkOrderFilter $filters
     * @param string $sort
     * @param int $limit
     * @param int $offset
     * @static
     * @access public
     * @return Core_Filter_Result
     */
	public static function filter($companyID, $fields, $timeStampFrom, $timeStampTo, $timeStampType, API_WorkOrderFilter $filters, $sort = '', $limit = 0, $offset = 0, &$count = null, $optimize = false)
	{
		$db = Core_Database::getInstance();
		$select = $db->select();
		$select->from(Core_Database::TABLE_WORK_ORDERS, array ("WIN_NUM"));
        //13892,13959
        if (!empty ($filters->StandardOffset) || strpos($sort,"StandardOffset")!==false ){
            $select->joinLeft(array("z"=>"zipcodes"), 
                    "z.ZIPCode=work_orders.Zipcode",
                    array("StandardOffset")
            );             
        }
        //end 13892,13959
		if (!empty ($filters->Contractor_ID)) {
			$select->join (array ("c" => "tech_certification"), "c.TechID = work_orders.Tech_ID", array ("Contractor_ID" => "c.number"));
			$select->where ("c.certification_id = 22");
		}
		
		if($filters->CheckedOut != ""){                                                       
			$select->columns(array ("work_orders.Date_Out", "work_orders.Time_Out"));
			$select->joinLeft(array("v"=> "work_order_visits"), "v.WIN_NUM = work_orders.WIN_NUM", array("CaseCheckOutDate"=>"(CASE v.OptionCheckInOut WHEN 1 THEN v.TechCheckOutDate WHEN 2 THEN v.CORCheckOutDate END)", "OptionCheckInOut" => "v.OptionCheckInOut"));
			$select->where("v.Type = ?", "1");
			$select->group("WIN_NUM");
			if($filters->CheckedOut == 1){
				$select->having("CaseCheckOutDate IS NOT NULL OR Date_Out IS NOT NULL OR Time_Out IS NOT NULL");
			}elseif($filters->CheckedOut == 0){
				$select->having("CaseCheckOutDate IS NULL AND Date_Out IS NULL AND Time_Out IS NULL");
			}
		}

		if (!empty($filters->WO_ID)) {
			$wo_id_esc = $db->quoteInto('?', $filters->WO_ID);
			$select->order("(CASE WHEN STRCMP(WO_ID, $wo_id_esc) = 0 THEN 3 ELSE STRCMP(WO_ID, $wo_id_esc) END) DESC");
		}

		$criteria = self::assembleCriteria($companyID, $timeStampFrom, $timeStampTo, $timeStampType, $filters);

		if (!empty($criteria))
			$select->where($criteria);

		$countSelect = clone ($select);		//Copy select object without limit to use for a full result count

        self::assembleSort($sort, $select, $optimize);

        if ($offset != 0 || $limit != 0)
			$select->limit($limit, $offset);

        $result = new Core_Filter_Result($select);

		$wins = array ();
		foreach ($result->toArray() as $wo) {
			$wins[] = $wo["WIN_NUM"];
		}

		$select = $db->select();
		$select->distinct ();
		if (!empty($fields)) {
			$fieldList = str_replace(' ', '', $fields);
			$fieldListParts = explode(",", $fieldList);
		} else {
			$fieldListParts = Core_Database::getFieldList(Core_Database::TABLE_WORK_ORDERS);
		}

		$fieldListMap = array_combine($fieldListParts, $fieldListParts);
		$fieldListMap["UpdatedPartCount"] = "(SELECT COUNT(*) FROM part_entries WHERE WinNum = work_orders.WIN_NUM AND TechIdLastUpd <> 0)";

		$select->from(Core_Database::TABLE_WORK_ORDERS, $fieldListMap);
        //13892
        $select->joinLeft(array("z"=>"zipcodes"), 
                "z.ZIPCode=work_orders.Zipcode",
                array("StandardOffset","TimeZone"=>"(CASE z.StandardOffset WHEN -10 THEN 'HST' WHEN -9 THEN 'AKST' WHEN -8 THEN 'PST' WHEN -7 THEN 'MST' WHEN -6 THEN 'CST' WHEN -5 THEN 'EST' ELSE '' END)")
        );            
        //end 13892
		if (!empty ($filters->Contractor_ID)) {
			$select->join (array ("c" => "tech_certification"), "c.TechID = work_orders.Tech_ID", array ("Contractor_ID" => "c.number"));
		}
		
		if($filters->CheckedOut != ""){                                                       
			$select->joinLeft(array("v"=> "work_order_visits"), "v.WIN_NUM = work_orders.WIN_NUM", array("CaseCheckOutDate"=>"(CASE v.OptionCheckInOut WHEN 1 THEN v.TechCheckOutDate WHEN 2 THEN v.CORCheckOutDate END)", "OptionCheckInOut" => "v.OptionCheckInOut"));
			$select->where ("v.WOVisitOrder = 1");
		}

        self::assembleSort($sort, $select);

		$select->where ("work_orders.WIN_NUM IN (?)", !empty ($wins) ? $wins : 0);
		
		$originalCountSelect = clone $countSelect;
		if (!empty($filters->WO_ID) && empty($filters->WO_ID_Mode)) {
			// try 30 day and 120 day for LIKE search
			$intervalArray = array(self::numDaysContainsFilterLimit1, self::numDaysContainsFilterLimit2, NULL); // NULL is no limit
			foreach ($intervalArray as $days) {
				if (!is_null($days))
					$countSelect->where('StartDate > ?' , new Zend_Db_Expr("DATE_SUB(NOW(), INTERVAL $days DAY)") );
				$records = Core_Database::fetchAll($countSelect);
				if(empty($records) || count($records)==0) {
					$count = 0;
				} else {
					$count = count ($records);
					if (!is_null($days))
						$select->where('StartDate > ?' , new Zend_Db_Expr("DATE_SUB(NOW(), INTERVAL $days DAY)") );
					break;
				}
				$countSelect = clone $originalCountSelect;
			}
		}

        $result = new Core_Filter_Result($select);        

        //13892, 13959
		if ($count !== null) {
			$records = Core_Database::fetchAll($countSelect);
            if(empty($records) || count($records)==0) {
                $count = 0;
            } else {
                $count = count ($records);
            }
        }
		
        //end 13892, 13959
		return $result;
	}
}
