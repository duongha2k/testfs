<?php
/**
 * class API_Timestamp
 * @package API_Timestamp
 */
class API_Timestamp {
//	private $fieldsList = "DateTime_Stamp,WO_UNID,Username,Description,Client_Name,Project_Name,Customer_Name,TechID,Tech_Name,Company_ID";
	private static $fieldMap = array(
				"DateTime_Stamp" => "dateTime", 
				"WIN_NUM" => "winNum",
				"Username" => "username",
				"Description" => "description",
				"wo.Company_Name" => "clientName",
				"wo.Project_Name" => "projectName",
				"Customer_Name" => "customerName",
				"Tech_ID" => "techId",
				"Tech_Name" => "techName",
				"wo.Company_ID" => "companyId"
				);

	/** 
	 * 
	 * @var string	 */
	public $dateTime;

	/** 
	 * 
	 * @var int	 */
	public $winNum;

	/** 
	 * 
	 * @var string	 */
	public $username;

	/** 
	 * 
	 * @var string	 */
	public $description;

	/** 
	 * 
	 * @var string	 */
	public $clientName;

	/** 
	 * 
	 * @var string	 */
	public $projectName;
		
	/** 
	 * 
	 * @var string	 */
	public $customerName;

	/** 
	 * 
	 * @var int	 */
	public $techId;

	/** 
	 * 
	 * @var string	 */
	public $techName;

	/** 
	 * 
	 * @var string	 */
	public $companyId;

	static private function fieldList() {
		return array_keys(self::$fieldMap);
	}

	public static function getTimestamps($winNum, $companyId = null, $offset = 0, $limit= 0, $sort = "") {
		$db = Core_Database::getInstance();
		$select = $db->select();
		$select->from(array("ts" => Core_Database::TABLE_TIMESTAMPS),
				self::fieldList())
			->join(array("wo" => Core_Database::TABLE_WORK_ORDERS), "wo.WIN_NUM = ts.WIN_NUM", array())
			->where("wo.WIN_NUM = ?", $winNum);
		if (!empty($companyId))
			$select->where("wo.Company_ID = ?", $companyId);
		if ($limit != 0 || $offset != 0)
			$select->limit($limit, $offset);
		if (empty($sort))
			$select->order(array('DateTime_Stamp DESC'));
		else 
			$select->order(array($sort));
        $result = false;

		try {
			$result = $select->query()->fetchAll();
			$countRows = sizeof($result);
		} catch (Exception $e) {
            return NULL;
        }

        $tsList = array();            
		foreach ($result as $ts) {
			$tsObj = new self();
			foreach (self::$fieldMap as $dbField=>$classField) {
			    $dbField = preg_replace('/[^\.]+\./', '', $dbField);
				$tsObj->$classField = $ts[$dbField];
			}
			$tsList[] = $tsObj;
		}
		return $tsList;
	}

    public static function getLastWinAssignedTimestamp($winNum)
    {
        $db = Core_Database::getInstance();
        $select = $db->select();
        $select->from(Core_Database::TABLE_TIMESTAMPS);
        $select->where("Description like 'Work Order Assigned%'");
        $select->where("WIN_NUM = '$winNum'");
        $select->order("DateTime_Stamp DESC");
        $records = $db->fetchAll($select);
        if(empty($records)) return null;
        else return $records[0]['DateTime_Stamp'];
    }
    //--- 13457
    public static function DidAssigned_InNdays($companyID,$Ndays=21)
    {
        $dateTime = new DateTime();
        if($Ndays>1) $dateTime->modify("-$Ndays days");
        else if($Nday==1) $dateTime->modify("-1 day");
        $strDateTime = $dateTime->format('Y-m-d');
        
        $db = Core_Database::getInstance();
        $select = $db->select();
        $select->from(array('ts'=>Core_Database::TABLE_TIMESTAMPS),self::fieldList());
        $select->join(array("wo" => Core_Database::TABLE_WORK_ORDERS), "wo.WIN_NUM = ts.WIN_NUM", array());
        $select->where("wo.Company_ID = '$companyID'");
        $select->where("ts.Description LIKE 'Work Order Assigned%'");
        $select->where("ts.DateTime_Stamp >= '$strDateTime'");
        $records = $db->fetchAll($select);
        if(empty($records)) return 0;
        else return 1;
	}
    //--- 13457
     public static function getLastWinAssignedTimestamp_ByCompany($companyID)
     {
        $db = Core_Database::getInstance();
        $select = $db->select();
        $select->from(array('ts'=>Core_Database::TABLE_TIMESTAMPS), array("MAX(ts.DateTime_Stamp)"));
        $select->join(array("wo" => Core_Database::TABLE_WORK_ORDERS), "wo.WIN_NUM = ts.WIN_NUM", array());
        $select->where("wo.Company_ID = '$companyID'");
        $select->where("ts.Description LIKE 'Work Order Assigned%'");
		$select->where("wo.StartDate >= DATE_SUB( CURDATE( ) , INTERVAL 1 MONTH )");
		$select->where("ts.DateTime_Stamp >= DATE_SUB( CURDATE( ) , INTERVAL 1 MONTH )");
//                $select->where("ts.DateTime_Stamp >= DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 1 MONTH)), INTERVAL 1 DAY)");
//        $select->order("ts.DateTime_Stamp DESC");
		$select->limit(1);

        $records = $db->fetchAll($select);
        if(empty($records)) return null;
        else return $records[0]['MAX(ts.DateTime_Stamp)'];
     }
     //--- 13534
     public static function getTimestamp_forUpdateRequest($winNumArray,$requestDateFrom,$requestDateTo,$startDateFrom,$startDateTo,$projectIDArray)
     {
        $wofields =  array('WO_ID','Status','Project_ID','Project_Name','StartDate','StartTime','ShortNotice','SiteNumber','SiteName','City','State','Zipcode','Tech_ID','FLS_ID','Tech_FName','Tech_LName','Lead','Assist');
        $db = Core_Database::getInstance();
        $select = $db->select();
		$tsFieldList = self::$fieldMap;
		unset($tsFieldList["Description"]);
		$tsFieldList = array_keys($tsFieldList);
		$tsFieldList["Description"] = new Zend_Db_Expr("(CASE WHEN ts.Description LIKE 'Work Order Updated%' THEN wo.Update_Reason ELSE ts.Description END)");
        $select->from(array('ts'=>Core_Database::TABLE_TIMESTAMPS),$tsFieldList);
        $select->join(array("wo" => Core_Database::TABLE_WORK_ORDERS), "wo.WIN_NUM = ts.WIN_NUM", $wofields);
        $select->where("ts.Description LIKE 'FLS Update Requested:%' OR (DateTime_Stamp <= '2012-11-28' AND (ts.Description LIKE '%Update_Requested%' AND ts.Description LIKE '%Update_Reason%'))");
		$select->where("wo.Company_ID = 'FLS'"); // added for potential bug with JSON encoding from report controller
        if(!empty($requestDateFrom))
        {
            $select->where("ts.DateTime_Stamp >= '$requestDateFrom'");
        }
        if(!empty($requestDateTo))
        {
            $select->where("ts.DateTime_Stamp <= '$requestDateTo'");
        }
        if(!empty($winNumArray))
        {
            $select->where("ts.WIN_NUM IN (?)",$winNumArray);
        }
        if(!empty($startDateFrom))
        {
            $select->where("wo.StartDate >= '$startDateFrom'");
        }
        if(!empty($startDateTo))
        {
            $select->where("wo.StartDate <= '$startDateTo'");
        }        
        
        if(!empty($projectIDArray) && count($projectIDArray)>0)
        {
            $select->where("wo.Project_ID IN (?)",$projectIDArray);
        }

        $select->order("ts.WIN_NUM");
        $records = $db->fetchAll($select);
        return $records;
     }
     
     //--- 13620
     public function getTimestampWhenPublishWO($winNum)
     {
        $db = Core_Database::getInstance();
        $select = $db->select();
        $select->from(Core_Database::TABLE_TIMESTAMPS);
        $select->where("WIN_NUM = '$winNum'");
        $select->where("Description LIKE 'Work Order Published%'");        
        $records = $db->fetchAll($select);
        if(empty($records)) return null;
        else return $records[0]['DateTime_Stamp'];
     }
}
