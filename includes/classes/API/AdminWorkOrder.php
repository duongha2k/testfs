<?php

/**
 * class API_AdminWorkOrder
 * @package API_AdminWorkOrder
 */
class API_AdminWorkOrder extends API_Abstract_AdminWorkOrder {
	/** 
	 * 
	 * @var boolean	 */
	var $ShortNotice;
	/** 
	 * 
	 * @var boolean	 */
	var $PreCall_1;
	/** 
	 * 
	 * @var boolean	 */
	var $PreCall_2;
	/** 
	 * 
	 * @var boolean	 */
	var $FLS_OOS;
	/** 
	 * 
	 * @var boolean	 */
	var $Unexpected_Steps;
	/** 
	 * 
	 * @var string	 */
	var $Unexpected_Desc;
	/** 
	 * 
	 * @var string	 */
	var $AskedBy;
	/** 
	 * 
	 * @var string	 */
	var $AskedBy_Name;
	/** 
	 * 
	 * @var string	 */
	var $BU1FLSID;
	/** 
	 * TODO What field in DB referrenced to?
	 * @var string	 */
	var $BU1Info;
	/** 
	 * TODO What field in DB referrenced to?
	 * @var string	 */
	var $BU2FLSID;
	/** 
	 * 
	 * @var string	 */
	var $BU2Info;
	/** 
	 * 
	 * @var string	 */
	var $FLS_ID;
	/** 
	 * 
	 * @var string	 */
	var $SourceByDate;
	/** 
	 * 
	 * @var string	 */
	var $PricingRuleID;
	/** 
	 * 
	 * @var string	 */
	var $WMID;
	/** 
	 * 
	 * @var string	 */
	var $WMAssignmentID;
	/** 
	 * 
	 * @var string	 */
	var $PushWM;	
}
