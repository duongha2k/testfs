<?php
class API_Response {
    /**
     * true means call completed successfully and false means one or more errors occurred
     * @var boolean */
    var $success;

    /**
     * array of one or more error objects
     * @var array */
    var $errors;
   
    /**
     * array of one or more work order objects (for create / update only WorkOrderID is filled) or one or more project objects for getAllProjects
     * @var array */
    var $data;

	public function __construct($success, $errors, $data) {
		$this->success = $success;
		$this->errors = $errors;
		$this->data = $data;
	}

    public static function escape($data) {
        return htmlentities(stripslashes($data));
    }
	
	public static function success($data) {
		return new API_Response(true, NULL, $data);
	}

	public static function fail($error = NULL) {
		if ($error == NULL)
			$error = API_Error::getAPIErrors();
		return new API_Response(false, $error, NULL);
	}

	public function toXML($param = NULL) {
		$responseXML = "";
		if ($this->success) {
			if (isset($this->data[0])) {
				$class = get_class($this->data[0]);
				if (!empty($class)) {
					if ($class == "API_WorkOrder") {
						$isResponse = TRUE;
						eval("\$responseXML = $class::toXMLWithArray(\$this->data, \$param, \$isResponse);");
					}
					else
						eval("\$responseXML = $class::toXMLWithArray(\$this->data, \$param);");
				}
			}
		}
		else {
  			$responseXML = API_Error::toXMLWithArray($this->errors);
		}
		return $responseXML;
	}
}