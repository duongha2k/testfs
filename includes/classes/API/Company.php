<?php
/**
 * class API_Company
 * @package API_Company
 * @author Alex Scherba
 */
class API_Company {
    /**
     * @var string */
    var $CompanyName;
    /*
     * @var string */
    var $Company_ID;

    public function toXMLElement($dom) {
        $company = $dom->createElement('Company');
        $id = $dom->createAttribute('ID');
        $name = $dom->createAttribute('NAME');

        $id->appendChild($dom->createTextNode($this->Company_ID));
        $name->appendChild($dom->createTextNode($this->CompanyName));

        $company->appendChild($id);
        $company->appendChild($name);

        return $company;
    }

    public static function toXMLWithArray($array, $notUsed) {
        $dom = new DOMDocument("1.0");
        $root = $dom->createElement('Companies');
        foreach ($array as $data) {
            $root->appendChild($data->toXMLElement($dom));
        }
        $dom->appendChild($root);
        return $dom->saveXML();
    }

    public static function companyWithResult($result) {
        $company = new API_Company();
        if (!empty($result)) {
            foreach ($result as $key=>$value) {
                if (!array_key_exists($key, $company)) continue;
                $company->$key = $value;
            }
        }
        return $company;
    }
}