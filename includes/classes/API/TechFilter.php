<?php


/**
 * API_TechFilter
 * @author Alexander Cheshchevik
 *
 */
class API_TechFilter extends API_Abstract_TechFilter
{
	
    /**
     * assembleCriteria
     * 
     * @param string $companyID 
     * @param string $timeStampFrom 
     * @param string $timeStampTo 
     * @param string $timeStampType 
     * @param API_WorkOrderFilter $filters 
     * @static
     * @access public
     * @return string
     */
    static public function assembleCriteria($companyId, API_TechFilter $filters)
    {
        $companyId = Core_Caspio::caspioEscape($companyId);
		$criteria = $filters->toArray();
		$searchCriteria  = array();

        foreach ($criteria as $key => $value) {
            // parse searchCriteria
            if (is_null($value) ) {
                continue;
            }
            if (!is_array($value) ) {
                $value = Core_Caspio::caspioEscape($value);    
            }
            if ($value == "Y") $value = 1;
            if ($value == "N") $value = 0;
            switch ($key) {
				case "FLSIDExists":
					// field not blank
					if ($value == "1")
						$searchCriteria[] = "ISNULL(FLSID, '') <> ''";
					break;
                case "CompTIA":
                    $searchCriteria[] = "ISNULL($key, '') <> ''";
                    break;
                case "DellCert":
                    $searchCriteria[] = "$key LIKE '%".Core_Caspio::caspioEscape($value)."%'";
                    break;
                case "No_Shows":
                    if (!is_numeric($value))
                        throw new Exception("$key|must be numeric");
                    $searchCriteria[] = "$key <= '$value'";
                    break;
                case "Qty_IMAC_Calls":
                case "SATRecommendedAvg":
                case "SATPerformanceAvg":
                case "SATRecommendedTotal":
                case "SATPerformanceTotal":
                case "Qty_FLS_Service_Calls":
					if (!is_numeric($value))
						throw new Exception("$key|must be numeric");
					$searchCriteria[] = "$key >= '$value'";
                    break;
                case "selfRating":
                	foreach ($value as $k=>$val) {
                		if (!is_numeric($val))
                        throw new Exception("$k|must be numeric");
                		$searchCriteria[] = "$k >= '$val'";
                	}
                	break;
                case "Bg_Test_Pass_Lite":
                    if (!is_numeric($value))
                        throw new Exception("$key|must be numeric");
                    $searchCriteria[] = "$key >= '$value'";
                    break;
				case "HP_CertNum":
					if ($value == "Any HP Cert")
						$searchCriteria[] = "ISNULL(HP_CertNum, '') <> ''";
					break;
				case 'preferredOnly':
				   // $searchCriteria[] = "TechID IN (SELECT TechID FROM Client_Preferred_Techs WHERE CompanyID = '" . $companyId . "' AND TechID = Tech_ID)";
				    $preferredList = Core_Tech::getClientPreferredTechsArray($companyId);
				    if(!empty($preferredList))
				    	$searchCriteria[] = 'TechID IN (' . implode(',',$preferredList) . ')';
				    break;
                case 'hideBanned':
					$deniedList = Core_Tech::getClientDeniedTechsArray($companyId);
					if (!empty($deniedList))
					    $searchCriteria[] = 'TechID NOT IN (' . implode(',',$deniedList) . ')';				
                    break;
                case 'TechIDs':
                    $items = explode(',', $value);
                    foreach ($items as &$value) {
                        $value = Core_Caspio::caspioEscape(trim($value));
                    }
                    $searchCriteria[] = "TechID IN (" . implode(',', $items) . ")";
                    break;
                case 'HourlyPay':
                    $searchCriteria[] = "$key <= '$value'";
                    break;
                case 'NotPrivate':
                    if ($key) {
                        //private logic
                        $searchCriteria[] = "(Private = '0' OR (SELECT Client_ID FROM ISO WHERE UNID = ISO_Affiliation_ID) = '$companyId') AND AcceptTerms = 'Yes'";
                    }
                    break;
                case 'WithCoordinates':
                    if ($key) {
                        //take only techs with coordinates
                        $searchCriteria[] = "Latitude IS NOT NULL AND Longitude IS NOT NULL AND Deactivated != '1'";
                    }
                    break;
                case 'SelfRating':
                    if (is_array($value) ) {
                        foreach (self::$SelfRatingValues as $sfKey) {
                            if (in_array($sfKey, $value)) {
                                $value = Core_Caspio::caspioEscape(trim($value));
                                $searchCriteria[] = "$sfKey >= '$value'";
                            }
                        }
                    }
                    break;
                default:
                    $searchCriteria[] = "$key = '$value'";
                    break;
            }
        }
        
        
		$criteria = '('. implode(') AND (', $searchCriteria) . ')';

		return $criteria;
    }

    /**
     * assembleCriteriaMysql
     * 
     * @param string $companyID 
     * @param string $timeStampFrom 
     * @param string $timeStampTo 
     * @param string $timeStampType 
     * @param API_WorkOrderFilter $filters 
     * @static
     * @access public
     * @return string
     */
    static private function assembleCriteriaMysql($companyId, API_TechFilter $filters)
    {
		$db = Core_Database::getInstance();
        $companyId = $companyId;
		$criteria = $filters->toArray();        
		$searchCriteria  = array();

        foreach ($criteria as $key => $value) {
            // parse searchCriteria
            if (is_null($value) ) {
                continue;
            }
            if ($value === "Y") $value = 1;
            if ($value === "N") $value = 0;
            switch ($key) {
				case "FLSIDExists":
					// field not blank
					if ($value == "1")
						$searchCriteria[] = "IFNULL(FLSID, '') <> ''";
					break;
                case "CompTIA":
                    $searchCriteria[] = "IFNULL($key, '') <> ''";
                    break;
                case "DellCert":
                    $searchCriteria[] = $db->quoteInto("$key LIKE ?", "%".$value."%");
                    break;
                case "No_Shows":
                    if (!is_numeric($value))
                        throw new Exception("$key|must be numeric");
                    $searchCriteria[] = $db->quoteInto("$key <= ?", $value);
                    break;
                case "Qty_IMAC_Calls":
                case "SATRecommendedAvg":
                case "SATPerformanceAvg":
                case "SATRecommendedTotal":
                case "SATPerformanceTotal":
                case "Qty_FLS_Service_Calls":
                case "Bg_Test_Pass_Lite":
					if (!is_numeric($value))
						throw new Exception("$key|must be numeric");
                    $searchCriteria[] = $db->quoteInto("$key >= ?", $value);
                    break;
                case "selfRating":
                	foreach ($value as $k=>$val) {
                		if (!is_numeric($val))
                        throw new Exception("$k|must be numeric");
	                    $searchCriteria[] = $db->quoteInto("$k >= ?", $val);
                	}
                	break;
				case "HP_CertNum":
					if ($value == "Any HP Cert")
						$searchCriteria[] = "IFNULL(HP_CertNum, '') <> ''";
					break;
				case 'preferredOnly':
				    $preferredList = Core_Tech::getClientPreferredTechsArray($companyId);
				    if(!empty($preferredList))
					    $searchCriteria[] = $db->quoteInto("TechID IN (?)", $preferredList);				
				    break;
                case 'hideBanned':
                    //13771
                    $deniedByCompanyID = $companyId;
                    if(!empty($criteria['DeniedByCompanyID'])){
                        $deniedByCompanyID = $criteria['DeniedByCompanyID'];
                    }
					$deniedList = Core_Tech::getClientDeniedTechsArray($deniedByCompanyID);
                    //end 13771
					if (!empty($deniedList))
					    $searchCriteria[] = $db->quoteInto("TechID NOT IN (?)", $deniedList);				
                    break;
                case 'DeniedByCompanyID':  break; //13771
                case 'TechIDs':
                    $items = explode(',', $value);
                    foreach ($items as &$value) {
                        $value = trim($value);
                    }
                    $searchCriteria[] = $db->quoteInto("TechID IN (?)", $items);
                    break;
                case 'HourlyPay':
                    $searchCriteria[] = $db->quoteInto("$key <= ?", $value);
                    break;
                case 'NotPrivate':
                    if ($key) {
                        //private logic 
                        $searchCriteria[] = "(Private = '0' OR (SELECT Client_ID FROM " . Core_Database::TABLE_ISO . " WHERE UNID = ISO_Affiliation_ID) = " . $db->quoteInto("?", $companyId) . ") AND AcceptTerms = 'Yes'";
                    }
                    break;
                case 'WithCoordinates':
                    if ($key) {
                        //take only techs with coordinates
                        $searchCriteria[] = "Latitude IS NOT NULL AND Longitude IS NOT NULL AND Deactivated != '1'";
                    }
                    break;
                case 'SelfRating':
                    if (is_array($value) ) {
                        foreach (self::$SelfRatingValues as $sfKey) {
                            if (in_array($sfKey, $value)) {
                                $value = Core_Caspio::caspioEscape(trim($value));
                                $searchCriteria[] = $db->quoteInto("$sfKey >= ?", $value);
                            }
                        }
                    }
                    break;
                //13735
                case 'FSExpertForCatID':
                    if(!empty($value)){
                        $FSExpertClass = new Core_Api_FSExpertClass();
                        $catFilter = array('Category_ID'=>$value);
                        $TechIDArray = $FSExpertClass->getTechIDArray_hasFSExpert($catFilter);
                        if(!empty($TechIDArray) && count($TechIDArray)>0){
                            $searchCriteria[] = $db->quoteInto("TechID IN (?)", $TechIDArray);
                        } else{
                            $searchCriteria[] = "1 = 0";
                        }
                    }
                    break;
                //end 13735
                //14023
                case 'hideBids':
                        $hideBidsList = Core_Tech::getTechIdsHideBids();
                        if (!empty($hideBidsList))
                            $searchCriteria[] = $db->quoteInto("TechID NOT IN (?)", $hideBidsList);				
                    break;	
                default:
                    $searchCriteria[] = $db->quoteInto("$key = ?", $value);
                    break;
            }
        }
        
		$TechClass = new Core_Api_TechClass;
        $hideTechs = $TechClass->getTechsNotVisibleToClient($companyId); 
		if (!empty($hideTechs)) {
			$searchCriteria[] = $db->quoteInto("TechID NOT IN (?)", $hideTechs);
		}
		
		$criteria = '('. implode(') AND (', $searchCriteria) . ')';
		
		return $criteria;
    }
    
	/**
	 * Filter
	 * @param unknown_type $companyID
	 * @param array $fields
	 * @param array $position
	 * @param API_TechFilter $filters
	 * @return array
	 */
	public static function filter($companyID, $fields, $position, API_TechFilter $filters, $offset = 0, $limit= 0, &$countRows = null, $sortBy = null, $formType = 'find_techs')
    {
        $requestTable = null;
		$mysqlMode = false;        
        $isView = false;
        
        switch ($formType) {
            case 'find_cabling_techs':
                $requestTable = TABLE_MASTER_LIST_CABLING;
                $isView = true;
                break;
            case 'find_telephony_techs':
                $requestTable = TABLE_MASTER_LIST_TELEPHONY;
                $isView = true;
                break;
            case 'find_techs':
            default:
                $requestTable = Core_Database::TABLE_TECH_BANK_INFO;
				$mysqlMode = true;
                break;
        }
		
		if (!$mysqlMode) {
	        $specialColumns = array(
	        	"AudioResume" => "(CASE WHEN CAST(ISNULL(AudioResume, '') AS varchar) = '' THEN 'No' ELSE 'Yes' END)", 
	        	"SpecialDistance" => "calculated_distance"
	        );
		}
		else {
	        $specialColumns = array(
	        	"AudioResume" => "(CASE WHEN CAST(IFNULL(AudioResume, '') AS CHAR) = '' THEN 'No' ELSE 'Yes' END)", 
	        	"SpecialDistance" => "calculated_distance"
	        );
		}
                
		$preferesIds = $preferredList = Core_Tech::getClientPreferredTechsArray($companyID);
        (!empty($preferredList)) ? $specialColumns["PreferLevel"] = '(CASE WHEN TechID IN ('.implode(",",$preferredList). ') THEN \'Yes\' ELSE \'No\' END)' : $specialColumns["PreferLevel"] = "'No'";
        
        //"PreferLevel" => "(CASE WHEN(SELECT TOP 1 PreferLevel FROM Client_Preferred_Techs WHERE CompanyID = '" . Core_Caspio::caspioEscape($companyID) . "' AND TechID = Tech_ID) IS NOT NULL THEN 'Yes' ELSE 'No' END)", 
		$fieldList = null;


        $sort = $sortBy;
        if (empty($sort) ) {
            $sort = "{$specialColumns["PreferLevel"]} DESC, {$specialColumns["AudioResume"]} DESC";
        }
        
        // fileds list for quick select
        $quickFields = array(
        	"TechID" => "TechID",
            "PrimaryEmail" => "PrimaryEmail"
//            "isPreferred" => "{$specialColumns["PreferLevel"]}"
        );
        
		if (!$mysqlMode) {
	        $fieldList = !empty($fields) ? $fields : Core_Caspio::getTableFieldsList(TABLE_MASTER_LIST);

	        $fieldList = str_replace(' ', '', $fieldList);

			$fieldListParts = explode(",", $fieldList);
		}
		else {
	        $fieldList = !empty($fields) ? $fields : Core_Database::getFieldList($requestTable);
			if (!is_array($fieldList)) {
		        $fieldList = str_replace(' ', '', $fieldList);
				$fieldListParts = explode(",", $fieldList);
			}
			else
				$fieldListParts = $fieldList;
		}

		$fieldListMap = array_combine($fieldListParts, $fieldListParts);

		if (!$mysqlMode)
	        $criteria = self::assembleCriteria($companyID, $filters);
		else
	        $criteria = self::assembleCriteriaMysql($companyID, $filters);		

		$db = Core_Database::getInstance();
		if($mysqlMode) {
			$select = $db->select();
			$select->from($requestTable, $fieldListMap)
				->where($criteria)
				->order($sort);
	            if (!empty($limit)) {
					$select->limit($limit, $offset);
				}
		}
		
        if (!empty($position) && !empty($position['ProximityZipCode']) ) {
			if (!$mysqlMode) {
	            $quickResult = Core_Caspio::caspioProximitySearchByCoordinatesRaw(
	                $requestTable, 
	                $isView, 
	                $position['ProximityLat'], 
					$position['ProximityLng'], 
					"Latitude", 
					"Longitude", 
					"<= {$position["ProximityDistance"]}.49", 
					"3", 
					$quickFields, 
					"AND {$criteria}", 
					"", 
					false,
					'#|^',
					'`'
				);
			}
			else {
				$select = Core_Database::whereProximity($select, $position['ProximityLat'], $position['ProximityLng'], "Latitude", "Longitude", "{$position["ProximityDistance"]}.49", "calculated_distance");
				$quickResult = $db->fetchAll($select);
			}
        } else {
			if (!$mysqlMode)
	            $quickResult = Core_Caspio::caspioSelectWithFieldListMap($requestTable, $quickFields, $criteria, '');
			else {
				$quickResult = $db->fetchAll($select);
			}
        }

        if ( (!$mysqlMode && is_array($quickResult) && count($quickResult)) || ($mysqlMode && $quickResult && sizeof($quickResult) > 0 ) ) {
            $ids = array();
            foreach ($quickResult as $qItem) {
                $ids[] = $qItem['TechID'];
            }
            $countRows = count($ids);
         
            // TODO Alex Che: Magic sliceing. Try to rewrite to sql limits, if its impossible in Caspio.
			if (!$mysqlMode) {
	            if (!empty($limit)) {
				    $ids = array_slice($ids, $offset, $limit);
	            }
				if (count($ids) > 0)        
	                $result = Core_Caspio::caspioSelectWithFieldListMap(TABLE_MASTER_LIST, $fieldListMap, 'TechID IN (' . implode(',', $ids) . ')', ''); 
			}
			else
				$result = $quickResult;
				
            // add prefered field
            foreach ($result as &$res) {
                $res['isPreferred'] = in_array($res['TechID'], $preferesIds);
            }
      
            return  API_Response::success(API_Tech::techOrderArray($result, new API_Tech() ) );
            
        } else {
            // empty results
            return API_Response::success(API_Tech::techOrderArray(array(), new API_Tech() ) );
        }
        
	}

    /**
	 * Filter
	 * @param unknown_type $companyID
	 * @param array $fields
	 * @param array $position
	 * @param API_TechFilter $filters
	 * @return array
	 */
	public static function filterMyRecent($companyID, $fields, $position, API_TechFilter $filters, $offset = 0, $limit= 0, &$countRows = null, $sortBy = null )
    {
		//echo "<pre>Vince Debugging During Thunderstorm filterMyRecent</pre>";
        $requestTable = null;
        $isView = false;
        $requestTable = Core_Database::TABLE_TECH_BANK_INFO;
	    $fieldList = null;

        // fileds list for quick select
        $quickFields = array(
       	    "TechID" => "TechID",
            "PrimaryEmail" => "PrimaryEmail"
        );

	    $sort = $sortBy;
        if (empty($sort) ) {
            $sort = array( "myQty DESC", "Qty_Calls DESC" );
        }

	$fieldList = !empty($fields) ? $fields : Core_Database::getFieldList($requestTable);
	if (!is_array($fieldList)) {
		$fieldList = str_replace(' ', '', $fieldList);
		$fieldListParts = explode(",", $fieldList);
	}else{
		$fieldListParts = $fieldList;
	}

	$fieldListMap = array_combine($fieldListParts, $fieldListParts);
	$criteria = self::assembleCriteriaMysql($companyID, $filters);		
	//echo "<pre>"; print_r($filters); echo "</pre>";

	$db = Core_Database::getInstance();

	$select = $db->select();
	$select->from($requestTable, $fieldListMap)
		->where($criteria)
		->order($sort)
		->having("myQty > 0");
	// added the Having clause to limit results to only techs 
	// that have done work for the client in the past year. 
	if (!empty($limit)) {
		$select->limit($limit, $offset);
	}
		
        if (!empty($position) && !empty($position['ProximityZipCode']) ) {
		$select = Core_Database::whereProximity($select, $position['ProximityLat'], $position['ProximityLng'], "Latitude", "Longitude", "{$position["ProximityDistance"]}.49", "calculated_distance");
		$select->columns(array("Qty_Calls"=> new Zend_Db_Expr("(Qty_IMAC_Calls + Qty_FLS_Service_Calls)") ) );
		$select->columns(array("myQty" => "(SELECT count(*) FROM `work_orders` WHERE TechBankInfo.TechID = work_orders.Tech_ID AND Company_ID = '{$companyID}' AND Date_Completed > DATE_SUB(curdate(), INTERVAL 1 YEAR) ) as myQty"));

		//echo "<pre>Company: $companyID </pre>";
		//echo "<pre>filterMyRecent - " . $select->__toString() . " </pre>";
		$quickResult = $db->fetchAll($select);
        }

        if ( $quickResult && sizeof($quickResult) > 0 ) {
            $ids = array();
			$test = array();	
			$test2 = array();
            foreach ($quickResult as $qItem) {
				//echo "<br />qItem {$qItem['TechID']} myQty: " . $qItem['myQty'] . " vs. " . $qItem['Qty_Calls'] ;
                $ids[ $qItem['Qty_Calls'] ] = $qItem['TechID'];
				$test[ $qItem['Qty_Calls'] ][] = $qItem;
            }
			krsort($ids);
			krsort($test);

			foreach($test as $myQty=>$q2Items){ 
				foreach($q2Items as $q2Item){ 
				$test2[] = $q2Item;
				}
			}

			//echo "<pre>"; print_r($test2); echo "</pre>";
            $countRows = count($ids);
			$result = $quickResult;
            return  API_Response::success(API_Tech::techOrderArray($test2, new API_Tech() ) );
        } else {
            // empty results
            return API_Response::success(API_Tech::techOrderArray(array(), new API_Tech() ) );
        }
        
	}


	/**
	 * Filter
	 * @param unknown_type $companyID
	 * @param array $fields
	 * @param array $position
	 * @param API_TechFilter $filters
	 * @return array
	 */
	public static function filterPreferred($companyID, $fields, $position, API_TechFilter $filters, $offset = 0, $limit= 0, &$countRows = null, $sortBy = null )
    {
        $requestTable = null;
        $isView = false;
        $requestTable = Core_Database::TABLE_TECH_BANK_INFO;
        $fieldList = null;

		//$preferredIds = $preferredList = Core_Tech::getClientPreferredTechsArray($companyID);
		//echo "<pre>Preferred List {$companyID}: "; print_r($preferredList); echo "</pre>";
        (!empty($preferredList)) ? $specialColumns["PreferLevel"] = '(CASE WHEN TechID IN ('.implode(",",$preferredList). ') THEN \'Yes\' ELSE \'No\' END)' : $specialColumns["PreferLevel"] = "'No'";

        // fileds list for quick select
        $quickFields = array(
            "TechID" => "TechID",
            "PrimaryEmail" => "PrimaryEmail"
        );

        $sort = $sortBy;
        if (empty($sort) ) {
            //$sort = "Qty_Calls DESC";
			$sort = array( "isPreferred DESC", "Qty_Calls DESC" );
            //$sort = "{$specialColumns["PreferLevel"]} DESC, {$specialColumns["AudioResume"]} DESC";
        }

        $fieldList = !empty($fields) ? $fields : Core_Database::getFieldList($requestTable);
        if (!is_array($fieldList)) {
            $fieldList = str_replace(' ', '', $fieldList);
            $fieldListParts = explode(",", $fieldList);
        }else{
            $fieldListParts = $fieldList;
        }

        $fieldListMap = array_combine($fieldListParts, $fieldListParts);
        $criteria = self::assembleCriteriaMysql($companyID, $filters);
		//echo "<pre>"; print_r($filters); echo "</pre>";
		//echo "<pre>$criteria</pre>";

        $db = Core_Database::getInstance();
        $select = $db->select();
        $select->from($requestTable, $fieldListMap)
            ->where($criteria)
            ->order($sort);
		$select->having("isPreferred = '1'");
        if (!empty($limit)) {
            $select->limit($limit, $offset);
        }

        if (!empty($position) && !empty($position['ProximityZipCode']) ) {
            $select = Core_Database::whereProximity($select, $position['ProximityLat'], $position['ProximityLng'], "Latitude", "Longitude", "{$position["ProximityDistance"]}.49", "calculated_distance");
            $select->columns(array("Qty_Calls"=> new Zend_Db_Expr("(Qty_IMAC_Calls + Qty_FLS_Service_Calls)") ) );
			$select->columns(array("isPreferred" => "(SELECT count(*) FROM client_preferred_techs t WHERE t.Tech_ID = TechBankInfo.TechID AND t.CompanyID = '{$companyID}')" ) );

			//echo "<pre>Company: {$companyID}</pre>";
            //echo "<pre>filterPreferred - " . $select->__toString() . " </pre>";
            $quickResult = $db->fetchAll($select);
        }

        if ( $quickResult && sizeof($quickResult) > 0 ) {
            $ids = array();
            foreach ($quickResult as $qItem) {
                $ids[] = $qItem['TechID'];
				//echo "Qitem - `{$qItem['TechID']}`.isPreferred : `{$qItem['isPreferred']}` <br />";
            }
            $countRows = count($ids);
            $result = $quickResult;
            //echo "<br />Yes we have preferred Results";
			//$result['isPreferred'] = in_array($result['TechID'], $preferredIds);

            return  API_Response::success(API_Tech::techOrderArray($result, new API_Tech() ) );
        } else {
            // empty results
            return API_Response::success(API_Tech::techOrderArray(array(), new API_Tech() ) );
        }

    }

	/**
	 * Filter
	 * @param unknown_type $companyID
	 * @param array $fields
	 * @param array $position
	 * @param API_TechFilter $filters
	 * @return array
	 */
	public static function filterRecent($fields, $position, API_TechFilter $filters, $offset = 0, $limit= 0, &$countRows = null, $sortBy = null )
    {
		$requestTable = null;
		$mysqlMode = false;
        $isView = false;
        
        $requestTable = Core_Database::TABLE_TECH_BANK_INFO;
		$mysqlMode = true;
		
	    $specialColumns = array(
	       	"AudioResume" => "(CASE WHEN CAST(IFNULL(AudioResume, '') AS CHAR) = '' THEN 'No' ELSE 'Yes' END)", 
	       	"SpecialDistance" => "calculated_distance"
	    );
                
		$fieldList = null;

        $sort = $sortBy;
        if (empty($sort) ) {
			$sort = "Qty_Calls DESC";
        }

	    $fieldList = !empty($fields) ? $fields : Core_Database::getFieldList($requestTable);
		if (!is_array($fieldList)) {
		    $fieldList = str_replace(' ', '', $fieldList);
			$fieldListParts = explode(",", $fieldList);
		} else {
			$fieldListParts = $fieldList;
		}

		$fieldListMap = array_combine($fieldListParts, $fieldListParts);
        
        $criteria = self::assembleCriteriaMysql($companyID, $filters);
		$db = Core_Database::getInstance();
		$select = $db->select();
		$select->from($requestTable, $fieldListMap)
			->where($criteria)
			->order($sort);
	    if (!empty($limit)) {
			$select->limit($limit, $offset);
		}

		$select = Core_Database::whereProximity($select, $position['ProximityLat'], $position['ProximityLng'], "Latitude", "Longitude", "{$position["ProximityDistance"]}.49", "calculated_distance");
		$select->columns(array("Qty_Calls"=> new Zend_Db_Expr("(Qty_IMAC_Calls + Qty_FLS_Service_Calls)") ) );

		$quickResult = $db->fetchAll($select);

        if ( $quickResult && sizeof($quickResult) > 0  ) {
			$ids = array();
            foreach ($quickResult as $qItem) {
				$ids[] = $qItem['TechID'];
            }
            $countRows = count($ids);
			$result = $quickResult;
				
            return  API_Response::success(API_Tech::techOrderArray($result, new API_Tech() ) );
        } else {
            // empty results
            return API_Response::success(API_Tech::techOrderArray(array(), new API_Tech() ) );
        }
        
	}



}