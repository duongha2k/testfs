<?php
/**
 * class API_Applicant
 * @package API_Applicant
 */
class API_Applicant {
	/** 
	 * id
	 * @var int	 */
	var $id;
	/** 
	 * WorkOrderNum
	 * @var string	 */
	var $WorkOrderNum;
    /**
	 * Tech_FName
	 * @var string	 */
	var $Tech_FName;
    /**
	 * BidAmount
	 * @var string	 */
	var $BidAmount;
    /**
	 * TechID
	 * @var number	 */
	var $TechID;
    /**
	 * WorkOrderID
	 * @var number	 */
	var $WorkOrderID;
    /**
	 * CreatedByUser
	 * @var string	 */
	var $CreatedByUser;
    /**
	 * Tech_LName
	 * @var string	 */
	var $Tech_LName;
    /**
	 * Bid_Date
	 * @var string	 */
	var $Bid_Date;
    /**
	 * Hide
	 * @var string	 */
	var $Hide;
    /**
	 * Company_ID
	 * @var string	 */
	var $Company_ID;
    /**
	 * BidModifiedDate
	 * @var string	 */
	var $BidModifiedDate;
    /**
	 * Comments
	 * @var string	 */
	var $Comments;
    /**
	 * ClientEmail
	 * @var string	 */
	var $ClientEmail;
    /**
	 * DeactivatedTech
	 * @var string	 */
	var $DeactivatedTech;

	public function toXMLElement($dom) {
		$pj = $dom->createElement('Applicant');
		$id = $dom->createAttribute('ID');

		$id->appendChild($dom->createTextNode($this->id));
		$pj->appendChild($id);
		return $pj;
	}

	public static function toXMLWithArray($array, $notUsed) {
		$dom = new DOMDocument("1.0");
		$root = $dom->createElement('Applicant');
		foreach ($array as $data) {
			$root->appendChild($data->toXMLElement($dom));
		}
		$dom->appendChild($root);
		return $dom->saveXML();
	}
	
	public static function applicantWithResult($result) {
		$applicant = new API_Applicant();
		if (!empty($result)) {
		    foreach ($result as $key=>$value) {
				if (!array_key_exists($key, $applicant)) continue;
				$applicant->$key = $value;
		    }
		}
		return $applicant;
	}
}