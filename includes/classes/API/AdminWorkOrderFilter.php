<?php
/**
 * class API_AdminWorkOrderFilter
 * @package API_AdminWorkOrderFilter
 */
class API_AdminWorkOrderFilter extends API_Abstract_AdminWorkOrderFilter {

    /**
     * assembleCriteria
     *
     * @param string $companyID
     * @param string $timeStampFrom
     * @param string $timeStampTo
     * @param string $timeStampType
     * @param API_AdminWorkOrderFilter $filters
     * @static
     * @access public
     * @return string
     */
    static public function assembleCriteria($companyID, $timeStampFrom, $timeStampTo, $timeStampType, API_AdminWorkOrderFilter $filters)
    {
		$criteria = $filters->toArray();

		$criteriaArr  = array();
		$db = Core_Database::getInstance();
		$select = $db->select();

		if (!empty($criteria["Company_ID"])) $companyID = $criteria["Company_ID"];

		if (!empty($timeStampFrom) || !empty($timeStampTo) || !empty($timeStampType) || !empty($criteria["UpdateTS_AND"]) || !empty($criteria["UpdateTS_OR"]))
		{
			$tsSelect = $db->select();
			$tsSelect->from(Core_Database::TABLE_TIMESTAMPS, "WO_UNID")->distinct();

			if (!empty($companyID))
				$tsSelect->where("Company_ID = ?", $companyID);

			if (!empty($timeStampType)) {
				$tsSelect->where("Description = ?", '%" . $timeStampType . "%');
			}

			if (!empty($timeStampFrom)) {
				$tsSelect->where("DateTime_Stamp >= ?", $timeStampFrom);
			}
			if (!empty($timeStampTo)) {
				$tsSelect->where("DateTime_Stamp <= ?", $timeStampTo);
			}

			// Created - ShowTechs = '0' AND ISNULL(Tech_ID, 0) = 0 AND Deactivated = '0'
			// Published - ShowTechs = '1' AND ISNULL(Tech_ID, 0) = 0 AND Deactivated = '0' AND ISNULL(FLS_ID,'') = ''
			// Assigned - ISNULL(Tech_ID, 0) != 0 AND Deactivated = '0' AND TechMarkedComplete = '0' AND ISNULL(MissingComments,'') = ''
			// Work Done - TechMarkedComplete = '1' AND Approved = '0' AND TechPaid = '0' AND Invoiced = '0' AND Deactivated = '0'
			// Approved - Approved = '1' AND Invoiced = '0' AND Deactivated = '0'
			// Incomplete - TechMarkedComplete = '0' AND Approved = '0' AND TechPaid = '0' AND Invoiced = '0' AND ISNULL(MissingComments,'') = '' AND Deactivated = '0'
			// In Accounting - Invoiced = '1' AND TechPaid = '0' AND Deactivated = '0'
			// Completed - TechPaid = '1' AND Deactivated = '0'
			// All - Deactivated = '0'
			// Deactivated - Deactivated = '1'

			if (!empty($criteria["UpdateTS_AND"])) {

				$updatedFieldList = str_replace(' ', '', $criteria["UpdateTS_AND"]);
				$updatedFieldList = explode(",", $updatedFieldList);
//				sort($updatedFieldList);
				$singleField = array();

				foreach ($updatedFieldList as $value) {
					$tsSelect->where("Description LIKE ?", "%$value%");
				}
			}

			if (!empty($criteria["UpdateTS_OR"])) {
				$updatedFieldList = str_replace(' ', '', $criteria["UpdateTS_OR"]);
				$updatedFieldList = explode(",", $updatedFieldList);
//				sort($updatedFieldList);

				$singleField = array();
				foreach ($updatedFieldList as $value) {
					$singleField[] = "Description LIKE '%$value%'";
				}

				if (sizeof($singleField) > 0)
					$tsSelect->where("((" . implode(") OR (", $singleField) . "))");
			}

			$timeStamps = false;
			try {
				$result = $select->query()->fetchAll();
				$timeStamps = array();
				foreach ($result as $row=>$values) {
					$timeStamps[] = $values["WO_UNID"];
				}
				unset($result);
			} catch (Exception $e) {
				error_log($e);
				$timeStamps = false;
			}

			if (!empty($timeStamps)) {
				$select->where("WIN_NUM IN (?)", $timeStamps);
			} else {
				return '';
			}
		}

		if (!empty($criteria["WO_State"])) {
			$stateQuery = Core_WorkOrder_Status::getStateConditionsQueryWithName($criteria["WO_State"]);
			if (empty($stateQuery))
				$select->where("Status = ?", $criteria["WO_State"]);
			else
				$select->where("$stateQuery");
		}

        /** @author Roman Gabrusenok Begin **/
        if ( !empty($criteria['AbortFee']) ) {
			$select->where("AbortFee = ?", $criteria['AbortFee']);
        }        
        if ( !empty($criteria['TB_UNID']) ) {
			$select->where("WIN_NUM IN (?)", $criteria['TB_UNID']);
        }

        if ( !empty($criteria['Call_Type']) ) {
			$select->where("Type_ID = ?", $criteria['Call_Type']);
        }

        if ( !empty($criteria['Parts_Presents']) ) {
			$select->where("ReturnPartCount > 0");
        }

        if ( !empty($criteria['Docs_Presents']) ) {
			/*$select->where("Pic1_FromTech IS NOT NULL OR Pic2_FromTech IS NOT NULL OR Pic3_FromTech IS NOT NULL");*/
        }

         //--- DeVry //Tech_ID 
        if (!empty($criteria['DeVry_Tech_ID_List']) ) {    
            $DeVry_Tech_ID_List=$criteria['DeVry_Tech_ID_List'];
            $select->where("Tech_ID IN ($DeVry_Tech_ID_List)");
        }
       
        if ( !empty($criteria['Amount_Per']) ) {
            $amountper = $criteria['Amount_Per'];
            $select->where("Amount_Per = '$amountper'");
        }

        /* Make filter by Tech: all should be grupped in ane place */
		$criteriaTech = $db->select();

		if ( is_array($criteria['Tech_ID_List']) && count($criteria['Tech_ID_List']) > 0) {
			$criteriaTech->where("Tech_ID IN (?)", $criteria['Tech_ID_List']);
		}

        if ( !empty($criteria['Tech']) ) {
            if ( $criteria['Tech'] + 0 ) {
				$criteriaTech->where("Tech_ID = ?", $criteria['Tech']);
            } else {
                $criteria['Tech'] = trim($criteria['Tech']);
                if ( FALSE !== strpos($criteria['Tech'], ' ') ) {
                    $_tech = explode(' ', $criteria['Tech'], 2);
					$criteriaTech->where("Tech_FName LIKE ?", "%" . trim($_tech[0]) . "%");
					$criteriaTech->where("Tech_LName LIKE ?", "%" . trim($_tech[1]) . "%");
                    unset($_tech);
                } else {
					$_tech = "%" . trim($criteria['Tech']) . "%";
					$select->where("Tech_FName LIKE $_tech OR Tech_LName LIKE $_tech");
                    unset($_tech);
                }
            }
        }
        if ( !empty($criteria['Tech_FName']) ) {
			$criteriaTech->where("Tech_FName LIKE ?", "%" . $criteria['Tech_FName'] . "%");
        }
        if ( !empty($criteria['Tech_LName']) ) {
			$criteriaTech->where("Tech_LName LIKE ?", "%" . $criteria['Tech_LName'] . "%");
        }
        if ( !empty($criteria['Tech_ID']) ) {
            $criteriaTech->where("Tech_ID = ?", $criteria['Tech_ID']);
        }
        
        if ( !empty($criteria['FLS_ID']) ) {
			$criteriaTech->where("FLS_ID = ?", $criteria['FLS_ID']);
        }

		$criteriaTech = implode(" ", $criteriaTech->getPart(Zend_Db_Select::WHERE));

        if ( NULL !== $criteria['Sourced'] ) {
            /* any tech filters */
            if ( !empty($criteriaTech) ) {
				$techFilter = '((' . $criteriaTech . ') OR ';
                $bracket = ' )';
            } else {
                $techFilter = '';
                $bracket = '';
            }

            if ( self::strtobool($criteria['Sourced']) ) {
				$select->where($techFilter."((IFNULL(Tech_ID,0) != 0) OR (IFNULL(FLS_ID,'') != ''))".$bracket);
            } else {
				$select->where($techFilter."((IFNULL(Tech_ID,0) = 0) AND (IFNULL(FLS_ID,'') = ''))".$bracket);
            }
        } else if ( !empty($criteriaTech) ) {
			$select->where($criteriaTech);
        }
        unset($criteriaTech);
        /* Make filter by Tech: all should be grupped in ane place */


        if ( !empty($criteria['Project_ID']) ) {
            if ( !is_array($criteria['Project_ID']) ) {
				$select->where("Project_ID = ?", $criteria['Project_ID']);
            } else {
				$select->where("Project_ID IN (?)", $criteria['Project_ID']);
            }
        }
        /** @author Roman Gabrusenok Finish **/

        if (!empty($criteria['StartDateFrom'])) {
		$date = self::dateFormat("Y-m-d", $criteria['StartDateFrom']);
		if ($date)
	                $select->where("StartDate >= ?", $date);
        }
        if (!empty($criteria['StartDateTo'])) {
		$date = self::dateFormat("Y-m-d", $criteria['StartDateTo']);
		if ($date)
	                $select->where("StartDate <= ?", $date);
        }

        if (!empty($criteria['EndDateFrom'])) {
		$date = self::dateFormat("Y-m-d", $criteria['EndDateFrom']);
		if ($date)
	                $select->where("EndDate >= ?", $date);
        }
        if (!empty($criteria['EndDateTo'])) {
		$date = self::dateFormat("Y-m-d", $criteria['EndDateTo']);
		if ($date)
	                $select->where("EndDate <= ?", $date);
        }
        /** @author Alex Scherba **/
        if (!empty($criteria['DateEnteredFrom'])) {
		$date = self::dateFormat("Y-m-d", $criteria['DateEnteredFrom']);
		if ($date)
	                $select->where("DateEntered >= ?", $date);
        }
        if (!empty($criteria['DateEnteredTo'])) {
		$date = self::dateFormat("Y-m-d", $criteria['DateEnteredTo']);
		if ($date)
	                $select->where("DateEntered <= ?", $date);
        }
        if (!empty($criteria['DateApprovedFrom'])) {
		$date = self::dateFormat("Y-m-d", $criteria['DateApprovedFrom']);
		if ($date)
	                $select->where("DateApproved >= ?", $date);
        }
        if (!empty($criteria['DateApprovedTo'])) {
		$date = self::dateFormat("Y-m-d", $criteria['DateApprovedTo']);
		if ($date)
	                $select->where("DateApproved <= ?", $date);
        }
        if (!empty($criteria['DateInvoicedFrom'])) {
		$date = self::dateFormat("Y-m-d", $criteria['DateInvoicedFrom']);
		if ($date)
	                $select->where("DateInvoiced >= ?", $date);
        }
        if (!empty($criteria['DateInvoicedTo'])) {
		$date = self::dateFormat("Y-m-d", $criteria['DateInvoicedTo']);
		if ($date)
	                $select->where("DateInvoiced <= ?", $date);
        }
        if (!empty($criteria['DatePaidFrom'])) {
		$date = self::dateFormat("Y-m-d", $criteria['DatePaidFrom']);
		if ($date)
	                $select->where("DatePaid >= ?", $date);
        }
        if (!empty($criteria['DatePaidTo'])) {
		$date = self::dateFormat("Y-m-d", $criteria['DatePaidTo']);
		if ($date)
	                $select->where("DatePaid <= ?", $date);
        }

        if (!empty($criteria['PayMaxFrom'])) {
                $select->where("PayMax >= ?", $criteria['PayMaxFrom']);
        }
        if (!empty($criteria['PayMaxTo'])) {
                $select->where("PayMax <= ?", $criteria['PayMaxTo']);
        }
        if (!empty($criteria['Net_Pay_AmountFrom'])) {
                $select->where("Net_Pay_Amount >= ?", $criteria['Net_Pay_AmountFrom']);
        }
        if (!empty($criteria['Net_Pay_AmountTo'])) {
                $select->where("Net_Pay_Amount <= ?", $criteria['Net_Pay_AmountTo']);
        }
        if (!empty($criteria['TechEmail'])) {
                $select->where("TechEmail = ?", $criteria['TechEmail']);
        }
        if (!empty($criteria['TechEmailContain'])) {
                $select->where("TechEmail LIKE ?", "%" . $criteria['TechEmailContain'] . "%");
        }
        if (!empty($criteria['Username'])) {
                $select->where("Username LIKE ?", "%" . $criteria['Username'] . "%");
        }
        if (!empty($criteria['Headline'])) {
                $select->where("Headline LIKE ?", "%" . $criteria['Headline'] . "%");
        }
        if (!empty($criteria['WO_Category'])) {
                $select->where("WO_Category = ?", $criteria['WO_Category']);
        }
        if (NULL !== $criteria['TBPay']) {
                $select->where("TBPay = ?", $criteria['TBPay']);
        }

        if (!empty($criteria['WO_ID'])) {
			$select->where("WO_ID LIKE ?", "%" . $criteria['WO_ID'] . "%");
        }

        if (!empty($criteria['PO'])) {
			$select->where("PO LIKE ?", "%" . $criteria['PO'] . "%");
        }
		
		if (isset($criteria['Tech_ID_IS_NULL']) && NULL !== $criteria['Tech_ID_IS_NULL'] ) {
			$select->where('Tech_ID IS NULL');
		}

        // True/False/NULL
        if ( NULL !== $criteria['ShowTechs']) {
			$select->where("ShowTechs = ?", ($criteria['ShowTechs']) ? 1 : 0);
        }

        if (!empty($criteria['Region'])) {
			$select->where("Region LIKE ?", "%" . $criteria['Region'] . "%");
        }

        if (!empty($criteria['SiteName'])) {
			$select->where("SiteName LIKE ?", "%" . $criteria['SiteName'] . "%");
        }
        if (!empty($criteria['Route'])) {
			$select->where("Route LIKE ?", "%" . $criteria['Route'] . "%");
        }
        if (!empty($criteria['SiteNumber'])) {
			$select->where("SiteNumber LIKE ?", $criteria['SiteNumber']);
        }

        if (!empty($criteria['City'])) {
			$select->where("City LIKE ?", "%" . $criteria['City'] . "%");
        }

        if (!empty($criteria['State'])) {
			$select->where("State LIKE ?", $criteria['State']);
        }

        if (!empty($criteria['Zipcode'])) {
            $select->where("Zipcode LIKE ?", "%" . $criteria['Zipcode'] . "%");
        }

        if (!empty($criteria['Deactivated_Reason'])) {
			$select->where("Deactivated_Reason LIKE ?", $criteria['Deactivated_Reason']);
        }
        
        // True/False/NULL
        if ( NULL !== ($criteria['Update_Requested']) ) {
            $select->where("Update_Requested = ?", ($criteria['Update_Requested']) ? 1 : 0);
        }

        // True/False/NULL
        if (NULL !== $criteria['CheckedIn']) {
            $select->where("CheckedIn = ?", ($criteria['CheckedIn']) ? 1 : 0);
        }

        //  True/False/NULL
        if ( NULL !== $criteria['StoreNotified'] ) {
            $select->where("StoreNotified = ?", ($criteria['StoreNotified']) ? 1 : 0);
        }
        //  True/False/NULL
        if ( NULL !== $criteria['Paperwork_Received'] ) {
            $select->where("Paperwork_Received = ?", ($criteria['Paperwork_Received']) ? 1 : 0);
        }
        //  True/False/NULL
        if ( NULL !== $criteria['Lead'] ) {
			$select->where("Lead = ?", ($criteria['Lead']) ? 1 : 0);
		}
        //  True/False/NULL
        if ( NULL !== $criteria['Assist'] ) {
            $select->where("Assist = ?", ($criteria['Assist']) ? 1 : 0);
        }
        // True/False/NULL
        if ( NULL !== $criteria['WorkOrderReviewed']) {
            $select->where("WorkOrderReviewed = ?", ($criteria['WorkOrderReviewed']) ? 1 : 0);
        }
        // True/False/NULL
        if ( NULL !== $criteria['TechCheckedIn_24hrs']) {
            $select->where("TechCheckedIn_24hrs = ?", ($criteria['TechCheckedIn_24hrs']) ? 1 : 0);
        }
        // True/False/NULL
        if ( NULL !== $criteria['TechMarkedComplete']) {
            $select->where("TechMarkedComplete = ?", ($criteria['TechMarkedComplete']) ? 1 : 0);
        }
        if ( NULL !== $criteria['TechCalled']) {
            $select->where("TechCalled = ?", ($criteria['TechCalled']) ? 1 : 0);
        }
        // True/False/NULL
        if ( NULL !== $criteria['Site_Complete'] ) {
            $select->where("Site_Complete = ?", ($criteria['Site_Complete']) ? 1 : 0);
        }

        // True/False/NULL
        if ( NULL !== $criteria['Approved']) {
            $select->where("Approved = ?", ($criteria['Approved']) ? 1 : 0);
        }
       
        // True/False/NULL
        if ( NULL !== $criteria['PcntDeduct']) { 
            $select->where("PcntDeduct = ?", ($criteria['PcntDeduct']) ? 1 : 0);
        }
        
        if (!empty($criteria['Additional_Pay_Amount']) && $criteria['Additional_Pay_Amount'] == true) { 
            $select->where("Additional_Pay_Amount IS NOT ?", new Zend_Db_Expr('NULL'));
        }
        
        // True/False/NULL
        if ( NULL !== $criteria['PricingRan']) { 
            $select->where("PricingRan = ?", ($criteria['PricingRan']) ? 1 : 0);
        }
        
        //  True/False/NULL
        if ( NULL !== $criteria['Invoiced'] ) {
            $select->where("Invoiced = ?", ($criteria['Invoiced']) ? 1 : 0);
        }
        
        // True/False/NULL
        if ( NULL !== $criteria['Work_Out_of_Scope']) {
            $select->where("Work_Out_of_Scope = ?", ($criteria['Work_Out_of_Scope']) ? 1 : 0);
        }
        // True/False/NULL
        if ( NULL !== $criteria['TechPaid']) {
            $select->where("TechPaid = ?", ($criteria['TechPaid']) ? 1 : 0);
        }

        if ( !empty($criteria['Short_Notice']) ) {
			$select->where("ShortNotice LIKE ?", $criteria['ShortNotice']);
        }

        
        if (!empty($criteria['SATRecommended_From'])) {
			$select->where("SATRecommended >= ?", $criteria['SATRecommended_From']);
        }
        
        if (!empty($criteria['SATPerformance_From'])) {
			$select->where("SATPerformance >= ?", $criteria['SATPerformance_From']);
        }
        //-- 13622
        if (!empty($criteria['InCompanyIDArray']) && count($criteria['InCompanyIDArray']) >0 ) {
            $select->where("Company_ID IN (?)", $criteria['InCompanyIDArray']);
        }
        // 13760
        if (!empty($criteria['Pay_ISO']) && count($criteria['Pay_ISO']) >0 ) {
            $select->where("Tech_ID IN (?)", $criteria['Pay_ISO']);                    
        }
        
        /**
        * ###########################
        * defaults to 'any' instead of 'false' as the Find Work Orders screen does
        * ###########################
        */
        //  True/False/NULL
        if (NULL !== $criteria['Deactivated']) {
			$select->where("work_orders.Deactivated = ?", ($criteria['Deactivated']) ? 1 : 0);
		}
		
        if (!empty($criteria['DeactivationCode']) ) {
			$select->where("DeactivationCode = ?", $criteria['DeactivationCode']);
		}
		
        if ($criteria['wm_tech_select'] === 1 || $criteria['wm_tech_select'] === '1') {
			$select->where("tbi.WMID IS NOT NULL", $criteria['wm_tech_select']);
        }
		else if ($criteria['wm_tech_select'] === 0 || $criteria['wm_tech_select'] === '0') {
			$select->where("tbi.WMID IS NULL", $criteria['wm_tech_select']);
		}
		
		if (!empty($criteria['wm_tech_id'])) {
			$select->where("tbi.WMID = ?", $criteria['wm_tech_id']);
		}
		
		if (!empty($criteria['wm_assignment_id'])) {
			$select->where("waf.WMAssignmentID = ?", $criteria['wm_assignment_id']);
		}
		
        if ( !empty($companyID) ) {
            $select->where("Company_ID = ?", $companyID);
        } else if ( !empty($criteria['Company_ID']) ) {
			$select->where("Company_ID = ?", $criteria['Company_ID']);
        }
                       
        return implode(" ", $select->getPart(Zend_Db_Select::WHERE));
    }

    /**
     * filter
     *
     * @param string $companyID
     * @param string $fields
     * @param string $timeStampFrom
     * @param string $timeStampTo
     * @param string $timeStampType
     * @param API_WorkOrderFilter $filters
     * @param string $sort
     * @param int $limit
     * @param int $offset
     * @static
     * @access public
     * @return Core_Filter_Result
     */
	public static function filter($companyID, $fields, $timeStampFrom, $timeStampTo, $timeStampType, API_AdminWorkOrderFilter $filters, $sort = '', $limit = 0, $offset = 0, &$count = null)
    {

		$db = Core_Database::getInstance();
		$select = $db->select();
		if (!empty($fields)) {
			$fieldList = str_replace(' ', '', $fields);
			$fieldListParts = explode(",", $fieldList);
		}
		else {
			$fieldListParts = Core_Database::getFieldList(Core_Database::TABLE_WORK_ORDERS);
		}

		$fieldListMap = array_combine($fieldListParts, $fieldListParts);

//		if (!empty($fieldListMap["Status"]) || empty($fields))
//			$fieldListMap["Status"] = Core_WorkOrder_Status::getStatusQueryAsName();


		$select->from(Core_Database::TABLE_WORK_ORDERS, $fieldListMap);

//        if ($filters->wm_tech_select != '' || !empty($filters->wm_tech_id)) {
			$select->joinLeft(array('tbi' => Core_Database::TABLE_TECH_BANK_INFO), 'Tech_ID = TechID', array('WMID'));
//		}
		
//		if (!empty($filters->wm_assignment_id)) {
			$select->join(array('waf' => 'work_orders_additional_fields'), 'WIN_NUM = WINNUM', array('WMAssignmentID'));
//		}
		
		$sort = strtolower($sort);
		$sortParts = explode(',', $sort);
                // fix for sorting text that should be float
                foreach ($sortParts as $key=>$part) {
                        $part = trim($part);
                        if ($part == "tb_unid asc" || $part == "tb_unid desc") {
                                $p = explode(" ", $part);
                                $sortParts[$key] = "win_num " . $p[1];
                        }
                        if ($part == "payamount asc" || $part == "payamount desc") {
                                $p = explode(" ", $part);
                                $sortParts[$key] = "(CASE WHEN payamount IS NOT NULL THEN payamount ELSE 99999 END) " . $p[1];
                        }
                        if ($part == "paymax asc" || $part == "paymax desc") {
                                $p = explode(" ", $part);
                                $sortParts[$key] = "(CASE WHEN paymax IS NOT NULL THEN paymax ELSE 99999 END) " . $p[1];
                        }
                        else if ($part == "startdate asc" ||
                                $part == "enddate asc") {
                                $p = explode(" ", $part);
                                $position = $p[1] == "asc" ? "2079-06-06" : "startdate";
                                $sortParts[$key] = "(CASE WHEN startdate IS NOT NULL THEN startdate ELSE 999999 END) " . $p[1];
                        }
                        else if ($part == "startdate desc") {
                                $p = explode(" ", $part);
                                $position = "1980-01-01";  
                                $sortParts[$key] = "(CASE WHEN startdate IS NOT NULL THEN startdate ELSE $position END) " . $p[1];
                        }
                        
                        /*else if ($part == "status asc" || $part == "status desc") {
                                $p = explode(" ", $part);
                                $sortParts[$key] = "(" . Core_WorkOrder_Status::getStatusQueryAsName() . ") {$p[1]}";
                        }*/
                }

		$select->order($sortParts);

		if ($offset != 0 || $limit != 0)
			$select->limit($limit, $offset);

		$criteria = self::assembleCriteria($companyID, $timeStampFrom, $timeStampTo, $timeStampType, $filters);
        
                
		if (!empty($criteria))
			$select->where($criteria);
                

        $result = new Core_Filter_Result($select);
		if ($count !== null) $count = count($result);
		return $result;
                
                
	}
}

/*
 * (CASE WHEN (ShowTechs = 0 AND ISNULL(Tech_ID, 0) = 0 AND Deactivated = 0 AND Approved = 0) THEN 'created' WHEN (ShowTechs = 1 AND ISNULL(Tech_ID, 0) = 0 AND Deactivated = 0 AND ISNULL(FLS_ID,'') = '') THEN 'published' WHEN (ISNULL(Tech_ID, 0) != 0 AND Deactivated = 0 AND TechMarkedComplete = 0 AND ISNULL(CAST (MissingComments AS VARCHAR(MAX)),'') = '') THEN 'assigned' WHEN (TechMarkedComplete = 1 AND Approved = 0 AND TechPaid = 0 AND Invoiced = 0 AND Deactivated = 0) THEN 'work done' WHEN (TechMarkedComplete = 0 AND Approved = 0 AND TechPaid = 0 AND Invoiced = 0 AND ISNULL(CAST (MissingComments AS VARCHAR(MAX)),'') != '' AND Deactivated = 0) THEN 'incomplete' WHEN (Approved = 1 AND Invoiced = 0 AND Deactivated = 0) THEN 'approved' WHEN (Invoiced = 1 AND TechPaid = 0 AND Deactivated = 0) THEN 'in accounting' WHEN (TechPaid = 1 AND Deactivated = 0) THEN 'completed' WHEN (Deactivated = 1) THEN 'deactivated' ELSE '' END)
 */
