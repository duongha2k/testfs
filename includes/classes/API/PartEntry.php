<?php
/**
 * class API_PartEntry
 * @package API_PartEntry
 */
class API_PartEntry {
	
	/** 
	 * 
	 * @var int	 */
	var $id;

	/** 
	 * 
	 * @var boolean	 */
	var $Consumable;	

	/** 
	 * array of API_Part
	 * @var API_NewPart	 */
	var $newPart;
	
	/** 
	 * array of API_Part
	 * @var API_ReturnPart	 */
	var $returnPart;

	public function __construct($id, $Consumable, $parts = NULL) {
		$this->id = $id;
		$this->Consumable = $Consumable;
//		$this->parts = $parts == NULL ? array() : $parts;
	}

	public function toXMLElement($dom, $short = false) {
		$entry = $dom->createElement('PartEntry');
		$id = $dom->createAttribute('id');
		$id->appendChild($dom->createTextNode($this->id));
		$entry->appendChild($id);
		if ($short) return $entry;
		$consumable = $this->Consumable;
		$element = $dom->createElement("Consumable");
		$element->appendChild($dom->createTextNode($consumable));
		$entry->appendChild($element);
		
		if (!empty($this->newPart))
			$entry->appendChild($this->newPart->toXMLElement($dom));
		
		if (!empty($this->returnPart))
			$entry->appendChild($this->returnPart->toXMLElement($dom));

		return $entry;
	}	

	public static function partEntryWithResult($result) {	
		if (!empty($result)) {
			$value = $result[0];
			$consumable = false;
			if (array_key_exists("Consumable", $value)) {
				$consumable = $value["Consumable"];
				unset($result[0]["Consumable"]);
			}
			$entry = new API_PartEntry($value['PartEntryID'], $consumable);
			unset($result[0]["PartEntryID"]);
			foreach ($result as $k=>$value) {
				// make part
				if ($value["IsNew"] == "True") {
					$entry->newPart = API_NewPart::partWithResult($value);
				}
				else {
					$entry->returnPart = API_ReturnPart::partWithResult($value);
				}
			}
		}
		else
			$entry = new API_PartEntry();
			
		return $entry;
	}

	public static function toXMLWithArray($array, $params) {			
		$single = $params["single"];
		$short = $params["short"];
		$dom = new DOMDocument("1.0");
		if (!$single) {
			$root = $dom->createElement('Parts');
			$dom->appendChild($root);
		}
		else
			$root = $dom;

		foreach ($array as $data) {
			$root->appendChild($data->toXMLElement($dom, $short));
		}
		return $dom->saveXML();
	}
	
	public static function partEntryToShortResponseXML($object) {
		$id = $object->id;
		$dom = new DOMDocument("1.0");
		$root = $dom->createElement('PartEntry');
		$attr = $dom->createAttribute("id");
		$attr->appendChild($dom->createTextNode($id));
		$root->appendChild($attr);
		$dom->appendChild($root);
		return $dom->saveXML();
	}
}