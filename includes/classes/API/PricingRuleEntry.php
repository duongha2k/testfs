<?php
/**
 * class API_PartEntry
 * @package API_PartEntry
 */
class API_PricingRuleEntry {
	
	/** 
	 * 
	 * @var int	 */
	var $PricingRuleId;

	/** 
	 * 
	 * @var boolean	 */
	var $Description;	

	public function __construct($PricingRuleId, $Description, $parts = NULL) {
		$this->PricingRuleId = $PricingRuleId;
		$this->Description = $Description;
	}

	public static function partEntryWithResult($result) {	
		if (!empty($result)) {
			$entry = new API_PricingRuleEntry($result['PricingRuleID'], $result['Description']);
		}
		else
			$entry = new API_PricingRuleEntry();
			
		return $entry;
	}


}