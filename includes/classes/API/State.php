<?php
/**
 * class API_State
 * @package API_State
 * @author Alex Scherba
 */
class API_State {

	/*
	 * State
	 * @var string */
	var $State;

	/*
	 * Abbreviation
	 * @var string */
	var $Abbreviation;

	/*
	 * Country
	 * @var string */
	var $Country;
	
	public static function stateWithResult($result) {
		$state = new API_State();
		if (!empty($result)) {
		    foreach ($result as $key => $value) {
                if (!property_exists($state, $key)) continue;
				$state->$key = $value;
		    }
		}
		return $state;
	}
}