<?php
/**
 * class API_ReturnPart
 * @package API_Part
 * @subpackage API_ReturnPart
 */
class API_ReturnPart {
	/** 
	 * 
	 * @var string	 */
	var $RMA;


	/** 
	 * 
	 * @var string	 */
	var $Instructions;

	public function toXMLElement($dom) {	
		$part = $dom->createElement("ReturnPart");
		
		$part = $this->dataToXML($dom, $part);
		
		return $part;
	}

	public static function partWithResult($result) {
		$part = new API_ReturnPart();
		$ignoreField = self::$ignoreNewPartsField;

		$part = API_ReturnPart::resultToData($result, $part, $ignoreField);
		return $part;
	}

/*--------------*/

	static $ignorePartsField = array("id" => 1, "IsNew" => 1, "PartEntryID" => 1);
	static $ignoreNewPartsField = array("ShippingAddress" => 1, "ShipTo" => 1);
	static $ignoreReturnPartsField = array("RMA" => 1, "Instructions" => 1);
	static $dateFields = array("ETA" => 1);

	/** 
	 * 
	 * @var string	 */
	var $PartNum;


	/** 
	 * 
	 * @var string	 */
	var $ModelNum;


	/** 
	 * 
	 * @var string	 */
	var $SerialNum;


	/** 
	 * 
	 * @var string	 */
	var $Description;


	/** 
	 * 
	 * @var string	 */
	var $Carrier;


	/** 
	 * 
	 * @var string	 */
	var $TrackingNum;


	/** 
	 * 
	 * @var string	 */
	var $ETA;
	
	protected function dataToXML($dom, $part) {
		foreach ($this as $k=>$v) {
			if (array_key_exists($k, self::$ignorePartsField)) continue;
			if ($v == "NULL")
				$val = "";
			else if (!empty($v) && array_key_exists($k, self::$dateFields )) {
				if ($ts = strtotime($v))
					$val = date("m/d/Y", $ts);
				else
					$val = NULL;
			}
			else
				$val = $v;
				
			$element = $dom->createElement($k);
			$element->appendChild($dom->createTextNode( htmlspecialchars($val, ENT_QUOTES) ));
			$part->appendChild($element);
		}
		return $part;
	}
	
/*	public static function partWithResult($value) {
		return $value["IsNew"] == "True" ? API_NewPart::partWithResult($value) : API_ReturnPart::partWithResult($value);
	}*/
	
	protected static function resultToData($result, $part,$ignoreField) {
		foreach ($result as $k=>$v) {
			if (array_key_exists($k, self::$ignorePartsField) || array_key_exists($k, $ignoreField)) continue;
			if ($v == "NULL")
				$val = "";
			else if (!empty($v) && array_key_exists($k, self::$dateFields )) {
				if ($ts = strtotime($v))
					$val = date("m/d/Y", $ts);
				else
					$val = NULL;
			}
			else
				$val = $v;
				
			$part->$k = $val;
		}
		return $part;
	}
	
}