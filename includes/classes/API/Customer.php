<?php
/**
 * class API_Customer
 * @package API_Customer
 */
class API_Customer {
	/** 
	 * UNID
	 * @var int	 */
	var $UNID;

	/** 
	 * Customer_Name
	 * @var string	 */
	var $Customer_Name;

	public function toXMLElement($dom) {
		$cst = $dom->createElement('Customer');
		$id = $dom->createAttribute('ID');
		$name = $dom->createAttribute('NAME');

		$id->appendChild($dom->createTextNode($this->UNID));
		$name->appendChild($dom->createTextNode($this->Customer_Name));

		$cst->appendChild($id);
		$cst->appendChild($name);

		return $cst;
	}

	public static function toXMLWithArray($array, $notUsed) {
		$dom = new DOMDocument("1.0");
		$root = $dom->createElement('Customer');
		foreach ($array as $data) {
			$root->appendChild($data->toXMLElement($dom));
		}
		$dom->appendChild($root);
		return $dom->saveXML();
	}
	
	public static function customerWithResult($result) {
		$customer = new API_Customer();
		if (!empty($result)) {
		    foreach ($result as $key=>$value) {
				if (!array_key_exists($key, $customer)) continue;
				$customer->$key = $value;
		    }
		}
		return $customer;
	}
}