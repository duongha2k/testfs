<?php
/**
 * class API_Abstract_WorkOrder
 * @package API_Abstract_WorkOrder
 */
abstract class API_Abstract_WorkOrder {	
	public static function internalFields() {
		return array('USERNAME','PASSWORD','PAID','PAYAPPROVE','CHECK_NUMBER','MOVEDTOMYSQL','ATTACHEDFILE',
    	                        'USEMYRECRUITEMAILSETTINGS','PRICINGRULEID','CALCULATEDINVOICE','PRICINGCALCULATIONTEXT',
    	                        'TECHNICIANFEE','PRICINGRULEAPPLIED','URGENT','FSSOURCINGSUPPORT','PRICINGRAN',
    	                        'REMINDERUSEWOSETTING','COMPANY_NAME','COMPANY_ID','CALCULATEDTOTALTECHPAY','PAYAMOUNT_FINAL',
    	                        'PRICINGACCUMULATOR', 'PRICINGACCUMULATOR2', "DEACTIVATED_DUMMYTECHFLAG", "TBPAY");
	}
	
	public static function readOnlyFields() {
		// override
		return array();
	}
	
	public static function fields() {
		return array_keys(new self());
    }
	
    /**
     * TBPay
     * 
     * @var boolean
     * @access public
     */
    var $TBPay;
    /**
     * Pic1_FromTech
     *
     * @var string File path
     * @access public
     */
    var $Pic1_FromTech;
    /**
     * Pic2_FromTech
     * 
     * @var string File path
     * @access public
     */
    var $Pic2_FromTech;
    /**
     * Pic3_FromTech
     * 
     * @var string File path
     * @access public
     */
    var $Pic3_FromTech;

    
	/** 
	 * 
	 * @var string	 */
	var $Pic1;

	/** 
	 * 
	 * @var string	 */
	var $Pic2;

	/** 
	 * 
	 * @var string	 */
	var $Pic3;
    
	/** 
	 * 
	 * @var int	 */
	var $WIN_NUM;


	/** 
	 * 
	 * @var string	 */
	var $WO_ID;


	/** 
	 * 
	 * @var string	 */
	var $DateEntered;


	/** 
	 * 
	 * @var string	 */
	var $StartDate;


	/** 
	 * 
	 * @var string	 */
	var $EndDate;


	/** 
	 * 
	 * @var string	 */
	var $Description;


	/** 
	 * 
	 * @var string	 */
	var $Requirements;


	/** 
	 * 
	 * @var string	 */
	var $Tools;


	/** 
	 * 
	 * @var string	 */
	var $PayMax;


	/** 
	 * 
	 * @var string	 */
	var $Address;


	/** 
	 * 
	 * @var string	 */
	var $City;


	/** 
	 * 
	 * @var string	 */
	var $State;


	/** 
	 * 
	 * @var string	 */
	var $Zipcode;


	/** 
	 * 
	 * @var string	 */
	var $Tech_FName;
	

	/** 
	 * 
	 * @var boolean	 */
	var $Approved;


	/** 
	 * 
	 * @var string	 */
	var $SpecialInstructions;


	/** 
	 * 
	 * @var string	 */
	var $SiteName;


	/** 
	 * 
	 * @var int	 */
	var $Tech_ID;

	/** 
	 * 
	 * @var string	 */
	var $Contractor_ID;


	/** 
	 * 
	 * @var boolean	 */
	var $TechMarkedComplete;


	/** 
	 * 
	 * @var string	 */
	var $TechComments;


	/** 
	 * 
	 * @var string	 */
	var $MissingComments;


	/** 
	 * 
	 * @var string	 */
	var $Tech_LName;


	/** 
	 * 
	 * @var boolean	 */
	var $TechPaid;


	/** 
	 * 
	 * @var string	 */
	var $DatePaid;


	/** 
	 * 
	 * @var float	 */
	var $PayAmount;


	/** 
	 * 
	 * @var string	 */
	var $SiteNumber;


	/** 
	 * 
	 * @var string	 */
	var $Region;

	/** 
	 * 
	 * @var string	 */
	var $StartTime;

	/** 
	 * 
	 * @var string	 */
	var $SitePhone;


	/** 
	 * 
	 * @var string	 */
	var $SiteEmail;


	/** 
	 * 
	 * @var string	 */
	var $SiteFax;


	/** 
	 * 
	 * @var string	 */
	var $PO;


	/** 
	 * 
	 * @var string	 */
	var $General1;


	/** 
	 * 
	 * @var string	 */
	var $General2;


	/** 
	 * 
	 * @var string	 */
	var $General3;


	/** 
	 * 
	 * @var string	 */
	var $General4;


	/** 
	 * 
	 * @var string	 */
	var $General5;


	/** 
	 * 
	 * @var string	 */
	var $General6;


	/** 
	 * 
	 * @var string	 */
	var $General7;


	/** 
	 * 
	 * @var string	 */
	var $General8;


	/** 
	 * 
	 * @var string	 */
	var $General9;


	/** 
	 * 
	 * @var string	 */
	var $General10;


	/** 
	 * 
	 * @var string	 */
	var $Ship_Contact_Info;


	/** 
	 * 
	 * @var boolean	 */
	var $WorkOrderReviewed;


	/** 
	 * 
	 * @var boolean	 */
	var $TechCheckedIn_24hrs;

	/** 
	 * 
	 * @var string	 */
	var $Project_Name;


	/** 
	 * 
	 * @var int	 */
	var $Project_ID;


	/** 
	 * 
	 * @var string	 */
	var $Time_In;


	/** 
	 * 
	 * @var string	 */
	var $Time_Out;


	/** 
	 * 
	 * @var string	 */
	var $TechEmail;


	/** 
	 * 
	 * @var string	 */
	var $TechPhone;


	/** 
	 * 
	 * @var string	 */
	var $Date_Assigned;

	/** 
	 * 
	 * @var float	 */
	var $Additional_Pay_Amount;

	/** 
	 * 
	 * @var string	 */
	var $EndTime;

	/** 
	 * 
	 * @var string	 */
	var $Amount_Per;


	/** 
	 * 
	 * @var float	 */
	var $OutofScope_Amount;


	/** 
	 * 
	 * @var int	 */
	var $Qty_Devices;


	/** 
	 * 
	 * @var string	 */
	var $Site_Contact_Name;


	/** 
	 * 
	 * @var string	 */
	var $StartRange;


	/** 
	 * 
	 * @var string	 */
	var $Duration;


	/** 
	 * 
	 * @var int	 */
	var $Type_ID;

	/** 
	 * 
	 * @var float	 */
	var $Latitude;


	/** 
	 * 
	 * @var float	 */
	var $Longitude;

	/** 
	 * 
	 * @var float	 */
	var $Tech_Bid_Amount;

	/** 
	 * 
	 * @var float	 */
	var $TripCharge;

	/** 
	 * 
	 * @var float	 */
	var $MileageReimbursement;


	/** 
	 * 
	 * @var float	 */
	var $MaterialsReimbursement;

	/** 
	 * 
	 * @var int	 */
	var $TechMiles;


	/** 
	 * 
	 * @var float	 */
	var $AbortFeeAmount;


	/** 
	 * 
	 * @var string	 */
	var $WO_Category;


	/** 
	 * 
	 * @var string	 */
	var $Headline;


	/** 
	 * 
	 * @var int	 */
	var $TechPhoneExt;

	/** 
	 * 
	 * @var string	 */
	var $ProjectManagerName;


	/** 
	 * 
	 * @var string	 */
	var $ProjectManagerPhone;


	/** 
	 * 
	 * @var string	 */
	var $ProjectManagerEmail;


	/** 
	 * 
	 * @var string	 */
	var $ResourceCoordinatorName;


	/** 
	 * 
	 * @var string	 */
	var $ResourceCoordinatorPhone;


	/** 
	 * 
	 * @var string	 */
	var $ResourceCoordinatorEmail;


	/** 
	 * 
	 * @var string	 */
	var $EmergencyName;


	/** 
	 * 
	 * @var string	 */
	var $EmergencyPhone;


	/** 
	 * 
	 * @var string	 */
	var $EmergencyEmail;


	/** 
	 * 
	 * @var string	 */
	var $TechnicalSupportName;


	/** 
	 * 
	 * @var string	 */
	var $TechnicalSupportPhone;


	/** 
	 * 
	 * @var string	 */
	var $TechnicalSupportEmail;


	/** 
	 * 
	 * @var string	 */
	var $CheckInOutName;


	/** 
	 * 
	 * @var string	 */
	var $CheckInOutNumber;


	/** 
	 * 
	 * @var string	 */
	var $CheckInOutEmail;

	/** 
	 * 
	 * @var string	 */
	var $Date_In;


	/** 
	 * 
	 * @var string	 */
	var $Date_Out;


	/** 
	 * 
	 * @var boolean	 */
	var $Lead;


	/** 
	 * 
	 * @var boolean	 */
	var $Assist;


	/** 
	 * 
	 * @var boolean	 */
	var $SignOffSheet_Required;

	/** 
	 * 
	 * @var string	 */
	var $ExtraTime;
	
	/** 
	 * 
	 * @var int */
	var $NewPartCount;

	/** 
	 * 
	 * @var string	 */
	var $ReturnPartCount;

	/**
	 *
	 * @var string	 */
	var $Username;

	/** 
	 * 
	 * @var string	 */
	var $Status;

	/** 
	 * 
	 * @var boolean	 */
	var $PcntDeduct;

	/** 
	 * 
	 * @var string	 */

	var $BU1FLSID;
	
	/** 
	 * TODO What field in DB referrenced to?
	 * @var string	 */
	var $BU1Info;

    /**
     *
     * @var string
     */
	var $Country;
	
	 /** 
	 * 
	 * @var boolean	 */
	var $AbortFee;	

    /**
     *
     * @var string
     */
	var $QBID;

    /** 
     * 
     * @var boolean     */
    var $PartManagerUpdate_Required;

	var $Route;
	
    var $CredentialCertificationId; /*13329*/
    var $PcntDeductPercent; /*13334*/
    
    var $WorkOrderOwner;
    
    var $SystemGeneratedEmailTo;
    
    var $SystemGeneratedEmailFrom;
    
    var $BidNotificationEmailTo;
    
    var $P2TNotificationEmailTo;
    
    var $WorkAssignedEmailTo;
    
    var $WorkDoneEmailTo;
    
    var $WorkAvailableEmailFrom;
    
        /** 
     * 
     * @var boolean     */
    var $isProjectAutoAssign;


    /** 
     * 
     * @var boolean     */
    var $isWorkOrdersFirstBidder;

	var $Total_Reimbursable_Expense;

    var $UpdatedPartCount;

    /** 
     * 
     * @var int     */
    var $Qty_Visits; //13541, 13543
 var $Company_ID;
    var $Company_Name;
    
    var $AuditNeeded; 
    var $AuditNeededBy; 
    var $AuditNeededDate; 
    var $AuditComplete; 
    var $AuditCompleteBy; 
    var $AuditCompleteDate; 
    var $EntityTypeId; //13622
    var $ServiceTypeId; //13622
    var $FSClientServiceDirectorId; //13622
    var $FSAccountManagerId; //13622    
    var $NumOfVisits;
    var $AllowDeVryInternstoObserveWork; //13713
    var $AllowDeVryInternstoPerformField; //13713    
    var $ACSNotifiPOC; //13691
    var $ACSNotifiPOC_Phone; //13691
    var $ACSNotifiPOC_Email; //13691
    var $SiteContactBackedOutChecked;//13732
    var $NotifiedTime;//13732
    var $StandardOffset;//13892
    var $TimeZone;//13892
    /** 
     * 
     * @var boolean     */
    var $CustomSignOff_Enabled;

    /** 
     * 
     * @var boolean     */
    var $SignOff_Disabled;
	var $PushWM;

    //---
    public function __construct($id = null) {
		$this->WIN_NUM = $id;
	}
	
	public function toXMLElement($dom, $fieldList = NULL, $isResponse = FALSE) {
		if (!is_array($fieldList))
			$fieldList = $this;
		$root = $dom->createElement('WO');
		foreach ($this as $key => $value) {
			if (!array_key_exists($key, $fieldList) || $value === NULL) continue;
			if ($key == "WIN_NUM" && !$isResponse) $key = "TB_UNID";
			if (is_bool($value))
				$value = $value === true ? "True" : "False";	
			$parameter = $dom->createElement($key, htmlspecialchars($value, ENT_QUOTES));
			$root->appendChild($parameter);
		}
		$dom->appendChild($root);
		return $root;
	}
	
	public static function workOrderWithResult($result, $workOrder = NULL) {
	    if ($workOrder == NULL)
            $workOrder = new API_WorkOrder(NULL);
	    if (!empty($result)) {
		$fieldListType = Core_Database::getFieldListType(Core_Database::TABLE_WORK_ORDERS);

		    foreach ($result as $k=>$val) {
		        
		    	$field = $k == "TB_UNID" ? "WIN_NUM" : $k;
				if (!$workOrder->fieldExists($field)) continue;
				if (strtolower($result[$k]) == "true") $result[$k] = true;
				if (strtolower($result[$k]) == "false") $result[$k] = false;
				if ($fieldListType[$k] == "tinyint" && !is_bool($result[$k])) $result[$k] = $result[$k] == 1;
				$workOrder->$field = $result[$k];
		    }
	    }
	    return $workOrder;
	}

	public static function toXMLWithArray($array, $param = NULL, $isResponse = FALSE) {
		if ($param == NULL)
			$param = array();
		$fieldList = array_key_exists("fieldList", $param) ? $param["fieldList"] : NULL;
		$single = array_key_exists("single", $param) ? $param["single"] : false;
		if ($fieldList != NULL)
			$fieldList = array_flip(explode(",", $fieldList));
		$dom = new DOMDocument("1.0");
		if (!$single) {
			$root = $dom->createElement("WOS");
			$dom->appendChild($root);
		}
		else
			$root = $dom;
	    if (!empty($array[0])) {
			foreach ($array as $key=>$value) {
				$root->appendChild($value->toXMLElement($dom, $fieldList, $isResponse));
			}
	    }
		return $dom->saveXML();
	}

	public static function makeWorkOrderArray($result, $object) {
	    $wo = array();
	    if (!empty($result[0])) {
			foreach ($result as $key=>$value) {
				$clone = clone $object;
				$wo[] = self::workOrderWithResult($value, $clone);
			}
	    }
		return $wo;
	}
	
	public function canShowCalculatedInvoice() {
		// used to check if should show calculated invoice amount to client
		if (!property_exists($this, "calculatedInvoice")) return false;
		return self::canShowCalculatedInvoiceWithDate($this->DateInvoiced);
	}

	public static function canShowCalculatedInvoiceWithDate($date) {
		// used to check if should show calculated invoice amount to client
		try {
			$dateInvoiced = strtotime($date);
		} catch (Exception $e) {
			return false;
		}
		if (empty($dateInvoiced) || !$dateInvoiced || date("Ymd") <= date("Ymd", $dateInvoiced))
			// do not show calculated invoice until day after invoice date
			return false;
		return true;
	}

	public function fieldExists($field) {
		return array_key_exists($field, $this);
	}
	
/*	private static function workOrderWithArray($array) {
	    $wo = new API_WorkOrder($value["TB_UNID"]);
	    foreach ($value as $k=>$val) {
	        if (!in_array( strtoupper($k), self::$internalFields)) {
		    	$field = $k == "TB_UNID" ? "WIN_NUM" : $k;
				if ($val == "True") $val = true;
				if ($val == "False") $val = false;
				$wo->$field = $val;
	        }
	    }
		return $wo;
	}*/
}
