<?php
/**
 * class API_Abstract_User
 * @author Alex Scherba
 */
abstract class API_Abstract_User extends Zend_Db_Table_Abstract
{

    protected $authenticate = false;

    /**
     * function to load user
     * @param array $authData
     * @author Alex Scherba
     */
    abstract function load($authData);
    /**
     * function to seve user in db
     * @author Alex Scherba
     */
    abstract function save();
    /**
     * function to remove user from db
     * @author Alex Scherba
     */
    abstract function remove();
    /**
     * Check up user information
     * @param array $authData
     * @author Alex Scherba
     */
    abstract function checkAuthentication($authData);
    /**
	 * Check up user authentication
	 * @return bool
	 */
	public function isAuthenticate()
	{
		return $this->authenticate;
	}
    /**
	 * Check up user protocol
	 * @return bool
	 */
	protected function checkProtocol()
	{
		return true;

		if (HTTPS_PROTOCOL_REQUIRED) {
			$cur_protocol = $_SERVER['SERVER_PROTOCOL'];
			$cur_protocol = explode('/',$cur_protocol);
			if ($cur_protocol[0] == 'HTTPS') {
				$errors = Core_Api_Error::getInstance();
				$errors->addError(4, 'Not HTTPS protocol used');
				return false;
			} else {
				return true;
			}
		}
		return false;
	}
}