<?php
/**
 * class API_Abstract_ClientWorkOrderFilter
 * @package API_Abstract_ClientWorkOrderFilter
 */
class API_Abstract_ClientWorkOrderFilter extends API_Abstract_WorkOrderFilter {
	/** 
	 * 
	 * @var int	 */
	var $Sourced;

	/** 
	 * 
	 * @var API_UpdateTimeStampFilter	 */
	var $UpdateTS;
}