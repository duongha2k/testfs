<?php
/**
 * class API_Abstract_TechFilter
 * @package API_Abstract_WorkOrderFilter
 * @author Alexander Cheshchevik
 */
class API_Abstract_TechFilter {
    
    
    static $SelfRatingValues = array(
        "electronicsSelfRating",
        "AdjustableBedRepairSelfRating",
        "AlarmSystemsSelfRating",  //13978
        "atmSelfRating",
        "cablingSelfRating",
        "CentralOfficeCablingSelfRating",
        "CCTVSelfRating",
        "constructionSelfRating",
        "desktopSelfRating",
        "DigitalSignageSelfRating",
        "DslSelfRating",
        "electricalSelfRating",
        "FiberCablingSelfRating",
        "FlatPanelTVSelfRating",
        "GeneralWiringSelfRating",
        "networkingSelfRating",
        "KioskSelfRating",
        "LowVoltageSelfRating",
        "posSelfRating",
        
        "PrintersSelfRating",
        "RFIDSelfRating",
        "routersSelfRating",
        "SatelliteSelfRating",
        "ServersSelfRating",
        "ServerSoftwareSelfRating",
        "SiteSurveySelfRating",
        "telephonySelfRating",
        "TelephonyVoIPSelfRating",
        "WirelessSelfRating",
        
        "FSExperience",
        "ResidentialSelfRating",
        "CommercialSelfRating",
        "GovernmentSelfRating"
    );
    
 	/** 
	 * 
	 * @var int	 */
	public $TechID;

	/**
	 *
	 * @var int	 */
	public $FLSID;

	/** 
	 * 
	 * @var string	 */
	public $Firstname;

	/** 
	 * 
	 * @var string	 */
	public $Lastname;

	/** 
	 * 
	 * @var string	 */
    public $PrimaryPhone;

    /**
     * SecondaryPhone  
     * 
     * @var string
     * @access public
     */
    public $SecondaryPhone;

	/** 
	 * 
	 * @var string	 */
	public $PrimaryEmail;

    /**
     * SecondaryEmail
     * 
     * @var string
     * @access public
     */
    public $SecondaryEmail;

    /**
     * Address1
     * 
     * @var string
     * @access public
     */
    public $Address1;
    /**
     * Address2 
     * 
     * @var string
     * @access public
     */
    public $Address2;
	
	/** 
	 * 
	 * @var string	 */
	public $City;

	/** 
	 * 
	 * @var string	 */
	public $State;

	/** 
	 * 
	 * @var string	 */
	public $Zipcode;

	/** 
	 * 
	 * @var float	 */
	public $Latitude;

	/** 
	 * 
	 * @var float	 */
	public $Longitude;
		
	/** 
	 * 
	 * @var boolean	 */
	public $W9;

	/** 
	 * 
	 * @var boolean	 */
	public $Bg_Test_Pass_Lite;

	/** 
	 * 
	 * @var string	 */
	public $Bg_Test_ResultsDate_Lite;

	/** 
	 * 
	 * @var string	 */
	public $Skills;

	/** 
	 * 
	 * @var string	 */
	public $Certifications;

	/** 
	 * 
	 * @var boolean	 */
	public $Cert_Hallmark_POS;

	/** 
	 * 
	 * @var boolean	 */
	public $NCR_Basic_Cert;

	/** 
	 * 
	 * @var boolean	 */
	public $Starbucks_Cert;

	/** 
	 * 
	 * @var boolean	 */
	public $FLSCSP_Rec;

	/** 
	 * 
	 * @var boolean	 */
	public $FS_Cert_Test_Pass;

	/** 
	 * 
	 * @var boolean	 */
	public $Cert_Maurices_POS;

	/** 
	 * 
	 * @var boolean	 */
	public $CORE_Cert;

	/** 
	 * 
	 * @var string	 */
	public $FLSstatus;

	/** 
	 * 
	 * @var boolean	 */
	public $HP_Cert;

	/** 
	 * 
	 * @var boolean	 */
	public $SamsungCert;

	/** 
	 * 
	 * @var boolean	 */
	public $DirectDeposit;

	/** 
	 * 
	 * @var boolean	 */
	public $isDenied;

	/** 
	 * 
	 * @var boolean	 */
	public $isPreferred;
	 
	/** 
	 * 
	 * @var string	 */
	public $TechIDs;
	
	
	public $NotPrivate;
	
	
	public $WithCoordinates;
	
	
	public $UserName;
	
	
	public $SelfRating;
    
    //13735
    public $FSExpertForCatID;
    //end 13735
    //13771
    public $DeniedByCompanyID;
    //end 13771
	
	public function toArray() 
	{
		$newFilters = array();
		foreach ($this as $key=>$val) {
			$newFilters[$key] = $val;
		}

		return $newFilters;
	}	
	
	
    
    static public function strtobool( $val )
    {
        if ( !is_string($val) )
            return $val;

        $val = strtolower(trim($val));

        if ( !empty($val) ) {
            switch ( $val ) {
                case 'true' :
                    return TRUE;
                case 'false':
                case '0' :
                    return FALSE;
                case 'null':
                    return NULL;
            }
        }
        if ( empty($val) )
            return FALSE;
        return TRUE;
    }
}
