<?php


/**
 * class API_Abstract_AdminWorkOrderFilter
 * @package API_Abstract_AdminWorkOrderFilter
 */
class API_Abstract_AdminWorkOrderFilter extends API_Abstract_WorkOrderFilter {
	/** 
	 * 
	 * @var int	 */
	var $Sourced;

	/** 
	 * 
	 * @var API_UpdateTimeStampFilter	 */
	var $UpdateTS;

	/** 
	 * 
	 * @var array	 */
	var $Tech_ID_List;
	
	/** 
	 * 
	 * @var array	 */
	var $wm_assignment_id;
	
	/** 
	 * 
	 * @var array	 */
	var $wm_tech_id;
	
	/** 
	 * 
	 * @var array	 */
	var $wm_tech_select;
}

