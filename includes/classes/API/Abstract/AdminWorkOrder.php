<?php
/**
 * class API_Abstract_AdminWorkOrder
 * @package API_Abstract_AdminWorkOrder
 */
abstract class API_Abstract_AdminWorkOrder extends API_Abstract_WorkOrder {

	public static function readOnlyFields() {
		return array();
	}

	/** 
	 * 
	 * @var string	 */
	var $ClientComments;


	/** 
	 * 
	 * @var boolean	 */
	var $ShowTechs;


	/** 
	 * 
	 * @var boolean	 */
	var $CheckedIn;


	/** 
	 * 
	 * @var string	 */
	var $Checkedby;


	/** 
	 * 
	 * @var string	 */
	var $CheckInNotes;

	/** 
	 * 
	 * @var boolean	 */
	var $StoreNotified;


	/** 
	 * 
	 * @var string	 */
	var $Notifiedby;


	/** 
	 * 
	 * @var string	 */
	var $DateNotified;


	/** 
	 * 
	 * @var string	 */
	var $NotifyNotes;


	/** 
	 * 
	 * @var int	 */
	var $Qty_Applicants;


	/** 
	 * 
	 * @var boolean	 */
	var $Invoiced;


	/** 
	 * 
	 * @var string	 */
	var $DateInvoiced;


	/** 
	 * 
	 * @var string	 */
	var $DateApproved;

	/** 
	 * 
	 * @var boolean	 */
	var $Deactivated;

	/** 
	 * 
	 * @var boolean	 */
	var $TechCalled;


	/** 
	 * 
	 * @var string	 */
	var $CalledBy;


	/** 
	 * 
	 * @var string	 */
	var $CallDate;

	/** 
	 * 
	 * @var int	 */
	var $NoShow_Tech;

	/** 
	 * 
	 * @var boolean	 */
	var $Update_Requested;


	/** 
	 * 
	 * @var string	 */
	var $Update_Reason;

	/** 
	 * 
	 * @var string	 */
	var $Add_Pay_AuthBy;

	/** 
	 * 
	 * @var boolean	 */
	var $Work_Out_of_Scope;

	/** 
	 * 
	 * @var string	 */
	var $OutOfScope_Reason;


	/** 
	 * 
	 * @var boolean	 */
	var $Site_Complete;


	/** 
	 * 
	 * @var string	 */
	var $Add_Pay_Reason;


	/** 
	 * 
	 * @var int	 */
	var $BackOut_Tech;

	/** 
	 * 
	 * @var string	 */
	var $lastModBy;


	/** 
	 * 
	 * @var string	 */
	var $lastModDate;


	/** 
	 * 
	 * @var string	 */
	var $approvedBy;


	/** 
	 * 
	 * @var string	 */
	var $Date_Completed;


	/** 
	 * 
	 * @var string	 */
	var $Deactivated_Reason;

	/** 
	 * 
	 * @var string	 */
	var $DeactivatedDate;


	/** 
	 * 
	 * @var string	 */
	var $DeactivatedBy;

	/** 
	 * 
	 * @var float	 */
	var $Total_Estimated_WO_Value;

	/** 
	 * 
	 * @var boolean	 */
	var $AutoBlastOnPublish;


	/** 
	 * 
	 * @var string	 */
	var $RecruitmentFromEmail;


	/** 
	 * 
	 * @var float	 */
	var $calculatedTechHrs;


	/** 
	 * 
	 * @var float	 */
	var $baseTechPay;

	/** 
	 * 
	 * @var string	 */
	var $TechnicianFeeComments;


	/** 
	 * 
	 * @var boolean	 */
	var $ExpediteFee;


	/** 
	 * 
	 * @var boolean	 */
	var $AbortFee;


	/** 
	 * 
	 * @var boolean	 */
	var $AfterHoursFee;


	/** 
	 * 
	 * @var float	 */
	var $OtherFSFee;

	/** 
	 * 
	 * @var int	 */
	var $WO_Category_ID;

	/** 
	 * 
	 * @var boolean	 */
	var $ReminderAcceptance;


	/** 
	 * 
	 * @var boolean	 */
	var $Reminder24Hr;


	/** 
	 * 
	 * @var boolean	 */
	var $Reminder1Hr;


	/** 
	 * 
	 * @var boolean	 */
	var $CheckInCall;


	/** 
	 * 
	 * @var boolean	 */
	var $ReminderNotMarkComplete;


	/** 
	 * 
	 * @var boolean	 */
	var $ReminderIncomplete;


	/** 
	 * 
	 * @var string	 */
	var $DateIncomplete;

	/** 
	 * 
	 * @var boolean	 */
	var $isProjectAutoAssign;


	/** 
	 * 
	 * @var boolean	 */
	var $isWorkOrdersFirstBidder;


	/** 
	 * 
	 * @var int	 */
	var $MinutesRemainPublished;


	/** 
	 * 
	 * @var int	 */
	var $MinimumSelfRating;


	/** 
	 * 
	 * @var int	 */
	var $MaximumAllowableDistance;


	/** 
	 * 
	 * @var boolean	 */
	var $ReminderAll;


	/** 
	 * 
	 * @var boolean	 */
	var $CheckOutCall;


	/** 
	 * 
	 * @var boolean	 */
	var $ACSInvoiced;


	/** 
	 * 
	 * @var string	 */
	var $ACSDateInvoiced;


	/** 
	 * 
	 * @var boolean	 */
	var $SMSBlast;


	/** 
	 * 
	 * @var boolean	 */
	var $P2TPreferredOnly;


	/** 
	 * 
	 * @var boolean	 */
	var $P2TAlreadyAssigned;

	/** 
	 * 
	 * @var boolean	 */
	var $CallClosed;


	/** 
	 * 
	 * @var boolean	 */
	var $ContactHD;


	/** 
	 * 
	 * @var string	 */
	var $HDPOC;

	/** 
	 * 
	 * @var string	 */
	var $Date_Checked_in;

	/** 
	 * 
	 * @var boolean	 */
	var $Unexpected_Steps;


	/** 
	 * 
	 * @var boolean	 */
	var $Incomplete_Paperwork;


	/** 
	 * 
	 * @var boolean	 */
	var $Paperwork_Received;


	/** 
	 * 
	 * @var string	 */
    var $DeactivationCode;

    /**
     * ReturnPartCount  
     * 
     * @var int
     * @access public
     */
    var $ReturnPartCount;

    /**
     * NewPartCount  
     * 
     * @var int
     * @access public
     */
    var $NewPartCount;

    /**
     * PartsProcessed
     * 
     * @var int
     * @access public
     */
    var $PartsProcessed;
    
    
    /**
     * Company_Name
     * 
     * @var string
     * @access public
     */
	var $Company_Name;
	
    /**
     * Company_ID
     * 
     * @var string
     * @access public
     */
	var $Company_ID;
	
	
	/**
     * FS_InternalComments
     * 
     * @var string
     * @access public
     */
	var $FS_InternalComments;
	
	
	/**
	 * Pricing Rule ID
	 * @var string
	 * @access public
	 */
	var $PricingRuleID;
	
	
	
	/**
	 * Pricing Rule Applied
	 * @var string
	 * @access public
	 */
	var $PricingRuleApplied;
	
	/**
	 * Pricing Calculation Text
	 * @var string
	 * @access public
	 */
	var $PricingCalculationText;
    
	
	/**
	 * PricindRan
	 * @var string
	 * @access public
	 */
	var $PricingRan;
	
	
	/**
	 * Calculated Invoice
	 * @var string
	 * @access public
	 */
	var $calculatedInvoice;
	
	/**
	 * Net_Pay_Amount
	 * @var string
	 * @access public
	 */
	var $Net_Pay_Amount;

	/**
	 * Z2T_Pay_Amount
	 * @var string
	 * @access public
	 */
	var $Z2T_Pay_Amount;
}
