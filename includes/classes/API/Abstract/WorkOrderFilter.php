<?php
/**
 * class API_Abstract_WorkOrderFilter
 * @package API_Abstract_WorkOrderFilter
 */
class API_Abstract_WorkOrderFilter {
    /**
     * TB_UNID 
     * 
     * @var int
     * @access public
     */
    var $TB_UNID;
    /**
     * Tech 
     * 
     * @var string
     * Example:
     *      integer Tech ID
     *      "FirstName"
     *      "Lastname"
     *      "Firstname Lastname"
     * @access public
     */
    var $Tech;
    /**
     * Distance
     * 
     * TODO This Field work for Tech's getWorkOrders method ONLY!!! 
     * TODO How it work see block of Distance code in getWorkOrders method
     *
     * @var string  proximityCriteria. Ex: '<= 50'
     * @access public
     */
    var $Distance;
    /**
     * Company_ID
     * 
     * @var string
     * @access public
     */
    var $Company_ID;
    /**
     * Call_Type 
     * 
     * @var integer
     * @access public
     */
    var $Call_Type;
    /**
     * Parts_Presents  
     * 
     * @var boolean
     * @access public
     */
    var $Parts_Presents = false;
    /**
     * Docs_Presents  
     * 
     * @var boolean
     * @access public
     */
    var $Docs_Presents = false;
    /**
     * StoreNotified
     * 
     * @var boolean
     * @access public
     */
    var $StoreNotified;
    /**
     * Paperwork_Received
     * 
     * @var boolean
     * @access public
     */
    var $Paperwork_Received;
    /**
     * Lead
     * 
     * @var boolean
     * @access public
     */
    var $Lead;
    /**
     * Assist
     * 
     * @var boolean
     * @access public
     */
    var $Assist;
	/** 
	 * 
	 * @var int	 */
	var $Project_ID;
	/** 
	 * 
	 * @var string	 */
	var $StartDateFrom;
    /**
     * FLS_ID
     * 
     * @var int
     * @access public
     */
    var $FLS_ID;
    /**
     * Short_Notice
     * 
     * @var string
     * @access public
     */
    var $Short_Notice;
	/** 
	 * 
	 * @var string	 */
	var $StartDateTo;
		
	/** 
	 * 
	 * @var string	 */
	var $EndDateFrom;
	
	/** 
	 * 
	 * @var string	 */
	var $EndDateTo;

        /**
	 *
	 * @var string	 */
	var $DateEnteredFrom;

	/**
	 *
	 * @var string	 */
	var $DateEnteredTo;
	/**
	 *
	 * @var string	 */
	var $DatePaidFrom;
        /**
	 *
	 * @var string	 */
	var $DatePaidTo;
	/**
	 *
	 * @var string	 */
	var $DateApprovedTo;
        /**
	 *
	 * @var string	 */
	var $DateApprovedFrom;
        /**
	 *
	 * @var string	 */
	var $DateInvoicedTo;
        /**
	 *
	 * @var string	 */
	var $DateInvoicedFrom;

	
	var $Invoiced;
	
	/** 
	 * 
	 * @var string	 */
	var $WO_ID;
        
	/** 
	 * 
	 * @var string	 */
	var $PO;
        
	/** 
	 * 
	 * @var boolean	 */
	var $ShowTechs;
	
	/** 
	 * 
	 * @var string	 */
	var $Region;
	
	/** 
	 * 
	 * @var string	 */
	var $Route;
	
	/** 
	 * 
	 * @var string	 */
	var $SiteName;
        
	/** 
	 * 
	 * @var string	 */
	var $SiteNumber;
	
	/** 
	 * 
	 * @var string	 */
	var $City;
	        
	/** 
	 * 
	 * @var string	 */
	var $State;
                
	/** 
	 * 
	 * @var string	 */
	var $Zipcode;
	        
	/** 
	 * 
	 * @var boolean	 */
	var $Update_Requested;
	
	/** 
	 * 
	 * @var boolean	 */
	var $CheckedIn;
        
	/** 
	 * 
	 * @var boolean	 */
	var $WorkOrderReviewed;
        
	/** 
	 * 
	 * @var boolean	 */
	var $TechCheckedIn_24hrs;
        
	/** 
	 * 
	 * @var boolean	 */
	var $TechMarkedComplete;
        
	/** 
	 * 
	 * @var boolean	 */
	var $Site_Complete;
	
	/** 
	 * 
	 * @var boolean	 */
	var $Approved;
	
	/** 
	 * 
	 * @var boolean	 */
	var $Additional_Pay_Amount;
                
	/** 
	 * 
	 * @var boolean	 */
	var $Work_Out_of_Scope;
        
	/** 
	 * 
	 * @var boolean	 */
	var $TechPaid;

	/** 
	 * 
	 * @var string	 */
	var $Tech_FName;
        
	/** 
	 * 
	 * @var string	 */
	var $Tech_LName;
        
	/** 
	 * 
	 * @var int	 */
	var $Tech_ID;
        
	/** 
	 * 
	 * @var string	 */
	var $Contractor_ID;
        
	/** 
	 * 
	 * @var boolean	 */
	var $Deactivated;	
		
	/** 
	 * 
	 * @var string	 */
	var $WO_State;
        /**
	 *
	 * @var string	 */
	var $Username;
         /**
	 *
	 * @var string	 */
	var $PayMaxFrom;
         /**
	 *
	 * @var string	 */
	var $PayMaxTo;
        /**
	 *
	 * @var string	 */
	var $Net_Pay_AmountFrom;
         /**
	 *
	 * @var string	 */
	var $Net_Pay_AmountTo;
    /**
	 *
	 * @var string	 */
	var $TechEmail;
    /**
	 *
	 * @var string	 */
	var $Headline;
    /**
	 *
	 * @var string	 */
	var $WO_Category;
    /**
	 *
	 * @var float	 */
	var $Latitude;
    /**
	 *
	 * @var float	 */
	var $Longitude;
    /**
	 *
	 * @var string	 */
	var $TBPay;
	
	
	var $TechCalled;
	
	
	var $PricingRan;
	
	
	var $withCoordinates;
	
	
	var $PcntDeduct;
	
	
	var $Deactivated_Reason;
	
	var $DeactivationCode;
	
	var $TechEmailContain;
	
	var $SATRecommended_From;
	
	var $SATPerformance_From;
    var $isProjectAutoAssign;
var $Amount_Per;

	var $availableOrAssignedToTech;

	var $Country;
	
	var $hideAvaiableIfClientDeniedForTech;
	var $biddedOnByTech;
	var $notBiddedOnByTech;
	
	var $QBID;
	var $QBIDIsNotEmpty;
		
	var $deactivatedTechLimitedAccess;
	var $Owner;
    var $StandardOffset;//13892
    var $TimeZone;//13892
	
	var $WO_ID_Mode; // 0 = like (default), 1 = exact
	
	public function toArray() {
		$newFilters = array();
		foreach ($this as $key=>$val) {
			if ($key == "UpdateTS" && $val !== NULL) {
				$key = $val->matchAll === TRUE ? "UpdateTS_AND" : "UpdateTS_OR";
				$fieldList = array();
				foreach ($val->fields as $field) {
					if (!empty($field->value))
						$fieldList[] = $field->value;
				}
				$val = implode(",", $fieldList);
			}
			$newFilters[$key] = $val;
		}

		if (array_key_exists("WO_State", $newFilters)) {
			$newFilters["WO_State"] = strtolower($newFilters["WO_State"]);
/*			switch ($newFilters["WO_State"]) {
				case WO_STATE_CREATED:
				case WO_STATE_PUBLISHED:
				case WO_STATE_ASSIGNED:
				case WO_STATE_WORK_DONE:
				case WO_STATE_INCOMPLETE:
				case WO_STATE_APPROVED:
				case WO_STATE_IN_ACCOUNTING:
				case WO_STATE_COMPLETED:
				case WO_STATE_ALL_ACTIVE:
				case WO_STATE_DEACTIVATED:
					break;
				default:
					$newFilters["WO_State"] = WO_STATE_ALL;
			}*/
		}
		return $newFilters;
	}	
    static public function strtobool( $val )
    {
        if ( !is_string($val) )
            return $val;

        $val = strtolower(trim($val));

        if ( !empty($val) ) {
            switch ( $val ) {
                case 'true' :
                    return TRUE;
                case 'false':
                case '0' :
                    return FALSE;
                case 'null':
                    return NULL;
            }
        }
        if ( empty($val) )
            return FALSE;
        return TRUE;
    }

	protected static function dateFormat($format, $date) {
		$t = strtotime($date);
		return $t ? date($format, $t) : false;
	}
}
