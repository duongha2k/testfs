<?php
/**
 * class API_Abstract_ClientWorkOrder
 * @package API_Abstract_ClientWorkOrder
 */
abstract class API_Abstract_ClientWorkOrder extends API_Abstract_WorkOrder {

    /**
     * uploadFileFields
     *
     * Return fields' names for file upload
     *
     * @static
     * @access public
     * @return array
     */
    public static function uploadFileFields()
    {
        return array();
    }

	public static function readOnlyFields() {
        //13892
        return array('USERNAME','PASSWORD',
                                    'PAID','PAYAPPROVE','CHECK_NUMBER','MOVEDTOMYSQL','ATTACHEDFILE',
    	                            'USEMYRECRUITEMAILSETTINGS','PRICINGRULEID','CALCULATEDINVOICE','PRICINGCALCULATIONTEXT',
    	                            'TECHNICIANFEE','PRICINGRULEAPPLIED','URGENT','FSSOURCINGSUPPORT','PRICINGRAN',
    	                            'REMINDERUSEWOSETTING','COMPANY_NAME','COMPANY_ID','CALCULATEDTOTALTECHPAY','PAYAMOUNT_FINAL',
    	                            'PRICINGACCUMULATOR', 'PRICINGACCUMULATOR2',
									'TB_UNID', 
									'WIN_NUM', 
									'DATEENTERED', 
									'TECH_FNAME', 
									'TBPAY', 
									'TECHCOMMENTS', 
									'TECH_LNAME', 
									'TECHPAID', 
									'DATEPAID', 
									'PAYAMOUNT', 
									'QTY_APPLICANTS', 
									'INVOICED', 
									'DATEINVOICED', 
									'DATEAPPROVED', 
									'DATE_IN', 
									'DATE_OUT', 
									'TIME_IN', 
									'TIME_OUT', 
									'TECHEMAIL', 
									'TECHPHONE', 
									'LASTMODBY', 
									'LASTMODDATE', 
									'DATE_COMPLETED', 
									'LATITUDE', 
									'LONGITUDE', 
									'AUTOBLASTONPUBLISH', 
									'RECRUITMENTFROMEMAIL',
									'TECHMILES', 
									'TECHPHONEEXT', 
									'REMINDERACCEPTANCES', 
									'REMINDER24HR', 
									'REMINDER1HR', 
									'REMINDERCHECKINOUT', 
									'REMINDERMARKNOTCOMPLETE', 
									'REMINDERINCOMPLETE', 
									'ISPROJECTAUTOASSIGN', 
									'ISWORKORDERFIRSTBIDDER', 
									//'MINIMUMSELFRATING', 
									//'MAXIMUMALLOWABLEDISTANCE', 
									'REMINDERALL',
									'NEWPARTCOUNT',
                                    'RETURNPARTCOUNT',
									'STATUS',
        							'UPDATEDPARTCOUNT',
                                    'AUDITNEEDED',
                                    'AUDITNEEDEDBY',
                                    'AUDITNEEDEDDATE',
                                    'AUDITCOMPLETE',
                                    'AUDITCOMPLETEBY',
                                    'AUDITCOMPLETEDATE',
                                    'ENTITYTYPEID',
                                    'SERVICETYPEID',
                                    'FSCLIENTSERVICEDIRECTORID',
                                    'FSACCOUNTMANAGERID',
                                    'NUMOFVISITS',
									'CONTRACTOR_ID',
                                    'ALLOWDEVRYINTERNSTOOBSERVEWORK',
                                    'ALLOWDEVRYINTERNSTOPERFORMFIELD',
                                    'ACSNOTIFIPOC',
                                    'ACSNOTIFIPOC_PHONE',
                                    'ACSNOTIFIPOC_EMAIL',
                                    'SITECONTACTBACKEDOUTCHECKED',
                                    'NOTIFIEDTIME',
                                    'STANDARDOFFSET',
                                    'TIMEZONE'
                                    );	
	}

	/** 
	 * 
	 * @var string	 */
	var $FS_InternalComments;

	/** 
	 * 
	 * @var string	 */
	var $ClientComments;


	/** 
	 * 
	 * @var boolean	 */
	var $ShowTechs;


	/** 
	 * 
	 * @var boolean	 */
	var $CheckedIn;


	/** 
	 * 
	 * @var string	 */
	var $Checkedby;


	/** 
	 * 
	 * @var string	 */
	var $CheckInNotes;

	/** 
	 * 
	 * @var boolean	 */
	var $StoreNotified;


	/** 
	 * 
	 * @var string	 */
	var $Notifiedby;


	/** 
	 * 
	 * @var string	 */
	var $DateNotified;


	/** 
	 * 
	 * @var string	 */
	var $NotifyNotes;


	/** 
	 * 
	 * @var int	 */
	var $Qty_Applicants;


	/** 
	 * 
	 * @var boolean	 */
	var $Invoiced;


	/** 
	 * 
	 * @var string	 */
	var $DateInvoiced;


	/** 
	 * 
	 * @var string	 */
	var $DateApproved;

	/** 
	 * 
	 * @var boolean	 */
	var $Deactivated;

	/** 
	 * 
	 * @var boolean	 */
	var $TechCalled;


	/** 
	 * 
	 * @var string	 */
	var $CalledBy;


	/** 
	 * 
	 * @var string	 */
	var $CallDate;

	/** 
	 * 
	 * @var int	 */
	var $NoShow_Tech;

	/** 
	 * 
	 * @var boolean	 */
	var $Update_Requested;


	/** 
	 * 
	 * @var string	 */
	var $Update_Reason;

	/** 
	 * 
	 * @var string	 */
	var $Add_Pay_AuthBy;

	/** 
	 * 
	 * @var boolean	 */
	var $Work_Out_of_Scope;

	/** 
	 * 
	 * @var string	 */
	var $OutOfScope_Reason;


	/** 
	 * 
	 * @var boolean	 */
	var $Site_Complete;


	/** 
	 * 
	 * @var string	 */
	var $Add_Pay_Reason;


	/** 
	 * 
	 * @var int	 */
	var $BackOut_Tech;

	/** 
	 * 
	 * @var string	 */
	var $lastModBy;


	/** 
	 * 
	 * @var string	 */
	var $lastModDate;


	/** 
	 * 
	 * @var string	 */
	var $approvedBy;


	/** 
	 * 
	 * @var string	 */
	var $Date_Completed;


	/** 
	 * 
	 * @var string	 */
	var $Deactivated_Reason;

	/** 
	 * 
	 * @var string	 */
	var $DeactivatedDate;


	/** 
	 * 
	 * @var string	 */
	var $DeactivatedBy;

	/** 
	 * 
	 * @var float	 */
	var $Total_Estimated_WO_Value;

	/** 
	 * 
	 * @var boolean	 */
	var $AutoBlastOnPublish;


	/** 
	 * 
	 * @var string	 */
	var $RecruitmentFromEmail;


	/** 
	 * 
	 * @var float	 */
	var $calculatedTechHrs;


	/** 
	 * 
	 * @var float	 */
	var $baseTechPay;

	/** 
	 * 
	 * @var string	 */
	var $TechnicianFeeComments;


	/** 
	 * 
	 * @var boolean	 */
	var $ExpediteFee;


	/** 
	 * 
	 * @var boolean	 */
	var $AbortFee;


	/** 
	 * 
	 * @var boolean	 */
	var $AfterHoursFee;


	/** 
	 * 
	 * @var float	 */
	var $OtherFSFee;

	/** 
	 * 
	 * @var int	 */
	var $WO_Category_ID;

	/** 
	 * 
	 * @var boolean	 */
	var $ReminderAcceptance;


	/** 
	 * 
	 * @var boolean	 */
	var $Reminder24Hr;


	/** 
	 * 
	 * @var boolean	 */
	var $Reminder1Hr;


	/** 
	 * 
	 * @var boolean	 */
	var $CheckInCall;


	/** 
	 * 
	 * @var boolean	 */
	var $ReminderNotMarkComplete;


	/** 
	 * 
	 * @var boolean	 */
	var $ReminderIncomplete;


	/** 
	 * 
	 * @var string	 */
	var $DateIncomplete;

	/** 
	 * 
	 * @var boolean	 */
	var $isProjectAutoAssign;


	/** 
	 * 
	 * @var boolean	 */
	var $isWorkOrdersFirstBidder;


	/** 
	 * 
	 * @var int	 */
	var $MinutesRemainPublished;


	/** 
	 * 
	 * @var int	 */
	var $MinimumSelfRating;


	/** 
	 * 
	 * @var int	 */
	var $MaximumAllowableDistance;


	/** 
	 * 
	 * @var boolean	 */
	var $ReminderAll;


	/** 
	 * 
	 * @var boolean	 */
	var $CheckOutCall;


	/** 
	 * 
	 * @var boolean	 */
	var $ACSInvoiced;


	/** 
	 * 
	 * @var string	 */
	var $ACSDateInvoiced;


	/** 
	 * 
	 * @var boolean	 */
	var $SMSBlast;


	/** 
	 * 
	 * @var boolean	 */
	var $P2TPreferredOnly;


	/** 
	 * 
	 * @var boolean	 */
	var $P2TAlreadyAssigned;

	/** 
	 * 
	 * @var boolean	 */
	var $CallClosed;


	/** 
	 * 
	 * @var boolean	 */
	var $ContactHD;


	/** 
	 * 
	 * @var string	 */
	var $HDPOC;

	/** 
	 * 
	 * @var string	 */
	var $Date_Checked_in;

	/** 
	 * 
	 * @var boolean	 */
	var $Unexpected_Steps;


	/** 
	 * 
	 * @var boolean	 */
	var $Incomplete_Paperwork;


	/** 
	 * 
	 * @var boolean	 */
	var $Paperwork_Received;


	/** 
	 * 
	 * @var string	 */
    var $DeactivationCode;

    /**
     * ReturnPartCount  
     * 
     * @var int
     * @access public
     */
    var $ReturnPartCount;

    /**
     * NewPartCount  
     * 
     * @var int
     * @access public
     */
    var $NewPartCount;

    /**
     * PartsProcessed
     * 
     * @var int
     * @access public
     */
    var $PartsProcessed;

    /**
     * SATPerformance
     *
     * @var int
     * @access public
     */
    var $SATPerformance;

    /**
     * SATRecommended
     *
     * @var int
     * @access public
     */
    var $SATRecommended;
    /**
     * SATRecommended
     *
     * @var string
     * @access public
     */
    var $bid_comments;

	var $__Visits;
	
	protected $calculatedInvoice;
    
    
	/**
	 * download project file from S3
	 *
	 * @param string $user
	 * @param string $pass
	 * @param string $fileName
	 * @return API_Response
     * @author Alex Che
	 */
    static public function getFile($fileName)
    {
        $errors = Core_Api_Error::getInstance();
        // get file from S3
        $file = new Core_File();
        $file = $file->downloadFile($fileName, S3_CLIENTS_DOCS_DIR);
        
        if ($file === false) $errors->addError(6, 'File not exists');

		$err = $errors->getErrors();
		if (!empty($err))
			return API_Response::fail();

        return $file;
    }
    /**
     * 
     */
   static public function getCountPartEmptries($win)
   {
        $db = Core_Database::getInstance();
        $select = $db->select()
                ->from(array('w' => Core_Database::TABLE_PART_ENTRIES))
                ->where('`w`.`WinNum` = ?', array($win));
        $parts = Core_Database::fetchAll($select);
        return count($parts);
}
   static public function checkPartEmptriesTechUpdated($win,$TechID)
   {
   		//If parts are required but there are no parts associated to work
   		//order, function must return false.
        $db = Core_Database::getInstance();
        $select = $db->select()
                ->from(array('w' => Core_Database::TABLE_PART_ENTRIES))
                ->where('`w`.`WinNum` = ?', $win)
                ->where('`w`.`TechIdLastUpd` = ?', $TechID)
                ->where('`w`.`Deleted` != ?', 1)
                ->where('`w`.`Consumable` != ?', 1);
        $parts = Core_Database::fetchAll($select);
        return !(count($parts) > 0);
   }
      
	  
	public function __set($name, $value) {
		// ignore setting of non public field from outside class
		if ($name == 'Expensethrough')
			$this->Expensethrough = $value;		
	}
	
	public function __get($name) {
		if (property_exists($this, $name)){
			if ($name == "calculatedInvoice" && !$this->canShowCalculatedInvoice())
				return NULL;
			return $this->$name;
		} else {
//            throw new Exception('Property not exists');
		}
	}
	
	public function fieldExists($field) {
		return parent::fieldExists($field) || $field == "calculatedInvoice";
	}	
}
