<?php
class Salesforce_EmailToCase {
	private $mail;
	public function __construct() {
		$this->mail = new Zend_Mail_Storage_Imap(array(
			'host' => 'imap.emailsrvr.com',
			'user' => 'xeroxrequest@fieldsolutions.com',
			'password' => '$alesForce2011',
			'ssl' => 'TLS')
		);
	}
	
	public function process() {
		require_once(dirname(__FILE__) . "/soapclient/SforcePartnerClient.php");
		$wsdl = dirname(__FILE__) . "/../../controllers/wsdl/salesforce/fieldsolutions/partner.wsdl.xml";
		$password = 'Tr1n1ty!';
		$token = 'FsqSmv0whOmuap807JzHzbJ6';
		$api = new SforcePartnerClient();
		$api->createConnection($wsdl);
		$authResult = $api->login('alin@fieldsolutions.com', $password . $token);
		$api->setSessionHeader($authResult->sessionId);
		
		//$r = $api->query("SELECT Id FROM Case WHERE Id = '5003000000FwYIi'");
		//$r = $api->query("SELECT Id FROM Account WHERE Name = 'Xerox'");
		//$r = $api->query("SELECT Id FROM Contact WHERE FirstName = 'A0R5011001702' AND LastName = 'X00ALHO1'");
		// 0013000000rJ8VK AA0 -- account
		// 0033000000zv0v8 AAA -- contact
		
/*		$r = $api->query("SELECT Id FROM Account WHERE Name = 'Xerox'");
		$accountId = $r->records[0]->Id;*/

		$defaultContact = null;
		$count = $this->mail->countMessages();
		
		$cache = Core_Cache_Manager::factory();
		$cacheKey = Core_Cache_Manager::makeKey('emailToCaseLastRead');

		if ( false === ($mId = $cache->load($cacheKey)) ) {
			$cache->save(2278, $cacheKey, array('emailToCaseLastRead'), LIFETIME_30DAYS);
			$mId = 2278;
		}
//		$mId = 2278; // fix to default to last known read email
//		$count = 2278;
		for ($mId = $mId; $mId <= $count; ++$mId) {
			$message = $this->mail->getMessage($mId);
//			echo "Mail from '{$message->from}': {$message->subject} {$message->contentType}\n";
			if ($message->hasFlag(Zend_Mail_Storage::FLAG_SEEN)) continue;
//			echo $message->getContent();
			$this->mail->setFlags($mId, array(Zend_Mail_Storage::FLAG_SEEN));
			$case = $this->parseEmail($message);
			if (isset($case['Outcome'])) {
				// notification for Xerox XPS
				if (strpos($case['Outcome'], 'Completed') !== FALSE)
					$response = "Yes\n";
				else
					$response = "No\n";
				$response .= date('m/d/Y h:i A') . "\n";
				$response .= $case['Outcome'];
				$db = Core_Database::getInstance();
				$select = $db->select();
				$select->from('emailToCase_closure', array('incident', 'reply_to', 'subject'));
				$select->where('incident = ?', $case['XSM']);
				$select->limit(1);
				$result = $db->fetchAll($select);
				if (empty($result[0])) continue;
				else $result = $result[0];
				$notifySender = new Core_Mail();
				$notifySender->setFromName("FieldSolutions");
				$notifySender->setFromEmail("nobody@fieldsolutions.com");
				$notifySender->setToEmail("tngo@fieldsolutions.com");
				$notifySender->setSubject("RE: " . $result['subject']);
				$notifySender->setBodyText($response);
				$notifySender->send();
				
				$notifySender = new Core_Mail();
				$notifySender->setFromName("Albert Lin");
				$notifySender->setFromEmail("alin@fieldsolutions.com");
				$notifySender->setToEmail($result['reply_to']);
				$notifySender->setSubject("RE: " . $result['subject']);
				$notifySender->setBodyText($response);
				$notifySender->send();
				continue;
			}
			if (strpos($message->subject, 'Closure') !== FALSE) {
				$db = Core_Database::getInstance();
				$db->insert('emailToCase_closure', array(
					'incident' => $case['XSM_Number__c'],
					'reply_to' => $message->from,
					'subject' => $message->subject
				));
				continue;
			}
			
			$accountId = $case['AccountId'];
			$r = $api->query("SELECT Id FROM Contact WHERE FirstName = '" . $case['_SerialNumber'] . "' AND LastName = '" . $case['_AssetNumber'] . "' AND AccountId = '$accountId'");
//			echo "SELECT Id FROM Contact WHERE FirstName = '" . $case['_SerialNumber'] . "' AND LastName = '" . $case['_AssetNumber'] . "'\n\n";
            $cache->save($mId, $cacheKey, array('emailToCaseLastRead'), LIFETIME_30DAYS);
			if (empty($r->records[0]->Id)) {
				// do 2nd lookup for duke energy
				$r = $api->query("SELECT Id FROM Contact WHERE FirstName = '" . $case['_SerialNumber'] . "' AND LastName = '00000000' AND AccountId = '$accountId'");
			}
			if (empty($r->records[0]->Id)) {
//				echo "DEFAULT CONTACT\n";
				if (empty($defaultContact)) {
					$defaultContact = $api->query("SELECT Id FROM Contact WHERE FirstName = 'Not Supported' AND LastName = 'Device' AND AccountId = '$accountId'");
					$defaultContact = $defaultContact->records[0]->Id;
				}
				$r->records[0]->Id = $defaultContact;
//				continue;
			}
			$contactId = $r->records[0]->Id;
//			$uid = $maill->getNumberByUniqueId($mail->getUniqueId($mid));

			$r = $api->query("SELECT Manufacturer_Name__c, Model_Name__c, Model_Class__c FROM Entitlement__c WHERE Unit_Serial_Number__c = '" . $case['_SerialNumber'] . "' AND Asset_Tag__c = '" . $case['_AssetNumber'] . "' AND Customer_Name__c = '$contactId'");
//			$r = $api->query("SELECT Manufacturer_Name__c, Model_Class__c FROM Entitlement__c WHERE Unit_Serial_Number__c = 'A0R6011001283' AND Asset_Tag__c = 'X00B098K'");
			if (!empty($r->records[0])) {
				$manufacturerName = $r->records[0]->fields->Manufacturer_Name__c;
				$modelName = $r->records[0]->fields->Model_Name__c;
				$modelClass = $r->records[0]->fields->Model_Class__c;
			}			

			$sObject = new SObject();
			$sfCase = array (
				'AccountId' => $accountId,
				'Origin' => 'Email',
				'Status' => 'New',
				'ContactId' => $contactId,
				'XSM_Number__c' => $case['XSM_Number__c'],
				'Problem_issue_or_question__c' => substr($case['Problem_issue_or_question__c'], 0, 254),
				'Model_Name__c' => $modelName,
				'SuppliedName' => $case['SuppliedName'],
				'SuppliedEmail' => $case['SuppliedEmail'],
				'SuppliedPhone' => $case['SuppliedPhone'],
				'Manufacturer_Name__c' => $manufacturerName,
				'Model_Class__c' => $modelClass,
				'Service_Description__c' => $case['Service_Description__c']
			);
			
			// Salesforce Account Id
			// 001a000001Luvtr - Xerox XPS
			// 0013000000rJ8VK - Xerox
			if ($accountId == '001a000001Luvtr') {
//				$sfCase['Contract_Number__c'] = $case['Contract_Number__c'];
				$sfCase['Client_Account_Number__c'] = $case['Client_Account_Number__c'];
//				$sfCase['Budget_Center__c'] = $case['Budget_Center__c'];
			}

			if ($contactId === $defaultContact) {
				$sfCase['Details_of_any_triage_already_performed__c'] = $case['_SerialNumber'] . " " . $case['_AssetNumber'];
			}
//			print_r($sfCase);
			foreach ($sfCase as $idx => $val) {
				$sfCase[$idx] = htmlspecialchars($val, ENT_NOQUOTES);
			}
			$sObject->fields = $sfCase;
			$sObject->type = 'Case';
			$api->create(array($sObject));
			
			if ($accountId == '001a000001Luvtr') {
				// notification for Xerox XPS
				$notifySender = new Core_Mail();
				$notifySender->setFromName("FieldSolutions");
				$notifySender->setFromEmail("nobody@fieldsolutions.com");
				$notifySender->setToEmail("tngo@fieldsolutions.com");
				$notifySender->setSubject("RE: " . $message->subject);
				$notifySender->setBodyText("Yes");
				$notifySender->send();
				
				$notifySender = new Core_Mail();
				$notifySender->setFromName("Albert Lin");
				$notifySender->setFromEmail("alin@fieldsolutions.com");
				$notifySender->setToEmail($message->from);
				$notifySender->setSubject("RE: " . $message->subject);
				$notifySender->setBodyText("Yes");
				$notifySender->send();				
			}
		}
	}

	private function convertHtml($nodes) {
		$msg = "";
		$msg .= $this->convertChild($nodes);
//		echo $msg;
		return $msg;
	}
		
	private function convertChild($child) {
		$msg = "";
		foreach ($child as $c) {
			if ($c->hasChildNodes()) $msg .= $this->convertChild($c->childNodes);
			else {
				switch ($c->nodeName) {
					case 'br':
						$msg .= "\n";
						break;
					case 'p':
						$msg .= "\n" . $c->nodeValue . "\n";
						break;
					default:
						$msg .= $c->nodeValue;
				}
			}
		}
		return $msg;
	}

	private function parseEmail($message) {
		$notify = strpos($message->subject, 'Notify FS Case#') !== FALSE;
		if ($message->isMultipart()) {
			$foundPart = null;
			foreach (new RecursiveIteratorIterator($message) as $part) {
				try {
					if (strtok($part->contentType, ';') == 'text/html') {
						$foundPart = $part;
						break;
					}
				} catch (Zend_Mail_Exception $e) {
					// ignore
				}
			}
			if (!empty($foundPart)) $message = $foundPart;
		}		
		$msg = "";
		$type = strtok($message->contentType, ';');
		if ($type == 'text/plain') $msg = $message->getContent();
		else if ($type == 'text/html') {
			$content = $message->getContent();
			try {
				switch ($message->contentTransferEncoding) {
					case 'base64':
						$content = base64_decode($content);
						break;
					case 'quoted-printable':
						$content = quoted_printable_decode($content);
						break;
				}
			} catch (Exception $e) {}
			$content = new Zend_Dom_Query($content);
			$body = $content->query('div');
			if (count($body) == 0) $body = $content->query('body');
			$msg = $body->current()->childNodes;
			$msg = $this->convertHtml($msg);
		}
		$case = array();
				
		$account = strpos($msg, "XPS Incident Number") !== FALSE ? 'XPS' : '';
		
		if ($notify) {
			preg_match('/Outcome: (.*)/', $msg, $matches, PREG_OFFSET_CAPTURE);
			$case['Outcome'] = trim($matches[1][0]);
			
			preg_match('/Case: (.*)/', $msg, $matches, PREG_OFFSET_CAPTURE);
			$case['CaseNumber'] = trim($matches[1][0]);
			
			preg_match('/XSM: (.*)/', $msg, $matches, PREG_OFFSET_CAPTURE);
			$case['XSM'] = trim($matches[1][0]);
			
			preg_match('/Company: (.*)/', $msg, $matches, PREG_OFFSET_CAPTURE);
			$case['Company'] = trim($matches[1][0]);
			return $case;
		}

		// Salesforce Account Id
		// 001a000001Luvtr - Xerox XPS
		// 0013000000rJ8VK - Xerox
		if ($account == 'XPS') {
			$locationLabel = 'Location Details:';
			$xsmLabel = '/XPS Incident Number: (.*)/';
			$customerDetailsLabel = '/Incident Customer: (.*)/';
			$incidentLabel = '/Incident Description:\s((?:.|\s)*?)Contact Details/';
			$modelLabel = '/Asset Model: (.*)/';
			$case['AccountId'] = '001a000001Luvtr';
		}
		else {
			$locationLabel = 'Location:';
			$xsmLabel = '/XSM Incident ID: (.*)/';
			$customerDetailsLabel = '/Primary Customer Details:(?:\s*)(.*)/';
			$incidentLabel = '/Incident Description:\s((?:.|\s)*?)Activity Description/';
			$modelLabel = '/Model Name: (.*)/';
			$case['AccountId'] = '0013000000rJ8VK';
		}
		$location = strpos($msg, $locationLabel);
		if ($location !== FALSE) {
			$case['Service_Description__c'] = trim(substr($msg, $location + strlen($locationLabel)));
//			$case['Service_Description__c'] = $msg;
		}

		preg_match($incidentLabel, $msg, $matches, PREG_OFFSET_CAPTURE);
		if (!empty($matches[1][0])) {
			$case['Problem_issue_or_question__c'] = $matches[1][0];
//			$offset = $matches[1][1] + strlen($case['Problem_issue_or_question__c']) + 1;
//			$msg = substr($msg, $offset);
		}
		else
			$case['Problem_issue_or_question__c'] = "";
					
		preg_match($modelLabel, $msg, $matches, PREG_OFFSET_CAPTURE);
		$case['Model_Name__c'] = trim($matches[1][0]);
//		$offset = $matches[1][1] + strlen($case['Model_Name__c']) + 1;
//		$msg = substr($msg, $offset);

		if ($account == 'XPS') {
			preg_match("/Client Account Number: (.*)/", $msg, $matches, PREG_OFFSET_CAPTURE);
			$case['Client_Account_Number__c'] = trim($matches[1][0]);
			preg_match("/Account Contract Number: (.*)/", $msg, $matches, PREG_OFFSET_CAPTURE);
			$case['Contract_Number__c'] = trim($matches[1][0]);
			preg_match("/Budget Center: (.*)/", $msg, $matches, PREG_OFFSET_CAPTURE);
			$case['Budget_Center__c'] = trim($matches[1][0]);
		}

		preg_match($xsmLabel, $msg, $matches, PREG_OFFSET_CAPTURE);
		$case['XSM_Number__c'] = trim($matches[1][0]);
		$offset = $matches[1][1] + strlen($case['XSM_Number__c']) + 1;
		$msg = substr($msg, $offset);
	
		preg_match($customerDetailsLabel, $msg, $matches, PREG_OFFSET_CAPTURE);
		$case['SuppliedName'] = trim($matches[1][0]);
		$offset = $matches[1][1] + strlen($case['SuppliedName']) + 1;
		$msg = substr($msg, $offset);

		preg_match('/Email: (.*)/', $msg, $matches, PREG_OFFSET_CAPTURE);
		$case['SuppliedEmail'] = trim($matches[1][0]);
		$offset = $matches[1][1] + strlen($case['SuppliedEmail']) + 1;
		$msg = substr($msg, $offset);
		if (strpos($case['SuppliedEmail'], "@") === FALSE) $case['SuppliedEmail'] = '';

		preg_match('/Phone: (.*)/', $msg, $matches, PREG_OFFSET_CAPTURE);
		$case['SuppliedPhone'] = trim($matches[1][0]);
		$offset = $matches[1][1] + strlen($case['SuppliedPhone']) + 1;
		$msg = substr($msg, $offset);

//      preg_match('/(?:Primary Customer Details:)(.*\n)
	
	/*      preg_match('/Location:\s*(.*)\s([^,]*),\s([^\s]*)\s([^\s]*)/', $msg, $matches, PREG_OFFSET_CAPTURE);
			$case['Site_Address__c'] = $matches[1][0];
			$case['Site_State__c'] = $matches[3][0];
			$case['Site_Zip_Code__c'] = $matches[4][0];
			$offset = $matches[4][1] + strlen($case['Site_Zip_Code__c']) + 1;
			$msg = substr($msg, $offset);*/
	
		preg_match('/Asset Number: (.*)/', $msg, $matches, PREG_OFFSET_CAPTURE);
		$case['_AssetNumber'] = trim($matches[1][0]);
		$offset = $matches[1][1] + strlen($case['_AssetNumber']) + 1;
		$msg = substr($msg, $offset);
	
		preg_match('/Serial Number: (.*)/', $msg, $matches, PREG_OFFSET_CAPTURE);
		$case['_SerialNumber'] = trim($matches[1][0]);
		$offset = $matches[1][1] + strlen($case['_SerialNumber']) + 1;
		$msg = substr($msg, $offset);
								
		return $case;
	}
}