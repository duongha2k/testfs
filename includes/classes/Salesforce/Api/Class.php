<?php

ini_set('memory_limit', '220M');
//ini_set('error_log', '/home/p3559/error.log');
require_once(dirname(__FILE__) . "/../soapclient/SforcePartnerClient.php");

class Salesforce_Api_Class
{
	private $_sfConnection = NULL;
	private $_sessionId = NULL;

	public function __construct() {
//		Core_Caspio::init();
	}

	public function connect($login, $password, $token) {
		// connect to SF using defa
		$wsdl = dirname(__FILE__) . "/../../../controllers/wsdl/salesforce/fieldsolutions/partner.wsdl.xml";
		$api = new SforcePartnerClient();
		$api->createConnection($wsdl);
		$authResult = $api->login($login, $password . $token);
		if ($authResult) {
			$api->setSessionHeader($authResult->sessionId);
			$this->_sfConnection = $api;
			$this->_sessionId = $authResult->sessionId;
			return $api;
		}
	}

	/**
	 * notifications from salesforce outbound messages
	 *
	 * @param stdClass $data
	 * @return stdClass
	 */
	public function notifications($data) {
//		error_log("*** sync " . print_r($data,true));
		$n = $data->Notification->sObject;
		$ret = new stdClass;
		$ret->Ack = false;

		if (!($n instanceof Salesforce_WorkOrder_Abstract)) return $ret;
		$wo = $n->toAPI_WorkOrder();
		$companyID = $n->companyID();

		$db = Core_Database::getInstance();
		$sql = "SELECT api_username AS user, Password AS pass FROM salesforce_organization JOIN clients ON api_username = UserName WHERE `salesforce_organization`.company_id = ? LIMIT 1";
		$login = $db->fetchAll($sql, array($companyID));
		if (!$login || sizeof($login) == 0) return $ret;
		$login = $login[0];

		$api = new Core_Api_Class;
		$sql = "SELECT WIN_NUM FROM work_orders WHERE Company_ID = ? AND WO_ID = ? LIMIT 1";
		$info = $db->fetchAll($sql, array($companyID, $wo->WO_ID));
		$woExists = ($info && sizeof($info) > 0);
		if ($woExists) $info = $info[0];
		$updateSF = false;
		$assignError = false;
		$msg = NULL;
		if (!empty($wo->Tech_ID)) {
			$updateSF = true;
			$tech = new API_Tech();
//			error_log($wo->Tech_ID);
			$t = $tech->lookupID($wo->Tech_ID, $companyID);
			if (!$t || $tech->isDenied) { 
				$assignError = true;
				$wo->Tech_ID = NULL;
				$msg = "Not found or denied";
			}
//			error_log(print_r($tech,true));
		}
		if (!$woExists) {
			$wo->Amount_Per = "Site";
			$result = $api->createWorkOrder($login['user'], $login['pass'], true, $wo);
			//error_log("**** SF create result: " . print_r($result,true));
		}
		else  {
			$result = $api->updateWorkOrder($login['user'], $login['pass'], $info["WIN_NUM"], $wo);
			//error_log("**** SF update result {$info["WIN_NUM"]}: " . print_r($result,true));
		}

//		error_log(print_r($result,true));

		if (!$result->success) return $ret;

		$ret->Ack = true;

		if ($updateSF) {
			try {
				$dir = dirname(__FILE__);
			        $end = $data->PartnerUrl;
			        $id = $n->Id;
			        $sessid = $data->SessionId;
			        $wsdl = "$dir/../../../controllers/wsdl/salesforce/$companyID/partner.wsdl.xml";
				error_log("$end $id $sessid");
			        $api = new SforcePartnerClient();
			        $api->createConnection($wsdl);
			        $updateFields = array (
			                'Id' => $id,
			                'Name_of_Tech_attending_Site__c' => !$assignError || $wo->Tech_ID == '' ? substr($tech->Firstname . ' ' . $tech->Lastname, 0, 20) : $msg,
			                'Tech_Cell_WO__c' => !$assignError || $wo->Tech_ID == '' ? $tech->PrimaryPhone : NULL
			        );
			        $sObject = new SObject();
			        $sObject->fields = $updateFields;
			        $sObject->type = 'Work_Order__c';
			        $api->setSessionHeader($sessid);
			        $api->setEndpoint($end);
			        $api->update(array($sObject));

				error_log($api->getLastResponse());

/*				$className = "Salesforce_WorkOrder_" . $companyID;
				$callbackWO = new $className;
				$dir = dirname(__FILE__);
				$callback = new SoapClient("$dir/../../../controllers/wsdl/salesforce/$companyID", array('trace' => true,
					'classmap' => array(
								"sObjects" => "Salesforce_WorkOrder_Abstract"
							),
				));*/
//				$callback->
			} catch (Exception $e) {}
		}
		return $ret;
	}
}
