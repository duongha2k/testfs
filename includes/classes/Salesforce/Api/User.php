<?php
class Salesforce_Api_User {
	private $companyId;
	private $token;
	private $isValid = NULL;
	private $api_username;
	private $api_password;
	private $organization_id;
	
	public function __construct($company, $token) {
		$this->companyId = $company;
		$this->token = $token;
	}
	
	public function isAuthentic() {
		if (empty($this->companyId) || empty($this->token)) $this->isValid = false;
		if ($this->isValid !== NULL) return $this->isValid;
		$db = Zend_Registry::get('DB');
		$sql = "SELECT organization_id, `o`.company_id, api_username, token_key, Password FROM salesforce_organization AS o JOIN clients AS c ON api_username = UserName WHERE `o`.company_id = ? LIMIT 1";
		$organization = $db->fetchAll($sql, array($this->companyId));
	
		if (!$organization || sizeof($organization) == 0 || $this->token != md5($organization[0]["token_key"] . $this->companyId)) {
			$key = empty($organization[0]["token_key"]) ? "sf-default-key" : $organization[0]["token_key"];
			$this->isValid = false;
			return $this->isValid;
		}
	
		$this->isValid = true;
		$this->organization_id = $organization[0]["organization_id"];
		$this->api_username = $organization[0]["api_username"];
		$this->api_password = $organization[0]["Password"];
		return $this->isValid;
	}
	
	public function getAPIUsername() {
		return $this->api_username;
	}
	public function getAPIPassword() {
		return $this->api_password;
	}
}
