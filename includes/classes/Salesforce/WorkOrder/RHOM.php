<?php
/**
 * class Salesforce_WorkOrder_RHOM
 * @package Salesforce_WorkOrder_RHOM
 */

class Salesforce_WorkOrder_RHOM extends Salesforce_WorkOrder_Abstract {
	/*
	* Address
	* @var string
	*/
	var $Address_WO_New__c;

	/*
	* City
	* @var string
	*/
	var $City_WO_New__c;

	/*
	* StartDate
	* @var date
	*/
	var $Time_of_Service__c;

	/*
	* EndDate
	* @var date
	*/
	var $Close_Date_WO__c;

	/*
	* PayMax
	* @var double
	*/
	var $Cost_WO__c;

	/*
	* Site Contact Name
	* @var string
	*/
	var $Customer_Account_Name_Hidden__c;

	/*
	* Site Phone
	* @var string
	*/
	var $Customer_Contact_Phone__c;

	/*
	* SiteContact
	* @var string
	*/
	var $Customer_Contact__c;

	/*
	* PO
	* @var string
	*/
	var $Purchase_Order_Number_WO__c;

	/*
	* State
	* @var string
	*/
	var $State_WO_New__c;

	/*
	* WO_ID
	* @var string
	*/
	var $Work_Order_Number_WO_Hidden__c;

	/*
	* Zipcode
	* @var string
	*/
	var $Zip_Code_WO_New__c;

	/*
	* Project_Name
	* @var string
	*/
	var $FS_Project__c;

	/*
	* SiteName
	* @var string
	*/
	var $Location_Account_Name__c;
	
	/*
	* SiteNumber
	* @var string
	*/
	var $Customer_Account_Number_Hidden__c;

	/*
	* Type_ID
	* @var string
	*/
	var $Type_of_Job__c;

	/*
	* _Stage
	* @var string
	*/
	var $Stage__c;

	/*
	* _ContractorAccount
	* @var string
	*/
	var $Contractor_Account_WO__c;

	/*
	* Region - wrong
	* @var string
	*/
	var $Job_Owner_Contact_WO_Hidden__c;

	/*
	* Region
	* @var string
	*/
	var $Job_Owner_Contact_WO__c;

	/*
	* Tech_F_Name / Tech_L_Name
	* @var string
	*/
	var $Name_of_Tech_attending_Site__c;
	
	/*
	* TechPhone
	* @var string
	*/
	var $Tech_Cell_WO__c;

	/*
	* TechPhone
	* @var string
	*/
//	var $Tech_Cell_WO__c;

	private $map = array(
			"Address_WO_New__c" => "Address",
			"City_WO_New__c" => "City",
			"State_WO_New__c" => "State",
			"Zip_Code_WO_New__c" => "Zipcode",
			"Time_of_Service__c" => "StartDate",
			"Close_Date_WO__c" => "EndDate",
			"Cost_WO__c" => "PayMax",
			"Customer_Contact_Phone__c" => "SitePhone",
			"Customer_Contact__c" => "Site_Contact_Name",
			"Purchase_Order_Number_WO__c" => "PO",
//			"Hourly_NTE__c" => "Amount_Per",
			"Work_Order_Number_WO_Hidden__c" => "WO_ID",
			"FS_Project__c" => "Project_Name",
			"Location_Account_Name__c" => "SiteName",
			"Customer_Account_Number_Hidden__c" => "SiteNumber",
			"Type_of_Job__c" => "Type_ID",
			"Stage__c" => "_Stage",
			"Contractor_Account_WO__c" => "_ContractorAccount",
			"Id" => "General5",
			"Job_Owner_Contact_WO__c" => "Region",
			"Name_of_Tech_attending_Site__c" => "Tech_ID",
			"Tech_Cell_WO__c" => "_TechCellPhone"
		);

	public function toAPI_WorkOrder() {
//		error_log("SF notification: " . print_r($this, true));
		$wo = new API_WorkOrder();
		foreach ($this->map as $property => $mapping) {
			$v = $this->$property;
			switch ($mapping) {
				case "Type_ID":
					$v = $v == "Install" ? 1 : 2;
					break;
				case "StartDate":
					//error_log("SF DATE: " . $v);
					$ut = strtotime($v);
					if ($ut !== FALSE) {
						$tz = date_default_timezone_get();
						date_default_timezone_set("America/New_York");
						$wo->StartTime = date("h:i A", $ut);
						$v = date("Y-m-d", $ut);
						date_default_timezone_set($tz);
					}
					else {
						$v = NULL;
					}
					break;
				case "Amount_Per":
					switch ($v) {
						case "Per Hour":
							$v = "Hour";
							break;
						case "Flat Rate":
							$v = "Site";
							break;
						default:
							$v = "Site";
					}
					break;
				case "_Stage":
					$mapping = NULL;
					if ($v == "Job Canceled") {
						$wo->Deactivated = true;
						$wo->DeactivationCode = "Work order cancelled";
						$wo->Deactivated_Reason = "SalesForce API";
					}
					break;
				case "_ContractorAccount":
					$mapping = NULL;
/*					if ($v != "Field Solutions") {
						$wo->Deactivated = true;
						$wo->DeactivationCode = "Work order cancelled";
						$wo->Deactivated_Reason = "SalesForce API";
					}*/
					break;
				case "General5":
					$v = substr($v, 0, -3);
					break;
				case "Tech_ID":
					if ($v == "") {
						$wo->Tech_Bid_Amount = NULL;
			}
					else if (!is_numeric($v)) {
						$mapping = NULL; // only pass when name is tech id
						$wo->Tech_Bid_Amount = $this->Cost_WO__c;
					}
					break;
				case "_TechCellPhone":
					$mapping = NULL;
					break;
				case "Zipcode":
					if ($v == "") $v = "59866"; // temp default
					break;
			}
			if ($mapping != NULL)
				$wo->$mapping = $v;
		}

//		error_log("SF RHOM Class: " . print_r($wo,true));
		return $wo;
	}

	public function companyID() {
		return "RHOM";
	}

	public function mapFieldToSalesforce($field) {
		$m = array_flip($this->map);
		if (!array_key_exists($field, $this->map)) return false;
		$mapping = $m[$field];
		switch ($mapping) {
			case "_TechCellPhone":
			break;
		}
		return $m[$field];
	}
}


