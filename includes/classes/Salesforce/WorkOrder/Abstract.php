<?php
/**
 * class Salesforce_WorkOrder_Abstract mapped to sObject
 * @package Salesforce_WorkOrder_Abstract
 */
class Salesforce_WorkOrder_Abstract {
	/*
	* fields to null
	* @var string
	*/
	var $fieldsToNull;

	/*
	* id
	* @var string
	*/
	var $Id;

	public function toAPI_WorkOrder() {
		$wo = new API_WorkOrder();
		return $wo;
	}

	public function companyID() {
		return "";
	}

	public function mapFieldToSalesforce($field) {
		return false;
	}
}

