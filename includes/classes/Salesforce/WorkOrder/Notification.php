<?php
/**
 * class Salesforce_WorkOrder_Notification
 * @package Salesforce_WorkOrder_Notification
 */

class Salesforce_WorkOrder_Notification {
    /**
     * Id
     *
     * @var string
     */
	var $Id;

    /**
     * sObject is abstract work order which will be defined based on the company
     *
     * @var Salesforce_WorkOrder_Abstract
     */
	var $sObject;
}
