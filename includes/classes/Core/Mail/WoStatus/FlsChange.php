<?php
class Core_Mail_WoStatus_FlsChange extends Core_Mail_Abstract
{    
    
    public function __construct(){}        
    
    public function send()
    {     
        if ($this->tech == null) {
            throw('Tech is undefined');    
        }
        $StartDate = !empty($this->data['PjctStartDate'])?$this->data['PjctStartDate']:'';
        $StartTime = !empty($this->data['PjctStartTime'])?$this->data['PjctStartTime']:'';
        $SiteAddress = !empty($this->data['Address1'])?$this->data['Address1']:'';
        $SiteCity = !empty($this->data['City'])?$this->data['City']:'';
        $SiteState = !empty($this->data['State'])?$this->data['State']:'';
        $SiteZip = !empty($this->data['Zip'])?$this->data['Zip']:'';
        $PayAmt = !empty($this->data['PayAmt'])?$this->data['PayAmt']:'';
        $FLSComments = !empty($this->data['FLSComments'])?$this->data['FLSComments']:'';
        $LastModBy = !empty($this->data['LastModBy'])?$this->data['LastModBy']:'';
        $whatChanged = "";

        // Process incoming date
        if ($StartDate != ""){
        	$StartDate = "Start Date: {\$PjctStartDate}";
        	$whatChanged .= "\n\n $StartDate";
        }
        if ($StartTime != ""){
        	$StartTime = "Start Time: {\$PjctStartTime}";
        	$whatChanged .= "\n\n $StartTime";
        }
        if ($SiteAddress != ""){
        	$SiteAddress = "Site Address: {\$Address1}";
        	$whatChanged .= "\n\n $SiteAddress";
        }	
        if ($SiteCity != ""){
        	$SiteCity = "City: {\$City}";
        	$whatChanged .= "\n\n $SiteCity";
        }	
        if ($SiteState != ""){
        	$SiteState = "State: {\$State}";
        	$whatChanged .= "\n\n $SiteState";
        }
        if ($SiteZip != ""){
        	$SiteZip = "Zipcode: {\$Zip}";
        	$whatChanged .= "\n\n $SiteZip";
        }
        if ($PayAmt != ""){
        	$PayAmount = "Pay Amount: {\$PayAmt}";
        	$whatChanged .= "\n\n $PayAmt";
        }
        if ($FLSComments!= ""){
        	$FLSComments = "Special Instructions: {\$FLSComments}";
        	$whatChanged .= "\n\n $FLSComments";
        }	
        if ($LastModBy != ""){
        	$LastModBy = "Modified By: {\$LastModBy}";
        	$whatChanged .= "\n\n $LastModBy";
        }

        // If Tech id and not reviewed
        if ($this->tech->getTechID() != ""){            
			if ($this->project != NULL) {
	        	$this->assign('projectName',$this->project->getProjectName());
	        	$this->assign('ccName', str_replace(".", "", $this->project->getCoordinator()));
	        	$this->assign('vFromName', str_replace(".", "", $this->project->getCoordinator()));
				$fromEmail = $this->project->getFrom_Email();
			}
			else {
	        	$this->assign('projectName',"FLS");
	        	$this->assign('ccName', "Call Coordinator");
	        	$this->assign('vFromName', "Call Coordinator");
				$fromEmail = "callcoordinators@ftxs.fujitsu.com";
			}
			
			$wo = new Core_WorkOrder();
			$wo->setData($this->data['TB_UNID']);
			$sysGenEmailTo = $wo->getSystemGeneratedEmailTo();
			$toEmail = ($sysGenEmailTo != "" && $sysGenEmailTo != NULL) ?$sysGenEmailTo : $this->project->getTo_Email();
			
			
        	$this->assign('projectCCphone', $this->project->getCoordinator_Phone());
        	$this->assign('FirstName', $this->tech->getFirstName());
        	$this->assign('LastName', $this->tech->getLastName());
        	$this->assign('TechID', $this->tech->getTechID());
        	
            // Send Email to the Tech
            $this->setToEmail($this->tech->getPrimaryEmail());
            $this->subject = "Project: {\$projectName} - Work Order# {\$WorkOrderNo} has been changed";
            $this->setFromEmail($fromEmail);
            $this->tpl = "Dear {\$FirstName} {\$LastName},\n\nWorkorder {\$WorkOrderNo} for Project: {\$projectName} has been changed. Below are the changes:\n\n$whatChanged\n\nTo view your workorder folow this link: https://www.fieldsolutions.com/techs/wosDetails.php?id={\$WorkOrderID}&v={\$TechID} \n\nCoordinator: {\$vFromName}\n\nPhone: {\$projectCCphone}\n\nThank you for your support,\n\nField Solutions Management";
			        		
            // Emails Tech
			$this->isTechEmail(true);
			parent::send();
            
            $this->setToEmail(str_replace(";", ",", $toEmail));
            $this->setFromEmail('projects@fieldsolutions.com');
            $this->setFromName('Field Solutions');
            $this->tpl = "Dear {\$ccName},\n\nWorkorder {\$WorkOrderNo} for Project: {\$projectName} has been changed. Below are the changes:\n\n$whatChanged \n\nThank you,\n\nField Solutions Management";
            		
            // Send Email to the Coordinator
            parent::send();
        }
    }
}