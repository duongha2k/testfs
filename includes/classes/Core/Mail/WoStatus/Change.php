<?php
class Core_Mail_WoStatus_Change extends Core_Mail_Abstract
{

    public function __construct(){}

    public function send($dontEmailToTech = false)
    {
        if ($this->tech == null) {
            throw('Tech is undefined');
        }
        
        $StartDate = !empty($this->data['StartDate'])?$this->data['StartDate']:'';
        $StartTime = !empty($this->data['StartTime'])?$this->data['StartTime']:'';
        $SiteAddress = !empty($this->data['Address'])?$this->data['Address']:'';
        $SiteCity = !empty($this->data['City'])?$this->data['City']:'';
        $SiteState = !empty($this->data['State'])?$this->data['State']:'';
        $SiteZip = !empty($this->data['Zipcode'])?$this->data['Zipcode']:'';
        $PayAmount = !empty($this->data['PayAmount'])?$this->data['PayAmount']:'';
        $SpecialInstructions = !empty($this->data['SpecialInstructions'])?$this->data['SpecialInstructions']:'';
        $BaseTechPay = !empty($this->data['BaseTechPay'])?$this->data['BaseTechPay']:'';
        $OutOfScope = !empty($this->data['OutofScope_Amount'])?$this->data['OutofScope_Amount']:'';
        $TripCharge = !empty($this->data['TripCharge'])?$this->data['TripCharge']:'';
        $MileageCharge = !empty($this->data['MileageReimbursement'])?$this->data['MileageReimbursement']:'';
        $MaterialsCharge = !empty($this->data['MaterialsReimbursement'])?$this->data['MaterialsReimbursement']:'';
        $AbortFeeAmount = !empty($this->data['AbortFeeAmount'])?$this->data['AbortFeeAmount']:'';
        $Additional_Pay_Amount = !empty($this->data['Additional_Pay_Amount'])?$this->data['Additional_Pay_Amount']:'';
        $Add_Pay_Reason = !empty($this->data['Add_Pay_Reason'])?$this->data['Add_Pay_Reason']:'';
        $lastModBy = !empty($this->data['lastModBy'])?$this->data['lastModBy']:'';
        $approved = (!empty($this->data["Approved"]) && $this->data["Approved"]== 1);
        $whatChanged = "";
        $whatChangedTech = "";

        // Process incoming date
        if ($StartDate != ""){
        	$StartDate = "Start Date: {$StartDate}";
        	$whatChanged .= "\n\n $StartDate";
        	$whatChangedTech .= "\n\n $StartDate";
        }
        if ($StartTime != ""){
        	$StartTime = "Start Time: {$StartTime}";
        	$whatChanged .= "\n\n $StartTime";
        	$whatChangedTech .= "\n\n $StartTime";
        }
        if ($SiteAddress != ""){
        	$SiteAddress = "Site Address: {$SiteAddress}";
        	$whatChanged .= "\n\n $SiteAddress";
        	$whatChangedTech .= "\n\n $SiteAddress";
        }
        if ($SiteCity != ""){
        	$SiteCity = "City: {$SiteCity}";
        	$whatChanged .= "\n\n $SiteCity";
        	$whatChangedTech .= "\n\n $SiteCity";
        }
        if ($SiteState != ""){
        	$SiteState = "State: {$SiteState}";
        	$whatChanged .= "\n\n $SiteState";
        	$whatChangedTech .= "\n\n $SiteState";
        }
        if ($SiteZip != ""){
        	$SiteZip = "Zipcode: {$SiteZip}";
        	$whatChanged .= "\n\n $SiteZip";
        	$whatChangedTech .= "\n\n $SiteZip";
        }
        if ($PayAmount != ""){
        	$PayAmount = "Pay Amount: {$PayAmount}";
        	$whatChanged .= "\n\n $PayAmount";
        	if ($approved)
        		$whatChangedTech .= "\n\n $PayAmount";
        }
        if ($SpecialInstructions != ""){
        	$SpecialInstructions = "Special Instructions: {$SpecialInstructions}";
        	$whatChanged .= "\n\n $SpecialInstructions";
        	$whatChangedTech .= "\n\n $SpecialInstructions";
        }
        if ($BaseTechPay != ""){
        	$BaseTechPay = "Base Tech Pay: {$BaseTechPay}";
        	$whatChanged .= "\n\n $BaseTechPay";
        	if ($approved)
        		$whatChangedTech .= "\n\n $BaseTechPay";
        }
        if ($OutOfScope != ""){
        	$OutOfScope = "Out of Scope Amount: {$OutOfScope}";
        	$whatChanged .= "\n\n $OutOfScope";
        	if ($approved)
        		$whatChangedTech .= "\n\n $OutOfScope";
        }
        if ($TripCharge != ""){
        	$TripCharge = "Trip Charge: {$TripCharge}";
        	$whatChanged .= "\n\n $TripCharge";
        	if ($approved)
        		$whatChangedTech .= "\n\n $TripCharge";
        }
        if ($MileageCharge != ""){
        	$MileageCharge = "Mileage Charge: {$MileageCharge}";
        	$whatChanged .= "\n\n $MileageCharge";
        	if ($approved)
        		$whatChangedTech .= "\n\n $MileageCharge";
        }
        if ($MaterialsCharge != ""){
        	$MaterialsCharge = "Materials Charge: {$MaterialsCharge}";
        	$whatChanged .= "\n\n $MaterialsCharge";
        	if ($approved)
        		$whatChangedTech .= "\n\n $MaterialsCharge";
        	}
        if ($AbortFeeAmount != ""){
        	$AbortFeeAmount= "Abort Fee: {$AbortFeeAmount}";
        	$whatChanged .= "\n\n $AbortFeeAmount";
        	if ($approved)
        		$whatChangedTech .= "\n\n $AbortFeeAmount";
        }
        if ($Additional_Pay_Amount != ""){
        	$Additional_Pay_Amount = "Other Adjustments: {$Additional_Pay_Amount}";
        	$whatChanged .= "\n\n $Additional_Pay_Amount";
        	if ($approved)
        		$whatChangedTech .= "\n\n $Additional_Pay_Amount";
        }
        if ($Add_Pay_Reason != ""){
        	$Add_Pay_Reason = "Other Pay Field Explaination: {$Add_Pay_Reason}";
        	$whatChanged .= "\n\n $Add_Pay_Reason";
        	if ($approved)
        		$whatChangedTech .= "\n\n $Add_Pay_Reason";
        }
        if ($lastModBy != ""){
        	$lastModBy = "Modified By: {$lastModBy}";
        	$whatChanged .= "\n\n $lastModBy";
        	if ($approved && $whatChangedTech != "")
        		$whatChangedTech .= "\n\n $lastModBy";
        }

        // If Tech id and not reviewed
            //if ($this->tech->getTechID() != ""){
            //$Core_Api_Class = new Core_Api_Class();
            //$fromEmail = $Core_Api_Class->getFromEmailWoAssign($this->project, $this->futurewoinfo);
            //if (empty($fromEmail)) {
	    //    	$cfgSite = Zend_Registry::get('CFG_SITE');
    	   //     $fromEmail = $cfgSite->admin_email;
            //}
                        
            //13736
            $wosClass = new Core_Api_WosClass;
            $result = $wosClass->getSystemGeneratedEmailAddress_ByWinNum($this->data['TB_UNID']);
            $toEmail = $result["SystemGeneratedEmailTo"];
			
			$fromEmail = $result["SystemGeneratedEmailFrom"];

            // use if project send to email is blank
            if ($fromEmail == ""){
                $fromEmail = "Projects@fieldsolutions.com";
            }
            //end 13736

			$vFromName = str_replace(".", "", $this->project->getResource_Coordinator());
        	$this->assign('projectName',$this->project->getProject_Name());
        	$this->assign('ccName', $vFromName);
        	$this->assign('vFromName', $vFromName);
        	$this->assign('projectCCphone', $this->project->getResource_Coord_Phone());
        	$this->assign('FirstName', $this->tech->getFirstName());
        	$this->assign('LastName', $this->tech->getLastName());
            $this->assign('TechID', $this->tech->getTechID());
			$this->assign('TB_UNID', $this->data['TB_UNID']);

            // Send Email to the Tech
            $this->setToEmail($this->tech->getPrimaryEmail());
            //13980
            $Company_ID   = !empty($this->data['Company_ID'])?$this->data['Company_ID']:'';
            $WIN_NUM   = !empty($this->data['WIN_NUM'])?$this->data['WIN_NUM']:''; 
            $WO_ID   = !empty($this->data['WO_ID'])?$this->data['WO_ID']:''; 
            $FirstName = $this->data['Tech_FName'];
            $LastName = $this->data['Tech_LName'];

            if(true) {
               $this->subject = "WIN# $WIN_NUM - Client ID $WO_ID has been changed";                             
            } else {
                $this->subject = "Project: {\$projectName} - Work Order# {\$WorkOrderNo} has been changed";                
            }
            $this->setFromEmail($fromEmail);
            $this->setFromName($vFromName);
            if(true) {
              $this->tpl = "Dear $FirstName $LastName,\n\nWIN# $WIN_NUM has been changed. The changes are below:\n\n$whatChangedTech\n\nTo view your workorder folow this link: https://www.fieldsolutions.com/techs/wosDetails.php?id={\$TB_UNID}&v={\$TechID} \n\nCoordinator: {\$vFromName}\n\nPhone: {\$projectCCphone}\n\nThank you for your support,\n\nField Solutions Management";                 
            } else {
                $this->tpl = "Dear {\$FirstName} {\$LastName},\n\nWorkorder {\$WorkOrderNo} for Project: {\$projectName} has been changed. Below are the changes:\n\n$whatChangedTech\n\nTo view your workorder folow this link: https://www.fieldsolutions.com/techs/wosDetails.php?id={\$TB_UNID}&v={\$TechID} \n\nCoordinator: {\$vFromName}\n\nPhone: {\$projectCCphone}\n\nThank you for your support,\n\nField Solutions Management";                
            }
            //end 13980
            // Emails Tech


            if ($whatChangedTech != "" && !$dontEmailToTech){
                //echo("<br/>--- SEND EMAIL: ");die();
                
				$this->isTechEmail(true);
                parent::send();
            }
                

            $this->setToEmail(str_replace(";", ",", $toEmail));
            $this->setFromEmail('projects@fieldsolutions.com');
            $this->setFromName('Field Solutions');
            $this->tpl = "Dear {\$ccName},\n\nWorkorder {\$WorkOrderNo} for Project: {\$projectName} has been changed. Below are the changes:\n\n$whatChanged \n\nThank you,\n\nField Solutions Management";

            // Send Email to the Coordinator
            //parent::send();
        }

    }
