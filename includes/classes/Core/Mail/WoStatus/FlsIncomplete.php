<?php
class Core_Mail_WoStatus_FlsIncomplete extends Core_Mail_Abstract
{

    public function __construct()
    {
        $this->tpl = "Dear FS Technician,\n\nWork Order No: {\$WorkOrderNo} needs to be updated on Field Solutions website.  Payment cannot be approved until these steps are taken.\n\nComments: {\$missingInfo}\n\nFollow this link to update the work order: https://www.fieldsolutions.com/techs/wosFLSWorkOrderUpdateWithWOID.php?WOID={\$TB_UNID}&TBID={\$TechID}\n\nCoordinator: {\$vFromName}\nEmail: {\$vFromEmail}\nPhone: {\$projectCCphone}\n\nThank you.\n\nRegards,\n\nField Solutions\nWeb: www.fieldsolutions.com";
        $this->subject = "Payment Delayed - Work Order# {\$WorkOrderNo} not updated on FS website.";
    }

    public function send()
    {
        if ($this->tech == null) {
            throw('Tech is undefined');
        }

        $fromName  = $this->project->getResource_Coordinator();
        $fromEmail = $this->project->getFrom_Email();

        $this->assign('vFromName', $fromName);
        $this->assign('vFromEmail', $fromEmail);

        $this->assign('projectCCphone', $this->project->getResource_Coord_Phone());

        $this->setToEmail($this->tech->getPrimaryEmail());

        if (empty($fromEmail)) {
        	$cfgSite = Zend_Registry::get('CFG_SITE');
            $fromEmail = $cfgSite->admin_email;
        }

        $this->setFromEmail($fromEmail);
        $this->setFromName($fromName);

		$this->isTechEmail(true);
        parent::send();
    }

}