<?php
abstract class Core_Mail_Abstract extends Core_Mail
{
    protected $data = array();
    protected $project = null;
    protected $tech = null; 
    protected $tpl = null;
    protected $subject = null;
    protected $futurewoinfo = null;

    /**
    *  Assign varibles into the mail template 
    * @param string $var
    * @param string $val
    * @return object $this
    */
    public function assign($var, $val)
    {
        $this->data[$var] = $val;
        return $this;
    }
    /**
    * Send the email
    * 
    */
    public function send()
    {
        $this->processEmail();
        $this->setBodyText($this->tpl);
        return parent::send();        
    }     
    
    /**
    * Replace the variables in the template
    * @return string processed template
    */
    protected function processEmail()
    {
        $emailVars = array_keys($this->data);
        
        foreach ($emailVars as &$val) {
            $val = '#{\$'.$val.'}#';   
        }
        
        $emailVals = array_values($this->data);
        
        $this->tpl = preg_replace($emailVars, $emailVals, $this->tpl);
        $this->subject = preg_replace($emailVars, $emailVals, $this->subject);  
        
        // Remove not assigned variables
        $this->tpl = preg_replace('#{\$(.)+?}#', '', $this->tpl);          
    }
    
    /**
    * Set the project ID and make an object for the project
    * 
    * @param int $id
    * @return object $this
    */
    public function loadProject($id)
    {
        $this->project = new Core_Project((int)$id);
        $this->project->setData(); 
        return $this;   
    }
	
	public function setProject(Core_Project $project) {
		$this->project = $project;
	}
    
    public function loadTech($id)
    {
        $this->tech = new Core_Tech((int)$id);
    }
    
    public function setTech(Core_Tech $tech)
    {
        $this->tech = $tech;    
    }
    public function setFuturewoinfo($futurewoinfo)
    {
        $this->futurewoinfo = $futurewoinfo;    
	} 
} 
