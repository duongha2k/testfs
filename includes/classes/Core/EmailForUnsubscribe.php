<?php
/*
  email sent out to support by users who unsubscribe.

*/
class Core_EmailForUnsubscribe{
    private $type;
    private $name;
    private $code;

    function __construct(){
        $this->mail = new Core_Mail();
    }

    public function setEmail($email) {
        $this->email = $email;
    }

    public function sendMail($techInfo) {
		$message = "
            Dear Fieldsolutions
            A user requested to unsubscribe from emails.
            Your FieldSolutions Team
        \r\n";
		
		$message .= "TechID: ".$techInfo['id']."\r\n";
		$message .= "Name: ".$techInfo['name']."\r\n";
		$message .= "Email: ".$techInfo['email']."\r\n";
		$message .= "Phone: ".$techInfo['phone']."\r\n";
		$message .= "Reason for unsubscribing: ".$techInfo['reason']."\r\n";

    	$this->mail->setBodyText($message);
        $this->mail->setFromEmail("support@fieldsolutions.com");
        $this->mail->setFromName("FieldSolutions");
        $this->mail->setToEmail("support@fieldsolutions.com");
        $this->mail->setToName("Support");
        $this->mail->setSubject("Unsubscribe Request");

        $this->mail->send();
    }
}

?>