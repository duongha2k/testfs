<?php

class Core_EmailForBackupTech {

    protected $tech;
    protected $wo;
    protected $project;
    protected $companyId;
    protected $companyName;
    protected $mail;
    protected $curWO;
    protected $Reasigned;

    function __construct()
    {
        $this->mail = new Core_Mail();
    }

    public function setWOData($wo)
    {
        $this->curWO = $wo;
    }

    public function setTech($tech)
    {
        if (is_object($tech))
        {
            $this->tech = $tech;
        } else
        {
            $this->tech = new Core_Tech((int) $tech);
        }
    }

    public function setWorkOrder($workOrder)
    {
        $this->wo = (int) $workOrder;
    }

    public function setProject($project)
    {
        if (is_object($project))
        {
            $this->project = $project;
        } else
        {
            $this->project = new Core_Project((int) $project);
            $this->project->setFields('Project_Name,From_Email,Resource_Coordinator,P2TNotificationEmail,AssignmentNotificationEmail,NotifyOnAssign,NotifyOnAutoAssign');
            $this->project->setData();
        }
    }

    public function setCompanyId($companyId)
    {
        $this->companyId = $companyId;
    }

    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName;
    }

    public function setReasigned($value)
    {
        $this->Reasigned = $value;
    }

    public function sendMail()
    {
        $techID = $this->tech->getTechID();
        $api = new Core_Api_TechClass;
        $techProperties = $api->getProperties($techID);

        $FirstName = $techProperties['FirstName'];
        $LastName = $techProperties['LastName'];
        $PrimaryEmail = $this->tech->getPrimaryEmail();

        $tbUNID = $this->wo;

        $curWO = $this->curWO;

        $startDate = "";
        $startTime = "";
        $city = "";
        $headline = "";
        if ($curWO)
        {
            $WO_ID = $curWO["WO_ID"];
            $WO_Category = $curWO["WO_Category"];
            $Headline = $curWO["Headline"];
            $StartDate = $curWO["StartDate"];
            $EndDate = $curWO["EndDate"];
            $StartTime = $curWO["StartTime"];
            $EndTime = $curWO["EndTime"];
            $SiteName = $curWO["SiteName"];
            $Address = $curWO["Address"];
            $workOrderID = $curWO["WO_ID"];

            $City = $curWO["City"];
            $State = $curWO["State"];
            $Zipcode = $curWO["Zipcode"];
            $PayMax = $curWO["PayMax"]; //334
            $Amount_Per = $curWO["Amount_Per"];
            $ProjectManagerName = $curWO["ProjectManagerName"];
            $ProjectManagerPhone = $curWO["ProjectManagerPhone"];
            $ProjectManagerEmail = $curWO["ProjectManagerEmail"];
//            $ProjectManagerName = $this->project->getResource_Coordinator();
//            $ProjectManagerPhone = $this->project->getProject_Manager_Phone();
//            $ProjectManagerEmail = $this->project->getProject_Manager_Email();
            
        }
        $message = "Dear $FirstName $LastName,

	You have been selected as a Backup Technician. Please contact the Coordinator listed below if you are unable.

	WIN#: $tbUNID
        Client WO #: $WO_ID
        $WO_Category &#8211; $Headline
        Start Date: $StartDate $StartTime
        End Date: $EndDate $EndTime
        Site Name: $SiteName
        $Address
        $City, $State $Zipcode
        Pay: $PayMax PER $Amount_Per

        Coordinator Name: $ProjectManagerName
        Phone: $ProjectManagerPhone
        Email: $ProjectManagerEmail

        Thank you,
        Your FieldSolutions Team

	";

        $this->mail->setBodyText($message);
        $this->mail->setFromEmail($ProjectManagerEmail);
        $this->mail->setFromName($ProjectManagerName);
        $this->mail->setToEmail($PrimaryEmail);
        $this->mail->setToName($FirstName . ' ' . $LastName);
        $this->mail->setSubject("You&#39;ve been selected as a Backup Tech - WIN#$tbUNID" );
        //3296
        $aipSchedulefile = new Core_Api_Class();
        $mySchedulefile = $aipSchedulefile->getAttachFileScheduleForWOBackupTech($tbUNID);
        
        $mySchedulefile = file_get_contents($mySchedulefile);
		if (!empty($mySchedulefile)) {
			$att = new Zend_Mime_Part($mySchedulefile);
			$att->type = 'application/octet-stream';
			$att->disposition = Zend_Mime::DISPOSITION_ATTACHMENT;
			$att->encoding = Zend_Mime::ENCODING_BASE64;
			$att->filename = $tbUNID . '_schedule.ics';
			$this->mail->addAttachments($att);
		}
		$this->mail->setTech($this->tech);
		$this->mail->isTechEmail(true);
        $this->mail->send();
        //$aipSchedulefile->deleteFile($mySchedulefile);

    }

}
