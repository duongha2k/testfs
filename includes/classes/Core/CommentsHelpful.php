<?php
class Core_CommentsHelpful extends Zend_Db_Table_Abstract {
	protected $_name = 'comments_helpful';
	private function setHelpful($id, $client, $flag) {
		$c = new Core_Comments;
		$c = $this->getComment($id, $client);
		if (empty($c)) return false;
		$select = $this->select()
			->where('comment_id = ?', $id)
			->where('ClientID = ?', $clientID);
		$c = $this->fetchRow($select);
		if ($flag === NULL) {
			if (!empty($c)) $c->delete();
			return;
		}
		if (empty($c)) $c = $this->createRow();
		$c->date = new Zend_Db_Expr('NOW()');
		$c->helpful = $flag;
		$c->ClientID = $clientID;
		$c->comment_id = $id;
		$c->save();
	}

	public function helpful($id, $clientID) {
		$this->setHelpful($id, $clientID, 1);
	}

	public function notHelpful($id, $clientID) {
		$this->setHelpful($id, $clientID, 0);
	}

	public function clearHelpful($id, $clientID) {
		$this->setHelpful($id, $clientID, NULL);
	}
}
