<?php

class Core_FlsEmailForAssignedTech
{
    public function setTech($tech)
    {
        if (is_object($tech)) {
        	$this->tech = $tech;
        } else {
            $this->tech = new Core_Tech((int)$tech);
        }
    }
        
    public function setProject($project)
    {
        if (is_object($project)) {
        	$this->project = $project;
        } else {
            $this->project = new Core_FlsProject((int)$project);
//            $this->project->setFields('Project_Name,From_Email,Resource_Coordinator,Project_Name');
            $this->project->setData();
        }
    }
            
    public function sendMail()
    {
        $techID = $this->tech->getTechID();
        $FirstName = $this->tech->getFirstName();
        $LastName  = $this->tech->getLastName();
        $PrimaryEmail = $this->tech->getPrimaryEmail();
        $TechAddress1 = $this->tech->getAddress1();
        $TechAddress2 = $this->tech->getAddress2();
        $TechCity     = $this->tech->getCity();
        $TechState    = $this->tech->getState();
        $TechZipCode  = $this->tech->getZipCode();
        $TechPrimaryPhone   = $this->tech->getPrimaryPhone();
        $TechSecondaryPhone = $this->tech->getSecondaryPhone();
        
       	$projectName = $this->project->getProject_Name();
		
		$WorkOrderID = $this->wo;
		
		$curWO = Core_Caspio::caspioSelectAdv(TABLE_FLS_WORK_ORDERS, "SiteName,Waybill,Address1,City,State,Zip,FLSComments,WorkOrderNo,PartNos,CallType", "WorkOrderID = $WorkOrderID", '');
		
		$siteName = $curWO[0]["SiteName"];
		$Waybill = $curWO[0]["Waybill"];
		if ($Waybill == "NULL")
			$Waybill = "";
		$Address1 = $curWO[0]["Address1"];
		$City = $curWO[0]["City"];
		$State = $curWO[0]["State"];
		$Zip = $curWO[0]["Zip"];
		$FLSSComments = $curWO[0]["FLSComments"];
		$WorkOrderNo = $curWO[0]["WorkOrderNo"];
		$partNo = $curWO[0]["PartNos"];
		if ($partNo == "NULL")
			$partNo = "";			
		$callType = $curWO[0]["CallType"];
				
		if ($callType == "Install"){	
	
			$message = "
Dear $FirstName $LastName,

Congratulations! You have been awarded work from Fujitsu.

You must click on the link below to review the work order information plus any related documentation. You need to confirm to Fujitsu that you are ready to perform the work listed in the work order by checking the box. You will receive another email 48 hours before your install. You will be required to revisit the web site to confirm that  you are ready to go for the install.

Failing to respond to this email and the 48-hour email per instructions, will result in Fujitsu activating another Tech for the job.

<a href='https://www.fieldsolutions.com/techs/wosFLSWorkOrderReviewed.php?woID=$WorkOrderID&techID=$techID'>I have reviewed all Work Order Details and documentation.</a>

Work Order: $WorkOrderNo
Site Name: $siteName
Address: $Address1
City: $City
State: $State
Zip: $Zip

Part No: $partNo
Waybill: $Waybill
Comments: $FLSComments

Your Contact Information:

$ContactAddress1 $ContactAddress2
$ContactCity, $ContactState  $ContactZipCode

Primary Phone: $primaryPhone  $PrimPhoneType
Secondary Phone: $secondaryPhone  $SecondPhoneType
Email: $sendToEmail

<a href='https://www.fieldsolutions.com/techs/profile.php'>Update Contact Information</a>



Fujitsu Coordinator: $vFromName
Email: $vFromEmail
Phone: $projectCCphone

Thank you.
Regards,

Field Solutions, LLC
Web: <a href='http://www.fieldsolutions.com'>www.fieldsolutions.com</a>";

		}else{

			$message = "
Dear $FirstName $LastName,

Congratulations! You have been awarded work from Fujitsu.

Please click on the link below and review the work order information. Note:
You may be required to login to your account if you are not already.
 
You must indicate that you have reviewed the work order info as well as any
project related documentation. Here's how you do that:
 
Refer to section 5 - Tech has reviewed all Work Order Details, which is
the same page you view when clicking on a work order in the system to
update it.
 
In section 5, be sure to check the box confirming to Fujitsu
that you are ready to perform the work listed in the work order.
 
You will also be receiving another auto generated email 48 hours before
your install. You will be required to revisit the website and same section to
click on another box confirming your intentions to be at the install.
 
If you do not do this, Fujitsu will activate the backup for the
install.

<a href='https://www.fieldsolutions.com/techs/wosFLSWorkOrderReviewed.php?woID=$WorkOrderID&techID=$techID'>I have reviewed all Work Order Details and documentation.</a>

Work Order: $WorkOrderNo 
Site Name: $siteName
Address: $Address1
City: $City
State: $State
Zip: $Zip
Part No: $partNo
Waybill: $Waybill

Comments: $FLSComments

Your Contact Information:

$ContactAddress1 $ContactAddress2
$ContactCity, $ContactState  $ContactZipCode

Primary Phone: $primaryPhone  $PrimPhoneType
Secondary Phone: $secondaryPhone  $SecondPhoneType
Email: $sendToEmail

<a href='https://www.fieldsolutions.com/techs/profile.php'>Update Contact Information</a>


Fujitsu Coordinator: $vFromName
Email: $vFromEmail
Phone: $projectCCphone

Thank you.
Regards,

Field Solutions, LLC
Web: <a href='http://www.fieldsolutions.com'>www.fieldsolutions.com</a>";
		}		
		
        $this->mail->setBodyText($message);
        $this->mail->setFromEmail($this->project->getFrom_Email());
        $this->mail->setFromName($this->project->getResource_Coordinator());
        $this->mail->setToEmail($PrimaryEmail);
        $this->mail->setToName($FirstName.' '.$LastName);
        $this->mail->setSubject("Project Assigned: $projectName");
        
        $this->mail->send();		
    }
}