<?php
class Core_Techt
{
    const TABLE_NAME = TABLE_MASTER_LIST;
	const FIND_TECH_MODE_CLIENT = 1;

    private $data = array();
    private $errors = array();
    
    function __construct($id = null)
    {
        if (!empty($id)) {
        	$this->setTechData((int)$id);
        }
    }
    
    private function setTechData($id)
    {
/*        $fields = Core_Caspio::getTableFieldsList(self::TABLE_NAME);
        $criteria = 'TechID='.$id;
        $techData = Core_Caspio::caspioSelectAdv(self::TABLE_NAME, $fields, $criteria, '');
        if (empty($techData[0])) {
            $this->addError('Tech ID '.$id.' not exists');
        	return false;
        }
        foreach ($techData[0] as $k=>$v) {
        	$this->data["$k"] = trim($v, "`'");
        }
        return true;*/
		$techData = self::getProfile($id, true, API_Tech::MODE_TECH, "", true);
        if (empty($techData)) {
            $this->addError('Tech ID '.$id.' not exists');
        	return false;
        }

        foreach ($techData as $k=>$v) {
        	$this->data["$k"] = $v;
        }

        return true;
    }
    
    public function __call($name, $params)
    {
        if (substr($name,0,3) == 'get') {
            $fieldName = substr($name,3);
            if (isset($this->data["$fieldName"])) {
            	return $this->data[$fieldName];
            } else {
                //throw new Exception();
            }
        }
        /*
        if (substr($name,0,3) == 'set') {
            $fieldName = $this->data[substr($name,3,0)];
            $this->data[$fieldName] = $params[0];
        }
        */
    }
    
    private function addError($descr)
    {
        $this->errors[] = $descr;
    }
    
    public function getErrors()
    {
        return empty($this->errors)?null:$this->errors;
    }
    
    public static function getClientPreferredTechs($companyID, $techID = "", $techFName = "", $techLName = ""){
		if(empty($companyID)) return NULL;
		
		$db = Core_Database::getInstance();
		try{
		    $s = $db->select();
		    $s->from(array('p' => Core_Database::TABLE_CLIENT_PREFERRED_TECHS), array("t.TechID","t.FirstName","t.LastName", "t.PrimaryEmail", "t.PrimaryPhone", "t.SATRecommendedAvg", "t.SATRecommendedTotal", "t.SATPerformanceAvg", "t.SATPerformanceTotal"))
		      ->join(array('t' => Core_Database::TABLE_TECH_BANK_INFO), "p.Tech_ID = t.TechID", array())
			->where('p.Tech_ID != 0');
			
			if($techID != "")$s->where('p.Tech_ID = ?', $techID);
			
			if($techID == "" && (!empty($techFName) || !empty($techLName))){
				//lookup tech in preferred table based on fname/lname
				if(!empty($techFName))
					$s->where('t.FirstName LIKE ?', '%'.$techFName.'%');
				
				if(!empty($techLName))
					$s->where('t.LastName LIKE ?', '%'.$techLName.'%');
			}
			
			 $s->where('p.CompanyID = ?', $companyID);
			 
	   		 $result = Core_Database::fetchAll($s);
	   		 if(sizeof($result) > 0){
	   		 	return $result;
	   		 }else{
	   		 	return false;
	   		 }
		}catch(Exception $e){
			error_log($e);
			return false;
		}
    }
    
    public static function removePreferredTech($companyID, $techID){
    	if(empty($companyID) || empty($techID)) return NULL;
    	
    	$db = Core_Database::getInstance();
    	$result = $db->delete(Core_Database::TABLE_CLIENT_PREFERRED_TECHS, array('CompanyID = ?' => $companyID,'Tech_ID = ?' => $techID));
    	
    	if($result >= 1){
    		return $techID;
    	}else{
    		return false;
    	}
    }
    
	public static function isTechPreferred($companyID, $techID){
    	if(empty($companyID) || empty($techID)) return NULL;
    	
    	$db = Core_Database::getInstance();
    	$result = true;
    	try{
		    $select = $db->select();
		    $select->from(Core_Database::TABLE_CLIENT_PREFERRED_TECHS)
		    		->where('Tech_ID = ?', $techID)
		    		->where('CompanyID = ?', $companyID);
		    $result = $db->fetchAll($select);
			$result = $result && sizeof($result) > 0;
    	}catch(Exception $e){
    		$result = "false";
    	}
    	return $result;
    }
    
	public static function addPreferredTech($companyID, $techID, $preferLevel = null){
    	if(empty($companyID) || empty($techID)) return NULL;
    	
    	$data = array("CompanyID"=>$companyID, "Tech_ID"=>$techID);
    	($preferLevel != null) ? $data['PreferLevel'] = $preferLevel : $data['PreferLevel'] = '1';
    	
    	$db = Core_Database::getInstance();
    	$result = $db->insert(Core_Database::TABLE_CLIENT_PREFERRED_TECHS, $data);
    	
    	if($result){
    		return $techID;
    	}else{
    		return false;
    	}
    }
    
	public static function getClientPreferredTechsArray($companyID = NULL) {
		$techs = self::getClientPreferredTechs($companyID);
		if (!$techs) return false;
		$result = array();
		foreach ($techs as $t) {
			if (strpos($t['TechID'], "ISO") !== FALSE) continue;
			$result[] = $t['TechID'];
		}
		return $result;
	}

	public static function clientDenyTech($id, $comments, $companyID, $clientID) {
		if (empty($id)) return false;
		$db = Core_Database::getInstance();
		$result = true;
		try {
			$db->insert(Core_Database::TABLE_CLIENT_DENIED_TECHS, array(
				'TechID' => $id,
				'ClientID' => $clientID,
				'Company_ID' => $companyID,
				'Comments' => $comments
			));
		} catch (Exception $e) { $result = false; }
		return $result;
	}

	public static function removeClientDeniedTech($id, $companyID = null) {
		$db = Core_Database::getInstance();
		$companyCriteria = empty($companyID) ? '' : ' AND ' . $db->quoteInto('Company_ID = ?', $companyID);
		$result = true;
		try {
			$db->delete(Core_Database::TABLE_CLIENT_DENIED_TECHS, $db->quoteInto('TechID = ?', $id) . $companyCriteria);
		} catch (Exception $e) { $result = false; }
		return $result;
	}
	
	public static function getClientDeniedTechs($companyID = null, $params = null, $sort = null, $offset = 0, $limit = 0, &$count = NULL) {
		$db = Core_Database::getInstance();
		$result = array();
		try {
			$selectD = $db->select()->from(array('d' => Core_Database::TABLE_CLIENT_DENIED_TECHS), array('TechID'));
			if (!empty($companyID)) $selectD->where('Company_ID = ?', $companyID);
			$deniedList = $db->fetchCol($selectD);
			if (empty($deniedList)) $deniedList = array(0);
			$select = $db->select()->from(array('d' => Core_Database::TABLE_CLIENT_DENIED_TECHS))
				->join(array('t' => Core_Database::TABLE_TECH_BANK_INFO), '`d`.TechID = `t`.TechID', array('FirstName', 'LastName', 'PrimaryPhone', 'SecondaryPhone', 'PrimaryEmail', 'SecondaryEmail'));
				$select->where('`d`.TechID IN (?)', $deniedList);

			if (!empty($companyID)) $select->where('Company_ID = ?', $companyID);
			if (!empty($sort)) $select->order($sort);
			if ($offset != 0 || $limit != 0) $select->limit($limit, $offset);
			$result = new Core_Filter_Result($select);
			if ($count !== null) $count = count($result);
			$result = $result->toArray();
		} catch (Exception $e) { }
		return $result;
	}
	
	public static function isClientDenied($id, $companyID) {
		$db = Core_Database::getInstance();
		$result = true;
		try {
			$select = $db->select()->from(Core_Database::TABLE_CLIENT_DENIED_TECHS)->where('TechID = ?', $id)->where('Company_ID = ?', $companyID);
			$result = $db->fetchAll($select);
			$result = $result && sizeof($result) > 0;
		} catch (Exception $e) { $result = false; }
		return $result;
	}
	
	public static function getClientDeniedTechsArray($companyID = NULL) {
		$techs = self::getClientDeniedTechs($companyID);
		if (!$techs) return false;
		$result = array();
		foreach ($techs as $t) {
			$result[] = $t['TechID'];
		}
		return $result;
	}
	
	public static function submitHPCertInfo($data){
		if(empty($data) || !is_array($data)) return NULL;
		
		return true;
		$db = Core_Database::getInstance();
		
		
	}
	
	public static function getW9Info($data, $privateKeyPW = NULL){
		if(empty($data)) return NULL;
		
		$techID = $data;
		
		try{
			$db = Core_Database::getInstance();
			$select = $db->select();
			$select->from(Core_Database::TABLE_TECH_W9_INFO)
				->where('TechID = ?', $techID)->order('id DESC')->limit(2);
			$result = Core_Database::fetchAll($select);
		}catch(Exception $e){
			error_log($e->getMessage());
		}
		
		$returnArray = array();
		if (sizeof($result) == 0) return $returnArray;
		foreach($result[0] as $k=>$v){
			$returnArray[$k] = $v;
		}
		
		if (!empty($privateKeyPW)) {
			$privateKey = getW9PrivateKey($privateKeyPW);
			$returnArray["SSN"] = decryptData($returnArray["SSN"], $privateKey);
			$returnArray["EIN"] = decryptData($returnArray["EIN"], $privateKey);
		}
		
		$returnArray['PreviousVersion'] = sizeof($result) > 1;
		
		return $returnArray;
	}
	
	public static function submitW9Info($data){
		if(empty($data) || !is_array($data)) return NULL;

		$db = Core_Database::getInstance();

		$techID = $data['techID'];
		$fields = Core_Database::getFieldList(Core_Database::TABLE_TECH_W9_INFO);
		
		try{
			$select = $db->select();
			$select->from(Core_Database::TABLE_TECH_W9_INFO, $fields)
				->where('TechID = ?', $techID)->order('id DESC')->limit(1);
			$oldW9 = Core_Database::fetchAll($select);
		}catch(Exception $e){
			error_log($e->getMessage());
		}

		if ($oldW9 && sizeof($oldW9) == 1)
			$oldW9 = $oldW9[0];
		else
			$oldW9 = NULL;

		$dbDataArray = array();
		$dbDataArray['TechID'] = $techID;
		foreach($fields as $f){
			if(isset($data[$f]) && $data[$f] != ""){
				if(strstr(strtolower($f), "date")){
					$dbDataArray[$f] =  date("Y-m-d H:i:s", strtotime($data[$f]));
				}else{
					$dbDataArray[$f] = $data[$f];
				}
			}
		}
	
		if((!empty($data['SSN1']) && stripos($data['SSN1'], "X") === FALSE)  
			&& (!empty($data['SSN2']) && stripos($data['SSN2'], "X") == FALSE)
			&& (!empty($data['SSN3']) && stripos($data['SSN3'], "X") == FALSE)){
			
				$SSN = $data['SSN1'].$data['SSN2'].$data['SSN3'];
				$publicKey = getW9PublicKey();
				if(!$publicKey){
					$msg =  "Unable to save information at this time.<br/>";
					return $msg;
					die();
				}
				$maskedSSN = "XXXXX".substr($SSN,-4);
				$SSN =  encryptData($SSN, $publicKey);
				$dbDataArray['MaskedSSN'] = $maskedSSN;
				$dbDataArray['SSN'] = $SSN;
		}
		else if ($data['TIN'] == 'SSN' && !empty($oldW9)) {
			$dbDataArray['MaskedSSN'] = $oldW9['MaskedSSN'];
			$dbDataArray['SSN'] = $oldW9['SSN'];
		}
		
		if((!empty($data['EIN1']) && stripos($data['EIN1'], "X") === FALSE)  
			&& (!empty($data['EIN2']) && stripos($data['EIN2'], "X") == FALSE)){
				$EIN = $data['EIN1'].$data['EIN2'];
				$publicKey = getW9PublicKey();
				if(!$publicKey){
					$msg =  "Unable to save information at this time.<br/>";
					return $msg;
					die();
				}
				$maskedEIN = "XXXXX".substr($EIN,-4);
				$EIN =  encryptData($EIN, $publicKey);
				$dbDataArray['MaskedEIN'] = $maskedEIN;
				$dbDataArray['EIN'] = $EIN;
		}
		else if ($data['TIN'] == 'EIN' && !empty($oldW9)) {
			$dbDataArray['MaskedEIN'] = $oldW9['MaskedEIN'];
			$dbDataArray['EIN'] = $oldW9['EIN'];
		}
		
		if($data['FTC'] != "" && $data['FTC'] == "Other"){
			$dbDataArray['FederalTaxClassification'] = $data['FTCOther'];
		}elseif($data['FTC'] != ""){
			$dbDataArray['FederalTaxClassification'] = $data['FTC'];
		} 
		
		$submitW9 = false;
	
		foreach($fields as $f){
			if($f != "ExemptPayee" && $data[$f] != "") $submitW9 = true;
		}
		
		if($submitW9 != true) return false;
		
		!isset($data['ExemptPayee']) ? $dbDataArray['ExemptPayee'] = 0 : $dbDataArray['ExemptPayee'] = $data['ExemptPayee'];	
		

		try{
			unset($dbDataArray["id"]);
/*			if($data['id'] != ""){
				$db->update(Core_Database::TABLE_TECH_W9_INFO,$dbDataArray,array("id = ?"=>$data['id']));
			}else{*/
				$db->insert(Core_Database::TABLE_TECH_W9_INFO,$dbDataArray);
//			}
		}catch(Exception $e){
			error_log($e->getMessage());
		}
		
		//check W9 Info
		$w9CheckSelect = $db->select()->from(Core_Database::TABLE_TECH_W9_INFO, 
						array("FirstName","LastName", "FederalTaxClassification", "Address1", "City", "State", "ZipCode", "SSN", "EIN", "DigitalSignature"))
				->where('TechID = ?', $techID)->order('id DESC')->limit(1);
		
		$w9CheckResult = Core_Database::fetchAll($w9CheckSelect);
		$w9Completed = true;
		foreach($w9CheckResult[0] as $k=>$v){
			if($k != "SSN" && $k != "EIN"){
				if($v == "" || empty($v)){
					$w9Completed = false;
				}
			}
		}
		
		if($w9CheckResult[0]['SSN'] == "" && $w9CheckResult[0]['EIN'] == ""){
			$w9Completed = false;
		}
		
		if($w9Completed == true){
			$w9CompData['techID'] = $techID;
			$w9CompData['W9'] = 1;
			self::updateTechInfo($w9CompData);
		}else{
			$w9CompData['techID'] = $techID;
			$w9CompData['W9'] = 0;
			self::updateTechInfo($w9CompData);
		}
		return true;
	}
	
	public static function techRegisterLookup($data){
		if(empty($data)) return NULL;
		$db = Core_Database::getInstance();
		
		try{
			$select = $db->select()->from(Core_Database::TABLE_TECH_BANK_INFO, array('ISO_Affiliation_ID'))->where($data['field']." = ?", $data['data']);
			$res = $db->fetchAll($select);
			$result = $res && sizeof($res) > 0;
			if($result == true && ($data['field'] != 'PrimaryEmail' || empty($res[0]['ISO_Affiliation_ID']))){
				return array("error" => $data['field']. " exists");
			}else{
				return array("iso" => $res[0]['ISO_Affiliation_ID']);
			}
		}catch(Exception $e){
			error_log($e->getMessage());
		}
	}
	
	public static function sendSMSOptInEmail($techID, $smsNumber = ""){
		if(empty($techID)) return NULL;
		
		if($smsNumber != ""){
			$db = Core_Database::getInstance();
			$smsSelect = $db->select()->from(Core_Database::TABLE_TECH_BANK_INFO, 
						array("SMS_Number"))
				->where('TechID = ?', $techID);
			$smsResult = Core_Database::fetchAll($smsSelect);
			$origSmsNumber = str_replace("-", "",$smsResult[0]);
			$newSmsNumber = $smsNumber;
			if($origSmsNumber == $newSmsNumber) return false;
		}
		
	//	$techID = $_SESSION['TechID'];
		$send = $_GET["send"];
		$old = $_GET["old"];
		$fromProfile = isset($_GET["TechID"]);
		
	//	ini_set("display_errors",1);
		$emailList = self::getSMSEmailFromTechID(array($techID));
		$message = "Welcome to FS Text Messaging: You will be receiving new work announcements via texting soon!";
		$htmlmessage = nl2br($message);
		
	//	print_r($emailList);
		
		if (sizeof($emailList) > 0) {
			$SendTo = $emailList[0];
			if ($SendTo != "")
				$mail = new Core_Mail();
				$mail->setBodyText($message);
				$mail->setFromName("FieldSolutions");
				$mail->setFromEmail('no-replies@fieldsolutions.com');
				$mail->setToEmail($SendTo);
				$mail->setSubject("Field Solutions SMS");
				$mail->send();
				//		($fromName, $fromEmail, $eList, $subject, $txt, $html, $caller)
				//smtpMail("Field Solutions", "nobody@fieldsolutions.com", $SendTo, "Field Solutions SMS", $message, $htmlmessage, "SMS_Test");
		}
		return true;
	}
	
	public static function getSMSEmailFromTechID($techIDList) {
		//$sms_domain = "ISNULL((SELECT TOP 1 SMS_Domain FROM Cell_Carrier WHERE CellProvider = Carrier), '')";
		if (!is_array($techIDList) || sizeof($techIDList) == 0) return array();
		//$emailList = caspioSelectAdv("TR_Master_List", "SMS_Number, $sms_domain", "TechID IN ('" . implode("','", $techIDList) . "') AND AllowText = '1' AND AcceptTerms = 'Yes' AND Deactivated != '1' AND $sms_domain != '' AND Deactivated !='1'", "", false, "`", "|", false);
			$techIdArray = implode(",", $techIDList);
			$db = Core_Database::getInstance();
			$smsSelect = $db->select()->from(array("tbi" => Core_Database::TABLE_TECH_BANK_INFO), array("SMS_Number"))
							  	  ->joinLeft(array("carrier" =>"cell_carriers"), "tbi.CellProvider = carrier.id", array("sms_domain"))
								  ->where('TechID IN(?)', $techIdArray)
								  ->where("AllowText = '1'")
								  ->where("AcceptTerms = 'Yes'")
								  ->where("Deactivated !='1'");
			$emailList = Core_Database::fetchAll($smsSelect);
		if ($emailList[0] == "") return array();
			$retList = array();
			foreach ($emailList as $e) {
				//if ($parts == "") continue;
				//$info = explode("|", $parts);
				//$phone = trim($info[0], "`");
				//$domain = trim($info[1], "`");
				$phone  = $e['SMS_Number'];
				$domain = $e['sms_domain'];
				
				$phone = filter_var($phone, FILTER_SANITIZE_NUMBER_INT);
				$phone = str_replace(array("-", ".", " ", "(", ")"), array("", "", "", "", ""), $phone);
				
				if ($phone!= "" && $domain != "")
					$retList[] = $phone . $domain;
		}
		return $retList;
	}
	
	public static function phoneExists($phone, $techID = null, $allowISO = true) {
		if (empty($phone)) return NULL;
		$db = Core_Database::getInstance();
		try{
/*			if (!empty($techID) && $allowISO) {
				$select2 = $db->select();
				$select2->from(Core_Database::TABLE_TECH_BANK_INFO, array('ISO_Affiliation_ID'));
				$select2->where("TechID = ?", $techID);
				$select2->limit(1);
				$result = $db->fetchAll($select2);
				$result = $result && sizeof($result) > 0;
				if ($result) $allowISO = false;
			}*/
			
			$select = $db->select();
			$select->from(Core_Database::TABLE_TECH_BANK_INFO);
			if (!empty($techID)) $select->where("TechID <> ?", $techID);
			if (!empty($allowISO)) $select->where("ISO_Affiliation_ID = 0 OR ISO_Affiliation_ID IS NULL"); // ignore ISO techs
			$phone2 = filter_var($phone, FILTER_SANITIZE_NUMBER_INT);
			$phone2 = str_replace(array("-", ".", " ", "(", ")"), array("", "", "", "", ""), $phone2);
			$select->where($db->quoteInto("PrimaryPhone = ?",$phone) . " OR " . $db->quoteInto("SecondaryPhone = ?", $phone) . " OR " . $db->quoteInto("PrimaryPhone = ?",$phone2) . " OR " . $db->quoteInto("SecondaryPhone = ?", $phone2))->where('Deactivated = 0');
			
			$select->limit(1);
			
			$result = $db->fetchAll($select);
			$result = $result && sizeof($result) > 0;
			return $result;
		}catch(Exception $e){
			error_log($e);
			return false;
		}
		return false;
	}

	public static function userExists($user = null, $techID = null, $hideDeactivated = true) {
		if(empty($user) && empty($techID)) return NULL;
		$db = Core_Database::getInstance();
		try{
			$select = $db->select();
			$select->from(Core_Database::TABLE_TECH_BANK_INFO);
			if (!empty($user)) $select->where("UserName = ?", $user);
			if (!empty($techID)) $select->where("TechID = ?", $techID);
			if ($hideDeactivated) $select->where("Deactivated = 0");

			$result = $db->fetchAll($select);
			$result = $result && sizeof($result) > 0;
			return $result;
		}catch(Exception $e){
			error_log($e);
			return false;
		}
		return false;
	}
	
	private static function saveTech($data){
		$db = Core_Database::getInstance();
		$fields = Core_Database::getFieldList(Core_Database::TABLE_TECH_BANK_INFO);
		$techID = $data['techID'];
		unset($data['techID']);
	
		$updateArray = array();

		foreach($fields as $f){
			if(isset($data[$f]) && ($f != "TechID")){
				if(strstr(strtolower($f), "date")){
					$t = strtotime($data[$f]);
					if ($t) 
						$updateArray[$f] =  date("Y-m-d H:i:s", $t);
					else
						$updataArray[$f] = new Zend_Db_Expr('NULL');
				}else{
					$updateArray[$f] = $data[$f];
				}
			}
		}

		$EnterpriseStorage=0;
		if(!empty($data['EnterpriseStorageSelfRating']))
		{            
			$EnterpriseStorage = 1;
		}
                
		if ($data['W9'] == '1') $updateArray['W9_Date_Rec'] = new Zend_Db_Expr('NOW()');
		else $updateArray['W9_Date_Rec'] = new Zend_Db_Expr('NULL');

		if($data['SMS_Number'] != "") $updateArray['AllowText'] = "1";

		if (!empty($data['FLSID'])) $updateArray['FLSstatus'] = "Trained";

		if (isset($updateArray['Bg_Test_Pass_Lite'])) {
			$updateArray['Bg_Test_Pass_Lite'] = empty($updateArray['Bg_Test_Pass_Lite']) ? "" : $updateArray['Bg_Test_Pass_Lite'];
			if ($updateArray['Bg_Test_Pass_Lite'] == '1') $updateArray['Bg_Test_Pass_Lite'] = 'Pass';
			if ($updateArray['Bg_Test_Pass_Lite'] == '0') $updateArray['Bg_Test_Pass_Lite'] = 'Fail';
		}
		if (isset($updateArray['FLSCSP_Rec'])) $updateArray['FLSCSP_Rec'] = empty($updateArray['FLSCSP_Rec']) ? 0 : 1;
		
		if (isset($data['AcceptTerms'])) $updateArray['AcceptTerms'] = empty($data['AcceptTerms']) ? 'No' : 'Yes';
		if (!empty($data['ISO_Affiliation_ID'])) {
			$updateArray['Activation_Confirmed'] = 1;
			$updateArray['ISO_Affiliation'] = new Zend_Db_Expr('(SELECT Company_Name FROM iso WHERE UNID = ' . $db->quoteInto("?", $data['ISO_Affiliation_ID']) . ' LIMIT 1)');
		}
		else if (isset($data['ISO_Affiliation_ID']) && $data['ISO_Affiliation_ID'] == 0){
			$updateArray['ISO_Affiliation_ID'] = new Zend_Db_Expr('NULL');
			$updateArray['ISO_Affiliation'] = new Zend_Db_Expr('NULL');
		}

		$geocode = new Core_Geocoding_Google();
		$runGeocode = false;
		if (!empty($data['Address1'])) {
			$geocode->setAddress($data['Address1']);
			$runGeocode = true;
		}
		if (!empty($data['City'])) {
			$geocode->setCity($data['City']);
			$runGeocode = true;
		}
		if (!empty($data['ZipCode'])) {
			$geocode->setZip($data['ZipCode']);
			$runGeocode = true;
		}
		if (!empty($data['State'])) {
			$geocode->setState($data['State']);
			$runGeocode = true;
		}
		if ($runGeocode) {
			$updateArray['Latitude'] = $geocode->getLat();
			$updateArray['Longitude'] = $geocode->getLon();
		}

		try{
		//	$db->getProfiler()->setEnabled(true);
//			$result = $db->update(Core_Database::TABLE_TECH_BANK_INFO,$updateArray,array("TechID = ?" => $techID, "Activation_Confirmed = '1'"));
			if (!empty($techID)) {
				$updateArray['EnterpriseStorage'] = 1;
				$result = $db->update(Core_Database::TABLE_TECH_BANK_INFO,$updateArray,array("TechID = ?" => $techID));					
//				$result = $db->update(Core_Database::TABLE_TECH_BANK_INFO,array('EnterpriseStorage'=>1),array("TechID = ?" => $techID));
			}
			else {
				if (self::userExists($data['UserName'], $techID, false)) return false;
				$db->beginTransaction();
				$maxIdSql = "SELECT MAX(TechID) AS TechID FROM TechBankInfo WHERE TechID NOT LIKE '%ISO%'";
				$result = $db->fetchAll($maxIdSql);
				$techID = $result[0]['TechID'] + 1;
				$updateArray['TechID'] = $techID;
				try {
					$result = $db->insert(Core_Database::TABLE_TECH_BANK_INFO,$updateArray);
					$db->commit();
					return $techID;
				}catch(Exception $e){
					error_log($e->getMessage());
					$db->rollBack();
				}
			}
			Core_Database_MysqlSync::sync(Core_Database::TABLE_TECH_BANK_INFO, "TechID", $techID);
			Core_History::logTechProfileUpdate($techID, Core_History::SECTION_TECH_MAIN);

			//error_log($db->getProfiler()->getLastQueryProfile()->getQuery());
			//error_log(print_r($db->getProfiler()->getLastQueryProfile()->getQueryParams(),true));
			//$db->getProfiler()->setEnabled(false);			
		}catch(Exception $e){
			error_log($e->getMessage());
			return false;
		}
		try {
			self::copyTechProfile(array($techID));
                              self::copyTechBankInfo(array($techID));
		} catch (Exception $e) {
		}

		return true;
	}

	public static function createTech($data){
		if(empty($data) || !is_array($data)) return NULL;
		
		return self::saveTech($data);		
	}

	public static function updateTechInfo($data){
		if(empty($data) || !is_array($data)) return NULL;

		if (!self::saveTech($data)) return NULL;
		$db = Core_Database::getInstance();
		
		if (!empty($data['certificationsInfo'])) {
			foreach ($data['certificationsInfo'] as $id=>$cert) {
				if ($cert['checked'])
					$cert['cert']->save();
				else
					$cert['cert']->delete();
			}
		}
		
		$techID = $data['techID'];

		$socialNetworks = array("Twitter", "Facebook", "LinkedIn");
		foreach($data as $k => $v){
			if(in_array(substr($k, 0, -1), $socialNetworks)){
				try{
					$snId = substr($k,-1);
					$select = $db->select();
					$select->from("tech_social_networks")
							->where("TechID = ?", $techID)
							->where("SocialNetworkID = ?", $snId);
					$snResult = Core_Database::fetchAll($select);
					$snResult = $snResult && sizeof($snResult) > 0;
					if($snResult){						
						$updResult = $db->update("tech_social_networks",array("URL"=>$v, "dateUpdated"=>date("Y-m-d H:i:s")),array("TechID = ?"=>$techID, "SocialNetworkID = ?"=>$snId));
					}else{
						$insertResult = $db->insert("tech_social_networks", array("TechID"=>$techID, "SocialNetworkID"=>$snId, "URL"=>$v, "dateUpdated"=> date("Y-m-d H:i:s"), "dateCreated"=>date("Y-m-d H:i:s")));
					}
				}catch(Exception $e){
					error_log($e->getMessage());
				}
			}
		}	

		if($data['SMS_Number'] != "")self::sendSMSOptInEmail($techID, $data['SMS_Number']);
		
		if($data['Password'] != "" && $data['passwordconfirm'] != ""){
			 //change strored in session pass
			if ($_SESSION['loggedInAs'] == 'tech') {
                    $_SESSION['UserPassword'] = $data['Password'];
		}
		}
		if($data['UserName'] != ""){
			if ($_SESSION['loggedInAs'] == 'tech') {
            $_SESSION['UserName'] = $data['UserName'];
		}
		}
		
		//update work orders
		if(isset($data['PrimaryEmail']) || isset($data['PrimaryPhone']) || isset($data['FirstName']) || isset($data['LastName'])){
			try{
				$db->update('work_orders', array("Tech_FName" => $data['FirstName'], "Tech_LName" => $data['LastName'], "TechEmail" => $data['PrimaryEmail'], "TechPhone" => $data['PrimaryPhone']), $db->quoteInto('Tech_ID = ?', $techID) . " AND TechPaid = 0");
			}catch(Exception $e){
				error_log($e->getMessage());
			}
		}
		
		return true;
	}
	
	public static function saveBackgroundCheckRequest($data){
		if(empty($data) || !is_array($data)) return NULL;
		//$strDateLite = strtotime($data['Bg_Test_ReqDate_Lite']);
		//$data['Bg_Test_ReqDate_Lite '] = date("Y-m-d H:i:s", $strDateLite);
		
		if($data['Bg_Test_Req_Lite'] == true && self::saveTech($data)){
			
			$mail = new Core_Mail();
			$vFromEmail = "background@fieldsolutions.com";
			$techID = $data["techID"];
			$FirstName = $data['BgFirstName'];	
			$LastName = $data['BgLastName'];
			$email = $data['BgPrimaryEmail'];
			$vSubject = "Background Check Request"; 
			$message = "We received your request for a background check. We will begin to process your request after: A) your payment has been verified (if you forgot to submit your $19 payment via PayPal, click here to submit payment now) and B) you create an account with our processing company by completing the following steps: \n 1. Go to www.Candidatelink.com/fieldsolutions and create an account. Write down your user name and password. (We suggest using your email address as your user name.)\n2. Complete the account creation form and click continue. And Continue Again.\n3. Login using the info you just entered.\n4. Fill in the Candidate Record > Continue.\n5. Fill Screening Collection Form > Continue.\n6. Read Notice and Disclosure info > Continue.\n7. Candidate Release Authorization, and add any needed additional info > Continue.\n8. Certification Release Form > Finish.\n9. When both boxes are checked click LOG OUT.\n\nPlease remember to include your email address, phone number AND enter your Legal Name in the candidate record.\n\nResults will be posted on your profile in about 3 to 5 business days. The result can be found under the My Profile tab of your profile. Passing results are also noted as a green check mark at the top of your profile homepage for 12 months. Clients will NOT see a failing result; it will appear that you have not taken a background check.\n\nIf you have any questions about this process please email background@fieldsolutions.com\n\nWhen you purchase a background check through FieldSolutions you will receive 1,000 FS-PlusOne Visa Debit Card Reward Points within 30 days. Background Check annual renewals are included in this program. Must be enrolled in FS-PlusOne to receive reward.\n\nFS-PlusOne is FieldSolutions’ Visa Debit Card rewards program promoting excellent service quality in which accumulated points will be automatically awarded via issuance to the technician of a personalized FieldSolutions FS-PlusOne Visa Debit Card reward. To register, click the FS-PlusOne logo in the top left hand corner of your technician Control Panel.\n\nThank you,\n\nJennifer\nFieldSolutions \nTechnician Resource Center\n\nYou are receiving this email as a registered technician on www.fieldsolutions.com.\nClick here to stop receiving future e-mails from us.";
			
			
			$mail->setBodyText($message);
			$mail->setFromName("FieldSolutions");
			$mail->setFromEmail($vFromEmail);
			$mail->setToName($FirstName." ".$LastName);
			//$mail->setToEmail('jcintron@fieldsolutions.com');
			$mail->setToEmail($email);
			$mail->setSubject($vSubject);
			$mail->send();
			
			$mail = new Core_Mail();

			$db = Core_Database::getInstance();
			$select = $db->select();
			$select->from(Core_Database::TABLE_TECH_BANK_INFO, array('Bg_Test_Req_Full', 'Bg_Test_ReqDate_Full'))
				->where("TechID = ?", $techID);
			$req = $db->fetchAll($select);

			if (!$req || sizeof($req) == 0) {
				$fullReg = 0;
				$fullRegDate = "";
			}
			else {
				$fullReg = $req[0]["Bg_Test_Req_Full"];
				$fullRegDate = $req[0]["Bg_Test_ReqDate_Full"];
			}
			$message = "Tech ID: $techID\nFirst Name: $FirstName\nLast Name: $LastName\nPrimary Email : $email\nBasic Background Check Requested? Yes \nDate Requested: {$data["Bg_Test_ReqDate_Lite"]}\nComplete Background Check Requested?  " . ($fullReg == true ? "Yes" : "No") . "\nDate Requested: " . $fullRegDate;

			$mail->setBodyText($message);
			$mail->setFromName("Support");
			$mail->setFromEmail("support@fieldsolutions.com");
			$mail->setToEmail("background@fieldsolutions.com");
			$mail->setSubject("New Background Check Requested - $techID");
			$mail->send();			
			return true;
		}else{
			return false;
		}
		
	}
	
	function isInvalidArray($obj) {
		return ($obj == NULL || !is_array($obj) || sizeof($obj) == 0);
	}
	
	function copyTechProfile($techIDList, $updateOnly = TRUE) {
		if(empty($techIDList) || !is_array($techIDList)) return NULL;
		// copies tech profile changes to quickbooks
		
		//$techIDList = "'" . implode("','", $techIDList) . "'";
		$db = Core_Database::getInstance();
		
		if ($updateOnly) {
			// restrict to only records currently in members table
			
			$select = $db->select()->from("quickbooks_members", array('member_id'))->where("member_id IN (?)", $techIDList);

			$result = $db->fetchCol($select);
			//$result = mysqlQuery("SELECT member_id FROM quickbooks_members WHERE member_id IN ($techIDList)");
			$list = array();
			/*
			while ($row = mysql_fetch_row($result)) {
				$list[] = $row[0];
			}
			*/
			foreach($result as $row){$list[] = $row;}
			if (sizeof($list) == 0) return true; // no records to update
			$techIDList = $list;
		}
		$db = Core_Database::getInstance();
		$select = $db->select();
		$select->from(Core_Database::TABLE_TECH_BANK_INFO, array('TechID', 'CompanyName', 'FirstName', 'LastName', 'PrimaryPhone', 'PrimaryEmail', 'Address1', 'Address2', 'City', 'State', 'ZipCode', "IFNULL(ISO_Affiliation_ID, '')", 'Pay_Company'))
			->where('TechID IN (?)', $techIDList);

		$profiles = $db->fetchAll($select, array(), Zend_Db::FETCH_NUM);

	

		if (!$profiles || sizeof($profiles) == 0) return false;
		foreach ($profiles as $key=>$row) {
			$techID = $row[0];
			$companyName = $db->quoteInto("?", $row[1]);
			$firstName = $db->quoteInto("?", $row[2]);
			$lastName = $db->quoteInto("?", $row[3]);
			$primaryPhone = $db->quoteInto("?", $row[4]);
			$primaryEmail = $db->quoteInto("?", $row[5]);
			$address1 = $db->quoteInto("?", $row[6]);
			$address2 = $db->quoteInto("?", $row[7]);
			$city = $db->quoteInto("?", $row[8]);
			$state = $db->quoteInto("?", $row[9]);
			$zip = $db->quoteInto("?", $row[10]);
			$ISOID = $db->quoteInto("?", $row[11]);
			$payCompany = $row[12];
			$ISOID = ($payCompany == 1 && $ISOID != "" ? "$ISOID" : "NULL");
			$profiles[$key] = "('$techID', $companyName, $ISOID, $firstName,$lastName, $primaryPhone, $primaryEmail, $address1, $address2, $city, $state, $zip, NOW(), 'Caspio')";
		}

//		print_r($profiles);

		$profiles = implode(",", $profiles);

		$insertQuery = "INSERT INTO quickbooks_members (member_id, company_name, iso_affiliation_id, firstname, lastname, phone, email, address1, address2, city, state, zip, last_update, last_update_by) VALUES $profiles ON DUPLICATE KEY UPDATE member_id = VALUES(member_id), company_name = VALUES(company_name), iso_affiliation_id = VALUES(iso_affiliation_id), firstname = VALUES(firstname), lastname = VALUES(lastname), phone = VALUES(phone), email = VALUES(email), address1 = VALUES(address1), address2 = VALUES(address2), city = VALUES(city), state = VALUES(state), zip = VALUES(zip), last_update = VALUES(last_update), last_update_by = VALUES(last_update_by)";

//		echo $insertQuery;

		return $db->query($insertQuery);
	}
	
	function copyTechBankInfo($techIDList, $updateOnly = TRUE) {
		// copies any banking info changes to quickbook tables
		if(empty($techIDList)) return NULL;
		$techIDList = "'" . implode("','", $techIDList) . "'";
		if ($updateOnly) {
			$insertQuery = "UPDATE quickbooks_members, TechBankInfo AS TBI LEFT JOIN states_list ON states_list.State = TBI.BankState LEFT JOIN Countries ON Countries.id = TBI.BankCountry SET payment_method = TBI.PaymentMethod, ach_account_name = TBI.AccountName, ach_bank_name = TBI.BankName, ach_bank_city = TBI.BankCity, ach_bank_state = states_list.Abbreviation, ach_bank_zip = TBI.BankZip, ach_bank_country = Countries.Name, ach_account_type = TBI.DepositType, ach_routing_number = TBI.RoutingNum, ach_account_number = TBI.AccountNum, last_update = NOW(), last_update_by = 'Caspio' WHERE quickbooks_members.member_id = TBI.TechID AND TBI.TechID IN (".$techIDList . ")";
		}else {
			$insertQuery = "INSERT INTO quickbooks_members (member_id, payment_method, ach_account_name, ach_bank_name, ach_bank_city, ach_bank_state, ach_bank_zip, ach_bank_country, ach_account_type, ach_routing_number, ach_account_number, last_update, last_update_by) SELECT TechID, PaymentMethod, AccountName, BankName, BankCity, states_list.Abbreviation, BankZip, Countries.Name, DepositType, RoutingNum, AccountNum, NOW(), 'Caspio' FROM TechBankInfo LEFT JOIN states_list ON states_list.State = BankState LEFT JOIN Countries ON Countries.id = BankCountry WHERE TechID IN ($techIDList) ON DUPLICATE KEY UPDATE member_id = VALUES(member_id), payment_method = VALUES(payment_method), ach_account_name = VALUES(ach_account_name), ach_bank_name = VALUES(ach_bank_name), ach_bank_city = VALUES(ach_bank_city), ach_bank_state = VALUES(ach_bank_state), ach_bank_zip = VALUES(ach_bank_zip), ach_bank_country = VALUES(ach_bank_country), ach_account_type = VALUES(ach_account_type), ach_routing_number = VALUES(ach_routing_number), ach_account_number = VALUES(ach_account_number), last_update = VALUES(last_update), last_update_by = VALUES(last_update_by)";
		}
		$db = Core_Database::getInstance();
		return $db->query($insertQuery);
	}
	
	public static function submitPaymentInfo($data){
		if(empty($data) || !is_array($data)) return NULL;
		$msg = "";
/*		$proceed = false;
		if (isset($data)) {
			if (array_key_exists("RoutingNum", $data) &&
				array_key_exists("AccountNum", $data) &&
				stripos($data["RoutingNum"], "x") === FALSE &&
				stripos($data["AccountNum"], "x") === FALSE) {
				$proceed = true;
			}
			else {
				// bad account info
				return "Your account # and/or routing # doesn't appear to be valid";
			}
		}*/
		if (isset($data)) {
			// Update tech info
			
			//$db = Core_Database::getInstance();
			if ($data["PaymentMethod"] != "Direct Deposit") {
				// Ignore/clear other info when not direct deposit
				return self::updateTechInfo($data);
				//$insertQuery = "INSERT INTO TechBankInfo (TechID, FirstName, LastName, PaymentMethod, AgreeTerms, DepositType, BankName, BankAddress1, BankAddress2, BankCity, BankState, BankZip, BankCountry, AccountName, RoutingNum, AccountNum, MaskedRoutingNum, MaskedAccountNum, DateChange) VALUES ('{$_POST['TechID']}', '$firstName', '$lastName', '{$_POST['PaymentMethod']}', '0', 'None', '', '', '', '', '', '', '', '', '', '', '', '', NOW()) ON DUPLICATE KEY UPDATE TechID = '{$_POST['TechID']}', FirstName = '$firstName', LastName = '$lastName', PaymentMethod = '{$_POST['PaymentMethod']}', AgreeTerms = '0', DepositType = '', BankName = '', BankAddress1 = '', BankAddress2 = '', BankCity = '', BankState = '', BankZip = '', BankCountry = '', AccountName = '', RoutingNum = '', AccountNum = '', MaskedRoutingNum = '', MaskedAccountNum = ''";
			}
			else {
				$publicKey = getBankingPublicKey();
				if (!$publicKey) {
					$msg =  "Unable to save information at this time.<br/>";
					return $msg;
					die();
				}

				if (stripos($data["RoutingNum"], 'x') === FALSE && stripos($data["AccountNum"], 'x') === FALSE) {

				if (array_key_exists("RoutingNum", $data)) {
					$repeat = strlen($data["RoutingNum"]) - 4;
					$data["MaskedRoutingNum"] = str_repeat("X", $repeat) . substr($data["RoutingNum"], -4);
					$data["RoutingNum"] =  encryptData($data["RoutingNum"], $publicKey);
				}
				if (array_key_exists("AccountNum", $data)) {
					$repeat = strlen($data["AccountNum"]) - 4;
					$data["MaskedAccountNum"] = str_repeat("X", $repeat) . substr($data["AccountNum"], -4);
					$data["AccountNum"] =  encryptData($data["AccountNum"], $publicKey);
				}
				}
				else {
					unset($data["RoutingNum"]);
					unset($data["AccountNum"]);
				}
				$data['AgreeTerms'] = ($data["PaymentMethod"] == "Direct Deposit" ? "1" : "0");
				$data['BankName'] =  $data["BankName"];
				$data['BankAddress1'] =  $data["BankAddress1"];
				$data['BankAddress2'] =  $data["BankAddress2"];
				$data['BankCity'] =  $data["BankCity"];
				$data['BankState'] =  $data["BankState"];
				$data['BankZip'] =  $data["BankZip"];
				$data['BankCountry'] =  $data["BankCountry"];
				$data['AccountName'] =  $data["AccountName"];
				//$insertQuery = "INSERT INTO TechBankInfo (TechID, FirstName, LastName, PaymentMethod, AgreeTerms, DepositType, BankName, BankAddress1, BankAddress2, BankCity, BankState, BankZip, BankCountry, AccountName, RoutingNum, AccountNum, MaskedRoutingNum, MaskedAccountNum, DateChange) VALUES ('{$_POST['TechID']}', '$firstName', '$lastName', '{$_POST['PaymentMethod']}', '$agreeTerms', '{$_POST['DepositType']}', '$BankName', '$BankAddress1', '$BankAddress2', '$BankCity', '$BankState', '$BankZip', '$BankCountry', '$AccountName', '$routingNum', '$accountNum', '$maskedRoutingNum', '$maskedAccountNum', NOW()) ON DUPLICATE KEY UPDATE TechID = '{$_POST['TechID']}', FirstName = '$firstName', LastName = '$lastName', PaymentMethod = '{$_POST['PaymentMethod']}', AgreeTerms = '$agreeTerms', DepositType = '{$_POST['DepositType']}', BankName = '$BankName', BankAddress1 = '$BankAddress1', BankAddress2 = '$BankAddress2', BankCity = '$BankCity', BankState = '$BankState', BankZip = '$BankZip', BankCountry = '$BankCountry', AccountName = '$AccountName', RoutingNum = '$routingNum', AccountNum = '$accountNum', MaskedRoutingNum = '$maskedRoutingNum', MaskedAccountNum = '$maskedAccountNum'";
				if (self::updateTechInfo($data)) {
				$msg = "Your information has been updated";
				$techID = array($data['techID']);
				self::copyTechProfile($techID);
				self::copyTechBankInfo($techID);
				return true;
				}
			}
		}
	}
	
	public static function saveAdminInternalInfo($data, $techID){
		if(empty($data) || !is_array($data) || empty($techID)) return NULL;
		
		$data['techID'] = $techID;
		$updateResult = self::updateTechInfo($data);
		
		return $updateResult;
	}

	public static function saveSelfRatings($data, $techID){
		if(empty($data) || !is_array($data) || empty($techID)) return NULL;
		
		$selfRatings = array();
		foreach($data as $d){
			$tempArr = get_object_vars($d);
			$selfRatings[$tempArr['column']] = $tempArr['value'];
		}
		$selfRatings['techID'] = $techID;
		$updateResult = self::updateTechInfo($selfRatings);

		Core_History::logTechProfileUpdate($techID, Core_History::SECTION_TECH_SKILLS);
		
		return $updateResult;
	}
	
	public static function saveEquipment($data, $techID, $otherTools = NULL, $telephonyData){
		if(empty($data) || !is_array($data) || empty($techID)) return NULL;
		
		$basicEquipArr = array();
		$techEquipArr = array();
		
		foreach($data as $d){
			$tempArr = get_object_vars($d);
			$basicArr = array("Ladder");
			if(in_array($tempArr['column'],$basicArr)){
				$basicEquipArr[$tempArr['column']] = $tempArr['value'];
			}else{
				try{
					$db = Core_Database::getInstance();
					$select = $db->select();
					$select->from("tech_equipment")
							->where("TechID = ?", $techID)
							->where("equipment_id = ?",$tempArr['value']);
					$result = $db->fetchAll($select);
					$result = $result && sizeof($result) > 0;
					
					if($tempArr['checked']=="1"){
						if(!$result)$insResult = $db->insert("tech_equipment", array("TechID"=>$techID, "equipment_id"=>$tempArr['value']));
					}elseif($tempArr['checked']=="0"){
						if($result)$result = $db->delete("tech_equipment", array('TechID = ?' => $techID,'equipment_id = ?' => $tempArr['value']));
					}
				}catch(Exception $e){
					error_log($e->getMessage());
				}
			}
		}
		
		foreach($telephonyData as $td){
			$tempArr = get_object_vars($td);
				try{
					$db = Core_Database::getInstance();
					$select = $db->select();
					$select->from("tech_skill")
							->where("TechID = ?", $techID)
							->where("skill_id = ?",$tempArr['value']);
					$result = $db->fetchAll($select);
					$result = $result && sizeof($result) > 0;
					
					if($tempArr['checked']=="1"){
						if(!$result)$insResult = $db->insert("tech_skill", array("TechID"=>$techID, "skill_id"=>$tempArr['value']));
					}elseif($tempArr['checked']=="0"){
						if($result)$result = $db->delete("tech_skill", array('TechID = ?' => $techID,'skill_id = ?' => $tempArr['value']));
					}
				}catch(Exception $e){
					error_log($e->getMessage());
				}
		}
		
		$basicEquipArr['techID'] = $techID;
		$basicEquipArr['Tools'] = $otherTools;
		
		try{
			$updateResult = self::updateTechInfo($basicEquipArr);
		}catch(Exception $e){
			error_log($e->getMessage());
		}
		Core_History::logTechProfileUpdate($techID, Core_History::SECTION_TECH_EQUIPMENT);
		return true;
	}
	
	public static function registerTech($data){
		if(empty($data) || !is_array($data)) return NULL;

		$db = Core_Database::getInstance();

		$autoActivate = 0;
		
		try{
			$select = $db->select()->from(Core_Database::TABLE_TECH_BANK_INFO, array('ISO_Affiliation_ID', 'ISO_Affiliation'))->where("PrimaryEmail = ?", $data['primaryEmail']);
			$res = $db->fetchAll($select);
			$result = $res && sizeof($res) > 0;
			if($result == true && empty($res[0]['ISO_Affiliation_ID'])) return array("error" => "email exists");
			if (!empty($res[0]['ISO_Affiliation_ID'])) $autoActivate = 1;
			$data['ISO_Affiliation_ID'] = $res[0]['ISO_Affiliation_ID'];
			$data['ISO_Affiliation'] = $res[0]['ISO_Affiliation'];
		}catch(Exception $e){
			error_log($e);
			return false;
		}
		
		$activationCode = md5(uniqid(rand(), true));
		$db->beginTransaction();
		
		($data['IAC'] == true) ? $data['IAC'] = date("Y-m-d H:i:s") : $data['IAC'] = "";
		$smsNumber = $data['smsPhone1'].$data['smsPhone2'].$data['smsPhone3'];
        $allowText = "0";
        if($smsNumber != "") $allowText = "1";
		
		
		$geocode = new Core_Geocoding_Google();
		if (!empty($data['primaryAddress']))
			$geocode->setAddress($data['primaryAddress']);
		if (!empty($data['city']))
			$geocode->setCity($data['city']);
		if (!empty($data['zip']))
			$geocode->setZip($data['zip']);
		if (!empty($data['state']))
			$geocode->setState($data['state']);
		$data['Latitude'] = $geocode->getLat();
		$data['Longitude'] = $geocode->getLon();
	    
			
		try{
			$maxIdSql = "SELECT MAX(TechID) AS TechID FROM TechBankInfo WHERE TechID NOT LIKE '%ISO%'";
			$result = $db->fetchAll($maxIdSql);
			
			$techID = $result[0]['TechID'] + 1;
			if (empty($data['ISO_Affiliation_ID'])) {
				$data['ISO_Affiliation_ID'] = new Zend_Db_Expr('NULL');
				$data['ISO_Affiliation'] = new Zend_Db_Expr('NULL');
			}

			$db->insert(Core_Database::TABLE_TECH_BANK_INFO, array(
				"TechID" => $techID,
				"ISO_Affiliation_ID" => $data['ISO_Affiliation_ID'],
				"ISO_Affiliation" => $data['ISO_Affiliation'],
				"FirstName" => $data['FName'],
				"LastName" => $data['LName'],
				"PrimaryEmail" => $data['primaryEmail'],
				"SecondaryEmail" => $data['secondaryEmail'],
				"PrimaryPhone" => $data['PrimaryPhone'],
				"SecondaryPhone" => $data['SecondaryPhone'],
				"SMS_Number" => $data['SMS_Number'],
				"AllowText" => $allowText,
				"CellProvider" => $data['CellProvider'],
				"Country" => $data['country'],
				"Address1" => $data['primaryAddress'],
				"Address2" => $data['primaryAddress2'],
				"City" => $data['city'],
				"State" => $data['state'],
				"ZipCode" => $data['zip'],
				"UserName" => $data['username'],
				"Password" => $data['password'],
				"AcceptTerms" => $data['termsOfUse'],
				"Date_TermsAccepted" => $data['IAC'],
				"RegDate" => new Zend_Db_Expr("NOW()"),
				"Activation_Code" => $activationCode,
				"Activation_Confirmed" => $autoActivate,
				"DateChange" => new Zend_Db_Expr("NOW()")
			));
			$db->commit();
			$data["techID"] = $techID;
			
			//INSERT INFO INTO W9 TABLE
			try{
				self::submitW9Info($data);
			}catch(Exception $e){
				error_log($e->getMessage());
			}
		
			if($smsNumber != "" && strlen($smsNumber) == 10){
				self::sendSMSOptInEmail($techID);
			}
			Core_Database_MysqlSync::sync(Core_Database::TABLE_TECH_BANK_INFO, "TechID", $techID);
			if ($autoActivate == 0)
				self::techActivationEmail($data['primaryEmail']);
			else
				self::techRegisterEmail($data['primaryEmail'], $techID);
		}catch(Exception $e){
			$db->rollBack();
			error_log($e->getMessage());
			return false;
		}		
	}
	
	public static function techActivationEmail($email){
		if(empty($email)) return NULL;
		
		$db = Core_Database::getInstance();	
		$emailData = $db->select();
		$emailData->from(Core_Database::TABLE_TECH_BANK_INFO, array("FirstName", "LastName", "Activation_Code"))
						->where('PrimaryEmail = ?', $email);
		$emailDataRes = $db->fetchRow($emailData);
		
		$mail = new Core_Mail();
		$message = "Thanks for registering with FieldSolutions! To activate your account please click the following link:";
		$message .= "http://www.fieldsolutions.com/activateTech.php?email=".$email."&activate=".$emailDataRes['Activation_Code'];
		$mail->setBodyText($message);
		$mail->setFromName("FieldSolutions");
		$mail->setFromEmail('no-replies@fieldsolutions.com');
		$mail->setToName($emailDataRes['FirstName']." ".$emailDataRes['LastName']);
		//$mail->setToEmail('jcintron@fieldsolutions.com');
		$mail->setToEmail($email);
		$mail->setSubject("FieldSolutions Registration Details");
		$mail->send();
		
		return true;
	}
	
	public static function techRegisterEmail($email, $techID = NULL){
		if(empty($email)) return NULL;
		
		$db = Core_Database::getInstance();	
		$emailData = $db->select();
		$emailData->from(Core_Database::TABLE_TECH_BANK_INFO, array("FirstName", "LastName", "UserName", "Password", "TechID"));

		if (empty($techID))
			$emailData->where('PrimaryEmail = ?', $email);
		else
			$emailData->where('TechID = ?', $techID);
		$emailDataRes = $db->fetchRow($emailData);
		
		$techName = $emailDataRes['FirstName']." ".$emailDataRes['LastName'];
		
		$msg = "<p>{$techName}, Welcome to FieldSolutions!</p>";
	
		$msg .= "<p>Your Username and Password for your account are:<br />  
			Username:  {$emailDataRes['UserName']} 
			Password:  {$emailDataRes['Password']} 
			Tech ID #: {$emailDataRes['TechID']} 
			</p>";
	
		$msg .= "<p><b>Getting Started</b>
		Thank you for registering with FieldSolutions. Your profile is viewed by clients everyday looking for technicians for project work and break/fix service nationwide. Over 3000 work orders are sourced through www.fieldsolutions.com each week.  The more information you include in your Technician Profile the more work you will receive. Remember to check your Technician Profile often as new skills, certifications, and sections are added regularly.
		<ul style='list-style-type:decimal;'>
			<li><b>Click this link</b> www.fieldsolutions.com and log in as a technician using your username and password in the upper right hand corner.</li>
			<li><b>Complete your Technician Profile</b> by clicking on <b>My Profile</b> in the left-hand	sidebar. Include detailed information about your experience, skills, certifications, tools, and anything that will assist clients in choosing you for an assignment. Need help? Email us at: support@fieldsolutions.com</li>
			<li><b>View and apply for available work orders.</b> A short list of Available Work Orders are listed on your Control Panel. By clicking on <b>Available Work</b> in the left-hand sidebar you can see all future work orders.</li>
		</ul></p>";
	
		$msg .= "<p><b>NOTIFICATION OF NEW AVAILABLE WORK</b>
		Our clients e-mail you EVERY work opportunity based on your location and your Technician Profile.<br />
		<b>Add the fieldsolutions.com domain to your safe senders list </b> to avoid blocking these timely emails. Occasionally check your spam folder also, just in case.<br />
		<b>FS-PlusOne Rewards– EARN CASH BONUSES</b> <img src='http://www.fieldsolutions.com/widgets/images/fsPlusOneHorizontal.jpg' width='125' height='38' />\n
		Enroll in the FS-PlusOne Rewards program to make extra money and learn about new money-earning opportunities (upper left-hand corner of your control panel). FS-PlusOne rewards you with performance-based CASH bonuses for your work completed through FieldSolutions. For details about the program click on <b>FS-PlusOne Rewards</b> and <b>FS-PlusOne FAQ</b> in the left-hand sidebar after you've logged in.<br />
		Thank you for registering with FieldSolutions. We look forward to working with you.<br />
		Your FieldSolutions Team<br />
		For assistance, contact: support@fieldsolutions.com
		Web: www.fieldsolutions.com<br />
		<img src='http://www.fieldsolutions.com/templates/test/images/fieldSolutionsLogo.jpg'>";
		
		$mail = new Core_Mail();
		$mail->setBodyText($msg);
		$mail->setFromName("FieldSolutions");
		$mail->setFromEmail('no-replies@fieldsolutions.com');
		$mail->setToName($techName);
		$mail->setToEmail($email);
		$mail->setSubject($techName." - Welcome to FieldSolutions!");
		$mail->send();
	}
	
	public static function confirmTechRegistration($email, $code){
		if(empty($email) || empty($code)) return false;
		
		$db = Core_Database::getInstance();
		$result = $db->update(Core_Database::TABLE_TECH_BANK_INFO,array("Activation_Confirmed" => '1'),array("PrimaryEmail = ?" => $email, "Activation_Code = ?" => $code));
		
		if($result){
			$select = $db->select()->from(Core_Database::TABLE_TECH_BANK_INFO, array("UserName","Password"))->where("PrimaryEmail = ?", $email);
			$result = $db->fetchRow($select);
			
			//welcome email
			self::techRegisterEmail($email);
			
			return $result;
		}else{
			return $result;
		}
	}

	public static function getProfile($techID, $returnDeactivated = false, $mode = API_Tech::MODE_TECH, $companyID = NULL, $short = FALSE){
		if (empty($techID)) return NULL;
		
		$returnArray = array();
		$techInfo = new API_Tech();
		$techInfo->setMode($mode);
		if ($mode == API_Tech::MODE_ADMIN) $returnDeactivated = true;
		$techInfo->lookupID($techID, $companyID, $returnDeactivated);
	
		$db = Core_Database::getInstance();
		foreach($techInfo as $k => $v){
			if($k != "RoutingNum" && $k != "AccountNum"){
				mb_detect_encoding($v, "UTF-8") == "UTF-8" ? $v : $v = utf8_encode($v);
				$returnArray[$k] = addslashes($v);	
			}
		}
		$returnArray['RegDate'] = date("m/d/Y",strtotime($returnArray['RegDate']));
		if ($short) return $returnArray;
		
		try{
			$select = $db->select();
			$select->from(array("tbi" =>Core_Database::TABLE_TECH_BANK_INFO),array("SpecificExperience", "EquipExperience","SiteRef", "CellProvider", "BG_Test_ResultsDate_Full", "DatePassDrug", "ISO_Affiliation_ID", "SMS_AgreeDate", "PreferencePercent", "PerformancePercent", "FLS_Photo_ID"))
					->joinLeft(array("carrier" => "cell_carriers"), "tbi.CellProvider = carrier.id", array("carrier"))
					->where('TechID = ?', $techID);
			$selectResult = Core_Database::fetchAll($select);
			foreach($selectResult[0] as $k=>$v){
				$returnArray[$k] = addslashes($v);
			}
		}catch(Exception $e){
			error_log($e->getMessage());
		}
		
		//get tech Last Login Date
		try{
			$select = $db->select();
			$select->from(array('lh' => Core_Database::TABLE_LOGIN_HISTORY), array("MAX(DateTime) as lastLoginDate"))
					->where("UserName = ?",$returnArray['UserName'])
					->where("UserType = 'Tech'");
			$selectResult = Core_Database::fetchAll($select);
			$returnArray['lastLoginDate'] = date("m/d/Y",strtotime($selectResult[0]['lastLoginDate']));
		}catch(Exception $e){
			error_log($e->getMessage());
		}
		
		try{
			$select = $db->select();
			$select->from("techFiles", array("id", "fileType", "filePath", "displayName", "description", "approved"))
				->where('techID = ?', $techID);
			$fileResult = new Core_Filter_Result($select);
			$fileResult = $fileResult->toArray();			

			if(!empty($fileResult)){
				foreach($fileResult as $file){
					$file['encodedFilename'] = base64_encode(urlencode($file['filePath']));
					$returnArray['techFiles'][] = $file;
				}
			}
		}catch(Exception $e){
			error_log($e->getMessage());
		}
		
		//return sms address
		$sms = self::getSMSEmailFromTechID(array($techID));
		@$returnArray['smsAddress'] = $sms[0];
			
		
		//get info for #4 Expertise section
		try{
            $select = $db->select();

            $select->from(array('woCat' => Core_Database::TABLE_WO_CATEGORIES))
            		->joinLeft(array("wo" => Core_Database::TABLE_WORK_ORDERS), "woCat.Category_ID = wo.WO_Category_ID", array("count(woCat.Category_ID) as total", "woCat.*", "wo.Tech_ID"))
            		->where("wo.Tech_ID = ?", $techID)
            		->where("woCat.Category_ID NOT IN (?)",array('26','11'))
            		->group("woCat.Category_ID")
            		->order("woCat.Category DESC");
			$catFieldResult = Core_Database::fetchAll($select);
			
			$expMergeArr = array();
			$selfRatingColumns = array();
			foreach($catFieldResult as $cat){
				$selfRatingColumns[] = $cat['TechSelfRatingColumn'];
			}
				$select = $db->select();
			$select->from(Core_Database::TABLE_TECH_BANK_INFO, $selfRatingColumns)
				->where('TechID = ?', $techID);
				$ratingResult = Core_Database::fetchAll($select);

			if ($ratingResult && !empty($ratingResult[0])) {
				foreach($catFieldResult as $cat){
					$cat['selfRating'] = $ratingResult[0][$cat['TechSelfRatingColumn']];
				$returnArray['expertiseInfo'][] = $cat;
			}
			}
			//select all categories that the user has 0 wo's completed for
			$zeroCatIDs = array();
			foreach($catFieldResult as $c){
				$zeroCatIDs[] = $c['Category_ID'];
			}
			$zeroSelect = $db->select();
			$zeroSelect->from(array(Core_Database::TABLE_WO_CATEGORIES));
			if(!empty($zeroCatIDs))$zeroSelect->where('Category_ID NOT IN (?)',$zeroCatIDs );
			$zeroSelect->where('Category_ID NOT IN ("26","11")');
			$zeroResults = Core_Database::fetchAll($zeroSelect);
			
			$selfRatingColumns = array();
			foreach($zeroResults as $z){
				$selfRatingColumns[] = $z['TechSelfRatingColumn'];
			}
			$select = $db->select();
			$select->from(Core_Database::TABLE_TECH_BANK_INFO, $selfRatingColumns)
							  ->where('TechID = ?', $techID);
			$zeroResSelectResult = Core_Database::fetchAll($select);
				
			foreach($zeroResults as $z){
				$z['total'] = "0";
				$z['selfRating'] = $zeroResSelectResult[0][$z['TechSelfRatingColumn']];
				$z['Tech_ID'] = $techID;
				$returnArray['expertiseInfo'][] = $z;
			}
			
			foreach($returnArray['expertiseInfo'] as $key => $row){
				$selfRating[$key] = $row['Category'];
			}
			
			$srLower = array_map('strtolower', $selfRating);
			array_multisort($srLower,SORT_ASC, SORT_STRING, $returnArray['expertiseInfo']);
			
		}catch(Exception $e){
			error_log($e->getMessage());
		}
		
		//get info for equipment section
		try{
			$select = $db->select();
			$select->from(array("te"=>"tech_equipment"))
					->joinLeft(array("e"=>"equipment"),"te.equipment_id = e.id", array("e.id","e.name", "e.label"))
					->where('te.TechID = ?',$techID);
			$equipRes = Core_Database::fetchAll($select);
			
			foreach($equipRes as $e){
				$returnArray['equipmentInfo'][] = $e['name'];
			}

			
			$select = $db->select();
			$select->from(Core_Database::TABLE_TECH_BANK_INFO, array("Ladder", "Tools"))
					->where("TechID = ?",$techID);
			$tbiRes = Core_Database::fetchAll($select);
			
			foreach($tbiRes as $tb){
				foreach($tb as $k=>$v){
					if($v == "1")$returnArray['equipmentInfo'][] = $k;
					if($k == "Tools")$returnArray['equipmentInfo2'][] = array($k=>$v);
				}
			}
					
			$select = $db->select();
			$select->from(array("ts"=>"tech_skill"))
					->joinLeft(array("s"=>"skills"), "ts.skill_id = s.id", array("s.id", "s.name","s.label"))
					->where("ts.TechID = ?", $techID);
			$telephonyRes = Core_Database::fetchAll($select);
			
			foreach($telephonyRes  as $t){
				$returnArray['equipmentInfo'][] = $t['name'];
			}
			
		}catch(Exception $e){
			error_log($e->getMessage());
		}
		
		//get info for social networks
		try{
			$select = $db->select();
			$select->from(array("tsn"=>"tech_social_networks"),array())
					->join(array("sn"=>"social_networks"), 'tsn.SocialNetworkID = sn.id', array("sn.name", "sn.id", "tsn.URL"))
					->where("tsn.TechID = ?",$techID);
			$snResult = Core_Database::fetchAll($select);

			foreach($snResult as $sn){
				$returnArray['socialNetworks'][] = $sn;
			}
		}catch(Exception $e){
			error_log($e->getMessage());
		}
		
		//get info for public credentials
		try{
				
			$select = $db->select();
			$select->from(array("tbi"=>Core_Database::TABLE_TECH_BANK_INFO),array("EssintialCert as essintialCertStatus","Bg_Test_Pass_Lite as bgCheckStatus", "Bg_Test_ResultsDate_Lite as bgCheckDate", "Bg_Test_Pass_Full as bgCheckFullStatus", "Bg_Test_ResultsDate_Full as bgCheckFullDate", "W9 as w9Status", "W9_Date_Rec as w9Date", "InterimSecClear as interimGovtStatus","InterimSecClearDate as interimGovtDate","TopSecClear as topGovtStatus", "TopSecClearDate as topGovtDate", "FullSecClear as fullGovtStatus", "FullSecClearDate as fullGovtDate", "NCR_Basic_Cert as ncrBasicStatus", "NCR_Basic_Cert_Date as ncrBasicDate", "FLSID as flsIdStatus", "FLSBadgeDate as flsBadgeDate", "FLSWhse as flsWhseStatus", "FLSWhseDate as flsWhseDate", "FLSCSP_Rec as flsCSPStatus", "FLSCSP_RecDate as flsCSPDate", "DrugPassed as drugTestStatus", "DatePassDrug as drugTestDate", "Bg_Test_Pass_Full as bgCheckFullStatus", "BG_Test_ResultsDate_Full as bgCheckFullDate"))
					->joinLeft(array("et"=>"Essintial_Test"), "tbi.TechID = et.tech_id", array("MAX(et.date_taken) as essintialCertDate"))
					->joinLeft(array("ncr" => "NCR_Test"), "tbi.TechID = ncr.tech_id", array("MAX(ncr.date_taken) as ncrBasicDate"))
					->where("tbi.TechID = ?",$techID);
		
			$credResult = Core_Database::fetchAll($select);
					
			foreach($credResult[0] as $k => $v){
				if(strpos($k, "Date") !== false && isset($credResult[0][$k])){
					if($credResult[0][$k] != "0000-00-00 00:00:00"){
						$credResult[0][$k] = date("m/d/Y",strtotime($v));
					}else{
						$credResult[0][$k] = "";
					}
				}
			}
			
			$returnArray['credentialInfo'] = $credResult[0];
			$returnArray['credentialInfo']["bgCheckStatus"] = $returnArray['credentialInfo']["bgCheckStatus"] == 'Pass' ? 1 : 0;
			$returnArray['credentialInfo']["bgCheckFullStatus"] = $returnArray['credentialInfo']["bgCheckFullStatus"] == 'Pass' ? 1 : 0;
		}catch(Exception $e){
			error_log($e->getMessage());
		}
		
		//get info for product/program traning
		try{
			
			$select = $db->select();
			$select->from(array("tbi"=>Core_Database::TABLE_TECH_BANK_INFO),array("Cert_Hallmark_POS as hallmarkCertStatus", "Hallmark_Cert_Num as hallmarkCertNum", "Starbucks_Cert as starbucksCertStatus", "Cert_Maurices_POS as mauricesCertStatus",  "HP_CertNum as hpCertNum", "ServRight_ElectroMech_Cert as electroMechCertStatus"))
					->joinLeft(array("ht"=>"Hallmark_Test"), "tbi.TechID = ht.tech_id", array("MAX(ht.date_taken) as hallmarkCertDate"))
					->joinLeft(array("st"=>"Starbucks_Test"), "tbi.TechID = st.tech_id", array("MAX(st.date_taken) as starbucksCertDate"))
					->joinLeft(array("mt"=>"Maurices_Test"), "tbi.TechID = mt.tech_id", array("MAX(mt.date_taken) as mauricesCertDate"))
					->joinLeft(array("em"=>"ServRight_ElectroMechTest"), "tbi.TechID = em.tech_id", array("MAX(em.date_taken) as electroMechCertDate"))
					->where("tbi.TechID = ?",$techID);

			$certResult = Core_Database::fetchAll($select);
			
			foreach($certResult[0] as $k => $v){
				if(strpos($k, "Date") !== false && isset($certResult[0][$k])){
					$certResult[0][$k] = date("m/d/Y",strtotime($v));
				}
			}
			
			($certResult[0]['hpCertNum'] != "" ? $certResult[0]['hpCertStatus'] = "1" : $certResult[0]['hpCertStatus'] = "0");
		
			$returnArray['trainingInfo'] = $certResult[0];
		}catch(Exception $e){
			error_log($e->getMessage());
		}

		//get info for certifications
		try{
			
			$select = $db->select();
			$select->from(array("tbi"=>Core_Database::TABLE_TECH_BANK_INFO),
						  array("MCSE as mcseStatus","MCSE_Cert_Number as mcseCertNum", "MCSE_Date as mcseDate", 
								"CCNA as ccnaStatus","CCNA_Cert_Number as ccnaCertNum", "CCNA_Date as ccnaDate", 
								"CompTIA as compTiaCertNum", "CompTIA_Date as compTiaDate", 
								"DELL_DCSE_Reg as dellDcseStatus", "DELL_DCSE_Cert_Number as dellDcseCertNum", "DELL_DCSE_Date as dellDcseDate",
								"BICSI as bicsiStatus", "BICSI_Cert_Number as bicsiCertNum", "BICSI_Date as bicsiDate", "APlus as aPlusStatus"))
					->where("tbi.TechID = ?",$techID);
			$certResult = Core_Database::fetchAll($select);
			
			($certResult[0]['compTiaCertNum'] != "" ? $certResult[0]['compTiaStatus'] = "1" : $certResult[0]['compTiaStatus'] = "0");
				 
			foreach($certResult[0] as $k => $v){
				if(strpos($k, "Date") !== false && isset($certResult[0][$k])){
					$certResult[0][$k] = date("m/d/Y",strtotime($v));
				}
			}
			
			$certFromTable = new Core_TechCertifications;
			$certResultFromTable = $certFromTable->getTechCertification(array($techID));
			
			if (!empty($certResultFromTable[$techID])) {
				foreach ($certResultFromTable[$techID] as $cert) {
					$certName = $cert->getName();
					$certNum = $cert->getNumber();
					$certResult[0][$certName . "CertStatus"] = 1;
					$certResult[0][$certName . "CertNum"] = $cert->getNumber();
					$date = $cert->getDate();
					$certResult[0][$certName . "Date"] = empty($date) ? NULL : $date->toString('mm/dd/YYYY');
				}
			}

			$returnArray['certificationsInfo'] = $certResult[0];
		}catch(Exception $e){
			error_log($e->getMessage());
		}

		//get info for likes
//		$numLikes = self::getTechLikes($techId);
//		$returnArray['numLikes'] = $numLikes['numLikes'];

		return $returnArray;
	}
	
	public static function likeTech($techId, $clientId){
		if(empty($techId) || empty($clientId)) return NULL;
		
		$db = Core_Database::getInstance();
		$select = $db->select();
		$select->from(array("l"=>"likes"))->where('TechID = ?',$techId)->where('userID = ?', $clientId);
		$likeCheck = Core_Database::fetchAll($select);
		
		$result = $likeCheck && sizeof($likeCheck) > 0;
		
		$returnArray = array();
		if($result){
			$returnArray['liked'] = true;
			$numLikes = self::getTechLikes($techId, $clientId);
			$returnArray['numLikes'] = $numLikes['numLikes'];
		}else{
			$insResult = $db->insert("likes", array("TechID"=>$techId, "userID"=>$clientId, "dateCreated"=>new Zend_Db_Expr("NOW()")));
			if($insResult){
				$numLikes = self::getTechLikes($techId, $clientId);
				$returnArray['liked'] = $numLikes['liked'];
				$returnArray['numLikes'] = $numLikes['numLikes'];
			}
		}
		
		return $returnArray;
	}
	
	public static function unlikeTech($techId, $clientId){
		if(empty($techId) || empty($clientId)) return NULL;
		
		$db = Core_Database::getInstance();
		$deleteRes = $db->delete("likes", array("TechID = ?" => $techId, "userID = ?"=>$clientId));
		
		$returnArray = array();
		$numLikes = self::getTechLikes($techId, $clientId);
		$returnArray['numLikes'] = $numLikes['numLikes'];
		$returnArray['liked'] = $numLikes['liked'];
		
		return $returnArray;
	}
	
	public static function getTechLikes($techId, $clientId){
		if(empty($techId) || empty($clientId)) return NULL;
		
		$db = Core_Database::getInstance();
		$select = $db->select();
		$select->from(array("l"=>"likes"), array("COUNT(id) as totalLikes"))->where('TechID = ?',$techId);
		$likes = Core_Database::fetchAll($select);
		
		$result = $likes && sizeof($likes) > 0;
		$returnArray = array();
		
		if($result){
			$returnArray["numLikes"] = $likes[0]["totalLikes"];
			
			$select = $db->select();
			$select->from(array("l"=>"likes"))->where('TechID = ?',$techId)->where('userID = ?', $clientId);
			$likeCheck = Core_Database::fetchAll($select);
		
			$likeCheckResult = $likeCheck && sizeof($likeCheck) > 0;
			$likeCheckResult == true ? $returnArray['liked'] = true : $returnArray['liked'] = false;
		}
		
		return $returnArray;
	}
	
	public static function getTechProfileInfo($userName, $passWord, $techID){
		if(empty($userName) || empty($passWord) || empty($techID)) return NULL;
		
		$authData = array('login'=>$userName, 'password'=>$passWord);
		$errors = Core_Api_Error::getInstance();

		$user = new Core_Api_TechUser();
		$user->checkAuthentication($authData);
		
		if (!$user->isAuthenticate())
			return NULL;
		
		return self::getProfile($techID, true);
	}
	
	public static function deleteTechFileAWS($data){
		if(empty($data)) return NULL;

		$S3 = new Core_File();
		$bucketName = "";
		if(strtolower($data['type']) == "resume"){
			$bucketName = S3_TECHS_DOCS_DIR."/resumes";
		}elseif(strtolower($data['type']) == "profile pic"){
			$bucketName = S3_TECHS_DOCS_DIR."/profile-pics";
		}elseif(strtolower($data['type']) == "vehicleimage"){
			$bucketName = S3_TECHS_DOCS_DIR."/vehicle-pics";
		}
		

		$result = $S3->removeFile($data['file'], $bucketName);
		if($result){
			try{
				$db = Core_Database::getInstance();
				$deleteResult = $db->delete("techFiles", array("filePath = ?" => $data['file']));
				if($deleteResult){
					return true;
				}
			}catch(Exception $e){
				error_log($e->getMessage());
				return false;
			}
		}else{
			return false;
		}
	}
	
	 public static function uploadTechFileAWS($data, $techID){
		
		if(empty($data) || empty($techID)) return NULL;
		
		$fileType = "";
		$fileS3Dir = "";
		$file = array();
		$returnArray = array();
		$hpCert = false;
		
		if($data['resumeUpload']){
			$fileType = "Resume";
			$fileS3Dir = "/resumes";
			$file["fileUpload"] = $data['resumeUpload'];
			@unlink($data['resumeUpload']);
		}elseif($data['badgePhotoUpload']){
			$fileType = "Profile Pic";
			$fileS3Dir = "/profile-pics";
			$file["fileUpload"] = $data['badgePhotoUpload'];
			@unlink($data['badgePhotoUpload']);
		}elseif($data['HP_CertProof']){
			$fileType = "HP Cert Proof";
			$fileS3Dir = "/hp-cert-proof";
			$file['fileUpload'] = $data['HP_CertProof'];
			$hpCert = true;
			@unlink($data['HP_CertProof']);
		}elseif($data['vehicleImage']){
			$fileType = "Vehicle Image";
			$fileS3Dir = "/vehicle-pics";
			$file['fileUpload'] = $data['vehicleImage'];
			@unlink($data['vehicleImage']);
		}else{
			return false;
		}
		
		if(!is_uploaded_file($file['fileUpload']['tmp_name'])  &&
            !is_readable($file['fileUpload']['tmp_name'])
           ){ return NULL; }
		
		$error = "";
		
		if(!empty($file['fileUpload']['error'])){
			switch($file['fileUpload']['error']){
				
				case '1':
					$error = 'The uploaded file exceeds the upload_max_filesize directive in php.ini';
				break;
				
				case '2':
					$error = 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form';
				break;
				
				case '3':
					$error = 'The uploaded file was only partially uploaded';
				break;
				
				case '4':
					$error = 'No file was uploaded.';
				break;

				case '6':
					$error = 'Missing a temporary folder';
				break;
				
				case '7':
					$error = 'Failed to write file to disk';
				break;
				
				case '8':
				$error = 'File upload stopped by extension';
				break;
				
				case '999':
				default:
					$error = 'No error code avaiable';
			}
			return $error;
		}elseif(empty($file['fileUpload']['tmp_name']) || $file['fileUpload']['tmp_name'] == 'none'){
			$error = 'No file was uploaded..';
			return false;
		}else{
			$error = Core_Api_Error::getInstance();
			$files = new Core_File();
			
			$displayName = $file['fileUpload']['name'];
			$info = pathinfo($displayName);
            $fName = uniqid($info['filename'].'_');
            $fName = $fName . '.' . $info['extension'];
            $desc = $data['description'];
            
            $fileExists = false;
            $db = Core_Database::getInstance();
            
            //Check db to see if file already uploaded
            $checkSelect = $db->select();
            $checkSelect->from("techFiles")
            			->where("fileType = ?",$fileType)
            			->where("techID = ?", $techID);
            
            $checkResult = new Core_Filter_Result($checkSelect);
			$checkResult = $checkResult->toArray();
			if(!empty($checkResult[0])){
				$fileExists = true;
				$deleteData = array();
				$deleteData['type'] = $checkResult[0]['fileType'];
				$deleteData['file'] = $checkResult[0]['filePath'];
				if(self::deleteTechFileAWS($deleteData) == true){
					$deleteFileRes = $db->delete("techFiles", array('id = ?' => $checkResult[0]['id']));
				}else{
					return false;
				}
			}
            			
			$fData = file_get_contents($file['fileUpload']['tmp_name']);
       	 	$uploadRes = $files->uploadFile($fName, $fData, S3_TECHS_DOCS_DIR.$fileS3Dir);

	     	if(!$uploadRes){
	         	$error->addError(6, 'Upload file '.$fName.' failed');
	         	$errorsDetails = $error->getErrors();
	         	error_log('FileUploading: (tech:Resume:uploadFileAWS) ' . implode('|', $errorsDetails));
	         	return false;
	     	}else{
	     		//add file data to techField table
	     		
	     		$insertArray = array(
						'techID' => $techID,
						'fileType' => $fileType,
						'filePath' => $fName,
						'displayName' => $displayName,
						'description' => $desc,
						'dateCreated' => date("Y-m-d H:i:s"),
						'dateUpdated' => date("Y-m-d H:i:s")
					);
					
				if($fileType == "Profile Pic"){
					$insertArray["approved"] = "0";
				}

	     		$result = true;
				try {
					$db->insert("techFiles", $insertArray);
					
					if($hpCert == true){
						$data['HP_CertProof'] = $fName;
						$data['techID'] = $techID;
						self::updateTechInfo($data);
					}
					
					$returnArray['fileName'] = $fName;
					$returnArray['encodedFilename'] = base64_encode(urlencode($fName));
					$returnArray['displayName'] = $displayName;
				} catch (Exception $e) { error_log($e->getMessage()); $result = false; }
	     		
	     	}
	
	     if ( $error->getErrors() ) {
	         return false;
	      }
			@unlink($file['fileUpload']);	
			return $returnArray;	
		}	
	}
	
	public static function getTechFileAWS($file){
		
		try{
			$db = Core_Database::getInstance();
			$select = $db->select();
			$select->from("techFiles", array("fileType", "filePath", "displayName"))
				->where('filePath = ?', $file);
			$fileResult = new Core_Filter_Result($select);
			$fileResult = $fileResult->toArray();	
			$fileType = $fileResult[0]['fileType'];
		}catch(Exception $e){
			error_log($e->getMessage());
		}
		
		$S3 = new Core_File();
		$bucketName = "";
		if(strtolower($fileType) == "resume"){
			$bucketName = S3_TECHS_DOCS_DIR."/resumes";
		}elseif(strtolower($fileType) == "profile pic"){
			$bucketName = S3_TECHS_DOCS_DIR."/profile-pics";
		}elseif(strtolower($fileType) == "vehicle image"){
			$bucketName = S3_TECHS_DOCS_DIR."/vehicle-pics";
		}
		
		$returnFile = $S3->downloadFile($file, $bucketName);
		return $returnFile;
	}
	
	public static function W9Authenticate($pw) {
		if (empty($pw)) return false;
		$privateKey = getW9PrivateKey($pw);
		if (empty($privateKey)) return false;
		
		return !empty($privateKey);
	}
	
	public static function getSubmittedOrDefaultField($name, $default = "") {
		return (isset($_POST[$name]) ? $_POST[$name] : $default);
	}

	public static function getUserSearchCriteria($data) {
		// coverts search form fields into SQL search criteria
		$fnLen = !empty($data["FirstName"]) ? strlen($data["FirstName"]) : 0;
		$lnLen = !empty($data["LastName"]) ? strlen($data["LastName"]) : 0;
		if (empty($data["ProximityZipCode"]) && empty($data["State"]) && (empty($data["TechID"]) && empty($data["PrimaryEmail"])) && $fnLen < 3 && $lnLen < 3 && ($data["v"] != 'FLS' || empty($data["FLSID"]))) {
			// requires zip code for searches that would return too many techs
			throw new Exception("Zip Code/Postal Code or State/Province is required.");
		}
		
		$ignoreFields = array("doSearch" => 1, "doReset" => 1, "hideBanned" => 1, "preferredOnly" => 1, "newSearchBtn" => 1, "ProximityZipCode" => 1 , "ProximityLat" => 1 , "ProximityLng" => 1, "ProximityDistance" => 1, "module" => 1, "controller" => 1, "action" => 1, "v" => 1, "searchBtn" => 1, "sort" => 1, "order" => 1, "page" => 1, "undefined" => 1, "12months" => 1, "newSearch" => 1, "USAuthorizedDummy" => 1); // ignores these fields on the search form
		
		$searchCriteria = array("TechBankInfo" => array());
		$db = Core_Database::getInstance();
		foreach ($data as $key => $value) {
			// parse searchCriteria
			
			if (array_key_exists($key, $ignoreFields) || $value == "") 				continue; // ignore blank fields and certain fields
			//$value = $value;
			$value = trim($value);
			if ($value == "Y") $value = 1;
			if ($value == "N") $value = 0;
			switch ($key) {
				// special cases
				case "FLSIDExists":
					// field not blank
					if ($value == "1")
						$searchCriteria["TechBankInfo"][] = "IFNULL(FLSID, '') <> ''";
					break;
				
					
				case "Laptop":
				case "CellPhone":
				case "DigitalCam":
				case "Vehicle":
				case "Ladder_6":
				case "Ladder_12":
				case "Ladder_20Plus":
				case "Screwdrivers":
				case "TruckCarryLaddersCable":
				case "OwnOrSourceCablingEquipment":
				case "PossessAllLicenses":
				case "CordMasonryDrillHammer":
				case "RotaryCoaxCable":
				case "RechargeCCTV":
				case "VoltOhmMeter":
				case "FishTape50":
				case "DigitalVOMMeter":
				case "ContinuityTester":
				case "ButtSet":
				case "ToneGeneratorAndWand":
				case "PunchTool66":
				case "PunchTool110":
				case "PunchToolBix":
				case "PunchToolKrone":
				case "CrimpToolRJ11":
				case "CrimpToolRJ45":
				case "PanelTruck":
				case "Truck":
				case "Cable":
				case "CableTesters":
					$searchCriteria["equipment"][] = "$value";
				break;
				
				
				case "AvayaPartnerMerlin":
					if($data['12months'] == '1')$value = "9";
						$searchCriteria["skills"][] = "$value";
						break;
				case "AvayaIPOffice":
					if($data['12months'] == '1')$value = "11";
						$searchCriteria["skills"][] = "$value";
						break;
				case "AvayaDefinity":
					if($data['12months'] == '1')$value = "13";
						$searchCriteria["skills"][] = "$value";
						break;
				case "AvayaPBX8300etc":
					if($data['12months'] == '1')$value = "15";
					
						$searchCriteria["skills"][] = "$value";
						break;
				case "AyayaGateway":
					if($data['12months'] == '1')$value = "17";
						$searchCriteria["skills"][] = "$value";
						break;
				case "NortelNorstar":
					if($data['12months'] == '1')$value = "19";
						$searchCriteria["skills"][] = "$value";
						break;
				case "NortelBMS":
					if($data['12months'] == '1')$value = "21";
						$searchCriteria["skills"][] = "$value";
						break;
				case "NortelMeridian":
					if($data['12months'] == '1')$value = "23";
						$searchCriteria["skills"][] = "$value";
						break;
				case "NortelCS":
					if($data['12months'] == '1')$value = "25";
						$searchCriteria["skills"][] = "$value";
						break;
				case "CiscoCM":
					if($data['12months'] == '1')$value = "27";
						$searchCriteria["skills"][] = "$value";
						break;
				case "Siemens9751":
					if($data['12months'] == '1')$value = "29";
						$searchCriteria["skills"][] = "$value";
						break;
				case "SiemensHicom":
					if($data['12months'] == '1')$value = "31";
						$searchCriteria["skills"][] = "$value";
						break;
				case "SiemensHipath":
					if($data['12months'] == '1')$value = "44";
						$searchCriteria["skills"][] = "$value";
						break;
				case "NEC":
					if($data['12months'] == '1')$value = "34";
						$searchCriteria["skills"][] = "$value";
						break;
				case "InterTel":
					if($data['12months'] == '1')$value = "36";
						$searchCriteria["skills"][] = "$value";
						break;
				case "Mitel":
					if($data['12months'] == '1')$value = "38";
						$searchCriteria["skills"][] = "$value";
						break;
				case "Toshiba":
					if($data['12months'] == '1')$value = "40";
						$searchCriteria["skills"][] = "$value";
						break;
				case "ReadWiringDiagrams":
                                case "ExpInstallSurv":
				case "SkilledCat5Fiber":
				case "40HrsLast6Months":
				case "InstallingTelephony":
				case "ServerTelephony":
					
					$searchCriteria["skills"][] = "$value";
				break;
				/*case "TopSecClear":
				case "FullSecClear":
				case "InterimSecClear":
				case "DrugPassed":
				case "NCR_Basic_Cert":
				
				case "ServRight_ElectroMech_Cert":
				case "MCSE":
				case "CCNA":
				case "APlus":
				case "BICSI":
				case "EssintialCert":*/
				case "HP_CertNum":
				case "FLSCSP":
				case "Cert_Hallmark_POS":
				case "Starbucks_Cert":
				case "Cert_Maurices_POS":
				case "CompTIA":
				case "DellCert":
				
				$searchCriteria["TechBankInfo"][] = "IFNULL($key, '') <> ''";
					break;				
				
				case "LastName":
				case "FirstName":
				case "City":
					$searchCriteria["TechBankInfo"][] = "$key LIKE '%$value%'";
					break;
				case "No_Shows":
				case "Back_Outs":
					if (!is_numeric($value))
					throw new Exception("$key|must be numeric");
					$searchCriteria["TechBankInfo"][] = "$key < '$value'";
					break;
				case "HourlyPay":
				case "Qty_IMAC_Calls":
                case "SATRecommendedAvg":
                case "SATPerformanceAvg":
                case "SATRecommendedTotal":
                case "SATPerformanceTotal":
				case "PerformancePercent":
				case "PreferencePercent":
				case "Qty_FLS_Service_Calls":
				case "Qty_FLS_Service_Calls":
				case "networkingSelfRating":
				case "routersSelfRating":
				case "cablingSelfRating":
                                case "CentralOfficeCablingSelfRating":
				case "electricalSelfRating":
				case "constructionSelfRating":
				case "desktopSelfRating":
				case "CopiersSelfRating":
				case "electroMechSelfRating":
				case "posSelfRating":
				case "atmSelfRating":
				case "electronicsSelfRating":
				case "SatelliteSelfRating":
				case "electricalSelfRating" :
				case "telephonySelfRating":
				case "PrintersSelfRating":
				case "ServersSelfRating":
				case "CCTVSelfRating":
				case "savesSecCabinetsSelfRating":
				case "appleSelfRating":
				case "atmSelfRating":
				case "DigitalSignageSelfRating":
				case "DslSelfRating":
				case "FiberCablingSelfRating":
				case "GeneralWiringSelfRating":
				case "KioskSelfRating":
				case "LowVoltageSelfRating":
				case "RFIDSelfRating":
				case "SatelliteSelfRating":
				case "ServerSoftwareSelfRating":
				case "SiteSurveySelfRating":
				case "TelephonyVoIPSelfRating":
				case "WirelessSelfRating":
				case "ResidentialSelfRating":
				case "CommercialSelfRating":
				case "GovernmentSelfRating":

				
				// field >=
				if (!is_numeric($value))
					throw new Exception("$key|must be numeric");
					$searchCriteria["TechBankInfo"][] = "$key >= '$value'";
					break;
					
				case "DevryShow":
				case "TBCShow":
				case "BlueRibbonTechnicianShow":
					$searchCriteria["certifications"][] = "$value";
					break;
					
				case "DellMRACompliant":					
					$searchCriteria["TechBankInfo"][] = "Bg_Test_Pass_Full = 'Pass' AND DrugPassed = '1' AND Bg_Test_ResultsDate_Full >= DATE_SUB(NOW(), INTERVAL 12 MONTH) AND DatePassDrug >= DATE_SUB(NOW(), INTERVAL 12 MONTH)";
					break;

				case "DellMRALapse":
					$searchCriteria["TechBankInfo"][] = "Bg_Test_Pass_Full = 'Pass' AND DrugPassed = '1' AND (Bg_Test_ResultsDate_Full < DATE_SUB(NOW(), INTERVAL 12 MONTH) OR DatePassDrug < DATE_SUB(NOW(), INTERVAL 12 MONTH))";
					break;
				
				default:
					// default case
					$searchCriteria["TechBankInfo"][] = "$key = '$value'";
					break;
				}
		}
		return $searchCriteria;
	}

	public static function getOrderCriteria($sortColumn, $order, $specialColumn, $defaultSort) {
		// returns SQL code for current sorted column or returns default code
		if ($sortColumn == "")
			return $defaultSort;
		$order = ($order == 1 ? "DESC" : "ASC");
		if (array_key_exists($sortColumn, $specialColumn))
        {
			$sortCriteria = $specialColumn[$sortColumn] . " " . $order;
        }
		else
        {            
			$sortCriteria = $sortColumn . " " . $order;
        }
		return $sortCriteria;
	}
	
    /**
	 * Maps common name fields to fields findTechs method expects
	 *
	 * @param array $params
	 * @return array of fields
	 */
	private static function mapToFindTechParams($params) {
/*        $params['ProximityZipCode'] = $request->getParam("ProximityZipCode");
        $params['ProximityLat'] = $request->getParam("ProximityLat");
        $params['ProximityLng'] = $request->getParam('ProximityLng');
        $params['ProximityDistance'] = $request->getParam('ProximityDistance');*/

	$cert = new Core_TechEquipment;
	$mapping = $cert->getMapping();

	foreach ($mapping as $field) {
		$name = $field["Name"];
		if (empty($params[$name])) continue;
		$params[$name] = $field["id"];;
	}

	$cert = new Core_TechCertifications;
	$mapping = $cert->getMapping();

	foreach ($mapping as $field) {
		$name = $field["name"];
		if (empty($params[$name])) continue;
		$params[$name] = $field["id"];;
	}

	$cert = new Core_TechSkills;
	$mapping = $cert->getMapping();

	foreach ($mapping as $field) {
		$name = $field["name"];
		if (empty($params[$name])) continue;
		$params[$name] = $field["id"];;
	}

        if (!empty($params['SATPerformanceAvg']))
            $params['PerformancePercent'] = $params['SATPerformanceAvg'];

        if (!empty($params['SATRecommendedAvg']))
            $params['PreferencePercent'] = $params['SATRecommendedAvg'];

        if (!empty($params['PerformanceTotal']))
            $params['SATPerformanceTotal'] = $params['PerformanceTotal'];

        if (!empty($params['RecommendedTotal']))
            $params['SATRecommendedTotal'] = $params['RecommendedTotal'];

        if (!empty($params['state']))
            $params['State'] = $params['state'];

        if (!empty($params['ShowDenied']))
            $params['hideBanned'] = self::mapToFindTechsBoolean($params['ShowDenied']) != '1';

        if (!empty($params['TotalCallsGreater']))
            $params['Qty_IMAC_Calls'] = $params['TotalCallsGreater'];

        if (!empty($params['Bg_Test_Pass_Lite']))
            $params['Bg_Test_Pass_Lite'] = self::mapToFindTechsBoolean($params['Bg_Test_Pass_Lite'], array('1', true, 'Pass'));

        if (!empty($params['USVerified']))
            $params['USAuthorized'] = self::mapToFindTechsBoolean($params['USVerified']);

        if (!empty($params['DrugPassed']))
            $params['DrugPassed'] = self::mapToFindTechsBoolean($params['DrugPassed']);

        if (!empty($params['TrapolloMedicalCertified']))
            $params['MDS'] = self::mapToFindTechsBoolean($params['TrapolloMedicalCertified']);

        if (!empty($params['ElectroMechanicalCertification']))
            $params['ServRight_ElectroMech_Cert'] = self::mapToFindTechsBoolean($params['ElectroMechanicalCertification']);

        unset($params['SATPerformanceAvg']);
        unset($params['SATRecommendedAvg']);
        unset($params['PerformanceTotal']);
        unset($params['RecommendedTotal']);
        unset($params['state']);
        unset($params['ShowDenied']);
        unset($params['TotalCallsGreater']);
        unset($params['USVerified']);
        unset($params['emailList']);
	unset($params['TrapolloMedicalCertified']);
	unset($params['ElectroMechanicalCertification']);
		return $params;
	}

	private static function mapToFindTechsBoolean($val, $trueVals = NULL) {
		if (empty($val)) return 0;
		if (empty($trueVals)) $trueVals = array('1', true);
		if (is_array($trueVals)) {
			if (in_array($val, $trueVals, true))
				$val = 1;
			else
				$val = 0;
		}
		else
			$val = $val == '1';
	}
	
    /**
	 * Finds techs bases on criteria
	 *
	 * @param array $params search criteria
	 * @return array of matching tech dis
	 */

public static function findTechs($params, $page = 1, $limit = 500, $mode = self::FIND_TECH_MODE_CLIENT) {
        $searchCriteria = "";
		$params = self::mapToFindTechParams($params);

        $db = Core_Database::getInstance();

        // get variables
        $companyID = empty($params['v']) ? 'BLANKID' : $params['v'];
        $showBanned = empty($params['hideBanned']) ? false : $params['hideBanned'];
        $showPreferred = empty($params['preferredOnly']) ? false : $params['preferredOnly'];
        $helpingHandsShow = empty($params['HelpingHandsShow']) ? false : $params['HelpingHandsShow'];
        $sort = empty($params['sort']) ? NULL : $params['sort'];
        $order = empty($params['order']) ? NULL : $params['order'];
        $USAuthorized = empty($params['USAuthorized']) ? '' : $params['USAuthorized'];
		$MDS = empty($params['MDS']) ? '' : $params['MDS'];
        $Copier_Skills_Assessment = empty($params['Copier_Skills_Assessment']) ? '' : $params['Copier_Skills_Assessment'];

		unset($params['SATPerformanceAvg']);
        unset($params['SATRecommendedAvg']);
        unset($params['emailList']);
        unset($params['HelpingHandsShow']);
        unset($params['USAuthorized']);
		unset($params['MDS']);
        unset($params['Copier_Skills_Assessment']);
		
        try {
            $searchCriteriaResults = self::getUserSearchCriteria($params);
        } catch (Exception $e) {
            // invalid field entered
			return array("error" => explode("|", $e->getMessage()));
        }

        // result page configuration
        $resultsPerPage = $limit;
		
		if ($limit == 500) $page = 1; // only return max of 500 techs
		$currentResultPage = $page;

        // fields pulled from DB
        $preferredTechs = self::getClientPreferredTechsArray($companyID);
        $DeVryTechs = Core_TechCertifications::getDeVryTechIdsArray();
        $TBCTechs = Core_TechCertifications::getTBCTechIdsArray();
        $BlueRibbonTechs = Core_TechCertifications::getBlueRibbonTechIdsArray();
        $I9VerifiedTechs = Core_Api_TechClass::getI9VerifiedTechIdsArray();
        $I9PendingTechs = Core_Api_TechClass::getI9PendingTechIdsArray();

        $specialColumns = array("ISO" => "(CASE WHEN IFNULL(ISO_Affiliation, '') = '' THEN 'No' ELSE 'Yes' END)",
            "PreferLevel" => "(CASE WHEN (" . ($preferredTechs && $preferredTechs[0] != '' ? 'TechID IN (' . implode(",", $preferredTechs) . ')' : '1 = 0') . ") THEN 'Yes' ELSE 'No' END)",
            "SpecialDistance" => "Distance",
            "TotalWOs" => "(IFNULL(Qty_FLS_Service_Calls,0) + IFNULL(Qty_IMAC_Calls,0))",
            "DellMRACompliant" => "(CASE WHEN (Bg_Test_Pass_Full = 'Pass' AND DrugPassed = '1' AND Bg_Test_ResultsDate_Full >= DATE_SUB(NOW(), INTERVAL 12 MONTH) AND DatePassDrug >= DATE_SUB(NOW(), INTERVAL 12 MONTH)) THEN 'Passed' WHEN (Bg_Test_Pass_Full = 'Pass' AND DrugPassed = '1' AND (Bg_Test_ResultsDate_Full < DATE_SUB(NOW(), INTERVAL 12 MONTH) OR DatePassDrug < DATE_SUB(NOW(), INTERVAL 12 MONTH))) THEN 'Lapsed' ELSE '' END)", "LapsedDate" => "DATE_ADD((CASE WHEN Bg_Test_ResultsDate_Full > DatePassDrug THEN DatePassDrug ELSE Bg_Test_ResultsDate_Full END), INTERVAL 12 MONTH)",
            "DeVryFlag" => "(CASE WHEN (" . (!empty($DeVryTechs) ? 'TechID IN (' . implode(",", $DeVryTechs) . ')' : '1 = 0') . ") THEN 1 ELSE 0 END)",
            "TBCFlag" => "(CASE WHEN (" . (!empty($TBCTechs) ? 'TechID IN (' . implode(",", $TBCTechs) . ')' : '1 = 0') . ") THEN 1 ELSE 0 END)",
            "BlueRibbonFlag" => "(CASE WHEN (" . (!empty($TBCTechs) ? 'TechID IN (' . implode(",", $TBCTechs) . ')' : '1 = 0') . ") THEN 1 ELSE 0 END)" ,
            "I9VerifiedFlag" => "(CASE WHEN (" . (!empty($I9VerifiedTechs) ? 'TechID IN (' . implode(",", $I9VerifiedTechs) . ')' : '1 = 0') . ") THEN 1 ELSE 0 END)",
            "I9PendingFlag" => "(CASE WHEN (" . (!empty($I9PendingTechs) ? 'TechID IN (' . implode(",", $I9PendingTechs) . ')' : '1 = 0') . ") THEN 1 ELSE 0 END)",
	    "Country" => "(CASE WHEN (t.Country = '' OR t.Country IS NULL) THEN 'US' ELSE t.Country END)",
	    "ISOTech" => "(CASE WHEN (t.ISO_Affiliation_ID IS NULL OR t.ISO_Affiliation_ID = '' OR t.ISO_Affiliation_ID = 0) THEN 0 ELSE 1 END)",
        );

        $fieldListTech = array(
		"TechID" => "TechID", "LastName" => "LastName", "FirstName" => "FirstName", "Username" => "Username", "PrimaryPhone" => "PrimaryPhone", "SecondaryPhone" => "SecondaryPhone", "SMS_Number" => "SMS_Number", "PrimaryEmail" => "PrimaryEmail", "SecondaryEmail" => "SecondaryEmail", "Address1" => "Address1", "Address2" => "Address2", "City" => "City", "State" => "State", "Zip" => "ZipCode", "Country" => $specialColumns["Country"],
		"PerformancePercent" => "PerformancePercent", "SATRecommendedTotal" => "SATRecommendedTotal", "PreferencePercent" => "PreferencePercent", "SATPerformanceTotal" => "SATPerformanceTotal", "Qty_IMAC_Calls" => $specialColumns["TotalWOs"], "No_Shows" => "No_Shows", "Back_Outs" => "Back_Outs", "BusinessStatement" => "BusinessStatement", "SiteRef" => "SiteRef", "EquipExperience" => "EquipExperience"
	);
        $fieldListISO= array(
		"ISOTech" => $specialColumns["ISOTech"], "ISOContact" => "Contact_Name", "ISOCompanyName" => "Company_Name", "ISOAddress1" => "Address_1", "ISOAddress2" => "Address_2",
		"ISOState" => "State", "ISOCity" => "City", "ISOCountry" => "Country", "ISOPhone" => "Contact_Phone"
	);

/*        $fieldList = array("TechID" => "TechID", "LastName" => "LastName", "FirstName" => "FirstName", "ISO" => "{$specialColumns["ISO"]}", "PrimaryPhone" => "PrimaryPhone", "PrimPhoneType" => "PrimPhoneType", "PrimaryEmail" => "PrimaryEmail", "City" => "City", "State" => "State", "PreferencePercent" => "PreferencePercent", "SATRecommendedTotal" => "SATRecommendedTotal", "Qty_IMAC_Calls" => "Qty_IMAC_Calls", "Qty_FLS_Service_Calls" => "Qty_FLS_Service_Calls", "No_Shows" => "No_Shows", "Back_Outs" => "Back_Outs", "PreferLevel" => "{$specialColumns["PreferLevel"]}", "FS_Cert_Test_Pass" => "FS_Cert_Test_Pass", "Bg_Test_Pass_Lite" => "Bg_Test_Pass_Lite", "Bg_Test_Pass_Full" => "Bg_Test_Pass_Full", "FLSCSP_Rec" => "FLSCSP_Rec", "FLSID" => "FLSID", "BG_Test_ResultsDate_Lite" => "BG_Test_ResultsDate_Lite", "BG_Test_ResultsDate_Full" => "BG_Test_ResultsDate_Full", "DatePassDrug" => "DatePassDrug", "PerformancePercent" => "PerformancePercent", "SATPerformanceTotal" => "SATPerformanceTotal", "DrugPassed" => "DrugPassed", "Zip" => "ZipCode", "TotalWOs" => "{$specialColumns["TotalWOs"]}", "DellMRACompliant" => $specialColumns["DellMRACompliant"], "LapsedDate" => $specialColumns["LapsedDate"]);

        //result page fields name to field list map. Format - "ColumnName" => "Order Returned in $fieldList"
        $resultFieldMap = array("TechID" => "0", "LastName" => "1", "FirstName" => "2", "ISO" => "3", "PrimaryPhone" => "4", "PrimPhoneType" => "5", "PrimaryEmail" => "6", "City" => "7", "State" => "8", "Qty_IMAC_Calls" => "12", "Qty_FLS_Service_Calls" => "13", "No_Shows" => "14", "Back_Outs" => "15", "PreferLevel" => "16", "FS_Cert_Test_Pass" => "17", "Bg_Test_Pass_Lite" => "18", "FLSCSP_Rec" => "19", "FLSID" => "20", "BG_Test_ResultsDate_Lite" => "21", "PreferencePercent" => "10", "SATRecommendedTotal" => "11", "PerformancePercent" => "22", "SATPerformanceTotal" => "23", "DrugPassed" => "24", "Zip" => "9", "TotalWOs" => "26", "DellMRACompliant" => "27", "TotalWOs" => "26", "LapsedDate" => "28");

        // result page columns label to field name / sortable map. Format - "ColumnLabel" => "ColumnName|isSortable (0 = false, 1 = true)
        if ($companyID == "FLS") {
            $resultColumnMap = array("Snap<br/>Shot" => "RatingBox|0",
                "FS-Tech ID#" => "TechID|1",
                "FLS ID" => "FLSID|1",
                "Name" => "LastName|1",
                "Contact Information" => "PrimaryPhone|1",
                "City, State<br />Zip" => "City|1",
                "Miles" => "SpecialDistance|1",
                "Preference<hr>Performance" => "PreferencePercent|1",
                "Total<br/>Work Orders" => "TotalWOs|1",
                "No&nbsp;Shows<hr>Back Outs" => "No_Shows|1",
                "FS-Tags&trade;" => "PreferLevel_USAuthorized|1",
                "Background<hr>Drug Test" => "Bg_Test_Pass_Lite|1",
                "ISO" => "ISO|1");
        } else {
            $resultColumnMap = array("Snap<br/>Shot" => "RatingBox|0",
                "FS-Tech ID#" => "TechID|1",
                "Name" => "LastName|1",
                "Contact Information" => "PrimaryPhone|1",
                "City, State<br />Zip" => "City|1",
                "Miles" => "SpecialDistance|1",
                "Preference<hr class='headerHr'>Performance" => "PreferencePercent|1",
                "Total<br/>Work Orders" => "TotalWOs|1",
                "No&nbsp;Shows<hr class='headerHr'>Back Outs" => "No_Shows|1",
                //"Preferred" => "PreferLevel|1", 
                "FS-Tags&trade;" => "PreferLevel_USAuthorized|1",
                "Background<hr class='headerHr'>Drug Test" => "Bg_Test_Pass_Lite|1",
                "ISO" => "ISO|1");
        }*/

        $quickResult = array();
        $searchResult = array();
        $preferredCriteria = ($showPreferred == true ? "AND t.TechID IN (SELECT Tech_ID FROM client_preferred_techs WHERE CompanyID = '$companyID')" : "");
        $clientDeniedCriteria = "";

        if ($showBanned != "1") {
            $deniedList = Core_Tech::getClientDeniedTechsArray($companyID);
            if (!empty($deniedList))
                $clientDeniedCriteria .= ' AND t.TechID NOT IN (' . implode(',', $deniedList) . ')';
        }

        $userSearchCriteria = implode(" AND ", $searchCriteriaResults["TechBankInfo"]);
        if ($userSearchCriteria != "")
            $userSearchCriteria = "AND " . $userSearchCriteria;


        @$equipmentSearchCriteria = $searchCriteriaResults['equipment'];
        if (sizeof($equipmentSearchCriteria) > 0) {
            $equipSearch = new Core_TechEquipment;
            $equipResult = $equipSearch->getTechsWithEquipment($searchCriteriaResults['equipment']);
            if (!empty($equipResult))
                $equipmentSearchCriteria = ' AND t.TechID IN (' . implode(",", $equipResult) . ')';
            else
                $equipmentSearchCriteria = ' AND t.TechID IN (0)';
        }
        //if($equipmentSearchCriteria != "") $equipmentSearchCriteria = ' AND t.TechID IN (SELECT TechID from tech_equipment WHERE equipment_id IN('.$equipmentSearchCriteria.'))';

        @$skillSearchCriteria = $searchCriteriaResults['skills'];
        if (sizeof($skillSearchCriteria) > 0) {
            $skillSearch = new Core_TechSkills;
            $skillResult = $skillSearch->getTechsWithSkills($skillSearchCriteria);
            if (!empty($skillResult))
                $skillSearchCriteria = ' AND t.TechID IN (' . implode(",", $skillResult) . ')';
            else
                $skillSearchCriteria = ' AND t.TechID IN (0)';
            //$skillsSearchCriteria = ' AND t.TechID IN (SELECT TechID from tech_skills WHERE skill_id IN('.$skillsSearchCriteria.'))';
        }
       $api = new Core_Api_TechClass;
        //--- MDS Search
        $MDSSearchCriteria = "";
        if (!empty($MDS)) {
            $ids = $api->getTechIdsWithTrapolloMedicalCertified();
            if (!empty($ids)) {
                $MDSSearchCriteria.=' AND t.TechID IN(' . $ids . ')';
            } else {
                $MDSSearchCriteria.=' AND 1=0';
            }
        }   
        //--- $Copier_Skills_Assessment Search
        $CopierSkillsAssessmentSearchCriteria = "";
        if (!empty($Copier_Skills_Assessment)) {
            $ids = $api->getTechIds_PassCopierSkills();
            if (!empty($ids)) {
                $CopierSkillsAssessmentSearchCriteria.=' AND t.TechID IN(' . $ids . ')';
            } else {
                $CopierSkillsAssessmentSearchCriteria.=' AND 1=0';
            }
        }
        //--- I9 Search
        //echo("<br/>USAuthorized: $USAuthorized");//test
        $I9SearchCriteria = "";
        $api = new Core_Api_TechClass;
        if (!empty($USAuthorized)) {
            $ids = $api->getTechIdsWithUSAuthorizedOrPending();
            if (!empty($ids)) {
                $I9SearchCriteria.=' AND t.TechID IN(' . $ids . ')';
            } else {
                $I9SearchCriteria.=' AND 1=0';
            }
        }
        @$certSearchCriteria = $searchCriteriaResults['certifications'];
        if (sizeof($certSearchCriteria) > 0) {
            $certSearch = new Core_TechCertifications;
            $certResult = $certSearch->getTechsWithCertification($certSearchCriteria);
            if (!empty($certResult)) {
                $certSearchCriteria = ' AND t.TechID IN (' . implode(",", $certResult) . ')';
            } else {
                $certSearchCriteria = ' AND t.TechID IN (0)';
            }
        }

        // search order
		if($sort=="FS_Tags")
        {
            /*DeVryFlag
TBCFlag
BlueRibbonFlag
*/
            $defaultCriteria = "{$specialColumns["DellMRACompliant"]} DESC, {$specialColumns["I9VerifiedFlag"]} DESC, {$specialColumns["I9PendingFlag"]} DESC, {$specialColumns["DeVryFlag"]} DESC, {$specialColumns["TBCFlag"]} DESC, {$specialColumns["BlueRibbonFlag"]} DESC,{$specialColumns["TotalWOs"]} DESC, TechID DESC";
            $orderCriteria = Core_Tech::getOrderCriteria("", $order, $specialColumns, $defaultCriteria);
        }
        else
        {
	$defaultCriteria = "{$specialColumns["PreferLevel"]} DESC, {$specialColumns["TotalWOs"]} DESC, (CASE WHEN {$specialColumns["TotalWOs"]} = 0 THEN TechID ELSE 0 END) DESC";
        $orderCriteria = Core_Tech::getOrderCriteria($sort, $order, $specialColumns, $defaultCriteria);
        }
        $helpingHandsCriteria = $helpingHandsShow ? "" : "AND HelpingHands = 0";

        $searchCriteria .= "(Private = '0' OR (SELECT Client_ID FROM " . Core_Database::TABLE_ISO . " WHERE UNID = ISO_Affiliation_ID) = '$companyID') AND AcceptTerms = 'Yes' AND Deactivated != '1' $userSearchCriteria $preferredCriteria $clientDeniedCriteria $equipmentSearchCriteria $skillSearchCriteria $certSearchCriteria $helpingHandsCriteria $I9SearchCriteria $MDSSearchCriteria $CopierSkillsAssessmentSearchCriteria";
        $proximitySearch = false;

//        if ($doSearch) {
            // do quick proximity search
            $select = $db->select();
            $select->from(array('t' => Core_Database::TABLE_TECH_BANK_INFO), array("t.TechID", "PrimaryEmail"))
                    ->where($searchCriteria);

            $removeParams = array("module", "dashboard", "controller", "action");
            $sendParams = $params;
            foreach ($sendParams as $k => $v) {
                if (in_array($k, $removeParams)) {
                    unset($sendParams[$k]);
                }
            }

            if (!empty($params["ProximityZipCode"])) {
                $params["ProximityDistance"] = empty($params["ProximityDistance"]) ? 50 : $params["ProximityDistance"];

                @$centerLatLong = array($params["ProximityLat"], $params["ProximityLng"]);
                if (empty($params["ProximityLat"])) {
                    $geocoder = new Core_Geocoding_Google();
                    $geocoder->setZip($params["ProximityZipCode"]);
                    $centerLatLong = array($geocoder->getLat(), $geocoder->getLon());
                }

                $savedProximityInfo = array($centerLatLong[0], $centerLatLong[1], $params["ProximityDistance"]);

                $select = Core_Database::whereProximity($select, $centerLatLong[0], $centerLatLong[1], "Latitude", "Longitude", "{$params["ProximityDistance"]}.49", "Distance");

                $proximitySearch = true;
            }

            $quickResult = $db->fetchAll($select);
            $emailList = array();
            $techList = array();

            if ($quickResult && sizeof($quickResult) > 0) {
                $numResult = sizeof($quickResult);
                foreach ($quickResult as $row) {
                    $techList[] = $row["TechID"];
                    $emailList[] = $row["PrimaryEmail"];
                    $resultsHtml = "<script type='text/javascript'>
                        var emailList = " . json_encode($emailList) . "
                        </script>";
                }
                $quickResult = implode(",", $techList);
            } else {
                $numResult = 0;
                $quickResult = "";
            }

            if ($numResult > 0) {

                // pull in data based on quick search
                $offset = ($resultsPerPage * ($currentResultPage - 1));
                $techsMatchingSearchCriteria = $quickResult;
                $Core_Api_TechClass = new Core_Api_TechClass;

                if (substr($techsMatchingSearchCriteria, "-1") == ",")
                    $techsMatchingSearchCriteria = rtrim($techsMatchingSearchCriteria, ",");

                $select = $db->select();
                $select->from(array("t" => Core_Database::TABLE_TECH_BANK_INFO), $fieldListTech)
		->joinLeft(array("iso" => Core_Database::TABLE_ISO), "t.ISO_Affiliation_ID = iso.UNID", $fieldListISO);
                if (!empty($techsMatchingSearchCriteria))
                    $select->where("TechID IN ($techsMatchingSearchCriteria)");
                $select->order($orderCriteria);
                if ($sort == "LastName")
                    $select->order("FirstName");

                if ($proximitySearch) {
                    $select = Core_Database::whereProximity($select, $savedProximityInfo[0], $savedProximityInfo[1], "Latitude", "Longitude", "{$savedProximityInfo[2]}.49", "Distance");
                }

                $searchResult = $db->fetchAll($select);

                $searchResult = array_slice($searchResult, $offset, $resultsPerPage);
		return $searchResult;
            }

	return false;
//        }//end if doSearch
    }

}


//$filters = array("ProximityZipCode" => 90210, "ProximityDistance" => 50, "v" => "NCR");
//$filters = array("ProximityZipCode" => 90210, "v" => "NCR");
//$filters = array("Country" => 'CA', "ProximityDistance" => 50);
//$filters = array("Country" => 'CA', "ProximityDistance" => 50);
//$filters = array("PrimaryEmail" => "fieldsolutions.com");
//$t = Core_Tech_T::findTechs($filters);
