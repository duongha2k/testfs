<?php
class Core_Admin_Controller_Action extends Zend_Controller_Action
{
    protected $_login;
    protected $_password;

    public function init()
    {
        $request = $this->getRequest();
        /* Index Controller view scripts paths */
        if( strtolower($request->getParam('version', 'full')) === 'lite') {
            $this->view->addScriptPath(ADMINDASHBOARD_MODULE_PATH.'views/lite');
        } elseif( strtolower($request->getParam('version', 'full')) === 'sp') {
            $this->view->addScriptPath(ADMINDASHBOARD_MODULE_PATH.'views/searchpay');
        } else {
            $this->view->addScriptPath(ADMINDASHBOARD_MODULE_PATH.'views/full');
        }

        /*if ( strtolower(trim($request->getParam('company', ''))) === 'fls' ) {
            defined('FLSVersion') || define('FLSVersion', true);
            if( strtolower($request->getParam('version', 'full')) === 'lite') {
                $this->view->addScriptPath(ADMINDASHBOARD_MODULE_PATH.'views/fls_lite');
            } else {
                $this->view->addScriptPath(ADMINDASHBOARD_MODULE_PATH.'views/fls_full');
            }
        }*/
        /* Index Controller view scripts paths */


        //  Check authentication
        $auth = new Zend_Session_Namespace('Auth_User');
        $this->_login = $auth->login;
        $this->_password = $auth->password;

        if ( !$this->_login || !$this->_password ) {
            throw new Core_Auth_Exception('Authentication required');
        }

        //  Check authentication end block


    }
    /**
     * _assembleSortStr
     *
     * @param array $sort
     * @access private
     * @return string
     */
    protected function _assembleSortStr(array $sort)
    {
        $output = array();

        for ( $i = 1; $i <= 3; ++$i ) {
            if ( empty($sort['s'.$i]) || empty($sort['d'.$i]) )
                continue;

            $_derection  = strtolower(trim($sort['d'.$i]));
            $_sort       = strtolower(trim($sort['s'.$i]));

            if ( $_derection === 'asc' )        $_derection = ' ASC';
            else if ( $_derection === 'desc' )  $_derection = ' DESC';
            else                                $_derection = NULL;

            switch ( $_sort ) {
                case 'win'          : $_sort = 'TB_UNID'; break;
                case 'clientid'     : $_sort = 'WO_ID'; break;
                case 'clientpo'     : $_sort = 'PO'; break;
                case 'start'        : $_sort = 'StartDate'; break;
                case 'starttime'    : $_sort = 'StartTime'; break;
                case 'checkin'      : $_sort = 'Date_In'; break;
                case 'hours'        : $_sort = 'calculatedTechHrs'; break;
                case 'timesinceworkdone'        : $_sort = 'TimeSinceWorkDone'; break;
                case 'skill'        : $_sort = 'WO_Category'; break;
                case 'hl'           : $_sort = 'Headline'; break;
                case 'loc'          : $_sort = 'City '.$_derection.', State '.$_derection.', Zipcode'; break;
                case 'city'         : $_sort = 'City'; break;
                case 'st'           : $_sort = 'State'; break;
                case 'state'        : $_sort = 'State'; break;
                case 'project'      : $_sort = 'Project_Name'; break;
                case 'site'         : $_sort = 'SiteName'; break;
                case 'sitenum'      : $_sort = 'SiteNumber'; break;
                case 'region'       : $_sort = 'Region'; break;
                case 'techid'       : $_sort = (defined('FLSVersion')) ? 'FLS_ID' : 'Tech_ID'; break;
                case 'techname'     : $_sort = 'Tech_LName'.$_derection.',Tech_FName'; break;
                case 'techphone'    : $_sort = 'TechPhone'; break;
                case 'techemail'    : $_sort = 'TechEmail'; break;
                case 'bid'          : $_sort = 'Tech_Bid_Amount'.$_derection.', Amount_Per'; break;
                case 'final'        : $_sort = 'PayAmount'; break;
                case 'basis'        : $_sort = 'Amount_Per'; break;
                case 'parts'        : $_sort = 'ReturnPartCount'; break;
                case 'startdate'    : $_sort = 'StartDate'.$_derection.', StartTime'; break;
                case 'enddate'      : $_sort = 'EndDate'.$_derection.', EndTime'; break;
                case 'paymax'       : $_sort = 'PayMax'; break;
                case 'zip'          : $_sort = 'Zipcode'; break;
                case 'loc1'         : $_sort = 'Address';
                case 'loc2'         : $_sort = 'City '.$_derection.', State'; break;
                case 'approved'     : $_sort = 'DateApproved'; break;
                case 'paid'         : $_sort = 'DatePaid'; break;
                case 'netpay'       : $_sort = 'Net_Pay_Amount'; break;
                case 'invoice'      : $_sort = 'DateInvoiced'; break;
                case 'deactcode'    : $_sort = 'DeactivationCode'; break;
                case 'stage'        : $_sort = 'Status'; break;
                case 'accepted'     : $_sort = 'WorkOrderReviewed'; break;
                case 'confirmed'    : $_sort = 'TechCheckedIn_24hrs'; break;
                case 'checkedin'    : $_sort = 'CheckedIn'; break;
                case 'bidcount'     : $_sort = 'Qty_Applicants'; break;
                case 'completed'    : $_sort = 'Date_Completed'; break;
                case 'incompleted'  : $_sort = 'DateIncomplete'; break;
                case 'lead'         : $_sort = 'Lead'; break;
                case 'assist'       : $_sort = 'Assist'; break;
                case 'parts'        : $_sort = 'Parts'; break;
                case 'paperwork'    : $_sort = 'Paperwork_Received'; break;
                case 'deduct'       : $_sort = 'PcntDeduct'; break;
                case 'distance'     : $_sort = 'calculated_distance'; break;
                case 'route'        : $_sort = 'Route'; break;
                case 'clientpo'     : $_sort = 'PO'; break;
                default             : $_sort = NULL;
            }

            $str = $_sort.$_derection;
            if ( $_sort && $_derection && !in_array($str, $output) ) $output[] = $str;
        }
        if ( !$output ) return '';
        else            return join(', ', $output);
    }

    /**
     * _cmpProjects 
     * 
     * This function Used for sorting projects in Filter popup
     *
     * @param API_Project $p1
     * @param API_Project $p2 
     * @static
     * @access private
     * @return int
     */
    static protected function _cmpProjects(API_Project $p1, API_Project $p2)
    {
        return strcmp(strtolower($p1->Project_Name), strtolower($p2->Project_Name));
    }
}

