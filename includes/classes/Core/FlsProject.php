<?php

class Core_FlsProject extends Core_Project
{
    const TABLE_NAME = TABLE_FLS_PROJECTS;
            
    public function setData()
    {
        if (empty($this->fields))
        	$this->fields = Core_Caspio::getTableFieldsList(self::TABLE_NAME);
        
        $criteria = "ProjectNo=$this->id";
        $data = Core_Caspio::caspioSelectAdv(self::TABLE_NAME, $this->fields, $criteria, '');
        if (empty($data[0])) {
            $this->addError('Project No '.$id.' not exists');
        	return false;
        }
        foreach ($data[0] as $k=>$v) {
        	$this->data["$k"] = $v;
        }
        return true;
    }
}