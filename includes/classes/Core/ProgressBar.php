<?php
/**
 *  @singleton
 */
class Core_ProgressBar
{
    static private $_instance;

    private $_handlers = array();
    private $_keyPostfix = '';

    /**
     * getInstance
     * 
     * @static
     * @access public
     * @return Core_ProgressBar
     */
    static public function getInstance()
    {
        if ( !self::$_instance ) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }
    /**
     * __construct
     * 
     * @access private
     * @return void
     */
    private function __construct()
    {
    }
    /**
     * __clone
     * 
     * @access public
     * @return void
     */
    public function __clone()
    {
        trigger_error("Object is singleton, you can't clone it", E_USER_ERROR);
    }
    /**
     * setUserLogin 
     * 
     * Function Make prefix for user's progressbar files, i.e. make unique
     *
     * @param string $login 
     * @access public
     * @return Core_ProgressBar
     */
    public function setUserLogin($login)
    {
        $this->_keyPostfix = crc32($login);
        return $this;
    }
    /**
     * write
     * 
     * @param string $key Wtire's key (filename without extention)
     * @param string $data String for write
     * @access public
     * @return int|false Count of write bytes, or FALSE on error
     */
    public function write($key, $data)
    {
        try {
            $h = $this->getHandler($key);
            flock($h, LOCK_EX);
            ftruncate($h, 0);
            fseek($h, 0, SEEK_SET);
            $write = fwrite($h, $data);
            fflush($h);
            flock($h, LOCK_UN);
        } catch ( Core_ProgressBar_Exception $e ) {
            return false;
        }
        return $write;
    }
    /**
     * read
     * 
     * @param string $key 
     * @access public
     * @return string|false Return string, or FALSE on error
     */
    public function read($key)
    {
        $out = '';
        try {
            $h = $this->getHandler($key);
            flock($h, LOCK_SH);
            fseek($h, 0, SEEK_SET);
            while ( !feof($h) )
                $out .= fread($h, 1024);
            flock($h, LOCK_UN);
        } catch ( Core_ProgressBar_Exception $e ) {
            return false;
        }
        return $out;
    }
    /**
     * getHandler
     * 
     * @param string $key 
     * @access private
     * @return resource
     * @throw Core_ProgressBar_Exception
     */
    private function getHandler($key)
    {
        $key = $this->preventKey($key);
        if ( array_key_exists($key, $this->_handlers) )
            return $this->_handlers[$key];

        $dir    = VAR_PATH.'/processbars/';
        $file   = $dir.$key.'.pbar';
        if ( file_exists($file) && is_writable($file) && is_readable($file) ) {
            if ( false === ($h = fopen($file, "ab+")) ) {
                throw new Core_ProgressBar_Exception('I can\'t open file for write and read');
            }
            $this->_handlers[$key] = $h;
        } else if ( !file_exists($file) ) {
            if ( !file_exists($dir) ) {
                if ( false === @mkdir($dir, 0777, true) ) { // with '@' I don't need check WRITE access for $dir full path
                    throw new Core_ProgressBar_Exception('I can\'t create directory: '.$dir);
                }
            } else if ( !is_writable($dir) || !is_readable($dir) ) {
                throw new Core_ProgressBar_Exception('Directory '.$dir.' is not writable or readable');
            }
            if ( false === ($h = fopen($file, "ab+")) ) {
                throw new Core_ProgressBar_Exception('I can\'t open file for write and read');
            }
            $this->_handlers[$key] = $h;
        } else {
            throw new Core_ProgressBar_Exception('File '.$file.' is not writable or readable');
        }

        return $this->_handlers[$key];
    }
    /**
     * preventKey
     * 
     * @param string $key 
     * @access private
     * @return string
     * @throw Core_ProgressBar_Exception
     */
    private function preventKey($key)
    {
        if ( !is_string($key) )
            throw new Core_ProgressBar_Exception("Key should be a string");
        return strtolower(trim(preg_replace('/[^\w]+/', "_", $key), ' _')).$this->_keyPostfix;
    }
}

