<?php
class Core_Data_Autoformat
{
    private $data         = array();
    private $fieldsFormat = array();
    
    public function __construct(array $data)
    {
        $this->data = $data;
        $this->setFormats();
        return $this;    
    } 
    
    private function setFormats()
    {
        $this->fieldsFormat['PayAmount']                = 'money';
        $this->fieldsFormat['PayMax']                   = 'money';
        $this->fieldsFormat['Tech_Bid_Amount']          = 'money';
        $this->fieldsFormat['OutofScope_Amount ']       = 'money';
        $this->fieldsFormat['Total_Estimated_WO_Value'] = 'money';
        $this->fieldsFormat['calculatedTotalTechPay']   = 'money';
        $this->fieldsFormat['baseTechPay']              = 'money';
        $this->fieldsFormat['TripCharge']               = 'money';
        $this->fieldsFormat['MileageReimbursement']     = 'money';
        $this->fieldsFormat['MaterialsReimbursement']   = 'money';
        $this->fieldsFormat['AbortFeeAmount']           = 'money';

        $this->fieldsFormat['DateEntered']    = 'datetime';
        $this->fieldsFormat['DatePaid']       = 'datetime';
        $this->fieldsFormat['DateApproved']   = 'datetime';
        $this->fieldsFormat['CallDate']       = 'datetime';
        $this->fieldsFormat['Date_Assigned']  = 'datetime';
        $this->fieldsFormat['DateIncomplete'] = 'datetime';
    }

    public function process()
    {
        $curZone = date_default_timezone_get();
        date_default_timezone_set('US/Central');
        foreach ($this->data as $key=>&$val) {
            if (!empty($this->fieldsFormat[$key])) {
                $val = $this->doFormat($val, $this->fieldsFormat[$key]);    
            }
        }
        date_default_timezone_set($curZone); 
        return $this->data;    
    }
    
    public function doFormat($val, $type)
    {
        if ($val === NULL) {
            return $val;    
        }
        
        switch ($type) {
           case 'money':
             $val = round($val, 2);
             $val = sprintf("%0.2f", $val);
             break;
           case 'datetime':
/*             if (!strpos($val, ' ')) {
            	$val = date('m/d/Y h:i:s A', strtotime($val));
             }*/
             break;
        } 
        return $val;   
    }
}