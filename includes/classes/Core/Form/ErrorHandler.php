<?php
class Core_Form_ErrorHandler
{
    const SAVE_PATH = 'var/form/errors/';

    private $_id;
    private $_messages = array();
    private $_fullpath;
    private $_handler;

    public function __construct( $id )
    {
        $id = $this->_prepareId($id);
        if ( !$this->_isIdValid($id) ) {
            trigger_error('Parameter $id is required and should be a string', E_USER_NOTICE);
            return;
        }
        $this->_id = $id;
        if ( $this->_makeStorage() ) {
            $this->_openFile();
            $this->_readMessages();
        }
    }

    public function __destruct()
    {
        $this->_saveMessages();
        $this->_closeFile();
    }

    public function addMessage( $msg )
    {
        $this->_messages[] = $msg;
        return $this;
    }

    public function clearMessages()
    {
        $this->_messages = array();
        return true;
    }

    public function count()
    {
        return sizeof($this->_messages);
    }

    public function hasMessages()
    {
        return ($this->count() > 0);
    }

    public function getMessages()
    {
        return $this->_messages;
    }

    private function _prepareId( $id )
    {
        if ( !is_string($id) )
            return $id;
        return trim(preg_replace( '/[^-_a-z0-9]+/i', '_', $id ), '_');
    }

    private function _isIdValid( $id )
    {
        if ( empty($id) || !is_string($id) || preg_match('/[^-_a-z0-9]/i', $id) ) {
            return false;
        }
        return true;
    }

    private function _makeStorage()
    {
        $this->_fullpath = APPLICATION_PATH .'/../'. self::SAVE_PATH;
        if ( !file_exists($this->_fullpath) ) {
            if ( !@mkdir( $this->_fullpath, 0777, true ) ) {
                trigger_error('Directory '.$this->_fullpath.' cannot be created', E_USER_NOTICE);
                return false;
            }
        } else if ( !is_writable($this->_fullpath) ) {
            trigger_error('Directory '.$this->_fullpath.' is not writable', E_USER_NOTICE);
            return false;
        }
        $this->_fullpath = realpath($this->_fullpath).'/messages_'.$this->_id;
        
        return true;
    }

    private function _readMessages()
    {
        if ( $this->_openFile() ) {
            flock($this->_handler, LOCK_SH);
            rewind($this->_handler);
            $messages = '';
            while ( !feof($this->_handler) ) {
                $messages .= fread($this->_handler, 4096);
            }
            flock($this->_handler, LOCK_UN);

            if ( strlen($messages) ) {
                $this->_messages = unserialize($messages);
            }
        }

        return true;
    }

    private function _saveMessages()
    {
        if ( $this->_openFile() ) {
            if ( $this->hasMessages() ) {
                $messages = serialize($this->_messages);

                flock($this->_handler, LOCK_EX);
                fflush($this->_handler);
                ftruncate($this->_handler, 0);
                rewind($this->_handler);
                fwrite($this->_handler, $messages, strlen($messages));
                fflush($this->_handler);
                flock($this->_handler, LOCK_UN);
            } else {
                flock($this->_handler, LOCK_EX);
                fflush($this->_handler);
                ftruncate($this->_handler, 0);
                rewind($this->_handler);
                flock($this->_handler, LOCK_UN);
            }
        }
    }

    private function _openFile()
    {
        if ( !is_resource($this->_handler) ) {
            if ( FALSE === ($this->_handler = fopen($this->_fullpath, 'a+')) ) {
                trigger_error('File '.$this->_fullpath.' can\'t be opend for read/write', E_USER_NOTICE);
                return false;
            }
        }
        return true;
    }

    private function _closeFile()
    {
        if ( is_resource($this->_handler) ) {
            fclose($this->_handler);
        }

        if ( is_file($this->_fullpath) && file_exists($this->_fullpath) && filesize($this->_fullpath) === 0 ) {
            unlink($this->_fullpath);
        }

        return true;
    }
}

