<?php

class Core_Project
{
    const TABLE_NAME = TABLE_CLIENT_PROJECTS;

    protected $id;
    protected $data = array();
    protected $errors = array();
    protected $fields = '';
    
    function __construct($id = null)
    {
       	$this->id = (int)$id;
    }

    public function getId() {
       return $this->id;
    }
    
    public function setFields($fields)
    {
        $this->fields = $fields;
    }
    
    public function setData()
    {
		if (empty($this->id)) return true;
        if (empty($this->fields))
        	$this->fields = Core_Database::getFieldList(Core_Database::TABLE_PROJECTS);
		else {
			$this->fields = explode(",", $this->fields);
		}
        
//        $criteria = "Project_ID=$this->id";
//        $data = Core_Caspio::caspioSelectAdv(self::TABLE_NAME, $this->fields, $criteria, '');
				
		$db = Core_Database::getInstance();
		$select = $db->select();
		$select->from(Core_Database::TABLE_PROJECTS, $this->fields);
		$select->where("Project_ID = ?", $this->id);
		$select->limit(1, 0);

		$data = Core_Database::fetchAll($select);
		
        if (count($data) == 0) {
            $this->addError('Project ID '.$this->id.' not exists');
        	return false;
        }
       	$this->data = $data[0];

		$fieldListType = Core_Database::getFieldListType(Core_Database::TABLE_PROJECTS);

		$this->data = Core_Database::convertQueryResult($this->data, $fieldListType);

        return true;
    }
    
    public function __call($name, $params)
    {
        if (substr($name,0,3) == 'get') {
		if (empty($this->id)) return "";
            $fieldName = substr($name,3);
            if (isset($this->data["$fieldName"])) {
            	//$futureFields = array('From_Email', 'To_Email');
            	//if(in_array($fieldName,$futureFields)){
            	//	$this->overwriteWithFutureData($fieldName);
            	//}
            	return $this->data[$fieldName];
            } else {
                //throw new Exception();
            }
        }
        /*
        if (substr($name,0,3) == 'set') {
            $fieldName = $this->data[substr($name,3,0)];
            $this->data[$fieldName] = $params[0];
        }
        */
    }
    
    public function save()
    {
        
    }
    
    protected function addError($descr)
    {
        $this->errors[] = $descr;
    }
    
    public function getErrors()
    {
        return empty($this->errors)?null:$this->errors;
    }
}
