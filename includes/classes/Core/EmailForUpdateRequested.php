<?php
class Core_EmailForUpdateRequested {
    private $wo;
    private $mail;
	private $msg;

    function __construct()
    {
        $this->mail = new Core_Mail();
		$this->msg = "";
    }
	
    public function setWOData($wo) {
        $this->wo = $wo;
    }
	            
	private function buildMessage()
    {
		if (!isset($this->wo[0])) {
			$this->wo = array($this->wo);
		}
		
		foreach ($this->wo as $wo) {
			$msg = "";
			$date = $wo['DateTime_Stamp'];
			if (!empty($date)) {
				try {
					$z = new Zend_Date($date, Zend_Date::ISO_8601);
					$date = $z->toString('MM/dd/yyyy');
				} catch (Exception $e) {
				}
			}
			else {
				$date = $wo["lastModDate"];
			}
	
			$msg .= "Date request created: $date\n";
			$msg .= "Start Date: {$wo['StartDate']}\n";
			$msg .= "Client Work Order: {$wo['WO_ID']}\n";
			$msg .= "Project: {$wo['Project_Name']}\n";
			$msg .= "Reason for update: {$wo['Update_Reason']}\n\n";
			
			$this->msg .= $msg;
		}
    }
	
	public function sendEmail() {
		if (empty($this->msg)) $this->buildMessage();
	    	$this->mail->setBodyText($this->msg);
	        $this->mail->setFromEmail('no-replies@fieldsolutions.com');
	        $this->mail->setFromName('FieldSolutions');
	        $this->mail->setToEmail("ftxs.updaterequest@fieldsolutions.com");
	        $this->mail->setSubject('FLS Update Requested');

	        $this->mail->send();
	}
}