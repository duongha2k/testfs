<?php
/**
 * @desc Adapter for static results pagination
 * @author Pavel Shutin
 */
class Core_Paginator_Adapter_Static implements Zend_Paginator_Adapter_Interface
{
    /**
     * Array
     *
     * @var array
     */
    protected $_array = null;

    /**
     * Item count
     *
     * @var integer
     */
    protected $_count = null;

    /**
     * Constructor.
     *
     * @param array $array Current page results
     * @param integer $total_count Total results count
     */
    public function __construct(array $data, $total_count = 0)
    {
        $this->_array = $data;
        $this->_count = ($total_count<count($data))?count($data):$total_count;
    }

    /**
     * Returns an array of items for a page.
     *
     * @param  integer $offset NOT USED
     * @param  integer $itemCountPerPage NOT USED
     * @return array
     */
    public function getItems($offset, $itemCountPerPage)
    {
        return $this->_array;
    }

    /**
     * Returns the total number of rows in the array.
     *
     * @return integer
     */
    public function count()
    {
        return $this->_count;
    }
}