<?php
class Core_State extends Core_LookupTable {
	
	public function __construct($state = NULL, $country = NULL) {
		$_country = new Core_Country($country);
		$country = $_country->getCode();
		self::getByCode($country);
		$this->aliasTableName = 'alias_state';
		$alias = $this->getCodeByAlias("$state|$country");
		if (!empty($alias)) $state = $alias;
		parent::__construct($state);
	}
		
	public static function getByCode($country = NULL) {
		$cache = Core_Cache_Manager::factory();
		$key = 'statesList' . $country . 'ById';
		$cacheKey = Core_Cache_Manager::makeKey($key);
		if (empty($country)) $country = 'US';
        
		if ( false === ($result = $cache->load($cacheKey)) ) {
			$fieldsList = array('Abbreviation', 'State');

			$result = self::getPair($fieldsList, $country);

			$cache->save($result, $cacheKey, array($key), LIFETIME_1DAY);
		}
	
		self::$lookupTable = $result;
		return $result;
	}

	public static function getById($country = NULL) {
		$cache = Core_Cache_Manager::factory();
		$key = 'statesList' . $country . 'ByRealId';
		$cacheKey = Core_Cache_Manager::makeKey($key);
		if (empty($country)) $country = 'US';
        
		if ( false === ($result = $cache->load($cacheKey)) ) {
			$fieldsList = array('id', 'State');

			$result = self::getPair($fieldsList, $country);
			$cache->save($result, $cacheKey, array($key), LIFETIME_1DAY);
		}
	
//		self::$lookupTable = $result;
		return $result;
	}

	private static function getPair($fieldsList, $country) {
		$db = Core_Database::getInstance();
		$select = $db->select();
		$_country = new Core_Country($country);
		$country = $_country->getCode();
		$select->from(Core_Database::TABLE_STATES_LIST, $fieldsList)
			->order('State ASC')
			->where("Country = ?", array($country));
			
		return $db->fetchPairs($select);
	}
}
