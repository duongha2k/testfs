<?php
/**
 * @author Sergey Petkevich
 * The class used for the mapping service
 */

class Core_Map
{
    public static function getColorMarkers()
    {
        $res = array();
        $res['blue']   = 'Blue';
        $res['green']  = 'Green';
        //$res['grey']   = 'Grey';
        $res['red']    = 'Red';
        $res['yellow'] = 'Yellow';

        return $res;
    }

    public static function getTechMarkerUrl($colorId, $type)
    {
        $url = '../../images/map/';
        switch ($colorId) {
          case 'blue':
            $url .= "tech_marker_blue_$type.png";
            break;
          case 'green':
            $url .= "tech_marker_green_$type.png";
            break;
          case 'grey':
            $url .= "tech_marker_grey_$type.png";
            break;
          case 'red':
            $url .= "tech_marker_red_$type.png";
            break;
          case 'yellow':
            $url .= "tech_marker_yellow_$type.png";
            break;
          default:
          $url .= 'tech_marker_grey_one.png';
            break;
        }

        return $url;
    }
    
    public static function getWoMarkerUrl($colorId, $type)
    {
        $url = '../../images/map/';
        switch ($colorId) {
          case 'blue':
            $url .= "wo_marker_blue_$type.png";
            break;
          case 'green':
            $url .= "wo_marker_green_$type.png";
            break;
          case 'grey':
            $url .= "wo_marker_grey_$type.png";
            break;
          case 'red':
            $url .= "wo_marker_red_$type.png";
            break;
          case 'yellow':
            $url .= "wo_marker_yellow_$type.png";
            break;
          default:
          $url .= 'wo_marker_grey_one.png';
            break;
        }

        return $url;
    }
    
    public static function getTechClusterUrl($colorId)
    {
        $url = '../../images/map/';
        switch ($colorId) {
          case 'blue':
            $url .= "tech_cluster_blue.png";
            break;
          case 'green':
            $url .= "tech_cluster_green.png";
            break;
          case 'grey':
            $url .= "tech_cluster_grey.png";
            break;
          case 'red':
            $url .= "tech_cluster_red.png";
            break;
          case 'yellow':
            $url .= "tech_cluster_yellow.png";
            break;
         default:
          $url .= 'tech_cluster_grey.png';
          break;
        }

        return $url;
    }
    
    public static function getWoClusterUrl($colorId)
    {
        $url = '../../images/map/';
        switch ($colorId) {
          case 'blue':
            $url .= "wo_cluster_blue.png";
            break;
          case 'green':
            $url .= "wo_cluster_green.png";
            break;
          case 'grey':
            $url .= "wo_cluster_grey.png";
            break;
          case 'red':
            $url .= "wo_cluster_red.png";
            break;
          case 'yellow':
            $url .= "wo_cluster_yellow.png";
            break;
         default:
          $url .= 'wo_cluster_grey.png';
          break;
        }

        return $url;
    }
    
    public static function addUsedColorToSession($color){
    	$colors = self::getColorMarkers();
	    $_SESSION['usedColors'][$color] = $colors[$color];
    }
    
    public static function delUsedColorFromSession($color){
    	if (isset($_SESSION['usedColors'][$color])) {
	    	unset($_SESSION['usedColors'][$color]);
	    }
    }
}

?>