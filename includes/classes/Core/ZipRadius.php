<?php
class Core_ZipRadius extends Core_LookupTable {
	
	public function __construct() {
		self::getByCode();
		parent::__construct();
	}
		
	public static function getByCode() {
        $cache = Core_Cache_Manager::factory();
        $cacheKey = Core_Cache_Manager::makeKey('zipRadius');
        
        if ( false === ($result = $cache->load($cacheKey)) ) {
            $fieldsList = array('radius','label');

            $db = Core_Database::getInstance();
            $select = $db->select();

            $select->from(Core_Database::TABLE_ZIP_RADIUS, $fieldsList);
			
            $result = $db->fetchPairs($select);

            $cache->save($result, $cacheKey, array('zipRadius'), LIFETIME_30DAYS);
        }
		
		self::$lookupTable = $result;
		return $result;
	}
}