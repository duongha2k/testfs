<?php
abstract class Core_TableRelatedItem {
	protected $table;
	protected $relatedField;
	protected $itemField;
	protected $additionalFields; // additional fields returned when using getItemsWithRelatedIds
	protected $additionalCriteria; // additional criteria which apply on all searches
	protected $returnAs;
	
	public function __construct($table, $relatedField, $itemField, $additionalFields = NULL, $additionalCriteria = NULL, $returnAs = NULL) {
		$this->table = $table;
		$this->relatedField = $relatedField;
		$this->itemField = $itemField;
		if (!is_array($additionalFields)) $additionalFields = array();
		$this->additionalFields = $additionalFields;
		if (!is_array($additionalCriteria)) $additionalCriteria = array();
		$this->additionalCriteria = $additionalCriteria;
		if (is_string($returnAs) && class_exists($returnAs)) {
			$interfaces = class_implements($returnAs);
			if (array_key_exists("Core_ObjectFromArray", $interfaces))
				$this->returnAs = $returnAs;
	}
	}
			
	private function getPairs($field, $ids, $and = true, $additionalCriteria = NULL) {
		if (empty($ids)) return array();
		$db = Core_Database::getInstance();
		$select = $db->select();
		$allItem = $and && $field == $this->itemField;
		$returnAdditional = false;
		if ($allItem)
			$fieldList = array($this->relatedField, $this->itemField, new Zend_Db_Expr("COUNT($this->itemField)"));
		else {
			$fieldList = array($this->relatedField, $this->itemField);
			if (!empty($this->additionalFields)) {
				$fieldList = array_merge($fieldList, $this->additionalFields);
				$returnAdditional = true;
			}
		}
		$select->from($this->table, $fieldList);
		$select->where($field . " IN (?)", $ids);
		$select->order(array($this->relatedField, $this->itemField));
		if ($allItem) {
			$select->group($this->relatedField)->having("COUNT($this->itemField) = " . sizeof($ids));
		}
		if (!empty($this->additionalCriteria)) {
			foreach ($this->additionalCriteria as $crit) {
				$select->where($crit);
			}
		}
		if (!empty($additionalCriteria)) {
			foreach ($additionalCriteria as $crit) {
				$select->where($crit);
			}
		}
	$pairs = $db->fetchAll($select);
		$result = array();
		if (!$pairs || sizeof($pairs) == 0) return $result;
		foreach ($pairs as $info) {
			$id = $info[$this->relatedField];
			if (!array_key_exists($id, $result)) $result[$id] = array();
			if ($field != $this->relatedField) {
				$result[$id] = 1;
				continue;
		}
			if (empty($this->returnAs))
				$result[$id][$info[$this->itemField]] = !$returnAdditional ? $info[$this->itemField] : $info;
			else {
				$ret = $this->returnAs;
				//$result[$id][$info[$this->itemField]] = $ret::objectFromArray($info);
				$result[$id][$info[$this->itemField]] = call_user_func(array($ret, "objectFromArray"), $info);
			}
		}
		return $result;
	}
		
	protected function getItemsWithRelatedIds($relatedIds) {
		// returns array of items matching related ids. ex. return list of skill for each tech id
		$result = $this->getPairs($this->relatedField, $relatedIds);
		return $result;
	}
	
	protected function getRelatedIdsWithItems($itemIds, $and = true, $additionalCriteria = NULL) {
		// returns array of related ids matching items. ex. return list of tech id that have all items (and = true) or any item (and = false)
		$result = $this->getPairs($this->itemField, $itemIds, $and, $additionalCriteria);
		return array_keys($result);
	}
}
