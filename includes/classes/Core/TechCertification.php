<?php
class Core_TechCertification implements Core_ObjectFromArray {
	protected $TechID;
	protected $certification_id;
	protected $number;
	protected $date;
	protected $dateExpired;
	protected static $mapping;
	protected $row;
	
	public function __construct($techID = NULL, $certification_id = NULL, $number = NULL, $date = NULL, $dateExpired = NULL) {
		$this->TechID = $techID;
		$this->certification_id = $certification_id;
		$this->number = $number;
		if (!empty($date))
			$this->date = new Zend_Date($date, 'YYYY-MM-dd');
		if (!empty($dateExpired))
			$this->dateExpired = new Zend_Date($dateExpired, 'YYYY-mm-dd');
		self::init();
	}
	
	private function load() {
		$db = Core_Database::getInstance();
		$table = new Zend_Db_Table(Core_Database::TABLE_TECH_CERTIFICATION);
		$this->row = $table->fetchRow($table->select(Zend_Db_Table::SELECT_WITH_FROM_PART)
			->where("TechID = ?", $this->TechID)
			->where("certification_id = ?", $this->certification_id));
	}
	
        public function loadRow() {
                $this->load();
		return $this->row;
	}
	
	private function prepareRow() {
		$this->load();
		$db = Core_Database::getInstance();
		$table = new Zend_Db_Table(Core_Database::TABLE_TECH_CERTIFICATION);
		if (empty($this->row)) $this->row = $table->createRow();
		$this->row->TechID = $this->TechID;
		$this->row->certification_id = $this->certification_id;
		$this->row->number = $this->number;
		if (!empty($this->date)) 
			$this->row->date = $this->date->toString('YYYY-MM-dd');
		else
			$this->row->date = NULL;
		if (!empty($this->dateExpired)) 
			$this->row->dateExpired = $this->dateExpired->toString('YYYY-MM-dd');
		else
			$this->row->dateExpired = NULL;
	}

	public function save() {
		$this->prepareRow();
		try {
			$this->row->save();
		}
		catch (Exception $e) {
		}
	}
	
	public function delete() {
		if (empty($this->row)) $this->load();
		if (empty($this->row)) return false;
		try {
			$this->row->delete();
		}
		catch (Exception $e) {
		}
	}

	public static function init() {
		if (!empty(self::$mapping)) return;
		self::$mapping = Core_TechCertifications::getMapping();
	}
	
	public function getTechID() {
		return $this->TechID;
	}

	public function getCertificationId() {
		return $this->certification_id;
	}
	
	public function getName() {
		self::init();
		if (!array_key_exists($this->certification_id, self::$mapping)) return "";
		return self::$mapping[$this->certification_id]['name'];
	}

	public function getNumber() {
		return $this->number;
	}

	public function getDate() {
		return $this->date;
	}

	public function getDateExpired() {
		return $this->dateExpired;
	}
	
	public static function objectFromArray($array) {
		@$techID = $array["TechID"];
		@$certification_id = $array["certification_id"];
		@$number = $array["number"];
		@$date = $array["date"];
		@$dateExpired = $array["dateExpired"];
		$me = new self($techID, $certification_id, $number, $date, $dateExpired);
		return $me;
	}
    
    public function getRow()
    {
        $db = Core_Database::getInstance();
        $query = $db->select();        
        $query->from(Core_Database::TABLE_TECH_CERTIFICATION);                
        $query->where("TechID = ?", $this->TechID);        
        $query->where("certification_id = ?", $this->certification_id);        
        $result = Core_Database::fetchAll($query);
        
        if(empty($result)) return null;
        else return $result[0];
    }
}