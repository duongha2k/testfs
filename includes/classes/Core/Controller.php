<?php

class Core_Controller extends Zend_Controller_Action
{
    protected $smarty;

    public function init()
    {
        $this->smarty = Zend_Registry::get('smarty');
    }
}