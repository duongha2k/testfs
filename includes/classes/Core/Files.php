<?php
class Core_Files extends Zend_Db_Table_Abstract
{
    protected $_name = 'files';
    const WOS_FILE = 1;
    const TECH_FILE = 2;
	const SIGNOFF_FILE = 3;
    const WOS_PROJECT_FILE = 4;

	public function getFileIDsByWinsAndType($wins, $type = self::WOS_FILE) {
		if (empty($wins) || !is_array($wins)) return false;
	        $select = $this->select()
	                ->from($this, array('id'))
			->where('type = ?', $type)
	                ->where('WIN_NUM IN (?)', $wins);
		$r = $this->fetchAll($select);
		if (!empty($r))
		        return $r->toArray();
		return array();
	}

    /**
     * remove files by IDs
     * @param <int> $win
     * @param <array> $files
     * @return bollean
     * @author Alex Scherba
     */
    public function removeFiles($win, $files){
        if(empty($files) || !is_array($files)) return false;
        $select = $this->select()
                ->from($this, array('path', 'WMFileID'))
                ->where('id IN (?) AND WIN_NUM ='.$win, $files);
        $filesNames = $this->fetchAll($select);
        $responce = $filesNames->toArray();
        $where = $this->getAdapter()->quoteInto('id IN (?) AND WIN_NUM ='.$win, $files);
        $del = $this->delete($where);
        if($del) {
            $fileRemoved = array();
            foreach ($responce as $item) {
                $fileRemoved[] = $item['path'];
				if (empty($item['WMFileID'])) continue;
				Core_WorkMarket::removeFileFromWMAssignment($item['WMFileID']);
            }
			$select = $this->select()
                ->from($this, array('path', 'WMFileID'))
				->where('path IN (?)', $fileRemoved);
			$responce = $this->fetchAll($select)->toArray();
			$doNotDelete = array();
			foreach ($responce as $item) {
				$doNotDelete[] = $item['path'];
			}
			if (!empty($doNotDelete) && sizeof($doNotDelete) > 0) {
				$newFilesNames = array();
				foreach ($filesNames as $item) {
					if (in_array($item['path'],$doNotDelete)) continue;
					$newFilesNames[] = $item;
				}
				$filesNames = $newFilesNames;
			}
            $files = new Core_File();
            $res = $files->removeFiles($filesNames, S3_CLIENTS_DOCS_DIR);
            Core_TimeStamp::createTimeStamp($win, $_SESSION['UserName'], "Work Order Updated: Files " . implode(' , ', $fileRemoved) ." was removed.", '', '', '', "", "", "");
            return $res;
            
        }
        return false;
    }

    /**
     * remove files by IDs
     * @param <int> $win
     * @param <int> $type
     * @return boolean
     * @author Alex Scherba
     */
    public function getFilesCount($win = null, $type = null){
		if (is_array ($win)) {
			$result = array ();
			$subquery = "SELECT COUNT(*) FROM files f WHERE f.WIN_NUM = files.WIN_NUM";

			foreach ($win as $key) {
				$result[$key] = 0;
			}

			if (!empty ($type)) $subquery .= " AND type = " . $type;

			$select = $this->select ();
			$select->from ($this, array ("WIN_NUM" => "WIN_NUM", "c" => "($subquery)"));
			$select->where('WIN_NUM IN (?)', $win);

			if(!empty($type)) $select->where ('type = ?', $type);

			$records = $this->fetchAll($select);

			foreach ($records as $record) {
				$result[$record["WIN_NUM"]] = $record["c"];
			}

			return $result;
		}
		else {
			$select = $this->select()
					->from($this, array('c'=>'count(id)'));
			if(!empty($win))
				$select->where('WIN_NUM = ?', $win);
			if(!empty($type))
				$select->where ('type = ?', $type);

			$c = $this->fetchRow($select);
			return $c->c;
		}
    }

	public function getFilesCountWithWOID($win = null, $type = null){
		if (is_array ($win)) {
			$db = Core_Database::getInstance();
			$win = implode (",", $win);
			$query = "SELECT DISTINCT f.WIN_NUM, w.WO_ID, " . 
				"(SELECT COUNT(*) FROM files f2 WHERE WIN_NUM = f.WIN_NUM) AS file_count " . 
				"FROM files f " . 
				"INNER JOIN work_orders w ON w.WIN_NUM = f.WIN_NUM " .
				"WHERE f.WIN_NUM IN ($win) AND " . 
				"type = $type";
			$select = $db->query ($query);
			$records = $select->fetchAll ();

			return $records;
		}
		else {
			$select = $this->select()
					->from($this, array('c'=>'count(id)'));
			if(!empty($win))
				$select->where('WIN_NUM = ?', $win);
			if(!empty($type))
				$select->where ('type = ?', $type);

			$c = $this->fetchRow($select);
			return $c->c;
		}
	}

    public function isThereTechDoc($win)
    {
        $files = new Core_Files();
        $select = $files->select();
        $select->where("WIN_NUM = '$win'");
        $select->where("type = 2");
        $filesList = $files->fetchAll($select);
        if(!empty($filesList)) return true;
        else return false;
    }
    
    //--- 13819
    public function getTechDocList($winArray)
    {
        return $this->getDocList($winArray,self::TECH_FILE);
    }
    //--- 13819 
    public function getDocList($winArray,$type)
    {
        $db = Core_Database::getInstance();
        $select = $db->select();
        $select->from(Core_Database::TABLE_FILES);  
        $select->where("WIN_NUM IN (?)",$winArray);
        $select->where("type = '$type'");           
        $records = Core_Database::fetchAll($select);
        return $records;
    }
	
	public function getProcessedWMFileID($ids) {
        $db = Core_Database::getInstance();
        $select = $db->select();
        $select->from(Core_Database::TABLE_FILES, array('WMFileID'));  
        $select->where("WMFileID IN (?)",$ids);
        $records = $db->fetchCol($select);
        return $records;
	}	
}

