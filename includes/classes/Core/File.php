<?php
class Core_File extends Zend_Service_Amazon_S3
{
    public function  __construct() {
        parent::__construct(S3_ACCESS_KEY, S3_SICRET_KEY);
    }
    
    /**
     * Upload file to S3
     * @param string $fileName
     * @param string $data
     * @param string $bucketName
     * @param bool $permissionPublic set public permission to file
     * @return boolean
     */
    public function uploadFile($fileName, $data, $bucketName, $permissionPublic = false) {
        try {
            if ($permissionPublic) {
                $permissions = array(Zend_Service_Amazon_S3::S3_ACL_HEADER=>Zend_Service_Amazon_S3::S3_ACL_PUBLIC_READ);
            } else {
                $permissions = array(Zend_Service_Amazon_S3::S3_ACL_HEADER=>Zend_Service_Amazon_S3::S3_ACL_PRIVATE);
            }
            return parent::putObject($bucketName."/".$fileName, $data, $permissions);
        } catch (Zend_Service_Amazon_S3_Exception $s3e) {
            $errors = Core_Api_Error::getInstance();
            $errors->addError(1, $s3e->getMessage());
        }
        return false;
    }

    public function downloadFile($fileName,$bucketName) {
        try {
            
            return parent::getObject($bucketName."/".$fileName);
        } catch (Zend_Service_Amazon_S3_Exception $s3e) {
            $errors = Core_Api_Error::getInstance();
            $errors->addError(1, $s3e->getCode());
        }
        return false;
    }

    public function getFileListFormBucket($bucketName) {
        try {
            
            return parent::getObjectsByBucket($bucketName);
        } catch (Zend_Service_Amazon_S3_Exception $s3e) {
            $errors = Core_Api_Error::getInstance();
            $errors->addError(1, $s3e->getMessage());
        }
        return false;
    }
    public function createBucket($bucketName, $location = NULL) {
        try {
            
            return parent::createBucket($bucketName);
        } catch (Zend_Service_Amazon_S3_Exception $s3e) {
            $errors = Core_Api_Error::getInstance();
            $errors->addError(1, $s3e->getMessage());
        }
        return false;
    }
    public function getBuckets() {
        try {
            
            return parent::getBuckets();
        } catch (Zend_Service_Amazon_S3_Exception $s3e) {
            $errors = Core_Api_Error::getInstance();
            $errors->addError(1, $s3e->getMessage());
        }
        return false;
    }

    public function removeFile($fileName,$bucketName) {
        try {
            
            return parent::removeObject($bucketName."/".$fileName);
        } catch (Zend_Service_Amazon_S3_Exception $s3e) {
            $errors = Core_Api_Error::getInstance();
            $errors->addError(1, $s3e->getMessage());
        }
        return false;
    }
    public function removeFiles($files,$bucketName) {
        $errors = Core_Api_Error::getInstance();
        try {
            
            $flag = true;
            foreach($files as $file){
                if(!parent::removeObject($bucketName."/".ltrim(trim($file->path),'/'))){
                    $flag = false;
                    $errors->addError(1, 'File '.$fileName.' didn\'t remove from S3');
                }
            }
            return $flag;
        } catch (Zend_Service_Amazon_S3_Exception $s3e) {
            $errors->addError(1, $s3e->getMessage());
        }
        return false;
    }
    public function fileExists($fileName,$bucketName) {
        try {
            return parent::isObjectAvailable($bucketName.$fileName);
        } catch (Zend_Service_Amazon_S3_Exception $s3e) {
            $errors = Core_Api_Error::getInstance();
            $errors->addError(1, $s3e->getMessage());
        }
        return false;
    }

	public function getInfo ($fileName, $bucketName) {
        try {
            return parent::getInfo ($bucketName.$fileName);
        } catch (Zend_Service_Amazon_S3_Exception $s3e) {
            $errors = Core_Api_Error::getInstance();
            $errors->addError(1, $s3e->getMessage());
        }
        return false;
    }

	public function getS3Url($fileName, $bucketName, $expiration = NULL) {
		// generates a preauthenticated link for an s3 file. expiration is unix timestamp
		$s3_url = "https://s3.amazonaws.com";
		if (empty($expiration)) $exp = strtotime("+10 year");
		else $exp = $expiration;
		$path = '/' . $bucketName . '/' . urlencode($fileName);
		$strtosign = "GET\n\n\n$exp\n$path";
		$signature = urlencode(base64_encode(hash_hmac("sha1", utf8_encode($strtosign), S3_SICRET_KEY, true)));
		$final_url = $s3_url . $path . "?AWSAccessKeyId=" . S3_ACCESS_KEY . "&Signature=$signature&Expires=$exp";
		return $final_url;
	}

}
