<?php 

require_once APPLICATION_PATH . '/modules/dashboard/views/helpers/DisplayDate.php';
require_once APPLICATION_PATH . '/modules/dashboard/views/helpers/ProjectName.php';
require_once APPLICATION_PATH . '/modules/dashboard/views/helpers/DisplayBid.php';
require_once APPLICATION_PATH . '/modules/dashboard/views/helpers/DisplayMoney.php';
require_once APPLICATION_PATH . '/modules/dashboard/views/helpers/DisplayAmountPer.php';
require_once APPLICATION_PATH . '/modules/dashboard/views/helpers/DisplayMoneyPer.php';
require_once APPLICATION_PATH . '/modules/dashboard/views/helpers/DisplayBidPer.php';

class Core_CSV_Mapper
{
	private $_techs;
    private $_workOrders;
    private $_projectMapping;
    
    public function __construct($wos, $techs, $projectMapping, $mappedAroundWos){
    	$this->_workOrders = $wos;
    	$this->_techs = $techs;
    	$this->_projectMapping = $projectMapping;
    	$this->_mappedAroundWos = $mappedAroundWos;
    	
    	 $this->_helpers = array();

        $this->_helpers['displayDate'] = new Dashboard_View_Helper_DisplayDate();
        $this->_helpers['projectName'] = new Dashboard_View_Helper_ProjectName();
        $this->_helpers['displayBid'] = new Dashboard_View_Helper_DisplayBid();
        $this->_helpers['displayMoney'] = new Dashboard_View_Helper_DisplayMoney();
        $this->_helpers['displayAmountPer'] = new Dashboard_View_Helper_DisplayAmountPer();
        $this->_helpers['displayMoneyPer'] = new Dashboard_View_Helper_displayMoneyPer();
        $this->_helpers['displayBidPer'] = new Dashboard_View_Helper_DisplayBidPer();
    }
	
	public function printCSV()
	{
    	ob_start();
    	//$this->putcsv($this->getWoCsvTitle());
    	ob_flush();
    	flush();

        $offset = 0;
        $limit = 500;
        if ($this->_projectMapping === true) {
        	
        	foreach ($this->_workOrders as $wo) {
        		$this->putcsv(array("Work Order"));
        		$this->putcsv($this->getWoCsvTitle());
            	$this->putcsv($this->makeWoDataLine($wo));
            		
            	$this->putcsv(array(""));
            	$this->putcsv(array("Techs"));
    			$this->putcsv($this->getTechCsvTitle());
    				
          		foreach ($this->_techs as $tech) {
            		$this->putcsv($this->makeTechDataLine($tech));
    			}
    			$this->putcsv(array(""));
    			$this->putcsv(array(""));
    		}
    		
          }elseif($this->_mappedAroundWos === true){
	          	foreach ($this->_workOrders as $wo) {
	        		$this->putcsv(array("Work Order"));
	        		$this->putcsv($this->getWoCsvTitle());
	            	$this->putcsv($this->makeWoDataLine($wo));
	            		
	            	$this->putcsv(array(""));
	            	$this->putcsv(array("Techs"));
	    			$this->putcsv($this->getTechCsvTitle());
	    				
	          		foreach ($wo['techsMappedAroundWos'] as $tech) {
	            		$this->putcsv($this->makeTechDataLine($tech));
	    			}
	    			$this->putcsv(array(""));
	    			$this->putcsv(array(""));
	    		}
           }else{
           		$this->putcsv(array("Work Orders"));
           		$this->putcsv($this->getWoCsvTitle());
           		foreach ($this->_workOrders as $wo) {
            		$this->putcsv($this->makeWoDataLine($wo));
    			}
    			if(!empty($this->_techs)){
    				$this->putcsv(array(""));
    				$this->putcsv(array(""));
    				$this->putcsv(array("Techs"));
    				$this->putcsv($this->getTechCsvTitle());
					foreach ($this->_techs as $tech) {
            			$this->putcsv($this->makeTechDataLine($tech));
    				}
    			}
           }
          

           ob_flush();flush();
           if ($offset % $limit > 0) break; //last page
        ob_end_flush();
    }
    
	/**
     * Format csv line with newlines support
     * @param array $row
     * @param string $fd
     * @param string $quot
     * @return number
     */
    private function putcsv($row, $fd=',', $quot='"')
    {
       $str='';
       foreach ($row as $cell)
       {
          $cell = str_replace($quot, $quot.$quot, $cell);
          if (strchr($cell, $fd) !== FALSE || strchr($cell, $quot) !== FALSE || strchr($cell, "\n") !== FALSE)
          {
             $str .= $quot.$cell.$quot.$fd;
          }
          else
          {
             $str .= $cell.$fd;
          }
       }
       echo substr($str, 0, -1)."\n";
       
       return strlen($str);
    }
    
	private function getWoCsvTitle()
    {
        $res = null;

        $res = array( "WIN#", "Client WO ID", "Work Order Stage", "Project", "Headline", "Region", 'Bundle', "Site Number", "Site Name", "Address", "City", "ST", "Zip", "Start Date", "Start Time", "End Date", "End Time", "Est. Duration", "Client $ Offer", "Per", "# Visits", "# Devices");
                  
        return $res;
    }
    
	private function getTechCsvTitle()
    {
        $res = null;

        $res = array( "FS Tech ID#", "First Name", "Last Name", "Day Phone", "Evening Phone", "City", 'ST', "Zip");
                  
        return $res;
    }
    
    private function makeWoDataLine($worder)
    {
        $row = null;
        
        $wo = new Core_Api_WosClass();
		$woVisits = $wo->getNumOfVisits($worder['WIN_NUM']);
        $row = array(
        	$worder['WIN_NUM'],
        	$worder['WO_ID'],
        	$worder['Status'],
        	$worder['Project_Name'],
        	$worder['Headline'],
        	$worder['Region'],
        	$worder['Route'],
        	$worder['SiteNumber'],
        	$worder['SiteName'],
        	$worder['Address'],
        	$worder['City'],
        	$worder['State'],
        	$worder['Zipcode'],
        	$this->_helpers['displayDate']->displayDate('n/j/Y', $worder['StartDate']),
        	$this->_helpers['displayDate']->displayDate('g:i A', $worder['StartTime']),
        	$this->_helpers['displayDate']->displayDate('n/j/Y', $worder['EndDate']),
        	$this->_helpers['displayDate']->displayDate('g:i A', $worder['EndTime']),
            $worder['Duration'],
            $this->_helpers['displayMoney']->displayMoney($worder['PayMax']),
            $worder['Amount_Per'],
            $woVisits,
            $worder['Qty_Devices']);

      	return $row;
    }
    
 	private function makeTechDataLine($tech)
    {
        $row = null;

        $row = array(
        	$tech->TechID,
        	$tech->Firstname,
        	$tech->Lastname,
        	$tech->PrimaryPhone,
        	$tech->SecondaryPhone,
        	$tech->City,
        	$tech->State,
        	$tech->Zipcode);

      	return $row;
    }
      	
    
	
}
?>