<?php

require_once APPLICATION_PATH . '/modules/dashboard/views/helpers/DisplayDate.php';
require_once APPLICATION_PATH . '/modules/dashboard/views/helpers/ProjectName.php';
require_once APPLICATION_PATH . '/modules/dashboard/views/helpers/DisplayBid.php';
require_once APPLICATION_PATH . '/modules/dashboard/views/helpers/DisplayMoney.php';
require_once APPLICATION_PATH . '/modules/dashboard/views/helpers/DisplayAmountPer.php';
require_once APPLICATION_PATH . '/modules/dashboard/views/helpers/DisplayMoneyPer.php';
require_once APPLICATION_PATH . '/modules/dashboard/views/helpers/DisplayBidPer.php';
       
class Core_CSV_Builder
{
    private $_tabName;

    private $_partName;

    private $_tmpFileHandler;

    private $_dataAdapter;

    private $_helpers;

    private $_flsVersion;

    private $_flsManager;

    private $_extendedData;
	
	private $_companyID;
	private $_showPay;
	private $_woAdditionalFields;


    public function __construct($partName, $tabName, $adapter, $isFLS = false, $isFLSManager = false, $extendedTechData = false)
    {
    	 
        $this->_partName = $partName;
        $this->_tabName = $tabName;
        
/*        $this->_tmpFileHandler = tmpfile();

        if (!$this->_tmpFileHandler) {
            throw Core_CSV_Exception('TMP File error');
        }*/

        $this->_dataAdapter = $adapter;

        $this->_flsVersion = $isFLS;

        $this->_flsManager = $isFLSManager;

        $this->_helpers = array();

        $this->_helpers['displayDate'] = new Dashboard_View_Helper_DisplayDate();
        $this->_helpers['projectName'] = new Dashboard_View_Helper_ProjectName();
        $this->_helpers['displayBid'] = new Dashboard_View_Helper_DisplayBid();
        $this->_helpers['displayMoney'] = new Dashboard_View_Helper_DisplayMoney();
        $this->_helpers['displayAmountPer'] = new Dashboard_View_Helper_DisplayAmountPer();
        $this->_helpers['displayMoneyPer'] = new Dashboard_View_Helper_displayMoneyPer();
        $this->_helpers['displayBidPer'] = new Dashboard_View_Helper_DisplayBidPer();

        set_time_limit(0);
        $this->_extendedData = $extendedTechData;		
    }


    public function __destruct()
    {
        //fclose($this->_tmpFileHandler);
    }


    public function getTabName()
    {
        return $this->_tabName;
    }


    private function writeExtendedData($workOrders) {
    	if (!count($workOrders)) return;
    	
    	if(!$this->_flsManager)
    		$this->updateWOWithTechInfo($workOrders);
    		
    	foreach ($workOrders as $wo) {
    		$this->putcsv($this->makeDataLine($wo));
    	}
    }

    /**
     * Outputs CSV file
     * @author Pavel Shutin
     */
    public function printCSV()
    {
    	ob_start();
    	$this->putcsv($this->getCsvTitle());
    	ob_flush();
    	flush();

        $offset = 0;
        $limit = 500;

        while ($result = $this->_dataAdapter->getResult($offset,$limit)) {
        	
            if ($this->_extendedData) {
                $workOrders = array();
                foreach ($result as $item) {
			// TODO: Should use Work Order Class based on adapter
                    $wo = API_AdminWorkOrder::workOrderWithResult($item, new API_AdminWorkOrder(null));
                    $workOrders[] = $wo;
                    $offset++;
                }
                
                $this->writeExtendedData($workOrders);
            }else{
                foreach ($result as $item) {
                    $wo = API_AdminWorkOrder::workOrderWithResult($item, new API_AdminWorkOrder(null));
                    $this->putcsv($this->makeDataLine($wo));
                    $offset++;
                }
            }

            ob_flush();flush();
            if ($offset % $limit > 0) break; //last page
        }
        ob_end_flush();
    }

    public function printCSVCustomer()
    {
    	ob_start();
        
		try {
			$this->_companyID = $this->_dataAdapter->getCompanyID();
			$this->_showPay = $this->_dataAdapter->getShowPay();
			 $this->_woAdditionalFields = $this->_dataAdapter->getWoAdditionalFields();
		} catch (Exception $e) {
			// shouldb't reach here unless not customer adapter
			$this->_companyID = "";
			$this->_showPay = false;
			$this->_woAdditionalFields = array();
		}

    	$this->putcsv($this->getCsvTitle());
        
    	ob_flush();
    	flush();

        $result = $this->_dataAdapter->getWos();

        if(!empty($result)) {            
            $total = count($result);
            $limit = 500;    
            $page = 0;   
            if ($this->_extendedData) {                
                $workOrders = array();
                foreach ($result as $item) {
                    $workOrders[] = $item;
                }
                if(!$this->_flsManager){
                    $this->updateWOWithTechInfo($workOrders);
                }
                 
                while($limit * $page < $total)
                {
                    $end_i = $limit*($page+1) < $total ? $limit*($page+1):$total;
                    for($i=$limit*$page; $i<$end_i; $i++){
                        if($i<$total){
                            $this->putcsv($this->makeDataLine($workOrders[$i]));   
                        }
                    }
                    ob_flush();flush();               
                    $page++;     
                }
            } else {
                while($limit * $page < $total)
                {
                    $end_i = $limit*($page+1) < $total ? $limit*($page+1):$total;
                    for($i=$limit*$page; $i<$end_i; $i++){
                        if($i<$total){
                            $wo = API_AdminWorkOrder::workOrderWithResult($result[$i], new API_AdminWorkOrder(null));
                            $this->putcsv($this->makeDataLine($wo));
                        }
                    }
                    ob_flush();flush();               
                    $page++;     
                }
            }            
        }
        ob_end_flush();
    }
    private function getCsvTitle()
    {
        $res = null;

        switch ($this->_partName) {
            case 'admin':
                switch ( $this->_tabName ) {
                    case 'workdone':
                    case 'incomplete':
                        $res = array( "WIN#", "WO_ID", "Company", "Start Date", "Start Time","End Date", "End Time", "Project", 'Route', "Site", "City", "State", "Zip", "MaxPay", "Tech Bid Amount", "Tech First Name", "Tech Last Name", "Tech Phone", "Tech Email", "Tech Accepted", "Tech Confirmed");
                        break;
                    case 'completed' :
                    case 'created' :
                        $res =array( "WIN#", "WO_ID", "Company", "Start Date", "Start Time","End Date", "End Time", "Project", 'Route', "Site", "City", "State", "Zip", "MaxPay");
                        break;
                    case 'published' :
                        $res =array( "WIN#", "WO_ID", "Company", "Start Date", "Start Time","End Date", "End Time", "Project", 'Route', "Site", "City", "State", "Zip", "MaxPay", "Qty_Applicants");
                        break;
                    case 'assigned' :
                        $res = array( "WIN#", "WO_ID", "Company", "Start Date", "Start Time","End Date", "End Time", "Project", 'Route', "Site", "City", "State", "Zip", "MaxPay", "Tech Bid Amount", "Tech First Name", "Tech Last Name", "Tech Phone", "Tech Email", "Tech Accepted", "Tech Confirmed");
                        break;
                    case 'approved' :
                    case 'inaccounting' :
                        $res = array( "WIN#", "WO_ID", "Company", "Start Date", "Start Time","End Date", "End Time", "Project", 'Route', "Site", "City", "State", "Zip", "MaxPay", "Tech Bid Amount", "Tech First Name", "Tech Last Name", "Tech Phone", "Tech Email", "Tech Accepted", "Tech Confirmed", "Pay Amount");
						break;
                    case 'all':
                        $res = array("WIN#", "WO_ID", "Company","PO","Username","DateEntered","Start Date", "Start Time","End Date", "End Time","Project_Name", 'Route',"SiteName","Type_ID","City","State","Zipcode","Tech_ID","Tech_FName","Tech_LName","TBPay","approvedBy","DateApproved","Amount_Per","PayMax","Tech_Bid_Amount","Time_In","Time_Out","calculatedTechHrs","Qty_Devices","baseTechPay","OutofScope_Amount","TripCharge","MileageReimbursement","MaterialsReimbursement","Additional_Pay_Amount","AbortFeeAmount","PayAmount","Net_Pay_Amount","calculatedInvoice","Add_Pay_AuthBy","Add_Pay_Reason","General9","General10","TechPaid","DatePaid","Invoiced","DateInvoiced","Deactivated","FS_InternalComments","AbortFee","FLS_ID","PcntDeduct","PricingRan","PricingRuleApplied", "Stage");
                        break;
                    case 'deactivated':
                        $res = array( "WIN#", "WO_ID", "Company", "Start Date", 'Start Time',"End Date", "End Time", "Project", 'Route', "Site", "City", "State", "Zip", "MaxPay", "Qty_Applicants", "Reason for Cancellation", "Reason Code");
                        break;
                    case 'searchpay':
                        $res = array( "WIN#", "WO ID", "Stage", "Client ID", "Company", "PO", "Created by", "Created", "Start", "Project ID", "Project Name", "Legal Entity", "Service Type", "AM", "CSD", "Site", "Skill Category", "Call Type (1 = install, 2 = serv call)", "City", "State", "Zip", "Tech ID", "Tech FName", "Tech LName", "ISO ID","TB Pay?", "Approved By", "DateApproved", "Work Order Type", "PayMax", "Tech Bid Amount", "Time In", "Time Out", "Qty Hours","Qty Visits", "Qty Devices", "Tech Base Pay", "Out of Scope Amount", "Trip Charge", "Mileage Charge", "Materials Fee", "Extra Pay", "Abort Fee Amount","Expense Report Reimbursement", "Gross Pay Amount", "Z2T Pay Amount", "Net Pay Amount", "Calculated Invoice Amount", "Extra Auth By", "Extra Pay Reason", "General 9", "General 5", "Paid", "Date Paid", "Invoiced", "Date Invoiced", "Deactivated", "FS Internal Comments", "Abort Fee", "FLS ID", "Work Market Tech ID#", "Work Market Ticket #", "Deduct fee from Tech","% Deduct" ,"Pricing Ran", "Pricing Rule Applied", "Pricing Rule Description", "Route");
                        break;
                    default :
                        throw new Core_CSV_Exception('Incorrect tab name: "' . $this->_tabName . '"', 0, Core_Exception::USING_ERROR);
                }
                break;
            case 'client':
                $res = $this->getClientCsvTitle();
                break;
            case 'customer':
                $res = $this->getClientCsvTitle();
                break;
            default:
                throw new Core_CSV_Exception('Incorrect tab name: "' . $this->_tabName . '"', 0, Core_Exception::USING_ERROR);
                break;
        }
        return $res;
    }


    private function getClientCsvTitle()
    {
        $res = null;
        if ($this->_flsManager) {
            switch ( $this->_tabName ) {
                case 'created' :
                    $res = array('WIN','Client ID','Client PO','Visits','Start Date', 'Start Time', "End Date", "End Time",'Hours','Skill','H/L','City','State','Zip','Project','Site Name','Site Number','Region','Route','Lead/Assist');
                    break;
                case 'published' :
                    $res = array('WIN','Client ID','Client PO','Start Date', 'Start Time', "End Date", "End Time",'Hours','Skill','H/L','City','State','Zip','Project','Site Name','Region','Route','Site Address','Site City','Site State','Site Zip','Lead/Assist');
                    break;
                case 'assigned' :
                    $res = array('WIN','Client ID','Client PO','Start Date', 'Start Time', "End Date", "End Time",'Hours','Tech Status','Skill','H/L','City','State','Zip','Project','Site Name','Site Number','Region','Route','Site Address','Site City','Site State','Site Zip','Tech ID','Tech Name',/*'Tech City','Tech State','Tech Zip','Tech Phone','Tech Email',*/'Lead/Assist');
                    break;
                case 'workdone':
                case 'incomplete':
                    $res = array('WIN','Client ID','Client PO','Visits','Start Date', 'Start Time', "End Date", "End Time",'Hours','Skill','H/L','City','State','Zip','Project','Site Name','Site Number','Region','Route','Tech ID','Tech Name',/*'Tech City','Tech State','Tech Zip','Tech Phone','Tech Email',*/ 'Lead/Assist','Parts','Paperwork');
                    break;
                case 'completed' :
                    $res = array('Approved','Paid','Invoice','WIN','Client ID','Client PO','Visits','Start Date', "Start Time","End Date", "End Time", 'Time In','Hours','Skill','H/L','City','Site Name','Site Number','Zip','Project','Site','Region','Route','Tech ID','Tech Name',/*'Tech City','Tech State','Tech Zip','Tech Phone','Tech Email',*/'Lead/Assist','Final Tech $', 'Invoice $');
                    break;
                case 'all':
                case 'today'://13944
                    $res = array('Stage','WIN','Client ID','Client PO','Visits','Start Date', 'Start Time',"End Date", "End Time", 'Time In','Hours','Tech Status','Skill','H/L','Address','City','Site Name','Zip','Project','Site Name','Site Number','Region','Route','Tech ID','Tech Name',/*'Tech City','Tech State','Tech Zip','Tech Phone','Tech Email',*/'Lead/Assist','Final Tech $', 'Invoice $');
                    break;
                case 'quickview':
                    $res = array( 'Stage', 'WIN', 'Client ID', 'Start Date', "Start Time","End Date", "End Time", 'Check In Date', 'Check In Time','Check Out Date' , 'Check Out Time', 'Hours', 'Tech Status','Skill', 'H/L', 'Address', 'City', 'Site Name', 'Zip', 'Project', 'Site Name','Site Number','Site Number', 'Region', 'Tech ID', 'Tech Name');
                    break;
                case 'deactivated':
                    $res = array('Reason','WIN','Client ID','Client PO','Visits','Start Date', 'Start Time',"End Date", "End Time",'Time In', 'Hours','Skill','H/L','City', 'Site Name', 'Zip','Project','Site Name','Site Number','Region','Route','Tech ID','Tech Name',/*'Tech City','Tech State','Tech Zip','Tech Phone','Tech Email',*/'Lead/Assist');
                    break;
                default :
                    throw new Zend_Exception('Incorrect tab name: "' . $this->_tabName . '"');
            }
        } else if (!$this->_flsVersion) {
        	
            switch ( $this->_tabName ) {
                case 'workdone':
                case 'incomplete':
                    $res = array( 'WIN', 'Client ID', 'Client PO','Visits', 'Start Date', 'Start Time',"End Date", "End Time",'Check In Date', 'Check In Time','Check Out Date' , 'Check Out Time', 'Hours', 'Skill', 'H/L', 'City', 'State', 'Zip', 'Project','Site Name','Site Number', 'Region','Route','Devices', 'Tech ID', 'Tech Name', 'Tech City', 'Tech State', 'Tech Zip','Tech Phone','Tech Email', 'Agreed $', 'Final $');
                    break;
                case 'completed' :
                    $res = array( 'WIN', 'Client ID', 'Client PO','Visits', 'Start Date', 'Start Time',"End Date", "End Time",'Check In Date','Check In Time','Check Out Date' , 'Check Out Time', 'Hours', 'Skill', 'H/L', 'City', 'State', 'Zip', 'Project','Site Name','Site Number', 'Region','Route','Devices', 'Tech ID', 'Tech Name', 'Tech City', 'Tech State', 'Tech Zip','Tech Phone','Tech Email','Final Tech $', 'Invoice $');
                    break;
                case 'created' :
                    $res = array( 'WIN', 'Client ID', 'Client PO','Visits', 'Start Date', 'Start Time',"End Date", "End Time", 'Hours', 'Skill', 'H/L', 'City', 'State', 'Zip', 'Project','Site Name','Site Number', 'Region','Route','Devices', "Max $");
                    break;
                case 'published' :
                    $res = array( 'WIN', 'Client ID', 'Client PO','Visits', 'Start Date', 'Start Time',"End Date", "End Time", 'Hours', 'Skill', 'H/L', 'City', 'State', 'Zip', 'Project','Site Name','Site Number', 'Region','Route','Devices', 'Site Address', 'Site City', 'Site State', 'Site Zip', 'Lead/Assist', 'Max $', 'Bids');
                    break;
                case 'assigned' :
                    $res = array(  'WIN', 'Client ID', 'Client PO','Visits', 'Start Date', 'Start Time',"End Date", "End Time",'Check In Date', 'Check In Time','Check Out Date' , 'Check Out Time', 'Hours', 'Tech Status', 'Skill', 'H/L', 'City', 'State', 'Zip', 'Project','Site Name','Site Number', 'Region','Route','Devices', 'Site Address', 'Site City', 'Site State', 'Site Zip', 'Tech ID', 'Tech Name', 'Tech City', 'Tech State', 'Tech Zip','Tech Phone','Tech Email', 'Agreed $');
                    break;
                case 'all':
				case 'today'://13944
                    $res = array( 'Stage', 'WIN', 'Client ID', 'Client PO','Visits', 'Start Date', 'Start Time',"End Date", "End Time",'Check In Date', 'Check In Time','Check Out Date' , 'Check Out Time', 'Hours','Tech Status', 'Skill', 'H/L', 'Address', 'City', 'State', 'Zip', 'Project','Site Name','Site Number', 'Region','Route','Devices', 'Tech ID', 'Tech Name', 'Tech City', 'Tech State', 'Tech Zip','Tech Phone','Tech Email', 'Bid $', 'Final Tech $', 'Basis', 'Invoice $');
                    break;
                case 'quickview':
					if ($this->_companyID == 'FATT') {
	                    $res = array( 'Stage', 'WIN', 'Client ID', 'Start Date', 'Start Time' , "End Date", "End Time",'Check In Date','Check In Time','Check Out Date' , 'Check Out Time', 'Hours', 'Tech Status','Skill', 'H/L', 'Address', 'City', 'State', 'Zip', 'Project','Site Name','Site Number', 'Region', 'Tech ID', 'Contractor ID#', 'Tech Name', 'Tech City', 'Tech State', 'Tech Zip');
						if ($this->_showPay) {
							$res[] = "Agreed $";
							$res[] = "Pay Code";
							$res[] = "Per";
							$res[] = "Final $";
						}
					}
					else{
                                                $CSapi = new Core_Api_CustomerClass();
                                                $QTV = $CSapi->getQTV_ByCustomerID($_SESSION['CustomerID']);
                                                //752
                                                if($QTV['ShowTechDirectContactInfo'] == 1){
                                                     $res = array( 'Stage', 'WIN', 'Client ID', 'Start Date', 'Start Time', "End Date", "End Time",'Check In Date','Check In Time','Check Out Date' , 'Check Out Time', 'Hours', 'Tech Status','Skill', 'H/L', 'Address', 'City', 'State', 'Zip', 'Project','Site Name','Site Number', 'Region', 'Tech ID', 'Tech Name', 'Tech Phone#','Tech Email' ,'Tech City', 'Tech State', 'Tech Zip');    
                                                }else{
                                                     $res = array( 'Stage', 'WIN', 'Client ID', 'Start Date', 'Start Time',"End Date", "End Time",'Check In Date', 'Check In Time','Check Out Date' , 'Check Out Time', 'Hours', 'Tech Status','Skill', 'H/L', 'Address', 'City', 'State', 'Zip', 'Project','Site Name','Site Number', 'Region', 'Tech ID', 'Tech Name', 'Tech City', 'Tech State', 'Tech Zip');
                                                }
                                                //End 752
                                               
                            }
                    break;
                case 'deactivated':
                    //14040
                   $res = array( 'Reason', 'Comments', 'WIN', 'Client ID', 'Client PO','Visits', 'Start Date', 'Start Time',"End Date", "End Time", 'Check In Date','Check In Time','Check Out Date' , 'Check Out Time', 'Hours', 'Skill', 'H/L', 'City', 'State', 'Zip', 'Project','Site Name','Site Number', 'Region','Route','Devices', 'Tech ID', 'Tech Name', 'Tech City', 'Tech State', 'Tech Zip','Tech Phone','Tech Email', 'Bid $', 'Final $');
                    //End 14040
                    break;
                default :
                    throw new Zend_Exception('Incorrect tab name: "' . $this->_tabName . '"');
            }
        } else {
        	
            switch ( $this->_tabName ) {
                case 'workdone':
                    $res = array( 'WIN', 'Client ID', 'Client PO','Visits', 'Start Date','StartTime',"End Date", "End Time", 'Check In Date','Check In Time','Check Out Date' , 'Check Out Time', 'Hours', 'Skill', 'H/L', 'City', 'State', 'Zip', 'Project','Site Name','Site Number', 'Region','Route','Devices', 'Tech ID', 'Tech Name', 'Tech City', 'Tech State', 'Tech Zip','Tech Phone','Tech Email', 'Lead/Assist', 'Agreed $', 'Final $', 'Parts', 'Paperwork');
                    break;
                case 'incomplete':
                    $res = array( 'WIN', 'Client ID', 'Client PO','Visits', 'Start Date','StartTime',"End Date", "End Time", 'Check In Date','Check In Time','Check Out Date' , 'Check Out Time', 'Hours', 'Skill', 'H/L', 'City', 'State', 'Zip', 'Project','Site Name','Site Number', 'Region','Route','Devices', 'Tech ID', 'Tech Name', 'Tech City', 'Tech State', 'Tech Zip','Tech Phone','Tech Email', 'Lead/Assist', 'Agreed $', 'Final $');
                    break;
                case 'completed' :
                    $res = array( 'WIN', 'Client ID', 'Client PO','Visits', 'Start Date','StartTime',"End Date", "End Time", 'Check In Date','Check In Time','Check Out Date' , 'Check Out Time', 'Hours', 'Skill', 'H/L', 'City', 'State', 'Zip', 'Project','Site Name','Site Number', 'Region','Route','Devices', 'Tech ID', 'Tech Name', 'Tech City', 'Tech State', 'Tech Zip','Tech Phone','Tech Email', 'Lead/Assist', 'Final $');
                    break;
                case 'created' :
                    $res = array( 'WIN', 'Client ID', 'Client PO','Visits', 'Start Date', 'Start Time',"End Date", "End Time", 'Hours', 'Skill', 'H/L', 'City', 'State', 'Zip', 'Project','Site Name','Site Number', 'Region','Route','Devices', 'Lead/Assist', "Max $");
                    break;
                case 'published' :
                    $res = array( 'WIN', 'Client ID', 'Client PO','Visits', 'Start Date','StartTime',"End Date", "End Time", 'Hours', 'Skill', 'H/L', 'City', 'State', 'Zip', 'Project','Site Name','Site Number', 'Region','Route','Devices', 'Site Address', 'Site City', 'Site State', 'Site Zip', 'Lead/Assist', 'Max $', 'Qty_Applicants');
                    break;
                case 'assigned' :
                    $res = array(  'WIN', 'Client ID', 'Client PO','Visits', 'Start Date', 'Start Time',"End Date", "End Time", 'Check In Date','Check In Time','Check Out Date' , 'Check Out Time', 'Hours', 'Tech Status', 'Skill', 'H/L', 'City', 'State', 'Zip', 'Project','Site Name','Site Number', 'Region','Route','Devices', 'Site Address', 'Site City', 'Site State', 'Site Zip', 'Tech ID', 'Tech Name', 'Tech City', 'Tech State', 'Tech Zip','Tech Phone','Tech Email', 'Lead/Assist', 'Agreed $');
                    break;
                case 'all':
				case 'today'://13944
                    $res = array( 'Stage', 'WIN', 'Client ID', 'Client PO','Visits', 'Start Date', 'Start Time',"End Date", "End Time", 'Check In Date','Check In Time','Check Out Date' , 'Check Out Time', 'Hours','Tech Status', 'Skill', 'H/L', 'Address', 'City', 'State', 'Zip', 'Project','Site Name','Site Number', 'Region','Route','Devices', 'Tech ID', 'Tech Name', 'Tech City', 'Tech State', 'Tech Zip','Tech Phone','Tech Email', 'Lead/Assist', 'Bid $', 'Final $', 'Basis');
                    break;
                case 'quickview':
                    $res = array( 'Stage', 'WIN', 'Client ID', 'Start Date','Start Time',"End Date", "End Time", 'Check In Date','Check In Time','Check Out Date' , 'Check Out Time', 'Hours', 'Tech Status','Skill', 'H/L', 'Address', 'City', 'State', 'Zip', 'Project','Site Name','Site Number', 'Region', 'Tech ID', 'Tech Name', 'Tech City', 'Tech State', 'Tech Zip');
                    break;
                case 'deactivated':
                    $res = array( 'Reason', 'WIN', 'Client ID', 'Client PO','Visits', 'Start Date','Start Time',"End Date", "End Time", 'Check In Date','Check In Time','Check Out Date' , 'Check Out Time', 'Hours', 'Skill', 'H/L', 'City', 'State', 'Zip', 'Project','Site Name','Site Number', 'Region','Route','Devices', 'Tech ID', 'Tech Name', 'Tech City', 'Tech State', 'Tech Zip','Tech Phone','Tech Email', 'Lead/Assist', 'Bid $', 'Final $');
                    break;
                default :
                    throw new Zend_Exception('Incorrect tab name: "' . $this->_tabName . '"');
            }
        }
        return $res;
    } // end function ngetClientCsvTitle


    private function makeDataLine($worder)
    {
        $row = null;
        //786
         $core = new Core_Api_WosClass();
         $arrWoDetails = $core->getBaseWOVisitInfo_ByWinnum($worder->WIN_NUM);
        //End 786
        // 14112 
        $Core_Api_TechClass = new Core_Api_TechClass;
        $resultISO = $Core_Api_TechClass->getTechInfo_ForAPIUsing($worder->Tech_ID);
        if (!empty($resultISO)) {
            $techISO_ID = $resultISO['ISO_Affiliation_ID'];                    
        } //14112 
         //$worder->ISO_Affiliation_ID://
        switch ($this->_partName) {
            case 'admin':
                switch ( $this->_tabName ) {
                    case 'workdone':
                    case 'incomplete':
                        $row = array(
                            $worder->WIN_NUM,
                            $worder->WO_ID,
                            $worder->Company_Name,
                            //$this->_helpers['displayDate']->displayDate('n/j/Y', $worder->StartDate),
                            //$worder->StartTime,
                            $arrWoDetails['StartDate'],
                            $arrWoDetails['StartTime'],
                            $arrWoDetails['EndDate'],
                            $arrWoDetails['EndTime'],
                            //$worder->EndDate,
                            $this->_helpers['projectName']->projectName($worder),
                            $worder->Route,
                            $worder->SiteName,
                            $worder->City,
                            $worder->State,
                            $worder->Zipcode,
                            $this->_helpers['displayMoney']->displayMoney($worder->PayMax),
                            $this->_helpers['displayMoney']->displayMoney($worder->Tech_Bid_Amount, "",$worder->Amount_Per),
                            $worder->Tech_FName,
                            $worder->Tech_LName,
                            $worder->TechPhone,
                            $worder->TechEmail,
                            $worder->WorkOrderReviewed,
                            $worder->TechCheckedIn_24hrs
                        );
                        break;
                    case 'completed' :
                    case 'created' :
                        $row = array(
                            $worder->WIN_NUM,
                            $worder->WO_ID,
                            $worder->Company_Name,
                            $arrWoDetails['StartDate'],
                            $arrWoDetails['StartTime'],
                            $arrWoDetails['EndDate'],
                            $arrWoDetails['EndTime'],
                            $this->_helpers['projectName']->projectName($worder),
                            $worder->Route,
                            $worder->SiteName,
                            $worder->City,
                            $worder->State,
                            $worder->Zipcode,
                            $this->_helpers['displayMoney']->displayMoney($worder->PayMax),
                            $this->_helpers['displayMoney']->displayMoney($worder->Tech_Bid_Amount, "",$worder->Amount_Per),
                            $worder->Tech_FName,
                            $worder->Tech_LName,
                            $worder->TechPhone,
                            $worder->TechEmail,
                            $worder->WorkOrderReviewed,
                            $worder->TechCheckedIn_24hrs,
                            $this->_helpers['displayMoney']->displayMoney($worder->PayAmount)
                        );
                        break;
                    case 'published' :
                        $row = array(
                            $worder->WIN_NUM,
                            $worder->WO_ID,
                            $worder->Company_Name,
                            $arrWoDetails['StartDate'],
                            $arrWoDetails['StartTime'],
                            $arrWoDetails['EndDate'],
                            $arrWoDetails['EndTime'],
                            $this->_helpers['projectName']->projectName($worder),
                            $worder->Route,
                            $worder->SiteName,
                            $worder->City,
                            $worder->State,
                            $worder->Zipcode,
                            $this->_helpers['displayMoney']->displayMoney($worder->PayMax),
                            $worder->Qty_Applicants
                        );
                        break;
                    case 'assigned' :
                        $row = array(
                            $worder->WIN_NUM,
                            $worder->WO_ID,
                            $worder->Company_Name,
                            $arrWoDetails['StartDate'],
                            $arrWoDetails['StartTime'],
                            $arrWoDetails['EndDate'],
                            $arrWoDetails['EndTime'],
                            $this->_helpers['projectName']->projectName($worder),
                            $worder->Route,
                            $worder->SiteName,
                            $worder->City,
                            $worder->State,
                            $worder->Zipcode,
                            $worder->PayMax,
                            $this->_helpers['displayMoney']->displayMoney($worder->Tech_Bid_Amount, "",$worder->Amount_Per),
                            $worder->Tech_FName,
                            $worder->Tech_LName,
                            $worder->TechPhone,
                            $worder->TechEmail,
                            $worder->WorkOrderReviewed,
                            $worder->TechCheckedIn_24hrs
                        );
                        break;
                    case 'approved' :
                    case 'inaccounting' :
                        $row = array(
                            $worder->WIN_NUM,
                            $worder->WO_ID,
                            $worder->Company_Name,
                            $arrWoDetails['StartDate'],
                            $arrWoDetails['StartTime'],
                            $arrWoDetails['EndDate'],
                            $arrWoDetails['EndTime'],
                            $this->_helpers['projectName']->projectName($worder),
                            $worder->Route,
                            $worder->SiteName,
                            $worder->City,
                            $worder->State,
                            $worder->Zipcode,
                            $worder->PayMax,
                            $this->_helpers['displayMoney']->displayMoney($worder->Tech_Bid_Amount, "",$worder->Amount_Per),
                            $worder->Tech_FName,
                            $worder->Tech_LName,
                            $worder->TechPhone,
                            $worder->TechEmail,
                            $worder->WorkOrderReviewed,
                            $worder->TechCheckedIn_24hrs,
                            $this->_helpers['displayMoney']->displayMoney($worder->PayAmount)
                        );
                        break;
                    case 'all':
                        $row = array(
                            $worder->WIN_NUM,
                            $worder->WO_ID,
                            $worder->Company_Name,
                            $worder->PO,
                            $worder->Username,
                            $worder->DateEntered,
                            $arrWoDetails['StartDate'],
                            $arrWoDetails['StartTime'],
                            $arrWoDetails['EndDate'],
                            $arrWoDetails['EndTime'],
                            $this->_helpers['projectName']->projectName($worder),
                            $worder->Route,
                            $worder->SiteName,
                            $worder->Type_ID,
                            $worder->City,
                            $worder->State,
                            $worder->Zipcode,
                            $worder->Tech_ID,
                            $worder->Tech_FName,
                            $worder->Tech_LName,
                            $worder->TBPay,
                            $worder->approvedBy,
                            $this->_helpers['displayDate']->displayDate($worder->DateApproved),
                            $worder->Amount_Per,
                            $this->_helpers['displayMoney']->displayMoney($worder->PayMax),
							$this->_helpers['displayMoney']->displayMoney($worder->Tech_Bid_Amount),
							$worder->Time_In,
							$worder->Time_Out,
							$worder->calculatedTechHrs,
							$worder->Qty_Devices,
							$this->_helpers['displayMoney']->displayMoney($worder->baseTechPay),
							$this->_helpers['displayMoney']->displayMoney($worder->OutofScope_Amount),
							$this->_helpers['displayMoney']->displayMoney($worder->TripCharge),
							$this->_helpers['displayMoney']->displayMoney($worder->MileageReimbursement),
							$this->_helpers['displayMoney']->displayMoney($worder->MaterialsReimbursement),
							$this->_helpers['displayMoney']->displayMoney($worder->Additional_Pay_Amount),
							$this->_helpers['displayMoney']->displayMoney($worder->AbortFeeAmount),
							$this->_helpers['displayMoney']->displayMoney($worder->PayAmount),
							$this->_helpers['displayMoney']->displayMoney($worder->Net_Pay_Amount),
							$this->_helpers['displayMoney']->displayMoney($worder->calculatedInvoice),
							$worder->Add_Pay_AuthBy,
							$worder->Add_Pay_Reason,
							$worder->General9,
							$worder->General10,
							$worder->TechPaid,
							$this->_helpers['displayDate']->displayDate($worder->DatePaid),
							$worder->Invoiced,
							$this->_helpers['displayDate']->displayDate($worder->DateInvoiced),
							$worder->Deactivated,
							$worder->FS_InternalComments,
							$worder->AbortFee,
							$worder->FLS_ID,
							$worder->PcntDeduct,
							$worder->PricingRan,
							$worder->PricingRuleApplied,
							$worder->Status
                        );
						break;
                    case 'deactivated':
                        $row = array(
                            $worder->WIN_NUM,
                            $worder->WO_ID,
                            $worder->Company_Name,
                            $arrWoDetails['StartDate'],
                            $arrWoDetails['StartTime'],
                            $arrWoDetails['EndDate'],
                            $arrWoDetails['EndTime'],
                            $this->_helpers['projectName']->projectName($worder),
                            $worder->Route,
                            $worder->SiteName,
                            $worder->City,
                            $worder->State,
                            $worder->Zipcode,
                            $worder->PayMax,
                            $worder->Qty_Applicants,
                            $worder->Deactivated_Reason,
                            $worder->DeactivationCode
                        );
                        break;
                    case 'searchpay':
						$rules = Core_Pricing::getRules();

/*                        $DateEntered = new Zend_Date($worder->DateEntered);
                        $StartDate = new Zend_Date($worder->StartDate);
                        $DateApproved = new Zend_Date($worder->DateApproved);
                        $DatePaid = new Zend_Date($worder->DatePaid);
                        $DateInvoiced = new Zend_Date($worder->DateInvoiced);*/
                         $coreapiWosClass=new Core_Api_WosClass;        
                        $qtyDevices = $worder->Qty_Devices;
                        //$qtyVisits = $coreapiWosClass->getNumOfVisits($worder->WIN_NUM);
						$qtyVisits = $worder->Qty_Visits;
                        $search = new Core_Api_AdminClass;
                        $woTagsInfo = $search->getWorkOrderTagsInfo($worder->WIN_NUM);
                        //throw new Core_CSV_Exception('Incorrect tab name: "' . $woTagsInfo . '"', 0, Core_Exception::USING_ERROR);
                        $row = array(
                            $worder->WIN_NUM,
                            $worder->WO_ID,
                            $worder->Status,
                            $worder->Company_ID,
                            $worder->Company_Name,
                            $worder->PO,
                            $worder->Username,
                            $this->_helpers['displayDate']->displayDate('n/j/Y', $worder->DateEntered),
                            $this->_helpers['displayDate']->displayDate('n/j/Y', $worder->StartDate),
                            //$arrWoDetails['StartDate'],
                            //$arrWoDetails['StartTime'],
                            //$arrWoDetails['EndDate'],
                            //$arrWoDetails['EndTime'],
                            $worder->Project_ID,
                            $worder->Project_Name,
                            $woTagsInfo['EntityType_Name'],
                            $woTagsInfo['ServiceType_Name'],
                            $woTagsInfo['AM_Name'],
                            $woTagsInfo['CSD_Name'],
                            $worder->SiteName,
                            $worder->WO_Category,
                            $worder->Type_ID,
                            $worder->City,
                            $worder->State,
                            $worder->Zipcode,
                            $worder->Tech_ID,
                            $worder->Tech_FName,
                            $worder->Tech_LName,
                            $techISO_ID,//14112
                            $worder->TBPay ? "Yes" : "No",
                            $worder->approvedBy,
                            $this->_helpers['displayDate']->displayDate('n/j/Y g:i A', $worder->DateApproved),
                            $worder->Amount_Per,
                            $worder->PayMax,
                            $worder->Tech_Bid_Amount,
                            $worder->Time_In,
                            $worder->Time_Out,
                            $worder->calculatedTechHrs,
                            $qtyVisits,
                            $qtyDevices,
                            $worder->baseTechPay,
                            $worder->OutofScope_Amount,
                            $worder->TripCharge,
                            $worder->MileageReimbursement,
                            $worder->MaterialsReimbursement,
                            $worder->Additional_Pay_Amount,
                            $worder->AbortFeeAmount,
                            $coreapiWosClass->getTotal_ApReThFS($worder->WIN_NUM),
                            $worder->PayAmount,
                            $worder->Z2T_Pay_Amount,
                            $worder->Net_Pay_Amount,
                            $worder->calculatedInvoice,
                            $worder->Add_Pay_AuthBy,
                            $worder->Add_Pay_Reason,
                            $worder->General9,
                            $worder->General5,
                            $worder->TechPaid ? "Yes" : "No",
                            $this->_helpers['displayDate']->displayDate('n/j/Y', $worder->DatePaid),
                            $worder->Invoiced ? "Yes" : "No",
                            $this->_helpers['displayDate']->displayDate('n/j/Y', $worder->DateInvoiced),
                            $worder->Deactivated ? "Yes" : "No",
                            $worder->FS_InternalComments,
                            $worder->AbortFee ? "Yes" : "No",
                            $worder->FLS_ID,
                            $worder->WMID,
                            $worder->WMAssignmentID,
                            $worder->PcntDeduct ? "Yes" : "No",
                            $worder->PcntDeduct ? ($worder->PcntDeductPercent*100).'%'  : "0%",
                            $worder->PricingRan ? "Yes" : "No",
                            $worder->PricingRuleApplied,
                            !empty($worder->PricingRuleApplied) ? $rules[$worder->PricingRuleApplied]['Description'] : '',
                            $worder->Route
                        );
                        break;
                    default :
                        throw new Core_CSV_Exception('Incorrect tab name: "' . $this->_tabName . '"', 0, Core_Exception::USING_ERROR);
                }
                break;

            // end admin part
            case 'client':
                $row = $this->makeClientDataLine($worder);
                break;
            case 'customer':
                $row = $this->makeClientDataLine($worder);
                break;
            //end client part
            default:
                throw new Core_CSV_Exception('Incorrect client part: "' . $this->_tabName . '"', 0, Core_Exception::USING_ERROR);
                break;
        }

        return $row;
    }


    private function makeClientDataLine($worder)
    {
        //--- visit info
        $api = new Core_Api_WosClass();
        $numOfVisit = $api->getNumOfVisits($worder->WIN_NUM);
        //786
         $_checkOutDate = "";
         $_checkOutTime = "";
         $_checkInDate = "";
         $_checkInTime = "";
         $core = new Core_Api_WosClass();
         $arrWoDetails = $core->getBaseWOVisitInfo_ByWinnum($worder->WIN_NUM);
         if(!empty($arrWoDetails)){
             if($arrWoDetails['OptionCheckInOut']==1){
                $_checkOutDate = $arrWoDetails['TechCheckOutDate'];
                $_checkOutTime = $arrWoDetails['TechCheckOutTime'];
                $_checkInDate =  $arrWoDetails['TechCheckInDate'];
                $_checkInTime =  $arrWoDetails['TechCheckInTime'];
             }else{
                $_checkOutDate = $arrWoDetails['CORCheckOutDate'];
                $_checkOutTime = $arrWoDetails['CORCheckOutTime'];
                $_checkInDate =  $arrWoDetails['CORCheckInDate'];
                $_checkInTime =  $arrWoDetails['CORCheckInTime'];
             }
         }
        //End 786
        //---

		//13784
        $checkedOut = $api->isWoCheckedOut($worder->WIN_NUM);  
        //13790
        $_showStatus = ''; 
        if(($worder->Date_Out || $worder->Time_Out || $checkedOut)) {
            $_showStatus = 'Checked Out';
        } else if(($worder->CheckedIn || $worder->Date_In || $worder->Time_In)) {
            $_showStatus = 'Checked In';
        } else if($worder->TechCheckedIn_24hrs) {
            $_showStatus = 'Confirmed';
        } else {
            $_showStatus = '';
        }       
        //end 13790 
        //13881
        $blankCheckInOutByClient = $api->blankCheckInOutByClient($worder->WIN_NUM);
        if($blankCheckInOutByClient){
            if($worder->TechCheckedIn_24hrs) {
                $_showStatus = 'Confirmed';
            } else {
                $_showStatus = '';
            }      
        }
        //end 13881
        $row = null;
        if ( !$this->_flsManager ) { //when user company id = or != fls and user type != manager and != install desk
            switch ( $this->_tabName ) {
                case 'workdone' :
                    $row = array(
                        $worder->WIN_NUM,
                        $worder->WO_ID,
                        $worder->PO,
                        $numOfVisit,
                        $arrWoDetails['StartDate'],
                        $arrWoDetails['StartTime'],
                        $arrWoDetails['EndDate'],
                        $arrWoDetails['EndTime'],
                        //$this->_helpers['displayDate']->displayDate('g:i A', $worder->Time_In),
                        //786
                        $_checkInDate ,
                        $_checkInTime,
                        $_checkOutDate,
                        $_checkOutTime,
                        //End 786
                        $worder->calculatedTechHrs + 0,
                        $worder->WO_Category,
                        $worder->Headline,
                        ( !empty($worder->City) )? $worder->City : '',
                        ( !empty($worder->State) )? $worder->State : '',
                        ( !empty($worder->Zipcode) )? '"'.$worder->Zipcode.'"' : '',
                        $this->_helpers['projectName']->projectName($worder),
                        $worder->SiteName,
                        $worder->SiteNumber,
                        $worder->Region,
                        $worder->Route,
						(!empty($worder->Qty_Devices))? $worder->Qty_Devices : '',
                        ( $this->_flsVersion ) ? $worder->FLS_ID : $worder->Tech_ID,
                        ( !empty($worder->techInfo) )? $worder->techInfo->Firstname.' '.$worder->techInfo->Lastname:'',
                        ( !empty($worder->techInfo) && !empty($worder->techInfo->City) ) ? $worder->techInfo->City : '',
                        ( !empty($worder->techInfo) && !empty($worder->techInfo->State) ) ? $worder->techInfo->State : '',
                        ( !empty($worder->techInfo) && !empty($worder->techInfo->Zipcode) ) ? '"'.$worder->techInfo->Zipcode.'"' : '',
                        ( !empty($worder->techInfo) && !empty($worder->techInfo->PrimaryPhone)) ? $worder->techInfo->PrimaryPhone : '',
                        ( !empty($worder->techInfo) && !empty($worder->techInfo->PrimaryEmail)) ? $worder->techInfo->PrimaryEmail : '',
                        ( $worder->Assist) ? 'Assist' : (( $worder->Lead ) ? 'Lead' : ''),
                        $this->_helpers['displayBid']->displayBid($worder->Tech_Bid_Amount, $worder->Amount_Per),
                        $this->_helpers['displayMoney']->displayMoney($worder->PayAmount),
                        ( $worder->PartsProcessed ) ? 'Yes' : 'No',
                        ( $worder->Paperwork_Received ) ? 'Yes' : 'No'
                    );
                    if ( !$this->_flsVersion ) {
                        $c = count($row);
                        unset( $row[$c-1], $row[$c-2], $row[$c-5] );
                    }
                    break;
                case 'incomplete':
                    $row = array(
                        $worder->WIN_NUM,
                        $worder->WO_ID,
                        $worder->PO,
                        $numOfVisit,
                        $arrWoDetails['StartDate'],
                        $arrWoDetails['StartTime'],
                        $arrWoDetails['EndDate'],
                        $arrWoDetails['EndTime'],
                        //$this->_helpers['displayDate']->displayDate('g:i A', $worder->Time_In),
                         //786
                        $_checkInDate ,
                        $_checkInTime,
                        $_checkOutDate,
                        $_checkOutTime,
                        //End 786
                        $worder->calculatedTechHrs + 0,
                        $worder->WO_Category,
                        $worder->Headline,
                        ( !empty($worder->City) )? $worder->City : '',
                        ( !empty($worder->State) )? $worder->State : '',
                        ( !empty($worder->Zipcode) )? '"'.$worder->Zipcode.'"' : '',
                        $this->_helpers['projectName']->projectName($worder),
                        $worder->SiteName,
                        $worder->SiteNumber,
                        $worder->Region,
                        $worder->Route,
						(!empty($worder->Qty_Devices))? $worder->Qty_Devices : '',
                        ( $this->_flsVersion ? $worder->FLS_ID : $worder->Tech_ID ),
                        ( !empty($worder->techInfo) )? $worder->techInfo->Firstname.' '.$worder->techInfo->Lastname:'',
                        ( !empty($worder->techInfo) && !empty($worder->techInfo->City) ) ? $worder->techInfo->City : '',
                        ( !empty($worder->techInfo) && !empty($worder->techInfo->State) ) ? $worder->techInfo->State : '',
                        ( !empty($worder->techInfo) && !empty($worder->techInfo->Zipcode) ) ? '"'.$worder->techInfo->Zipcode.'"' : '',
                        ( !empty($worder->techInfo) && !empty($worder->techInfo->PrimaryPhone)) ? $worder->techInfo->PrimaryPhone : '',
                        ( !empty($worder->techInfo) && !empty($worder->techInfo->PrimaryEmail)) ? $worder->techInfo->PrimaryEmail : '',
                        ( $worder->Assist) ? 'Assist' : (( $worder->Lead ) ? 'Lead' : ''),
                        $this->_helpers['displayBid']->displayBid($worder->Tech_Bid_Amount, $worder->Amount_Per),
                        $this->_helpers['displayMoney']->displayMoney($worder->PayAmount)
                    );
                    if ( !$this->_flsVersion ) {
                        $c = count($row);
                        unset( $row[$c-3] );
                    }
                    break;

                case 'completed':
                    $row = array(
                        $worder->WIN_NUM,
                        $worder->WO_ID,
                        $worder->PO,
                        $numOfVisit,
                        $arrWoDetails['StartDate'],
                        $arrWoDetails['StartTime'],
                        $arrWoDetails['EndDate'],
                        $arrWoDetails['EndTime'],
                        //$this->_helpers['displayDate']->displayDate('g:i A', $worder->Time_In),
                         //786
                        $_checkInDate ,
                        $_checkInTime,
                        $_checkOutDate,
                        $_checkOutTime,
                        //End 786
                        $worder->calculatedTechHrs + 0,
                        $worder->WO_Category,
                        $worder->Headline,
                        ( !empty($worder->City) )? $worder->City : '',
                        ( !empty($worder->State) )? $worder->State : '',
                        ( !empty($worder->Zipcode) )? '"'.$worder->Zipcode.'"' : '',
                        $this->_helpers['projectName']->projectName($worder),
                        $worder->SiteName,
                        $worder->SiteNumber,
                        $worder->Region,
                        $worder->Route,
						(!empty($worder->Qty_Devices))? $worder->Qty_Devices : '',
                        ( $this->_flsVersion ? $worder->FLS_ID : $worder->Tech_ID ),
                        ( !empty($worder->techInfo) )? $worder->techInfo->Firstname.' '.$worder->techInfo->Lastname:'',
                        ( !empty($worder->techInfo) && !empty($worder->techInfo->City) ) ? $worder->techInfo->City : '',
                        ( !empty($worder->techInfo) && !empty($worder->techInfo->State) ) ? $worder->techInfo->State : '',
                        ( !empty($worder->techInfo) && !empty($worder->techInfo->Zipcode) ) ? '"'.$worder->techInfo->Zipcode.'"' : '',
                        ( !empty($worder->techInfo) && !empty($worder->techInfo->PrimaryPhone)) ? $worder->techInfo->PrimaryPhone : '',
                        ( !empty($worder->techInfo) && !empty($worder->techInfo->PrimaryEmail)) ? $worder->techInfo->PrimaryEmail : '',
                        ( $worder->Assist) ? 'Assist' : (( $worder->Lead ) ? 'Lead' : ''),
                        $this->_helpers['displayMoney']->displayMoney($worder->PayAmount),
						$worder->canShowCalculatedInvoice() ? $this->_helpers['displayMoney']->displayMoney($worder->calculatedInvoice) : "--"
                    );
                    if ( !$this->_flsVersion ) {
                        $c = count($row);
                        unset( $row[$c-3] );
                    }
                    break;

                case 'created' :
                    $row = array(
                        $worder->WIN_NUM,
                        $worder->WO_ID,
                        $worder->PO,
                        $numOfVisit,
                        $arrWoDetails['StartDate'],
                        $arrWoDetails['StartTime'],
                        $arrWoDetails['EndDate'],
                        $arrWoDetails['EndTime'],
                        $worder->calculatedTechHrs+0,
                        $worder->WO_Category,
                        $worder->Headline,
                        $worder->City,
                        $worder->State,
                        '"'.$worder->Zipcode.'"',
                        $this->_helpers['projectName']->projectName($worder),
                        $worder->SiteName,
                        $worder->SiteNumber,
                        $worder->Region,
                        $worder->Route,
						(!empty($worder->Qty_Devices))? $worder->Qty_Devices : '',
                        ( $worder->Assist) ? 'Assist' : (( $worder->Lead ) ? 'Lead' : ''),
                        $this->_helpers['displayBid']->displayBid($worder->PayMax, $worder->Amount_Per)
                    );
                    if ( !$this->_flsVersion ) {
                        $c = count($row);
                        unset( $row[$c-2] );
                    }
                    break;

                case 'published' :
					$row = array(
                        $worder->WIN_NUM,
                        $worder->WO_ID,
                        $worder->PO,
                        $numOfVisit,
                        $arrWoDetails['StartDate'],
                        $arrWoDetails['StartTime'],
                        $arrWoDetails['EndDate'],
                        $arrWoDetails['EndTime'],
                        $worder->calculatedTechHrs + 0,
                        $worder->WO_Category,
                        $worder->Headline,
                        ( !empty($worder->City) )? $worder->City : '',
                        ( !empty($worder->State) )? $worder->State : '',
                        ( !empty($worder->Zipcode) )? $worder->Zipcode : '',
                        $this->_helpers['projectName']->projectName($worder),
                        $worder->SiteName,
                        $worder->SiteNumber,
                        $worder->Region,
                        $worder->Route,
                        (!empty($worder->Qty_Devices))? $worder->Qty_Devices : '',
                        ( !empty($worder->Address) )? $worder->Address : '',
                        ( !empty($worder->City) )? $worder->City : '',
                        ( !empty($worder->State) )? $worder->State : '',
                        ( !empty($worder->Zipcode) )? $worder->Zipcode : '',
                        ( $worder->Assist) ? 'Assist' : (( $worder->Lead ) ? 'Lead' : ''),
                        $this->_helpers['displayMoneyPer']->displayMoneyPer($worder->PayMax,'',$worder->Amount_Per),
                         $worder->Qty_Applicants
                    );
/*                    if ( !$this->_flsVersion ) {
                        $c = count($row);
                        unset( $row[$c-2] );
                    }*/
                    break;

                case 'assigned' :
                    $row = array(
                        $worder->WIN_NUM,
                        $worder->WO_ID,
                        $worder->PO,
                        $numOfVisit,
                        $arrWoDetails['StartDate'],
                        $arrWoDetails['StartTime'],
                        $arrWoDetails['EndDate'],
                        $arrWoDetails['EndTime'],
                        //$this->_helpers['displayDate']->displayDate('g:i A', $worder->Time_In),
                         //786
                        $_checkInDate ,
                        $_checkInTime,
                        $_checkOutDate,
                        $_checkOutTime,
                        //End 786
                        $worder->calculatedTechHrs + 0,
                        $_showStatus,//13790   
                        $worder->WO_Category,
                        $worder->Headline,
		                ( !empty($worder->City) )? $worder->City : '',
                        ( !empty($worder->State) )? $worder->State : '',
                        ( !empty($worder->Zipcode) )? $worder->Zipcode : '',
                        $this->_helpers['projectName']->projectName($worder),
                        $worder->SiteName,
                        $worder->SiteNumber,
                        $worder->Region,
                        $worder->Route,
                        (!empty($worder->Qty_Devices))? $worder->Qty_Devices : '',
                        ( !empty($worder->Address) )? $worder->Address : '',
                        ( !empty($worder->City) )? $worder->City : '',
                        ( !empty($worder->State) )? $worder->State : '',
                        ( !empty($worder->Zipcode) )? $worder->Zipcode : '',
                        ( $this->_flsVersion ? $worder->FLS_ID : $worder->Tech_ID ),
                         implode(' ', array($worder->Tech_FName, $worder->Tech_LName) ),
                        ( !empty($worder->techInfo) && !empty($worder->techInfo->City) ) ? $worder->techInfo->City : '',
                        ( !empty($worder->techInfo) && !empty($worder->techInfo->State) ) ? $worder->techInfo->State : '',
                        ( !empty($worder->techInfo) && !empty($worder->techInfo->Zipcode) ) ? '"'.$worder->techInfo->Zipcode.'"' : '',
                        ( !empty($worder->techInfo) && !empty($worder->techInfo->PrimaryPhone)) ? $worder->techInfo->PrimaryPhone : '',
                        ( !empty($worder->techInfo) && !empty($worder->techInfo->PrimaryEmail)) ? $worder->techInfo->PrimaryEmail : '',
                        ( $worder->Assist) ? 'Assist' : (( $worder->Lead ) ? 'Lead' : ''),
                        $this->_helpers['displayBidPer']->displayBidPer($worder->Tech_Bid_Amount, $worder->Amount_Per)
                    );
                    if ( !$this->_flsVersion ) {
                        $c = count($row);
                        unset( $row[$c-2] );
                    }
                    break;

                case 'all' :
				case 'today':
                    $row = array(
                        $worder->Status,
                        $worder->WIN_NUM,
                        $worder->WO_ID,
                        $worder->PO,
                        $numOfVisit,
                        $arrWoDetails['StartDate'],
                        $arrWoDetails['StartTime'],
                        $arrWoDetails['EndDate'],
                        $arrWoDetails['EndTime'],
                        //$this->_helpers['displayDate']->displayDate('g:i A', $worder->Time_In),
                         //786
                        $_checkInDate ,
                        $_checkInTime,
                        $_checkOutDate,
                        $_checkOutTime,
                        //End 786
                        $worder->calculatedTechHrs + 0,
                        $_showStatus,//13944
                        $worder->WO_Category,
                        $worder->Headline,
                        ( !empty($worder->Address) )? $worder->Address : '',
                        ( !empty($worder->City) )? $worder->City : '',
                        ( !empty($worder->State) )? $worder->State : '',
                        ( !empty($worder->Zipcode) )? '"'.$worder->Zipcode.'"' : '',
                        $this->_helpers['projectName']->projectName($worder),
                        $worder->SiteName,
                        $worder->SiteNumber,
                        $worder->Region,
                        $worder->Route,
						(!empty($worder->Qty_Devices))? $worder->Qty_Devices : '',
                        ( $this->_flsVersion ? $worder->FLS_ID : $worder->Tech_ID ),
                        ( !empty($worder->techInfo) )? $worder->techInfo->Firstname.' '.$worder->techInfo->Lastname:'',
                        ( !empty($worder->techInfo) && !empty($worder->techInfo->City) ) ? $worder->techInfo->City : '',
                        ( !empty($worder->techInfo) && !empty($worder->techInfo->State) ) ? $worder->techInfo->State : '',
                        ( !empty($worder->techInfo) && !empty($worder->techInfo->Zipcode) ) ? '"'.$worder->techInfo->Zipcode.'"' : '',
                        ( !empty($worder->techInfo) && !empty($worder->techInfo->PrimaryPhone)) ? $worder->techInfo->PrimaryPhone : '',
                        ( !empty($worder->techInfo) && !empty($worder->techInfo->PrimaryEmail)) ? $worder->techInfo->PrimaryEmail : '',
                        ( $worder->Assist) ? 'Assist' : (( $worder->Lead ) ? 'Lead' : ''),
                        $this->_helpers['displayBid']->displayBid($worder->Tech_Bid_Amount, $worder->Amount_Per),
                        $this->_helpers['displayMoney']->displayMoney($worder->PayAmount),
                        $this->_helpers['displayAmountPer']->displayAmountPer($worder->Amount_Per),
						$worder->canShowCalculatedInvoice() ? $this->_helpers['displayMoney']->displayMoney($worder->calculatedInvoice) : "--"
                    );
                    if ( !$this->_flsVersion ) {
                        $c = count($row);
                        unset( $row[$c-5] );
                    }
                    break;
					case 'quickview' :
					$techData = $this->_dataAdapter->getTech($worder->Tech_ID);
                    //752
                    $CSapi = new Core_Api_CustomerClass();
                    $QTV = $CSapi->getQTV_ByCustomerID($_SESSION['CustomerID']);
                    if($QTV['ShowTechDirectContactInfo'] == 1){
                         $_showEmail = (!empty($techData->PrimaryEmail))?$techData->PrimaryEmail:'';
                    }else{
                         $_showEmail = '';
                    }
                    //End 752
                    $row = array(
                        $worder->Status,
                        $worder->WIN_NUM,
                        $worder->WO_ID,
                        $arrWoDetails['StartDate'],
                        $arrWoDetails['StartTime'],
                        $arrWoDetails['EndDate'],
                        $arrWoDetails['EndTime'],
                        //$this->_helpers['displayDate']->displayDate('g:i A', $worder->Time_In),
                         //786
                        $_checkInDate ,
                        $_checkInTime,
                        $_checkOutDate,
                        $_checkOutTime,
                        //End 786
                        $worder->calculatedTechHrs + 0,
                        //13784
                        //trim((($worder->WorkOrderReviewed)?"Accepted":'').(($worder->TechCheckedIn_24hrs)?" Confirmed":'').(($worder->CheckedIn || $worder->Date_In || $worder->Time_In)?" Checked In":'').(($worder->Date_Out || $worder->Time_Out || $checkedOut)?" Checked Out":'')),                        
                        $_showStatus,//13790 
                        $worder->WO_Category,
                        $worder->Headline,
                        ( !empty($worder->Address) )? $worder->Address : '',
                        ( !empty($worder->City) )? $worder->City : '',
                        ( !empty($worder->State) )? $worder->State : '',
                        ( !empty($worder->Zipcode) )? '"'.$worder->Zipcode.'"' : '',
                        $this->_helpers['projectName']->projectName($worder),
                        $worder->SiteName,
                        $worder->SiteNumber,
                        $worder->Region,
                        ( $this->_flsVersion ? $worder->FLS_ID : $worder->Tech_ID ));
					
					if ($this->_companyID == 'FATT') {
						$row[] = $techData->CertNumber;
					}
					
					$row[] = ( !empty($techData) )? $techData->Firstname.' '.$techData->Lastname:'';
                                        if(!empty($_showEmail)){
                                            $row[] = $worder->TechPhone;
					$row[] = $_showEmail;
                                        }
					$row[] = ( !empty($techData) && !empty($techData->City) ) ? $techData->City : ' ';
					$row[] = ( !empty($techData) && !empty($techData->State) ) ? $techData->State : '';
					$row[] = ( !empty($techData) && !empty($techData->Zipcode) ) ? '"'.$techData->Zipcode.'"' : '';
					
					if ($this->_companyID == 'FATT' && $this->_showPay) {
						$row[] = $worder->Tech_Bid_Amount;
						if ($this->_companyID == 'FATT') {
							$row[] = $this->_woAdditionalFields[$worder->WIN_NUM]['Flex_Pay_Code'];
						}
						$row[] = $worder->Amount_Per;
						$row[] = $worder->PayAmount;		
					}
                    break;
                case 'deactivated' :
                    $row = array(
                        $worder->DeactivationCode,
						 //14040
						$worder->Deactivated_Reason,
                        //End 14040
                        $worder->WIN_NUM,
                        $worder->WO_ID,
                        $worder->PO,
                        $numOfVisit,
                        $arrWoDetails['StartDate'],
                        $arrWoDetails['StartTime'],
                        $arrWoDetails['EndDate'],
                        $arrWoDetails['EndTime'],
                        //$this->_helpers['displayDate']->displayDate('g:i A', $worder->Time_In),
                         //786
                        $_checkInDate ,
                        $_checkInTime,
                        $_checkOutDate,
                        $_checkOutTime,
                        //End 786
                        $worder->calculatedTechHrs + 0,
                        $worder->WO_Category,
                        $worder->Headline,
                        ( !empty($worder->City) )? $worder->City : '',
                        ( !empty($worder->State) )? $worder->State : '',
                        ( !empty($worder->Zipcode) )? '"'.$worder->Zipcode.'"' : '',
                        $this->_helpers['projectName']->projectName($worder),
                        $worder->SiteName,
                        $worder->SiteNumber,
                        $worder->Region,
                        $worder->Route,
						(!empty($worder->Qty_Devices))? $worder->Qty_Devices : '',
                        ( $this->_flsVersion ? $worder->FLS_ID : $worder->Tech_ID ),
                        ( !empty($worder->techInfo) )? $worder->techInfo->Firstname.' '.$worder->techInfo->Lastname:'',
                        ( !empty($worder->techInfo) && !empty($worder->techInfo->City) ) ? $worder->techInfo->City : '',
                        ( !empty($worder->techInfo) && !empty($worder->techInfo->State) ) ? $worder->techInfo->State : '',
                        ( !empty($worder->techInfo) && !empty($worder->techInfo->Zipcode) ) ? '"'.$worder->techInfo->Zipcode.'"' : '',
                        ( !empty($worder->techInfo) && !empty($worder->techInfo->PrimaryPhone)) ? $worder->techInfo->PrimaryPhone : '',
                        ( !empty($worder->techInfo) && !empty($worder->techInfo->PrimaryEmail)) ? $worder->techInfo->PrimaryEmail : '',
                        ( $worder->Assist) ? 'Assist' : (( $worder->Lead ) ? 'Lead' : ''),
                        $this->_helpers['displayBid']->displayBid($worder->Tech_Bid_Amount, $worder->Amount_Per),
                        $this->_helpers['displayMoney']->displayMoney($worder->PayAmount)
                    );
                    if ( !$this->_flsVersion ) {
                        $c = count($row);
                        unset( $row[$c-3] );
                    }
                    break;
                }

        } else { //when user company id = fls and user type = manager or install desk
            switch ( $this->_tabName ) {
                case 'created' :
                    $row = array(
                        $worder->WIN_NUM,
                        $worder->WO_ID,
                        $worder->PO,
                        $numOfVisit,
                        $arrWoDetails['StartDate'],
                        $arrWoDetails['StartTime'],
                        $arrWoDetails['EndDate'],
                        $arrWoDetails['EndTime'],
                        $worder->calculatedTechHrs+0,
                        $worder->WO_Category,
                        $worder->Headline,
                        $worder->City,
                        $worder->State,
                        '"'.$worder->Zipcode.'"',
                        $this->_helpers['projectName']->projectName($worder),
                        $worder->SiteName,
                        $worder->SiteNumber,
                        $worder->Region,
                        $worder->Route,
						(!empty($worder->Qty_Devices))? $worder->Qty_Devices : '',
                        ( $worder->Assist) ? 'Assist' : (( $worder->Lead ) ? 'Lead' : '')
                    );
                    if ( !$this->_flsVersion ) {
                        $c = count($row);
                        unset( $row[$c-2] );
                    }
                    break;
                case 'published' :
                    $row = array(
                        $worder->WIN_NUM,
                        $worder->WO_ID,
                        $worder->PO,
                        $arrWoDetails['StartDate'],
                        $arrWoDetails['StartTime'],
                        $arrWoDetails['EndDate'],
                        $arrWoDetails['EndTime'],
                        $worder->calculatedTechHrs+0,
                        $worder->WO_Category,
                        $worder->Headline,
                        ( !empty($worder->City) )? $worder->City : '',
                        ( !empty($worder->State) )? $worder->State : '',
                        ( !empty($worder->Zipcode) )? '"'.$worder->Zipcode.'"' : '',
                        $this->_helpers['projectName']->projectName($worder),
                        $worder->SiteName,
                        $worder->SiteNumber,
                        $worder->Region,
                        $worder->Route,
						(!empty($worder->Qty_Devices))? $worder->Qty_Devices : '',
                        ( !empty($worder->Address) )? $worder->Address : '',
                        ( !empty($worder->City) )? $worder->City : '',
                        ( !empty($worder->State) )? $worder->State : '',
                        ( !empty($worder->Zipcode) )? '"'.$worder->Zipcode.'"' : '',
                        ( $worder->Assist) ? 'Assist' : (( $worder->Lead ) ? 'Lead' : '')
                    );
                    if ( !$this->_flsVersion ) {
                        $c = count($row);
                        unset( $row[$c-2] );
                    }
                    break;
                case 'assigned' :
                    $row = array(
                        $worder->WIN_NUM,
                        $worder->WO_ID,
                        $worder->PO,
                        $arrWoDetails['StartDate'],
                        $arrWoDetails['StartTime'],
                        $arrWoDetails['EndDate'],
                        $arrWoDetails['EndTime'],
                        //$this->_helpers['displayDate']->displayDate('g:i A', $worder->Time_In),
                         //786
                        $_checkInDate ,
                        $_checkInTime,
                        $_checkOutDate,
                        $_checkOutTime,
                        //End 786
                        $worder->calculatedTechHrs + 0,
                        $_showStatus,//13790  
                        $worder->WO_Category,
                        $worder->Headline,
		                ( !empty($worder->City) )? $worder->City : '',
                        ( !empty($worder->State) )? $worder->State : '',
                        ( !empty($worder->Zipcode) )? '"'.$worder->Zipcode.'"' : '',
                        $this->_helpers['projectName']->projectName($worder),
                        $worder->SiteName,
                        $worder->SiteNumber,
                        $worder->Region,
                        $worder->Route,
						(!empty($worder->Qty_Devices))? $worder->Qty_Devices : '',
                        ( !empty($worder->Address) )? $worder->Address : '',
                        ( !empty($worder->City) )? $worder->City : '',
                        ( !empty($worder->State) )? $worder->State : '',
                        ( !empty($worder->Zipcode) )? '"'.$worder->Zipcode.'"' : '',
                       
                        ( $this->_flsVersion ? $worder->FLS_ID : $worder->Tech_ID ),
                        ( !empty($worder->techInfo) )? $worder->techInfo->Firstname.' '.$worder->techInfo->Lastname:$worder->Tech_FName.' '.$worder->Tech_LName
                        /*
                        ( !empty($worder->techInfo) && !empty($worder->techInfo->City) ) ? $worder->techInfo->City : '',
                        ( !empty($worder->techInfo) && !empty($worder->techInfo->State) ) ? $worder->techInfo->State : '',
                        ( !empty($worder->techInfo) && !empty($worder->techInfo->Zipcode) ) ? '"'.$worder->techInfo->Zipcode.'"' : '',
                        ( !empty($worder->techInfo) && !empty($worder->techInfo->PrimaryPhone)) ? $worder->techInfo->PrimaryPhone : '',
                        ( !empty($worder->techInfo) && !empty($worder->techInfo->PrimaryEmail)) ? $worder->techInfo->PrimaryEmail : '',
                        */
                        ( $worder->Assist) ? 'Assist' : (( $worder->Lead ) ? 'Lead' : '')
                    );
                    if ( !$this->_flsVersion ) {
                        $c = count($row);
                        unset( $row[$c-2] );
                    }
                    break;
                case 'incomplete':
                case 'workdone' :
                    $row = array(
                        $worder->WIN_NUM,
                        $worder->WO_ID,
                        $worder->PO,
                        $numOfVisit,
                        $arrWoDetails['StartDate'],
                        $arrWoDetails['StartTime'],
                        $arrWoDetails['EndDate'],
                        $arrWoDetails['EndTime'],
                        //$this->_helpers['displayDate']->displayDate('g:i A', $worder->Time_In),
                         //786
                        $_checkInDate ,
                        $_checkInTime,
                        $_checkOutDate,
                        $_checkOutTime,
                        //End 786
                        $worder->calculatedTechHrs + 0,
                        $worder->WO_Category,
                        $worder->Headline,
                        ( !empty($worder->City) )? $worder->City : '',
                        ( !empty($worder->State) )? $worder->State : '',
                        ( !empty($worder->Zipcode) )? '"'.$worder->Zipcode.'"' : '',
                        $this->_helpers['projectName']->projectName($worder),
                        $worder->SiteName,
                        $worder->SiteNumber,
                        $worder->Region,
                        $worder->Route,
						(!empty($worder->Qty_Devices))? $worder->Qty_Devices : '',
                        ( $this->_flsVersion ) ? $worder->FLS_ID : $worder->Tech_ID,
                        ( !empty($worder->techInfo) )? $worder->techInfo->Firstname.' '.$worder->techInfo->Lastname:$worder->Tech_FName.' '.$worder->Tech_LName,
                        /*
                        ( !empty($worder->techInfo) && !empty($worder->techInfo->City) ) ? $worder->techInfo->City : '',
                        ( !empty($worder->techInfo) && !empty($worder->techInfo->State) ) ? $worder->techInfo->State : '',
                        ( !empty($worder->techInfo) && !empty($worder->techInfo->Zipcode) ) ? '"'.$worder->techInfo->Zipcode.'"' : '',
                        ( !empty($worder->techInfo) && !empty($worder->techInfo->PrimaryPhone)) ? $worder->techInfo->PrimaryPhone : '',
                        ( !empty($worder->techInfo) && !empty($worder->techInfo->PrimaryEmail)) ? $worder->techInfo->PrimaryEmail : '',
                        */
                        ( $worder->Assist) ? 'Assist' : (( $worder->Lead ) ? 'Lead' : ''),
                        ( $worder->PartsProcessed ) ? 'Yes' : 'No',
                        ( $worder->Paperwork_Received ) ? 'Yes' : 'No'
                    );
                    if ( !$this->_flsVersion ) {
                        $c = count($row);
                        unset( $row[$c-1], $row[$c-2], $row[$c-5] );
                    }
                    break;
                case 'completed':
                    $row = array(
                        $this->_helpers['displayDate']->displayDate('n/j/Y', $worder->DateApproved),
                        $this->_helpers['displayDate']->displayDate('n/j/Y', $worder->DatePaid),
                        $this->_helpers['displayDate']->displayDate('n/j/Y', $worder->DateInvoiced),
                        $worder->WIN_NUM,
                        $worder->WO_ID,
                        $worder->PO,
                        $numOfVisit,
                        $arrWoDetails['StartDate'],
                        $arrWoDetails['StartTime'],
                        $arrWoDetails['EndDate'],
                        $arrWoDetails['EndTime'],
                        //$this->_helpers['displayDate']->displayDate('g:i A', $worder->Time_In),
                         //786
                        $_checkInDate ,
                        $_checkInTime,
                        $_checkOutDate,
                        $_checkOutTime,
                        //End 786
                        $worder->calculatedTechHrs + 0,
                        $worder->WO_Category,
                        $worder->Headline,
                        ( !empty($worder->City) )? $worder->City : '',
                        ( !empty($worder->State) )? $worder->State : '',
                        ( !empty($worder->Zipcode) )? '"'.$worder->Zipcode.'"' : '',
                        $this->_helpers['projectName']->projectName($worder),
                        $worder->SiteName,
                        $worder->SiteNumber,
                        $worder->Region,
                        $worder->Route,
						(!empty($worder->Qty_Devices))? $worder->Qty_Devices : '',
                        ( $this->_flsVersion ? $worder->FLS_ID : $worder->Tech_ID ),
                        ( !empty($worder->techInfo) )? $worder->techInfo->Firstname.' '.$worder->techInfo->Lastname:$worder->Tech_FName.' '.$worder->Tech_LName,
                       /*
                        ( !empty($worder->techInfo) && !empty($worder->techInfo->City) ) ? $worder->techInfo->City : '',
                        ( !empty($worder->techInfo) && !empty($worder->techInfo->State) ) ? $worder->techInfo->State : '',
                        ( !empty($worder->techInfo) && !empty($worder->techInfo->Zipcode) ) ? '"'.$worder->techInfo->Zipcode.'"' : '',
                        ( !empty($worder->techInfo) && !empty($worder->techInfo->PrimaryPhone)) ? $worder->techInfo->PrimaryPhone : '',
                        ( !empty($worder->techInfo) && !empty($worder->techInfo->PrimaryEmail)) ? $worder->techInfo->PrimaryEmail : '',
                        */
                        ( $worder->Assist) ? 'Assist' : (( $worder->Lead ) ? 'Lead' : ''),
						$worder->calculatedInvoice
                    );
                    if ( !$this->_flsVersion ) {
                        $c = count($row);
                        unset( $row[$c-2] );
                    }
                    break;
                case 'all' :
                    $row = array(
                        $worder->Status,
                        $worder->WIN_NUM,
                        $worder->WO_ID,
                        $worder->PO,
                        $numOfVisit,
                        $arrWoDetails['StartDate'],
                        $arrWoDetails['StartTime'],
                        $arrWoDetails['EndDate'],
                        $arrWoDetails['EndTime'],
                        //$this->_helpers['displayDate']->displayDate('g:i A', $worder->Time_In),
                         //786
                        $_checkInDate ,
                        $_checkInTime,
                        $_checkOutDate,
                        $_checkOutTime,
                        //End 786
                        $worder->calculatedTechHrs + 0,
                        $worder->WO_Category,
                        $worder->Headline,
                        ( !empty($worder->Address) )? $worder->Address : '',
                        ( !empty($worder->City) )? $worder->City : '',
                        ( !empty($worder->State) )? $worder->State : '',
                        ( !empty($worder->Zipcode) )? '"'.$worder->Zipcode.'"' : '',
                        $this->_helpers['projectName']->projectName($worder),
                        $worder->SiteName,
                        $worder->SiteNumber,
                        $worder->Region,
                        $worder->Route,
						(!empty($worder->Qty_Devices))? $worder->Qty_Devices : '',
                        ( $this->_flsVersion ? $worder->FLS_ID : $worder->Tech_ID ),
                        ( !empty($worder->techInfo) )? $worder->techInfo->Firstname.' '.$worder->techInfo->Lastname:$worder->Tech_FName.' '.$worder->Tech_LName,
                        /*
                        ( !empty($worder->techInfo) && !empty($worder->techInfo->City) ) ? $worder->techInfo->City : '',
                        ( !empty($worder->techInfo) && !empty($worder->techInfo->State) ) ? $worder->techInfo->State : '',
                        ( !empty($worder->techInfo) && !empty($worder->techInfo->Zipcode) ) ? '"'.$worder->techInfo->Zipcode.'"' : '',
                        ( !empty($worder->techInfo) && !empty($worder->techInfo->PrimaryPhone)) ? $worder->techInfo->PrimaryPhone : '',
                        ( !empty($worder->techInfo) && !empty($worder->techInfo->PrimaryEmail)) ? $worder->techInfo->PrimaryEmail : '',
                        */
                        ( $worder->Assist) ? 'Assist' : (( $worder->Lead ) ? 'Lead' : ''),
						$worder->calculatedInvoice
                    );
                    if ( !$this->_flsVersion ) {
                        $c = count($row);
                        unset( $row[$c-4] );
                    }
                    break;
					case 'quickview' :
                    $row = array(
                        $worder->Status,
                        $worder->WIN_NUM,
                        $worder->WO_ID,
                        $worder->PO,
                       $arrWoDetails['StartDate'],
                        $arrWoDetails['StartTime'],
                        $arrWoDetails['EndDate'],
                        $arrWoDetails['EndTime'],
                        //$this->_helpers['displayDate']->displayDate('g:i A', $worder->Time_In),
                         //786
                        $_checkInDate ,
                        $_checkInTime,
                        $_checkOutDate,
                        $_checkOutTime,
                        //End 786
                        $worder->calculatedTechHrs + 0,
                        //13784
                        //trim((($worder->WorkOrderReviewed)?"Accepted":'').(($worder->TechCheckedIn_24hrs)?" Confirmed":'').(($worder->CheckedIn || $worder->Date_In || $worder->Time_In)?" Checked In":'').(($worder->Date_Out || $worder->Time_Out || $checkedOut)?" Checked Out":'')), 
                        $_showStatus,//13790  
                        $worder->WO_Category,
                        $worder->Headline,
                        ( !empty($worder->Address) )? $worder->Address : '',
                        ( !empty($worder->City) )? $worder->City : '',
                        ( !empty($worder->State) )? $worder->State : '',
                        ( !empty($worder->Zipcode) )? '"'.$worder->Zipcode.'"' : '',
                        $this->_helpers['projectName']->projectName($worder),
                        $worder->SiteName,
                        $worder->SiteNumber,
                        $worder->Region,
                        ( $this->_flsVersion ? $worder->FLS_ID : $worder->Tech_ID ),
                        ( !empty($worder->techInfo) )? $worder->techInfo->Firstname.' '.$worder->techInfo->Lastname:'',
                        /*( !empty($worder->techInfo) && !empty($worder->techInfo->City) ) ? $worder->techInfo->City : '',
                        ( !empty($worder->techInfo) && !empty($worder->techInfo->State) ) ? $worder->techInfo->State : '',
                        ( !empty($worder->techInfo) && !empty($worder->techInfo->Zipcode) ) ? '"'.$worder->techInfo->Zipcode.'"' : '',*/
                        ( $worder->Assist) ? 'Assist' : (( $worder->Lead ) ? 'Lead' : '')
                    );
                    if ( !$this->_flsVersion ) {
                        $c = count($row);
                        unset( $row[$c-4] );
                    }
                    break;
                case 'deactivated' :
                    $row = array(
                        $worder->DeactivationCode,
                        $worder->WIN_NUM,
                        $worder->WO_ID,
                        $worder->PO,
                        $numOfVisit,
                        $arrWoDetails['StartDate'],
                        $arrWoDetails['StartTime'],
                        $arrWoDetails['EndDate'],
                        $arrWoDetails['EndTime'],
                        //$this->_helpers['displayDate']->displayDate('g:i A', $worder->Time_In),
                         //786
                        $_checkInDate ,
                        $_checkInTime,
                        $_checkOutDate,
                        $_checkOutTime,
                        //End 786
                        $worder->calculatedTechHrs + 0,
                        $worder->WO_Category,
                        $worder->Headline,
                        ( !empty($worder->City) )? $worder->City : '',
                        ( !empty($worder->State) )? $worder->State : '',
                        ( !empty($worder->Zipcode) )? '"'.$worder->Zipcode.'"' : '',
                        $this->_helpers['projectName']->projectName($worder),
                        $worder->SiteName,
                        $worder->SiteNumber,
                        $worder->Region,
                        $worder->Route,
						(!empty($worder->Qty_Devices))? $worder->Qty_Devices : '',
                        ( $this->_flsVersion ? $worder->FLS_ID : $worder->Tech_ID ),
                        ( !empty($worder->techInfo) )? $worder->techInfo->Firstname.' '.$worder->techInfo->Lastname:$worder->Tech_FName.' '.$worder->Tech_LName,
                       /*
                        ( !empty($worder->techInfo) && !empty($worder->techInfo->City) ) ? $worder->techInfo->City : '',
                        ( !empty($worder->techInfo) && !empty($worder->techInfo->State) ) ? $worder->techInfo->State : '',
                        ( !empty($worder->techInfo) && !empty($worder->techInfo->Zipcode) ) ? '"'.$worder->techInfo->Zipcode.'"' : '',
                        ( !empty($worder->techInfo) && !empty($worder->techInfo->PrimaryPhone)) ? $worder->techInfo->PrimaryPhone : '',
                        ( !empty($worder->techInfo) && !empty($worder->techInfo->PrimaryEmail)) ? $worder->techInfo->PrimaryEmail : '',
                        */
                        ( $worder->Assist) ? 'Assist' : (( $worder->Lead ) ? 'Lead' : '')
                    );
                    if ( !$this->_flsVersion ) {
                        $c = count($row);
                        unset( $row[$c-3] );
                    }
                    break;
                }
        }
        return $row;
    }

    /**
     * Format csv line with newlines support
     * @param array $row
     * @param string $fd
     * @param string $quot
     * @return number
     */
    private function putcsv($row, $fd=',', $quot='"')
    {
       $str='';
       foreach ($row as $cell)
       {
          $cell = str_replace($quot, $quot.$quot, $cell);
          if (strchr($cell, $fd) !== FALSE || strchr($cell, $quot) !== FALSE || strchr($cell, "\n") !== FALSE)
          {
             $str .= $quot.$cell.$quot.$fd;
          }
          else
          {
             $str .= $cell.$fd;
          }
       }
       echo substr($str, 0, -1)."\n";
       
       return strlen($str);
    }

    /**
     *
     * Update work orders with tech info
     * @param array $workOrders array of API_WorkOrder
     */
    private function updateWOWithTechInfo(array $workOrders)
    {
    	
    	$dataByTechs = array();
    	$techsIds = array();
    	foreach ($workOrders as $item) {
    		if (!empty($item->Tech_ID) ) {
    			$techsIds[] = $item->Tech_ID;
    			if (!key_exists($item->Tech_ID, $dataByTechs) ) {
    				$dataByTechs[$item->Tech_ID] = array();
    			}
    			$dataByTechs[$item->Tech_ID][] = $item;
    		}
    	}

    	$techIds = array_unique($techsIds);
    	if (count($techIds)) {
    		$db = Core_Database::getInstance();
    		$select = $db->select()->from('TechBankInfo', array('TechID', 'Firstname' => 'FirstName', 'Lastname' => 'LastName','City','State','Zipcode','PrimaryPhone','PrimaryEmail'))->where('TechID IN (?)', array($techIds));
    		$result = $db->query($select);

    		while ($tech = $result->fetch(Zend_Db::FETCH_OBJ)) {
    			foreach ($dataByTechs[$tech->TechID] as $item) {
    				$item->techInfo = $tech;
    			}
    		}
    	}
    }
}
