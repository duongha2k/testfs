<?php


/**
 * Adapter for user part
 *
 * @author Alexander Cheshchevik, Pavel Shutin
 *
 */
class Core_CSV_Adapter_Client implements Core_CSV_Adapter_Interface
{
    private $_filters;

    private $_fields;

    private $_sortBy;

    private $_companyID;

    public function __construct($login, $pass, $filters, $fields, $sortBy)
    {
		$user = new Core_Api_User();
        $user->checkAuthentication(array('login'=>$login, 'password'=>$pass));
		if (!$user->isAuthenticate()) throw new Core_CSV_Adapter_Exception('User is not authentificated');

		$this->_companyID = $user->getContextCompanyId();

        $this->_filters = $filters;
        $this->_fields = $fields;
        $this->_sortBy = $sortBy;
        
    }

    /**
     * (non-PHPdoc)
     * @see Core_CSV_Adapter_Interface::getResult()
     */
	public function getResult($offset = 0, $limit = 0)
	{
        return API_WorkOrderFilter::filter($this->_companyID, $this->_fields, '', '', '', $this->_filters, $this->_sortBy, $limit, $offset);
	}

}