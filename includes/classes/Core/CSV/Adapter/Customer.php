<?php

/**
 * Adapter for user part
 *
 * @author Alexander Cheshchevik, Pavel Shutin
 *
 */
class Core_CSV_Adapter_Customer implements Core_CSV_Adapter_Interface {

    private $_filters;
    private $_fields;
    private $_sortBy;
    private $_companyID;
    private $_pass;
    private $_login;
    private $_wos;
    private $_techs;
	private $_showPay;
	private $_woAdditionalFields;
const FIELDS = 'WIN_NUM,WO_ID,DateEntered,PO,ShowTechs,SourceByDate,ShortNotice,StartDate,StartTime,EndDate,EndTime,StartRange,Duration,Region,Type_ID,Project_ID,WO_Category_ID,Headline,Site_Contact_Name,SiteName,SiteNumber,Project_Name,Address,City,State,Zipcode,SitePhone,SiteFax,SiteEmail,Update_Requested,Update_Reason,Description,Requirements,Tools,SpecialInstructions,Lead,Assist,SignOffSheet_Required,ProjectManagerName,ProjectManagerPhone,ProjectManagerEmail,ResourceCoordinatorName,ResourceCoordinatorPhone,ResourceCoordinatorEmail,EmergencyName,EmergencyPhone,EmergencyEmail,TechnicalSupportName,TechnicalSupportPhone,TechnicalSupportEmail,CheckInOutName,CheckInOutNumber,CheckInOutEmail,Tech_ID,Tech_FName,Tech_LName,NoShow_Tech,BackOut_Tech,FLS_ID,BU1FLSID,BU1Info,Backup_FLS_ID,Ship_Contact_Info,PayMax,Amount_Per,Qty_Devices,Tech_Bid_Amount,WorkOrderReviewed,TechCheckedIn_24hrs,CheckedIn,Checkedby,PreCall_1,PreCall_2,CheckInNotes,StoreNotified,Notifiedby,DateNotified,NotifyNotes,TechMarkedComplete,Date_In,Time_In,Date_Out,Time_Out,ContactHD,HDPOC,TechMiles,TechComments,AttachedFile,FLS_OOS,Unexpected_Steps,Unexpected_Desc,AskedBy,AskedBy_Name,Paperwork_Received,Incomplete_Paperwork,MissingComments,Site_Complete,CallClosed,PartsProcessed,ClientComments,Work_Out_of_Scope,OutofScope_Amount,OutofScope_Reason,TripCharge,MileageReimbursement,MaterialsReimbursement,Additional_Pay_Amount,Penalty_Reason,AbortFee,AbortFeeAmount,Add_Pay_AuthBy,Add_Pay_Reason,calculatedTechHrs,baseTechPay,PayAmount,Approved,TechPaid,DatePaid,Deactivated,DeactivationCode,Deactivated_Reason,General1,General2,General3,General4,General5,General6,General7,General8,General9,General10,FS_InternalComments,Username,Invoiced,Company_Name,Company_ID,TechEmail,TechPhone,TechPhoneExt,Date_Assigned,lastModBy,lastModDate,approvedBy,DateApproved,DeactivatedDate,DeactivatedBy,Latitude,Longitude,PricingRan,WO_Category,DateIncomplete,ReminderAcceptance,Reminder24Hr,Reminder1Hr,CheckInCall,CheckOutCall,ReminderNotMarkComplete,ReminderIncomplete,ReminderAll,SMSBlast,MinutesRemainPublished,MinimumSelfRating,MaximumAllowableDistance,isProjectAutoAssign,isWorkOrdersFirstBidder,P2TPreferredOnly,P2TAlreadyAssigned,SATRecommended,SATPerformance,ReturnPartCount,NewPartCount,Qty_Applicants,Status,DateInvoiced,Work_Out_of_Scope,OutOfScope_Reason,Status,PcntDeduct';
    public function __construct($login, $pass, $filters, $fields, $sortBy, $companyid,$wos, $showPay = false)
    {
//		$user = new Core_Api_User();
//        $user->checkAuthentication(array('login'=>$login, 'password'=>$pass));
//		if (!$user->isAuthenticate()) throw new Core_CSV_Adapter_Exception('User is not authentificated');
//
//		$this->_companyID = $user->getContextCompanyId();
        $this->_companyID = $companyid;
        $this->_filters = $filters;
        $this->_fields = $fields;
        $this->_sortBy = $sortBy;
        $this->_login = $login;
        $this->_pass = $pass;
        $this->_wos = $wos;
		$this->_showPay = $showPay;

		// pull tech data
		$techs = array();
		$wins = array();
		foreach ($this->_wos as $_wo)
        {
            $techs[] = (defined('FLSVersion')) ? $_wo->FLS_ID : $_wo->Tech_ID;
			$wins[] = $_wo->WIN_NUM;
		}
		
		$this->_woAdditionalFields = array();
		if ($this->_companyID == "FATT") {
			$search = new Core_Api_Class;
			$additionalFields = $search->getWorkOrdersAdditionalFields($wins);
			foreach ($additionalFields as $idx => $val) {
				$this->_woAdditionalFields[$val['WINNUM']] = $val;
			}
		}
		$techs = array_unique($techs);
		$tech = new API_Tech;
		if (!empty($techs)) {
			$tech = new API_Tech;
			if ($this->_companyID != "FATT")
				$this->_techs = $tech->lookupID($techs);
			else
				$this->_techs = $tech->lookupID(array_unique($techs), null, false, 22);	//Include cert number for Flex Contractor ID (cert id 22)
		}
    }

	public function getCompanyID() {
		return $this->_companyID;
	}

	public function getShowPay() {
		return $this->_showPay;
	}

	public function getTech($id) {
		if (empty($this->_techs[$id])) return new API_Tech;
		return $this->_techs[$id];
	}

	public function getWoAdditionalFields() {
		return $this->_woAdditionalFields;
	}
    
	public function getWos()
    {
        return $this->_wos;
    }
    /**
     * (non-PHPdoc)
     * @see Core_CSV_Adapter_Interface::getResult()
     */
    public function getResult($offset = 0, $limit = 0)
    {
        return $this->_wos;
    }

}