<?php

/**
 * Interface for data adapter
 *
 * @author Alexander Cheshchevik
 *
 */
interface Core_CSV_Adapter_Interface
{
    /**
     * @param int $offset
     * @param int $limit
     * @return Core_Filter_Result
     */
	public function getResult($offset = 0,$limit = 0);
}
