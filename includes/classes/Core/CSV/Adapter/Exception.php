<?php

/**
 * CSV adapter exception class
 * @author Alexander Cheshchevik
 *
 */
class Core_CSV_Adapter_Exception extends Core_CSV_Exception
{
    
}