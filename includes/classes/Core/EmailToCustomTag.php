<?php
class Core_EmailToCustomTag {
	private $mail;
	public function __construct() {
		$dbInfo = Zend_Registry::get("DB_Info");
		$isTesting = $dbInfo["dbname"] !== "gbailey_technicianbureau";
		$this->mail = new Zend_Mail_Storage_Imap(array(
			'host' => 'imap.emailsrvr.com',
			'user' => 'clc@fieldsolutions.com',
			'password' => 'C1C2013!',
			'ssl' => 'TLS')
		);
	}
	
	protected function getLastRead() {
		$db = Core_Database::getInstance();
		$select = $db->select();
		$select->from("clc_gateway_read", array("msg_id"))->order("id DESC")->limit(1);
		return $db->fetchOne($select);
	}
	
	protected function setLastRead($msg_id) {
		$db = Core_Database::getInstance();
		$db->insert("clc_gateway_read", array("msg_id" => $msg_id, "read_date" => new Zend_Db_Expr("NOW()")));
	}

	public function process() {
		$mId = $this->getLastRead();
		if (empty($mId)) $mId = 1;
		$count = $this->mail->countMessages();
//		$mId = 97;
//		$count = 97;
		for ($mId = $mId; $mId <= $count; ++$mId) {
			$message = $this->mail->getMessage($mId);
			//echo "Mail from '{$message->from}': {$message->subject} {$message->contentType}\n";
			if($message->getContent() != "")
				$techId = trim(filter_var(strtok(strip_tags($message->getContent()),"Company:"), FILTER_SANITIZE_NUMBER_INT));
			$this->setLastRead($mId);
			if ($message->hasFlag(Zend_Mail_Storage::FLAG_SEEN)) continue;
//			echo $message->getContent() . "\n";
			$this->mail->setFlags($mId, array(Zend_Mail_Storage::FLAG_SEEN));

			if (!empty($techId)) {
				try {
					$tagClass = new Core_Api_FSTagClass();
					$date = date("Y-m-d");
					$tagClass->saveFSTagForTech($techId, "64", "", "", $date);
				}
				catch (Exception $e) {
					error_log($e->getMessage());
				}
			}
		}
	}
}
