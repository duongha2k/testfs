<?php

class Core_Combiner_WorkOrder
{
	const COMBINER_MULTIVISIT_OFF = 0;
	const COMBINER_MULTIVISIT_ON = 1;
	const COMBINER_MODE_MORE_DATA = 2; // returns additional info ie. Project Custom Sign Off Sheet. Data should be processed and then removed from the Work Order fields returned by Core_Combiner_WorkOrder by calling removeMoreDataFields
    protected $workOrderData = array();
    protected $companyId = '';
    protected $companyName = '';
    protected $userName = '';
    protected $errors = array();
	protected $projectData = NULL;
	protected $multiVisitFields = 0; // Multivisits fields are not pulled into the work order data by default. This keeps compatiblity where combiner is used but client code can pull MultiVisits info and create the base visit
	protected $mode = NULL;

    function __construct($data, $mv = self::COMBINER_MULTIVISIT_OFF, $mode = NULL)
    {
		if ($data['WO_Category_ID'] == 11) $data['WO_Category_ID'] = 27;	//13102

    	$this->multiVisitFields = $mv;
        $this->workOrderData = $data;
		$this->mode = $mode;
    }

    public function combine($curWO = NULL)
    {		
		if ($curWO == NULL)
			$curWO = array();

		$this->workOrderData['Company_ID'] = $this->companyId;
        $this->workOrderData['Company_Name'] = $this->companyName;
		if ($curWO == null && empty($this->workOrderData['WorkOrderOwner'])) {
			// for new work order, use project owner if none provided
	        $this->workOrderData['WorkOrderOwner'] = $this->workOrderData['Username'];
			$this->userName = $this->workOrderData['Username'];
		}
		if ($curWO == null && empty($this->userName)) {
			$this->userName = $this->workOrderData['WorkOrderOwner'];
		}

		if (empty($this->workOrderData['Project_ID'])) {
			$this->getProjectData($this->workOrderData['Project_Name']);
			$this->workOrderData['Project_ID'] = $this->projectData['Project_ID'];
		}

        if ($this->fieldPresentAndChanged("WorkOrderOwner", $curWO) || ($this->fieldPresentAndChanged("Project_ID", $curWO) && !empty($this->workOrderData['Project_ID']))) {
			// owner changed, or project changed pull in owner info
        	$this->processProjectData((int)$this->workOrderData['Project_ID']);
        }
		else if ($this->fieldPresentAndChanged("Project_Name", $curWO) || (!empty($this->workOrderData['Project_Name']) && empty($this->workOrderData['Project_ID']))) {
        	$this->processProjectData($this->workOrderData['Project_Name']);
		}

//        if ($this->fieldPresentAndChanged("Tech_ID", $curWO)) {
//        	$this->processTechData(Core_Caspio::caspioEscape((int)$this->workOrderData['Tech_ID']));
//        }
        if ($this->fieldPresentAndChanged("WO_Category_ID", $curWO)) {
        	$this->processCategory((int)$this->workOrderData['WO_Category_ID']);
        }
        $this->excludeFields();
        $this->formatData();

        if (!empty($this->errors)) {
        	return false;
        }
        return true;
    }

	private function fieldPresentAndChanged($field, $curWO) {
		return !empty($this->workOrderData[$field]) && (sizeof($curWO) == 0 || $curWO[$field] != $this->workOrderData[$field]);
	}

    private function processProjectData($projectId)
    {
		$projectData = $this->getProjectData($projectId);
		if (!Core_ProjectUsersAllowedCreateWO::isUserAllowedForProjectID($this->workOrderData["Username"], $projectId)) {
			$this->addError('You do not have permission to create Work Orders in this Project');
		}
		else if (!empty($projectData[0])) {
			if ($projectData[0]['Tools']) $projectData[0]['Requirements'] .= "\n" . $projectData[0]['Tools']; //By CR #12563
			unset($projectData[0]['Tools']);
			unset($projectData[0]['AllowCustomCommSettings']);
			$woToProjectFieldMap = array("Project_Manager" => "ProjectManagerName",
					"Project_Manager_Phone" => "ProjectManagerPhone",
					"Project_Manager_Email" => "ProjectManagerEmail",
	
					"Resource_Coordinator" => "ResourceCoordinatorName",
					"Resource_Coord_Phone" => "ResourceCoordinatorPhone",
					"Resource_Coord_Email" => "ResourceCoordinatorEmail",
					"SpecialInstructions_AssignedTech" => "Ship_Contact_Info",
	
					"AutoSMSOnPublish" => "SMSBlast",
					"WorkStartTime" => "StartTime",
					"WorkEndTime" => "EndTime",
	
					"Default_Amount" => "PayMax"
				);
	
			$this->workOrderData["Project_Name"] = $projectData[0]["Project_Name"];
			$this->workOrderData["Project_ID"] = $projectData[0]["Project_ID"];
			
			//13969
			if($projectData[0]["RequiredStartDateTime"] == 1){
				if(empty($this->workOrderData["StartTime"]) || empty($this->workOrderData["StartDate"])){
					//$this->addError('A Start Date and Time is required for this Work Order test');  //14084
				}
			}
			
			if($projectData[0]["RequiredEndDateTime"] == 1){
				if(empty($this->workOrderData["EndTime"]) || empty($this->workOrderData["EndDate"])){
					//$this->addError('An End Date and Time is required for this Work Order'); //14084
				}
			}
			
			$futureToProjectMap = array(
				'SystemGeneratedEmailTo',
				'WorkAvailableEmailFrom',
				'BidNotificationEmailTo',
				'P2TNotificationEmailTo',
				'WorkAssignedEmailTo',
				'WorkDoneEmailTo'
			);
				
			foreach ($futureToProjectMap as $key=>$val) {
				// clear notification emails and always take from project / owner
				$this->workOrderData[$val] = "";
			}
			if ($this->projectData[0]["AllowCustomCommSettings"] == 1)
				// pull owner when done
				$this->processFutureData();
	
			foreach ($projectData[0] as $key=>$val) {
				$buf = $val=="NULL" ? '' : $val;
				if (key_exists($key, $woToProjectFieldMap)) {
					if (empty($this->workOrderData[$woToProjectFieldMap[$key]]) && !empty($buf)) {
						$this->workOrderData[$woToProjectFieldMap[$key]] = $buf;
					}
				} else {
					if (strlen($this->workOrderData[$key]) == 0) {
						$this->workOrderData[$key] = $buf;
					}
				}
			}
			
			if ($this->mode != self::COMBINER_MODE_MORE_DATA) {
				// clears more data fields
				$this->workOrderData = self::removeMoreDataFields($this->workOrderData);
			}
										
			if ($this->multiVisitFields == self::COMBINER_MULTIVISIT_OFF) {
				// clears multivisit fields
				unset($this->workOrderData["TechArrivalInstructions"]);
				unset($this->workOrderData["StartType"]);
			}

			return true;
        } else {
            $this->addError('There is no such Project');
        }
    }
    
    private function processFutureData(){
    	$future = new Core_FutureWorkOrderInfo;
		$futureInfo = $future->find(!empty($this->workOrderData['WorkOrderOwner']) ? $this->workOrderData['WorkOrderOwner'] : $this->userName)->toArray();

		$woToFutureFieldMap = array("ProjectManagerName" => "ProjectManagerName",
								"ProjectManagerPhone" => "ProjectManagerPhone",
								"ProjectManagerEmail" => "ProjectManagerEmail",
								"ResourceCoordinatorName" => "ResourceCoordinatorName",
								"ResourceCoordinatorPhone" => "ResourceCoordinatorPhone",
								"ResourceCoordinatorEmail" => "ResourceCoordinatorEmail",
								"EmergencyName" => "EmergencyName",
								"EmergencyPhone" => "EmergencyPhone",
								"EmergencyEmail" => "EmergencyEmail",
								"CheckInOutName" => "CheckInOutName",
								"CheckInOutNumber" => "CheckInOutNumber",
								"CheckInOutEmail" => "CheckInOutEmail",
								"TechnicalSupportName" => "TechnicalSupportName",
								"TechnicalSupportPhone" => "TechnicalSupportPhone",
								"TechnicalSupportEmail" => "TechnicalSupportEmail",
								"SystemGeneratedEmailTo" => "SystemGeneratedEmailTo",
								"SystemGeneratedEmailFrom" => "SystemGeneratedEmailFrom",
								"BidNotificationEmailTo" => "BidNotificationEmailTo", 
								"P2TNotificationEmailTo" => "P2TNotificationEmailTo",
								"WorkAssignedEmailTo" => "WorkAssignedEmailTo",
								"WorkDoneEmailTo" => "WorkDoneEmailTo",
								"WorkAvailableEmailFrom" => "WorkAvailableEmailFrom",
								"ACSNotifiPOC" => "ACSNotifiPOC",
								"ACSNotifiPOC_Phone" => "ACSNotifiPOC_Phone",
								"ACSNotifiPOC_Email" => "ACSNotifiPOC_Email"
							);
		
		$alwaysOverwrite = array(
								// name of future field to always overwrite even if info is present
								"SystemGeneratedEmailTo" => "SystemGeneratedEmailTo",
								"SystemGeneratedEmailFrom" => "SystemGeneratedEmailFrom",
								"BidNotificationEmailTo" => "BidNotificationEmailTo", 
								"P2TNotificationEmailTo" => "P2TNotificationEmailTo",
								"WorkAssignedEmailTo" => "WorkAssignedEmailTo",
								"WorkDoneEmailTo" => "WorkDoneEmailTo",
								"WorkAvailableEmailFrom" => "WorkAvailableEmailFrom"
							);
   		//overwrite with future info data before project data
        foreach($futureInfo[0] as $k=>$v){
        	$buf = $v=="NULL" ? '' : $v;
        	if(key_exists($k, $woToFutureFieldMap)){
        		if(array_key_exists($k, $alwaysOverwrite) || (empty($this->workOrderData[$woToFutureFieldMap[$k]]) && !empty($buf))){
        			$this->workOrderData[$woToFutureFieldMap[$k]] = $buf;
        		}
        	}
        }
    }

    private function processTechData($techId)
    {
        $fieldsList = 'FirstName,LastName,PrimaryEmail,PrimaryPhone,No_Shows,Back_Outs,ShipTo_Address1';
        $techData = Core_Caspio::caspioSelectAdv(TABLE_MASTER_LIST, $fieldsList, 'TechID = '.$techId, '');

        if (!empty($techData[0])) {
        	$this->workOrderData['Tech_FName']        = $techData[0]['FirstName'];
        	$this->workOrderData['Tech_LName']        = $techData[0]['LastName'];
        	$this->workOrderData['TechEmail']         = $techData[0]['PrimaryEmail'];
        	$this->workOrderData['TechPhone']         = $techData[0]['PrimaryPhone'];
        	return true;
        } else {
            $this->addError('There is no such Tech');
        }
    }

    private function processCategory($categoryId)
    {
		if ($categoryId == 11) $categoryId = 27;	//13102
	$fieldsList = array('Category','Category_ID');
	$fieldsList = array_combine($fieldsList, $fieldsList);

	$db = Core_Database::getInstance();
	$select = $db->select();

	$select->from(Core_Database::TABLE_WO_CATEGORIES);

	$results = Core_Database::fetchAll($select);

        $map = array();
        foreach ($results as $val) {
        	$map[$val['Category_ID']] = $val['Category'];
        }

        if (array_key_exists($categoryId, $map)) {
        	$this->workOrderData['WO_Category'] = $map[$categoryId];
        } else {
		    $this->addError('There is no such Category ID');
        }
    }

    private function formatData()
    {
		if ($this->workOrderData['Approved'] == 1 || strtolower($this->workOrderData['Approved']) == 'true') {
			if (empty($this->workOrderData['SATRecommended']) || $this->workOrderData['SATRecommended'] == '0.00') $this->workOrderData['SATRecommended'] = 3;
			if (empty($this->workOrderData['SATPerformance']) || $this->workOrderData['SATPerformance'] == '0.00') $this->workOrderData['SATPerformance'] = 3;
		}
		if (empty($this->workOrderData['Country'])) $this->workOrderData['Country'] = 'US';
		else {
			$c = new Core_Country($this->workOrderData['Country']);
			if ($c->isValid())
				$this->workOrderData['Country'] = $c->getCode();
		}
		
		if (!empty($this->workOrderData['State'])) {
			$s = new Core_State($this->workOrderData['State'], $this->workOrderData['Country']);
			if ($s->isValid())
				$this->workOrderData['State'] = $s->getCode();
		}

		$urlStripFields = array("Headline", "Description", "Requirements", "SpecialInstructions");
		foreach ($urlStripFields as $field) {
			if (empty($this->workOrderData[$field])) continue;
			$this->workOrderData[$field] = preg_replace("/[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})/","", $this->workOrderData[$field]);
			$this->workOrderData[$field] = preg_replace("/\b(?:(?:https?|ftp):\/\/)?(?:www\d{0,3}[.])?(?:[a-z0-9\-]|.(?!.))+[.](?:com|co|mx|ca|edu|info|biz|net|org)[\/]?(?:[^\s]+)?/","", $this->workOrderData[$field]);
		}
		if (key_exists("SitePhone", $this->workOrderData) && ($this->workOrderData['Country'] == 'US' || $this->workOrderData['Country'] == 'CA')) {
			$getSitePhone = $this->workOrderData["SitePhone"];
			$getSitePhone = trim($getSitePhone,"' ");
			if ($getSitePhone != "" && !preg_match("/^([(](\d{3})[)]\s(\d{3})[-](\d{4})|[(](\d{3})[)]\s(\d{3})[-](\d{4})[^0-9]+[0-9]*)$/", $getSitePhone)) {
				$getSitePhone = preg_replace("/[^0-9]/", "", $getSitePhone);
				if (strlen($getSitePhone) >= 10) {
					$ac = substr($getSitePhone, 0, 3);
					$ex = substr($getSitePhone, 3, 3);
					$num = substr($getSitePhone, 6, 4);
					$ext = substr($getSitePhone, 10);
					$getSitePhone = "($ac) $ex-$num";
					if (strlen($ext) > 0) $getSitePhone .= " x $ext";
					$this->workOrderData["SitePhone"] = $getSitePhone;
				}
			}
		}
		// FORMAT ZIP IF ONLY 4 CHAR (MAIN SITE)
		if (key_exists("Zipcode", $this->workOrderData)) {
			$this->workOrderData["Zipcode"] = trim($this->workOrderData["Zipcode"]);
			$getZipcode = $this->workOrderData["Zipcode"];
			if(strlen($getZipcode) == 4){
				$getZipcode = trim($getZipcode, "'");
				if (is_numeric($getZipcode))
					$this->workOrderData["Zipcode"] = "0".$getZipcode;
		    }
		}
		// FORMAT ZIP IF ONLY 4 CHAR (FLS)
		if (key_exists("Zip", $this->workOrderData)) {
			$getZipcode = $this->workOrderData["Zip"];
			if(strlen($getZipcode) == 4){
				$getZipcode=trim($getZipcode, "'");
				if (is_numeric($getZipcode))
					$this->workOrderData["Zip"]="0".$getZipcode;
			}
		}
		// truncate headline
		if (key_exists("Headline", $this->workOrderData)) {
			$headline = trim($this->workOrderData["Headline"],"'");
			$headline = substr($headline, 0, 40);
			$this->workOrderData["Headline"] = $headline;
		}

		//FORMAT DOLLAR AMOUNTS
		//Tech_Bid_Amount
		if (key_exists("Tech_Bid_Amount", $this->workOrderData)) {
			$Tech_Bid_Amount = trim($this->workOrderData["Tech_Bid_Amount"],"'");
			if ($Tech_Bid_Amount !== "NULL" && $Tech_Bid_Amount !== "" && $Tech_Bid_Amount !== null) {
				$formatted_Tech_Bid_Amount = number_format($Tech_Bid_Amount, 2, ".", "");
				$this->workOrderData["Tech_Bid_Amount"] = $formatted_Tech_Bid_Amount;
			}
		}
		//PayMax
		if (key_exists("PayMax", $this->workOrderData)) {
			$payMax = trim($this->workOrderData["PayMax"],"'");
			if ($payMax !== "NULL" && $payMax !== "" && $payMax !== null) {
				$formatted_payMax = number_format($payMax, 2, ".", "");
				$this->workOrderData["PayMax"] = $formatted_payMax;
			}
		}
		//PayAmt (FLS)
		if (key_exists("PayAmt", $this->workOrderData)) {
			$PayAmt = trim($this->workOrderData["PayAmt"],"'");
			$formatted_PayAmt = number_format($PayAmt, 2);
			$this->workOrderData["PayAmt"] = $PayAmt;
		}

		return true;
    }

    private function excludeFields()
    {
//        $notSettable = array('ReminderAll', 'SMSBlast', 'Reminder24Hr', 'Reminder1Hr', 'CheckInCall', 'CheckOutCall',
//                             'ReminderNotMarkComplete', 'ReminderIncomplete', 'isProjectAutoAssign', 'IsWorkOrderFirstBidder', 'MinimumSelfRating', 'MaximumAllowableDistance', 'TB_UNID');
        $notSettable = array('TB_UNID');
        foreach ($this->workOrderData as $k=>$v) {

            if (in_array($k, $notSettable)) {
            	unset($this->workOrderData[$k]);
            }
        }
    }

    public function getData()
    {
        return $this->workOrderData;
    }

    public function getErrors()
    {
        return $this->errors;
    }

    protected function addError($description)
    {
        $this->errors[] = $description;
    }

    public function setCompanyId($val)
    {
        $this->companyId = Core_Caspio::caspioEscape($val);
    }
    public function setCompanyName($val)
    {
        $this->companyName = Core_Caspio::caspioEscape($val);
    }
    public function setUserName($val)
    {
        $this->userName = Core_Caspio::caspioEscape($val);
    }

	private function getProjectData($projectId) {
		if (empty($projectId)) return $this->projectData;
		if ($this->projectData == NULL) {
			$fieldsList = array('Project_ID','Project_Name','Headline','Project_Manager_Email','Project_Manager_Phone','Project_Manager',
                    'Resource_Coordinator','Resource_Coord_Email','Resource_Coord_Phone',
                    'EmergencyName','EmergencyPhone','EmergencyEmail','TechnicalSupportName','TechnicalSupportEmail',
                    'TechnicalSupportPhone','CheckInOutName','CheckInOutNumber','CheckInOutEmail','WO_Category_ID',
                    'isProjectAutoAssign','isWorkOrdersFirstBidder','MinutesRemainPublished','MinimumSelfRating',
                    'MinimumSelfRating','MaximumAllowableDistance','Description','Requirements','Tools',
                    'SpecialInstructions','SpecialInstructions_AssignedTech','PO','General1','General2','General3','General4','General5','General6','General7',
                    'General8','General9','General10','ReminderAll','ReminderAcceptance','Reminder24Hr','Reminder1Hr',
                    'CheckInCall','CheckOutCall','ReminderNotMarkComplete','ReminderIncomplete','P2TPreferredOnly','SignOffSheet_Required',
		      		'Default_Amount','Qty_Devices','Qty_Visits','Amount_Per','PcntDeduct','AutoSMSOnPublish','WorkStartTime','WorkEndTime','StartRange','Duration','PcntDeductPercent', 'Type_ID',
					'AllowCustomCommSettings', 'TechArrivalInstructions', 'StartType', 'EstimatedDuration', 'CustomSignOff', 'SignOff_Disabled', 'ServiceTypeId', 'EntityTypeId', 'FSClientServiceDirectorId', 'FSAccountManagerId',
					'ACSNotifiPOC', 'ACSNotifiPOC_Phone', 'ACSNotifiPOC_Email','RequiredStartDateTime', 'RequiredEndDateTime', 'PushWM');

			$db = Core_Database::getInstance();
			$select = $db->select();
			$select->from(Core_Database::TABLE_PROJECTS, $fieldsList);
			if (is_numeric($projectId))
				$select->where("Project_ID = ?", $projectId);
			else
				$select->where("Project_Name = ?", $projectId)
					->where("Project_Company_ID = ?", $this->companyId);

			$select->limit(1, 0);

			$this->projectData = Core_Database::fetchAll($select);
		}

		return $this->projectData;
	}
	
	public static function removeMoreDataFields($woData) {
		// unset more data fields returned by combiner
		$fields = array(
			"CustomSignOff", 'ServiceTypeId', 'EntityTypeId', 'FSClientServiceDirectorId', 'FSAccountManagerId',
			'ACSNotifiPOC', 'ACSNotifiPOC_Phone', 'ACSNotifiPOC_Email', 'RequiredStartDateTime', 'RequiredEndDateTime', 'PushWM'
		);
		foreach ($fields as $field) {
			unset($woData[$field]);
		}
		return $woData;
	}	
}
