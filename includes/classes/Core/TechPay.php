<?php
class Core_TechPay {
	protected $winNum;

	public static function recalcFields() {
		// array of fields that require a recalc of tech pay when updated
		$a = array("baseTechPay", "OutofScope_Amount", "TripCharge", "MileageReimbursement", "MaterialsReimbursement", "Additional_Pay_Amount", "AbortFeeAmount", "Amount_Per", "Tech_Bid_Amount", "Qty_Devices", "Qty_Visits", "calculatedTechHrs", "AbortFee", "PcntDeduct", "PcntDeductPercent");
		return $a;
	}

	public function setWinNum($num) {
		$this->winNum = $num;
	}

	public function updateTechPay($newValues) {
		$fields = $newValues;

		$baseTechPay = $fields["baseTechPay"];
		$outOfScopeAmount = $fields["OutofScope_Amount"];
		$tripCharge = $fields["TripCharge"];
		$milageReimbursement = $fields["MileageReimbursement"];
		$materialsReimbursement = $fields["MaterialsReimbursement"];
		$additionalPayAmount = $fields["Additional_Pay_Amount"];
		$abortFeeAmount = $fields["AbortFeeAmount"];
		$Amount_Per = $fields["Amount_Per"];
		$startTime = strtotime($fields["Time_In"]);
		$endTime = strtotime($fields["Time_Out"]);
		$bid_amount = $fields["Tech_Bid_Amount"];
		$qty_devices = $fields["Qty_Devices"];
		$qty_visits = $fields["Qty_Visits"];
		$tech_hrs = $fields["calculatedTechHrs"];
		$abortFee = $fields["AbortFee"];
		$companyID = $fields["Company_ID"];
		$companyName = $fields["Company_Name"];
		$projectName = $fields["Project_Name"];
		$approved = $fields["Approved"];
		$pcntDeduct = $fields["PcntDeduct"];
		$percent = $fields["PcntDeductPercent"];
		$Total_Reimbursable_Expense = $fields['Total_Reimbursable_Expense'];
		$base_pay = $bid_amount;

		if (empty($tech_hrs)) {
			// Calc hours
			if ($endTime < $startTime) {
				$endTime += 86400;
			}

			$calculatedTechHrs = round(($endTime - $startTime)/3600, 2);
		}
		else
		$calculatedTechHrs = $tech_hrs;

		// Turns out they want it in format such as 5.5, not 5:30.
		// $hours = intval(($endTime - $startTime)/3600);
		// $mins = intval((($endTime - $startTime) / 60) % 60);
		// $calculatedTechHrs = $hours . ":" . $mins;

		if (empty($baseTechPay)) {
			if ($Amount_Per == "Hour") {
				// Calc Base Tech Pay
				if ($calculatedTechHrs == 0 ) {
					$base_pay = 0;
				} else {
					$base_pay *= $calculatedTechHrs;
				}
			} else if ($Amount_Per == "Device") {
				$base_pay *= $qty_devices;
			} else if ($Amount_Per == "Visit") {
				$base_pay *= $qty_visits;
			}
				
			$baseTechPay = $base_pay;
		}
		else
		$base_pay = $baseTechPay;

		// Calc Pricing Fees to get total tech pay
		$PayAmount = 0;

		if($abortFee == "True" ) {
			$PayAmount = $abortFeeAmount;
		} else {
			$PayAmount = $baseTechPay;
			$PayAmount += $outOfScopeAmount + $tripCharge + $milageReimbursement + $materialsReimbursement + $additionalPayAmount;
		}

		// let's print the international format for the en_US locale
		$isZ2T = $fields['Tech_ID'] == 49537;
		//376
		if (!empty($Total_Reimbursable_Expense))
			$PayAmount = $PayAmount + $Total_Reimbursable_Expense;
		//end 376
		$Net_Pay_Amount = $pcntDeduct == "True" ? $PayAmount * (1.0 - $percent) : $PayAmount;
		$Z2T_Pay_Amount = $isZ2T ? $Net_Pay_Amount : 0;
		$Net_Pay_Amount = $isZ2T ? 0 : $Net_Pay_Amount;
		$Net_Pay_Amount = sprintf("%01.2f", $Net_Pay_Amount);
		$PayAmount = sprintf("%01.2f", $PayAmount);

		$newValues["PayAmount"] = $PayAmount;
		$newValues["calculatedTechHrs"] = $calculatedTechHrs;
		$newValues["baseTechPay"] = $baseTechPay;
		$newValues["Net_Pay_Amount"] = $Net_Pay_Amount;
		$newValues["Z2T_Pay_Amount"] = $Z2T_Pay_Amount;
		return $newValues;
	}
}
