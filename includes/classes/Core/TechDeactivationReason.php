<?php
class Core_TechDeactivationReason extends Core_LookupTable {
	
	public function __construct() {
		self::getByCode();
		parent::__construct();
	}
		
	public static function getByCode() {
        $cache = Core_Cache_Manager::factory();
        $cacheKey = Core_Cache_Manager::makeKey('techDeactivationReason');
        
        if ( false === ($result = $cache->load($cacheKey)) ) {
            $fieldsList = array('id','reason');

            $db = Core_Database::getInstance();
            $select = $db->select();

            $select->from(Core_Database::TABLE_TECH_DEACTIVATION_REASONS, $fieldsList);
			
            $result = $db->fetchPairs($select);

            $cache->save($result, $cacheKey, array('techDeactivationReason'), LIFETIME_30DAYS);
        }
		
		self::$lookupTable = $result;
		return $result;
	}
}