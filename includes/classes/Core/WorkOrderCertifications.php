<?php
class Core_WorkOrderCertifications extends Core_TableRelatedItem {	
	public function __construct() {
		parent::__construct(Core_Database::TABLE_WORK_ORDER_CERTIFICATION, "WIN_NUM", "certification_id");
	}
	
	public function getWorkOrderCertification($wins) {
		if (is_numeric($wins)) $wins = array($wins);
		if (!is_array($wins)) return false;
		return $this->getItemsWithRelatedIds($wins);
	}
	
	public function getWorkOrderWithCertification($skillIds) {
		if (is_numeric($skillIds)) $skillIds = array($skillIds);
		if (!is_array($skillIds)) return false;
		return $this->getRelatedIdsWithItems($skillIds);
	}

	public function getWorkOrderWithCertificationAny($skillIds) {
		if (is_numeric($skillIds)) $techIds = array($skillIds);
		if (!is_array($skillIds)) return false;
		return $this->getRelatedIdsWithItems($skillIds, false);
	}
	
	public static function getMapping() {
		$cache = Core_Cache_Manager::factory();
		$mapping = $cache->load('workorderCertificationsMapping');
		if ($mapping) return $mapping;
		
		$db = Core_Database::getInstance();
		$select = $db->select();
		$select->from(Core_Database::TABLE_CERTIFICATIONS, array('id', 'name'));
		$mapping = $db->fetchPairs($select);
		$cache->save($mapping, 'workorderCertificationsMapping');
		
		return $mapping;
	}
}