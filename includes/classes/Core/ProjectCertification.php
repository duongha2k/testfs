<?php
class Core_ProjectCertification implements Core_ObjectFromArray {
	protected $Project_ID;
	protected $certification_id;
	protected static $mapping;
	protected $row;
	
	public function __construct($Project_ID = NULL, $certification_id = NULL) {
		$this->Project_ID = $Project_ID;
		$this->certification_id = $certification_id;
		self::init();
	}
	
	private function load() {
		$db = Core_Database::getInstance();
		$table = new Zend_Db_Table(Core_Database::TABLE_PROJECT_CERTIFICATION);
		$this->row = $table->fetchRow($table->select(Zend_Db_Table::SELECT_WITH_FROM_PART)
			->where("Project_ID = ?", $this->Project_ID)
			->where("certification_id = ?", $this->certification_id));
	}
	
	public function loadRow() {
		$this->load();
		return $this->row;
	}
	
	private function prepareRow() {
		$this->load();
		$db = Core_Database::getInstance();
		$table = new Zend_Db_Table(Core_Database::TABLE_PROJECT_CERTIFICATION);
		if (empty($this->row)) $this->row = $table->createRow();
		$this->row->Project_ID = $this->Project_ID;
		$this->row->certification_id = $this->certification_id;
	}

	public function save() {
		$this->prepareRow();
		try {
			$this->row->save();
		}
		catch (Exception $e) {
		}
	}
	
	public function delete() {
		if (empty($this->row)) $this->load();
		if (empty($this->row)) return false;
		try {
			$this->row->delete();
		}
		catch (Exception $e) {
		}
	}

	public static function init() {
		if (!empty(self::$mapping)) return;
		self::$mapping = Core_ProjectCertifications::getMapping();
	}
	
	public function getProjectID() {
		return $this->Project_ID;
	}

	public function getCertificationId() {
		return $this->certification_id;
	}
		
	public static function objectFromArray($array) {
		@$projectID = $array["Project_ID"];
		@$certification_id = $array["certification_id"];
		$me = new self($projectID, $certification_id);
		return $me;
	}
    
    public function getRow()
    {
        $db = Core_Database::getInstance();
        $query = $db->select();        
        $query->from(Core_Database::TABLE_PROJECT_CERTIFICATION);                
        $query->where("Project_ID = ?", $this->Project_ID);        
        $query->where("certification_id = ?", $this->certification_id);        
        $result = Core_Database::fetchAll($query);
        
        if(empty($result)) return null;
        else return $result[0];
    }
    public function loadByProjectId() {
            $db = Core_Database::getInstance();
            $table = new Zend_Db_Table(Core_Database::TABLE_PROJECT_CERTIFICATION);
            return $table->fetchAll($table->select(Zend_Db_Table::SELECT_WITH_FROM_PART)
                    ->where("Project_ID = ?", $this->Project_ID));
    }
    public function deleteByProjectId() {
        $data = $this->loadByProjectId();
        if (empty($data)) return false;
        try {
             foreach($data as $item){
                 $item->delete();
             }
        }
        catch (Exception $e) {
        }
    }
    public function saveExt() {
        $this->deleteByProjectId();
        try {
             $this->save();
        }
        catch (Exception $e) {
        }
    }
    public function getRowByProjectId()
    {
        $db = Core_Database::getInstance();
        $query = $db->select();        
        $query->from(Core_Database::TABLE_PROJECT_CERTIFICATION);                
        $query->where("Project_ID = ?", $this->Project_ID);           
        $result = Core_Database::fetchAll($query);
        
        if(empty($result)) return null;
        else return $result;
    }
}