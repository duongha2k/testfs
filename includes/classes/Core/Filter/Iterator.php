<?php
/**
 *
 * Iterator for API_WorkOrderFilter result
 * @author Dmitry Kamenka
 *
 */
class Core_Filter_Iterator implements Iterator
{
	private $current;
	private $filterResult;
	private $statement;
	private $pos = 0;

	public function __construct(Core_Filter_Result $filterResult)
	{
		$this->filterResult = $filterResult;
		$db = Core_Database::getInstance();
		$this->statement = $db->query($filterResult->getSelect());
		$this->current = $this->statement->fetch();
	}

	public function current()
	{
		if ($this->valid()) {
			return $this->current;
		}
		return null;
	}

	public function key()
	{
		return $this->pos;
	}

	public function next()
	{
		$this->current = $this->statement->fetch();
		$this->pos++;
	}

	public function rewind()
	{
		//Do nothing
	}

	public function valid()
	{
		return (bool)$this->current;
	}
}
