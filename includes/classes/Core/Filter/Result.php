<?php
/**
 *
 * Work orders filter result class
 * @author Dmitry Kamenka
 *
 */
class Core_Filter_Result implements IteratorAggregate, Countable
{
	private $select;

	public function __construct(Zend_Db_Select $select)
	{
		$this->select = $select;
	}

	public function getSelect()
	{
		return $this->select;
	}

	public function getIterator()
	{
		return new Core_Filter_Iterator($this);
	}

	public function count()
	{
		return Core_Database::getCount($this->getSelect());
        //return Core_Database::getInstance()->fetchRow($this->getSelect());
	}

	public function toArray()
	{
		return Core_Database::getInstance()->fetchAll($this->getSelect());
	}
}