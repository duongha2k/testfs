<?php
class Core_DashboardSettings_Project extends Core_DashboardSettings_Abstract
// if no projects listed then shows only my project
{
	protected $_name = 'dashboard_settings_project';
	protected $_primary = array("client_id", "project_id");
	protected static $_fieldlist = array('Project_ID', 'Project_Name', 'Project_Company_ID');
						
	private static function getSelectJoin() {
		$select = self::getSelect();
		$select->from(array("ua" => 'dashboard_settings_project'), array())
			->join(array("p" => Core_Database::TABLE_PROJECTS), "p.Project_ID = ua.Project_ID", self::$_fieldlist)
		->setIntegrityCheck(false);
		return $select;
	}

	public static function getSettings($client_id) {
		// get list of projects the user (client_id) wants visible
		$me = self::getInstance();
		$select = self::getSelectJoin();
		$select->where('ua.client_id = ?', $client_id);
		$rows = $me->getAdapter()->fetchAll($select);
		return self::prepareResults($rows);
	}
	
	public static function saveSettings($client_id, $projects) {
		// save list of project ids to show
		if (!is_numeric($client_id)) return false;
		if (is_numeric($projects)) $projects = array($projects);
		if (!is_array($projects)) return false;
		self::clearSettings($client_id);
		$me = self::getInstance();
		foreach ($projects as $project) {
			$row = $me->createRow();
			$row->client_id = $client_id;
			$row->project_id = $project;
			$row->save();
		}
	}
	
	public static function getProjectsMy($client_id) {
		$db = Core_Database::getInstance();
		$selectMyProject = $db->select();
		$user = new Core_Api_User;
		$user->loadById($client_id);
		$username = $user->getLogin();
		$selectMyProject->from(Core_Database::TABLE_PROJECTS, self::$_fieldlist)
			->where('Owner = ? OR (Owner IS NULL AND CreatedBy = ?)', $username, $username);
		$rows = $db->fetchAll($selectMyProject);
		return self::prepareResults($rows);
	}
	
	protected static function prepareResults($rows) {
		$list = array();
		if (count($rows) == 0) return $list;
		foreach ($rows as $row) {
			$val = $row;
			unset($val['Project_ID']);
			$list[$row['Project_ID']] = $val;
		}
		return $list;		
	}
}
