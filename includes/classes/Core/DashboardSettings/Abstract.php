<?php
abstract class Core_DashboardSettings_Abstract extends Zend_Db_Table_Abstract
{	
	protected static function getInstance() {
		$db = Core_Database::getInstance();
		$myClass = get_called_class();
		return new $myClass(array('db'=>$db));
	}
	
	protected static function getSelect() {
		$me = self::getInstance();
		$select = $me->select();
		return $select;
	}
					
	public static function getSettings($client_id) {
		// get setting for work orders the user (client_id) wants visible
		$me = self::getInstance();
		$select = self::getSelect();;
		$select->where('client_id = ?', $client_id);
		$rows = $me->getAdapter()->fetchRow($select);
		return $rows;
	}
	
	public static function saveSettings($client_id, $filter) {
		// save dashboard 
		if (!is_numeric($client_id)) return false;
//		if (!is_array($filter)) return false;
//		self::clearSettings($client_id);
		$me = self::getInstance();
		$row = $me->find($client_id);
		if (!$row || count($row) == 0)
			$row = $me->createRow();
		else
			$row = $row->getRow(0);
		$row->client_id = $client_id;
		$row->filter = $filter;
		$row->save();
	}
	
	public static function clearSettings($client_id) {
		// clears all work order settings for user
		$me = self::getInstance();
		$me->delete($me->getAdapter()->quoteInto('client_id = ?', $client_id));
	}
}
