<?php
class Core_DashboardSettings_WorkOrder extends Core_DashboardSettings_Abstract
{
	// bitwise flags to allow combination
	const FILTER_NONE = 0;
	const FILTER_CREATED = 1;
	const FILTER_PUBLISHED = 2;
	const FILTER_ASSIGNED = 4;
	const FILTER_APPROVED = 8;
	const FILTER_OWNED = 16;
	protected $_name = 'dashboard_settings_work_order';
	protected $_primary = 'client_id';
	
	public static function isFilterActive($currentFilter, $testFilter) {
		// return if one or more filter is active by passing bitwise flags
		return ($currentFilter & $testFilter) ? true : false;
	}
}
