<?php

class Core_EmailForNewProject
{
    private $mail;
	private $user;
	private $projectID;
	private $projectName;

    function __construct()
    {
        $this->mail = new Core_Mail();
    }
    
    public function setUser(Core_Api_User $user) {
		$this->user = $user;
    }

    public function setProjectID($id) {
		$this->projectID = $id;
    }

    public function setProjectName($name) {
		$this->projectName = $name;
    }
    
    public function sendMail()
    {
		if (!isset($this->user)) return;
		$user = $this->user;
		$client = $user->getRepClientforCompany($user->getContextCompanyId());
		$clientId = $client[0]['ClientID'];
		$clientExt = $user->getClientExt($clientId);
		$clientExt = $clientExt[0];
		$csdId = $clientExt['FSClientServiceDirectorId'];
		$amId = $clientExt['FSAccountManagerId'];
        $common = new Core_Api_CommonClass(); 
		$csd = $common->getOpeStaffById($csdId);
		$am = $common->getOpeStaffById($amId);
		$toEmail = "";
		if (!empty($csd['email'])) $toEmail = $csd['email'];
		if (!empty($am['email'])) {
			$toEmail .= (empty($toEmail) ? "" : ",") . $am['email'];
		}
		if (empty($toEmail)) return; // shouldn't reach here unless test client
		$companyName = $user->getContextCompanyName();
		$username = $user->getContactName() . " - " . $user->UserName;
		$subject = "New Project - $companyName - {$this->projectName}";
	
		$message = "A new project has been created. Please ensure that the pricing rule for this new project is accurate.\n\n" . 
		"Project Name: {$this->projectName}\n" .
		"Project ID: {$this->projectID}\n" .
		"Company: $companyName\n" .
		"User: $username";
	
		$this->mail->setBodyText($message);
		$this->mail->setFromEmail("no-replies@fieldsolutions.com");
		$this->mail->setToEmail($toEmail);
		$this->mail->setSubject($subject);
	
		$this->mail->send();	
    }
}
