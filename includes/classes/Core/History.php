<?php
class Core_History {
	const SECTION_TECH_MAIN = 'main';
	const SECTION_TECH_SKILLS = 'skills';
	const SECTION_TECH_EQUIPMENT = 'equipment';

	public static function logTechProfileUpdate($TechID, $section = self::SECTION_TECH_MAIN) {
		$db = Core_Database::getInstance();
		if (!is_numeric($TechID)) return false;
		try {
			$data = array(
				'TechID' => $TechID,
				'section' => $section,
				'DateChanged' => new Zend_Db_Expr('NOW()')
			);
			$db->insert('history_tech_profile_update', $data);
		} catch (Exception $e) {
		}
	}
}
