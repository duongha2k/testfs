<?php

class Core_EmailForCreateQuickticketWO {

    private $wo;
    private $companyName;
    private $mail;
    private $tomail;
    private $clientName;
    private $CustomerName;
    private $ToMailRC;
    private $ToNameRC;

    function __construct()
    {
        $this->mail = new Core_Mail();
    }

    public function setWorkOrder($workOrder)
    {
        if (is_object($workOrder))
        {
            $this->wo = $workOrder;
        } else
        {
            $this->wo = new Core_WorkOrder((int) $workOrder);
        }
    }

    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName;
    }

    public function setClientName($clientname)
    {
        $this->clientName = $clientname;
    }

    public function setToMail($tomail)
    {
        $this->tomail = $tomail;
    }

    public function setCustomerName($CustomerName)
    {
        $this->CustomerName = $CustomerName;
    }
    
    public function setToMailRC($ToMailRC)
    {
        $this->ToMailRC = $ToMailRC;
    }
    
    public function setToNameRC($ToNameRC)
    {
        $this->ToNameRC = $ToNameRC;
    }
    
    public function sendMail()
    {
        $WIN_NUM = $this->wo->getWIN_NUM();
        $WorkOrderNo = $this->wo->getWO_ID();
        $projectName = $this->wo->getProject_Name();
        $Headline = $this->wo->getHeadline();
        $StartDate = $this->wo->getStartDate();
        $StartDateitem = split("-", $StartDate);
        $StartDate = $StartDateitem[1]."-".$StartDateitem[2]."-".$StartDateitem[0];
        $StartTime = $this->wo->getStartTime();
        $sendToEmail = $this->tomail;
        $companyName = $this->companyName;
        $caller = "wosEmailTechWhenApproved";
        $ClientName = $this->clientName;
        $CustomerName = $this->CustomerName;
        
        $vFromName = "FieldSolutions QuickTicket";
        $vFromEmail = "no-replies@fieldsolutions.com";
        //13330
        $vSubject = "QuickTicket Created - WIN#".$WIN_NUM."";
        if(!empty($sendToEmail))
        {
        $message = "Hello,\n\n
$ClientName at $companyName has created a QuickTicket Work Order that requires action as it has NOT been published or assigned.\n\n 
WIN#: $WIN_NUM\n
Client WO ID: $WorkOrderNo\n
Project: $projectName\n
Headline: $Headline\n
Start Date/Time: $StartDate $StartTime\n
\n\n
Thank you,\n
Your FieldSolutions Team";

        $this->mail->setBodyText($message);
        $this->mail->setFromEmail($vFromEmail);
        $this->mail->setFromName($vFromName);
        $this->mail->setToEmail($sendToEmail);
        $this->mail->setToName($ClientName);
        $this->mail->setSubject($vSubject);
        $this->mail->send();
    }

        if(!empty($this->ToMailRC))
        {
            $vFromEmail = "no-replies@fieldsolutions.com";
            $vFromName = "FieldSolutions";
            $sendToEmail = $this->ToMailRC;
            $vSubject = "New QuickTicket Work Order Created - WIN#".$WIN_NUM.".";
            $message = "Please note that a new QuickTicket Work Order (WIN#: $WIN_NUM, Client WO ID: $WorkOrderNo) has been created by $ClientName at $CustomerName. This Work Order can be found in the Created Tab of the Work Order Dashboard and will not be visible to Technicians until it is reviewed and manually Published.\n
Thank you, \n
Your FieldSolutions Team";
            $this->mail->setBodyText($message);
            $this->mail->setFromEmail($vFromEmail);
            $this->mail->setFromName($vFromName);
            $this->mail->setToEmail($sendToEmail);
            $this->mail->setToName($this->ToNameRC);
            $this->mail->setSubject($vSubject);
            $this->mail->send();
}
    }

}