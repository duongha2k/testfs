<?php

class Core_TechStats {
	const CACHE_ENABLED = 1;
	const CACHE_DISABLED = 2;
	protected $techId;
	protected $imac;
	protected $service;
	protected $preference;
	protected $preferenceTotal;
	protected $performance;
	protected $performanceTotal;
	protected $backOuts;
	protected $noShows;
	public function __construct($techId, $imac, $service, $pref, $prefTotal, $per, $perTotal, $backOuts, $noShows) {
		$this->techId = $techId;
		$this->imac = $imac;
		$this->service = $service;
		$this->preference = $pref;
		$this->preferenceTotal = $prefTotal;
		$this->performance = $per;
		$this->performanceTotal = $perTotal;
		$this->backOuts = $backOuts;
		$this->noShows = $noShows;
		if (!is_numeric($this->techId)) $this->techId = NULL;
		if (!is_numeric($this->imac)) $this->imac = 0;
		if (!is_numeric($this->service)) $this->service = 0;
		if (!is_numeric($this->preference)) $this->preference = 0;
		if (!is_numeric($this->preferenceTotal)) $this->preferenceTotal = 0;
		if (!is_numeric($this->performance)) $this->performance = 0;
		if (!is_numeric($this->performanceTotal)) $this->performanceTotal = 0;
		if (!is_numeric($this->backOuts)) $this->backOuts = 0;
		if (!is_numeric($this->noShows)) $this->noShows = 0;
	}
	
	public function save() {
		$db = Core_Database::getInstance();
		if (empty($this->techId) || !is_numeric($this->techId)) return;
		$table = new Zend_Db_Table(Core_Database::TABLE_TECH_BANK_INFO);
		$table->update(
			array(
				'Qty_IMAC_Calls' => $this->imac,
				'Qty_FLS_Service_Calls' => $this->service,
				'PreferencePercent' =>  $this->preference,
				'SATRecommendedTotal' =>  $this->preferenceTotal,
				'PerformancePercent' => $this->performance,
				'SATPerformanceTotal' => $this->performanceTotal,
				'Back_Outs' => $this->backOuts,
				'No_Shows' => $this->noShows
			),
			$db->quoteInto("TechID = ?", $this->techId)
		);
	}

	public function toArray() {
		return array(
			'imac' => $this->imac,
			'service' => $this->service,
                        'preferencePercent' => $this->preference,
			'recommendedTotal' => $this->preferenceTotal,
			'performancePercent' => $this->performance,
			'performanceTotal' => $this->performanceTotal,
			'backOut' => $this->backOuts,
			'noShow' => $this->noShows
		);
	}

	public static function getLogbookStats($techId, $interval = "", $allowCache = self::CACHE_ENABLED) {
        
//        $allowCache = self::CACHE_DISABLED;//test
        
		$db = Core_Database::getInstance();
		if (is_numeric($techId)) $techId = array($techId);
		if (!is_array($techId)) return false;
        
        //--- find wins
        //14008
        $query = $db->select();
        $query->from(array('w'=>Core_Database::TABLE_WORK_ORDERS),
            array('w.WIN_NUM','w.Tech_ID','w.WO_Category_ID','w.BackOut_Tech','w.NoShow_Tech'));
        $query->joinLeft(array("cat"=>"wo_categories"), 
                "w.WO_Category_ID = cat.Category_ID", 
                array("cat.Category")
        );
        $query->orWhere('w.Approved = 1 AND w.Tech_ID IN (?)', $techId);
        $query->orWhere('w.BackOut_Tech IN (?)', $techId);
        $query->orWhere('w.NoShow_Tech IN (?)', $techId);   
        //end 14008

        $ResultWins = Core_Database::fetchAll($query);
        
        $arrWins = array();
        $arrWins_BackOut = array();
        $arrWins_NoShow = array();
        if(!empty($ResultWins))
        {
            for($i=0;$i<count($ResultWins);$i++)
            {
                $arrWins[]=$ResultWins[$i]['WIN_NUM'];
                if(in_array($ResultWins[$i]['BackOut_Tech'],$techId)){
                    $arrWins_BackOut[]=$ResultWins[$i]['WIN_NUM'];
                }
                if(in_array($ResultWins[$i]['NoShow_Tech'],$techId)){
                    $arrWins_NoShow[]=$ResultWins[$i]['WIN_NUM'];
                }
            }
        }
        
        //14008
        $arrCategory = array();
        if(!empty($ResultWins))
        {
            for($i=0;$i<count($ResultWins);$i++)
            {
                $catID = $ResultWins[$i]['WO_Category_ID'];
                $arrCategory[$catID]=$ResultWins[$i]['Category'];
            }
            $arrCategory = array_unique($arrCategory);
        }        
        //end 14008
 
       //echo('<pre>');print_r($arrWins);
        
        //---
        //$strTechId = implode(",", $techId); //2012-05

		$ikey = "";
		if (!empty($interval)) {
			$ikey = ucwords($interval);
			$ikey = str_replace(" ", "", $interval);
		}
		
		$db = Core_Database::getInstance();
		$cache = Core_Cache_Manager::factory();
        
		
		$fullResult = array();
		$notCachedId = array();
		
		foreach ($techId as $id) {
			$cacheKey = 'getLogbookStats' . $ikey . '_' . (int)$id;
			if ($allowCache == self::CACHE_ENABLED && ($r = $cache->load($cacheKey)) !== false)
				// get from cache
				$fullResult[$id] = $r;
			else
				$notCachedId[] = $id;
		}
		
				
		//if (sizeof($notCachedId) > 0) {	
			$select = $db->select()->from(Core_Database::TABLE_WORK_ORDERS, 
					array(
						'imac' => 'COUNT(CASE WHEN TYPE_ID = 1 THEN 1 ELSE NULL END)',
						'service' => 'COUNT(CASE WHEN TYPE_ID = 2 THEN 1 ELSE NULL END)',
						'preferenceGood' => 'COUNT(CASE WHEN IFNULL(SATRecommended,0) >= 3 THEN SATRecommended ELSE NULL END)',
						'preferenceTotal' => 'COUNT(CASE WHEN IFNULL(SATRecommended,0) <> 0 THEN SATRecommended ELSE NULL END)',
						'performanceGood' => 'COUNT(CASE WHEN IFNULL(SATPerformance,0) >= 2 THEN SATPerformance ELSE NULL END)',
						'performanceTotal' => 'COUNT(CASE WHEN IFNULL(SATPerformance,0) <> 0 THEN SATPerformance ELSE NULL END)',
						'Tech_ID' => 'Tech_ID',
						'WO_Category_ID' => 'WO_Category_ID'
						
					))
					   ->joinLeft(array("categories"=>"wo_categories"), "work_orders.WO_Category_ID = categories.Category_ID", array("Category"))
					   ->where("WIN_NUM IN (?)", $arrWins)
                       ->where("TYPE_ID=1 OR TYPE_ID=2")
                       ->where("Tech_ID IN (?)",$techId)//14008
                       ->where("Approved = 1")//14008
					   ->group('WO_Category_ID')
					   ->order('COUNT(CASE WHEN TYPE_ID = 1 THEN 1 ELSE NULL END)+COUNT(CASE WHEN TYPE_ID = 2 THEN 1 ELSE NULL END) DESC');
					   
			if (!empty($interval)) $select->where('StartDate <= ?' , new Zend_Db_Expr('NOW()') )->where('StartDate >= ?' , new Zend_Db_Expr("DATE_SUB(NOW(), INTERVAL $interval)") );
            
            //echo("select: <pre>"); echo($select->__toString());echo("</pre><br/>");

			
			//$resultOne = $db->fetchAll($select);
			if (!empty($arrWins))
				$resultOne = $db->fetchAll($select);
			else
				$resultOne = array();			

            //14008
            $arrCategory_1 = array();
            if(!empty($resultOne))
            {
                for($i=0;$i<count($resultOne);$i++)
                {
                    $catID = $resultOne[$i]['WO_Category_ID'];
                    $arrCategory_1[$catID]=$resultOne[$i]['Category'];
                }
                $arrCategory_1 = array_unique($arrCategory_1);
            }        
            //echo('<br/>arrCategory_1: <pre>');print_r($arrCategory_1);echo("</pre>");
            //end 14008
//print_r($resultOne);//test
			$rOne = array();
			foreach ($resultOne as $k=>$v) {
				$id = $v['Tech_ID'];
				unset($v['Tech_ID']);
				$rOne[$id] = $v;
			}

			//backOutMain
            if (!empty($interval) && count($arrWins_BackOut)>0){
                $timestampQuery = $db->select();
                $timestampQuery->from(array('t'=>'timestamps'),
                    array('WIN_NUM','DateTime_Stamp'=>'Max(DateTime_Stamp)')
                );
                $timestampQuery->where("Description LIKE '%BackOut_Tech%'");
                $timestampQuery->where('WIN_NUM IN (?)', $arrWins_BackOut);
                $timestampQuery->group('WIN_NUM');  
                $timestampRecords = $db->fetchAll($timestampQuery);
            }

			$select = $db->select();
            $select->from(array('w'=>Core_Database::TABLE_WORK_ORDERS), 
                    array('BackOut_Tech' => 'BackOut_Tech', 'backOutMain' => 'COUNT(BackOut_Tech)', "WO_Category_ID") 
            );
            
            if (!empty($interval) && count($timestampRecords) > 0){
                $select->joinLeft(array('t'=>$timestampQuery),
                    't.WIN_NUM = w.WIN_NUM',
                    array()
                );                
            }
            
			$select->where('BackOut_Tech IN (?)', $techId);
			$select->group('WO_Category_ID');
					   
            if (!empty($interval) && count($timestampRecords) > 0) {                
                $select->where('COALESCE(t.DateTime_Stamp,w.StartDate) <= ?' , new Zend_Db_Expr('NOW()') );
                $select->where('COALESCE(t.DateTime_Stamp,w.StartDate) >= ?' , new Zend_Db_Expr("DATE_SUB(NOW(), INTERVAL $interval)") );
            } else if(!empty($interval)){
                $select->where('w.StartDate <= ?' , new Zend_Db_Expr('NOW()') );
                $select->where('w.StartDate >= ?' , new Zend_Db_Expr("DATE_SUB(NOW(), INTERVAL $interval)") );                
            }
					   
            $rBO = $db->fetchAll($select);
            //echo("<p>rBO-count: ".count($rBO));print_r($rBO);echo("</p>");

			//noShowMain
            if (!empty($interval) && count($arrWins_NoShow)>0){
                $timestampNoShowQuery = $db->select();
                $timestampNoShowQuery->from(array('t'=>'timestamps'),
                    array('WIN_NUM','DateTime_Stamp'=>'Max(DateTime_Stamp)')
                );
                $timestampNoShowQuery->where("Description LIKE '%NoShow_Tech%'");
                $timestampNoShowQuery->where('WIN_NUM IN (?)', $arrWins_NoShow);
                $timestampNoShowQuery->group('WIN_NUM');  
                $timestampNoShowRecords = $db->fetchAll($timestampNoShowQuery);
                        }

			$select = $db->select();
            $select->from(array('w'=>Core_Database::TABLE_WORK_ORDERS), 
                    array('NoShow_Tech' => 'NoShow_Tech', 
                    'noShowMain' => 'COUNT(NoShow_Tech)', "WO_Category_ID") 
            );
            

            if (!empty($interval) && count($timestampNoShowRecords) > 0){
                $select->joinLeft(array('t'=>$timestampNoShowQuery),
                    't.WIN_NUM = w.WIN_NUM',
                    array()
                );                
					}

			$select->where('w.NoShow_Tech IN (?)', $techId);
			$select->group('w.WO_Category_ID');

            
            if (!empty($interval) && count($timestampNoShowRecords) > 0) {                
                $select->where('COALESCE(t.DateTime_Stamp,w.StartDate) <= ?' , new Zend_Db_Expr('NOW()') );
                $select->where('COALESCE(t.DateTime_Stamp,w.StartDate) >= ?' , new Zend_Db_Expr("DATE_SUB(NOW(), INTERVAL $interval)") );
            } else if(!empty($interval)){
                $select->where('w.StartDate <= ?' , new Zend_Db_Expr('NOW()') );
                $select->where('w.StartDate >= ?' , new Zend_Db_Expr("DATE_SUB(NOW(), INTERVAL $interval)") );                
				}
				
            $rNS = $db->fetchAll($select);
            //echo("<p>rNS-count: ".count($rNS));print_r($rNS);echo("</p>");

                        //14008
			$returnArr = array();                        
			foreach($resultOne as $res){
                if($res['WO_Category_ID']==0)
                {
                   $res['WO_Category_ID'] = 26;
                   $res['Category']= 'General';
                        }
				$prefAvg = $res['preferenceTotal'] > 0 ? ($res['preferenceGood'] / $res['preferenceTotal']) * 100 : 0;
				$perAvg = $res['performanceTotal'] > 0 ? ($res['performanceGood'] / $res['performanceTotal']) * 100 : 0;
				$returnArr[$res['WO_Category_ID']]["preferenceTotal"] = $res['preferenceTotal'];
				$returnArr[$res['WO_Category_ID']]["performanceTotal"] = $res['performanceTotal'];
				$returnArr[$res['WO_Category_ID']]["preferenceAvg"] = $prefAvg;
				$returnArr[$res['WO_Category_ID']]["performanceAvg"] = $perAvg;
				$returnArr[$res['WO_Category_ID']]['WO_Category_ID'] = $res['WO_Category_ID'];
				$returnArr[$res['WO_Category_ID']]['imac'] = $res['imac'];
				$returnArr[$res['WO_Category_ID']]['service'] = $res['service'];
				$returnArr[$res['WO_Category_ID']]['Category'] = $res['Category'];
                                
				if(!empty($rBO))
                {
                    $returnArr[$res['WO_Category_ID']]['backOuts'] = 0;
                    foreach($rBO as $bo)
                    {
                        if($bo['WO_Category_ID'] == $res['WO_Category_ID'])
                        {
                            $returnArr[$res['WO_Category_ID']]['backOuts']= $bo['backOutMain'];
                            break;       
					}

                    }
				}else{
					$returnArr[$res['WO_Category_ID']]['backOuts'] = 0;
				}				
				
				if(!empty($rNS))
                {
                    $returnArr[$res['WO_Category_ID']]['noShows'] = 0;
					foreach($rNS as $ns)
                    {
                        if($ns['WO_Category_ID'] == $res['WO_Category_ID'])
                        {
                            $returnArr[$res['WO_Category_ID']]['noShows']=$ns['noShowMain'];
                            break;                            
                        }
					}
				}else{
					$returnArr[$res['WO_Category_ID']]['noShows'] = 0;
				}				
			}//and of foreach($resultOne as $res)				
        
        //14008
        if(count($arrCategory)>0){
            foreach($arrCategory as $catID=>$Category){
                
                if(!isset($arrCategory_1[$catID])){ 
                    $backOut = 0;
                    $noShow = 0;                   
                    if(!empty($rBO)) {
                        foreach($rBO as $bo) {
                            if($bo['WO_Category_ID'] == $catID) {
                                $backOut = $bo['backOutMain'];
                                break;       
                            }
                        }
                    }
                    
                    if(!empty($rNS)) {
                        foreach($rNS as $ns) {
                            if($ns['WO_Category_ID'] == $catID) {
                                $noShow = $ns['noShowMain'];
                                break;       
                            }
                        }
                    }
                    
                    if($backOut > 0 || $noShow > 0){
                        if($catID==0){
                            $catID = 26;
                            $Category = 'General';
                        }
                        
                        if(isset($returnArr[$catID])){
                            $returnArr[$catID]['backOuts'] += $backOut;
                             $returnArr[$catID]['noShows'] += $noShow;
                        } else {
                            $returnArr[$catID]["preferenceTotal"] = 0;
                            $returnArr[$catID]["performanceTotal"] = 0;
                            $returnArr[$catID]["preferenceAvg"] = 0;
                            $returnArr[$catID]["performanceAvg"] = 0;
                            $returnArr[$catID]['WO_Category_ID'] = $catID;
                            $returnArr[$catID]['imac'] = 0;
                            $returnArr[$catID]['service'] = 0;
                            $returnArr[$catID]['Category'] = $Category;
                            $returnArr[$catID]['backOuts'] = 0;
                            $returnArr[$catID]['noShows'] = 0;
                            $returnArr[$catID]['backOuts']= $backOut;
                            $returnArr[$catID]['noShows']= $noShow;                            
                        }

                    }
                }//and of for $arrCategory
            }
        }
        //end 14008     
				
		return $returnArr;

	}

	
    protected static function getStats($techId, $interval = "", $allowCache = self::CACHE_ENABLED,$companyID='') {
     
//        $allowCache = self::CACHE_DISABLED;
        $db = Core_Database::getInstance();
        if (is_numeric($techId)) $techId = array($techId);
        if (!is_array($techId)) return false;
        //---

        //---
        $ikey = "";
        if (!empty($interval)) {
            $ikey = ucwords($interval);
            $ikey = str_replace(" ", "", $interval);
        }

        $fullResult = array();
        
        $db = Core_Database::getInstance();
        $cache = Core_Cache_Manager::factory();
        
        $fullResult = array();
        $notCachedId = array();
        
        foreach ($techId as $id) {
            $cacheKey = 'getStats' . $ikey . '_' . $companyID . (int)$id;
            if ($allowCache == self::CACHE_ENABLED && ($r = $cache->load($cacheKey)) !== false){
                // get from cache
                $fullResult[$id] = $r;
            }else
                $notCachedId[] = $id;
        }
        //---
        //echo('<pre>');print_r($arrWins);
        //---        
        if (sizeof($notCachedId) > 0) {    
 
            //--- find wins       
            $query = $db->select();
            $query->from(Core_Database::TABLE_WORK_ORDERS,array('WIN_NUM','Tech_ID','BackOut_Tech','NoShow_Tech'));
            //13943
            if(!empty($companyID)){
                $query->where("Company_ID='$companyID'");
            }
            $or1 = $db->quoteInto('Approved=1 AND Tech_ID IN (?)', $notCachedId);
            $or2 = $db->quoteInto('BackOut_Tech IN (?)', $notCachedId);
            $or3 = $db->quoteInto('NoShow_Tech IN (?)', $notCachedId);
            $query->where("($or1) OR ($or2) OR ($or3)");
            //print_r($query->__toString());
            //end 13943
            $ResultWins = Core_Database::fetchAll($query);
            
            $arrWins = array();
            $arrWins_BackOut = array();
            $arrWins_NoShow = array();
            if(!empty($ResultWins))
            {
                for($i=0;$i<count($ResultWins);$i++)
                {
                    $arrWins[]=$ResultWins[$i]['WIN_NUM'];
                    if(in_array($ResultWins[$i]['BackOut_Tech'],$notCachedId)){
                        $arrWins_BackOut[]=$ResultWins[$i]['WIN_NUM'];
                }
                    if(in_array($ResultWins[$i]['NoShow_Tech'],$notCachedId)){
                        $arrWins_NoShow[]=$ResultWins[$i]['WIN_NUM'];
            }
                }
            }

            $select = $db->select()->from(Core_Database::TABLE_WORK_ORDERS, 
                    array(
                        'imac' => 'COUNT(CASE WHEN TYPE_ID = 1 THEN 1 ELSE NULL END)',
                        'service' => 'COUNT(CASE WHEN TYPE_ID = 2 THEN 1 ELSE NULL END)',
                        'preferenceGood' => 'COUNT(CASE WHEN IFNULL(SATRecommended,0) >= 3 THEN SATRecommended ELSE NULL END)',
                        'preferenceTotal' => 'COUNT(CASE WHEN IFNULL(SATRecommended,0) <> 0 THEN SATRecommended ELSE NULL END)',
                        'performanceGood' => 'COUNT(CASE WHEN IFNULL(SATPerformance,0) >= 2 THEN SATPerformance ELSE NULL END)',
                        'performanceTotal' => 'COUNT(CASE WHEN IFNULL(SATPerformance,0) <> 0 THEN SATPerformance ELSE NULL END)',
                        'Tech_ID' => 'Tech_ID'
                    ));
                       
                       
            if (!empty($arrWins)) {
                $select->where('WIN_NUM IN (?)', $arrWins);         
            }
            
            if (!empty($interval)) {
                $select->where('StartDate <= ?' , new Zend_Db_Expr('NOW()') );
                $select->where('StartDate >= ?' , new Zend_Db_Expr("DATE_SUB(NOW(), INTERVAL $interval)") );   
            }
			$select->group('Tech_ID');
			
			if (!empty($arrWins)){
				$resultOne = $db->fetchAll($select);
			}
			else{
				$resultOne = array();	
			}		

            $rOne = array();
            foreach ($resultOne as $k=>$v) {
                $id = $v['Tech_ID'];
                unset($v['Tech_ID']);
                $rOne[$id] = $v;
            }

            //backOutMain, 14026
            if (!empty($interval) && count($arrWins_BackOut) > 0){
                $timestampQuery = $db->select();
                $timestampQuery->from(array('t'=>'timestamps'),
                    array('WIN_NUM','DateTime_Stamp'=>'Max(DateTime_Stamp)')
                );
                $timestampQuery->where("Description LIKE '%BackOut_Tech%'");
                $timestampQuery->where('WIN_NUM IN (?)', $arrWins_BackOut);
                $timestampQuery->where('t.DateTime_Stamp <= ?' , new Zend_Db_Expr('NOW()') );
                $timestampQuery->where('t.DateTime_Stamp >= ?' , new Zend_Db_Expr("DATE_SUB(NOW(), INTERVAL $interval)") );
                $timestampQuery->group('WIN_NUM');                
                $timestampRecords = $db->fetchAll($timestampQuery);
            }            
            
            $select = $db->select();
            $select->from(array('w'=>Core_Database::TABLE_WORK_ORDERS),
                    array('BackOut_Tech' => 'BackOut_Tech',
                    'backOutMain' => 'COUNT(w.BackOut_Tech)') 
            );
            
            if (!empty($interval) && count($timestampRecords) > 0){
                $select->joinLeft(array('t'=>$timestampQuery),
                    't.WIN_NUM = w.WIN_NUM',
                    array()
                );                
            }            
                
            $select->where('w.BackOut_Tech IN (?)', $notCachedId);
            $select->group('w.BackOut_Tech');
                       
            if (!empty($interval) && count($timestampRecords) > 0) {                
                $select->where('COALESCE(t.DateTime_Stamp,w.StartDate) <= ?' , new Zend_Db_Expr('NOW()') );
                $select->where('COALESCE(t.DateTime_Stamp,w.StartDate) >= ?' , new Zend_Db_Expr("DATE_SUB(NOW(), INTERVAL $interval)") );
            } else if(!empty($interval)){
                $select->where('w.StartDate <= ?' , new Zend_Db_Expr('NOW()') );
                $select->where('w.StartDate >= ?' , new Zend_Db_Expr("DATE_SUB(NOW(), INTERVAL $interval)") );                
            }
            
            //13943
            if(!empty($companyID)){
                $select->where("w.Company_ID='$companyID'");
            }            
            //end 13943
            $rBO = $db->fetchPairs($select);
        //print_r($select->__toString());//test
        //print_r($rBO);//test
        //print_r($select->__toString());//test
        //die();//test
                                        
            //noShowMain
            if (!empty($interval) && count($arrWins_NoShow) > 0){
                $timestampNoShowQuery = $db->select();
                $timestampNoShowQuery->from(array('t'=>'timestamps'),
                    array('WIN_NUM','DateTime_Stamp'=>'Max(DateTime_Stamp)')
                );
                $timestampNoShowQuery->where("Description LIKE '%NoShow_Tech%'");
                $timestampNoShowQuery->where('WIN_NUM IN (?)', $arrWins_NoShow);
                $timestampNoShowQuery->where('t.DateTime_Stamp <= ?' , new Zend_Db_Expr('NOW()') );
                $timestampNoShowQuery->where('t.DateTime_Stamp >= ?' , new Zend_Db_Expr("DATE_SUB(NOW(), INTERVAL $interval)") );
                $timestampNoShowQuery->group('WIN_NUM');                
                $timestampNoShowRecords = $db->fetchAll($timestampNoShowQuery);
                }
            
            $select = $db->select();
            $select->from(array('w'=>Core_Database::TABLE_WORK_ORDERS), 
                array('NoShow_Tech' => 'w.NoShow_Tech', 
                    'noShowMain' => 'COUNT(w.NoShow_Tech)') 
                    );
                    
            if (!empty($interval) && count($timestampNoShowRecords) > 0){
                $select->joinLeft(array('t'=>$timestampNoShowQuery),
                    't.WIN_NUM = w.WIN_NUM',
                    array()
                );                
                }
        
            $select->where('w.NoShow_Tech IN (?)', $notCachedId);
            $select->group('w.NoShow_Tech');

            if (!empty($interval) && count($timestampNoShowRecords) > 0) {                
                $select->where('COALESCE(t.DateTime_Stamp,w.StartDate) <= ?' , new Zend_Db_Expr('NOW()') );
                $select->where('COALESCE(t.DateTime_Stamp,w.StartDate) >= ?' , new Zend_Db_Expr("DATE_SUB(NOW(), INTERVAL $interval)") );
            } else if(!empty($interval)){
                $select->where('w.StartDate <= ?' , new Zend_Db_Expr('NOW()') );
                $select->where('w.StartDate >= ?' , new Zend_Db_Expr("DATE_SUB(NOW(), INTERVAL $interval)") );                
            }

            //13943
            if(!empty($companyID)){
                $select->where("Company_ID='$companyID'");
        	}
            //end 13943

            $rNS = $db->fetchPairs($select);
                    
            foreach ($notCachedId as $id) {
                if (!empty($rOne[$id])) 
                {
                    $pref = $rOne[$id]['preferenceTotal'] > 0 ? ($rOne[$id]['preferenceGood'] / $rOne[$id]['preferenceTotal']) * 100 : 0;
                    $per = $rOne[$id]['performanceTotal'] > 0 ? ($rOne[$id]['performanceGood'] / $rOne[$id]['performanceTotal']) * 100 : 0;
                    $fullResult[$id] = new self($id, $rOne[$id]['imac'], $rOne[$id]['service'], $pref, $rOne[$id]['preferenceTotal'], $per, $rOne[$id]['performanceTotal'], isset($rBO[$id]) ? $rBO[$id] : 0, isset($rNS[$id]) ? $rNS[$id] : 0);
    			}
                else {
                    $fullResult[$id] = new self($id, 0, 0, 0, 0, 0, 0, isset($rBO[$id]) ? $rBO[$id] : 0, isset($rNS[$id]) ? $rNS[$id] : 0);
				}
                if (empty($interval)&& empty($companyID))//14022
                    $fullResult[$id]->save();
                    
               // $cacheKey = 'getStats' . $ikey . '_' . (int)$id;
               $cacheKey = 'getStats' . $ikey . '_' . $companyID . (int)$id;
                $cache->save($fullResult[$id], $cacheKey , array(), 900);
			}
        }
                
        return $fullResult;

    }
	
    //13943
	public static function getStatsWithTechID($techIds, $interval = "", $allowCache = self::CACHE_ENABLED,$companyID="") {
		$db = Core_Database::getInstance();
//		$allowCache = self::CACHE_DISABLED; //test
		return self::getStats($techIds, $interval, $allowCache,$companyID);
	}
	
	public static function recalcLifetimeStatsWithTechID($techId) {
		$t = self::getStatsWithTechID($techId, "", self::CACHE_DISABLED);
	}
	
	public static function recalcLifetimeStatsAll() {
		$db = Core_Database::getInstance();
		$select = $db->select();
		$select->from(Core_Database::TABLE_WORK_ORDERS, array("Tech_ID"))->distinct()
			->where("SATRecommended IS NOT NULL AND SATRecommended != 0 AND Tech_ID IS NOT NULL AND Tech_ID <> ''");

		$list = $db->fetchCol($select);
		$list1Flip = array_flip($list);
		if ($list && sizeof($list) > 0) {
			foreach($list as $techId) {
				echo "Calc: $techId\n";
				self::recalcLifetimeStatsWithTechID($techId);
			}
		}
		$select = $db->select();
		$select->from(Core_Database::TABLE_WORK_ORDERS, array("Tech_ID"))->distinct()
			->where("SATPerformance IS NOT NULL AND SATPerformance != 0 AND Tech_ID IS NOT NULL AND Tech_ID <> ''");
		$list = $db->fetchCol($select);
		if ($list && sizeof($list) > 0) {
			foreach($list as $techId) {
				if (array_key_exists($techId, $list1Flip)) { 
					echo "Skipping: $techId\n";
					continue;
				}
				echo "Calc: $techId\n";
				self::recalcLifetimeStatsWithTechID($techId);
			}
		}
	}
    
    //13970
    /*
        $interval: '', '12 Month','90 Day'
    */
    public static function getDenyTechIDArray_NoShowGreater($companyID,$noShowGreater,$interval = "")
    {
        //--- get techs prefered by the company        
        $techIDArray_Prefered = self::getTechIDArray_PreferedByCompany($companyID);        
        //--- TechIDArray_NoShowGreater
        $db = Core_Database::getInstance();
        $query = $db->select();
        $query->from(Core_Database::TABLE_WORK_ORDERS,array('NoShow_Tech'));
        //$query->where("Approved=1");
        //$query->where("Company_ID = '$companyID'");
        $query->where("NoShow_Tech IS NOT NULL AND NoShow_Tech <> ''");  
        
        if (!empty($interval)) $query->where('StartDate <= ?' , new Zend_Db_Expr('NOW()') )->where('StartDate >= ?' , new Zend_Db_Expr("DATE_SUB(NOW(), INTERVAL $interval)") );
        
        if(count($techIDArray_Prefered) > 0){
            $query->where("NoShow_Tech NOT IN (?)",$techIDArray_Prefered);            
        }
        
        $query->group("NoShow_Tech");
        $query->having("Count(*) >= '$noShowGreater'");
        
        $records = Core_Database::fetchAll($query);
        
        $denyTechArray = array();
        if(!empty($records) && count($records) > 0)
        {
            foreach($records as $rec){
                if($rec['NoShow_Tech'] > 0){
                    $denyTechArray[] = $rec['NoShow_Tech'];
                }
            }
        }
        return $denyTechArray;
    }
    
    //13970
    
    /*
        $interval: '', '12 Month','90 Day'
    */
    public static function getDenyTechIDArray_BackOutGreater($companyID,$backOutGreater,$interval = "")
    {
        //--- get techs prefered by the company        
        $techIDArray_Prefered = self::getTechIDArray_PreferedByCompany($companyID);

        //---  TechIDArray_BackOutGreater
        $db = Core_Database::getInstance();
        $query = $db->select();
        $query->from(Core_Database::TABLE_WORK_ORDERS,array('BackOut_Tech'));
        //$query->where("Approved=1");
        //$query->where("Company_ID = '$companyID'");
        $query->where("BackOut_Tech IS NOT NULL AND BackOut_Tech <> ''");   
        
        if (!empty($interval)) $query->where('StartDate <= ?' , new Zend_Db_Expr('NOW()') )->where('StartDate >= ?' , new Zend_Db_Expr("DATE_SUB(NOW(), INTERVAL $interval)") );

        if(count($techIDArray_Prefered) > 0){
            $query->where("BackOut_Tech NOT IN (?)",$techIDArray_Prefered);            
        }
        
        $query->group("BackOut_Tech");
        $query->having("Count(*) >= '$backOutGreater'");
        //print_r($query->__toString());
        $records = Core_Database::fetchAll($query);
        
        $denyTechArray = array();
        if(!empty($records) && count($records) > 0)
        {
            foreach($records as $rec){
                if($rec['BackOut_Tech'] > 0){
                    $denyTechArray[] = $rec['BackOut_Tech'];
                }
            }
        }
        return $denyTechArray;
    }

    
    /* 13970
        $interval = '', '12 Month','90 Day'
        $ratingSmaller = 0.50, 0.75, 0.90, 0.95
    */
    public static function getDenyTechIDArray_PreferenceRatingSmaller($companyID,
        $ratingSmaller,$interval = "")
    {
        if(empty($ratingSmaller)){
            return array();
        }
        
        //--- techIDArray_Prefered
        $techIDArray_Prefered = self::getTechIDArray_PreferedByCompany($companyID);

        //--- denyTechArray   
        $denyTechArray = array();
     
        $db = Core_Database::getInstance();
        if(empty($interval)){
            $query = $db->select();
            $query->from("tech_preference_count",
                array('Tech_ID','Preference_Total','Preference_Good'));
            $query->where("Preference_Total > 0 ");
            $query->where("Tech_ID > 0 ");
            $query->where("Preference_Good <= (Preference_Total * $ratingSmaller) ");
            
            if(!empty($techIDArray_Prefered) && count($techIDArray_Prefered)>0){
                $query->where("Tech_ID NOT IN (?)",$techIDArray_Prefered);            
            }           
            
            $records = Core_Database::fetchAll($query);
            if(!empty($records) && count($records) > 0)
            {
                foreach($records as $rec){
                    $denyTechArray[] = $rec['Tech_ID'];
                }
            }
            
        } else {
            $query = $db->select();
            $query->from(Core_Database::TABLE_WORK_ORDERS,
                    array(
                            'preferenceGood' => 'COUNT(CASE WHEN IFNULL(SATRecommended,0) >= 3 THEN SATRecommended ELSE NULL END)',
                            'preferenceTotal' => 'COUNT(CASE WHEN IFNULL(SATRecommended,0) <> 0 THEN SATRecommended ELSE NULL END)',
                            'Tech_ID' => 'Tech_ID'
                        )
            );
            $query->where("Approved=1");
            $query->where("Tech_ID > 0");   
            
            if(!empty($techIDArray_Prefered) && count($techIDArray_Prefered)>0){
                $query->where("Tech_ID NOT IN (?)",$techIDArray_Prefered);            
            }        
            
            //$query->where("Company_ID = '$companyID'");
            
            if (!empty($interval)) $query->where('StartDate <= ?' , new Zend_Db_Expr('NOW()') )->where('StartDate >= ?' , new Zend_Db_Expr("DATE_SUB(NOW(), INTERVAL $interval)") );
            
            $query->group("Tech_ID");        
            
            //print_r($query->__toString());return;//test
            $records = Core_Database::fetchAll($query);
            if(!empty($records) && count($records) > 0)
            {
                foreach($records as $rec){
                    $total = $rec['preferenceTotal'];
                    $good = $rec['preferenceGood'];
                    $rate = $total > 0 ? ($good/$total) : 0;        
                                            
                    if($total > 0 && $rate <= $ratingSmaller){
                        $denyTechArray[] = $rec['Tech_ID'];
                    }
                }
            }
        }//end of if
        
        
        return $denyTechArray;
    }

    /* 13970
        $interval = '', '12 Month','90 Day'
        $ratingSmaller = 0.50, 0.75, 0.90, 0.95
    */
    public static function getDenyTechIDArray_PerformanceRatingSmaller($companyID,$ratingSmaller, $interval = "")
    {
        if(empty($ratingSmaller)){
            return array();
        }
     
        //--- techIDArray_Prefered
        $techIDArray_Prefered = self::getTechIDArray_PreferedByCompany($companyID);
   
        //--- denyTechArray
        $db = Core_Database::getInstance();
        $denyTechArray = array();
        
        if(empty($interval)){
            $query = $db->select();
            $query->from("tech_perf_count",
                array('Tech_ID','Performance_Total','Performance_Good'));
            $query->where("Performance_Total > 0 ");
            $query->where("Tech_ID > 0 ");
            $query->where("Performance_Good <= (Performance_Total * $ratingSmaller) ");
            
            if(!empty($techIDArray_Prefered) && count($techIDArray_Prefered)>0){
                $query->where("Tech_ID NOT IN (?)",$techIDArray_Prefered);            
            }           
            
            $records = Core_Database::fetchAll($query);
            if(!empty($records) && count($records) > 0)
            {
                foreach($records as $rec){
                    $denyTechArray[] = $rec['Tech_ID'];
                }
            }
        } else {
            $query = $db->select();
            $query->from(Core_Database::TABLE_WORK_ORDERS,
                    array(
                            'performanceGood' => 'COUNT(CASE WHEN IFNULL(SATPerformance,0) >= 2 THEN SATPerformance ELSE NULL END)',
                            'performanceTotal' => 'COUNT(CASE WHEN IFNULL(SATPerformance,0) <> 0 THEN SATPerformance ELSE NULL END)',
                            'Tech_ID' => 'Tech_ID'
                        )
            );
            $query->where("Approved=1");
            $query->where("Tech_ID > 0");   

            if(!empty($techIDArray_Prefered) && count($techIDArray_Prefered)>0){
                $query->where("Tech_ID NOT IN (?)",$techIDArray_Prefered);            
            }        
                
            //$query->where("Company_ID = '$companyID'");
            
            if (!empty($interval)) $query->where('StartDate <= ?' , new Zend_Db_Expr('NOW()') )->where('StartDate >= ?' , new Zend_Db_Expr("DATE_SUB(NOW(), INTERVAL $interval)") );
        
            $query->group("Tech_ID");     
            
            //echo("<br/>");print_r($query->__toString());die();
               
            $records = Core_Database::fetchAll($query);
            
            if(!empty($records) && count($records) > 0)
            {
                foreach($records as $rec){                    
                        $total = $rec['performanceTotal'];
                        $good = $rec['performanceGood'];
                        $rate = $total > 0 ? ($good/$total) : 0;        
                                                
                        if($total > 0 && $rate <= $ratingSmaller){
                            $denyTechArray[] = $rec['Tech_ID'];
                            //$denyTechArray[] = $rec['Tech_ID'].' '.$total.' '.$good;//test
                        }                       
                }
            }            
        } // end 
        
        
        return $denyTechArray;
    }
    
    //13970
    public static function getTechIDArray_PreferedByCompany($companyID)
    {
        $db = Core_Database::getInstance();
        $query_1 = $db->select();
        $query_1->from('client_preferred_techs',array('Tech_ID'));
        $query_1->where("CompanyID = '$companyID'");
        $query_1->group("Tech_ID");
        $records_1 = Core_Database::fetchAll($query_1);
        
        $techIDArray_Prefered = array();
        if(!empty($records_1) && count($records_1) > 0)
        {
            foreach($records_1 as $rec1){
                if($rec1['Tech_ID'] > 0){
                    $techIDArray_Prefered[] = $rec1['Tech_ID'];
                }
            }
        }
        return $techIDArray_Prefered;
    }

}
