<?php

class Core_EmailForNewInstallDeskWorkOrder
{

    private $wo;
    private $ackFrom;
    private $ackTo;
    private $notiFrom;
    private $notiTo;
    private $mail;

    function __construct()
    {
        $this->mail = new Core_Mail();
    }
    
    public function setAckFrom($from) {
	$this->ackFrom = $from;
    }

    public function setAckTo($to) {
	$this->ackTo = $to;
    }

    public function setNotiFrom($from) {
	$this->notiFrom = $from;
    }

    public function setNotiTo($to) {
	$this->notiTo = $to;
    }

    public function setWorkOrder($workOrder)
    {
        if (is_object($workOrder)) {
        	$this->wo = $workOrder;
        } else {
            $this->wo = new Core_WorkOrder($workOrder);
        }
    }
    
    public function sendMail()
    {
	$subject = $this->wo->getUsername() . ", " . $this->wo->getSiteName() . " Sourcing Request Received";

	$message = "The FLS Resource team has received your request for resources.

Work Order No : " . $this->wo->getWO_ID() . "

Please allow for 5 business days for sourcing this request.

If this is a short notice request please contact the escalation line directly.


Thank you for your continued support of FLS.

FLS Team

(214) 429-4123";

	$this->mail->setBodyText($message);
	$this->mail->setFromEmail($this->ackFrom);
//	$this->mail->setFromName($this->ackFrom);
	$this->mail->setToEmail($this->ackTo);
//	$this->mail->setToName($this->ackTo);
	$this->mail->setSubject($subject);

	$this->mail->send();

	$dateEntered = $this->wo->getDateEntered();
	$dateEntered = empty($dateEntered) ? "" : date("m/d/Y", strtotime($this->wo->getDateEntered()));
	$startDate = $this->wo->getDateEntered();
	$startDate = empty($startDate) ? "" : date("m/d/Y", strtotime($this->wo->getStartDate()));

	$subject = "Sourcing Request $startDate by " . $this->wo->getUsername();

	$message = "Win #: " . $this->wo->getTB_UNID() . "
Work Order #: " . $this->wo->getWO_ID() . "
DateEntered :  " . $dateEntered . "
StartTime : " . $this->wo->getStartTime() . "
Description: " . $this->wo->getDescription() . "
CallType : Install
ProjectName:  " . $this->wo->getProjectName() . "
SiteName : " . $this->wo->getSiteName() . "
Address1 : " . $this->wo->getAddress() . "
City :  " . $this->wo->getCity() . "
State : " . $this->wo->getState() . "
Zip : " . $this->wo->getZipcode() . "
StartDate : " . $startDate . "
StartTime : " . $this->wo->getStartTime() . "
SiteList : " . "" . "
Addedby: " . $this->wo->getAddedby() . "
Email: $this->ackTo
Lead:  " . (strtolower($this->wo->getLead()) == "true" ? 'Yes' : 'No') . "
Assist: " . (strtolower($this->wo->getAssist()) == "true" ? 'Yes' : 'No');

	$this->mail = new Core_Mail();
	$this->mail->setBodyText($message);
	$this->mail->setFromEmail($this->notiFrom);
//	$this->mail->setFromName($this->notiFrom);
	$this->mail->setToEmail($this->notiTo);
//	$this->mail->setToName($this->notiTo);
	$this->mail->setSubject($subject);

	$this->mail->send();

    }
}
