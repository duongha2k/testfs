<?php

class Core_EmailForApprovedTech
{

    private $tech;
    private $wo;
    private $project;
    private $companyId;
    private $companyName;
    private $mail;

    function __construct()
    {
        $this->mail = new Core_Mail();
    }
    
    public function setTech($tech)
    {
        if (is_object($tech)) {
        	$this->tech = $tech;
        } else {
            $this->tech = new Core_Tech((int)$tech);
        }
    }
    
    public function setWorkOrder($workOrder)
    {
        if (is_object($workOrder)) {
        	$this->wo = $workOrder;
        } else {
            $this->wo = new Core_WorkOrder((int)$workOrder);
        }
    }
    
    public function setProject($project)
    {
        if (is_object($project)) {
        	$this->project = $project;
        } else {
            $this->project = new Core_Project((int)$project);
            $this->project->setFields('Project_Name,From_Email,Resource_Coordinator,Project_Name');
            $this->project->setData();
        }
    }
    
    public function setCompanyId($companyId)
    {
        $this->companyId = $companyId;
    }
    
    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName;
    }
    
    public function sendMail()
    {
    	$SiteName = $this->wo->getSiteName();
    	$WIN_NUM = $this->wo->getWIN_NUM();
    	$WorkOrderNo = $this->wo->getWO_ID();
    	$DateApproved = $this->wo->getDateApproved();
    
	    $projectID = $this->project->getProject_ID();
    	$projectName = $this->project->getProject_Name();
    	$vFromName = $this->project->getResource_Coordinator();
    	$projectCCphone = $this->project->getResource_Coord_Phone();
		$PcntDeduct = $this->wo->getPcntDeduct();
        $PcntDeductPercent = $this->wo->getPcntDeductPercent();
			
        //$deductMsg = $PcntDeduct == "True" ? "A 10% service fee will be deducted from your final total payment amount from this work order." : "NO service fee is being deducted from this work order.";
        if(empty($PcntDeductPercent) || $PcntDeductPercent == 0)
        {
            $PcntDeductPercentMsg = "No service fee will be deducted from your final total payment amount from this work order.";
        }
        else if($PcntDeductPercent == 1)
        {
            $PcntDeductPercentMsg = "You will be paid outside of the FieldSolutions Platform for this work order.";
        }
        else
        {
            $PcntDeductPercentMsg = "A ".($PcntDeductPercent * 100)."% service fee will be deducted from your final total payment amount from this work order.";
        }
	
        //13736
        $wosClass = new Core_Api_WosClass;
        $result = $wosClass->getSystemGeneratedEmailAddress_ByWinNum($WIN_NUM);
        $vFromEmail = $result["SystemGeneratedEmailFrom"];

        // use if project send to email is blank
        if ($vFromEmail == ""){
        	$vFromEmail = "support@fieldsolutions.com";}
        //end 13736
	
    	$techID = $this->tech->getTechID();
//    	$FirstName = $this->tech->getFirstName();
//    	$LastName = $this->tech->getLastName();
        
        $api = new Core_Api_TechClass;
        $techProperties = $api->getProperties($techID);

        $FirstName = $techProperties['FirstName']; 
        $LastName  = $techProperties['LastName'];
    	$sendToEmail = $this->tech->getPrimaryEmail();
    	$isoID = $this->tech->getISO_Affiliation_ID();
    	$W9 = $this->tech->getW9();
    
    	$eList = $sendToEmail;
    	$caller = "wosEmailTechWhenApproved";
    	$vSubject = "WIN# {$WIN_NUM}: Work Order# $WorkOrderNo has been approved.";	

		$deductMsg = $PcntDeduct == "True" || $PcntDeduct == 1 ? "A 10% service fee has been deducted from your final total payment amount  from this work order." : "No service fee has been deducted from this work order.";
    	$message = "Dear $FirstName $LastName,\n\n
            Work Order No: $WorkOrderNo has been approved.\n\n 
            $PcntDeductPercentMsg\n\n
            Your payment via direct deposit (or check if you have not signed up for direct deposit) is expected within 14 business days.\n\n
            Coordinator: $vFromName\n
            Email: $vFromEmail\n
            Phone: $projectCCphone\n\n
            https://www.fieldsolutions.com/techs/Training/FS_PaymentPolicy.php \n\n
            Thank you.\n\n
            Regards,\n
            Field Solutions\n
            Web: http://www.fieldsolutions.com";

    	$this->mail->setBodyText($message);
        $this->mail->setFromEmail($vFromEmail);
        $this->mail->setFromName($vFromName);
        $this->mail->setToEmail($eList);
        $this->mail->setToName($caller);
        $this->mail->setSubject($vSubject);
        
		$this->mail->setTech($this->tech);
		$this->mail->isTechEmail(true);
        $this->mail->send();    
        
        		
    	/* ------------------------------------------------------ */
    	/* W9 Reminder                                            */
    	/* ------------------------------------------------------ */
	    if ($W9 == "False" && $isoID == 0) {
		    $message = "You are in jeopardy of having payment for your recently completed work order(s) being delayed.  We cannot pay you until we have a W-9 on record for you.

            Instructions for submitting your W-9 can be found by clicking within the HELP tab after logging in to the site, or by clicking https://www.fieldsolutions.com/techs/w9.php.  
            
            If you are submitting your W-9 in a company name, please include your name and/or your technician ID somewhere on the W-9.
            
            Fax your completed W-9 to 888-258-1656, or email it to accounting@fieldsolutions.com.  W-9's are recorded as received in your profile within 1 business day.  An email will be sent to you acknowledging receipt.
            
            Thank you for your prompt attention to this matter.
            
            Sincerely,
            
            Accounting
            Field Solutions";
			
    		$this->mail->setBodyText($message);
            $this->mail->setFromEmail('accounting@fieldsolutions.com');
            $this->mail->setFromName('Field Solutions Accounting');
            $this->mail->setToEmail($eList);
            $this->mail->setToName($caller);
            $this->mail->setSubject('Payment could be delayed if your W-9 is not received');
            
			$this->mail->setTech($this->tech);
			$this->mail->isTechEmail(true);
            $this->mail->send();
	    }
    }
}