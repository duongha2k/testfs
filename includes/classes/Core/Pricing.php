<?php
class Core_Pricing {
	const PROJECT_LEVEL_PRICING_RULE = "(SELECT IFNULL(p.PricingRuleID,0) FROM projects AS p WHERE p.Project_ID = work_orders.Project_ID)";
	const CLIENT_LEVEL_PRICING_RULE = "(SELECT IFNULL(c.PricingRuleID,0) FROM clients AS c WHERE c.Company_ID = work_orders.Company_ID ORDER BY c.Admin DESC, c.ClientID LIMIT 1)";
	const WHICH_PRICING_RULE = "(CASE WHEN IFNULL(PricingRuleID, 0) <> 0 THEN PricingRuleID WHEN IFNULL(Project_ID,'') <> '' AND (SELECT IFNULL(p.PricingRuleID,0) FROM projects AS p WHERE p.Project_ID = work_orders.Project_ID) <> 0 THEN (SELECT IFNULL(p.PricingRuleID,0) FROM projects AS p WHERE p.Project_ID = work_orders.Project_ID) ELSE (SELECT IFNULL(c.PricingRuleID,0) FROM clients AS c WHERE c.Company_ID = work_orders.Company_ID ORDER BY c.Admin DESC, c.ClientID LIMIT 1) END)";

	private static $fieldList = NULL;
	private static $fieldDesign = NULL;
	
	public static function getRules($order = null) {
		$db = Core_Database::getInstance();
		$select = $db->select();
		$select->from(Core_Database::TABLE_PRICING_RULES, array('PricingRuleID', 'Description', 'ParentID', 'CloneDate'));
		if (!empty($order)) $select->order($order);
		$rules = $db->fetchAll($select);
		$result = array();
		foreach ($rules as $r) {
			$id = $r['PricingRuleID'];
//			unset($r['PricingRuleID']);
			$result[$id] = $r;
		}
		return $result;
	}
		
	public static function saveRule($id, $rule) {
		$db = Core_Database::getInstance();
		$allowed = array('Description');
		foreach ($rule as $k=>$v) {
			if (!in_array($k, $allowed)) unset($rule[$k]);
		}
		if (empty($id))
			$db->insert(Core_Database::TABLE_PRICING_RULES, $rule);
		else
			$db->update(Core_Database::TABLE_PRICING_RULES, $rule, $db->quoteInto('PricingRuleID = ?', $id));
	}
	
	public static function createField($displayName, $actualName) {
		$db = Core_Database::getInstance();
		$db->insert(Core_Database::TABLE_PRICING_FIELDS, array(
			'DisplayName' => $displayName,
			'ActualName' => $actualName
		));
		self::$fieldList = NULL;
	}
	
	public static function saveField($id, $displayName) {
		$db = Core_Database::getInstance();
		$db->update(Core_Database::TABLE_PRICING_FIELDS, array(
			'DisplayName' => $displayName
		), $db->quoteInto('PricingFieldID = ?', $id));
		self::$fieldList = NULL;
	}

	public static function getFields() {
		if (self::$fieldList !== NULL) return self::$fieldList;
		$db = Core_Database::getInstance();
		self::$fieldDesign = array_combine(Core_Database::getFieldList(Core_Database::TABLE_WORK_ORDERS), Core_Database::getFieldListType(Core_Database::TABLE_WORK_ORDERS));

		$select = $db->select();
		$select->from(Core_Database::TABLE_PRICING_FIELDS, array("PricingFieldID", "DisplayName", "ActualName"))
		->order("DisplayName ASC");
		self::$fieldList = $db->fetchAll($select);
		if (!self::$fieldList) self::$fieldList = array();

		$final = array();
		foreach (self::$fieldList as $field) {
			if (empty($field) || !is_array($field)) continue;
			$displayName = $field['DisplayName'];
			$actualName = $field['ActualName'];
			$type = self::$fieldDesign[$actualName];
			$final[$field['PricingFieldID']] = array($displayName, $actualName, $type);
		}
		self::$fieldList = $final;
		return self::$fieldList;
	}
	
	public static function getFieldDesign() {
		self::getFields();
		return self::$fieldDesign;
	}
		
	public static function getField($id) {
		// returns a pricing field's actual and display names based on the pricing field ID
		$fieldList = self::getFields();
		return (array_key_exists($id, $fieldList) ? $fieldList[$id] : FALSE);
	}
	
	public static function getFieldDisplayName($id) {
		// returns a pricing field's display names based on the pricing field ID
		$fieldList = self::getFields();
		return (array_key_exists($id, $fieldList) ? $fieldList[$id][0] : FALSE);
	}
	
	public static function getFieldActualName($id) {
		// returns a pricing field's actual names based on the pricing field ID
		$fieldList = self::getFields();
		return (array_key_exists($id, $fieldList) ? $fieldList[$id][1] : FALSE);
	}

	public static function getFieldType($id) {
		// returns a pricing field's actual names based on the pricing field ID
		$fieldList = self::getFields();
		return (array_key_exists($id, $fieldList) ? $fieldList[$id][2] : FALSE);
	}
	
	private static function moneyFormat($val) {
		return number_format($val, 2, ".", "");
	}
	
	public static function saveChargeType($id, $description, $percentage, $flatFee, $perX, $min, $max, $inputField, $outputField, $calculationOrder, $applyWhen, $pricingRuleID = "") {
		// gets all charge types for a pricing rule
		if ($percentage === "") $percentage = NULL;
		if ($flatFee === "") $flatFee = NULL;
		if ($perX === "") $perX = NULL;
		if ($min === "") $min = NULL;
		if ($max === "") $max = NULL;
		$db = Core_Database::getInstance();
		if ($id == 0) {
			$result = $db->insert(Core_Database::TABLE_CHARGE_TYPES, array(
				'Description' => $description,
				'Percentage' => $percentage,
				'FlatFee' => $flatFee,
				'PerX' => $perX,
				'Min' => $min,
				'Max' => $max,
				'InputField' => $inputField,
				'OutputField' => $outputField,
				'CalculationOrder' => $calculationOrder,
				'ApplyWhen' => $applyWhen,
				'PricingRuleID' => $pricingRuleID
			));
		}
		else {
			$result = $db->update(Core_Database::TABLE_CHARGE_TYPES, array(
				'Description' => $description,
				'Percentage' => $percentage,
				'FlatFee' => $flatFee,
				'PerX' => $perX,
				'Min' => $min,
				'Max' => $max,
				'InputField' => $inputField,
				'OutputField' => $outputField,
				'CalculationOrder' => $calculationOrder,
				'ApplyWhen' => $applyWhen
			), $db->quoteInto('ChargeTypeID = ?', $id));
		}
		return $result;
	}
	
	public static function deleteChargeType($id) {
		$db = Core_Database::getInstance();
		$result = $db->delete(Core_Database::TABLE_CHARGE_TYPES, $db->quoteInto('ChargeTypeID = ?', $id));
	}

	public static function getChargeTypes($id) {
		// gets all charge types for a pricing rule
		$db = Core_Database::getInstance();
		$select = $db->select();
		$select->from(Core_Database::TABLE_CHARGE_TYPES, array('ChargeTypeID', 'Description', 'Percentage', 'FlatFee', 'PerX', 'InputField', 'OutputField', 'CalculationOrder', 'Min', 'Max', 'ApplyWhen'))
		->where('PricingRuleID = ?', $id)
		->order('CalculationOrder ASC');
		
		$result = $db->fetchAll($select);
		
		return $result;
	}
		
	public static function cloneRule($id) {
		$db = Core_Database::getInstance();
		$select = $db->select();
		$select->from(Core_Database::TABLE_PRICING_RULES, array("Description"))
		->where('PricingRuleID = ?', $id);
		$pricingRule = $db->fetchAll($select);
		$pricingRule = $pricingRule ? $pricingRule[0] : false;
		if (!$pricingRule || $pricingRule['Description'] == "") return FALSE;
		$description = $pricingRule['Description'];
		$description .= " - " . strtotime("now") . rand(0, 5000);
		$db->insert(Core_Database::TABLE_PRICING_RULES, array(
			'Description' => $description,
			'ParentID' => $id,
			'CloneDate' => new Zend_Db_Expr('NOW()')
		));
		$lastInsert = $db->lastInsertId();
		if (empty($lastInsert)) return FALSE;
		$clonedID = $lastInsert;
		$chargeTypes = self::getChargeTypes($id);
		foreach ($chargeTypes as $charge) {
			if (!$charge) continue;
			$description = $charge['Description'];
			$percentage = $charge['Percentage'];
			$flatFee = $charge['FlatFee'];
			$perX = $charge['PerX'];
			$inputField = $charge['InputField'];
			$outputField = $charge['OutputField'];
			$calculationOrder = $charge['CalculationOrder'];
			$min = $charge['Min'];
			$max = $charge['Max'];
			$applyWhen = $charge['ApplyWhen'];
			$db->insert(Core_Database::TABLE_CHARGE_TYPES, array(
				'PricingRuleID' => $clonedID,
				'Description' => $description,
				'Percentage' => $percentage,
				'FlatFee' => $flatFee,
				'PerX' => $perX,
				'InputField' => $inputField,
				'outputField' => $outputField,
				'CalculationOrder' => $calculationOrder,
				'Min' => $min,
				'Max' => $max,
				'ApplyWhen' => $applyWhen
			));
		}
		return TRUE;
	}

	public static function getNextCalculationOrder($id) {
		// gets the next available calculation order value
		$db = Core_Database::getInstance();		
		$select = $db->select();
		$select->from(Core_Database::TABLE_CHARGE_TYPES, array('next' => new Zend_Db_Expr('MAX(IFNULL(CalculationOrder,0)) + 1')))
		->where('PricingRuleID = ?', $id);
		$result = $db->fetchOne($select);
		return (!empty($result) ? $result : 0);
	}

	public static function findApplyWhenFields($applyWhen) {
		preg_match_all("(@pricingField:([\d]+))", $applyWhen, $fieldStrs, PREG_SET_ORDER | PREG_OFFSET_CAPTURE);
		$stringToReplace = array();
		foreach ($fieldStrs as $field) {
			$fieldStr = $field[0][0];
			$fieldID = $field[1][0];
			$stringToReplace[$fieldStr] = $fieldID;
		}
		return $stringToReplace;
	}
	
	public static function chargeTypeCalculate($chargeType, $inputValues) {
		// process a charge type and return the calculated result

		$result = 0.00;
		$inputField = $chargeType["InputField"];
		if (!array_key_exists($inputField, $inputValues))
			return FALSE;
		$inputValue = $inputValues[$inputField];

		if (!empty($chargeType["ApplyWhen"])) {
			$applyWhen = $chargeType["ApplyWhen"];
			$stringToReplace = self::findApplyWhenFields($applyWhen);
			foreach ($stringToReplace as $str=>$id) {
				$applyWhen = str_replace($str, "\"$inputValues[$id]\"", $applyWhen);
			}
			// Evaluates Apply When Conditional if it's not blank
			$applyWhenEval = FALSE;
			eval("\$applyWhenEval = $applyWhen;");
			if (!$applyWhenEval)
				// skips calulation when Apply When is FALSE
				return 0.00;
		}

		if ($chargeType["Percentage"] !== NULL) {
			$result += $inputValue * $chargeType["Percentage"] * 0.01;
		}
		if ($chargeType["FlatFee"] !== NULL) {
			$result += $chargeType["FlatFee"];
		}
		if ($chargeType["PerX"] !== NULL) {
			$result += $inputValue * $chargeType["PerX"];
		}
		if ($chargeType["Max"] !== NULL) {
			$result = min($result, $chargeType["Max"]);
		}
		if ($chargeType["Min"] !== NULL) {
			$result = max($result, $chargeType["Min"]);
		}

		return $result;
	}
		
	public static function createCalculationMatrix($chargeTypeList) {
		self::getFields(); // init
		// creates a pricing calculation matrix based on the list of charge types for a pricing rule
		$calculation = array();
		$inputFieldList = array();
		$outputFieldList = array();
		foreach ($chargeTypeList as $info) {
			if (empty($info) ) continue;
			$inputField = $info['InputField'];
			if (!array_key_exists($inputField, $inputFieldList))
				$inputFieldList[$inputField] = NULL;
			$outputField = $info['OutputField'];
			if (!array_key_exists($outputField, $outputFieldList))
				$outputFieldList[$outputField] = NULL;
			$calcOrder = $info['CalculationOrder'];
			$applyWhen = $info['ApplyWhen'];
			if (!empty($applyWhen)) {
				$applyWhenFieldsRequired = self::findApplyWhenFields($applyWhen);
				foreach ($applyWhenFieldsRequired as $str=>$id) {
					if (!array_key_exists($id, $inputFieldList))
						$inputFieldList[$id] = NULL;
				}
			}

			$chargeType = array("Description" => $info['Description'], "Percentage" => $info['Percentage'], "FlatFee" => $info['FlatFee'], "PerX" => $info['PerX'], "InputField" => $inputField, "OutputField" => $outputField, "CalculationOrder" => $info['CalculationOrder'], "Min" => $info['Min'], "Max" => $info['Max'], "ApplyWhen" => $applyWhen);

			if (!array_key_exists($calcOrder, $calculation))
				$calculation[$calcOrder] = array();
			if (!array_key_exists($outputField, $calculation[$calcOrder]))
				$calculation[$calcOrder][$outputField] = array();
			$calculation[$calcOrder][$outputField][] = $chargeType;
		}
		// adds list of required inputs and expected output field
		$calculation["inputFieldList"] = $inputFieldList;

		$calculation["outputFieldList"] = $outputFieldList;

		return $calculation;
	}
	
	public static function performCalculation($calculationMatrix, $inputValues, $pricingRuleID = "") {
		// performs the calculations and returns the output values and the text based log of the calculations performed
		$log = (!empty($pricingRuleID) ? "Using Pricing Rule: $pricingRuleID\n" : "");
		$log .= "Date/Time: " . date("m/d/Y H:i:s") . "\n\n";
		$inputFieldList = $calculationMatrix["inputFieldList"];
		$outputFieldList = $calculationMatrix["outputFieldList"];
/*		foreach ($inputFieldList as $key=>$value) {
			// check that all required fields are passed
			if (!array_key_exists($key, $inputValues))
				return FALSE;
		}*/
		foreach ($calculationMatrix as $order=>$calc) {
			if ($order === "inputFieldList" || $order === "outputFieldList")
				continue;
			$log .= "Phase $order - \nInput Values:\n";
			foreach ($inputValues as $key=>$value) {
				if (is_numeric($value))
					$value = self::moneyFormat($value);
				$log .= self::getFieldDisplayName($key) . " = $value\n";
			}
			$log .= "\nCalculated Values:\n";
			foreach ($calc as $outputField=>$chargeTypeList) {
				$outputFieldDisplayName = self::getFieldDisplayName($outputField);
				if (self::getFieldActualName($outputField) == "PayAmount") {
					// Skip Total Tech Pay Calculation since it is already done in WO
					$total = $inputValues[$outputField];
					$log .= "$outputFieldDisplayName = <span class=\"totalCalculated\">" . self::moneyFormat($total) . "</span> (skipped)\n";
					continue;
				}
				$chargeTypeListDesc = "";
				$total = 0.00;
				foreach ($chargeTypeList as $chargeType) {
					$chargeTypeCalculatedValue = self::chargeTypeCalculate($chargeType, $inputValues);
					if ($chargeTypeCalculatedValue === FALSE) {
						$log .= "ERROR: Unable to calculate charge type '{$chargeType["Description"]}'. '" . getFieldDisplayName($chargeType["InputField"]) . "' was not provided.";
						return array(FALSE, $log);
					}
					$chargeTypeListDesc .= ($chargeTypeListDesc != "" ? " + " : "") . self::moneyFormat($chargeTypeCalculatedValue) . " **{$chargeType["Description"]}** ";
					$total += $chargeTypeCalculatedValue;
				}
				$outputFieldList[$outputField] = self::moneyFormat($total);
				$log .= "$outputFieldDisplayName = $chargeTypeListDesc = <span class=\"totalCalculated\">" . self::moneyFormat($total) . "</span>\n";
			}
			$log .= "---\n\n";
			foreach ($outputFieldList as $key=>$value) {
				// updates input fields for next calculation phase
				if ($value != NULL)
					$inputValues[$key] = $outputFieldList[$key];
			}
		}
		return array($outputFieldList, $log, $pricingRuleID);
	}
	
	public static function saveCalculatedValuesToWO($unid, $calcResults) {
		$db = Zend_Registry::get('DB');
		$outputFieldList = $calcResults[0];
		$updateFields = array();
		foreach ($outputFieldList as $key=>$value) {
			$updateFields[self::getFieldActualName($key)] = $value;
		}
		$calcText = nl2br($calcResults[1]);
		$ruleID = $calcResults[2];
		$updateFields['PricingRuleApplied'] = $ruleID;
		$updateFields['PricingCalculationText'] = $calcText;
		$updateFields['PricingRan'] = 1;
		
		$db->update('work_orders', $updateFields, $db->quoteInto('WIN_NUM=?', $unid));

	}

	public function runCalculation($unid) {
		if (!is_numeric($unid)) return;
		// only pass ruleID
		$db = Zend_Registry::get('DB');
		$sql = "SELECT " . self::WHICH_PRICING_RULE . ", Approved FROM work_orders WHERE WIN_NUM=?";
		$result = $db->fetchRow($sql, array($unid), Zend_Db::FETCH_NUM);
		if (!$result) {
			// no pricing rule attached
//			pricingErrorMsg("No pricing rule attached to WO UNID: $unid");
			return;
		}
/*				$message = "Session: " .  print_r($_SESSION, true) . "<br/><br/>" .
				"Queue: " . print_r($queue, true) . "<br/><br/>" .
				"Referer: {$_SERVER['HTTP_REFERER']}<br/>" .
				"page: {$_SERVER['REQUEST_URI']}<br/>";
				smtpMail("Trung Ngo", "tngo@fieldsolutions.com", "codem01@gmail.com", "Pricing Ran: $unid", $message, $message, "Test");*/

		$id = $result[0];
		$approved = $result[1];
//				echo "Checking...<br/>";
/*		if ($approved == "False" && !$ignoreApproved) {
//		    print 'asd';print 18; exit;
//			pricingErrorMsg("Tried to run pricing on unapproved WO: $unid");
			return;
		}*/
/*				echo "Running Calc...<br/>";
				flush();
				ob_flush();*/
/*				echo "Get Charge type ... <br/>";
				flush();
				ob_flush();*/

		$chargeTypes = self::getChargeTypes($id);
		if (!$chargeTypes) {
//			pricingErrorMsg("Unable to find Charge Types for Rule: $ruleID WO: $unid");
			return;
		}
		
/*				echo "Create Matrix ... <br/>";
				flush();
				ob_flush();*/
		self::runCalculationUsingChargeType($unid, $id, $chargeTypes);
	}

	public static function runCalculationUsingChargeType($unid, $ruleID, $chargeTypes) {
		$calcMatrix = self::createCalculationMatrix($chargeTypes);

		if (!$calcMatrix) {
//			pricingErrorMsg("Problem creating matrix using rule: $ruleID WO: $unid");
			return false;
		}
/*				echo "Get input values ... <br/>";
				flush();
				ob_flush();*/

		$inputValues = self::getInputValuesFromWO($unid, $calcMatrix["inputFieldList"]);
		if (!$inputValues) {
//			pricingErrorMsg("Unable to get input values WO: $unid");
			return false;
		}
/*				echo "Performing Calc ... <br/>";
				flush();
				ob_flush();*/
		$calcResults = self::performCalculation($calcMatrix, $inputValues, $ruleID);
//				print_r($calcResults);
/*				echo "Saving Results ... <br/>";
				flush();
				ob_flush();*/
		self::saveCalculatedValuesToWO($unid, $calcResults);
		return true;
	}

	public static function getInputValuesFromWO($unid, $inputFieldList) {
		$fieldDesign = self::$fieldDesign;
		if (sizeof($inputFieldList) == 0) return array();
		$inputFieldListActualName = $inputFieldList;
		$inputValues = array();
		foreach ($inputFieldListActualName as $key=>$value) {
			// get input fields from record
			$inputFieldListActualName[$key] = self::getFieldActualName($key);
			$inputValues[$key] = 0;
		}
		$db = Zend_Registry::get('DB');
		$sql = "SELECT " . implode(",", $inputFieldListActualName) . " FROM work_orders WHERE WIN_NUM=?";
		$result = $db->fetchRow($sql, array($unid), Zend_Db::FETCH_NUM);
		if (!$result) return FALSE;
		$index = 0;
		foreach ($inputValues as $key=>$value) {
			if ($fieldDesign[self::getFieldActualName($key)] == "tinyint") { //'Yes/No'
				$result[$index] = ($result[$index] == 1 ? "TRUE" : "FALSE");
			}
			if ($result[$index] === NULL)
				switch ($fieldDesign[self::getFieldActualName($key)]) {
/*					case "ShortText":
					case "LongText":
					case "Date/Time":
					case "File":
						$result[$index] = "";
						break;
					case "AutoNumber":
					case "Number":
						$result[$index] = 0;
						break;*/
					case "tinyint":
						$result[$index] = "FALSE";
						break;
					default:
						$result[$index] = "";
				}
			$inputValues[$key] = $result[$index];
			$index++;
		}
		return $inputValues;
	}
}
?>