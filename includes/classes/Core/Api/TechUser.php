<?php

//require_once 'Error.php';
//require_once realpath(dirname(__FILE__) . '/../' ) . '/Caspio.php';

class Core_Api_TechUser
{
	protected $login = '';
	protected $password = '';
//	protected $companyId = '';
//	protected $companyName = '';
	protected $techID = '';
	protected $authenticate = false;
    protected $latitude;
    protected $longitude;
    protected $zipcode;
    protected $flsID;
    protected $flsCSPRec;
    protected $isFlsTech;
	protected $FLSstatus;
    
    protected $firstName;
    protected $lastName;
    protected $smsNumber;
    protected $deactivated;
    protected $acceptTerms;
    protected $dateAccepted;
    protected $accountActivated;
    protected $primaryEmail;

    protected $paymentMethod;
	protected $RegDate;
	
	protected $activationCode;

    /**
     *
     * @param array $authData login/password or just login
     * @return Core_Api_TechUser $this
     * @author Pavel Shutin
     */
    public function load($authData) {
        
        $errors = Core_Api_Error::getInstance();

        $db = Core_Database::getInstance();
        $userSelect = $db->select();
        

        if (!is_array($authData) || empty($authData['login'])) {
			$errors->addError(2, 'Login is empty but required');
            return false;
        }

        $this->login = Core_Caspio::caspioEscape($authData['login']);
        
        if (isset($authData['password'])) {
          $this->password = Core_Caspio::caspioEscape($authData['password']);
        }
        
		$criteria = 'UserName = \''.$this->login.'\'';
		
		
        if (!empty($this->password)) {
            $criteria .= ' AND MD5(Password) = MD5(\''.$this->password.'\')';
        }

        if (defined('SHORT_AUTH')) {
            if (!empty($_SESSION['UserObj']) && !empty($_SESSION['UserObj'][0]['TechID'])) {
                $user = $_SESSION['UserObj'];
            }else{
                 $selObj =  $userSelect->from("TechBankInfo", array('TechID','Password','Latitude','Longitude','ZipCode','FLSID','FLSTech','FLSCSP_Rec', 'FirstName','LastName','SMS_Number','Deactivated','AcceptTerms', 'Date_TermsAccepted', 'FLSstatus', 'Activation_Confirmed', 'Activation_Code', 'PrimaryEmail','PaymentMethod', 'RegDate'))
             						->distinct()
             						->where($criteria);
             	$stmt = $selObj->query();
             	$user = $stmt->fetchAll();

                $_SESSION['UserObj'] = $user;
            }
        }else{
            $selObj =  $userSelect->from("TechBankInfo", array('TechID','Password','Latitude','Longitude','ZipCode','FLSID','FLSTech','FLSCSP_Rec','FirstName','LastName','SMS_Number','Deactivated','AcceptTerms', 'Date_TermsAccepted', 'FLSstatus', 'Activation_Confirmed', 'Activation_Code', 'PrimaryEmail','PaymentMethod', 'RegDate'))
             						->distinct()
             						->where($criteria);
            $stmt = $selObj->query();
            $user = $stmt->fetchAll();
        }

		if (!empty($user)) {
			$this->techID       = Core_Caspio::caspioEscape($user[0]['TechID']);
            $this->password       = Core_Caspio::caspioEscape($user[0]['Password']);
            $this->longitude    = Core_Caspio::caspioEscape($user[0]['Longitude']);
            $this->latitude     = Core_Caspio::caspioEscape($user[0]['Latitude']);
            $this->zipcode      = Core_Caspio::caspioEscape($user[0]['ZipCode']);
            $this->flsID        = Core_Caspio::caspioEscape($user[0]['FLSID']);
            $this->isFlsTech    = Core_Caspio::caspioEscape($user[0]['FLSTech']);
            $this->FLSstatus    = Core_Caspio::caspioEscape($user[0]['FLSstatus']);
            $this->flsCSPRec		= Core_Caspio::caspioEscape($user[0]['FLSCP_Rec']);

            $this->firstName    = Core_Caspio::caspioEscape($user[0]['FirstName']);
            $this->lastName    = Core_Caspio::caspioEscape($user[0]['LastName']);
            $this->smsNumber    = Core_Caspio::caspioEscape($user[0]['SMS_Number']);
            $this->deactivated    = Core_Caspio::caspioEscape($user[0]['Deactivated']);
            $this->acceptTerms    = Core_Caspio::caspioEscape($user[0]['AcceptTerms']);
            $this->dateAccepted = Core_Caspio::caspioEscape($user[0]['Date_TermsAccepted']);
            $this->primaryEmail = Core_Caspio::caspioEscape($user[0]['PrimaryEmail']);

			$this->paymentMethod = Core_Caspio::caspioEscape($user[0]['PaymentMethod']);
			$this->RegDate = Core_Caspio::caspioEscape($user[0]['RegDate']);
			
			$this->activationCode = Core_Caspio::caspioEscape($user[0]['Activation_Code']);
            $user[0]['Activation_Confirmed'] == "1" ? $this->accountActivated = true : $this->accountActivated = false;

		} else {

			$er = $errors->getErrors();
			if (empty($er)) {
				$errors->addError(3, 'Login or password is not correct');
			}
		
            $this->techID = '';
            $this->longitude    = NULL;
            $this->latitude     = NULL;
            $this->zipcode      = NULL;
            $this->flsID        = NULL;
            $this->isFlsTech    = NULL;
            $this->FLSstatus    = NULL;

			$this->paymentMethod = '';

            return false;
		}

        return $this;
    }
	
	/**
	 * Check up user information
	 *
	 * @param array $authData
	 * @return bool
	 */
	public function checkAuthentication($authData)
	{
		
		$errors = Core_Api_Error::getInstance();
		
		$this->login = Core_Caspio::caspioEscape($authData['login']);
		$this->password = Core_Caspio::caspioEscape($authData['password']);
		
		

		if (empty($this->login) || empty($this->password)) {
			$errors->addError(2, 'Login or password is empty but required');
			$this->authenticate = false;
			return false;
		}
        
		if (!$this->checkProtocol()) {
			throw new SoapFault("jeff", "failing in check protocol");
			$this->authenticate = false;
			return false;
		}

        $this->load($authData);
        if ($this->techID) {
            $this->authenticate = true;
        }else{
            $this->authenticate = false;
        }
        
		return $this->authenticate;
	}
	
	/**
	 * Check up user authentication
	 *
	 * @return bool
	 */
	public function isAuthenticate()
	{
		return $this->authenticate;
	}
	
	public function getEmail()
	{
		return $this->primaryEmail;
	}
	
	public function isActivated()
	{
		return $this->accountActivated;
	}
	
	public function loginHistory($userName)
	{
		
		$class = new Core_Api_Class;
		$class->loginHistory($userName, "Tech");
	}
	
	
	/**
	 * 
	 *
	 * @return bool
	 */
	protected function checkProtocol()
	{
		return true;
		
		if (HTTPS_PROTOCOL_REQUIRED) {
			$cur_protocol = $_SERVER['SERVER_PROTOCOL'];
			$cur_protocol = explode('/',$cur_protocol);
			if ($cur_protocol[0] == 'HTTPS') {
				$errors = Core_Api_Error::getInstance();
				$errors->addError(4, 'Not HTTPS protocol used');
				return false;
			} else {
				return true;
			}
		}
		return false;
	}
	
	public function getTechId()
	{
		return $this->techID;
	}
    /**
     * getLogin
     * 
     * @access public
     * @return string
     */
	public function getLogin()
	{
	    return $this->login;
    }
    /**
     * getPassword
     * 
     * @access public
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }
    /**
     * getLatitude
     * 
     * @access public
     * @return double
     */
    public function getLatitude()
    {
        return $this->latitude;
    }
    /**
     * getLongitude  
     * 
     * @access public
     * @return double
     */
    public function getLongitude()
    {
        return $this->longitude;
    }
    /**
     * getZipcode
     * 
     * @access public
     * @return string
     */
    public function getZipcode()
    {
        return $this->zipcode;
    }
    /**
     * getFlsId
     * 
     * @access public
     * @return int
     */
    public function getFlsId()
    {
        return $this->flsID;
    }
    /**
     * isFlsTech
     * 
     * @access public
     * @return bool
     */
    public function getFlsCpRec()
    {
        return $this->flsCSPRec;
    }
    
    public function getFLSstatus()
    {
        return $this->FLSstatus;
    }
    public function isFlsTech()
    {
        return $this->isFlsTech;
    }
    
    public function getFirstName()
    {
        return $this->firstName;
    }
    
    public function getLastName()
    {
        return $this->lastName;
    }
    
    public function getSmsNumber()
    {
        return $this->smsNumber;
    }
    
    public function getDeactivated()
    {
        return $this->deactivated;
    }
    
    public function getAcceptTerms()
    {
        return $this->acceptTerms;
    }
    
	public function getDateAccepted()
    {
        return $this->dateAccepted;
    }
    
	public function getRegDate()
    {
        return $this->RegDate;
    }
    
    public function getActivationCode(){
    	return $this->activationCode;
    }

	static function getLoginByAuth($authLogin)
	{
	    list($login) = explode("|pmContext=", $authLogin);
	    return $login;
	}

	public function getPaymentMethod()
    {
        return $this->paymentMethod;
}

	public static function getTechLoginByWMID($WMID) {
        $db = Core_Database::getInstance();
        $userSelect = $db->select();
		$selObj =  $userSelect->from("TechBankInfo", array('Username','Password', 'TechID'))
             						->distinct()
             						->where("WMID = ?", $WMID);
		$stmt = $selObj->query();
		$user = $stmt->fetchAll();
		if (!$user) return false;
		return array('login' => $user[0]['Username'], 'password' => $user[0]['Password'], 'TechID' => $user[0]['TechID']);
	}

}
