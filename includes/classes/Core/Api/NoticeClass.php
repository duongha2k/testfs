<?php
class Core_Api_NoticeClass
{
    const SEE_NOTICES_OPTIONS_FOR_WOS_OWNER = 0;
    const SEE_NOTICES_OPTIONS_FOR_SELECTED_PROJECTS = 1;
    
    const WOStatusID_FOR_PUBLISHED = 1;
    const WOStatusID_FOR_ASSIGNED = 2;
    const WOStatusID_FOR_WORKDONE = 3;
    const WOStatusID_FOR_APPROVED = 4;
    const WOStatusID_FOR_UNASSIGNED_QA = 5;
    const WOStatusID_FOR_ASSIGNED_QA = 6;
    const WOStatusID_FOR_TECH_BID = 7;
    /*(1: Published, 2:Assigned, 3:WorkDone, 4: Approved)*/    
    /*
    * return 0: for wos client own only, 1: for selected projects    
    */
    public function getSeeNoticeFor($clientID)
    {
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from("notice_settings");
        $select->where("client_id = '$clientID'");
        $records = Core_Database::fetchAll($select);  
        
        if(empty($records) || count($records)==0) return 0;
        else return $records[0]['see_notices_for'];        
    }
    
    public function getLastViewDatetime($clientID)
    {
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from("notice_settings");
        $select->where("client_id = '$clientID'");
        $records = Core_Database::fetchAll($select);  
        
        if(empty($records) || count($records)==0) return null;
        else
        {
            $lastViewDate = $records[0]['last_view_date'];  
            if($lastViewDate < '1970-01-01') return null;      
            else return $lastViewDate;
        }
    }
    
    public function saveLastViewDatetime($clientID)
    {
        if($this->exists($clientID)==0)
        {
            //--- insert   
            $fieldvals = array("client_id"=>$clientID,
                        "last_view_date"=>date('Y-m-d H:i:s'));
            $result = Core_Database::insert("notice_settings",$fieldvals);
        }
        else
        {
            //--- update
            $fieldvals = array("last_view_date"=>date('Y-m-d H:i:s'));   
            $criteria = "client_id='$clientID'";  
            $result = Core_Database::update("notice_settings",$fieldvals,$criteria);
        }         
    }
    
    public function getProjectList($clientID)
    {
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from(array('nsp'=>"notice_settings_project"),array('*'));
        $select->join(array('p'=>'projects'),
            'p.Project_ID=nsp.project_id',
            array('Project_Name')
        );        
        $select->where("nsp.client_id = '$clientID'");
        $records = Core_Database::fetchAll($select); 
        return $records; 
    }
    
    public function getProjectIDs($clientID)
    {
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from("notice_settings_project");
        $select->where("client_id = '$clientID'");
        $records = Core_Database::fetchAll($select); 
        if(empty($records) || count($records)==0) return null;
        
        $projectIDArray = array();
        foreach($records as $rec)
        {
            $projectIDArray[]=$rec['project_id'];
        }
        return $projectIDArray; 
    }
    
    public function saveSeeNoticeFor($clientID,$seeNoticesFor)
    {
        if($this->exists($clientID)==0)
        {
            //--- insert   
            $fieldvals = array("client_id"=>$clientID,
                        "see_notices_for"=>$seeNoticesFor);
            $result = Core_Database::insert("notice_settings",$fieldvals);
        }
        else
        {
            //--- update
            $fieldvals = array("see_notices_for"=>$seeNoticesFor);   
            $criteria = "client_id='$clientID'";  
            $result = Core_Database::update("notice_settings",$fieldvals,$criteria);
        }        
        
        if($seeNoticesFor==0)
        {
            $this->deleteProjects($clientID);
        }
    }
    
    public function saveNoticeProjects($clientID,$projectIDArray)
    {
        $this->deleteProjects($clientID);
        foreach($projectIDArray as $projectID)
        {
            $this->addProject($clientID,$projectID);
        }        
    }
    
    public function addProject($clientID,$projectID)
    {
        $fieldvals = array("client_id"=>$clientID,
                        "project_id"=>$projectID);
        $result = Core_Database::insert("notice_settings_project",$fieldvals);
        return $result;
    }
    
    public function exists($clientID)
    {
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from("notice_settings");
        $select->where("client_id = '$clientID'");
        $records = Core_Database::fetchAll($select);
        if(empty($records) || count($records)==0) return 0;
        else return 1;
    }
    
    public function deleteProjects($clientID)
    {
        $criteria = "client_id='$clientID'";  
        $db = Core_Database::getInstance();
        $result = $db->delete('notice_settings_project',$criteria);
        return $result;
    }
    
    //public function getCountNotices_WOs ($clientID) {
    public function getCountNotices_NotViewYet ($clientID) {
    	$Core_Api_Class = new Core_Api_Class();
        $clientInfo = $Core_Api_Class->getClientByID($clientID);
        $clientUsername = $clientInfo['UserName'];

        $see_notices_for = $this->getSeeNoticeFor($clientID);
        
        $lastViewDate = $this->getLastViewDatetime($clientID);
        $logFields = array('WIN_NUM','DateTime_Stamp');
        $logFields = array('Num'=>'(Count(*))');
        $woFields = array();
 
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from(array('ts'=>'WOChangeStatusLog'),
                    $logFields
        );
        $select->join(array('w'=>Core_Database::TABLE_WORK_ORDERS),
                    'w.WIN_NUM=ts.WIN_NUM',
                    $woFields
        );          
        //$select->where("ts.WOStatusID = 1");
        
        if($see_notices_for==self::SEE_NOTICES_OPTIONS_FOR_WOS_OWNER)
        {
            $select->where("w.WorkOrderOwner = '$clientUsername'");
        }
        else if($see_notices_for==self::SEE_NOTICES_OPTIONS_FOR_SELECTED_PROJECTS)
        {            
            $projectIDArray = $this->getProjectIDs($clientID);
			if (!empty ($projectIDArray))
				$select->where("w.Project_ID IN (?)",$projectIDArray);            
        }
        
        if(!empty($lastViewDate))
        {
            $select->where("ts.DateTime_Stamp > '$lastViewDate'");
        }
        
        $records = Core_Database::fetchAll($select); 
        
        if(empty($records) || count($records)==0) return 0;

        return $records[0]['Num']; 
    }

    //public function getNotices_WOS_All ($clientID)
    public function getNotices ($clientID)
    {
        $Core_Api_Class = new Core_Api_Class();
        $clientInfo = $Core_Api_Class->getClientByID($clientID);
        $clientUsername = $clientInfo['UserName'];

        $see_notices_for = $this->getSeeNoticeFor($clientID);

        $logFields = array('WOStatusID', 'WIN_NUM','DateTime_Stamp', 'Notice_Tech_ID' => 'Tech_ID', 'Tech_Name');
        $woFields =  array('WorkOrderOwner','Project_ID','WO_ID','Tech_ID','Tech_FName','Tech_LName','Company_ID');        
 
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from(array('ts'=>'WOChangeStatusLog'),
                    $logFields
        );
        $select->join(array('w'=>Core_Database::TABLE_WORK_ORDERS),
                    'w.WIN_NUM=ts.WIN_NUM',
                    $woFields
        );          

		$select->where("ts.DateTime_Stamp > ?", date("Y-m-d", strtotime('-1 month')));
        
        if($see_notices_for==self::SEE_NOTICES_OPTIONS_FOR_WOS_OWNER)
        {
            $select->where("w.WorkOrderOwner = '$clientUsername'");
        }
        else if($see_notices_for==self::SEE_NOTICES_OPTIONS_FOR_SELECTED_PROJECTS)
        {            
            $projectIDArray = $this->getProjectIDs($clientID);    
            $select->where("w.Project_ID IN (?)",$projectIDArray);            
        }
        
       	$select->order("ts.DateTime_Stamp DESC");

        $records = Core_Database::fetchAll($select); 
        
		if(empty($records) || count($records)==0) return null;

		foreach($records as $rec) {
			$type = $rec["WOStatusID"];
			$Created_Date = $rec['DateTime_Stamp'];             
			$techID = empty ($rec['Notice_Tech_ID']) ? $rec['Tech_ID'] : $rec['Notice_Tech_ID'];
			$techName = empty ($rec['Tech_Name']) ? $rec['Tech_FName'].' '.$rec['Tech_LName'] : $rec['Tech_Name'];
			$winNum = $rec['WIN_NUM']; 
			$woID = $rec['WO_ID']; 
			$company_ID = $rec['Company_ID'];

			switch ($rec["WOStatusID"]) {
				case "1":
					$message = "Work Order Published";
					$message_type = "WO_PUBLISHED";
					break;
				case "2":
					$message = "Work Order Assigned";
					$message_type = "WO_ASSIGNED_TO";
					break;
				case "3":
					$message = "Work Done";
					$message_type = "WO_COMPLETED";
					break;
				case "4":
					$message = "Work Order Approved";
					$message_type = "WO_APPROVED";
					break;
				case "5":
					$message = "Technician {Q&A}";
					$message_type = "TECH_QUESTION_UNASSIGNED_WO";
					break;
				case "6":
					$message = "Technician {Q&A}";
					$message_type = "TECH_QUESTION_ASSIGNED_WO";
					break;
				case "7":
					$message = "Technician {BID}";
					$message_type = "TECH_BID";
					break;
				default:
					$message = var_export ($rec["WOStatusID"], true);
			}

			$resultArray[]=array(
				'message'=>$message,
				'message_type'=>$message_type,
				'timestamp'=>$Created_Date, 
				'data'=>array(
					'TechID'=>$techID,
					'TechName'=>$techName,
					'WinNum'=>$winNum,
					'Company_ID'=>$company_ID,
					'WoID'=>$woID
				)
			);
		}         

		return $resultArray;
    }

    public function createWOChangeStatusLog($winNum,$statusID,$datetimeStamp = null, $tech_id = null, $tech_name = null, $amount = null)
    {        
        if(empty($winNum)) return;
        
        if(empty($datetimeStamp)) $datetimeStamp=date("Y-m-d H:i:s");
        
        $fields = array('WIN_NUM'=>$winNum,
                    'WOStatusID'=>$statusID,
        			'Tech_ID' => $tech_id,
        			'Tech_Name' => $tech_name,
        			'Amount' => $amount,
                    'DateTime_Stamp'=>$datetimeStamp);

        if ($amount != NULL) $fields["Amount"] = $amount;

        $result = Core_Database::insert('WOChangeStatusLog',$fields);        
    }
        
}
