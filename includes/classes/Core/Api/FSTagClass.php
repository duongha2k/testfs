<?php
class Core_Api_FSTagClass
{
    const VISIBILITY_SELECTED_CLIENTS_ONLY = 1;
    const VISIBILITY_ALL_CLIENTS = 2;
    const TAG_SOURCE_NEW = "";
    const TAG_SOURCE_CERTIFICATIONS = "certifications";
    const CertID_ErgoMotionKeyContractor = 41;
    const CertID_ErgoMotionCertifiedTechnician = 42;
    const FSTagID_ErgoMotionKeyContractor = 27;
    const FSTagID_ErgoMotionCertifiedTechnician = 28;
    
    //13912, 13931
    public function createFSTag($tagTitle,$visibililyID,$tagArt,
            $companyIDArray,$addByUser,$tagName,$tagSource,$insertTechs=0,
            $hideFromTechs=0,$canNonGPMEdit=0,$canGPMEdit=1,$canTechEdit=0,
            $hasMultiTagLevel=0,$tagLevelsData=null
        )
    {
        //echo("<br/>createFSTag->tagLevelsData: ");print_r($tagLevelsData);die();
        $tagTitle = trim($tagTitle);
        $err = array();
        //--- validate
        if(empty($tagTitle)) $err[] = "Tag title is empty but required.";
        if($this->isDuplicate_Title($tagTitle)==1) $err[] = "Tag tille is duplicate";
        if(empty($tagArt) && $hasMultiTagLevel===0){
            $err[] = "Tag art is empty but required.";  
        } 
        if($visibililyID==self::VISIBILITY_SELECTED_CLIENTS_ONLY &&  (empty($companyIDArray) || count($companyIDArray)==0) ) $err[] = "Client is empty but required.";
        if(empty($tagName)) $err[] = "Tag name is empty but required.";
        if($this->isDuplicate_Name($tagName)==1) $err[] = "Tag name is duplicate";
                
        if(count($err) > 0) return $err;
        
        //--- insert into fstags
        $updateFields = array();
        $updateFields['Title'] = $tagTitle;
        $updateFields['VisibilityId'] = $visibililyID;
        $updateFields['TagArt'] = $tagArt;
        $updateFields['Active'] = 1;
        $updateFields['AddedDate'] = date("Y-m-d H:i:s");
        $updateFields['AddedByUser'] = $addByUser;        
        $updateFields['Name'] = $tagName;        
        $updateFields['Source'] = $tagSource;      
        $updateFields['HasMultiTagLevels'] = $hasMultiTagLevel; //13931     
        
        //13874
         if($hideFromTechs==0 || $hideFromTechs==1 || $hideFromTechs==2) {
            $updateFields['HideFromTechs'] = $hideFromTechs;   
         }
        //end 13874 
        //13912
         if($canNonGPMEdit==0 || $canNonGPMEdit==1) {
            $updateFields['CanNonGPMEdit'] = $canNonGPMEdit;   
         }
         if($canGPMEdit==0 || $canGPMEdit==1) {
            $updateFields['CanGPMEdit'] = $canGPMEdit;   
         }
         if($canTechEdit==0 || $canTechEdit==1) {
            $updateFields['CanTechEdit'] = $canTechEdit;   
         }        
        //end 13912        
        $result = Core_Database::insert('fstags',$updateFields);        
        
        //--- id
        $tagInfo = $this->getFSTagInfo_byTitle($tagTitle);
        $tagID = $tagInfo['id'];
        
        if(empty($tagID))
        {
            $err[] = "Insert tag fail.";
            return $err;
        }
        
        //--- insert into fstag_clients
        foreach($companyIDArray as $companyID)
        {
            $updateFields = array();
            $updateFields['FSTagId'] = $tagID;
            $updateFields['CompanyId'] = $companyID;
            $result = Core_Database::insert('fstag_clients',$updateFields);
        }
        
        //--- get data from tech_certificates and insert into fstags_techs
        if($tagSource=='certifications' && $insertTechs==1)
        {
            $db = Core_Database::getInstance();  
            $select = $db->select();
            $select->from(array('tc'=>'tech_certification'),
                array('TechID','certification_id','number','dateExpired','date')
            );
            $select->join(array('c'=>'certifications'),
                    'c.id=tc.certification_id',
                    array('name')
            );
            $select->where("c.name = '$tagName'");
            $records = Core_Database::fetchAll($select);  
            if(!empty($records) && count($records)>0)
            {
                foreach($records as $rec)
                {                
                    $updateFields = array();
                    $updateFields['FSTagId'] = $tagID;
                    $updateFields['TechId'] = $rec['TechID'];
                    $updateFields['Number'] = $rec['number'];
                    $updateFields['DateExpired'] = $rec['dateExpired'];
                    $updateFields['Date'] = $rec['date'];
                    $result = Core_Database::insert('fstag_techs',$updateFields);
                }
            }
        }
                
        //--- insert multi tag levels, 13931
//$tagLevelsData = array();
//$tagLevelsData[]=array('Title'=>$levelTitle,'TagArt'=>$levelTagArt,'DisplayOrder'=>$levelDisplayOrder);
        if($hasMultiTagLevel==1 && !empty($tagLevelsData) && count($tagLevelsData)>0){
            foreach($tagLevelsData as $tagLevel)
            {
                $tagLevel['fstag_id'] = $tagID;
                $result = Core_Database::insert('fstag_levels',$tagLevel);        
            }
        }
                
        return $err;                
    }
    
    public function createFSTag_FromExistingTag($tagName,$tagTitle,$tagSource,$addByUser)
    {
        $tagTitle = trim($tagTitle);
        $err = array();
        //--- validate
        if(empty($tagTitle)) $err[] = "Tag title is empty but required.";
        if($this->isDuplicate_Title($tagTitle)==1) $err[] = "Tag tille is duplicate";
        if(empty($tagName)) $err[] = "Tag name is empty but required.";
        if($this->isDuplicate_Name($tagName)==1) $err[] = "Tag name is duplicate";
                
        if(count($err) > 0) return $err;
        
        //--- insert into fstags
        $updateFields = array();
        $updateFields['Title'] = $tagTitle;
        $updateFields['VisibilityId'] = self::VISIBILITY_ALL_CLIENTS;
        $updateFields['TagArt'] = '';
        $updateFields['Active'] = 1;
        $updateFields['AddedDate'] = date("Y-m-d H:i:s");
        $updateFields['AddedByUser'] = $addByUser;        
        $updateFields['Name'] = $tagName;        
        $updateFields['Source'] = $tagSource;        
        
        $result = Core_Database::insert('fstags',$updateFields);        
        
        //--- id
        $tagInfo = $this->getFSTagInfo_byTitle($tagTitle);
        $tagID = $tagInfo['id'];
        
        if(empty($tagID))
        {
            $err[] = "Insert tag fail.";
            return $err;
        }
                                
        return $err;                
    }
    
     public function getFSTagInfo_byTitle($tagTitle)
     {
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from("fstags");
        $select->where("Title = ?", $tagTitle);
        $records = Core_Database::fetchAll($select);  
        if(empty($records) || count($records)==0) return null;
        else return $records[0];
     }
     
     public function getFSTagInfo_byName($tagName)
     {
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from("fstags");
        $select->where("Name = '$tagName'");
        $records = Core_Database::fetchAll($select);  
        if(empty($records) || count($records)==0) return null;
        else return $records[0];
     }

     
     public function isDuplicate_Title($tagTitle)
     {
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from("fstags");
        $select->where("Title = '$tagTitle'");
        $records = Core_Database::fetchAll($select);  
        if(empty($records) || count($records)==0) return 0;
        else return 1;         
     }

     public function isDuplicate_Name($tagName)
     {
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from("fstags");
        $select->where("Name = '$tagName'");
        $records = Core_Database::fetchAll($select);  
        if(empty($records) || count($records)==0) return 0;
        else return 1;         
     }
     
     public function getExistingTagList()
     {
         //new Zend_Db_Expr('"friend" as type')
        $expr =  '"'.self::TAG_SOURCE_CERTIFICATIONS.'" as TagSource';
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from(array('c'=>"certifications"),
            array('TagName'=>'name',
                new Zend_Db_Expr($expr))
        );
        $select->joinLeft(array('ft'=>'fstags'),
            'ft.Name = c.name',
            array('TagTitle'=>'(CASE WHEN ft.Title is null THEN c.label ELSE ft.Title END)')
        );
        
        $select->where("c.hide = 0");
        $records = Core_Database::fetchAll($select);  
        if(empty($records) || count($records)==0){
            return null;  
        } else {
            return $records;           
        } 
     }
     
     //13624, 13874, 13912, 13931
     /*
     * $visibility: '' , 1: Clients Only, 2:All Clients
     * $activity: '',0,1
     * $extFilters-keys:
     *      - HideFromTechs
     *      - CanNonGPMEdit
     *      - CanGPMEdit
     *      - CanTechEdit
     *      - HasMultiTagLevels
     */
     public function getTagSearchResult($visibility,$companyID,$tagTitleLike,$addByUsername,$activity,$forCount=0,$limit=0,$offset=0,$sort='',$extFilters=null)
     {
         //13931
        $fields = array('id','Title','VisibilityId','AddedByUser','AddedDate','Active','TagArt','Name','Source','HideFromTechs','CanNonGPMEdit','CanGPMEdit','CanTechEdit','HasMultiTagLevels');
        //end 13931
        $tc_fields = array('CompanyId'=>"(CASE WHEN t.VisibilityId=2 THEN NULL ELSE CompanyId END) ");
        
        if($forCount==1)
        {
            $fields = array('NUM'=>'(Count(*))');
            $tc_fields = array();
        }

        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from(array('t'=>'fstags'),$fields);
        $select->joinLeft(array('tc'=>'fstag_clients'),
            'tc.FSTagId = t.id',
            $tc_fields
        );
        
        if($visibility != '')
        {
            $select->where("t.VisibilityId = '$visibility'");    
        }
        
        if(!empty($companyID))
        {
            $select->where("tc.CompanyId = '$companyID'");    
        }
        
        if(!empty($tagTitleLike))
        {
            $select->where("t.Title like '%$tagTitleLike%'");    
        }
        
        if($addByUsername != '')
        {
            $select->where("t.AddedByUser = '$addByUsername'");    
        }
        
        if($activity != '')
        {
            $select->where("t.Active = '$activity'");    
        }

        //13931
        if(!empty($extFilters) && count($extFilters)>0){
            if(isset($extFilters['HideFromTechs'])){
                $select->where("t.HideFromTechs = ?",$extFilters['HideFromTechs']);  
            }
            if(isset($extFilters['CanNonGPMEdit'])){
                $select->where("t.CanNonGPMEdit = ?",$extFilters['CanNonGPMEdit']);                    }
            if(isset($extFilters['CanGPMEdit'])){
                $select->where("t.CanGPMEdit = ?",$extFilters['CanGPMEdit']);                          }
            if(isset($extFilters['CanTechEdit'])){
                $select->where("t.CanTechEdit = ?",$extFilters['CanTechEdit']);                        }
            if(isset($extFilters['HasMultiTagLevels'])){
                $select->where("t.HasMultiTagLevels = ?",$extFilters['HasMultiTagLevels']);                        }
                
        }
        //end 13931
        
        if($forCount==0 && !empty($limit))
        {
            $select->limit($limit,$offset);
        }
        
        if(!empty($sort))
        {
            $select->order($sort);
        }
        $select->distinct();
        $records = Core_Database::fetchAll($select);  
        
        if($forCount==1)
        {
            if(empty($records) || count($records)==0) return 0;
            else return $records[0]['NUM'];            
        }
        
        if(empty($records) || count($records)==0) return null;
        else return $records;

     }
     //--- 13624, 13931
     public function getFSTagInfo($fsTagID)
     {
        $fields = array('id','Title','VisibilityId','AddedByUser','AddedDate','Active','TagArt','Name','Source','IsExistingTag','Source');
        $tc_fields = array('CompanyId');
        
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from('fstags');
        $select->where("id = '$fsTagID'");    
        
        $records = Core_Database::fetchAll($select);    
        if(empty($records) || count($records)==0)                
        {
            return null;
        }
        else
        {
            //13931
            $rec = $records[0];
            $tagLevelsData = array();
            $select_1 = $db->select();
            $select_1->from("fstag_levels");
            $select_1->where("fstag_id = '$fsTagID'");
            $select_1->order("DisplayOrder asc");
            $records_1 = Core_Database::fetchAll($select_1);
            if(!empty($records_1) && count($records_1)>0){
                foreach($records_1 as $rec_1){
                    $tagLevelsData[]=$rec_1;
                }
            }
            $rec['TagLevelsData']=$tagLevelsData;            
            return $rec;
            //end 13931
        }
     }
     public function getFSTagTechInfo($fsTagID,$techID)
     {        
     // id     FSTagId     TechId     Number     DateExpired     Date
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from('fstag_techs',array('*'));
        $select->where("FSTagId = '$fsTagID'");    
        $select->where("TechId = '$techID'");            
        $records = Core_Database::fetchAll($select);    
        
        if(empty($records) || count($records)==0)                
        {
            return null;
        }
        else
        {
            return $records[0];
        }
     }
     
     public function getFSTagClientInfo($fsTagID,$companyID)
     {        
     //Full Texts     id     FSTagId     CompanyId
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from('fstag_clients',array('*'));
        $select->where("FSTagId = '$fsTagID'");    
        $select->where("CompanyId = '$companyID'");            
        $records = Core_Database::fetchAll($select);    
        
        if(empty($records) || count($records)==0)                
        {
            return null;
        }
        else
        {
            return $records[0];
        }
     }
     
     public function getFSTagCompanyList($fsTagID)
     {
         //id     FSTagId     CompanyId
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from('fstag_clients',array('FSTagId','CompanyId'));
        $select->where("FSTagId = '$fsTagID'");    
        
        $records = Core_Database::fetchAll($select);    
        if(empty($records) || count($records)==0)                
        {
            return null;
        }
        else
        {
            return $records;
        }         
     }
     
     //--- 13624,13874,13912,13931
     public function updateFSTag_ByID($fsTagID,$tagTitle,$visibililyID,$tagArt,$active,
     $companyIDArray,$hideFromTechs=0,$canNonGPMEdit=0,$canGPMEdit=1,$canTechEdit=0,
     $hasMultiTagLevel='',$tagLevelsData=null)
     {
         if(empty($fsTagID)) return;
         
         //13931         
         $FSTagInfo = $this->getFSTagInfo($fsTagID);
         $oldTagLevelsData = $FSTagInfo['TagLevelsData'];
         //end 13931
         
         //--- update fstags
         $criteria = "id = '$fsTagID'";
         $updateFields = array();
         if(!empty($tagTitle)) $updateFields['Title'] = $tagTitle;
         if(!empty($visibililyID)) $updateFields['VisibilityId'] = $visibililyID;
         if(!empty($tagArt)) $updateFields['TagArt'] = $tagArt;
         //13931
         if($hasMultiTagLevel===0 || $hasMultiTagLevel===1) {
            $updateFields['HasMultiTagLevels'] = $hasMultiTagLevel;   
         }         
         //end 13931
         if($active===0 || $active===1) {
            $updateFields['Active'] = $active;   
         }
         //13874 HideFromTechs
         if(isset($hideFromTechs)) {
            $updateFields['HideFromTechs'] = $hideFromTechs;   
         }
         //13912
         if($canNonGPMEdit===0 || $canNonGPMEdit===1) {
            $updateFields['CanNonGPMEdit'] = $canNonGPMEdit;   
         }
         if($canGPMEdit===0 || $canGPMEdit===1) {
            $updateFields['CanGPMEdit'] = $canGPMEdit;   
         }
         if($canTechEdit===0 || $canTechEdit===1) {
            $updateFields['CanTechEdit'] = $canTechEdit;   
         }        
        //end 13912                 
         if(count($updateFields) > 0)
         {
            $result = Core_Database::update("fstags",$updateFields,$criteria);
         }
         
         //--- update fstag_clients
         if($visibililyID==2){ //Global tag
              //--- delete
             $criteria = "FSTagId = '$fsTagID'";
             $db = Core_Database::getInstance();
             $result = $db->delete('fstag_clients',$criteria);
         } else if(count($companyIDArray) > 0) {
             $isThereCompanyID = 0;
             foreach($companyIDArray as $companyID)
             {
                 if(!empty($companyID)) {
                     $isThereCompanyID = 1;
                     break;
                 }     
             }
             
             if($isThereCompanyID==1)
             {
                 //--- delete
                 $criteria = "FSTagId = '$fsTagID'";
                 $db = Core_Database::getInstance();
                 $result = $db->delete('fstag_clients',$criteria);
                 
                 //--- insert
                 foreach($companyIDArray as $companyID)
                 {                 
                    $FSTagClientInfo = $this->getFSTagClientInfo($fsTagID,$companyID);
                    if(empty($FSTagClientInfo) || count($FSTagClientInfo)==0)
                    {
                        if(!empty($companyID))
                        {
                            $updateFields = array();
                            $updateFields['FSTagId'] = $fsTagID;
                            $updateFields['CompanyId'] = $companyID;
                            $result = Core_Database::insert('fstag_clients',$updateFields);
                        }                    
                    }
                 }
             }
         }
         //--- update fstag_levels
         //13931
         $db = Core_Database::getInstance();
         if($hasMultiTagLevel===0){
            //--- delete
             $criteria = "fstag_id = '$fsTagID'";
             $result = $db->delete('fstag_levels',$criteria);
         } else if($hasMultiTagLevel===1) {
             if(!empty($tagLevelsData) && count($tagLevelsData > 0)){
                 //$tagLevelsData = array();
//$tagLevelsData[]=array('id'=>$levelID,'Title'=>$levelTitle,'TagArt'=>$levelTagArt,'DisplayOrder'=>$levelDisplayOrder);
                
                //delete level that is not in $tagLevelsData
                if(!empty($oldTagLevelsData) && count($oldTagLevelsData)>0){
                    foreach($oldTagLevelsData as $oldTagLevel){
                        $oldLevelID = $oldTagLevel['id'];
                        $inUpdate = false;
                        foreach($tagLevelsData as $tagLevel){
                            if($oldLevelID == $tagLevel['id']){
                                $inUpdate=true;
                                break;
                            }
                        }
                        if($inUpdate==false){
                            $criteria = "fstag_id = '$fsTagID' AND id='$oldLevelID'";
                            $result = $db->delete('fstag_levels',$criteria);
                        }
                    }
                }
                //insert or update
                foreach($tagLevelsData as $tagLevel){
                    $levelID = $tagLevel['id'];
                    unset($tagLevel['id']);
                    if($levelID > 0){
                        $criteria="id = '$levelID'";
                        $result = Core_Database::update("fstag_levels",$tagLevel,$criteria);
                    } else {
                        $tagLevel['fstag_id']=$fsTagID;
                        $result = Core_Database::insert("fstag_levels",$tagLevel);
                    }
                } // end of for
             } // end of if
         }
         //end 13931
     }
     
     //--- 13624, 13702, 13839 ,14071
     public function getFSTagList_ForCompany($isGPM,$companyID,$forClientCredentialsOnly=1,$withoutPrivateTech=1,$withoutFSCheckINN=0)
     {         
        //select * from fstags order by Title;
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from(array('t'=>'fstags'),array('*'));
        $select->joinLeft(array('tc'=>'fstag_clients'),
            'tc.FSTagId = t.id',
            array('DisplayOrder'=>"(CASE WHEN (t.VisibilityId = 1 AND tc.CompanyId = '$companyID') THEN 1 WHEN (t.VisibilityId = 2) THEN 2 ELSE 3 END)")
        );
        $select->where("t.Active = 1");
        if($isGPM!=1 && !empty($companyID))
        {
            $select->where("(t.VisibilityId = 1 AND tc.CompanyId = '$companyID') OR (t.VisibilityId = 2) ");    
        }
        if($forClientCredentialsOnly==1)
        {
            $select->where("t.DisplayOnClientCredentials = 1");
        }
        //13702
        if($withoutPrivateTech){
            $select->where("t.Title NOT LIKE '%Private Tech%'");
        }
        //end 13702
        //14071
        if($withoutFSCheckINN){
            $select->where("t.Name != 'CLCLodgingDiscountProgram'");
			$select->where("t.Name != 'FieldSolutionsCheckINNHotelSavingsCard'");
        }
        //$select->order("DisplayOrder ASC"); 13839
        $select->group("t.id");//13839
        $select->order("t.Title ASC");
        $select->distinct();
        //echo("<br/>");print_r($select->__toString());echo("<br/>");
        $records = Core_Database::fetchAll($select);    
        if(empty($records) || count($records)==0) {
            return null;   
        } else{
            //13931
            $ret = array();
            foreach($records as $rec){
                $fsTagID = $rec['id'];
                $tagLevelsData = array();
                $select_1 = $db->select();
                $select_1->from("fstag_levels");
                $select_1->where("fstag_id = '$fsTagID'");
                $select_1->order("DisplayOrder asc");
                $records_1 = Core_Database::fetchAll($select_1);
                if(!empty($records_1) && count($records_1)>0){
                    foreach($records_1 as $rec_1){
                        $tagLevelsData[]=$rec_1;
                    }
                }
                $rec['TagLevelsData']=$tagLevelsData;            
                $ret[]=$rec;
            }
            return $ret;
            //end 13931
        }
     }
     //---- 13931
     public function getTechIDArray_forFSTag($fstagIDArray)
     {         
         if(empty($fstagIDArray)) return null;
         if(is_array($fstagIDArray) && count($fstagIDArray)==0) return null;
         
         if(!is_array($fstagIDArray)){
             $fstagIDArray = array($fstagIDArray);
         }

         if(count($fstagIDArray) > 0)
         {
             foreach($fstagIDArray as $fstagID)
             {
                //$FSTagInfo = $this->getFSTagInfo($fstagID);//13931
                $this->insert_Tofstagstechs_FromExistingTagData($fstagID);
             }
         }

         $TechIDArray = array();
         //13931
         $db = Core_Database::getInstance();  
         foreach($fstagIDArray as $k=>$fstagID)
         {
             
             if(strpos($fstagID,"_")!==false) continue;
             $select = $db->select();
             $select->from('fstag_techs',array('FSTagId','TechId'));
             $select->where("FSTagId = '$fstagID'");  
             $records = Core_Database::fetchAll($select);    
             
             $tempTechIDArray = array();
             if(!empty($records) || count($records)>0)
             {
                 foreach($records as $rec) {
                    $tempTechIDArray[] = $rec['TechId'];
                 }
                 
                 if(count($TechIDArray) > 0) {
                    $TechIDArray = array_intersect($TechIDArray,$tempTechIDArray);      
                 }  else {
                    $TechIDArray = $tempTechIDArray;
                 }
             }
         }
         //--- for tags with multi-tag-levels
         $MultiLevelsArray = array();
         foreach($fstagIDArray as $k=>$v)
         {
             if(strpos($v,"_")!==false){
                 $part = explode('_',$v);
                 $tagID = $part[0];
                 $levelID = $part[1];
                 $levelOrder = $part[2];
                 if(!in_array($tagID,$fstagIDArray)){
                    if(!isset($MultiLevelsArray[$tagID])){
                        $MultiLevelsArray[$tagID] = array($levelOrder);
                    } else {
                        $MultiLevelsArray[$tagID][]=$levelOrder;
                    }
                 }
             }
         }
         
         if(count($MultiLevelsArray)>0){
             $select = $db->select();
             $select->from(array('tagte'=>'fstag_techs'),array('FSTagId','TechId'));
             $select->join(array('tagle'=>'fstag_levels'),
                    'tagle.id = tagte.FSTagLevelId',
                    array('DisplayOrder')
             );
             foreach($MultiLevelsArray as $tagID=>$MultiLevels){
                 $select->where("tagte.FSTagId = '$tagID'");                 
                 if(count($MultiLevels)==1) {
                     $levelOrder = $MultiLevels[0];
                     $select->where("tagle.DisplayOrder >= '$levelOrder'");
                 } else {
                     $orWhere = "";
                     foreach($MultiLevels as $levelOrder){
                         if(!empty($orWhere)){
                             $orWhere .= " OR ";
                         }
                         $orWhere .=" (tagle.DisplayOrder >= '$levelOrder') ";
                     }
                     $select->where($orWhere);
                 }
             }//end of for
             //print_r($select->__toString());
             $records = Core_Database::fetchAll($select);    
             $tempTechIDArray_1 = array();
             if(!empty($records) || count($records)>0)
             {
                 foreach($records as $rec) {
                    $tempTechIDArray_1[] = $rec['TechId'];
                 }
                 
                 if(count($TechIDArray) > 0) {
                    $TechIDArray = array_intersect($TechIDArray,$tempTechIDArray_1);
                 }  else {
                    $TechIDArray = $tempTechIDArray_1;
                 }
             }                 
         }         
         //echo("<br/>TechIDArray: ");print_r($TechIDArray);
         //end 13931
         return $TechIDArray;
     }
     
     //---- 13624, 13931
     public function insert_Tofstagstechs_FromExistingTagData($fstagID)
     {
         $FSTagInfo = $this->getFSTagInfo($fstagID);
         //13931         
         if(empty($FSTagInfo)){
             return;
         }
         //end 13931
         $tagName = $FSTagInfo['Name'];
         $isExistingTag = $FSTagInfo['IsExistingTag'];
         $source = $FSTagInfo['Source'];
         //
         if($isExistingTag==1 && $source='certifications')
         {
             $db = Core_Database::getInstance();  
             $select = $db->select();
             $select->from(array('tc'=>'tech_certification'),
                    array('TechID','certification_id','number','dateExpired','date')
             );
             $select->join(array('c'=>'certifications'),
                        'c.id=tc.certification_id',
                        array('name')
             );
             $select->where("c.name = '$tagName'");
             $records = Core_Database::fetchAll($select);  
             
             if(!empty($records) && count($records)>0)
             {
                //--- delete
                $criteria = "FSTagId = '$fstagID'";
                $db = Core_Database::getInstance();
                $result = $db->delete('fstag_techs',$criteria);
                 
                //--- insert
                foreach($records as $rec)
                {                
                    $updateFields = array();
                    $updateFields['FSTagId'] = $fstagID;
                    $updateFields['TechId'] = $rec['TechID'];
                    $updateFields['Number'] = $rec['number'];
                    $updateFields['DateExpired'] = $rec['dateExpired'];
                    $updateFields['Date'] = $rec['date'];
                    $result = Core_Database::insert('fstag_techs',$updateFields);
                }
             }
         }
     }
     
     //---- 13624, 13702, 13735, 13839, 13931, 13910
     /*
     $extParameterArray-keys:
        - VisibilityId:  value = 1: Client Only, 
                                 2: Public (all clients)
                                 3: GPM Only
                                 4: Client Only and GPM Only
        - SortByTitle: 1      
        - UpdateData: NO                   
     */
     public function getFSTagList_forTech_forCompany($techID,$companyID,
            $withOtherCompanyTags=1,$all=0,$callerIsGPM=1,
            $techInfo,$withoutPrivateTech=1,
            $extParameterArray=null)
     {         
         if(empty($techID)) {return; }
         
         //--- 13910
         $updateData = true;
         if(!empty($extParameterArray) && count($extParameterArray)>0){
             if($extParameterArray['UpdateData']=='NO'){
                 $updateData = false;
             }
         }
         //end 13910
         //echo('<br/>$updateData: ');print_r($updateData);
         
         $db = Core_Database::getInstance();  
         //--- all fs-tags
         $select = $db->select();
         $select->from('fstags');
         $select->where("Active = 1");
         $select->order("Title ASC");
         $allTags = Core_Database::fetchAll($select);    
         if(empty($allTags) || count($allTags)==0) {  return null;   }
         
         //--- list tagID for company
         $select = $db->select();
         $select->from('fstag_clients',array('FSTagId'));
            $select->where("CompanyId = ?", $companyID);
         $records = Core_Database::fetchAll($select);    
         $tagIDArray_forCompany = array();
         if(!empty($records) && count($records) > 0)
         {
             foreach($records as $rec)
             {
                 $tagIDArray_forCompany[] = $rec['FSTagId'];
             }
         }
         //--- 13910
         if(empty($techInfo)) {
            $select = $db->select();
            $select->from(Core_Database::TABLE_TECH_BANK_INFO,
                array('Bg_Test_Pass_Full','DrugPassed','BG_Test_ResultsDate_Full','DatePassDrug')
            );
            $select->where("TechID = ?", $techID);
            $records = Core_Database::fetchAll($select);    
            if(!empty($records) && count($records) > 0){
                $techInfo = $records[0];
            }
            //$techInfo = Core_Tech::getProfile($techID, false, API_Tech::MODE_TECH, NULL, true);   
         }
         //end 13910
         
        $apiTechClass = new Core_Api_TechClass();
		$isExistingCertTagArr = array();
		$isExistingDellTagArr = array();
        
   if($updateData) {    
         $techGGEStatusArray = $apiTechClass->getStatusArrayForGGECert($techID);
         //---
         foreach($allTags as $tag)
         {         	
             $isExistingTag = $tag['IsExistingTag'];
             $source = $tag['Source'];
             $tagName = $tag['Name'];
             $fsTagID = $tag['id'];
             //--- get data of this tech from tech_certificates and insert into fstags_techs
             if($isExistingTag==1 && $source=='certifications' && $tagName!='GGETechEvaluation')
             {
             	 $isExistingCertTagArr[$tagName] = array('FSTagName'=>$tagName,
                                                        'FSTagID'=>$fsTagID);
                 //Moved this section outside of foreach, below
             }
             
             //--- get data of this tech from TechBankInfo and insert into fstags_techs
             if($isExistingTag==1 && $source=='TechBankInfo' && 
                    $tagName=='Dell_MRA_Compliant')
             {
                 
                $bgCheckFullStatus = $techInfo['Bg_Test_Pass_Full'];
                $drugTestStatus = $techInfo['DrugPassed'];
                if($bgCheckFullStatus==1 && $drugTestStatus==1)
                {
                    $isExistingDellTagArr[$tagName] = array('FSTagName'=>$tagName,
                                                        'FSTagID'=>$fsTagID);
                   //Moved this section outside of foreach, below
                }   
                else
                {
                   //--- delete
                   $criteria = "FSTagId = '$fsTagID' AND TechId='$techID'";
                   $db = Core_Database::getInstance();
                   $result = $db->delete('fstag_techs',$criteria);                    
                }      
             }
             //13837         
             if($isExistingTag==1 && $source=='certifications' && 
                    $tagName=='GGETechEvaluation')
             {                 
                 if(!empty($techGGEStatusArray))
                 {
                     $GGEPass = false;
                     $GGEDate = '';
                     if($techGGEStatusArray['GGE1stAttemptStatus']=='Pass'){
                         $GGEPass = true;
                         $GGEDate = $techGGEStatusArray['GGE1stAttemptDate'];
                     } else if ($techGGEStatusArray['GGE2ndAttemptStatus']=='Pass'){
                         $GGEPass = true;
                         $GGEDate = $techGGEStatusArray['GGE2ndAttemptDate'];
                     }
                     
                     if($GGEPass){
                          $fsTagTechInfo = $this->getFSTagTechInfo($fsTagID,$techID);
                          if(empty($fsTagTechInfo)){
                             //insert 
                             $updateFields = array();
                             $updateFields['FSTagId'] = $fsTagID;
                             $updateFields['TechId'] = $techID;    
                             $updateFields['Date'] = $GGEDate;    
                             $result = Core_Database::insert('fstag_techs',$updateFields);
                          } else {
                             //update
                             $criteria = "FSTagId = '$fsTagID' AND TechId='$techID'";
                             $updateFields = array();
                             $updateFields['Date'] = $GGEDate;    
                             $result = Core_Database::update('fstag_techs',$updateFields,$criteria);
                          }                           
                     } else {
                          //delete
                          $criteria = "FSTagId = '$fsTagID' AND TechId='$techID'";
                          $db = Core_Database::getInstance();
                          $result = $db->delete('fstag_techs',$criteria);
                     }
                 }
             }
             //End 13837
         }//end of for
         
         //13874
         //moved outside of the foreach above
         if(!empty($isExistingCertTagArr) && count($isExistingCertTagArr) > 0 ){             
             $fsTagIDs = "";
             $fsTagNameArray = array();
             foreach($isExistingCertTagArr as $k=>$v) {
                $fsTagIDs .= empty($fsTagIDs) ? $v['FSTagID'] : ','.$v['FSTagID'];
                $fsTagNameArray[] = $k;
             }
             
             $db = Core_Database::getInstance();  
             $select = $db->select();
             $select->from(array('tc'=>'tech_certification'),
                    array('TechID','certification_id','number','dateExpired','date')
             );
             $select->join(array('c'=>'certifications'),
                        'c.id=tc.certification_id',
                        array('name')
             );
             $select->where("c.name IN (?)", $fsTagNameArray);
             $select->where("tc.TechID = '$techID'");     
                   
             $records = Core_Database::fetchAll($select);  
             
             if(!empty($records) && count($records)>0) {
               //--- delete
               $criteria = "TechId='$techID' AND FSTagId IN ($fsTagIDs)";
               $db = Core_Database::getInstance();
               $result = $db->delete('fstag_techs',$criteria);
               //--- insert  
                foreach($records as $rec)
                {                
                    $tagName = $rec['name'];
                    $fstagInfo = $isExistingCertTagArr[$tagName];
                    $fsTagID = $fstagInfo['FSTagID'];
                    $updateFields = array();
                    $updateFields['FSTagId'] = $fsTagID;
                    $updateFields['TechId'] = $rec['TechID'];
                    $updateFields['Number'] = $rec['number'];
                    $updateFields['DateExpired'] = $rec['dateExpired'];
                    $updateFields['Date'] = $rec['date'];
                    $result = Core_Database::insert('fstag_techs',$updateFields);
                }
             }  else {
               //--- delete
               $criteria = "TechId='$techID' AND FSTagId IN ($fsTagIDs)";
               $db = Core_Database::getInstance();
               $result = $db->delete('fstag_techs',$criteria);
             }
         }
         
         if(!empty($isExistingDellTagArr)){
             $fsTagIDs = "";
             $fsTagIDArray = array();
             $fsTagNameArray = array();
             foreach($isExistingDellTagArr as $k=>$v) {
                $fsTagIDs .= empty($fsTagIDs) ? $v['FSTagID'] : ','.$v['FSTagID'];
                $fsTagNameArray[] = $k;
                $fsTagIDArray[]=$k;
             }
             //--- for AdjustableBedExpert
             $this->Update_AdjustableBedExpert($techID);

            $bgDate = $techInfo['Bg_Test_ResultsDate_Full'];//Bg_Test_ResultsDate_Full
            $dtDate = $techInfo['DatePassDrug'];//DatePassDrug
            ///dellMRACompliantDate = bgDate > dtDate ? bgDate : dtDate;
            $dellMRACompliantDate="";
            if(empty($bgDate) && empty($dtDate)){
                $dellMRACompliantDate="";    
            } else if(empty($bgDate) && !empty($dtDate)){
                $dellMRACompliantDate= date("Y-m-d",strtotime($dtDate)) ;    
            } else if(!empty($bgDate) && empty($dtDate)){
                $dellMRACompliantDate= date("Y-m-d",strtotime($bgDate)) ;    
            } else if(!empty($bgDate) && !empty($dtDate)){
                $tsbgDate = strtotime($bgDate);
                $tsdtDate = strtotime($dtDate);
                   if($tsbgDate > $tsdtDate){
                    $dellMRACompliantDate= date("Y-m-d",strtotime($bgDate)) ;    
                } else {
                       $dellMRACompliantDate= date("Y-m-d",strtotime($dtDate)) ;    
                }
            }
             //--- if exist->update, else insert
             $db = Core_Database::getInstance();  
             $select = $db->select();
             $select->from('fstag_techs');
             $select->where("TechId = '$techID'");
             $select->where("FSTagId IN (?)",$fsTagNameArray);  
             $records = Core_Database::fetchAll($select);    
             
             if(!empty($records) && count($records)>0)
             {
                 $criteria = "TechId='$techID' AND FSTagId IN ($fsTagIDs)";                  
                 $updateFields=array();
                 $updateFields['Date']=$dellMRACompliantDate;
                 $result = Core_Database::update('fstag_techs',$updateFields,$criteria);       
             }else{
                 
                  $updateFields = array();
                  $updateFields['FSTagId'] = $fsTagIDArray[0];
                  $updateFields['TechId'] = $techID;
                  $updateFields['Date'] = $dellMRACompliantDate;
                  $result = Core_Database::insert('fstag_techs',$updateFields);
              }
         }
   }      
         //--- get fstags for tech
         //HideFromTechs     CanNonGPMEdit     CanGPMEdit     CanTechEdit
         $db = Core_Database::getInstance();  
         $select = $db->select();
         //13912
         $select->from(array('tag'=>'fstags'),array('FSTagId'=>'id','Title','VisibilityId','Name','Active','ExistingImagePath','IsExistingTag','TagArt','HideFromTechs','CanNonGPMEdit','CanGPMEdit','CanTechEdit','HasMultiTagLevels'));
         //end 13912
         if($all==1)
         {
             $select->joinLeft(array('tagte'=>'fstag_techs'),
                    "tagte.FSTagId = tag.id AND tagte.TechId = '$techID'" ,
                    array('TechId','Number','DateExpired','Date','FSTagLevelId')
             );
             $select->joinLeft(array('taglevel'=>'fstag_levels'),
                    "taglevel.id = tagte.FSTagLevelId" ,
                    array('LevelTagArt'=>'TagArt','LevelTitle'=>'Title','LevelOrder'=>'DisplayOrder')
             );             
         }
         else
         {
             $select->join(array('tagte'=>'fstag_techs'),
                    "tagte.FSTagId = tag.id AND tagte.TechId = '$techID'" ,
                    array('TechId','Number','DateExpired','Date','FSTagLevelId')
             );         
             $select->joinLeft(array('taglevel'=>'fstag_levels'),
                    "taglevel.id = tagte.FSTagLevelId" ,
                    array('LevelTagArt'=>'TagArt','LevelTitle'=>'Title','LevelOrder'=>'DisplayOrder')
             );                  
         }

         $select->where("tag.Active = 1");
         $select->where("tag.DisplayOnClientCredentials = 1");
         //13874
         if(empty($callerIsGPM)){
            $select->where("tag.VisibilityId <> 3");             
         }
         //end 13874
         //13702
         if($withoutPrivateTech){
            $select->where("tag.Title NOT LIKE '%Private Tech%'");
         }
         //end 13702
         //13735, 13912
         if(!empty($extParameterArray)){
             if(!empty($extParameterArray['VisibilityId'])){
                $visibilityFilter = $extParameterArray['VisibilityId'];
                if($visibilityFilter == 1 || $visibilityFilter == 2 || $visibilityFilter == 3) {
                    $select->where("tag.VisibilityId = '$visibilityFilter'");
                } else if($visibilityFilter == 4){
                    $select->where("tag.VisibilityId = 1 OR tag.VisibilityId = 2 OR tag.VisibilityId = 3");
                }
             }             
         }
         //end 13735, 13912
         $select->order("tag.Title ASC");
         $select->order("taglevel.DisplayOrder DESC");
         $select->distinct();
         
         $tags_forTech = Core_Database::fetchAll($select);    
         
         if(empty($tags_forTech) || count($tags_forTech)==0){
             return null;
         }
         
         //--- TagArt for leveltag
         $multiLevelTagID=0;
         $unsetKeys = array();
         for($i=0;$i<count($tags_forTech);$i++){             
             if($tags_forTech[$i]['FSTagId']==$multiLevelTagID){
                 $unsetKeys[]=$i;
             } 
             if($tags_forTech[$i]['HasMultiTagLevels']==1){
                 $fsTagID =  $tags_forTech[$i]['FSTagId'];
                 $tags_forTech[$i]['TagArt'] = $tags_forTech[$i]['LevelTagArt'];
                 $multiLevelTagID = $tags_forTech[$i]['FSTagId'];
                 $tagLevelsData = array();
                 $select_1 = $db->select();
                 $select_1->from("fstag_levels");
                 $select_1->where("fstag_id = '$fsTagID'");
                 $select_1->order("DisplayOrder asc");
                 $records_1 = Core_Database::fetchAll($select_1);
                 if(!empty($records_1) && count($records_1)>0){
                    foreach($records_1 as $rec_1){
                        $tagLevelsData[]=$rec_1;
                    }
                 }
                 $tags_forTech[$i]['TagLevelsData']=$tagLevelsData;
             }
         }
         if(count($unsetKeys)>0){
             foreach($unsetKeys as $key){
                 unset($tags_forTech[$key]);
             }
         }
         
         $tagArray_forTech = array();
         $sortByTitleOnly = '';
         if(!empty($extParameterArray) && $extParameterArray['SortByTitle']==1){
             $sortByTitleOnly = "1";
         }
         if($sortByTitleOnly == "1"){
             foreach($tags_forTech as $tagfortech) {
                 if($tagfortech['VisibilityId']==1 && in_array($tagfortech['FSTagId'],$tagIDArray_forCompany)) {                     
                     $tagfortech['BelongCompany']=true;
                 } else {
                     $tagfortech['BelongCompany']=false;
                     if(empty($withOtherCompanyTags) && !in_array($tagfortech['FSTagId'],$tagIDArray_forCompany) && $tagfortech['VisibilityId']==1){
                         continue;
                     }
                 }
                 $tagArray_forTech[]=$tagfortech;
             }             
//error_log ("Core_Api_FSTagClass::getFSTagList_forTech_forCompany() - check 1: " . var_export ($tagArray_forTech, true));
         } else {
             //--- for Company Tags first         
             if(count($tagIDArray_forCompany)>0)  {
                 foreach($tags_forTech as $tagfortech) {
                     if($tagfortech['VisibilityId']==1 && in_array($tagfortech['FSTagId'],$tagIDArray_forCompany)) {                     
                         $tagfortech['BelongCompany']=true;
                         $tagArray_forTech[]=$tagfortech;
                     }
                 }
//error_log ("Core_Api_FSTagClass::getFSTagList_forTech_forCompany() - check 2: " . var_export ($tagArray_forTech, true));
             }         
             //--- for Global Tags, for Only-GPM Tags, 13874
             foreach($tags_forTech as $tagfortech)  {
                 if($tagfortech['VisibilityId']==2 || $tagfortech['VisibilityId']==3) {
                     $tagfortech['BelongCompany']=false;
                     $tagArray_forTech[]=$tagfortech;
                 } 
             }
//error_log ("Core_Api_FSTagClass::getFSTagList_forTech_forCompany() - check 3: " . var_export ($tagArray_forTech, true));
             //--- for other company tags
             if($withOtherCompanyTags==1) {
                 foreach($tags_forTech as $tagfortech) {
                     if($tagfortech['VisibilityId']==1 && !in_array($tagfortech['FSTagId'],$tagIDArray_forCompany))  {
                         $tagfortech['BelongCompany']=false;
                         $tagArray_forTech[]=$tagfortech;
                     }
                 }
//error_log ("Core_Api_FSTagClass::getFSTagList_forTech_forCompany() - check 4: " . var_export ($tagArray_forTech, true));
             }                      
         }
//error_log ("Core_Api_FSTagClass::getFSTagList_forTech_forCompany(): " . var_export ($tagIDArray_forCompany, true));
         return $tagArray_forTech;         
     }//end of functions
     
     //---- 13624, 13931     
     public function getFSTagList_forTech($techID,$forClientCredentialsOnly=1,$withoutPrivateTech=1)
     {
         //--- all fs-tags
         $db = Core_Database::getInstance();  
         $select = $db->select();
         $select->from('fstags');
         $select->where("Active = 1");
         $select->order("Title ASC");
         $allTags = Core_Database::fetchAll($select);    
         if(empty($allTags) || count($allTags)==0) {  return null;   }
         //--- techInfo
         $techInfo = Core_Tech::getProfile($techID);         
         //---
         foreach($allTags as $tag)
         {
             $isExistingTag = $tag['IsExistingTag'];
             $source = $tag['Source'];
             $tagName = $tag['Name'];
             $fsTagID = $tag['id'];
             //--- get data of this tech from tech_certificates and insert into fstags_techs
             if($isExistingTag==1 && $source=='certifications')
             {
                 $db = Core_Database::getInstance();  
                 $select = $db->select();
                 $select->from(array('tc'=>'tech_certification'),
                        array('TechID','certification_id','number','dateExpired','date')
                 );
                 $select->join(array('c'=>'certifications'),
                            'c.id=tc.certification_id',
                            array('name')
                 );
                 $select->where("c.name = '$tagName'");
                 $select->where("tc.TechID = '$techID'");             
                 $records = Core_Database::fetchAll($select);  
                 
                 if(!empty($records) && count($records)>0)
                 {
                   //--- delete
                   $criteria = "FSTagId = '$fsTagID' AND TechId='$techID'";
                   $db = Core_Database::getInstance();
                   $result = $db->delete('fstag_techs',$criteria);
                   //--- insert  
                    foreach($records as $rec)
                    {                
                        $updateFields = array();
                        $updateFields['FSTagId'] = $fsTagID;
                        $updateFields['TechId'] = $rec['TechID'];
                        $updateFields['Number'] = $rec['number'];
                        $updateFields['DateExpired'] = $rec['dateExpired'];
                        $updateFields['Date'] = $rec['date'];
                        $result = Core_Database::insert('fstag_techs',$updateFields);
                    }
                 }
             }
             
                          //--- get data of this tech from TechBankInfo and insert into fstags_techs
             if($isExistingTag==1 && $source=='TechBankInfo' && 
                    $tagName=='Dell_MRA_Compliant')
             {
                $bgCheckFullStatus = $techInfo['credentialInfo']['bgCheckFullStatus'];
                $drugTestStatus = $techInfo['credentialInfo']['drugTestStatus'];
                if($bgCheckFullStatus==1 && $drugTestStatus==1)
                {
                    $bgDate = $techInfo['credentialInfo']['bgCheckFullDate'];//Bg_Test_ResultsDate_Full
                    $dtDate = $techInfo['credentialInfo']['drugTestDate'];//DatePassDrug
                    ///dellMRACompliantDate = bgDate > dtDate ? bgDate : dtDate;
                    $dellMRACompliantDate="";
                    if(empty($bgDate) && empty($dtDate)){
                        $dellMRACompliantDate="";    
                    } else if(empty($bgDate) && !empty($dtDate)){
                        $dellMRACompliantDate= date("Y-m-d",strtotime($dtDate)) ;    
                    } else if(!empty($bgDate) && empty($dtDate)){
                        $dellMRACompliantDate= date("Y-m-d",strtotime($bgDate)) ;    
                    } else if(!empty($bgDate) && !empty($dtDate)){
                        $tsbgDate = strtotime($bgDate);
                        $tsdtDate = strtotime($dtDate);
                        if($tsbgDate > $tsdtDate){
                            $dellMRACompliantDate= date("Y-m-d",strtotime($bgDate)) ;    
                        } else {
                            $dellMRACompliantDate= date("Y-m-d",strtotime($dtDate)) ;    
                        }
                    }
                    //--- if exist->update, else insert
                    $db = Core_Database::getInstance();  
                    $select = $db->select();
                    $select->from('fstag_techs');
                    $select->where("TechId = '$techID'");
                    $select->where("FSTagId = '$fsTagID'");
                    $records = Core_Database::fetchAll($select);    
                    if(!empty($records) && count($records)>0)
                    {
                        $criteria = "TechId = '$techID' AND FSTagId = '$fsTagID'";
                        $updateFields=array();
                        $updateFields['Date']=$dellMRACompliantDate;
                        $result = Core_Database::update('fstag_techs',$updateFields,$criteria);       
                    }else{
                        $updateFields = array();
                        $updateFields['FSTagId'] = $fsTagID;
                        $updateFields['TechId'] = $techID;
                        $updateFields['Date'] = $dellMRACompliantDate;
                        $result = Core_Database::insert('fstag_techs',$updateFields);
                    }
                }   
                else
                {
                   //--- delete
                   $criteria = "FSTagId = '$fsTagID' AND TechId='$techID'";
                   $db = Core_Database::getInstance();
                   $result = $db->delete('fstag_techs',$criteria);                    
                }      
             }

                          
         }//end of for

         //--- get fstags for tech, 13728
         $db = Core_Database::getInstance();  
         $select = $db->select();
         //13912
         $select->from(array('tag'=>'fstags'),
             array('FSTagId'=>'id','Title','VisibilityId','Name','Active','ExistingImagePath','IsExistingTag','TagArt','HideFromTechs','CanNonGPMEdit','CanGPMEdit','CanTechEdit','HasMultiTagLevels')
         );
         //end 13912
         $select->joinLeft(array('tagte'=>'fstag_techs'),
                "tag.id = tagte.FSTagId AND tagte.TechId='$techID'",
                array('TechId','Number','DateExpired','Date','FSTagLevelId')
         );
         $select->joinLeft(array('taglevel'=>'fstag_levels'),
                    "taglevel.id = tagte.FSTagLevelId" ,
                    array('LevelTagArt'=>'TagArt','LevelTitle'=>'Title','LevelOrder'=>'DisplayOrder')
         );                  

         $select->where("tag.Active = 1");
         if($forClientCredentialsOnly==1)
         {
            $select->where("tag.DisplayOnClientCredentials = 1");
         }
         
         //13702
         if($withoutPrivateTech){
            $select->where("tag.Title NOT LIKE '%Private Tech%'");
         }
         //end 13702
         
         $select->order("tag.Title ASC");
         $tags_forTech = Core_Database::fetchAll($select);    
         if(empty($tags_forTech) || count($tags_forTech)==0){
             return null;
         }
         
         //--- TagArt for leveltag
         $multiLevelTagID=0;
         $unsetKeys = array();
         for($i=0;$i<count($tags_forTech);$i++){             
             if($tags_forTech[$i]['FSTagId']==$multiLevelTagID){
                 $unsetKeys[]=$i;
             } 
             if($tags_forTech[$i]['HasMultiTagLevels']==1){
                 $fsTagID =  $tags_forTech[$i]['FSTagId'];
                 $tags_forTech[$i]['TagArt'] = $tags_forTech[$i]['LevelTagArt'];
                 $multiLevelTagID = $tags_forTech[$i]['FSTagId'];
                 $tagLevelsData = array();
                 $select_1 = $db->select();
                 $select_1->from("fstag_levels");
                 $select_1->where("fstag_id = '$fsTagID'");
                 $select_1->order("DisplayOrder asc");
                 $records_1 = Core_Database::fetchAll($select_1);
                 if(!empty($records_1) && count($records_1)>0){
                    foreach($records_1 as $rec_1){
                        $tagLevelsData[]=$rec_1;
                    }
                 }
                 $tags_forTech[$i]['TagLevelsData']=$tagLevelsData;
             }
         }
         if(count($unsetKeys)>0){
             foreach($unsetKeys as $key){
                 unset($tags_forTech[$key]);
             }
         }
         
         //--- 13624
         $tagArray_forTech = array();
         foreach($tags_forTech as $tagfortech)
         {
             $tagID = $tagfortech['FSTagId'];
             $tagName = $tagfortech['Name'];
             $visibilityID = $tagfortech['VisibilityId'];
             $tagArray_forTech[$tagName]=$tagfortech;             
         }
         
         return $tagArray_forTech;         
     }//end of functions
          
     public function deleteFSTagForTech($techID,$fstagID)
     {
        $FSTagInfo = $this->getFSTagInfo($fstagID);
        $IsExistingTag = $FSTagInfo['IsExistingTag'];
        $source = $FSTagInfo['Source'];
        $tagName = $FSTagInfo['Name'];

        //---  fstag_techs
        $criteria = "FSTagId = '$fstagID' AND TechId='$techID'";
        $db = Core_Database::getInstance();
        $result = $db->delete("fstag_techs",$criteria);
        
        //--- certifications
        if($IsExistingTag==1 && $source=='certifications')
        {
            $certInfo = $this->getCertInfoByName($tagName);
            if(!empty($certInfo))
            {
                $certID = $certInfo['id'];
                $criteria = "TechID = '$techID' AND certification_id='$certID'";
                $db = Core_Database::getInstance();
                $result = $db->delete("tech_certification",$criteria);                
            }
        }
        
        return $result;
     }

     //--- 13624
     public function deleteFSTagForTech_FStagName($techID,$fstagName)
     {
         $fsTagInfo = $this->getFSTagInfo_byName($fstagName);
         if(empty($fsTagInfo) || count($fsTagInfo)==0) return;
         
         $fstagID = $fsTagInfo['id'];
         $this->deleteFSTagForTech($techID,$fstagID);
     }

     //--- 13624, 13931
     public function saveFSTagForTech_FSTagName($techID,$fstagName,$Number,$DateExpired,$Date,$FSTagLevelId=null)
     {
         $fsTagInfo = $this->getFSTagInfo_byName($fstagName);
         if(empty($fsTagInfo) || count($fsTagInfo)==0) return;
         
         $fstagID = $fsTagInfo['id'];
         $this->saveFSTagForTech($techID,$fstagID,$Number,$DateExpired,$Date,$FSTagLevelId);
     }
     //13931
     public function saveFSTagForTech($techID,$fstagID,$Number,$DateExpired,$Date,
           $FSTagLevelId=null)
     {
        $FSTagInfo = $this->getFSTagInfo($fstagID);
        $IsExistingTag = $FSTagInfo['IsExistingTag'];
        $source = $FSTagInfo['Source'];
        $tagName = $FSTagInfo['Name'];
        $HasMultiTagLevels = $FSTagInfo['HasMultiTagLevels'];
        $FSTagTechInfo = $this->getFSTagTechInfo($fstagID,$techID);
        if(empty($FSTagTechInfo))
        {
            $updateFields = array();
            $updateFields['FSTagId'] = $fstagID;
            $updateFields['TechId'] = $techID;
            $updateFields['Number'] = $Number;
            $updateFields['DateExpired'] = $DateExpired;
            if(!empty($Date)){
                $Date = date("Y-m-d",strtotime($Date));
            } else {
                $Date = date("Y-m-d");
            }
            $updateFields['Date'] = $Date;
            if($HasMultiTagLevels==1 && !empty($FSTagLevelId)){
                $updateFields['FSTagLevelId'] = $FSTagLevelId;                
            }
            $result = Core_Database::insert('fstag_techs',$updateFields);            
        }
        else
        {
            $criteria = "FSTagId = '$fstagID' AND TechId='$techID'";
            $updateFields = array();
            if(!empty($Number)) $updateFields['Number'] = $Number;
            if(!empty($DateExpired)) {
                
                $updateFields['DateExpired'] = $DateExpired;   
            }
            if(!empty($Date)){
                $Date = date("Y-m-d",strtotime($Date));
                $updateFields['Date'] = $Date;
            } 
            if($HasMultiTagLevels==1 && !empty($FSTagLevelId)){
                $updateFields['FSTagLevelId'] = $FSTagLevelId;                
            }            
            if(count($updateFields) > 0)
            {
                $result = Core_Database::update('fstag_techs',$updateFields,$criteria);
            }                     
        }
        //--- certifications
        if($IsExistingTag==1 && $source=='certifications')
        {
            $certInfo = $this->getCertInfoByName($tagName);
            if(!empty($certInfo))
            {
                $certID = $certInfo['id'];
                $techCertInfo = $this->getTechCertInfo($techID,$certID);
                if(empty($techCertInfo))
                {
                    $updateFields = array();
                    $updateFields['certification_id'] = $certID;
                    $updateFields['TechID'] = $techID;
                    $updateFields['number'] = $Number;
                    $updateFields['dateExpired'] = $DateExpired;
                    $updateFields['date'] = $Date;
                    $result = Core_Database::insert('tech_certification',$updateFields);
                }
                else
                {
                    $criteria = "TechID = '$techID' AND certification_id='$certID'";
                    $updateFields = array();
                    if(!empty($Number)) $updateFields['number'] = $Number;
                    if(!empty($DateExpired)) $updateFields['dateExpired'] = $DateExpired;
                    if(!empty($Date)) $updateFields['date'] = $Date;
                    if(count($updateFields) > 0)
                        $result = Core_Database::update('tech_certification',$updateFields,$criteria);
                }
            }
        }
     }
     
     public function getCertInfoByName($name)
     {
         $db = Core_Database::getInstance();
         $select = $db->select();
         $select->from('certifications');
         $select->where("name = '$name'");
         $records = Core_Database::fetchAll($select);
         if(empty($records) || count($records)==0)     
         {
             return null;
         }
         else 
         {
             return $records[0];
         }
     }
     
     public function getTechCertInfo($techID,$certID)
     {
         $db = Core_Database::getInstance();
         $select = $db->select();
         $select->from('tech_certification');
         $select->where("TechID = '$techID'");
         $select->where("certification_id = '$certID'");
         $records = Core_Database::fetchAll($select);
         if(empty($records) || count($records)==0)     
         {
             return null;
         }
         else 
         {
             return $records[0];
         }
     }
     /*
     *  $fstagSearchInfoArray: array(
     *      [0]=>array('fstag_id'=>2,'yes_no'=>'','greater_date'=>'2013-01-30','less_date'=>'2013-01-31' ))
     */
     //--- 13624
     public function getTechIdsArray_forFSTagSearch($fstagSearchInfoArray)
     {         
         if(empty($fstagSearchInfoArray) || count($fstagSearchInfoArray)==0) return null;
         
         if(count($fstagSearchInfoArray) > 0)
         {
             foreach($fstagSearchInfoArray as $fstagSearchInfo)
             {
                $fstagID =  $fstagSearchInfo['fstag_id'];
                $this->insert_Tofstagstechs_FromExistingTagData($fstagID);
             }
         }
         $TechIDArray = array();
         foreach($fstagSearchInfoArray as $fstagSearchInfo)
         {
             $fstagID =  $fstagSearchInfo['fstag_id'];
             $yes_no =  $fstagSearchInfo['yes_no'];
             $greateDate = $fstagSearchInfo['greater_date'];
             $lessDate = $fstagSearchInfo['less_date'];

             if($yes_no=="1")
             {
                 $db = Core_Database::getInstance();  
                 $select = $db->select();
                 $select->from('fstag_techs',array('FSTagId','TechId'));
                 $select->where("FSTagId = '$fstagID'");
                 if(!empty($greateDate)){
                     $select->where("Date >= '$greateDate'");
                 }
                 if(!empty($lessDate)){
                     $select->where("Date <= '$lessDate'");
                 }                 
                 $records = Core_Database::fetchAll($select);
                 $TechIDArray_forTag = array();
                 if(!empty($records) || count($records)>0)
                 {
                     foreach($records as $rec) {
                        $TechIDArray_forTag[] = $rec['TechId'];
                     }
                     if(count($TechIDArray) > 0) {
                        $TechIDArray = array_intersect($TechIDArray,$TechIDArray_forTag);        
                     } else {
                        $TechIDArray = $TechIDArray_forTag;
                     }
                 }                 
             }  else if($yes_no=="0") {
                 $db = Core_Database::getInstance();  
                 $select = $db->select();
                 $select->from('fstag_techs',array('FSTagId','TechId'));
                 $select->where("FSTagId = '$fstagID'");
                 $records = Core_Database::fetchAll($select);
                 $TechIDArray_forTag = array();
                 if(!empty($records) || count($records)>0)
                 {
                     foreach($records as $rec)
                     {
                        $TechIDArray_forTag[] = $rec['TechId'];
                     }
                 }
                 //---
                 $db = Core_Database::getInstance();  
                 $select = $db->select();
                 $select->from('TechBankInfo',array('TechID'));
                 if(count($TechIDArray_forTag)>0)
                 {
                    $select->where("TechID NOT IN (?)",$TechIDArray_forTag);
                 }
                 $records = Core_Database::fetchAll($select);
                 $TechIDArray_notInTag = array();
                 if(!empty($records) || count($records)>0)
                 {
                     foreach($records as $rec)
                     {
                        $TechIDArray_notInTag[] = $rec['TechID'];
                     }
                     
                     if(count($TechIDArray) > 0) {
                        $TechIDArray = array_intersect($TechIDArray,$TechIDArray_notInTag);        
                     } else {
                        $TechIDArray = $TechIDArray_notInTag;
                     }                     
                 }
             } else if(strpos($yes_no,"LevelID_")!==false){
                 $part = explode('_',$yes_no);
                 $levelID = $part[1];
                 
                 $db = Core_Database::getInstance();  
                 $select = $db->select();
                 $select->from('fstag_techs',array('FSTagId','TechId'));
                 $select->where("FSTagId = '$fstagID'");
                 $select->where("FSTagLevelId = '$levelID'");
                 if(!empty($greateDate)){
                     $select->where("Date >= '$greateDate'");
                 }
                 if(!empty($lessDate)){
                     $select->where("Date <= '$lessDate'");
                 }                 
                 $records = Core_Database::fetchAll($select);
                 $TechIDArray_forTag = array();
                 if(!empty($records) || count($records)>0)
                 {
                     foreach($records as $rec) {
                        $TechIDArray_forTag[] = $rec['TechId'];
                     }
                     if(count($TechIDArray) > 0) {
                        $TechIDArray = array_intersect($TechIDArray,$TechIDArray_forTag);        
                     } else {
                        $TechIDArray = $TechIDArray_forTag;
                     }
                 }                 
                 
             }
         }         
         return $TechIDArray;
     }
     //--- 13624, 13851
     public function Update_AdjustableBedExpert($techID)
     {
        if(empty($techID)) {return; }
         
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from(array("tbi"=>Core_Database::TABLE_TECH_BANK_INFO),
            array("Bg_Test_Pass_Lite","Bg_Test_Pass_Full")
        );
        $select->joinLeft(array("te"=>"tech_certification"), 
                "tbi.TechID = te.TechID", 
                array("certification_id","date")
        );
        $select->where("tbi.TechID = '$techID'");  
        $select->where("tbi.Bg_Test_Pass_Lite = 'Pass' OR tbi.Bg_Test_Pass_Full = 'Pass'");
        $select->where("te.certification_id = ".self::CertID_ErgoMotionKeyContractor);                        
        $records = Core_Database::fetchAll($select);
        
        $CertID_BedExpert = self::CertID_ErgoMotionCertifiedTechnician;
        $FSTagID_BedExpert = self::FSTagID_ErgoMotionCertifiedTechnician;
        if(empty($records) || count($records)==0) {
            
            $criteria = "certification_id = '$CertID_BedExpert' AND TechID='$techID'";
            $result = $db->delete('tech_certification',$criteria);
            
            $criteria_1 = "FSTagId = '$FSTagID_BedExpert' AND TechId='$techID'";
            $result = $db->delete('fstag_techs',$criteria_1);                    
        } else {
            $rec = $records[0];
            $date = $rec['date'];
            $result =  $this->saveFSTagForTech($techID,$FSTagID_BedExpert,'','',$date);
            //13851
            $techCertInfo = $this->getTechCertInfo($techID,$CertID_BedExpert);
            if(empty($techCertInfo)) {
                $fields = array('certification_id'=>$CertID_BedExpert,'TechID'=>$techID,'date'=>$date); 
                $result = Core_Database::insert('tech_certification',$fields);
            } else {
                $criteria = "certification_id = '$CertID_BedExpert' AND TechID='$techID'";
                $fields = array('date'=>$date);
                $result = Core_Database::update('tech_certification',$fields,$criteria);
            }
        }
        return $result;
     }
     
     //---- 13874,13735
     /*
     $techID:
     $companyID: 
     $filters:
     - key - HideFromTechs: value = 0, 1
     - key - VisibilityId:  value = 1: Client Only, 2: Public (all clients)           if VisibilityId=1, the tags of $companyID will be returned. 
     - key - CompanyId:     We need this filter when VisibilityId=1
     */     
     public function getFSTagList_forTech_withFilters($techID,$filters)
     {
         //--- all fs-tags
         $db = Core_Database::getInstance();  
         $select = $db->select();
         $select->from('fstags');
         $select->where("Active = 1");
         $select->order("Title ASC");
         $allTags = Core_Database::fetchAll($select);    
         if(empty($allTags) || count($allTags)==0) {  return null;   }
         //--- techInfo
         $techInfo = Core_Tech::getProfile($techID);         
         //---
         foreach($allTags as $tag)
         {
             $isExistingTag = $tag['IsExistingTag'];
             $source = $tag['Source'];
             $tagName = $tag['Name'];
             $fsTagID = $tag['id'];
             //--- get data of this tech from tech_certificates and insert into fstags_techs
             if($isExistingTag==1 && $source=='certifications')
             {
                 $db = Core_Database::getInstance();  
                 $select = $db->select();
                 $select->from(array('tc'=>'tech_certification'),
                        array('TechID','certification_id','number','dateExpired','date')
                 );
                 $select->join(array('c'=>'certifications'),
                            'c.id=tc.certification_id',
                            array('name')
                 );
                 $select->where("c.name = '$tagName'");
                 $select->where("tc.TechID = '$techID'");             
                 $records = Core_Database::fetchAll($select);  
                 
                 if(!empty($records) && count($records)>0)
                 {
                   //--- delete
                   $criteria = "FSTagId = '$fsTagID' AND TechId='$techID'";
                   $db = Core_Database::getInstance();
                   $result = $db->delete('fstag_techs',$criteria);
                   //--- insert  
                    foreach($records as $rec)
                    {                
                        $updateFields = array();
                        $updateFields['FSTagId'] = $fsTagID;
                        $updateFields['TechId'] = $rec['TechID'];
                        $updateFields['Number'] = $rec['number'];
                        $updateFields['DateExpired'] = $rec['dateExpired'];
                        $updateFields['Date'] = $rec['date'];
                        $result = Core_Database::insert('fstag_techs',$updateFields);
                    }
                 }
             }
             
             //--- get data of this tech from TechBankInfo and insert into fstags_techs
             if($isExistingTag==1 && $source=='TechBankInfo' && 
                    $tagName=='Dell_MRA_Compliant')
             {
                $bgCheckFullStatus = $techInfo['credentialInfo']['bgCheckFullStatus'];
                $drugTestStatus = $techInfo['credentialInfo']['drugTestStatus'];
                if($bgCheckFullStatus==1 && $drugTestStatus==1)
                {
                    $bgDate = $techInfo['credentialInfo']['bgCheckFullDate'];//Bg_Test_ResultsDate_Full
                    $dtDate = $techInfo['credentialInfo']['drugTestDate'];//DatePassDrug
                    ///dellMRACompliantDate = bgDate > dtDate ? bgDate : dtDate;
                    $dellMRACompliantDate="";
                    if(empty($bgDate) && empty($dtDate)){
                        $dellMRACompliantDate="";    
                    } else if(empty($bgDate) && !empty($dtDate)){
                        $dellMRACompliantDate= date("Y-m-d",strtotime($dtDate)) ;    
                    } else if(!empty($bgDate) && empty($dtDate)){
                        $dellMRACompliantDate= date("Y-m-d",strtotime($bgDate)) ;    
                    } else if(!empty($bgDate) && !empty($dtDate)){
                        $tsbgDate = strtotime($bgDate);
                        $tsdtDate = strtotime($dtDate);
                        if($tsbgDate > $tsdtDate){
                            $dellMRACompliantDate= date("Y-m-d",strtotime($bgDate)) ;    
                        } else {
                            $dellMRACompliantDate= date("Y-m-d",strtotime($dtDate)) ;    
                        }
                    }
                    //--- if exist->update, else insert
                    $db = Core_Database::getInstance();  
                    $select = $db->select();
                    $select->from('fstag_techs');
                    $select->where("TechId = '$techID'");
                    $select->where("FSTagId = '$fsTagID'");
                    $records = Core_Database::fetchAll($select);    
                    if(!empty($records) && count($records)>0)
                    {
                        $criteria = "TechId = '$techID' AND FSTagId = '$fsTagID'";
                        $updateFields=array();
                        $updateFields['Date']=$dellMRACompliantDate;
                        $result = Core_Database::update('fstag_techs',$updateFields,$criteria);       
                    }else{
                        $updateFields = array();
                        $updateFields['FSTagId'] = $fsTagID;
                        $updateFields['TechId'] = $techID;
                        $updateFields['Date'] = $dellMRACompliantDate;
                        $result = Core_Database::insert('fstag_techs',$updateFields);
                    }
                }   
                else
                {
                   //--- delete
                   $criteria = "FSTagId = '$fsTagID' AND TechId='$techID'";
                   $db = Core_Database::getInstance();
                   $result = $db->delete('fstag_techs',$criteria);                    
                }      
             }

                          
         }//end of for

         //--- get fstags for tech, 13728
         $db = Core_Database::getInstance();  
         $select = $db->select();
         //13912
         $select->from(array('tag'=>'fstags'),
             array('FSTagId'=>'id','Title','VisibilityId','Name','Active','ExistingImagePath','IsExistingTag','TagArt','HideFromTechs','CanNonGPMEdit','CanGPMEdit','CanTechEdit')
         );
         
         $select->joinLeft(array('tagte'=>'fstag_techs'),
                "tag.id = tagte.FSTagId AND tagte.TechId='$techID'",
                array('TechId','Number','DateExpired','Date')
         );
         
         $select->where("tag.Active = 1");
         
         if(isset($filters['HideFromTechs'])){
            $select->where("tag.HideFromTechs = 2 OR tag.HideFromTechs = ?",$filters['HideFromTechs']);
         }
         //end 13912
         //13735
         if(!empty($filters['VisibilityId'])){
             $visibilityID = $filters['VisibilityId'];
             $select->where("tag.VisibilityId = '$visibilityID'"); 
         } 
         //--- list tagID for company
         $tagIDArray_forCompany = array();
         if(!empty($filters['CompanyId'])){
             $companyID = $filters['CompanyId'];
             $db = Core_Database::getInstance();  
             $select2 = $db->select();
             $select2->from('fstag_clients',array('FSTagId'));
             $select2->where("CompanyId = '$companyID'");
             $records = Core_Database::fetchAll($select2);    
             $tagIDArray_forCompany = array();
             if(!empty($records) && count($records) > 0)
             {
                 foreach($records as $rec)
                 {
                     $tagIDArray_forCompany[] = $rec['FSTagId'];
                 }
             }
         }
         //end 13735
         
         $select->order("tag.Title ASC");
         $tags_forTech = Core_Database::fetchAll($select);    
         if(empty($tags_forTech) || count($tags_forTech)==0){
             return null;
         }
         
         //--- 13624
         $tagArray_forTech = array();
         foreach($tags_forTech as $tagfortech)
         {
             $tagID = $tagfortech['FSTagId'];
             $tagName = $tagfortech['Name'];
             $visibilityID = $tagfortech['VisibilityId'];
             $tagArray_forTech[$tagName]=$tagfortech;             
         }
         
         return $tagArray_forTech;         
     }//end of functions
     
}
