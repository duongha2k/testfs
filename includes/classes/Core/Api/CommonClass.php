<?php
/**
 * Core_Api_CommonClass
 * Class fro common functionality for all users
 * @author Alex Scherba
 */

ini_set('memory_limit', '328M');
define ("WO_STATE_CREATED", "created");
define ("WO_STATE_PUBLISHED", "published");
define ("WO_STATE_ASSIGNED", "assigned");
define ("WO_STATE_WORK_DONE", "work done");
define ("WO_STATE_INCOMPLETE", "incomplete");
define ("WO_STATE_APPROVED", "approved");
define ("WO_STATE_IN_ACCOUNTING", "in accounting");
define ("WO_STATE_COMPLETED", "completed");
define ("WO_STATE_DEACTIVATED", "deactivated");
define ("WO_STATE_ALL", "");

define ("WO_STATE_ALL_ACTIVE", "all active");
define ("WO_STATE_COMPLETED_TAB", "completed tab");

class Core_Api_CommonClass
{

	public function __construct() {
		Core_Caspio::init();
	}

    /**
	 * Function for getting list of countries
	 *
	 * @return API_Response
     * @author Alex Scherba
	 */
	public function getCountries()
	{
       	$logger = Core_Api_Logger::getInstance();
		$logger->setUrl('get countries');
        $logger->setLogin($user);
		$logger->addLog(LOGGER_ERROR_NO, null, 'db request');

        $errors = Core_Api_Error::getInstance();

        $cache = Core_Cache_Manager::factory();
        $cacheKey = Core_Cache_Manager::makeKey('countriesList');
        
        if ( false === ($countries = $cache->load($cacheKey))) {
            $fieldsList = array('Code','Name','Region');

            $db = Core_Database::getInstance();
            $select = $db->select();

            $select->from(Core_Database::TABLE_COUNTRIES, $fieldsList);
            $select->where("Disable = 0");
            $select->order("(CASE Region WHEN 'North America' THEN 1 WHEN 'Central / South America' THEN 2 WHEN 'EMEA' THEN 3 WHEN 'APAC' THEN 4 ELSE 5 END) ASC");
            $select->order('DisplayPriority ASC');
            $select->order('Name ASC');
            //print_r($select->__toString());    
            $result = Core_Database::fetchAll($select);
            
            $result0 = array();
            $region = "North America";
            for($i=0;$i<count($result);$i++){
                $rec = $result[$i];
                if($rec['Region']==$region){
                    $result0[]=$rec;
                } else {
                    $region = $rec['Region'];
                    $name = '---'.$region;
                    if($region=='Central / South America'){
                        $name = '---South America';
                    }
                    $result0[]=array('Code'=>'','Name'=>$name,'Region'=>$region);
                    $result0[]=$rec;
                }
            }
            
            $err = $errors->getErrors();
            if (!empty($err))
                return API_Response::fail();
            $countries = $this->makeCountryArray($result0);

            $cache->save($countries, $cacheKey, array('countriesList'), LIFETIME_1DAY);
        }
		
        //print_r($countries);
		
		return API_Response::success($countries);
	}

    /**
     * Function for getting list of states
     *
     * @return API_Response
     */
    public function getStates()
    {
        $logger = Core_Api_Logger::getInstance();
		$logger->setUrl('get states');
        $logger->setLogin($user);
		$logger->addLog(LOGGER_ERROR_NO, null, 'db request');

        $errors = Core_Api_Error::getInstance();

        $cache = Core_Cache_Manager::factory();
        $cacheKey = Core_Cache_Manager::makeKey('statesList');

        if ( false === ($states = $cache->load($cacheKey)) ) {
            $db = Core_Database::getInstance();
            $select = $db->select();

            $select->from(Core_Database::TABLE_STATES_LIST)
                ->order('State ASC');

            $result = Core_Database::fetchAll($select);
            
            $err = $errors->getErrors();
            if (!empty($err))
                return API_Response::fail();
            $states = $this->makeStatesArray($result);

            $cache->save($states, $cacheKey, array('statesList'), LIFETIME_1DAY);
        }
		return API_Response::success($states);
    }
    
    
    public function getISOsArray()
    {
        $errors = Core_Api_Error::getInstance();

        $cache = Core_Cache_Manager::factory();
        $cacheKey = Core_Cache_Manager::makeKey('isosList');

//        if ( false === ($isos = $cache->load($cacheKey)) ) {
            $db = Core_Database::getInstance();
            $select = $db->select();

            $select->from(Core_Database::TABLE_ISO, array('UNID', 'Company_Name'))
                ->order('Company_Name ASC');

            $result = $db->fetchPairs($select);
            $err = $errors->getErrors();
            if (!empty($err))
                return API_Response::fail();
            $isos = $result;

            $cache->save($states, $cacheKey, array('isosList'), LIFETIME_1DAY);
//        }
		
        return API_Response::success(
			$isos
        );
    }

	/**
     * getStates
     *
	 * @param string $country
     * @access public
     * @return API_Response
     */
    public function getStatesArray($country = 'US')
    {
        return API_Response::success(
			Core_State::getByCode($country)
        );
    }

    
    /**
     * getStates
     *
     * @access public
     * @return API_Response
     */
    public function getCanadianStatesArray()
    {
		$states = Core_State::getByCode('CA');
        return API_Response::success($states);
    }
    
    public function getMexicoStateArray()
    {
		$states = Core_State::getByCode('MX');
        return API_Response::success($states);
    }

/* PRIVATE SECTION */

    private function makeCountryArray($result)
	{
		$countries = array();
	    if (!empty($result[0])) {
			foreach ($result as $key=>$value) $countries[] = API_Country::countryWithResult($value);
	    }
		return $countries;
	}
    private function makeStatesArray($result)
	{
		$states = array();
	    if (!empty($result[0])) {
			foreach ($result as $key=>$value) $states[] = API_State::stateWithResult($value);
	    }
		return $states;
	}

	/**
    *  getClientServiceDirectors
    * @return $result  
    */
    public function getClientServiceDirectors()
    {
        $db = Core_Database::getInstance();
        $select = $db->select();
        $select->from(Core_Database::TABLE_OPERATIONS_STAFF)
                ->where('groupid=1');
        $result = Core_Database::fetchAll($select);
        return $result;
    }

    //14145
    
     public function getTechSourceArray()
    {
        $result0 = array();
        $result= array("AnyoneHiring.com","Beyond.com","Craigslist.com","Dice.com","GetHired.com","Indeed.com","simplyHire.com","Search Engine","Contacted by FieldSolutions","Referral-Client","Referral-Technician","Likedln","Facebook","Twitter","Internet Forum","College Job Boards","Local Employment Agency","Misc Job Search site","ISO","Other");                                                             
            for($i=0;$i<count($result);$i++)
            {
                $rec = $result[$i];
               // return $result[$i];
               $result0[]=$rec; 
            }   
            return $result0;
    }
    //14145
    /**
    *  getClientServiceDirectors
    * @return $result  
    */
    public function getAccountManagers()
    {
        $db = Core_Database::getInstance();
        $select = $db->select();
        $select->from(Core_Database::TABLE_OPERATIONS_STAFF)
                ->where('groupid=2');
        $result = Core_Database::fetchAll($select);
        return $result;
    }
    
    public function getOpeStaffNameById($id)
    {
        $db = Core_Database::getInstance();
        $select = $db->select();
        $select->from(Core_Database::TABLE_OPERATIONS_STAFF)
                ->where('id=?',$id);
        $result = Core_Database::fetchAll($select);
        $name="";
        if(!empty($result)) {$name=$result[0]["name"];}
        return $name;    
    }
   
    public function getOpeStaffById($id)
    {
        $db = Core_Database::getInstance();
        $select = $db->select();
        $select->from(Core_Database::TABLE_OPERATIONS_STAFF)
                ->where('id=?',$id);
        $result = Core_Database::fetchAll($select);
        if(!empty($result)) {return $result[0];}
        return null;    
    }
    /**
    * getAllClientAccessAttributes
    * @return $result   
    */
    public function getAllClientAccessAttributes()
    {
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from("cliaccess_atribs");
        $select->order("presentation_order ASC");
        $result = Core_Database::fetchAll($select);
        return $result;
    }  
    //13329
    public function getCertLabel($certId)
    {
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from("certifications");
        $select->where("id='$certId'");
        $result = Core_Database::fetchAll($select);
        if(empty($result)) return "";
        else return $result[0]['label'];
    }
    //13334A
    public function getCountryCode($country)
    {
        // find code
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from(Core_Database::TABLE_COUNTRIES);
        $select->where("Code='$country'");
        $result = Core_Database::fetchAll($select);
    
        if(!empty($result)) return $country;
        //find Name
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from(Core_Database::TABLE_COUNTRIES);
        $select->where("Name='$country'");
        $result = Core_Database::fetchAll($select);
        
        if(!empty($result)) return $result[0]['Code'];
        
        return "US";
    }

    public function getDeducts()
    {
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from("deducts");
		$select->order("DeductPcnt ASC");
        $records = Core_Database::fetchAll($select);
        return $records;        
    }
    
    public function getTimeSince($EndDate,$EndTime)
    {
        $value=0;
        if(!empty($EndDate))
        {
            try
            {
                $timeEnDate=strtotime($EndDate);
                $timeCurrentDate=  time();
                $value=ceil(($timeCurrentDate-$timeEnDate)/86400);        
}
            catch(Exception $ex)
            {
                $value=0;
            }
        }
        return $value;
        
    }
    
    public function isStringHTML($strTextCont)
    {
        if(strlen($strTextCont) != strlen(strip_tags($strTextCont)))
            return true; 
        else
            return false;                
	}

    public function ZipcodeExists($zipcode)
    {
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from("zipcodes");
        $select->where("ZIPCode='$zipcode'");
        $records = Core_Database::fetchAll($select);
        if(empty($records)) return 0;
        else return 1;
    }

    public static function WOLeadTime($WIN_NUM,$StartDate,$ShowTechs,$DateEntered,$Status, $UseCurrentDate = false){
        $win = $WIN_NUM;
        if (!empty($Tech_ID))
        {
            $lastAssignedDate = API_Timestamp::getLastWinAssignedTimestamp($win);

        }
        $publishedDate = "";
        if ($ShowTechs == 1)
        {
			if ($UseCurrentDate) {
				$publishedDate = date ("Y-m-d H:i:s");
			}
			else {
				$publishedDate = $DateEntered;
			}
            //$publishedime = $lastAssignedDate;
        } else if ($ShowTechs != 1 && strtolower($Status) == "assigned")
        {
            $publishedDate = API_Timestamp::getLastWinAssignedTimestamp($win);
            //$publishedime = $publishedDate;
        }
        $Timestampitem = $publishedDate;
        $LeadTime = "";
        if (!empty($Timestampitem) && !empty($StartDate)) {
            $publishdate = new DateTime($Timestampitem);
            $_publishdate = date_format($publishdate, 'm/d/Y');
            $publishdate = new DateTime($_publishdate);
            $startdate = new DateTime($StartDate);
            if ($startdate == $publishdate) {
                $LeadTime = "Same Business Day";
            } else {
                $arrayDate = explode('/', $_publishdate);
                $days = (int) $arrayDate[1];
                $month = (int) $arrayDate[0];
                $tomorrow = date('m/d/Y', mktime(0, 0, 0, $month, $days + 1, $arrayDate[2]));
                $tomorrow = new DateTime($tomorrow);
                if ($tomorrow == $startdate) {
                    $LeadTime = "Next Business Day";
                } else {
                    $LeadTime = "Scheduled";
                }
            }
        }
        return $LeadTime;
    }
    public static function WODatePart($StartDate,$StartTime){
        $DatePart = "";
        if (!empty($StartTime) && !empty($StartDate)) {
            $startdate = new DateTime($StartDate . " " . $StartTime);
            $time4am = new DateTime($StartDate . " 4:00:00 AM");
            $time5pm = new DateTime($StartDate . " 5:00:00 PM");
            $time501pm = new DateTime($StartDate . " 5:01:00 PM");
            $time8pm = new DateTime($StartDate . " 8:00:00 PM");
            $time801pm = new DateTime($StartDate . " 8:01:00 PM");
            $arrayDate = explode('-', $StartDate);
            $_timeTowmorrow559am = date('m/d/Y H:m:s', mktime(5, 59, 0, $arrayDate[0], (int) $arrayDate[2] + 1, $arrayDate[1]));
            $time559am = new DateTime($_timeTowmorrow559am);
            $dayinweek = date('w', strtotime($StartDate . " " . $StartTime));
            if ($dayinweek == 6 || $dayinweek == 0) {
                $DatePart = "Weekend";
            } else if ($dayinweek == 5) {
                if ($startdate >= $time8pm) {
                    $DatePart = "Weekend";
                }else if ($startdate >= $time4am && $startdate <= $time5pm) {
                    $DatePart = "Day";
                } else if ($startdate >= $time501pm && $startdate <= $time8pm) {
                    $DatePart = "Evening";
                } else { //if ($startdate >= $time801pm && $startdate <= $time559am) {
                    $DatePart = "Night";
                }
            } else if ($dayinweek == 1) {
                if ($startdate <= $time559am) {
                    $DatePart = "Weekend";
                }else if ($startdate >= $time4am && $startdate <= $time5pm) {
                    $DatePart = "Day";
                } else if ($startdate >= $time501pm && $startdate <= $time8pm) {
                    $DatePart = "Evening";
                } else { //if ($startdate >= $time801pm && $startdate <= $time559am) {
                    $DatePart = "Night";
                }
            } else if ($startdate >= $time4am && $startdate <= $time5pm) {
                $DatePart = "Day";
            } else if ($startdate >= $time501pm && $startdate <= $time8pm) {
                $DatePart = "Evening";
            } else { //if ($startdate >= $time801pm && $startdate <= $time559am) {
                $DatePart = "Night";
            }
        }
        return $DatePart;
    }    
    public static function getSiteStatus($isGPM, $win ,$siteNumber, $project_id){
        $ProjectClass = new Core_Api_ProjectClass();
        $siteInfo = $ProjectClass->getSiteStatus_BySiteOfProject($siteNumber,$project_id);
        $projectInfo = $ProjectClass->getProject($project_id);

        if(empty($siteInfo) || empty($siteInfo["Status"])){
            $siteInfo["Status"] = "Scheduled";
}
        if($isGPM && $projectInfo["EnableSiteStatusReporting"] && !empty($siteNumber)){
            return "(<a win='$win' href='javascript:;' style='text-decoration: underline;' class='openSiteStatus'>Site ".$siteInfo["Status"]."</a>)";
        }
        return "";
    }
    public static function getSiteStatusHtml($isGPM, $win ,$siteNumber, $project_id){
        if(!empty($siteNumber)){
            return "Site#: $siteNumber ".Core_Api_CommonClass::getSiteStatus($isGPM, $win ,$siteNumber, $project_id);
        }
        return "";
    }
    
    public static function displayDate($format, $strDate)
    {
        if (!empty($strDate) ) { 
            $stamp = strtotime($strDate);
            if ( $stamp && $stamp !== -1 )
                return (date($format, $stamp));
        }
        return '';
    }
	
	public function getWoCategories() {
        //13735
		$fieldsList = array('Category_ID','Category','TechSelfRatingColumn','Abbr');
		$db = Core_Database::getInstance();
		$select = $db->select();

		$select->from(array('c'=>Core_Database::TABLE_WO_CATEGORIES), $fieldsList);
        $select->join(array('e'=>'fsexperts'),
            'e.Code = c.Abbr', 
            array('FSExpertEnable','Abbreviation')
        );
        //end 13735
	$select->where("c.Category_ID !='11'");//13102	
        $select->order('Category');

		$result = Core_Database::fetchAll($select);
		return $result;
	}
    
    //14041
    public function getCountries_1()
    {
        $fieldsList = array('id','Code','Name');
        $db = Core_Database::getInstance();
        $select = $db->select();
        $select->from(Core_Database::TABLE_COUNTRIES, $fieldsList);
        $select->where("Disable = 0");
        $result = Core_Database::fetchAll($select);
        return $result;
    }
}