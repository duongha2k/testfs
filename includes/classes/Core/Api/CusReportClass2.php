<?php

class Core_Api_CusReportClass2 {

    protected $TimeOfDayList = array("12:00 AM", "12:30 AM", "1:00 AM", "1:30 AM", "2:00 AM", "2:30 AM", "3:00 AM", "3:30 AM", "4:00 AM", "4:30 AM", "5:00 AM", "5:30 AM", "6:00 AM", "6:30 AM", "7:00 AM", "7:30 AM", "8:00 AM", "8:30 AM", "9:00 AM", "9:30 AM", "10:00 AM", "10:30 AM", "11:00 AM", "11:30 AM", "12:00 PM", "12:30 PM", "1:00 PM", "1:30 PM", "2:00 PM", "2:30 PM", "3:00 PM", "3:30 PM", "4:00 PM", "4:30 PM", "5:00 PM", "5:30 PM", "6:00 PM", "6:30 PM", "7:00 PM", "7:30 PM", "8:00 PM", "8:30 PM", "9:00 PM", "9:30 PM", "10:00 PM", "10:30 PM", "11:00 PM", "11:30 PM");
    protected $TimezoneList = array("-10" => 10, "-9" => 8, "-8" => 7, "-7" => 6, "-6" => 5, "-5" => 4);
    protected $LocalTimeZone = "-6";

    public function getReportListForAutoEmail($date = null, $time = null)
    {
        if (empty($time))
        {
            $currentTime = date("g:i A");
        } else
        {
            $currentTime = $time;
        }

        if (empty($date))
        {
            $currentDayOfWeek = date("N");
            $currentDayOfMonth = date("j");
            $currentTimestamp = strtotime(date("Y-m-d h:i:s A"));
            $numDayOfMonth = date("t");
        } else
        {
            $currentDayOfWeek = date("N", $date);
            $currentDayOfMonth = date("j", $date);
            $currentTimestamp = strtotime(date("Y-m-d h:i:s A", $date));
            $numDayOfMonth = date("t", $date);
        }

        $dayOrderInMonth = $this->getDayOrderInMonth($currentTimestamp);
//print_r("currentTime<pre>");
//        print_r($currentTime);
//        print_r("</pre>");die;
        if (in_array($currentTime, $this->TimeOfDayList))
        {

            $criteriaForTimeOfDay = "";
            foreach ($this->TimezoneList as $key => $val)
            {

                $currentTimeForTimezone = $this->getCurrentTimeForTimezone($key, $currentTime); //
                if ($currentTimeForTimezone != '')
                {
                    if (!empty($criteriaForTimeOfDay))
                    {
                        $criteriaForTimeOfDay .= " OR ";
                    }
                    $criteriaForTimeOfDay .= " (RRDTimeOfDay = '$currentTimeForTimezone' AND RRDTimeZone='$key') ";
                }
            }
//            $criteriaForTimeOfDay .= " OR RRDTimeOfDay = '$currentTime' ";
            $db = Core_Database::getInstance();
            $select = $db->select();
            $select->from("custom_reports");
            $select->where("RRDEnable = '1'");
            $select->where($criteriaForTimeOfDay);

            $scheduleCriteria = "(RRDEmailSchedule = 'Daily' AND RRDDailyOptions=1 AND DATEDIFF(CURDATE(),IFNULL(last_autosend_date,last_run_date)) >= RRDDailyEveryDaySpaceOutNum)";
            $scheduleCriteria .= " OR (RRDEmailSchedule = 'Daily' AND RRDDailyOptions=2 AND DAYOFWEEK(CURDATE()) >1 AND DAYOFWEEK(CURDATE()) <7 )";
            $scheduleCriteria .= " OR (RRDEmailSchedule = 'Weekly' AND INSTR(RRDWeeklyDays, '$currentDayOfWeek') > 0 AND DATEDIFF(CURDATE(),IFNULL(last_autosend_date,last_run_date)) >= RRDWeeklySpaceOutNum*7)";

            //RRDMonthlyScheduleOptions = 1, RRDMonthlyScheduleDayNum 
            if ($currentDayOfMonth == $numDayOfMonth)
            {
                $scheduleCriteria .= " OR (RRDEmailSchedule = 'Monthly' AND RRDMonthlyScheduleOptions=1 AND RRDMonthlyScheduleDayNum='Last Day' AND DATEDIFF(CURDATE(),IFNULL(last_autosend_date,last_run_date)) >= RRDMonthlyDayNumSpaceOutNum*30)";
            } else if ($currentDayOfMonth < $numDayOfMonth)
            {
                $scheduleCriteria .= " OR (RRDEmailSchedule = 'Monthly' AND RRDMonthlyScheduleOptions=1 AND RRDMonthlyScheduleDayNum='$currentDayOfMonth' AND DATEDIFF(CURDATE(),IFNULL(last_autosend_date,last_run_date)) >= RRDMonthlyDayNumSpaceOutNum*30)";
            }
            $scheduleCriteria .= " OR (RRDEmailSchedule = 'Monthly' AND RRDMonthlyScheduleOptions=2 AND RRDMonthlyScheduleDayOrder = '$dayOrderInMonth' AND INSTR(RRDMonthlyScheduleDayOrderDay, '$currentDayOfWeek') > 0 AND DATEDIFF(CURDATE(),IFNULL(last_autosend_date,last_run_date)) >= RRDMonthlyDayOrderSpaceOutNum*30)";

            $select->where($scheduleCriteria);
//            print_r("select<pre>");
//            print_r($select->__toString());
//            print_r("</pre>");
//            die;
            //print_r($select->__toString());//test            
            $records = Core_Database::fetchAll($select);
            if (empty($time) && empty($date))
            {
                $result = $this->updateReportAutoSend($records);
            }
            return $records;
        } else
        {

            return null;
        }
    }

    public function getReportListForAutoEmailtest($date, $time)
    {
        $currentTime = date("g:i A");
        $currentDayOfWeek = date("N", $date);
        $currentDayOfMonth = date("j");
        $currentTimestamp = strtotime(date("Y-m-d h:i:s A"));
        $numDayOfMonth = date("t");
        $dayOrderInMonth = $this->getDayOrderInMonth($currentTimestamp);
        $currentTime = "12:30 AM";

        if (in_array($currentTime, $this->TimeOfDayList))
        {

            $criteriaForTimeOfDay = "";
            foreach ($this->TimezoneList as $key => $val)
            {

                $currentTimeForTimezone = $this->getCurrentTimeForTimezone($key, $currentTime); //
                if ($currentTimeForTimezone != '')
                {
                    if (!empty($criteriaForTimeOfDay))
                    {
                        $criteriaForTimeOfDay .= " OR ";
                    }
                    $criteriaForTimeOfDay .= " (RRDTimeOfDay = '$currentTimeForTimezone' AND RRDTimeZone='$key') ";
                }
            }
            $criteriaForTimeOfDay .= " RRDTimeOfDay = '$currentTime' ";
            $db = Core_Database::getInstance();
            $select = $db->select();
            $select->from("custom_reports");
            $select->where("RRDEnable = '1'");
            $select->where($criteriaForTimeOfDay);

            $scheduleCriteria = "(RRDEmailSchedule = 'Daily' AND RRDDailyOptions=1 AND DATEDIFF(CURDATE(),IFNULL(last_autosend_date,last_run_date)) >= RRDDailyEveryDaySpaceOutNum)";
            $scheduleCriteria .= " OR (RRDEmailSchedule = 'Daily' AND RRDDailyOptions=2 AND DAYOFWEEK(CURDATE()) >1 AND DAYOFWEEK(CURDATE()) <7 )";
            $scheduleCriteria .= " OR (RRDEmailSchedule = 'Weekly' AND INSTR(RRDWeeklyDays, '$currentDayOfWeek') > 0 AND DATEDIFF(CURDATE(),IFNULL(last_autosend_date,last_run_date)) >= RRDWeeklySpaceOutNum*7)";

            //RRDMonthlyScheduleOptions = 1, RRDMonthlyScheduleDayNum 
            if ($currentDayOfMonth == $numDayOfMonth)
            {
                $scheduleCriteria .= " OR (RRDEmailSchedule = 'Monthly' AND RRDMonthlyScheduleOptions=1 AND RRDMonthlyScheduleDayNum='Last Day' AND DATEDIFF(CURDATE(),IFNULL(last_autosend_date,last_run_date)) >= RRDMonthlyDayNumSpaceOutNum*30)";
            } else if ($currentDayOfMonth < $numDayOfMonth)
            {
                $scheduleCriteria .= " OR (RRDEmailSchedule = 'Monthly' AND RRDMonthlyScheduleOptions=1 AND RRDMonthlyScheduleDayNum='$currentDayOfMonth' AND DATEDIFF(CURDATE(),IFNULL(last_autosend_date,last_run_date)) >= RRDMonthlyDayNumSpaceOutNum*30)";
            }
            $scheduleCriteria .= " OR (RRDEmailSchedule = 'Monthly' AND RRDMonthlyScheduleOptions=2 AND RRDMonthlyScheduleDayOrder = '$dayOrderInMonth' AND INSTR(RRDMonthlyScheduleDayOrderDay, '$currentDayOfWeek') > 0 AND DATEDIFF(CURDATE(),IFNULL(last_autosend_date,last_run_date)) >= RRDMonthlyDayOrderSpaceOutNum*30)";

            $select->where($scheduleCriteria);
//            print_r("select<pre>");
//            print_r($select->__toString());
//            print_r("</pre>");
//            die;
            //print_r($select->__toString());//test            
            $records = Core_Database::fetchAll($select);
            $result = $this->updateReportAutoSend($records);
            return $records;
        } else
        {

            return null;
        }
    }

    public function getDayOrderInMonth($timeStamp)
    {
        $dayOfMonth = date("j", $timeStamp);
        $dayOrderInMonth = 0;
        if ($dayOfMonth <= 7)
        {
            $dayOrderInMonth = 1;
        } else if ($dayOfMonth <= 14)
        {
            $dayOrderInMonth = 2;
        } else if ($dayOfMonth <= 21)
        {
            $dayOrderInMonth = 3;
        } else if ($dayOfMonth <= 28)
        {
            $dayOrderInMonth = 4;
        } else
        {
            $dayOrderInMonth = 5;
        }
        return $dayOrderInMonth;
    }

    public function getCurrentTimeForTimezone($timezone, $currentTime)
    {
        $timezoneoffset = $this->TimezoneList[$this->LocalTimeZone] - $this->TimezoneList[$timezone];
        //strtotime($currentTime)
        return date("g:i A", strtotime($currentTime . " " . $timezoneoffset . " hours"));
    }

    public function getTimeForTimezoneStandardOffset($Time, $TimezoneStandardOffsett)
    {
        $return = array();
        $TimezoneListstandard = array("-10" => 10, "-9" => 8, "-8" => 7, "-7" => 6, "-6" => 5, "-5" => 4);
        foreach ($TimezoneListstandard as $key => $val)
        {
            $timezoneoffset = $TimezoneListstandard[$TimezoneStandardOffsett] - $TimezoneListstandard[$key] ;
            $return[$key] = date("g:i A", strtotime($Time . " " . $timezoneoffset . " hours"));
        };

        return $return;
    }

    //14100
    public function updateReportAutoSend($records)
    {
        $reportIds = '0';
        $RptIdArray = array();
        if (!empty($records) && count($records) > 0)
        {
            foreach ($records as $record)
            {
                $RptIdArray[] = $record["id"];
            }
            $reportIds = implode(',', $RptIdArray);
        }
        $updateSQL = "UPDATE custom_reports SET last_autosend_date = CURDATE() WHERE id IN ($reportIds)";
        $db = Core_Database::getInstance();
        $db->query($updateSQL);

        return true;
    }

    function getLocalTimezone()
    {
        $iTime = time();
        $arr = localtime($iTime);
        $arr[5] += 1900;
        $arr[4]++;
        $iTztime = gmmktime($arr[2], $arr[1], $arr[0], $arr[4], $arr[3], $arr[5], $arr[8]);
        $offset = doubleval(($iTztime - $iTime) / (60 * 60));
        $zonelist =
                array
                    (
                    'Kwajalein' => -12.00,
                    'Pacific/Midway' => -11.00,
                    'Pacific/Honolulu' => -10.00,
                    'America/Anchorage' => -9.00,
                    'America/Los_Angeles' => -8.00,
                    'America/Denver' => -7.00,
                    'America/Tegucigalpa' => -6.00,
                    'America/New_York' => -5.00,
                    'America/Caracas' => -4.30,
                    'America/Halifax' => -4.00,
                    'America/St_Johns' => -3.30,
                    'America/Argentina/Buenos_Aires' => -3.00,
                    'America/Sao_Paulo' => -3.00,
                    'Atlantic/South_Georgia' => -2.00,
                    'Atlantic/Azores' => -1.00,
                    'Europe/Dublin' => 0,
                    'Europe/Belgrade' => 1.00,
                    'Europe/Minsk' => 2.00,
                    'Asia/Kuwait' => 3.00,
                    'Asia/Tehran' => 3.30,
                    'Asia/Muscat' => 4.00,
                    'Asia/Yekaterinburg' => 5.00,
                    'Asia/Kolkata' => 5.30,
                    'Asia/Katmandu' => 5.45,
                    'Asia/Dhaka' => 6.00,
                    'Asia/Rangoon' => 6.30,
                    'Asia/Krasnoyarsk' => 7.00,
                    'Asia/Brunei' => 8.00,
                    'Asia/Seoul' => 9.00,
                    'Australia/Darwin' => 9.30,
                    'Australia/Canberra' => 10.00,
                    'Asia/Magadan' => 11.00,
                    'Pacific/Fiji' => 12.00,
                    'Pacific/Tongatapu' => 13.00
        );
        $index = array_keys($zonelist, $offset);
        if (sizeof($index) != 1)
        {
            $arrSVZone = array();
            return false;
        } else
        {
            $nameZone = $index[0];
            $dateTime = new DateTime();
            $dateTime->setTimeZone(new DateTimeZone($nameZone));
            $abbrZone = $dateTime->format('T');
            $timeZone = $zonelist[$nameZone];
            $arrSVZone = array('nameZone' => $nameZone, 'abbrZone' => $abbrZone, 'timeZone' => $timeZone);
        }
        return $arrSVZone;
    }

    function getOffsetTimeZone($tz)
    {
        $zonelist = array(
            'AST' => 3, //Asia/Riyadh
            'EST' => -5, //America/New_York
            'EDT' => -5, //America/New_York
            'CST' => -6, //America/Chicago
            'CDT' => -6, //America/Chicago
            'MST' => -7, //America/Denver
            'MDT' => -6, //America/Denver
            'PST' => -8, //America/Los_Angeles
            'PDT' => -8, //America/Los_Angeles
            'AKST' => -9, //
            'AKDT' => -9, //America/Juneau  | America/Anchorage
            'HAST' => -10, //Alaska/ Adak
            'HADT' => -10, //Adak
            'SST' => -11, //Pacific/Samoa
            'SDT' => -11, //Not sure
            'CHST' => 10, //Pacific/Guam
        );
        if (isset($zonelist[$tz]))
        {
            return $zonelist[$tz];
        } else
        {
            return '';
        }
    }

}
