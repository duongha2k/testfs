<?php

ini_set('memory_limit', '220M');

defined("WO_STATE_CREATED") || define ("WO_STATE_CREATED", "created");
defined("WO_STATE_PUBLISHED") || define ("WO_STATE_PUBLISHED", "published");
defined("WO_STATE_ASSIGNED") || define ("WO_STATE_ASSIGNED", "assigned");
defined("WO_STATE_WORK_DONE") || define ("WO_STATE_WORK_DONE", "work done");
defined("WO_STATE_INCOMPLETE") || define ("WO_STATE_INCOMPLETE", "incomplete");
defined("WO_STATE_APPROVED") || define ("WO_STATE_APPROVED", "approved");
defined("WO_STATE_IN_ACCOUNTING") || define ("WO_STATE_IN_ACCOUNTING", "in accounting");
defined("WO_STATE_COMPLETED") || define ("WO_STATE_COMPLETED", "completed");
defined("WO_STATE_DEACTIVATED") || define ("WO_STATE_DEACTIVATED", "deactivated");
defined("WO_STATE_ALL") || define ("WO_STATE_ALL", "");

defined("WO_STATE_ALL_ACTIVE") || define ("WO_STATE_ALL_ACTIVE", "all active");
defined("WO_STATE_COMPLETED_TAB") || define ("WO_STATE_COMPLETED_TAB", "completed tab");
defined("WO_STATE_TECH_PAID_TAB") || define ("WO_STATE_TECH_PAID_TAB", "tech paid tab");

defined("WO_UPDATE_MODE_NORMAL") || define ("WO_UPDATE_MODE_NORMAL", 0);
defined("WO_UPDATE_MODE_PUBLISH") || define ("WO_UPDATE_MODE_PUBLISH", 1);
defined("WO_UPDATE_MODE_ASSIGN") || define ("WO_UPDATE_MODE_ASSIGN", 2);
defined("WO_UPDATE_MODE_COMPLETE") || define ("WO_UPDATE_MODE_COMPLETE", 3);
defined("WO_UPDATE_MODE_WORK_DONE") || define ("WO_UPDATE_MODE_WORK_DONE", 4);
defined("WO_UPDATE_MODE_INCOMPLETE") || define ("WO_UPDATE_MODE_INCOMPLETE", 5);
defined("WO_UPDATE_MODE_APPROVE") || define ("WO_UPDATE_MODE_APPROVE", 6);
defined("WO_UPDATE_MODE_COMPLETE") || define ("WO_UPDATE_MODE_COMPLETE", 7);
defined("WO_UPDATE_MODE_DEACTIVATE") || define ("WO_UPDATE_MODE_DEACTIVATE", 8);

defined("WO_UPLOAD_DIR") || define ("WO_UPLOAD_DIR",1630);
defined("WO_UPLOAD_DIR_NAME") || define ('WO_UPLOAD_DIR_NAME','/Work Order Pics');



class Core_Api_Class
{
	private $user = null;
	private $mode = WO_UPDATE_MODE_NORMAL;

	public function __construct() {
		Core_Caspio::init();
	}
	/**
	 * pull basic tech info
	 *
	 * @return array
	 */
	public function test() {
		return array();
	}

	public function setMode ($mode = WO_UPDATE_MODE_NORMAL) {
		if ($mode >= 0 && $mode <= 8)
			$this->mode = $mode;
	}

	public function getMode () {
		return $this->mode;
	}

    public function createClient($login, $hash, $data){
        $errors = Core_Api_Error::getInstance();

        $authData = array('login'=>$login, 'password'=>$hash);
        $user = new Core_Api_User();
        $user->checkAuthentication($authData);
        if (!$user->isAuthenticate()) return API_Response::fail();

        $logger = Core_Api_Logger::getInstance();
        $logger->setUrl('createClient');
        $logger->setLogin($login);
        $logger->addLog(LOGGER_ERROR_NO, null, 'db request');

        $factory = new Core_Validation_Factory(Core_Validation_Factory::VALIDATION_TYPE_CLIENT_USER_CREATE);
	    $validation = $factory->getValidationObject($data);
		
        if ($validation->validate() === false) {
            $errors->addErrors('6', $validation->getErrors());
			return API_Response::fail();
        }

        unset($data['PasswordConf']);
        
        $client = new Core_Api_User();
        foreach( $data as $f=>$v ) {
            $client->$f = $v;
        }

		$client->CompanyName = $user->getContextCompanyName();
		
        $result = $client->save();
		
        $err = $errors->getErrors();
  		if (!empty($err))
			return API_Response::fail();

        return API_Response::success(array($result));
    }

    public function updateClient($login, $hash, $id, $data){
        $errors = Core_Api_Error::getInstance();

        $authData = array('login'=>$login, 'password'=>$hash);
        $user = new Core_Api_User();
        $user->checkAuthentication($authData);
        if (!$user->isAuthenticate()) return API_Response::fail();

        $logger = Core_Api_Logger::getInstance();
        $logger->setUrl('updateClient');
        $logger->setLogin($login);
        $logger->addLog(LOGGER_ERROR_NO, null, 'db request');

        $user = new Core_Api_User($id);
        $user->loadById();
        if($data['UserName'] == $user->UserName) unset($data['UserName']);
        
        $datatoupdate = array();
        foreach($data as $field=>&$val){
            if($val != $user->$field) $datatoupdate[$field] = $val;
        }

		$datatoupdate['CompanyName'] = $user->CompanyName;
        $factory = new Core_Validation_Factory(Core_Validation_Factory::VALIDATION_TYPE_CLIENT_USER_UPDATE);
		if (!empty($data['Password']) && $data['Password'] == $user->Password) $data['Password'] = 'bypass78'; // bypass password validation for old password

	    $validation = $factory->getValidationObject($data);

        if ($validation->validate() === false) {
            $errors->addErrors('6', $validation->getErrors());
			return API_Response::fail();
        }
        if(empty($datatoupdate)) return API_Response::success(array($user));
        $user = new Core_Api_User($id);
        $user->loadById();
        $result = $user->save($datatoupdate);
        $err = $errors->getErrors();
  		if (!empty($err))
			return API_Response::fail();

        return API_Response::success(array($result));
    }

    public function updateSite($login, $hash, $data){
        $errors = Core_Api_Error::getInstance();

        $authData = array('login'=>$login, 'password'=>$hash);
        $user = new Core_Api_User();
        $user->checkAuthentication($authData);
        if (!$user->isAuthenticate()) return API_Response::fail();

		$companyID = $user->getContextCompanyId();
		$logger = Core_Api_Logger::getInstance();
        $logger->setUrl('updateSitelist');
        $logger->setLogin($login);
        $logger->addLog(LOGGER_ERROR_NO, null, 'db request');

        $factory = new Core_Validation_Factory(Core_Validation_Factory::VALIDATION_TYPE_SITE_CREATE);
	    $validation = $factory->getValidationObject($data);

        if ($validation->validate() === false) {
            $errors->addErrors('6', $validation->getErrors());
			return API_Response::fail();
        }
		
		$site = new Core_Site();
		$result = false;

		foreach ($data as $k=>$v) {
			$site->$k = $v;
		}

		$n = $site->SiteNumber;
		$p = $site->Project_ID;
		if (!empty($n) && !empty($p)) {
			$site->save($companyID);
			$result = true;
		}
				
        $err = $errors->getErrors();
  		if (!empty($err))
			return API_Response::fail();

        return API_Response::success(array($result));
    }
	
    public function getSite($login, $hash, $siteNumber, $project_id){
        $errors = Core_Api_Error::getInstance();

        $authData = array('login'=>$login, 'password'=>$hash);
        $user = new Core_Api_User();
        $user->checkAuthentication($authData);
        if (!$user->isAuthenticate()) return API_Response::fail();

        $logger = Core_Api_Logger::getInstance();
        $logger->setUrl('getSitelist');
        $logger->setLogin($login);
        $logger->addLog(LOGGER_ERROR_NO, null, 'db request');
		
		$companyID = $user->getContextCompanyId();		

		$site = new Core_Site();
		$result = false;
	
		if (!$site->load($siteNumber, $project_id, $companyID)) $result = false;
		else $result = $site->toArray();
						
        $err = $errors->getErrors();
  		if (!empty($err))
			return API_Response::fail();

        return API_Response::success(array($result));
    }
	
    public function deleteSite($login, $hash, $siteNumber, $project_id){
        $errors = Core_Api_Error::getInstance();

        $authData = array('login'=>$login, 'password'=>$hash);
        $user = new Core_Api_User();
        $user->checkAuthentication($authData);
        if (!$user->isAuthenticate()) return API_Response::fail();

        $logger = Core_Api_Logger::getInstance();
        $logger->setUrl('deleteSite');
        $logger->setLogin($login);
        $logger->addLog(LOGGER_ERROR_NO, null, 'db request');
		
		$companyID = $user->getContextCompanyId();		

		$site = new Core_Site();
		$result = false;
	
		if (!$site->load($siteNumber, $project_id, $companyID)) $result = false;
		else $site->delete();
						
        $err = $errors->getErrors();
  		if (!empty($err))
			return API_Response::fail();

        return API_Response::success(array($result));
    }	
	
    /**
	 * Function for getting Sitea
	 *
	 * @param string $user
	 * @param string $pass
	 * @param array $filters
	 * @param string $sort
	 * @return API_Response
	 */
	public function getSiteList($login, $hash, $filters, $sort, $offset = 0, $limit = 0, &$count)
	{
        $errors = Core_Api_Error::getInstance();

        $authData = array('login'=>$login, 'password'=>$hash);
        $user = new Core_Api_User();
        $user->checkAuthentication($authData);
        if (!$user->isAuthenticate()) return API_Response::fail();

        $logger = Core_Api_Logger::getInstance();
        $logger->setUrl('getSiteList');
        $logger->setLogin($login);
        $logger->addLog(LOGGER_ERROR_NO, null, 'db request');

		$db = Core_Database::getInstance();
		$select = $db->select()->from(array('s' => Core_Database::TABLE_SITES),
            array('id','SiteNumber','SiteName','Site_Contact_Name','Address','City','State','Zipcode','Country','SitePhone','SiteFax','SiteEmail','Latitude','Longitude','Project_ID',
                'SiteTechs'=>'(Select Count(*) From tech_site Where SiteNumber=s.SiteNumber AND Project_ID=s.Project_ID )')
        );
        $select->join(array('p' => Core_Database::TABLE_PROJECTS), '`s`.Project_ID = `p`.Project_ID', array());

		// apply filters
		if (is_array($filters) ) {
		    if (isset($filters['Company_ID']) )
		        $select->where("`p`.Project_Company_ID=?", $filters['Company_ID']);
		    if (isset($filters['Project_ID']) )
		        $select->where("`p`.Project_ID=?", $filters['Project_ID']);
		    if (isset($filters['AutoComplete']) ) {
				if ($filters['AutoComplete'])
			        $select->where("`s`.SiteNumber LIKE ?", '%' . $filters['AutoComplete'] . '%');
			}
		}

		if (!empty($sort) )
	    	$select->order($sort);

		if ($offset != 0 || $limit != 0)
			$select->limit($limit, $offset);
        //echo $select; exit;
		$result = new Core_Filter_Result($select);
		if ($count !== null) $count = count($result);

		$err = $errors->getErrors();
		if (!empty($err))
			return API_Response::fail();

		return API_Response::success($result->toArray());
	}
	
    public function addContact($login, $hash, $data, $fromEmail, $copy)
    {
        $errors = Core_Api_Error::getInstance();

        $authData = array('login'=>$login, 'password'=>$hash);
        $user = new Core_Api_User();
        $user->checkAuthentication($authData);
        if (!$user->isAuthenticate()) return API_Response::fail();

        $logger = Core_Api_Logger::getInstance();
        $logger->setUrl('addContact');
        $logger->setLogin($login);
        $logger->addLog(LOGGER_ERROR_NO, null, 'db request');
        
        if ($data['Type'] == 'email') {
            $eList = $data['Contact'];
            if ($copy == 1) $eList .= "," . $fromEmail;
			
			$contactMail = new Core_Mail();
			$contactMail->setFromName($data['Created_By']);
			$contactMail->setFromEmail($fromEmail);
			$contactMail->setToName("");
			$contactMail->setToEmail($eList);
			$contactMail->setSubject($data['Subject']);
			$contactMail->setBodyText($data['Message']);

			$contactMail->send();
        }
        
        $objDateNow = new Zend_Date();
        //$data['Created_Date'] = $objDateNow->toString('yyyy-MM-dd H:i:s');
        $data['Created_Date'] = date('Y-m-d H:i:s');
        $contact = new Core_ContactLog();
        $result = $contact->insert($data);

        $err = $errors->getErrors();
  		if (!empty($err))
			return API_Response::fail();

        return API_Response::success(array($result));
    }

	/**
	 * pull the list of timestamps for a work order
	 *
	 * @param string $user
	 * @param string $pass
	 * @param int $winNum
	 * @param int $offset
	 * @param int $limit
	 * @param string $sort
	 * @return API_Response
	 */
	public function getTimestampsWithWinNum($user, $pass, $winNum, $offset = 0, $limit = 0, $sort = "")
	{
       	$logger = Core_Api_Logger::getInstance();
		$logger->setUrl('getTimestampsForWinNum' . $winNum);
		$logger->setLogin($user);
		$logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');

		$authData = array('login'=>$user, 'password'=>$pass);

		$user = new Core_Api_User();
		$user->checkAuthentication($authData);

		if (!$user->isAuthenticate())
			return API_Response::fail();

		$companyID = $user->getContextCompanyId();

		return API_Response::success(API_Timestamp::getTimestamps($winNum, $companyID, $offset, $limit, $sort));
	}

	
	/**
	 * pull the list of timestamps for a project
	 *
	 * @param string $user
	 * @param string $pass
	 * @param int $winNum
	 * @param int $offset
	 * @param int $limit
	 * @param string $sort
	 * @return API_Response
	 */
	public function getProjectTimestampsByProject($user, $pass, $projectId, $offset = 0, $limit = 0, $sort = "")
	{
       	$logger = Core_Api_Logger::getInstance();
		$logger->setUrl('getTimestampsForWinNum' . $winNum);
		$logger->setLogin($user);
		$logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');

		$authData = array('login'=>$user, 'password'=>$pass);

		$user = new Core_Api_User();
		$user->checkAuthentication($authData);

		if (!$user->isAuthenticate())
			return API_Response::fail();

		$companyID = $user->getContextCompanyId();
		
		
		$db = Core_Database::getInstance();
		$select = $db->select();
		$select->from(array("ts" => Core_Database::TABLE_PROJECTASCTIMESTAMPS),
				'*')
			->where("Project_ID = ?", $projectId);
		if (!empty($companyID))
			$select->where("Company_ID = ?", $companyID);
		if ($limit != 0 || $offset != 0)
			$select->limit($limit, $offset);
		if (empty($sort))
			$select->order(array('DateChanged DESC'));
		else 
			$select->order(array($sort));
        $result = false;

        $tsList = array();
        
		try {
			$result = $select->query()->fetchAll();
			$countRows = sizeof($result);
			
    		foreach ($result as $ts) {
    			$tsObj = new self();
    			foreach ($ts as $key => $value) {
    			    //$dbField = preg_replace('/[^\.]+\./', '', $dbField);
    				$tsObj->$key = $value;
    			}
    			$tsList[] = $tsObj;
    		}
			
		} catch (Exception $e) {
            return API_Response::fail();
        }

		return API_Response::success($tsList);
	}

	/**
	 * remove tech to client preferred
	 *
	 * @param string $user
	 * @param string $pass
	 * @param int $id
	 * @return API_Response
	 */
	public function removeTechPrefer($user, $pass, $id) {
		$authData = array('login'=>$user, 'password'=>$pass);
		$errors = Core_Api_Error::getInstance();

		$user = new Core_Api_User();
		$user->checkAuthentication($authData);

		if (!$user->isAuthenticate())
			return API_Response::fail();

		$companyID = $user->getContextCompanyId();

		$tech = new Core_Tech();
		$result = $tech->removePreferredTech($companyID, $id);
		if (!$result)
			return API_Response::fail();

		return API_Response::success(array());
	}

	/**
	 * Function for getting Techs Denied
	 *
	 * @param string $user
	 * @param string $pass
	 * @param array $filters
	 * @param string $sort
	 * @param init $offset
	 * @param int $limit
	 * @param int $count
	 * @return API_Response
	 */
	public function getTechsDenied($user, $pass, $filters, $sort, $offset = 0, $limit = 0, &$count) {
		$authData = array('login'=>$user, 'password'=>$pass);
		$errors = Core_Api_Error::getInstance();

		$user = new Core_Api_User();
		$user->checkAuthentication($authData);

		if (!$user->isAuthenticate())
			return API_Response::fail();

		$companyID = $user->getContextCompanyId();
		
		$count = 0;
		$result = Core_Tech::getClientDeniedTechs($companyID, $filters, $sort, $offset, $limit, $count);
		
		$err = $errors->getErrors();
		if (!empty($err))
			return API_Response::fail();

		return API_Response::success($result);
	}
	
	/* getTechs, added a new function to get the three different tech search results for
	*  the findMyTechs gadget. The three queries fun faster and actually return data for 
	*  results over 200 miles.
	*/

	public function getTechs($user, $pass, $params, $use, $offset = 0, $limit = 0, &$countRows = null, $sortStr = null)
	{

		$authData = array('login'=>$user, 'password'=>$pass);
		$errors = Core_Api_Error::getInstance();

		$user = new Core_Api_User();
		$user->checkAuthentication($authData);

		if (!$user->isAuthenticate())
			return API_Response::fail();

        $companyID = $user->getContextCompanyId();



        $tech = null;
        $position = null;
        if ($params instanceof API_TechFilter) {
            $tech = $params;
        } else {
            $tech = new API_TechFilter();

            $allowedParams = array(
            	"FLSIDExists",
            	"No_Shows",
            	"Qty_IMAC_Calls",
            	"SATRecommendedAvg",
            	"SATPerformanceAvg",
                "SATRecommendedTotal",
			    "SATPerformanceTotal",
            	"Qty_FLS_Service_Calls",
            	"selfRating",
            	"Bg_Test_Pass_Lite",
            	"HP_CertNum",
            	'hideBanned',
		        'preferredOnly',
            	'TechID',
            	'TechIDs',
                'HourlyPay',
                'UserName',
                'PrimaryEmail',
                'LastName',
                'FirstName',
                'City',
                'State',
                'Country',
                'CompTIA',
                'FS_Cert_Test_Pass',
                'HP_CertNum',
                'SamsungCert',
                'Bg_Test_Pass_Lite',
                'APlus',
                'MCSE',
                'CCNA',
                'DellCert',
                'BICSI',
                'Laptop',
                'DigitalCam',
                'CellPhone',
                'Ladder',
                'HelpingHands',
                'SelfRating'
            );
            //13735
            $allowedParams[] = 'FSExpertForCatID';
            //end 13735
            $allowedParams[] = 'hideBids';//14023
            foreach ($params as $key => $value) {
                if (in_array($key, $allowedParams) ) {
                    $tech->{$key} = $value;
                }

            }

            $tech->NotPrivate = true;
            $tech->WithCoordinates = true;


            if (!empty($params['ProximityZipCode']) ) {
                $position = array(
                    'ProximityZipCode' => $params['ProximityZipCode'],
                    'ProximityLat' => $params["ProximityLat"],
                    'ProximityLng' => $params['ProximityLng'],
                    'ProximityDistance' => $params['ProximityDistance']
                );
            }
        }

        switch($use){
          case "myRecent":
               $result = $tech->filterMyRecent($companyID, null, $position, $tech, $offset, $limit, $countRows, $sortStr );
               break;

          case "myPreferred":
               $result = $tech->filterPreferred($companyID, null, $position, $tech, $offset, $limit, $countRows, $sortStr );
               break;

          case "recent":
               $tech->DeniedByCompanyID = $companyID;//13771
               $result = $tech->filterRecent( null, $position, $tech, $offset, $limit, $countRows, $sortStr );
               break;
        }
        return $result;
	}

	/**
	 * remove tech to tech deny
	 *
	 * @param string $user
	 * @param string $pass
	 * @param int $id
	 * @return API_Response
	 */
	public function removeTechDeny($user, $pass, $id) {
		$authData = array('login'=>$user, 'password'=>$pass);
		$errors = Core_Api_Error::getInstance();

		$user = new Core_Api_User();
		$user->checkAuthentication($authData);

		if (!$user->isAuthenticate())
			return API_Response::fail();

		$companyID = $user->getContextCompanyId();
                
                $apiremovedeninedctech = new Core_Api_WosClass();
                $apiremovedeninedctech->TechNoLongerClientDenied($id,$user->getClientId(),$companyID) ;
                        
		$result = Core_Tech::removeClientDeniedTech($id, $companyID);
		
		if (!$result)
			return API_Response::fail();

		return API_Response::success(array());
	}

	/**
	 * add tech to client preferred
	 *
	 * @param string $user
	 * @param string $pass
	 * @param int $id
	 * @return API_Response
	 */
	public function techPrefer($user, $pass, $id) {
		$authData = array('login'=>$user, 'password'=>$pass);
		$errors = Core_Api_Error::getInstance();

		$user = new Core_Api_User();
		$user->checkAuthentication($authData);

		if (!$user->isAuthenticate())
			return API_Response::fail();

        // check prefer
        $techStatus = $this->techGetPreferStatus($authData['login'], $authData['password'], $id);

        if ($techStatus->data == 1) {
            // added yet
            return API_Response::success(array());
        }
        
		$companyID = $user->getContextCompanyId();

		$result = Core_Tech::addPreferredTech($companyID, $id);
		if (!$result)
			return API_Response::fail();

		return API_Response::success(array());
	}

	/**
	 * add tech to tech deny
	 *
	 * @param string $user
	 * @param string $pass
	 * @param int $id
	 * @param string $comments
	 * @return API_Response
	 */
	public function techDeny($user, $pass, $id, $comments) {
		$authData = array('login'=>$user, 'password'=>$pass);
		$errors = Core_Api_Error::getInstance();

		$user = new Core_Api_User();
		$user->checkAuthentication($authData);

		if (!$user->isAuthenticate())
			return API_Response::fail();

		$companyID = $user->getContextCompanyId();
		$clientID = $user->getClientID();
		
		$result = Core_Tech::clientDenyTech($id, $comments, $companyID, $clientID);
		
		if (!$result)
			return API_Response::fail();

		return API_Response::success(array());
	}

	/**
	 * Get Tech PreferStatus
	 *
	 * @param string $user
	 * @param string $pass
	 * @param int $techID
	 * @return API_Response
     * @author Artem Sukharev
	 */
	public function techGetPreferStatus($user, $pass, $techID)
	{
       	$logger = Core_Api_Logger::getInstance();
		$logger->setUrl('techGetPreferStatus ' . $techID);
        $logger->setLogin($user);
		$logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');

		$errors = Core_Api_Error::getInstance();

		if ( empty($techID) ) $errors->addError(2, 'Tech ID parameter is empty but required');
		if ( $errors->hasErrors() ) return API_Response::fail();

		$authData = array('login'=>$user, 'password'=>$pass);
		$user = new Core_Api_User();
		$user->checkAuthentication($authData);
		if ( !$user->isAuthenticate() ) return API_Response::fail();

        $companyID = Core_Caspio::caspioEscape($user->getContextCompanyId());
		$preferred = Core_Tech::isTechPreferred($companyID, $techID);
        $denied = Core_Tech::isClientDenied($techID, $companyID);

        if ( $errors->hasErrors() ) return API_Response::fail();

		$status = 0;
		if ( $denied ) $status = 2;
		elseif ( $preferred ) $status = 1;

		return API_Response::success($status);
	}

	/**
	 * pull basic tech info
	 *
	 * @param string $user
	 * @param string $pass
	 * @param int $id
	 * @return API_Response
	 */
	public function techLookup($user, $pass, $id)
	{
		$authData = array('login'=>$user, 'password'=>$pass);
		$errors = Core_Api_Error::getInstance();

		$user = new Core_Api_User();
		$user->checkAuthentication($authData);

		if (!$user->isAuthenticate())
			return API_Response::fail();

		$companyID = $user->getContextCompanyId();

		$tech = new API_Tech();
		$tech->lookupID($id, $companyID);
		return API_Response::success(array($tech));
	}
    //13458
    public function techLookupFLSID($user, $pass, $id)
    {
        $authData = array('login'=>$user, 'password'=>$pass);
        $errors = Core_Api_Error::getInstance();

        $user = new Core_Api_User();
        $user->checkAuthentication($authData);

        if (!$user->isAuthenticate())
            return API_Response::fail();

        $companyID = $user->getContextCompanyId();
        $tech = new API_Tech();
        $tech->lookupFLSID($id, $companyID);
        return API_Response::success(array($tech));
    }

	public function searchTechs($user, $pass, $params, $offset = 0, $limit = 0, &$countRows = null, $sortStr = null)
	{

		$authData = array('login'=>$user, 'password'=>$pass);
		$errors = Core_Api_Error::getInstance();

		$user = new Core_Api_User();
		$user->checkAuthentication($authData);

		if (!$user->isAuthenticate())
			return API_Response::fail();

        $companyID = $user->getContextCompanyId();

        $tech = null;
        $position = null;
        if ($params instanceof API_TechFilter) {
            $tech = $params;
        } else {
            $tech = new API_TechFilter();

            $allowedParams = array(
            	"FLSIDExists",
            	"No_Shows",
            	"Qty_IMAC_Calls",
            	"SATRecommendedAvg",
            	"SATPerformanceAvg",
                "SATRecommendedTotal",
			    "SATPerformanceTotal",
            	"Qty_FLS_Service_Calls",
            	"selfRating",
            	"Bg_Test_Pass_Lite",
            	"HP_CertNum",
            	'hideBanned',
		'preferredOnly',
            	'TechID',
            	'TechIDs',
                'HourlyPay',
                'UserName',
                'PrimaryEmail',
                'LastName',
                'FirstName',
                'City',
                'State',
                'Country',
                'CompTIA',
                'FS_Cert_Test_Pass',
                'HP_CertNum',
                'SamsungCert',
                'Bg_Test_Pass_Lite',
                'APlus',
                'MCSE',
                'CCNA',
                'DellCert',
                'BICSI',
                'Laptop',
                'DigitalCam',
                'CellPhone',
                'Ladder',
                'HelpingHands',
                'SelfRating'
            );

            foreach ($params as $key => $value) {
                if (in_array($key, $allowedParams) ) {
                    $tech->{$key} = $value;
                }

            }

            $tech->NotPrivate = true;
            $tech->WithCoordinates = true;


            if (!empty($params['ProximityZipCode']) ) {
                $position = array(
                    'ProximityZipCode' => $params['ProximityZipCode'],
                    'ProximityLat' => $params["ProximityLat"],
                    'ProximityLng' => $params['ProximityLng'],
                    'ProximityDistance' => $params['ProximityDistance']
                );
            }
        }

        // search type
        $formType = 'find_techs';
        if (is_array($params) && !empty($params['formType']) && in_array($params['formType'], array('find_techs', 'find_cabling_techs', 'find_telephony_techs') ) ) {
            $formType = $params['formType'];
        }
	
        $result = $tech->filter($companyID, null, $position, $tech, $offset, $limit, $countRows, $sortStr, $formType );

		return $result;
	}

    /**
     * uploadFile
     *
     * @param string $user
     * @param string $pass
     * @param string $winNum
     * @param string $fName
     * @param string $fData
     * @param string $fMime
     * @param string $saveToField ENUM {Pic1, Pic2, Pic3}
     * @access public
     * @return API_Response
     */
    public function uploadFile( $user, $pass, $winNum, $fName, $fData, $fMime, $saveToField = 'Pic1' )
    {
        require_once 'Core/Api/Logger.php';
        require_once 'Core/Api/Error.php';
        require_once 'API/Response.php';
        require_once 'Core/Caspio.php';

        /* Log request */
       	$logger = Core_Api_Logger::getInstance();
		$logger->setUrl('Upload file from Core_Api_Class ' . $winNum);
        $logger->setLogin($user);
        $logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');
        /* Log request */

        $error = Core_Api_Error::getInstance();

        /* Check field */
        if ( !in_array($saveToField, API_WorkOrder::uploadFileFields()) ) {
            $error->addError(6, 'Incorrect field name "'.$saveToField.'" for file saving');
            return API_Response::fail();
        }
        /* Check field */

        /* Check auth and get WO with file field we save want */
        $wo = $this->getWorkOrder($user, $pass, $winNum, join(',', API_WorkOrder::uploadFileFields()).',TB_UNID');
        if ( $error->getErrors() ) {
            return API_Response::fail();
        }
        $wo = $wo->data[0];
        /* Check auth and get WO with file field we save want */

        /* Check Filename */
        //$fields = array_combine(API_WorkOrder::getWritableFields(), array_fill(0, count(API_WorkOrder::getWritableFields()), NULL));
//        $fields = array($saveToField => $fName);
//        $validation = new Core_Validation_Factory(Core_Validation_Factory::VALIDATION_TYPE_UPDATE);
//	    $validation = $validation->getValidationObject($fields);
//
//	    if ( !$validation->validate() ) {
//
//            $error->addErrors(6, $validation->getErrors());
//			return API_Response::fail();
//        }
        /* Check Filename */

        /* save new file if we have no errors */
        /* Detect User's file with the same name */

        $fReplace = false;
        foreach ( API_WorkOrder::uploadFileFields() as $_field ) {
            if ( ltrim($wo->$_field, '/') === WO_UPLOAD_DIR_NAME.$fName ) {
                $fReplace    = true;
                $saveToField = $_field; //  Save new file to field where file with the same name
                break;
            }
        }


        /* Detect User's file with the same name */
        Core_Caspio::caspioUploadFile($fName, $fData, $fMime, WO_UPLOAD_DIR, $fReplace);

        if ( $error->getErrors() ) {
            return API_Response::fail();
        }

        /* save new file if we have no errors */

        /* Remove old file name and save new one */
        if ( !$fReplace && $wo->$saveToField ) {
            // TODO Remove file from Caspio
        }

        if ( !$fReplace ) {
            $woUp = new API_WorkOrder;
            $woUp->$saveToField = WO_UPLOAD_DIR_NAME.'/'.ltrim($fName, '/') ;
            return $this->updateWorkOrder($user, $pass, $winNum, $woUp);
        } else {
            return API_Response::success(array($wo));
        }
        /* Remove old file name and save new one */
    }


    /**
     * uploadFileAWS
     *
     * @param string $user
     * @param string $pass
     * @param string $winNum
     * @param string $fName
     * @param string $fData
     * @param int $type
     * @param string $descr
     * @access public
     * @return mixed
     */
    public function uploadFileAWS( $user, $pass, $winNum, $fName, $fData, $type, $descr)
    {
        require_once 'Core/Api/Logger.php';
        require_once 'Core/Api/Error.php';
        require_once 'API/Response.php';

        $fName = ltrim(trim($fName), '/');

        /* Log request */
       	$logger = Core_Api_Logger::getInstance();
		$logger->setUrl('Upload file from Core_Api_Class ' . $winNum);
        $logger->setLogin($user);
        $logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');

        $error = Core_Api_Error::getInstance();

        $wo = $this->getWorkOrder($user, $pass, $winNum);
        if ( $error->getErrors() || empty($wo->data)) {
            $error->addError(6, 'There is no such WO');
            return API_Response::fail();
        }
		// decode file data if it has been base64 encoded
		$decoded = base64_decode($fData, true);
		if ($decoded !== FALSE) $fData = $decoded;
        $files = new Core_File();
        $uploadRes = $files->uploadFile($fName, $fData, S3_CLIENTS_DOCS_DIR);

        if(!$uploadRes){
            $error->addError(6, 'Upload file '.$fName.' failed');
            $errorsDetails = $error->getErrors();
            error_log('FileUploading: (client:WO:uploadFileAWS) ' . implode('|', $errorsDetails));
            return API_Response::fail();
        }

        if ( $error->getErrors() ) {
            return API_Response::fail();
        }

        if ($type == Core_Files::SIGNOFF_FILE) $fName = "wos-docs/" . $fName;

        $files = new Core_Files();
        $lastId = $files->insert(array(
            'WIN_NUM' => $winNum,
            'path'    => $fName,
            'type'    => $type,
            'description' => mysql_escape_string($descr)
        ));

        Core_TimeStamp::createTimeStamp($winNum, Core_Api_User::getLoginByAuth($user), "Work Order Updated: File {$fName} was uploaded.", '', '', '', "", "", "");
        
        if($lastId) $res = new API_Response (1, 0, $lastId);
        else $res = API_Response::fail();

        return $res;
    }
    

    /**
     * copyWorkOrderDocs
     *
     * @param string $user
     * @param string $pass
     * @param string $origWinNum
     * @param string $newWinNum
     * @access public
     * @return mixed
     */
    public function copyWorkOrderDocs( $user, $pass, $origWinNum, $newWinNum)
    {
        require_once 'Core/Api/Logger.php';
        require_once 'Core/Api/Error.php';
        require_once 'API/Response.php';


        /* Log request */
       	$logger = Core_Api_Logger::getInstance();
		$logger->setUrl('Copy files from Core_Api_Class ' . $origWinNum);
        $logger->setLogin($user);
        $logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');

        $error = Core_Api_Error::getInstance();

        $wo = $this->getWorkOrder($user, $pass, $origWinNum);
        if ( $error->getErrors() ) {
            $error->addError(6, 'There is no such WO');
            return API_Response::fail();
        }

        if ( $error->getErrors() ) {
            return API_Response::fail();
        }
        
        $db = Core_Database::getInstance();
		$select = $db->select();
		$select->from(array("files" => "files"),'*')
			->where("WIN_NUM = ?", $origWinNum)
			->where("type = '1'");
		
        $result = false;
        
   		 try {
			$result = $select->query()->fetchAll();
			$countRows = sizeof($result);
			
    		foreach ($result as $file) {
    			$fileObj = new Core_Files();
        		$lastId = $fileObj->insert(array(
            		'WIN_NUM' => $newWinNum,
            		'path'    => $file["path"],
            		'type'    => $file["type"],
            		'description' => mysql_escape_string($file["description"])
       			 ));
    		}
			
		} catch (Exception $e) {
            return API_Response::fail();
        }
        
    	if ( $error->getErrors() ) {
            return API_Response::fail();
        }

        return API_Response::success(array());
    }

    /**
     * uploadClientLogo
     *
     * @param string $user
     * @param string $pass
     * @param string $client_id
     * @param string $fName
     * @param string $fData
     * @param string $field
     * @access public
     * @return API_Response
     */
    public function uploadClientLogo( $login, $hash, $client_id, $fName, $fData, $field)
    {
        $errors = Core_Api_Error::getInstance();

        $authData = array('login'=>$login, 'password'=>$hash);
        $user = new Core_Api_User();
        $user->checkAuthentication($authData);

        if (!$user->isAuthenticate()) return API_Response::fail();

        $logger = Core_Api_Logger::getInstance();
        $logger->setUrl('updateClient');
        $logger->setLogin($login);
        $logger->addLog(LOGGER_ERROR_NO, null, 'db request');

        $fName = ltrim(trim($fName), '/');
        $file = new Core_File();
        $uploadResult = $file->uploadFile($fName, $fData, S3_CLIENTS_LOGO_DIR, true);

        if(!$uploadResult){
            $errors->addError(6, 'Upload file '.$fName.' failed');
            $errorsDetails = $errors->getErrors();
            error_log('FileUploading: (admin:client:uploadFileAWS) ' . implode('|', $errorsDetails));
            return API_Response::fail();
        }

        if ( $errors->getErrors() ) {
            return API_Response::fail();
        }

        $client = new Core_Api_User($client_id);
        $res = $client->save(array($field=>$fName));

        Core_TimeStamp::createTimeStamp($client_id, Core_Api_User::getLoginByAuth($login), "Client Updated: Company Logo {$fName} was uploaded.", '', '', '', "", "", "");

        $err = $errors->getErrors();
  		if (!empty($err))
			return API_Response::fail();

        return API_Response::success(array());
    }

    /**
	 * Function for getting Clients
	 *
	 * @param string $user
	 * @param string $pass
	 * @param array $filters
	 * @param string $sort
	 * @return API_Response
	 */
	public function getCustomersList($login, $hash, $filters, $sort, $offset = 0, $limit = 0, &$count)
	{
        $errors = Core_Api_Error::getInstance();

        $authData = array('login'=>$login, 'password'=>$hash);
        $user = new Core_Api_User();
        $user->checkAuthentication($authData);
        if (!$user->isAuthenticate()) return API_Response::fail();

        $logger = Core_Api_Logger::getInstance();
        $logger->setUrl('getClientsList');
        $logger->setLogin($login);
        $logger->addLog(LOGGER_ERROR_NO, null, 'db request');

		$db = Core_Database::getInstance();
		$select = $db->select()->from(Core_Database::TABLE_CUSTOMERS);

		// apply filters
		if (is_array($filters) ) {
		    if (isset($filters['Company_ID']) )
		        $select->where("Company_ID=?", $filters['Company_ID']);
		}

		if (!empty($sort) )
	    	$select->order($sort);

		if ($offset != 0 || $limit != 0)
			$select->limit($limit, $offset);
        //echo $select; exit;
		$result = new Core_Filter_Result($select);
		if ($count !== null) $count = count($result);

		$err = $errors->getErrors();
		if (!empty($err))
			return API_Response::fail();

		return API_Response::success($this->makeCustomersArray($result->toArray()));
	}
    /**
	 * Function for getting Clients
	 *
	 * @param string $user
	 * @param string $pass
	 * @param array $filters
	 * @param string $sort
	 * @return API_Response
	 */
	public function getClientsList($login, $hash, $filters, $sort, $offset = 0, $limit = 0, &$count)
	{
		$errors = Core_Api_Error::getInstance();
		
        $authData = array('login'=>$login, 'password'=>$hash);
        $user = new Core_Api_User();
        $user->checkAuthentication($authData);
        if (!$user->isAuthenticate()) return API_Response::fail();

        $logger = Core_Api_Logger::getInstance();
        $logger->setUrl('getClientsList');
        $logger->setLogin($login);
        $logger->addLog(LOGGER_ERROR_NO, null, 'db request');

		$db = Core_Database::getInstance();
		$select = $db->select()->from(Core_Database::TABLE_CLIENTS);

		// apply filters
		if (is_array($filters) ) {
		    if (isset($filters['Company_ID']) )
				if(!empty($filters['ShowAllGPM']))
			        $select->where("Company_ID=? OR ProjectManager=1", $filters['Company_ID']);
				else
			        $select->where("Company_ID=?", $filters['Company_ID']);
		     if(isset($filters['AccountEnabled']))
		     	$select->where("AccountEnabled=?", $filters['AccountEnabled']);
		}
                $select->where("MarkDelete=?", 0);
		if (!empty($sort) )
	    	$select->order($sort);

		if ($offset != 0 || $limit != 0)
			$select->limit($limit, $offset);
        //echo $select; exit;
        
		$result = new Core_Filter_Result($select);
		if ($count !== null) $count = count($result);

		$err = $errors->getErrors();
		if (!empty($err))
			return API_Response::fail();

		return API_Response::success($this->makeClientsArray($result->toArray()));
	}

    //13504 
    public function getClientsList_NoCheck($filters, $sort, $offset = 0, $limit = 0, &$count)
    {
        $errors = Core_Api_Error::getInstance();
        
        $db = Core_Database::getInstance();
        $select = $db->select()->from(Core_Database::TABLE_CLIENTS);

        // apply filters
        if (is_array($filters) ) {
            if (isset($filters['Company_ID']) )
                if(!empty($filters['ShowAllGPM']))
                    $select->where("Company_ID=? OR ProjectManager=1", $filters['Company_ID']);
                else
                    $select->where("Company_ID=?", $filters['Company_ID']);
             if(isset($filters['AccountEnabled']))
                 $select->where("AccountEnabled=?", $filters['AccountEnabled']);
        }
                $select->where("MarkDelete=?", 0);
        if (!empty($sort) )
            $select->order($sort);

        if ($offset != 0 || $limit != 0)
            $select->limit($limit, $offset);
        //echo $select; exit;
        
        $result = new Core_Filter_Result($select);
        if ($count !== null) $count = count($result);

        $err = $errors->getErrors();
        if (!empty($err))
            return API_Response::fail();

        return API_Response::success($this->makeClientsArray($result->toArray()));
    }
    /**
	 * Function for getting ContactLog
	 *
	 * @param string $user
	 * @param string $pass
	 * @param array $filters
	 * @param string $sort
	 * @return API_Response
	 */
	public function getContactLog($login, $hash, $filters, $sort, $offset = 0, $limit = 0, &$count)
	{
        $errors = Core_Api_Error::getInstance();

        $authData = array('login'=>$login, 'password'=>$hash);
        $user = new Core_Api_User();
        $user->checkAuthentication($authData);
        if (!$user->isAuthenticate()) return API_Response::fail();

        $logger = Core_Api_Logger::getInstance();
        $logger->setUrl('getClientsList');
        $logger->setLogin($login);
        $logger->addLog(LOGGER_ERROR_NO, null, 'db request');

		$log = Core_Database::getInstance();
		$select = $log->select()
                ->from(Core_Database::TABLE_WO_CONTACT_LOG);

		// apply filters
		if (is_array($filters) ) {
		    if (isset($filters['Company_ID']) )
		        $select->where("Company_ID=?", $filters['Company_ID']);
            if (isset($filters['WorkOrder_ID']) )
		        $select->where("WorkOrder_ID=?", $filters['WorkOrder_ID']);
            if (isset($filters['id']) )
		        $select->where("id=?", $filters['WorkOrder_ID']);
		}

		if (!empty($sort) )
	    	$select->order($sort);

		if ($offset != 0 || $limit != 0)
			$select->limit($limit, $offset);
		$result = new Core_Filter_Result($select);
		if ($count !== null) $count = count($result);

		$err = $errors->getErrors();
		if (!empty($err))
			return API_Response::fail();

		return API_Response::success($result->toArray());
	}

	/**
	 * pull basic tech info
	 *
	 * @param string $user
	 * @param string $pass
	 * @param string $fName
	 * @return API_Response
	 */

    public function getFile( $user, $pass, $fName)
    {
       	$logger = Core_Api_Logger::getInstance();
		$logger->setUrl('getWorkOrders');
        $logger->setLogin($user);
		$logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');

		$authData = array('login'=>$user, 'password'=>$pass);
		$errors = Core_Api_Error::getInstance();
		$errors->resetErrors();

		$user = new Core_Api_User();
        $user->checkAuthentication($authData);

		if (!$user->isAuthenticate())
			return API_Response::fail();
        $fName = ltrim(trim($fName), '/');
        $file = Core_Caspio::caspioDownloadFile( $fName );
		if($file === false) return API_Response::fail();

        return $file;
    }

	/**
	 * add record to login history table
	 *
	 * @param string $user
	 * @param string $pass
	 * @param string $usertType
	 * @param string $browser
	 * @param string $platform
	 * @param string $mobileVersion
	 * @return API_Response
	 */

    public function loginHistory( $userName, $userType, $browser = NULL, $platform = NULL, $mobileVersion = NULL)
    {
    	if(empty($userName) || empty($userType)) return NULL;
    	
    	/*
       	$logger = Core_Api_Logger::getInstance();
		$logger->setUrl('getWorkOrders');
        $logger->setLogin($user);
		$logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');
		*/
    	$objDateNow = new Zend_Date();
        $date = $objDateNow->toString('yyyy-MM-dd HH:mm:ss');
		
		$userAgent = NULL;
		if (!empty($_SERVER['HTTP_USER_AGENT'])) {
			try {
				$b = new Browscap(CACHE_DIR_FOR_FILES);
				$r = $b->getBrowser(null, true);
				$userAgent = substr($_SERVER['HTTP_USER_AGENT'], 0, 255);
				if (empty($platform)) $platform = empty($platform) ? $r['Platform'] : NULL;
				$browser = empty($browser) ? $r['Parent'] : NULL;
			} catch (Exception $e) {error_log($e->getMessage());}
		}
		$platform = empty($platform) ? NULL : substr($platform, 0, 63);
		$browser = empty($browser) ? NULL : substr($browser, 0, 63);
		$mobileVersion = empty($mobileVersion) ? NULL : substr($mobileVersion, 0, 63);
		$data = array('UserName'=>$userName, 'UserType'=>$userType, 'DateTime'=>$date, 'UserAgent' => $userAgent, "Platform" => $platform, "Browser" => $browser, "MobileVersion" => $mobileVersion, "IP_Address" => $_SERVER['REMOTE_ADDR']);
    	try{
			$db = Core_Database::getInstance();
			$result = $db->insert(Core_Database::TABLE_LOGIN_HISTORY, $data);
		}catch(Exception $e){
    		error_log($e);
    	}
		
		if ($result && $userType == "Tech") {			
			$select = $db->select ()
				->from (Core_Database::TABLE_TECH_BANK_INFO, array("id", "TechID", "FS_Mobile_Date"))
				->where ("UserName = ?", $userName);
			$tech = $db->fetchAll ($select);
			try {
				$result = $db->update('login_tech_last', array(
					'datetime' => $date
				), "TechID = " . $tech[0]["TechID"]);
				if (empty($result)) {
					$result = $db->insert('login_tech_last', array(
						'TechID' => $tech[0]["TechID"],
						'datetime' => $date
					));
				}
			} catch (Exception $e) {}
			if (stripos ($platform, "iphone") !== false || 
				stripos ($platform, "android") !== false) {
				if ($tech && (!$tech[0]["FS_Mobile_Date"] || $tech[0]["FS_Mobile_Date"] == "0000-00-00 00:00:00")) {
					$id = $tech[0]["id"];
					$fields = array ("FS_Mobile_Date" => $date);
					$db->update (Core_Database::TABLE_TECH_BANK_INFO, $fields, "id = '$id'");
				}
			}
		}

		return $result;
    }

	/**
	 * get list of work orders
	 *
	 * @param string $user
	 * @param string $pass
	 * @param string $fields
	 * @param string $timeStampFrom
	 * @param string $timeStampTo
	 * @param string $timeStampType
	 * @param API_WorkOrderFilter $filters
	 * @param int $offset
	 * @param int $limit
	 * @param int $countRows
	 * @param string $sort
	 * @return API_Response
	 */
	public function getWorkOrders($user, $pass, $fields, $timeStampFrom, $timeStampTo, $timeStampType, $filters, $offset = 0, $limit = 0, &$countRows = NULL, $sort = "", $withAdditionalFields=false, $visits = false, $optimize = false)
	{
       	$logger = Core_Api_Logger::getInstance();
		$logger->setUrl('getWorkOrders');
        $logger->setLogin($user);
		$logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');

		$authData = array('login'=>$user, 'password'=>$pass);
		$errors = Core_Api_Error::getInstance();

		$user = new Core_Api_User();
        $user->checkAuthentication($authData);

		if (!$user->isAuthenticate())
			return API_Response::fail();
        $companyID = $user->getContextCompanyId();

		$countRows = 0;
		$client_id = $user->getClientId();
/*		$myDashboardSettings = Core_DashboardSettings::getSettings($client_id);
		if (!empty($myDashboardSettings)) {
			$myDashboardSettings = $myDashboardSettings['filter'];
			if ($myDashboardSettings == Core_DashboardSettings::FILTER_PROJECT_MY || $myDashboardSettings == Core_DashboardSettings::FILTER_PROJECT_SELECT)
				$filters->DisplayProjectByClientUser = $client_id;
			else if ($myDashboardSettings == Core_DashboardSettings::FILTER_WORK_ORDER)
				$filters->DisplayByClientUserAction = $client_id;
		}*/
		try {
			$result = API_WorkOrderFilter::filter($companyID,
													$fields,
													$timeStampFrom,
													$timeStampTo,
													$timeStampType,
													$filters, $sort, $limit, $offset, $countRows, $optimize);

        /******** FROM trunk *********
		// fix for sorting text that should be float
		foreach ($sortParts as $key=>$part) {
			$part = trim($part);
			if ($part == "payamount asc" || $part ==     "payamount desc") {
				$p = explode(" ", $part);
				$sortParts[$key] = "(CASE WHEN ISNUMERIC(payamount) = 1 THEN CAST(payamount AS Money) ELSE 99999 END) " . $p[1];
			}
			if ($part == "paymax asc" || $part == "paymax desc") {
				$p = explode(" ", $part);
				$sortParts[$key] = "(CASE WHEN ISNUMERIC(paymax) = 1 THEN CAST(paymax AS Money) ELSE 99999 END) " . $p[1];
			}
			else if ($part == "startdate asc" ||
				$part == "enddate asc") {
				$p = explode(" ", $part);
				$position = $p[1] == "asc" ? "2079-06-06" : "startdate";
				$sortParts[$key] = "(CASE WHEN startdate IS NOT NULL THEN startdate ELSE 999999 END) " . $p[1];
			}
			else if ($part == "status asc" || $part == "status desc") {
				$p = explode(" ", $part);
				$sortParts[$key] = "(" . Core_WorkOrder_Status::getStatusQueryAsName() . ") {$p[1]}";
			}
		} */

			$woArray = API_WorkOrder::makeWorkOrderArray($result->toArray(), new API_WorkOrder(NULL));

		} catch (Exception $e) {
			$errors->addError(7, 'SQL Error: ' . $select . " " . $e);
			error_log($e);
		}
               
		$err = $errors->getErrors();
		if (!empty($err))
			return API_Response::fail();

        $wos = API_Response::success($woArray);
        //--- for additional fields 360
		$wocount = count($wos->data);
		$winList = array();
        for($i=0;$i<$wocount;$i++)
        {
            $winList[] = $wos->data[$i]->WIN_NUM;
		}
		
        if($withAdditionalFields && !empty($result))
        {                        
            for($i=0;$i<$wocount;$i++)
            {
                $wo_id = $wos->data[$i]->WIN_NUM;
                $additionalWO = $this->getWorkOrderAdditionalFields($wo_id);
                if(!empty($additionalWO))
                {
                    $wos->data[$i]->AuditNeeded = $additionalWO['AuditNeeded'];
                    $wos->data[$i]->AuditNeededBy = $additionalWO['AuditNeededBy'];
                    $wos->data[$i]->AuditNeededDate = $additionalWO['AuditNeededDate'];
                    $wos->data[$i]->AuditComplete = $additionalWO['AuditComplete'];
                    $wos->data[$i]->AuditCompleteBy = $additionalWO['AuditCompleteBy'];
                    $wos->data[$i]->AuditCompleteDate = $additionalWO['AuditCompleteDate'];
                    $wos->data[$i]->AllowDeVryInternstoObserveWork = $additionalWO['AllowDeVryInternstoObserveWork'];
                    $wos->data[$i]->AllowDeVryInternstoPerformField = $additionalWO['AllowDeVryInternstoPerformField'];
                    $wos->data[$i]->PushWM = $additionalWO['PushWM'];
                }
                else
                {
                    $wos->data[$i]->AuditNeeded = 0;
                    $wos->data[$i]->AuditNeededBy = '';
                    $wos->data[$i]->AuditNeededDate = null;
                    $wos->data[$i]->AuditComplete = 0;
                    $wos->data[$i]->AuditCompleteBy = '';
                    $wos->data[$i]->AuditCompleteDate = null;
                    $wos->data[$i]->AllowDeVryInternstoObserveWork = null;
                    $wos->data[$i]->AllowDeVryInternstoPerformField = null;
                }  
            }      
            
        }                
        //--- for StartDate, EndDate (13458)
		$Core_Api_WosClass = new Core_Api_WosClass();
		//if ($visits) {
			//$baseWOInfo = $Core_Api_WosClass->getBaseWOVisitInfo($winList);// 14005 remove
		//}
		$numVisits = $Core_Api_WosClass->getNumOfVisits($winList);
		
        for($i=0;$i<$wocount;$i++)
        {
            $wo_id = $wos->data[$i]->WIN_NUM;
			if ($visits) {
				//$recordWOVisit = (!empty($baseWOInfo[$wo_id]) ? $baseWOInfo[$wo_id] : null);                              
                                $recordWOVisit = $Core_Api_WosClass->getBaseWOVisitInfoDisplayrules($wo_id);// 14005
				$wos->data[$i]->StartDate = "";
				$wos->data[$i]->StartTime = "";  
				$wos->data[$i]->EndDate = "";
				$wos->data[$i]->EndTime = "";
				if(!empty($recordWOVisit)){
					$wos->data[$i]->StartDate = $recordWOVisit["StartDate"];
					$wos->data[$i]->StartTime = $recordWOVisit["StartTime"];
					$wos->data[$i]->EndDate = $recordWOVisit["EndDate"];
					$wos->data[$i]->EndTime = $recordWOVisit["EndTime"];
				}
			}
			 $wos->data[$i]->NumOfVisits = $numVisits[$wo_id];
		}

        //--- return
        return $wos;            
	}
	/**
	 * get total of work orders
	 *
	 * @param string $user
	 * @param string $pass
	 * @param string $fields
	 * @param string $timeStampFrom
	 * @param string $timeStampTo
	 * @param string $timeStampType
	 * @param API_WorkOrderFilter $filters
	 * @param int $offset
	 * @param int $limit
	 * @param int $countRows
	 * @param string $sort
	 * @return API_Response
	 */
	public function getTotalWorkOrders($user, $pass, $fields, $timeStampFrom, $timeStampTo, $timeStampType, $filters, $offset = 0, $limit = 0, &$countRows = NULL, $sort = "")
	{
       	$logger = Core_Api_Logger::getInstance();
		$logger->setUrl('getWorkOrders');
        $logger->setLogin($user);
		$logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');

		$authData = array('login'=>$user, 'password'=>$pass);
		$errors = Core_Api_Error::getInstance();

		$user = new Core_Api_User();
        $user->checkAuthentication($authData);

		if (!$user->isAuthenticate())
			return API_Response::fail();
        $companyID = $user->getContextCompanyId();

		$countRows = 0;
                $count = 0;
		try {
			$result = API_WorkOrderFilter::filter($companyID,
													$fields,
													$timeStampFrom,
													$timeStampTo,
													$timeStampType,
													$filters, $sort, $limit, $offset, $countRows);

                        $count = $result->count();

		} catch (Exception $e) {
			$errors->addError(7, 'SQL Error: ' . $select . " " . $e);
			error_log($e);
		}
		$err = $errors->getErrors();
		if (!empty($err))
			return 0;

		return $count;
	}
	
	
	/**
	 * Get list of recent tech for last 12 month ordered by wo counts for company
	 * 
	 * @param string $user
	 * @param string $pass
	 * @return API_Response
	 */
	public function getMyRecentTechIds($user, $pass, $companyID = NULL, $techIds = NULL)
	{
       	$logger = Core_Api_Logger::getInstance();
		$logger->setUrl('getWorkOrders');
        $logger->setLogin($user);
		$logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');

		$authData = array('login'=>$user, 'password'=>$pass);
		$errors = Core_Api_Error::getInstance();

		$user = new Core_Api_User();
        $user->checkAuthentication($authData);

		if ( !$user->isAuthenticate() ) return API_Response::fail();
	    
	    $db = Core_Database::getInstance();
		$intervalArray = array(10, 14, 21, 30);
		foreach ($intervalArray as $days) {
		$select = $db->select();
		$select->from(Core_Database::TABLE_WORK_ORDERS, 
		    array('Tech_ID', 'cnt' => 'COUNT(*)')
		);
		
	
		$select->where('TechPaid = 1 AND Deactivated = 0');
			$select->where('StartDate > ?' , new Zend_Db_Expr("DATE_SUB(NOW(), INTERVAL $days DAY)") );
		if($companyID != NULL){
			$select->where('Company_ID = ?', $companyID );
		}else{
			$select->where('Company_ID = ?', $user->getContextCompanyId() );
		}
		if (is_array($techIds)) {
			if (sizeof($techIds) == 0) return array();
			$select->where('Tech_ID IN (?)', $techIds);
		}
		else
			$select->where('Tech_ID > 0');
		$select->group('Tech_ID');
    		$select->order('cnt DESC')->limit(5);

		$result = new Core_Filter_Result($select);
		$result = $result->toArray();

		$techs = array();
		if (is_array($result) ) {
		    foreach ($result as $item) {
		        $techs[] = $item['Tech_ID'];
		    }
		}
			if (!empty($techs)) break;
		}
		
		return $techs;
	}
	
	
	/**
	 * Get list of all  tech for last 12 month ordered by wo counts
	 * 
	 * @param string $user
	 * @param string $pass
	 * @return API_Response
	 */
    public function getAllRecentTechIds($user, $pass, $techIds = NULL)
	{
       	$logger = Core_Api_Logger::getInstance();
		$logger->setUrl('getWorkOrders');
        $logger->setLogin($user);
		$logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');

		$authData = array('login'=>$user, 'password'=>$pass);
		$errors = Core_Api_Error::getInstance();

		$user = new Core_Api_User();
        $user->checkAuthentication($authData);

		if ( !$user->isAuthenticate() ) return API_Response::fail();
	    
	    $db = Core_Database::getInstance();
		$select = $db->select();
		$select->from(Core_Database::TABLE_WORK_ORDERS, 
		    array('Tech_ID', 'cnt' => 'COUNT(*)')
		);
		
	
		$select->where('TechPaid = 1 AND Deactivated = 0');
		$select->where('StartDate > ?' , new Zend_Db_Expr('DATE_SUB(NOW(), INTERVAL 12 MONTH)') );
		if (is_array($techIds)) {
			if (sizeof($techIds) == 0) return array();
			$select->where('Tech_ID IN (?)', $techIds);
		}
		else
			$select->where('Tech_ID > 0');
		$select->group('Tech_ID');
    	$select->order('cnt DESC');

		$result = new Core_Filter_Result($select);
		$result = $result->toArray();

		$techs = array();
		if (is_array($result) ) {
		    foreach ($result as $item) {
		        $techs[] = $item['Tech_ID'];
		    }
		}
		
		return $techs;
	}

	
	
	/**
	 * Get 12 month tech stats
	 * 
	 * @param string $user
	 * @param string $pass
	 * @param string $techId
	 * @return array
	 */
	public function get12MonthsCallStats($user, $pass, $techId,$companyID='')
	{
		$logger = Core_Api_Logger::getInstance();
		$logger->setUrl('getWorkOrders');
		$logger->setLogin($user);
		$logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');

		$authData = array('login'=>$user, 'password'=>$pass);
		$errors = Core_Api_Error::getInstance();

		$user = new Core_Api_User();
		$user->checkAuthentication($authData);

		if ( !$user->isAuthenticate() ) return API_Response::fail();
		
		if (!is_array($techId)) $techId = array($techId);
	    		
		$cache = Core_Cache_Manager::factory();
				
		$fullResult = array();

		
		$db = Core_Database::getInstance();
        
		$fullResult = array();
		$notCachedId = array();

		$fullResult = Core_TechStats::getStatsWithTechID($techId, "12 MONTH",Core_TechStats::CACHE_ENABLED,$companyID);

		foreach ($techId as $id) {
			if (is_object ($fullResult[$id])) {
				$stats = $fullResult[$id]->toArray();
				$fullResult[$id] = array(
					'imacsMain' => $stats['imac'],
					'serviceMain' => $stats['service'],
					'recommendedAvg' => $stats['preferencePercent'],
					'recommendedTotal' => $stats['recommendedTotal'],
					'performanceAvg' => $stats['performancePercent'],
					'performanceTotal' => $stats['performanceTotal'],
					'backOutMain' => $stats['backOut'],
					'noShowMain' => $stats['noShow']
				);
			}
		}

		//if (sizeof($fullResult) == 1) $fullResult = $fullResult[$techId[0]];

		return $fullResult;
	}
    //--- 13321
    public function getLifeTimeCallStats($user, $pass, $techId,$companyID='')
    {
        $logger = Core_Api_Logger::getInstance();
        $logger->setUrl('getWorkOrders');
        $logger->setLogin($user);
        $logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');

        $authData = array('login'=>$user, 'password'=>$pass);
        $errors = Core_Api_Error::getInstance();

        $user = new Core_Api_User();
        $user->checkAuthentication($authData);

        if ( !$user->isAuthenticate() ) return API_Response::fail();
        
        if (!is_array($techId)) $techId = array($techId);
                
        $cache = Core_Cache_Manager::factory();
                
        $fullResult = array();

        
        $db = Core_Database::getInstance();
        
        $fullResult = array();
        $notCachedId = array();

        $fullResult = Core_TechStats::getStatsWithTechID($techId,'',Core_TechStats::CACHE_ENABLED,$companyID);

        foreach ($techId as $id) {
            if (is_object ($fullResult[$id])) {
                $stats = $fullResult[$id]->toArray();
                $fullResult[$id] = array(
                    'imacsMain' => $stats['imac'],
                    'serviceMain' => $stats['service'],
                    'recommendedAvg' => $stats['preferencePercent'],
                    'recommendedTotal' => $stats['recommendedTotal'],
                    'performanceAvg' => $stats['performancePercent'],
                    'performanceTotal' => $stats['performanceTotal'],
                    'backOutMain' => $stats['backOut'],
                    'noShowMain' => $stats['noShow']
                );
            }
        }

        //if (sizeof($fullResult) == 1) $fullResult = $fullResult[$techId[0]];

        return $fullResult;
    }

    /**
     * get count of WO by status
     * @param string $user
     * @param string $pass
     * @param string $counters
     * @return API_Response
     * @author Artem Sukharev
     */
    public function getWorkOrdersCountByStatus($user, $pass, &$counters)
    {
       	$logger = Core_Api_Logger::getInstance();
		$logger->setUrl('getWorkOrders');
        $logger->setLogin($user);
		$logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');

		$authData = array('login'=>$user, 'password'=>$pass);
		$errors = Core_Api_Error::getInstance();

		$user = new Core_Api_User();
        $user->checkAuthentication($authData);

		if ( !$user->isAuthenticate() ) return API_Response::fail();

        $companyID = $user->getContextCompanyId();

        $filters = new API_WorkOrderFilter;
        $filters->Deactivated = false;

        $counters = array('total' => 0);
        $count = 0;
        try {
        	$result = API_WorkOrderFilter::filter($companyID, 'WIN_NUM', NULL, NULL, NULL, $filters, '', 1 ,0, $count);
        } catch (Exception $e) {
			$errors->addError(7, 'SQL Error: ' . $e);
			error_log($e);
        }
        $counters['total'] = $count;
        //13768
        $states = array(WO_STATE_CREATED, WO_STATE_PUBLISHED, WO_STATE_ASSIGNED, WO_STATE_WORK_DONE, WO_STATE_INCOMPLETE, WO_STATE_COMPLETED);
        foreach ( $states as $state ) {
            $stateFilter = clone $filters;
            if($state==WO_STATE_COMPLETED){
                $stateFilter->Approved = 1;
                $stateFilter->Deactivated = 0;
            } else {
                $stateFilter->WO_State = $state;                
            }
            try {
            	$result = API_WorkOrderFilter::filter($companyID, 'WIN_NUM', NULL, NULL, NULL, $stateFilter, '', 1, 0, $count);
	        } catch (Exception $e) {
				$errors->addError(7, 'SQL Error: ' . $e);
				error_log($e);
	        }
            $counters[$state] = $count;
        }
        //end 13768

        if ( $errors->hasErrors() ) return API_Response::fail();

        return API_Response::success($counters);
    }

	/**
	 * publish work order
	 *
	 * @param string $user
	 * @param string $pass
	 * @param int $winNum
	 * @return API_Response
	 */
	public function publishWorkOrder($user, $pass, $winNum, $additionalFields = array())
	{
       	$logger = Core_Api_Logger::getInstance();
		$logger->setUrl('publishWorkOrder ' . $winNum);
        $logger->setLogin($user);
		$logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');

		$authData = array('login'=>$user, 'password'=>$pass);

		$user = new Core_Api_User();
		$user->checkAuthentication($authData);

		if (!$user->isAuthenticate())
			return API_Response::fail();

		$params = array('TB_UNID'=>$winNum);
		$queryFieldList = array();
		$queryFieldList["ShowTechs"] = "True";
		$queryFieldList["Deactivated"] = "False";
		$queryFieldList["Tech_ID"] = "";
		$queryFieldList["TechMarkedComplete"] = "False";

		foreach ($additionalFields as $key => $value) {
			$queryFieldList[$key] = $value;
		}

		return $this->updateWorkOrderPostXMLParse($user, $params, $queryFieldList);
	}

	/**
	 * assign work order
	 *
	 * @param string $user
	 * @param string $pass
	 * @param int $winNum
	 * @param int $techID
	 * @param float $bidAmount
     * @param string $bidComments
     * @param string $amountPer
     * @param int $qtyDevices
	 * @param boolean $ignoreWarnings
	 * @return API_Response
	 */
 	public function assignWorkOrder($user, $pass, $winNum, $techID, $bidAmount, $bidComments=null,$amountPer=null,$qtyDevices=null, $ignoreWarnings = false, $route = NULL)
	{
		//error_log("HERE ASSIGN!!!");error_log(print_r(func_get_args(),true));
       	$logger = Core_Api_Logger::getInstance();
		$logger->setUrl('assignWorkOrder ' . $winNum);
        $logger->setLogin($user);
		$logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');

		if ($this->user == null) {
			$authData = array('login'=>$user, 'password'=>$pass);

			$user = new Core_Api_User();
			$user->checkAuthentication($authData);

			$this->user = $user;
		}
		else {
			$user = $this->user;
		}

		if (!$user->isAuthenticate())
			return API_Response::fail();

		$this->setMode (WO_UPDATE_MODE_ASSIGN);

		$params = array('TB_UNID'=>$winNum);
		$idField = defined("FLSVersion") && FLSVersion ? "FLS_ID" : "Tech_ID";
		$queryFieldList = array($idField=>$techID, 'Tech_Bid_Amount'=>$bidAmount, 'bid_comments'=>$bidComments);
		if($route != null)$queryFieldList['Route'] = $route;
        if(!empty($amountPer)) {
            $queryFieldList['Amount_Per'] = $amountPer;
            $queryFieldList['Qty_Devices'] = $qtyDevices;
        }
		return $this->updateWorkOrderPostXMLParse($user, $params, $queryFieldList, $ignoreWarnings);
	}

	/**
	 * approve work order
	 *
	 * @param string $user
	 * @param string $pass
	 * @param int $winNum
	 * @return API_Response
	 */
 	public function approveWorkOrder($user, $pass, $winNum, $payAmount = null)
	{
/*  
     	$logger = Core_Api_Logger::getInstance();
		$logger->setUrl('approveWorkOrder ' . $winNum);
        $logger->setLogin($user);
		$logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');
*/
		$authData = array('login'=>$user, 'password'=>$pass);

		$user = new Core_Api_User();
		$user->checkAuthentication($authData);

		if (!$user->isAuthenticate())
			return API_Response::fail();
			
		$params = array('TB_UNID'=>$winNum);
		$queryFieldList = array();
		$queryFieldList["Approved"] = "True";
		$queryFieldList["Invoiced"] = "False";
		$queryFieldList["TechMarkedComplete"] = "True";
		if ($payAmount) {
			$queryFieldList['PayAmount'] = $payAmount;
		}
		return $this->updateWorkOrderPostXMLParse($user, $params, $queryFieldList);
	}

	/**
	 * deactivate work order
	 *
	 * @param string $user
	 * @param string $pass
	 * @param int $winNum
	 * @param string $code
	 * @param string $reason
	 * @return API_Response
	 */
 	public function deactivateWorkOrder($user, $pass, $winNum, $code, $reason = "")
	{
       	$logger = Core_Api_Logger::getInstance();
		$logger->setUrl('deactivateWorkOrder ' . $winNum);
        $logger->setLogin($user);
		$logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');

		$authData = array('login'=>$user, 'password'=>$pass);

		$user = new Core_Api_User();
		$user->checkAuthentication($authData);

		if (!$user->isAuthenticate())
			return API_Response::fail();

		$params = array('TB_UNID'=>$winNum);
		$queryFieldList = array();
		$queryFieldList["Deactivated"] = "True";
		$queryFieldList["DeactivationCode"] = $code;
		$queryFieldList["Deactivated_Reason"] = $reason;
		return $this->updateWorkOrderPostXMLParse($user, $params, $queryFieldList);
	}

	/**
	 * mark work order incomplete
	 *
	 * @param string $user
	 * @param string $pass
	 * @param int $winNum
	 * @param string $comments
	 * @return API_Response
	 */
 	public function markWorkOrderIncomplete($user, $pass, $winNum, $comments)
	{
       	$logger = Core_Api_Logger::getInstance();
		$logger->setUrl('markWorkOrderIncomplete ' . $winNum);
        $logger->setLogin($user);
		$logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');

		$authData = array('login'=>$user, 'password'=>$pass);

		$user = new Core_Api_User();
		$user->checkAuthentication($authData);

		if (!$user->isAuthenticate())
			return API_Response::fail();

		$params = array('TB_UNID'=>$winNum);
		$queryFieldList = array();
		$queryFieldList["TechMarkedComplete"] = "False";
		$queryFieldList["MissingComments"] = $comments;
		$queryFieldList["Approved"] = "False";
		return $this->updateWorkOrderPostXMLParse($user, $params, $queryFieldList);
	}

	/**
	 * get work orders using win num
	 *
	 * @param int $winNum
	 * @param string $fields
	 * @param string $companyId
	 * @param string $sort
	 * @access protected
	 * @return API_Response
	 */
	public function getWorkOrdersWithWinNum($winNum, $fields, $sort, $companyId = "")
	{
		if (is_numeric($winNum)) $winNum = array($winNum);
		if (!is_array($winNum)) return false;
		if (!empty($fields)) {
			$fieldList = str_replace(' ', '', $fields);
			$fieldListParts = explode(",", $fieldList);
		}
		else
			$fieldListParts = Core_Database::getFieldList(Core_Database::TABLE_WORK_ORDERS);

		$fieldListMap = array_combine($fieldListParts, $fieldListParts);

/*		if (!empty($fieldListMap["Status"]) || empty($fields))
			$fieldListMap["Status"] = Core_WorkOrder_Status::getStatusQueryAsName();*/

		$db = Core_Database::getInstance();
		$select = $db->select();

		$select->from(Core_Database::TABLE_WORK_ORDERS, $fieldListMap )
			->where("WIN_NUM IN (?)", $winNum)
			->order($sort);

		if (!empty($companyId)) {
			$select->where("Company_ID = ?", $companyId);
		}
		$result = Core_Database::fetchAll($select);

		return $result;
	}

	/**
	 * Get Work Order
	 *
	 * @param string $user
	 * @param string $pass
	 * @param int $winNum
	 * @return API_Response
	 */
	public function getWorkOrder($user, $pass, $winNum,$withAdditionalFields=false)
	{
       	$logger = Core_Api_Logger::getInstance();
		$logger->setUrl('getWorkOrder ' . $winNum);
        $logger->setLogin($user);
		$logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');

		$authData = array('login'=>$user, 'password'=>$pass);
		$errors = Core_Api_Error::getInstance();
		
		if (empty($winNum))
			$errors->addError(2, 'Work Order ID parameter is empty but required');

		$err = $errors->getErrors();
		if (!empty($err))
			return API_Response::fail();

		$wo_id = (int)$winNum;

		$user = new Core_Api_User();
		$user->checkAuthentication($authData);

		if (!$user->isAuthenticate())
			return API_Response::fail();

		$fieldList = Core_Database::getFieldList(Core_Database::TABLE_WORK_ORDERS);
		$fieldList = array_combine($fieldList, $fieldList);
		$fieldList["UpdatedPartCount"] = "(SELECT COUNT(*) FROM part_entries WHERE WinNum = $winNum AND TechIdLastUpd <> 0)";
        //--- 13566
        
        //---  13622, 13713,13728
        $wofFields =  array('AuditNeeded','AuditNeededBy','AuditNeededDate','AuditComplete','AuditCompleteBy','AuditCompleteDate',
							'EntityTypeId','ServiceTypeId','FSClientServiceDirectorId','FSAccountManagerId',
							'AllowDeVryInternstoObserveWork','AllowDeVryInternstoPerformField',
							'ACSNotifiPOC','ACSNotifiPOC_Phone','ACSNotifiPOC_Email',
							'SiteContactBackedOutChecked','NotifiedTime',
							'PushWM'
		);
        
		$db = Core_Database::getInstance();
		$select = $db->select();
		$select->from(array('w'=>Core_Database::TABLE_WORK_ORDERS, $fieldList));
        $select->joinLeft(array('waf'=>'work_orders_additional_fields'),
                'waf.WINNUM = w.WIN_NUM',
                $wofFields
        );        
		$select->where("w.WIN_NUM = ?", $wo_id);
		$select->where("w.Company_ID = ?", $user->getContextCompanyId());

		$result = Core_Database::fetchAll($select);

		$wosClass = new Core_Api_WosClass();
		$visitFields = array('WoVisitID', 
			'StartTypeID', 
			'StartDate', 
			'StartTime', 
			'EndDate', 
			'EndTime', 
			'EstimatedDuration',
			'TechArrivalInstructions',
			'Type',
			'TechCheckInDate',
			'TechCheckInTime',
			'TechCheckOutDate',
			'TechCheckOutTime',
			'CORCheckInDate',
			'CORCheckInTime',
			'CORCheckOutDate',
			'CORCheckOutTime',
			'OptionCheckInOut',
			'VisitDisable'
			);
		$visitList = $wosClass->GetAllWOVisitList($wo_id, 0, 0, $visitFields);

		$result[0]['__Visits'] = $visitList;

    	$err = $errors->getErrors();
		if (!empty($err))
			return API_Response::fail();

		$wos = API_Response::success(API_WorkOrder::makeWorkOrderArray($result, new API_WorkOrder(NULL)));
        

        //--- return 
        return $wos;
	}

	/**
	 * get win num
	 *
	 * @param string $user
	 * @param string $pass
	 * @param string $woID
	 * @return API_Response
	 */
	public function getWinNum($user, $pass, $woID)
	{
       	$logger = Core_Api_Logger::getInstance();
		$logger->setUrl('getWinNum ' . $woID);
        $logger->setLogin($user);
		$logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');

		$authData = array('login'=>$user, 'password'=>$pass);

		$user = new Core_Api_User();
		$user->checkAuthentication($authData);

		if (!$user->isAuthenticate())
			return API_Response::fail();

		$winNum = $this->retrieveWinNum($woID, $user->getContextCompanyId());

		if (empty($winNum))
		{
			$errors = Core_Api_Error::getInstance();
			$errors->addError(6, 'No such client work order number');
			return API_Response::fail();
		}

		return API_Response::success(array(new API_WorkOrder($winNum)));
	}
	
	public function getLastWOBidComment($user, $pass, $techId, $woId, $fields=null)
	{
       	$logger = Core_Api_Logger::getInstance();
		$logger->setUrl('getWorkOrders');
        $logger->setLogin($user);
		$logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');

		$authData = array('login'=>$user, 'password'=>$pass);
		$errors = Core_Api_Error::getInstance();

		$user = new Core_Api_User();
        $user->checkAuthentication($authData);

		if (!$user->isAuthenticate())
			return API_Response::fail();
        /*
        $companyID = $user->getContextCompanyId();
        
        $db = Core_Database::getInstance();
        
        //serviceMain
		$select = $db->select()->from(Core_Database::TABLE_WORK_ORDERS, array('Tech_Bid_Amount', 'bid_comments') )

               ->where('Company_ID = ?', $companyID)
               ->where('DateEntered >= ?' , new Zend_Db_Expr('DATE_SUB(NOW(), INTERVAL 6 MONTH)') )
		       ->where('Tech_Bid_Amount IS NOT ?' , new Zend_Db_Expr('NULL') )
		       ->order('DateEntered DESC')
		       ->limit(1);
        $result = $db->fetchRow($select);
        return $result;
        */

        $bidData = $this->pullBidData($techId, $woId,$fields);
        return $bidData;

	}
	
	private function baseVisitFromWorkOrder($woData) {
		// pull visit fields out of work order info to be saved after wo create / update
		// make sure to set multivisit to Core_Combiner_WorkOrder::COMBINER_MULTIVISIT_OFF
		$visitFields = array(
			"Duration",
			"TechArrivalInstructions",
			"StartType",
			"StartDate",
			"StartTime",
			"EndDate",
			"EndTime"			
		);
		$baseVisit = array();
		foreach ($visitFields as $field) {
			if (!array_key_exists($field, $woData)) continue;
			$baseVisit[$field] = empty($woData[$field]) ? NULL : $woData[$field];
		}
		if (!empty($woData["Duration"]) && empty($woData["EstimatedDuration"]) && is_numeric($woData["Duration"])) {
			// map duration to visit estimated duration for calls coming api
			$baseVisit["EstimatedDuration"] = $woData["Duration"];
		}
		else if (!empty($woData["EstimatedDuration"]))
			$baseVisit["EstimatedDuration"] = $woData["EstimatedDuration"];
		return $baseVisit;
	}
	
	private function baseVisitRemoveFields($woData) {
		// unset wo fields which are stored in visits table only
		$visitFields = array(
			"EstimatedDuration",
			"TechArrivalInstructions",
			"StartType",
		);
		foreach ($visitFields as $field) {
			unset($woData[$field]);
		}
		return $woData;
	}

	private function baseVisitSave($winNum, $baseVisit) {
		if (empty($baseVisit)) return;
		// create base visit
		$wosClass = new Core_Api_WosClass();
		$base = $wosClass->getBaseWOVisitInfo_ByWinnum($winNum);
		$wosClass->connectWOVisitToWorkOrder();

		foreach ($base as $field=>$value) {
			if (isset($baseVisit[$field])) continue;
			$baseVisit[$field] = $value;
		}
		if (empty($baseVisit['StartType'])) $baseVisit['StartType'] = $baseVisit['StartTypeID'];

		$wosClass->saveWOVisit(
			$base['WoVisitID'],
			$winNum,
			empty($baseVisit['StartType']) ? 1 : $baseVisit['StartType'],
			!empty($baseVisit['StartDate'])?date_format(new DateTime($baseVisit['StartDate']), "Y-m-d"):"",
			$baseVisit['StartTime'],
			!empty($baseVisit['EndDate'])?date_format(new DateTime($baseVisit['EndDate']), "Y-m-d"):"",
			$baseVisit['EndTime'],
			$baseVisit['EstimatedDuration'],
			$baseVisit['TechArrivalInstructions']);
	}


	private function retrieveWinNum($woID, $companyID)
	{
		$db = Core_Database::getInstance();
		$select = $db->select();

		$select->from(Core_Database::TABLE_WORK_ORDERS, "WIN_NUM")
			->where("WO_ID = ?", $woID)
			->where("Company_ID = ?", $companyID);

		$result = Core_Database::fetchAll($select);

		return empty($result[0]['WIN_NUM']) ? NULL : $result[0]['WIN_NUM'];
	}

    private function blankIfInvalidDate($date) {
        if (empty($date)) return '';
        if (strtotime($date) === FALSE) return null;
        return $date;
    }

	private function pullAssignTechInfo($user, $id, $isFLS = FALSE) {
		$fieldListMap = array(
					"TechID" => "TechID",
					"AcceptTerms" => "AcceptTerms",
					"FirstName" => "FirstName",
					"LastName" => "LastName",
					"PrimaryEmail" => "PrimaryEmail",
					"PrimaryPhone" => "PrimaryPhone",
					"PrimaryPhoneExt" => "PrimaryPhoneExt",
					"PrimaryPhoneExt" => "PrimaryPhoneExt",
					"FLS_ID" => "FLSID",
					"Deactivated" => "Deactivated",
					"WMID" => "WMID"
				);

		$id = Core_Caspio::caspioEscape($id);
		$criteria = $isFLS ? "FLSID = '$id'" : "TechID = '$id'";
		if ($isFLS && is_numeric($id)) {
			$criteria .= " OR TechID = '$id'";
		}
		$criteria .= " AND Deactivated = '0'";

		$db = Core_Database::getInstance();
                $select = $db->select();
                $select->from(Core_Database::TABLE_TECH_BANK_INFO, $fieldListMap)
                        ->where($criteria);
                $result = $db->fetchAll($select);
		
		if (!$result) return $result;
		$result[0]['IsAdminDenied'] = Core_Tech::isClientDenied($id, $user->getContextCompanyId()) ? $id : '';
		return $result;
	}

    public function getBids($user, $pass, $winNum, $offset = 0, $limit = 0, $showHidden = false, $criteria = NULL) {
		$authData = array('login'=>$user, 'password'=>$pass);
		$errors = Core_Api_Error::getInstance();

		$user = new Core_Api_User();
        $user->checkAuthentication($authData);

		if (!$user->isAuthenticate())
			return API_Response::fail();

		$companyID = $user->getContextCompanyId();

        if (empty($winNum)) return API_Response::success(array());
		if (empty($criteria)) $criteria = array();
        $db = Core_Database::getInstance();
        $s = $db->select();
		$preferredList = Core_Tech::getClientPreferredTechsArray($companyID);
		$preferred = (empty($preferredList)) ? new Zend_Db_Expr('NULL') : '(CASE WHEN `b`.TechID IN (' . implode(',', $preferredList) . ') THEN 1 ELSE NULL END)';
		$techFields = array('FirstName', 'LastName', 'PreferencePercent', 'SATRecommendedTotal', 'PerformancePercent', 'SATPerformanceTotal', 'Bg_Check' => 'Bg_Test_Pass_Lite', 'Bg_Check_Date' => 'BG_Test_ResultsDate_Lite', 'Back_Outs' => 'Back_Outs', 'No_Shows' => 'No_Shows');
		if ($companyID == 'FLS')
			$techFields[] = 'FLSID';
		$s->from(array('b' => Core_Database::TABLE_WORK_ORDER_BIDS), array(
			'WIN_NUM' => 'WorkOrderID', 
			'TechID' => 'TechID', 
			'BidAmount' => 'BidAmount',
			'Comments' => 'Comments',
			'Bid_Date' => 'Bid_Date',
			'Preferred' => $preferred
		))
		->join(array('t' => Core_Database::TABLE_TECH_BANK_INFO), '`b`.TechID = `t`.TechID', $techFields)
		->where('WorkOrderID = ?', $winNum)
		->where('`t`.Deactivated = ?', 0)
		->where('`t`.AcceptTerms = ? OR `t`.AcceptTerms = ?', 'Yes', 'WM')
		->order("$preferred DESC");

		if ($companyID == 'FLS' && !empty($criteria['showFLSOnly']) && $criteria['showFLSOnly'] === true)
			$s->where('`t`.FLSID IS NOT NULL');

		if ($offset != 0 || $limit != 0)
			$s->limit($limit, $offset);

		if (!$showHidden) {
			$s->where('Hide = ?', 0);
			$deniedList = Core_Tech::getClientDeniedTechsArray($companyID);
			if (!empty($deniedList))
			    $s->where('`b`.TechID NOT IN (?)', $deniedList);
		}

		$result = $db->fetchAll($s);
		if (!$result) return API_Response::success(array());
		foreach ($result as &$v) {
			if (empty($v['PreferencePercent'])) $v['PreferencePercent'] = 0;
			else $v['PreferencePercent'] = intval($v['PreferencePercent']);
			if (empty($v['SATRecommendedTotal'])) $v['SATRecommendedTotal'] = 0;
			else $v['SATRecommendedTotal'] = intval($v['SATRecommendedTotal']);
			if (empty($v['PerformancePercent'])) $v['PerformancePercent'] = 0;
			else $v['PerformancePercent'] = intval($v['PerformancePercent']);
			if (empty($v['SATPerformanceTotal'])) $v['SATPerformanceTotal'] = 0;
			else $v['SATPerformanceTotal'] = intval($v['SATPerformanceTotal']);
			$v['Bg_Check'] = $v['Bg_Check'] == 'Pass';
			$v['Preferred'] = $v['Preferred'] == 1;
			$v['Bg_Check'] = $v['Bg_Check'] == 'Pass';
		}		
		return API_Response::success($result);
	}

    private function pullBidData($techId, $woId, $fields = null){
        if(empty($techId) || empty($woId)) return NULL;
        $db = Core_Database::getInstance();
        $s = $db->select();
        if(is_array($fields)) $s->from(Core_Database::TABLE_WORK_ORDER_BIDS, $fields);
        else $s->from(Core_Database::TABLE_WORK_ORDER_BIDS);
        $s->where('TechID = ?', $techId)
          ->where('WorkOrderID = ?', $woId)
          ->order('Bid_Date DESC');
        return $db->fetchRow($s);
    }
    
    public function copyBidData($origWinNum, $newWinNum){
		
        if(empty($origWinNum) || empty($newWinNum)) return NULL;
        $db = Core_Database::getInstance();
        $s = $db->select();
       	$s->from(Core_Database::TABLE_WORK_ORDER_BIDS);
        $s->where('WorkOrderID = ?', $origWinNum)
          ->order('Bid_Date DESC');
        
    	try {
			$result = $s->query()->fetchAll();
			$countRows = sizeof($result);
			$objDateNow = new Zend_Date();
    		foreach ($result as $bid) {
    			$bid['id'] = "";
    			$bid['BidModifiedDate'] = $objDateNow->toString('yyyy-MM-dd HH:mm:ss');
    			$bid['WorkOrderID'] = $newWinNum;
    			Core_Database::insert(Core_Database::TABLE_WORK_ORDER_BIDS, $bid);
    		}
			
		} catch (Exception $e) {
            return API_Response::fail();
        }
    }
    
    
	/**
	 * copy Work Order
	 *
	 * @param string $user
	 * @param string $pass
	 * @param boolean $publish
	 * @param API_WorkOrder $workOrder
	 * @param boolean $ignoreWarnings
	 * @return API_Response
	 */
    
    public function copyWorkOrder($user, $pass, $publish, $workOrder, $ignoreWarnings = false, $copyToCompanyId = "", $copyToProjectId = "")
	{	
		//look up any wo's that may have been copies of same WIN_NUM 
		$db = Core_Database::getInstance();
		$s = $db->select();
		$s->from(Core_Database::TABLE_WORK_ORDERS, array("WO_ID"));
		$Z2TCopy = $copyToCompanyId == 'Z2T' && $workOrder->Company_ID != $copyToCompanyId;
		if ($Z2TCopy) {
			$workOrder->PO = $workOrder->Company_ID . $workOrder->WIN_NUM;
			$s->where('WO_ID LIKE ?', '%'.mysqli_real_escape_string($workOrder->WO_ID).'z%');
		}
		else {
			if(strpos($workOrder->WO_ID,"COPY")){
				$s->where('WO_ID LIKE ?', "%".mysqli_real_escape_string($workOrder->WO_ID)."%");
			}else{
				$s->where('WO_ID LIKE ?', "%".mysqli_real_escape_string($workOrder->WO_ID)." COPY%");
			}
		}		
		
		if(!empty($copyToCompanyId)){
			$s->where('Company_ID = ?', $copyToCompanyId);
		}else{
			$pmContextPos = strpos($user, "=");
			$companyID = substr($user,$pmContextPos+1);
			$s->where('Company_ID = ?', $companyID);
		}
				
		$s->order('WIN_NUM DESC')
		->limit(1,0);
		
    $woapi = new Core_Api_WosClass;
		
	if($row = $db->fetchRow($s))
    {
        $copyAppend = "";
		if ($Z2TCopy) 
        {
				preg_match("/((?:.(?!z(?:\s\d+)*$))+.)z(?:\s(\d+))*$/", $row['WO_ID'], $parts);
				$copyString = $parts[0];
				$copyNum = empty($parts[1]) ? 0 : $parts[1];
				++$copyNum;
				$copyAppend = "z " . $copyNum;
		} 
        else 
        {   
            //13996, 14158     
            $originalWoID = $workOrder->WO_ID;  
            $realName = "";
            
            $pos = strpos($originalWoID,' COPY ');            
            if($pos===false){
                $copyNum = 0;                       
                $realName = $originalWoID;
            } else {
                $parts = explode(' COPY ',$originalWoID);
                $realName = $parts[0];
                $copyNum = empty($parts[1]) ? 0 : $parts[1];
            }
            $copyNum++;
            
            //print_r($parts);
            //echo('<br/>realName: '.$realName);
            //echo('<br/>copyNum: '.$copyNum);
            //die();            
            $woID = $realName." COPY $copyNum" ;
            while ($woapi->exists_ClientWorkOrderID($woID)){
               $copyNum ++;
               $woID = $realName." COPY $copyNum" ;
            }
            
            $workOrder->WO_ID =$woID;
            //end 13996, 14158
		}
              
	}  else  {
       //13996
	   if ($Z2TCopy) {
	        $workOrder->WO_ID = $workOrder->WO_ID."z";
       } else {
            $originalWoID = $workOrder->WO_ID;  
            $realName = "";
            
            $pos = strpos($originalWoID,' COPY ');            
            if($pos===false){
                $copyNum = 0;                       
                $realName = $originalWoID;
            } else {
                $parts = explode(' COPY ',$originalWoID);
                $realName = $parts[0];
                $copyNum = empty($parts[1]) ? 0 : $parts[1];
            }
            $copyNum++;
            
            $woID = $realName." COPY $copyNum" ;
            while ($woapi->exists_ClientWorkOrderID($woID)){
               $copyNum ++;
               $woID = $realName." COPY $copyNum" ;
            }
            
            $workOrder->WO_ID =$woID;
       }
       //end 13996
    }

		if(!empty($copyToCompanyId)){
	        try{
	        	$select = $db->select();
		        $coNameSel = $select->from(Core_Database::TABLE_CLIENTS, array("CompanyName"))
						->distinct()
						->where('Company_ID = ?', $copyToCompanyId)
						->limit(1,0);
				$row = $db->fetchRow($coNameSel);
		        $workOrder->Company_ID = $copyToCompanyId;
		        $workOrder->Company_Name = $row['CompanyName'];		        
	        }catch(Exception $e){
	        	error_log($e);
	        }
        }
        
        
        if(!empty($copyToProjectId)){
        try{
	        	$select = $db->select();
		        $projNameSel = $select->from(Core_Database::TABLE_PROJECTS, array("Project_Name", "PcntDeductPercent"))
						->distinct()
						->where('Project_ID = ?', $copyToProjectId)
						->limit(1,0);
				$row = $db->fetchRow($projNameSel);
		        $workOrder->Project_ID = $copyToProjectId;
		        $workOrder->Project_Name = $row['Project_Name'];
		        $workOrder->PcntDeductPercent = $row['PcntDeductPercent'];	        
	        }catch(Exception $e){
	        	error_log($e);
	        }
        }
        
		try{
			$return = $this->createWorkOrder($user,$pass,$publish,$workOrder,$ignoreWarnings);
			if ($return->success && !empty($return->data[0]->WIN_NUM )) {
				// copy wo visits since start date was passed
				$woapi = new Core_Api_WosClass;
				$woapi->copyWOVisits($workOrder->WIN_NUM, $return->data[0]->WIN_NUM, empty($workOrder->StartDate), $workOrder->Qty_Visits);
                //13955
                if(empty($workOrder->StartDate)){
                    $woapi->updateBaseVisit_TechArrivalInstructions($workOrder->WIN_NUM,$return->data[0]->WIN_NUM);
		}
                //end 13955
			}
			
        }catch(Exception $e){
			error_log($e);
        }
		return $return;
		
	}
	
	public function getGlobalPmCompanies($login, $pass){
		
		$user = new Core_Api_User;
		$authData = array('login'=>$login, 'password'=>$pass);
		$user->checkAuthentication($authData);

		$returnArr = array();
		if ($user->isPM()) {
			$db = Core_Database::getInstance();
			$select = $db->select();
			$select->from(Core_Database::TABLE_CLIENTS, array("CompanyName", "Company_ID"))
				->distinct()
				->order(array("CompanyName"));
			$companyList = $db->fetchPairs($select);
	
			$companyHtml = "";
			$returnArr['companies'] = $companyList;
			$returnArr["isPM"] = "true";
		}else{
			$returnArr[] = array("isPM"=>"false");
		}
		
		return $returnArr;
	}
	
	public function getGlobalPmProjects($companyId, $defaultWin = ""){

		$returnArr = array();
		
		$db = Core_Database::getInstance();
		$select = $db->select();
		$select->from(Core_Database::TABLE_PROJECTS, array("Project_Name", "Project_ID"))
			->where("Project_Company_ID = ?",$companyId)
			->where("Active = '1'")
			->order(array("Project_ID"));
		$projectList = $db->fetchPairs($select);
		$returnArr['projects'] = $projectList;
		
		//get default project
		if(!empty($defaultWin)){
			$defSelect = $db->select();
			$defSelect->from(Core_Database::TABLE_WORK_ORDERS, array("Project_ID"))
				->where("WIN_NUM = ?", $defaultWin);
			
			$row = $db->fetchRow($defSelect);
			$returnArr['defaultProject'] = $row['Project_ID'];
		}
		
		return $returnArr;
	}

	/**
	 * create Work Order
	 *
	 * @param string $user
	 * @param string $pass
	 * @param boolean $publish
	 * @param API_WorkOrder $workOrder
	 * @param boolean $ignoreWarnings
	 * @return API_Response
	 */
	public function createWorkOrder($user, $pass, $publish, $workOrder, $ignoreWarnings = false)
	{
		
		$benchmark  = Core_Benchmark::getInstance();

		$benchmark->point('Method: "createWorkOrder"; Start log method call');
		if (is_string($workOrder)) $woXML = $workOrder;
		else {
			$dom = new DOMDocument("1.0");
			$dom->appendChild($workOrder->toXMLElement($dom));
			$woXML = $dom->saveXML();
		}

		$ignoreWarnings = $ignoreWarnings === true;

       	$logger = Core_Api_Logger::getInstance();
		$logger->setUrl('createWorkOrder');
        $logger->setLogin($user);
		$logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');

		$authData = array('login'=>$user, 'password'=>$pass);
		$params = array('xml'=>$woXML);
		$errors = Core_Api_Error::getInstance();

		if (empty($params['xml']))
			$errors->addError(2, 'XML parameter is empty but required');

		$err = $errors->getErrors();
		if (!empty($err))
			return API_Response::fail();

		$benchmark->point('Auth');
		$user = new Core_Api_User();
		$user->checkAuthentication($authData);
		$benchmark->point('Auth Done');

		if (!$user->isAuthenticate())
			return API_Response::fail();

        $woData = $this->getDataFromXML($params['xml']);
		//848

		$workOrderAdditionalFieldsTemp = array(
			'ServiceTypeId' => $woData['ServiceTypeId'], 
			'EntityTypeId' => $woData['EntityTypeId'],
			'FSClientServiceDirectorId' => $woData['FSClientServiceDirectorId'],
			'FSAccountManagerId' => $woData['FSAccountManagerId'],
			'ACSNotifiPOC' => $woData['ACSNotifiPOC'],
			'ACSNotifiPOC_Phone' => $woData['ACSNotifiPOC_Phone'],
			'ACSNotifiPOC_Email' => $woData['ACSNotifiPOC_Email'],
			'PushWM' => $woData['PushWM']
		);
        unset($woData['Notifiedby']);
        unset($woData['DateNotified']);
        unset($woData['NotifiedTime']);

		// save visit field for use creating visits
		$extraWoData['Visits'] = $woData['__Visits'];
		unset($woData['__Visits']);
        //end 848
        $this->excludeFields($woData);
		
		$fieldListType = Core_Database::getFieldListType(Core_Database::TABLE_WORK_ORDERS);

		$woData = Core_Database::convertQueryResult($woData, $fieldListType);		

		foreach ($workOrderAdditionalFieldsTemp as $f => $v) {
			$woData[$f] = $v;
		}
		//unset($woData['PushWM']);

        $woData['ShowTechs'] = ($publish === TRUE ? "True" : "False");
         
        if($user->isPM() && !empty($workOrder->Company_ID) && !empty($workOrder->Company_Name) ){
			$woData['Company_ID']   = $workOrder->Company_ID;
			$woData['Company_Name'] = $workOrder->Company_Name;
        }else{
        	$woData['Company_ID']   = $user->getContextCompanyId();
			$company = $user->getContextCompanyId();
			$woData['Company_Name'] = $user->getContextCompanyName();
        }
       
		$payFieldUpdated = false;
		$payFields = Core_TechPay::recalcFields();
		$payFields = array_combine($payFields, $payFields);
		foreach ($woData as $key=>$val) {
			if (array_key_exists($key, $payFields))
				$payFieldUpdated = true;
		}

        if ( ((!empty($woData['TechMarkedComplete']) && $woData['TechMarkedComplete'] == "True") || (!empty($woData['Approved']) && $woData['Approved'] == "True")) && (empty($woData['PayAmount']) || $woData['PayAmount'] == 0.00 || $payFieldUpdated)) {
			$techPayCalculator = new Core_TechPay();
			$woData = $techPayCalculator->updateTechPay($woData);
		}

		$benchmark->point('Pull assign if needed');
		if (!empty($woData["FLS_ID"]))
			$techData = $this->pullAssignTechInfo($user, $woData["FLS_ID"], true);
		else if (!empty($woData["Tech_ID"]))
			$techData = $this->pullAssignTechInfo($user, $woData["Tech_ID"], false);

		$benchmark->point('Pull assign done');

		if (!empty($techData[0])) {
			$techData = $techData[0];
			$woData["Tech_ID"] = $techData["TechID"];
			$woData["Tech_FName"] = $techData["FirstName"];
			$woData["Tech_LName"] = $techData["LastName"];
			$woData["TechEmail"] = $techData["PrimaryEmail"];
			$woData["TechPhone"] = $techData["PrimaryPhone"];

			if (!empty($techData["PrimaryPhoneExt"]))
				$woData["TechPhoneExt"] = $techData["PrimaryPhoneExt"];
			else
				$woData["TechPhoneExt"] = NULL;
			if (!$ignoreWarnings && !empty($techData["IsAdminDenied"])) {
		        	$errors->addError(2, "You are about to assign work to a technician that has been \"denied access\" by your company. Please re-confirm your choice of technician. Tech ID: " . $woData["Tech_ID"] . " (" . $woData["Tech_FName"] . " " . $woData["Tech_LName"] . ")");
				return API_Response::fail();
			}
			if (!empty($techData["FLS_ID"]) && $techData["FLS_ID"] != 'NULL')
				$woData["FLS_ID"] = trim($techData["FLS_ID"] );
			else {
				$woData["FLS_ID"] = NULL;
				if ($woData['Company_ID'] == "FLS") {
					$errors->addError(2, "You are about to assign work to a technician that does not have a FLS Badge. Please re-confirm your choice of technician. Tech ID: " . $woData["Tech_ID"] . " (" . $woData["Tech_FName"] . " " . $woData["Tech_LName"] . ")");
					return API_Response::fail();
				}
			}
			if (strtolower($techData["AcceptTerms"]) != "yes" && ($techData["AcceptTerms"] != 'WM' || $workOrderAdditionalFieldsTemp['PushWM'] != 1)) {
	        		$errors->addError(2, "The technician you're attempting to assign has not accepted terms of usage.  Sorry for any issues this may cause you. Tech ID: " . $woData["Tech_ID"] . " (" . $woData["Tech_FName"] . " " . $woData["Tech_LName"] . ")");
				return API_Response::fail();
			}
			$WMClass = new Core_WorkMarket($wo_id);
			if ($workOrderAdditionalFieldsTemp['PushWM'] == 1 && $techData["AcceptTerms"] != 'WM' && !$WMClass->prepareToAssignInFS()) {
        		$errors->addError(2, "This work order was already assigned in Work Market");
				return API_Response::fail();
			}			
		}
		else {
			$woData['Tech_ID'] = NULL;
            $woData['Date_Assigned'] = "";
			$woData["Tech_FName"] = "";
			$woData["Tech_FName"] = "";
			$woData["Tech_LName"] = "";
			$woData["TechEmail"] = "";
			$woData["TechPhone"] = "";
			$woData["Tech_Bid_Amount"] = NULL;
			$woData["FLS_ID"] = NULL;
		}

		if (!$user->isPM()) {
			unset($woData["PcntDeduct"]);
        }

//		if (empty($woData["Project_ID"]) && empty($woData["Project_Name"]))
//                        $woData["Project_Name"] = "Default Project";

		$woData["Username"] = $user->getLogin();
		$benchmark->point('Combine Project Info');
		$combiner = new Core_Combiner_WorkOrder($woData, Core_Combiner_WorkOrder::COMBINER_MULTIVISIT_ON, Core_Combiner_WorkOrder::COMBINER_MODE_MORE_DATA);
		$combiner->setCompanyId($woData['Company_ID']);
		$combiner->setCompanyName($woData['Company_Name']);
		$combiner->setUserName($workOrder->WorkOrderOwner);
		if ($combiner->combine()) {
			$woData = $combiner->getData();
		} else {
		    $errors->addErrors('6', $combiner->getErrors());
			return API_Response::fail();
		}
		$benchmark->point('Combiner Project Info Done');
		
		if (!empty($woData['Duration']) && empty($woData['EstimatedDuration']) && is_numeric($woData['Duration'])) {
			// map Duration field to EstimatedDuration for calls going through api #13458
			$woData['EstimatedDuration'] = $woData['Duration'];
		}
		$baseVisit = $this->baseVisitFromWorkOrder($woData);
		$woData = $this->baseVisitRemoveFields($woData);
		$customSignOff = $woData['CustomSignOff'];
		$workOrderAdditionalFields = array(
			'ServiceTypeId' => $woData['ServiceTypeId'], 
			'EntityTypeId' => $woData['EntityTypeId'],
			'FSClientServiceDirectorId' => $woData['FSClientServiceDirectorId'],
			'FSAccountManagerId' => $woData['FSAccountManagerId'],
			'ACSNotifiPOC' => $woData['ACSNotifiPOC'],
			'ACSNotifiPOC_Phone' => $woData['ACSNotifiPOC_Phone'],
			'ACSNotifiPOC_Email' => $woData['ACSNotifiPOC_Email'],
			'PushWM' => $woData['PushWM']
		);
		if ($workOrderAdditionalFields["PushWM"] == "True") $workOrderAdditionalFields["PushWM"] = 1;

		$woData = Core_Combiner_WorkOrder::removeMoreDataFields($woData);

		if (!empty($woData['Tech_ID']) && !empty($woData["Project_ID"])) {
			$projectInfo = new Core_Project((int)$woData["Project_ID"]);
			$projectInfo->setData();
			if (strtoupper($projectInfo->getFixedBid()) == "TRUE")
				$woData["Tech_Bid_Amount"] = $woData["PayMax"];
		}

		if (!empty($woData['Approved']) && $woData['Approved'] != "True" && $woData['Approved'] != "False")
			$woData["Approved"] = "False";
		if (!empty($woData['Invoiced']) && $woData['Invoiced'] != "True" && $woData['Invoiced'] != "False")
			$woData["Invoiced"] = "False";
		if (!empty($woData['TechPaid']) && $woData['TechPaid'] != "True" && $woData['TechPaid'] != "False")
			$woData["TechPaid"] = "False";

		if (array_key_exists("StartDate", $woData))
			$woData["StartDate"] = $this->blankIfInvalidDate($woData["StartDate"]);
		if (array_key_exists("EndDate", $woData))
			$woData["EndDate"] = $this->blankIfInvalidDate($woData["EndDate"]);

		$benchmark->point('Validation');
		$factory = new Core_Validation_Factory(Core_Validation_Factory::VALIDATION_TYPE_CREATE);
	    
        $requiredFields = $woData;    
        $Country = Core_Country::getCountryInfoByCode($requiredFields["Country"]);
        if(!empty($Country))
        {
            if($Country["ZipRequired"] != "1" && trim($requiredFields["Zipcode"]) == "")
            {
                $requiredFields["Zipcode"] = "12345";
            } 
        }   
        $validation = $factory->getValidationObject($requiredFields);
        if ($validation->validate() === false) {
            $errors->addErrors('6', $validation->getErrors());
			return API_Response::fail();
        }

        //--- Start Date/Time must be before End Date/Time
        if(!empty($woData["StartDate"]) && !empty($woData["EndDate"]))
        {
            $startDateTime = $woData['StartDate'];
            if(!empty($woData["StartTime"]) && !empty($woData["EndTime"]))  $startDateTime .= " ".$woData['StartTime'];  
            
            $endDateTime = $woData['EndDate'];
            if(!empty($woData["StartTime"]) && !empty($woData["EndTime"]))  $endDateTime .= " ".$woData['EndTime']; 
            
            $startDT = strtotime($startDateTime); 
            $endDT = strtotime($endDateTime); 
            
            $dterr = "";
            if($endDT < $startDT)
            {
                 $dterr="Start Date/Time must be before End Date/Time.";              
                 $errors->addError(6, $dterr);
                 return API_Response::fail();
            } 
        }
                     
	$benchmark->point('Validation Done');

        date_default_timezone_set('US/Central');
        $woData['DateEntered'] = date('Y-m-d H:i:s');

	if (!empty($woData["StartDate"])) {
		// auto calc sourced by date and auto mark short notice
			$numWeekDays = $this->numberWeekDays(date("Y-m-d"), $woData["StartDate"]);

            if ($numWeekDays <= 5)
                $woData["ShortNotice"] = "True";

            if ($woData["Type_ID"] == "1") {

				$startDateTS = strtotime(date("Y-m-d"));
			$endDateTS = strtotime($woData["StartDate"]);

                $sourceByDateTS = $this->calculateSourceByDate($startDateTS, $endDateTS, $woData['DateEntered'], $numWeekDays);

				$woData["SourceByDate"] = date("Y-m-d", $sourceByDateTS);
		}
	}


        $benchmark->point('Geocode');
        if (empty($woData['Latitude']) || empty($woData['Longitude'])) {
			$geocode = new Core_Geocoding_Google();
			if (!empty($woData['Address']))
				$geocode->setAddress($woData['Address']);
			if (!empty($woData['City']))
				$geocode->setCity($woData['City']);
			if (!empty($woData['Zipcode']))
				$geocode->setZip($woData['Zipcode']);
			if (!empty($woData['State']))
				$geocode->setState($woData['State']);
			$woData['Latitude'] = $geocode->getLat();
			$woData['Longitude'] = $geocode->getLon();
	    }

        $benchmark->point('Geocode done');

	    if (!empty($woData['Approved']) && $woData['Approved']=="True" && $woData['Invoiced']=="False") {
    		$woData['DateApproved'] = $woData['DateEntered'];
    		$woData['approvedBy'] = $user->getLogin();
	    }
	    if (!empty($woData['Deactivated']) && $woData['Deactivated']=='True') {
            $woData['DeactivatedDate'] = date('Y-m-d H:i:s');
            $woData['DeactivatedBy'] = $user->getLogin();
        }

		if (!empty($woData['TechMarkedComplete']) && $woData['TechMarkedComplete']=="True")
            $woData['Date_Completed'] = $curTime;

		$woData['lastModDate'] = date('Y-m-d H:i:s');
		$woData['lastModBy'] = $user->getLogin();

        $fieldsList = implode(',',array_keys($woData));

        $benchmark->point('Inserting WO');
        $db = Core_Database::getInstance();

		// Update Status Field
		$woData["Status"] = Core_WorkOrder_Status::getStatusWithWorkOrder($woData);

		$result = false;
		$result = Core_Database::insert(Core_Database::TABLE_WORK_ORDERS, $woData);
		$wo_id = $db->lastInsertId(Core_Database::TABLE_WORK_ORDERS);

        $benchmark->point('Inserting WO done');
	if (empty($woData["Project_Name"]))
		$woData["Project_Name"] = "";

        if (!$result) {
    		$err = $errors->getErrors();
    		if (!empty($err))
				return API_Response::fail();

		    $errors->addError(7, 'Incorrect SQL request');
			return API_Response::fail();
		}
		
		// pull in cretification from project
		$projectId = $woData['Project_ID'];
		$pcs = new Core_ProjectCertifications();
		$certs = $pcs->getProjectCertification($projectId);
		$certs = empty($certs[$projectId]) ? array() : array_values($certs[$projectId]);
		
		// save client credential
		foreach ($certs as $cid) {
			$pc = new Core_WorkOrderCertification($wo_id, $cid);
			$pc->save();
		}
		
		//--- 13418 update Total_Reimbursable_Expense
		$Api_WosClass = new Core_Api_WosClass();
		$result = $Api_WosClass->update_Total_Reimbursable_Expense($wo_id);
		$workOrderAdditionalFields['WINNUM'] = $wo_id;
		$this->insertWorkOrderAdditionalFields($workOrderAdditionalFields);

		$benchmark->point('Time stamps');

		// perform any outbound sync
		if ($obsync = Core_WorkOrder_OutboundSync_Factory::build($company))
			$obsync->syncWithWorkOrder($wo_id, null, $woData, Core_WorkOrder_OutboundSync_Abstract::CONTEXT_CLIENT, $authData['login']);

		$this->baseVisitSave($wo_id, $baseVisit);
		if (!empty ($customSignOff)) {
			// add custom signoff from project
			$files = new Core_Files();
			$lastId = $files->insert(array(
				'WIN_NUM' => $wo_id,
				'path'    => $customSignOff,
				'type'    => Core_Files::SIGNOFF_FILE
			));
		}

       	Core_TimeStamp::createTimeStamp($wo_id, $user->getLogin(), "Work Order Created", $woData['Company_ID'], $woData['Company_Name'], $woData['Project_Name'], "", "", "");
		
/*		if (!empty($woData['Tech_ID']))
			$recalcList[$woData['Tech_ID']] = 1;
		if (!empty($woData['BackOut_Tech']))
			$recalcList[$woData['BackOut_Tech']] = 1;
		if (!empty($woData['NoShow_Tech']))
			$recalcList[$woData['NoShow_Tech']] = 1;
		
		if (sizeof($recalcList) > 0)
			Core_TechStats::recalcLifetimeStatsWithTechID(array_keys($recalcList));*/

       	if (!empty($woData['Tech_ID'])) {
			$mail  = new Core_EmailForAssignedTech();
			$mail->setWOData($woData);
			$mail->setTech($woData['Tech_ID']);
			$mail->setWorkOrder($wo_id);
			$mail->setProject($woData['Project_ID']);
			$mail->setCompanyId($woData['Company_ID']);
			$mail->setCompanyName($woData['Company_Name']);
			$mail->sendMail();
			//14064
			Core_TimeStamp::createTimeStamp($wo_id, $user->getLogin(), "Work Order Assigned: Tech ID# ". $woData['Tech_ID'], $woData['Company_ID'], $woData['Company_Name'], $woData['Project_Name'], "", "", "");
                        $NoticeClass = new Core_Api_NoticeClass();
                        $NoticeClass->createWOChangeStatusLog($wo_id,Core_Api_NoticeClass::WOStatusID_FOR_ASSIGNED);
		}

       	if (!empty($woData['ShowTechs']) && $woData['ShowTechs'] == "True")
        {
			Core_TimeStamp::createTimeStamp($wo_id, $user->getLogin(), "Work Order Published", $woData['Company_ID'], $woData['Company_Name'], $woData['Project_Name'], "", "", "");
                        $NoticeClass = new Core_Api_NoticeClass();
                        $NoticeClass->createWOChangeStatusLog($wo_id,Core_Api_NoticeClass::WOStatusID_FOR_PUBLISHED);
            $apiUser = new Core_Api_User();
            if($user->isAdmin() && $user->isPM())
            {
                //$apiUser->ResetAccessAndMoreWhenPublishing($woData['Company_ID'],$wo_id,$user->getLogin());//13995
            }
        }

        if (!empty($woData['ShowTechs']) && $woData['ShowTechs'] == "True" && (empty($woData['Deactivated']) || $woData['Deactivated'] == "False") && empty($woData['Tech_ID'])) {
    		    $mail = new Core_BlastRecruitmentEmail();
    		    $mail->setWorkOrder($wo_id);
    		    $mail->setCompanyId($woData['Company_ID']);
				$mail->setUser($user->getLogin());
    		    $mail->sendMail();
			    if ($woData['isProjectAutoAssign']=='True' || $woData['isProjectAutoAssign'] == 1) {
			    	$this->createAtJob($wo_id);
				}
				if ($workOrderAdditionalFields['PushWM'] == 1) {
					$wm = new Core_WorkMarket($wo_id);
					$wm->startWMAssignment();
				}
				if (!empty($woData['Country']) && $woData['Country'] != 'US' && $woData['Country'] != 'CA') {
					// send notification of foreign work order published if needed
					$mail = new Core_EmailForPublishedForeignCountryWO();
					$mail->setWorkOrder($wo_id);
					$mail->setUserFullName($user->getContactName());
					$mail->sendMail();
				}
        }
        if (!empty($woData['WorkOrderReviewed']) && $woData['WorkOrderReviewed']=="True") {
          	Core_TimeStamp::createTimeStamp($wo_id, $user->getLogin(), "Work Order Reviewed", $woData['Company_ID'], $woData['Company_Name'], $woData['Project_Name'], "", "", "");
        }
        if (!empty($woData['TechCheckedIn_24hrs']) && $woData['TechCheckedIn_24hrs']=="True") {
          	Core_TimeStamp::createTimeStamp($wo_id, $user->getLogin(), "Work Order Confirmed", $woData['Company_ID'], $woData['Company_Name'], $woData['Project_Name'], "", "", "");
        }
        if (!empty($woData['TechMarkedComplete']) && $woData['TechMarkedComplete']=="True") {
	        Core_TimeStamp::createTimeStamp($wo_id, $user->getLogin(), "Work Order Marked: Completed", $woData['Company_ID'], $woData['Company_Name'], $woData['Project_Name'], "", "", "");
                $NoticeClass = new Core_Api_NoticeClass();
                $NoticeClass->createWOChangeStatusLog($wo_id,Core_Api_NoticeClass::WOStatusID_FOR_WORKDONE);
			if ($updatedFields["Approved"] == "False") {
//				$techPayCalculator = new Core_TechPay();
//				$techPayCalculator->setWinNum($wo_id);
//				$techPayCalculator->updateTechPay();
			}
        }
        if (!empty($woData['Approved']) && $woData['Approved']=="True" && $woData['Invoiced']=="False") {
	        Core_TimeStamp::createTimeStamp($wo_id, $user->getLogin(), "Work Order Approved", $woData['Company_ID'], $woData['Company_Name'], $woData['Project_Name'], "", "", "");
                $NoticeClass = new Core_Api_NoticeClass();
                $NoticeClass->createWOChangeStatusLog($wo_id,Core_Api_NoticeClass::WOStatusID_FOR_APPROVED);
	        $mail = new Core_EmailForApprovedTech();
			$mail->setCompanyId($woData['Company_ID']);
            $mail->setCompanyName($woData['Company_Name']);
            $mail->setTech($curWO['Tech_ID']);
            $mail->setWorkOrder($wo_id);
            $mail->setProject($curWO['Project_ID']);
            $mail->sendMail();
        }

        if (!empty($woData['Deactivated']) && $woData['Deactivated']=="True") {
			if ($workOrderAdditionalFields['PushWM'] == 1) {
				$wm = new Core_WorkMarket($wo_id);
				$wm->stopWMAssignment(true, 0.00, $updatedFields['Reason_Deactivated']); // void or cancel assignment
			}
			$mail  = new Core_EmailForDeactivatedWO();
			$mail->setWOData($woData);
			$mail->setTech($woData['Tech_ID']);
			$mail->setWorkOrder($wo_id);
			$mail->setProject($woData['Project_ID']);
			$mail->setCompanyId($woData['Company_ID']);
			$mail->setCompanyName($woData['Company_Name']);
			$mail->sendMail();
	        Core_TimeStamp::createTimeStamp($wo_id, $user->getLogin(), "Work Order Deactivated", $woData['Company_ID'], $woData['Company_Name'], $woData['Project_Name'], "", "", "");
        }
/*        if (!empty($woData['AbortFee']) && $woData['AbortFee']=="False")
	        Core_TimeStamp::createTimeStamp($wo_id, $user->getLogin(), "Abort Fee Un-checked", $woData['Company_ID'], $woData['Company_Name'], $woData['Project_Name'], "", "", "");*/
        if (!empty($woData['AbortFee']) && $woData['AbortFee']=="True") {
	        Core_TimeStamp::createTimeStamp($wo_id, $user->getLogin(), "Abort Fee Checked", $woData['Company_ID'], $woData['Company_Name'], $woData['Project_Name'], "", "", "");
        }

		$benchmark->point('Time stamps done');
		$devicesText = "";
		if (!empty($woData["Amount_Per"]) && $woData["Amount_Per"] == "Device") {
			$devicesText = "
			Devices: {$woData["Qty_Devices"]}";
		}

		if (!defined('NO_CREATE_WORK_ORDER_EMAIL')) {
			$benchmark->point('Emails');

			//$startDate = $woData["StartDate"];
			if (!empty($woData["StartDate"]))
				$startDate = date("m/d/Y", strtotime($woData["StartDate"]));
			else
				$startDate = NULL;
			$newWOMail = new Core_Mail();
			$newWOMail->setFromName("Work Order Created");
			$newWOMail->setFromEmail("nobody@fieldsolutions.com");
			$newWOMail->setToName("NewWOsNote");
			$newWOMail->setToEmail("NewWOsNote@fieldsolutions.com");
			$newWOMail->setSubject("WIN# {$wo_id}: New $company WO - {$woData["Project_Name"]}");
			@$newWOMail->setBodyText("
				WorkOrderID: {$woData["WO_ID"]}
				Username: {$woData["Username"]}
				Max Pay: {$woData["PayMax"]}
				Amount Per: {$woData["Amount_Per"]} $devicesText
				Start Date: $startDate
				Start Time: {$woData["StartTime"]}
				Project Name: {$woData["Project_Name"]}
				Address: {$woData["Address"]}
				City: {$woData["City"]}
				State: {$woData["State"]}
				Zip: {$woData["Zipcode"]}

				Description:

				{$woData["Description"]}

				Special Instructions:

				{$woData["SpecialInstructions"]}");

			$newWOMail->send();
			$benchmark->point('Emails done');
		}
		$utype = $user->getUserType();
		if (!empty($utype) &&  ($utype == "Install Desk" || $utype == "Manager") && $company == "FLS") {
		        $mail  = new Core_EmailForNewInstallDeskWorkOrder();
		        $mail->setAckFrom("gary.baugh@us.fujitsu.com");
		        $mail->setAckTo($user->getEmailPrimary());
		        $mail->setNotiFrom("support@fieldsolutions.com");
		        $mail->setNotiTo("jeff.paroline@us.fujitsu.com,gary.baugh@us.fujitsu.com,jeremy.helton@us.fujitsu.com");
		        $mail->setWorkOrder($wo_id);
		        $mail->sendMail();
		}

//		$result = $this->makeWorkOrderXML(array('TB_UNID'=>$wo_id));

//		return $result;

		return API_Response::success(array(new API_WorkOrder($wo_id)));
	}
    
    /**
	 * create Project
	 *
	 * @param string $user
	 * @param string $pass
	 * @param API_Project $workOrder
	 * @return API_Response
	 */
	public function createProject($user, $pass, $workOrder)
	{
        $errors = Core_Api_Error::getInstance();
        
        $logger = Core_Api_Logger::getInstance();
		$logger->setUrl('createProject');
        $logger->setLogin($user);
		$logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');

		$authData = array('login'=>$user, 'password'=>$pass);
        $user = new Core_Api_User();
		$user->checkAuthentication($authData);
        if (!$user->isAuthenticate())
			return API_Response::fail();
        
		if (is_string($workOrder)) $woXML = $workOrder;
		else {
			$dom = new DOMDocument("1.0");
			$dom->appendChild($workOrder->toXMLElementTect($dom));
			$woXML = $dom->saveXML();
		}
        if (empty($woXML))
			$errors->addError(2, 'XML parameter is empty but required');

		$err = $errors->getErrors();
		if (!empty($err))
			return API_Response::fail();

        $prjData = $this->getDataFromProjectXML($woXML);

        $fieldListType = Core_Database::getFieldListType(Core_Database::TABLE_PROJECTS);
        $prjData = Core_Database::convertQueryResult($prjData, $fieldListType);


		$factory = new Core_Validation_Factory(Core_Validation_Factory::VALIDATION_TYPE_PROJECT_CREATE);
	    $validation = $factory->getValidationObject($prjData);

        if ($validation->validate() === false) {
            $errors->addErrors('6', $validation->getErrors());
			return API_Response::fail();
        }

        $fieldsList = implode(',',array_keys($prjData));

        $db = Core_Database::getInstance();

		$result = false;
		$result = Core_Database::insert(Core_Database::TABLE_PROJECTS, $prjData);
		$prj_id = $db->lastInsertId(Core_Database::TABLE_PROJECTS);

        if (!$result) {
    		$err = $errors->getErrors();
    		if (!empty($err))
				return API_Response::fail();

		    $errors->addError(7, 'Incorrect SQL request');
			return API_Response::fail();
		}

		return API_Response::success(array(new API_WorkOrder($wo_id)));
	}
    
	/**
	 * update Work Order
	 *
	 * @param string $user
	 * @param string $pass
	 * @param int $winNum
	 * @param API_WorkOrder $workOrder
	 * @param boolean $ignoreWarnings
	 * @return API_Response
	 */
	public function updateWorkOrder($user, $pass, $winNum, $workOrder, $ignoreWarnings = false, $isMassDeactivate = false, $is_reactivation = false)
	{
		if (is_string($workOrder)) $woXML = $workOrder;
		else {
			$dom = new DOMDocument("1.0");
			$dom->appendChild($workOrder->toXMLElement($dom));
			$woXML = $dom->saveXML();
		}

       	$logger = Core_Api_Logger::getInstance();
		$logger->setUrl('updateWorkOrder ' . $winNum);
        $logger->setLogin($user);
		$logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');

		$authData = array('login'=>$user, 'password'=>$pass);

		$user = new Core_Api_User();
		$user->checkAuthentication($authData);

		if (!$user->isAuthenticate())
			return API_Response::fail();
			
		//FSPlusOne - Apply Points for Back Out
        if(!empty($workOrder->BackOut_Tech)){
	        Core_FSPlusOne::applyPoints(Core_FSPlusOne::WORK_ORDER_BACK_OUT_CALL, $workOrder->BackOut_Tech, $workOrder->Tech_LName, "tech", $workOrder->WIN_NUM);
         }
            
        //FSPlusOne - Apply Points for No Show
        if(!empty($workOrder->NoShow_Tech)){
	        Core_FSPlusOne::applyPoints(Core_FSPlusOne::WORK_ORDER_NO_SHOW_NO_CALL, $workOrder->NoShow_Tech, $workOrder->Tech_LName, "tech", $workOrder->WIN_NUM);
         }
         
	 	//FSPlusOne - Apply Points for Performance Rating of 4 or better (?)
        if(!empty($workOrder->SATPerformance) && intval($workOrder->SATPerformance) >= 4){
	        Core_FSPlusOne::applyPoints(Core_FSPlusOne::WORK_ORDER_SUPERIOR_RATING, $workOrder->Tech_ID, $workOrder->Tech_LName, "tech", $workOrder->WIN_NUM);
        }
	
		$params = array();
		$params['TB_UNID'] = $winNum;
		$params['xml'] = $woXML;
		$params['isMassDeactivate'] = $isMassDeactivate;
		
		$errors = Core_Api_Error::getInstance();
		if (empty($params['TB_UNID']))
			$errors->addError(2, 'TB_UNID parameter is empty but required');

		if (empty($params['xml']))
			$errors->addError(2, 'XML parameter is empty but required');

		$err = $errors->getErrors();
		if (!empty($err))
			return API_Response::fail();

		$woData = $this->getDataFromXML($params['xml']);

		return $this->updateWorkOrderPostXMLParse($user, $params, $woData, $ignoreWarnings, $is_reactivation);
	}

	/**
	 * update Work Orders
	 *
	 * @param string $user
	 * @param string $pass
	 * @param string $timeStampFrom
	 * @param string $timeStampTo
	 * @param string $timeStampType
	 * @param array $criteria
	 * @param API_WorkOrder $workOrder
	 * @return API_Response
	 */
	private function updateWorkOrders($user, $pass, $timeStampFrom, $timeStampTo, $timeStampType, $criteria, $workOrder)
	{
		
		if (is_string($workOrder)) $woXML = $workOrder;
		else {
			$dom = new DOMDocument("1.0");
			$dom->appendChild($workOrder->toXMLElement($dom));
			$woXML = $dom->saveXML();
		}

		$authData = array('login'=>$user, 'password'=>$pass);
		$errors = Core_Api_Error::getInstance();

	    $logger = Core_Api_Logger::getInstance();
        $logger->setLogin($companyID);
	    $logger->setUrl('updateWorkOrders');

		$userObj = new Core_Api_User();
		$userObj->checkAuthentication($authData);
		
		if (!$userObj->isAuthenticate())
			return API_Response::fail();
        $companyID = $userObj->getContextCompanyId();

		$workOrders = API_WorkOrderFilter::filter($companyID,
														'WIN_NUM',
														$timeStampFrom,
														$timeStampTo,
														$timeStampType,
														$criteria);
		$woData = $this->getDataFromXML($woXML);

		$updates = '<WOS>';

		try { //API_WorkOrderFilter::filter return Core_Filter_Result that implements Traversable. So it fetch data in foreach
			foreach ($workOrders as $workOrder)
			{
				$this->updateWorkOrderPostXMLParse($authData, $workOrder, $woData);
				$updates .= '<WO><WIN_NUM>' . $workOrder['TB_UNID'] . '</WIN_NUM></WO>';

				$err = $errors->getErrors();
				if (!empty($err))
				{
					return API_Response::fail();
				}
			}
		} catch (Exception $e) {
			$errors->addError(7, 'SQL Error: '. $e);
			error_log($e);
			return API_Response::fail();
		}

		$updates .= '</WOS>';
		return $updates;
	}

	private function updateWorkOrderPostXMLParse($user, $params, $woData, $ignoreWarnings = false, $is_reactivation = false) {
		$sentworknotificationmail = 0;
		if(isset($woData['sentworknotificationmail']))
		{
			$sentworknotificationmail = $woData['sentworknotificationmail'];
			unset($woData['sentworknotificationmail']);
		}
		// save visit field for creating / updating visits
		$extraWoData['Visits'] = $woData['__Visits'];
		unset($woData['__Visits']);
                
		$errors = Core_Api_Error::getInstance();

		$wo_id = trim($params['TB_UNID'],"`'");
		//$ignoreWarnings = $ignoreWarnings === true;
		
		$db = Core_Database::getInstance();
		$fieldListMap = Core_Database::getFieldList(Core_Database::TABLE_WORK_ORDERS);
		$fieldListMap = array_combine($fieldListMap, $fieldListMap);
		$select = $db->select();
		$select->from(Core_Database::TABLE_WORK_ORDERS, $fieldListMap)
			->where("WIN_NUM = ?", $wo_id);

		$curWO = Core_Database::fetchAll($select);
                
        if (empty($curWO[0])) {
            
            $err = $errors->getErrors();
    		if (!empty($err))
				return API_Response::fail();

        	$errors->addError(7, 'Work Order id '.$wo_id.' does not exist');
			return API_Response::fail();
        }
        $curWO = $curWO[0];
        
                //743
                $Core_Api_User = new Core_Api_User();
                $apiemailrestricted = new Core_EmailForRestricted();
                $companyID = $curWO['Company_ID'];
                $clients = $Core_Api_User->getRepClientforCompany($companyID);
                //$clientInfo = $Core_Api_User->loadWithExt(
                //                array(
                //                    'login' => $clients[0]['UserName'],
                //                    'password' => $clients[0]['Password']
                //                ));

                //$CSD =$clientInfo->FSClientServiceDirectorEmail;
                //$AM =$clientInfo->FSAccountManagerEmail;
                $CSD =$user->FSClientServiceDirectorEmail;
                $AM =$user->FSAccountManagerEmail;
                //end 743
		if (!isset($woData['PushWM'])) {
			// pull in PushWM
	                $additionalWO = $this->getWorkOrderAdditionalFields($wo_id);
			$woData['PushWM'] = $additionalWO['PushWM'] == 1 ? "True" : "False";
		}
		$workOrderAdditionalFieldsTemp = array(
			'ServiceTypeId' => $woData['ServiceTypeId'], 
			'EntityTypeId' => $woData['EntityTypeId'],
			'FSClientServiceDirectorId' => $woData['FSClientServiceDirectorId'],
			'FSAccountManagerId' => $woData['FSAccountManagerId'],
			'ACSNotifiPOC' => $woData['ACSNotifiPOC'],
			'ACSNotifiPOC_Phone' => $woData['ACSNotifiPOC_Phone'],
			'ACSNotifiPOC_Email' => $woData['ACSNotifiPOC_Email'],
			'PushWM' => $woData['PushWM'] == "True" ? 1 : 0
		);
		
        /*
		$copyWoSelect = $db->select();
        $copyWoSelect->from(Core_Database::TABLE_WORK_ORDERS, array("WO_ID"));
		$copyWoSelect->where('WO_ID LIKE ?', '%'.$params['WO_ID'].'%');
		$copyWoSelect->where('Company_ID = ?', $curWO['Company_ID']);
		error_log($copyWoSelect->__toString());
		if($row = $db->fetchRow($copyWoSelect)){
			$errors->addError(7, "Client WO ID # already exists");
			return API_Response::fail();
		}
		*/

		$fieldListType = Core_Database::getFieldListType(Core_Database::TABLE_WORK_ORDERS);
		$woData = Core_Database::convertQueryResult($woData, $fieldListType);
		$curWO = Core_Database::convertQueryResult($curWO, $fieldListType);

		foreach ($workOrderAdditionalFieldsTemp as $f => $v) {
			$woData[$f] = $v;
			$curWO[$f] = $v;
		}

		unset($woData['PushWM']);
        if ($user->isPM() != true && $curWO['Company_ID'] != $user->getContextCompanyId()) {
        	$errors->addError(5, 'You do not have enough permissions to update this Work Order');
			return API_Response::fail();
        }

		if ($this->mode == WO_UPDATE_MODE_NORMAL) {
			if (array_key_exists("StartDate", $woData))
				$woData["StartDate"] = $this->blankIfInvalidDate($woData["StartDate"]);
			if (array_key_exists("EndDate", $woData))
				$woData["EndDate"] = $this->blankIfInvalidDate($woData["EndDate"]);
		}

		$techData = NULL;
        
		if (!empty($woData["Tech_ID"])){
			$techData = $this->pullAssignTechInfo($user, $woData["Tech_ID"]);
            $bidData = $this->pullBidData($woData["Tech_ID"], $wo_id, array('Comments'));
        } else if (!empty($woData["FLS_ID"])) {
			$techData = $this->pullAssignTechInfo($user, $woData["FLS_ID"], true);
            $bidData = $this->pullBidData($woData["Tech_ID"], $wo_id, array('Comments'));
        }

		if ($this->mode == WO_UPDATE_MODE_NORMAL) {
			if($woData['bid_comments']===null){
				$bidData = NULL;
				if (!empty($woData["Tech_ID"]))
					$bidData = $this->pullBidData($woData["Tech_ID"], $wo_id, array('Comments'));
				else if (!empty($woData["FLS_ID"]))
					$bidData = $this->pullBidData($woData["Tech_ID"], $wo_id, array('Comments'));
				$woData['bid_comments'] = $bidData['Comments'];
			}
        
			if ($curWO["Invoiced"] == "True") {
				// don't allow changing approve / deactivated if invoiced
				if (isset($woData["Approved"]) && $woData["Approved"] != "True") unset($woData["Approved"]);
				if (isset($woData["Deactivated"]) && $woData["Deactivated"] != "False") unset($woData["Deactivated"]);
			}
		}

        $updatedFields = array();
        foreach ($curWO as $key=>$val) {
            if (isset($woData[$key]) && $woData[$key]!==null) {
                $updatedFields[$key] = $woData[$key];
            } else {
                $updatedFields[$key] = $val;
            }
        }
        

		//if (!$user->isPM() || $curWO["Status"] != WO_STATE_CREATED) {
		//	$updatedFields["PcntDeduct"] = $curWO["PcntDeduct"];
		//}

		if ($this->mode == WO_UPDATE_MODE_NORMAL) {
			if (!empty($woData['Expensethrough']))
				$woData['Total_Reimbursable_Expense'] = $woData['Expensethrough'];

			$payFieldUpdated = false;
			$payFields = Core_TechPay::recalcFields();
			$payFields = array_combine($payFields, $payFields);
			foreach ($woData as $key=>$val) {
				if (array_key_exists($key, $payFields) && $curWO[$key] != $updatedFields[$key])
					$payFieldUpdated = true;
			}
			if (!empty($woData['Total_Reimbursable_Expense'])) {
				$payFieldUpdated = true;
			}

			$payData = NULL;

			if ( (($updatedFields['TechMarkedComplete'] == "True") || ($updatedFields['Approved'] == "True" && $curWO['Approved'] == "False")) && (empty($updatedFields['PayAmount']) || $updatedFields['PayAmount'] == 0.00 || $payFieldUpdated || $curWO['TechMarkedComplete'] == "False")) {
				// must be done before exclude of read only field otherwise we loose access to these fields
				$techPayCalculator = new Core_TechPay();
				//376
				$updatedFields['Total_Reimbursable_Expense'] = $woData['Total_Reimbursable_Expense'];
            
				$payData = $techPayCalculator->updateTechPay($updatedFields);
				unset($updatedFields['Expensethrough']);
				//376
			}
			unset($woData['Expensethrough']);

			//--- 13571
			if(($updatedFields['TechMarkedComplete'] == "True" && $curWO['TechMarkedComplete'] == "False") || ($updatedFields['Approved'] == "True" && $curWO['Approved'] == "False")){
				//743
                                $isFullRestricted_old = $Core_Api_User->AreFullRestriction($companyID);
                                $isPartialRestricted_old = $Core_Api_User->ArePartialRestriction($companyID);
                                $isFullOpened_old = $Core_Api_User->AreFullorPartialRestriction($companyID);
                                //end 743

				$beginDate = date("Y-m-d");
				$fieldvals=array('BeginDateForCountingNDaysRule'=>$beginDate);
				$Core_Api_User->saveClientCompanyAdditionalFields($curWO['Company_ID'],$fieldvals);
				//$Core_Api_User->setAllAccessOpened($curWO['Company_ID']);  //13995

                                //743
                                $isFullRestricted = $Core_Api_User->AreFullRestriction($companyID);
                                $isPartialRestricted = $Core_Api_User->ArePartialRestriction($companyID);
                                $isFullOpened=$Core_Api_User->AreFullorPartialRestriction($companyID);
                                $wonum = $wo_id;
                                $wostatus = $curWO["Status"];
                                $restrictedtype = 5;

                                $issendmail=FALSE;
                                if(($isFullRestricted != $isFullRestricted_old)
                                        || ($isPartialRestricted != $isPartialRestricted_old)
                                        || ($isFullOpened!=$isFullOpened_old))
                                {
                                    $issendmail = TRUE;
			}

                                if($issendmail)
                                {
                                    $CompanyName = $user->getCompanyName();
                                    $apiemailrestricted->setCompanyId($companyID);
                                    $apiemailrestricted->setCompanyName($CompanyName);
                                    $apiemailrestricted->settoMail($AM.",".$CSD);
                                    $apiemailrestricted->setusername($user->getLogin());
                                    $apiemailrestricted->setrestrictedtype($restrictedtype);
                                    $apiemailrestricted->setwonum($wonum);
                                    $apiemailrestricted->setwostatus($wostatus);
                                    $apiemailrestricted->sendMail();
                                }
                                //end 743
			}

			$isProjectAutoAssign = $updatedFields["isProjectAutoAssign"];
			$this->excludeFields($updatedFields);
			$updatedFields["isProjectAutoAssign"] = $isProjectAutoAssign;

			foreach ($workOrderAdditionalFieldsTemp as $f => $v) {
				$updatedFields[$f] = $v;
			}
			if ($payData != NULL) {
				// restore tech pay info excluded
				$updatedFields["PayAmount"] = $payData ["PayAmount"];
				$updatedFields["calculatedTechHrs"] = $payData ["calculatedTechHrs"];
				$updatedFields["baseTechPay"] = $payData ["baseTechPay"];
				$updatedFields["Net_Pay_Amount"] = $payData ["Net_Pay_Amount"];
				$updatedFields["Z2T_Pay_Amount"] = $payData ["Z2T_Pay_Amount"];
				$updatedFields["PricingRan"] = "False";
			}

			if (!empty($woData["Project_Name"]) && $curWO["Project_Name"] != $updatedFields["Project_Name"]) {
				// clear project id if project name is set
				$updatedFields["Project_ID"] = "";
			}

			// If invalid field is passed
			$this->excludeFields($woData);
			$exceptionConverter = new Core_Caspio_ExceptionConverter();
			if (!$exceptionConverter->columnValidate(Core_Database::TABLE_WORK_ORDERS, array_keys($woData))){
				$errors->addError(6, 'Invalid field "'.$exceptionConverter->getNotValidColumn().'"');
				return API_Response::fail();
			}
		}

		if ($techData != NULL) {
			// use already pulled in tech data
			$techData = $techData[0];
            $updatedFields["Tech_ID"] = $techData["TechID"];
			$updatedFields["Tech_FName"] = $techData["FirstName"];
			$updatedFields["Tech_LName"] = $techData["LastName"];
			$updatedFields["TechEmail"] = $techData["PrimaryEmail"];
			$updatedFields["TechPhone"] = $techData["PrimaryPhone"];
			if (!empty($techData["PrimaryPhoneExt"]))
				$updatedFields["TechPhoneExt"] = $techData["PrimaryPhoneExt"];
			else
				$updatedFields["TechPhoneExt"] = NULL;

			if (!$ignoreWarnings && !empty($techData["IsAdminDenied"])) {
		        	$errors->addError(2, "You are about to assign work to a technician that has been \"denied access\" by your company. Please re-confirm your choice of technician. Tech ID: " . $updatedFields["Tech_ID"] . " (" . $updatedFields["Tech_FName"] . " " . $updatedFields["Tech_LName"] . ")");
				return API_Response::fail();
			}
			if (!empty($techData["FLS_ID"]) && $techData["FLS_ID"] != 'NULL')
				$updatedFields["FLS_ID"] = trim($techData["FLS_ID"]);
			else {
				$updatedFields["FLS_ID"] = NULL;
				if ($curWO['Company_ID'] == "FLS") {
					$errors->addError(2, "You are about to assign work to a technician that does not have a FLS Badge. Please re-confirm your choice of technician. Tech ID: " . $updatedFields["Tech_ID"] . " (" . $updatedFields["Tech_FName"] . " " . $updatedFields["Tech_LName"] . ")");
					return API_Response::fail();
				}
				if (!empty($techData[6]) && $techData[6] != 'NULL')
					$updatedFields["FLS_ID"] = trim($techData[6]);
				else {
					$updatedFields["FLS_ID"] = NULL;
					if ($curWO['Company_ID'] == "FLS") {
						$errors->addError(2, "You are about to assign work to a technician that does not have a FLS Badge. Please re-confirm your choice of technician. Tech ID: " . $updatedFields["Tech_ID"] . " (" . $updatedFields["Tech_FName"] . " " . $updatedFields["Tech_LName"] . ")");
						return API_Response::fail();
					}
				}
			}
			if (strtolower($techData["AcceptTerms"]) != "yes" && ($techData["AcceptTerms"] != 'WM' || $workOrderAdditionalFieldsTemp['PushWM'] != 1)) {
	        		$errors->addError(2, "The technician you're attempting to assign has not accepted terms of usage.  Sorry for any issues this may cause you. Tech ID: " . $updatedFields["Tech_ID"] . " (" . $updatedFields["Tech_FName"] . " " . $updatedFields["Tech_LName"] . ")");
				return API_Response::fail();
			}
			$WMClass = new Core_WorkMarket($wo_id);
			if ($workOrderAdditionalFieldsTemp['PushWM'] == 1 && $techData["AcceptTerms"] != 'WM' && !$WMClass->prepareToAssignInFS()) {
        		$errors->addError(2, "This work order was already assigned in Work Market");
				return API_Response::fail();
			}
		}
	
		$combiner = new Core_Combiner_WorkOrder($updatedFields, Core_Combiner_WorkOrder::COMBINER_MULTIVISIT_OFF, Core_Combiner_WorkOrder::COMBINER_MODE_MORE_DATA);
		$combiner->setCompanyId($curWO["Company_ID"]);
		$combiner->setCompanyName($curWO["Company_Name"]);
		$combiner->setUserName($user->getLogin());
        if ($combiner->combine($curWO)) {
            //976
            $errors->resetErrors();
            //end 976
			$updatedFields = $combiner->getData();
		} else {
		    $errors->addErrors('6', $combiner->getErrors());
			return API_Response::fail();
		}

		if (!empty($woData['Duration']) && empty($woData['EstimatedDuration']) && is_numeric($woData['Duration'])) {
			// map Duration field to EstimatedDuration for calls going through api #13458
			$updatedFields['EstimatedDuration'] = $woData['Duration'];
		}
		$baseVisit = $this->baseVisitFromWorkOrder($updatedFields);
		$updatedFields = $this->baseVisitRemoveFields($updatedFields);

		$workOrderAdditionalFields = array(
			'ServiceTypeId' => $updatedFields['ServiceTypeId'], 
			'EntityTypeId' => $updatedFields['EntityTypeId'],
			'FSClientServiceDirectorId' => $updatedFields['FSClientServiceDirectorId'],
			'FSAccountManagerId' => $updatedFields['FSAccountManagerId'],
			'ACSNotifiPOC' => $updatedFields['ACSNotifiPOC'],
			'ACSNotifiPOC_Phone' => $updatedFields['ACSNotifiPOC_Phone'],
			'ACSNotifiPOC_Email' => $updatedFields['ACSNotifiPOC_Email'],
			'PushWM' => $updatedFields['PushWM']
		);
		unset($updatedFields['PushWM']);
		$updatedFields = Core_Combiner_WorkOrder::removeMoreDataFields($updatedFields);

		$factory = new Core_Validation_Factory(Core_Validation_Factory::VALIDATION_TYPE_UPDATE);
		$validationFields = $updatedFields;
		
		$Country = Core_Country::getCountryInfoByCode($validationFields["Country"]);
		if(!empty($Country))
		{
			if($Country["ZipRequired"] != "1" && trim($validationFields["Zipcode"]) == "")
			{
				$validationFields["Zipcode"] = "12345";
			} 
		}

         //Bypass Validation for mass deactivate
        if($params['isMassDeactivate'] != true && $updatedFields['Deactivated'] != 'True' && $updatedFields['DeactivationCode'] == ""){
	        	$validation = $factory->getValidationObject($validationFields);
	        if ($validation->validate() === false) {
	            $errors->addErrors('6', $validation->getErrors());
				return API_Response::fail();
	        }
        }

		if ($this->mode == WO_UPDATE_MODE_NORMAL) {
			//--- Start Date/Time must be before End Date/Time
			if (!empty($updatedFields['StartDate']) && !empty($updatedFields['EndDate'])
							&& ($updatedFields['Deactivated']!='True' || $curWO['Deactivated']!='False')) {
				$startDateTime = $updatedFields['StartDate'];
				$endDateTime = $updatedFields['EndDate'];
		
				if (!empty($updatedFields['StartTime']) && !empty($updatedFields['EndTime'])) {
					$startDateTime .= $updatedFields['StartTime'];
					$endDateTime .= ' '.$updatedFields['EndTime'];
				}
	        
				$startDT = strtotime($startDateTime); 
				$endDT = strtotime($endDateTime ? $endDateTime : "2100-01-01 00:00:00");
				$dterr = "";
				if($endDT < $startDT)
				{
					 $dterr="Start Date/Time must be before End Date/Time.";              
					 $errors->addError(6, $dterr);
					 return API_Response::fail();
				} 
			}
			//--- for Visit field
			if(array_key_exists("Amount_Per", $updatedFields) && $updatedFields['Amount_Per'] == 'Visit')
			{
				/* 13543
				if(empty($updatedFields['Qty_Devices']))
				{
					$errors->addError(6, '# of Visit empty but required.');
					return API_Response::fail();                
				}
				else if(!is_numeric ($updatedFields['Qty_Devices']))
				{
					$errors->addError(6, '# of Visit must be number.');
					return API_Response::fail();                
				}
				else if($updatedFields['Qty_Devices']<=0)
				{
					$errors->addError(6, '# of Visit must be greater then 0.');
					return API_Response::fail();                                
				}            
				*/           
			}
			//--- 

			if (!empty($updatedFields["Tech_ID"]) && $updatedFields["TechMarkedComplete"] == "False" && $curWO["TechMarkedComplete"] == "True" && empty($updatedFields["MissingComments"])) {
				$errors->addError(2, 'Incomplete Work Order Comments are required.');
				return API_Response::fail();
			}

			date_default_timezone_set('US/Central');
			$curTime = date('Y-m-d H:i:s');

			if ($updatedFields['TechMarkedComplete']=="True" && $curWO['TechMarkedComplete']=="False")
				$updatedFields['Date_Completed'] = $curTime;
			else if ($updatedFields['TechMarkedComplete']=="False")
				$updatedFields['Date_Completed'] = null;

			$updatedFields['lastModDate'] = date('Y-m-d H:i:s');
			$updatedFields['lastModBy'] = $user->getLogin();

			// End field validation. Now only clean up fields before updating
			$projectInfo = NULL;
			if (!empty($updatedFields['Tech_ID'])) {
				$projectInfo = new Core_Project((int)$updatedFields["Project_ID"]);
				$projectInfo->setData();
				if (strtoupper($projectInfo->getFixedBid()) == "TRUE" && empty($updatedFields["Tech_Bid_Amount"]))
					$updatedFields["Tech_Bid_Amount"] = $updatedFields["PayMax"];
			}
		}

        $changedFields = false;
        if (!empty($updatedFields['Tech_ID'])){
            $bufArr = array('StartDate','StartTime','Address','City','State','Zipcode','PayAmount',
                            'SpecialInstructions','BaseTechPay','OutofScope_Amount','TripCharge','MileageReimbursement',
                            'MaterialsReimbursement','AbortFeeAmount','Additional_Pay_Amount','Add_Pay_Reason','lastModBy');
            $woChangedMail = new Core_Mail_WoStatus_Change();
			$woChangedMail->assign($key, $updatedFields["Approved"]);
			
			$changedFieldsArr = array();
            foreach ($curWO as $key=>$val) {
                if (!empty($updatedFields[$key]) && $val != $updatedFields[$key]) {
                    if (in_array($key,$bufArr)) {
                    	$changedFieldsArr[] = $key;
                        $woChangedMail->assign($key, $updatedFields[$key]);
                    	$changedFields = true;
                    }
                }
            }
            //13980
            $woChangedMail->assign('WIN_NUM', $curWO['WIN_NUM']);
            $woChangedMail->assign('Company_ID', $curWO['Company_ID']);
            $woChangedMail->assign('WO_ID', $curWO['WO_ID']);
            $woChangedMail->assign('Tech_FName', $curWO['Tech_FName']);
            $woChangedMail->assign('Tech_LName', $curWO['Tech_LName']);
            //end 13980
            $future = new Core_FutureWorkOrderInfo;
            $row = $future->find($user->getLogin())->toArray();
            $woChangedMail->setFuturewoinfo($row[0]);
        }
        //set changedFields to false if only updating lastModBy and Qbid
		if(count($changedFieldsArr) == 1 &&  in_array("lastModBy", $changedFieldsArr) && $curWO['QBID'] != $updatedFields['QBID']){
			$changedFields = false;
		}
		
		if ($this->mode == WO_UPDATE_MODE_NORMAL || $this->mode == WO_UPDATE_MODE_ASSIGN) {
			if (empty($curWO['Latitude']) || empty($curWO['Longitude']) || $updatedFields['Zipcode']!=$curWO['Zipcode'] ||
				$updatedFields['Address']!=$curWO['Address'] || $updatedFields['City']!=$curWO['City']) {
				$geocode = new Core_Geocoding_Google();
				$geocode->setAddress($updatedFields['Address']);
				$geocode->setCity($updatedFields['City']);
				$geocode->setZip($updatedFields['Zipcode']);
				$geocode->setState($updatedFields['State']);
				$updatedFields['Latitude'] = $geocode->getLat();
				$updatedFields['Longitude'] = $geocode->getLon();
			}

			if (!empty($updatedFields["StartDate"]) && $updatedFields['StartDate'] != $curWO['StartDate']) {
				// auto calc sourced by date and auto mark short notice
				$numWeekDays = $this->numberWeekDays(date("Y-m-d"), $updatedFields["StartDate"]);

				if ($numWeekDays <= 5)
					$updatedFields["ShortNotice"] = "True";

				if ($updatedFields["Type_ID"] == "1") {
					$startDateTS = strtotime(date("Y-m-d"));
					$endDateTS = strtotime($updatedFields["StartDate"]);

					$sourceByDateTS = $this->calculateSourceByDate($startDateTS, $endDateTS, $curWO['DateEntered'], $numWeekDays);

					$updatedFields["SourceByDate"] = date("Y-m-d", $sourceByDateTS);
				}
			}

			if ($updatedFields['Approved']=="True") {
				$updatedFields['TechMarkedComplete'] = 'True';
				if ($curWO['Approved'] == "False") {
					$updatedFields['DateApproved'] = date('Y-m-d H:i:s');
					$updatedFields['approvedBy'] = $user->getLogin();
				}
			
				//FSPlusOne - Apply Points for Approved Work Order
				Core_FSPlusOne::applyPoints(Core_FSPlusOne::WORK_ORDER_APPROVED, $updatedFields['Tech_ID'], $updatedFields["Tech_LName"], "tech", $wo_id);
			}
			else {
				// clear approve fields
				$updatedFields['DateApproved'] = NULL;
				$updatedFields['approvedBy'] = "";
			}

			$requireACSReset = $updatedFields['Tech_ID'] != $curWO['Tech_ID'] || $updatedFields['StartDate'] != $curWO['StartDate'] || $updatedFields['StartTime'] != $curWO['StartTime'];
			$woChangedToPublished = $updatedFields["ShowTechs"] == "True" && $updatedFields["ShowTechs"] != $curWO["ShowTechs"];
			$isrepublish = empty($updatedFields['Tech_ID']) && $curWO['Tech_ID']!="" && !empty($curWO['Tech_ID']) && $curWO['Status'] == WO_STATE_ASSIGNED;
			$techAssignedOrReassigned = $updatedFields['Tech_ID'] != $curWO['Tech_ID'] && !empty($updatedFields['Tech_ID']);
			$techReassigned = $techAssignedOrReassigned && !empty($curWO['Tech_ID']);
			$woChangedToApproved = $updatedFields["Approved"] == "True" && $updatedFields["Approved"] != $curWO["Approved"];
			$techBackup = !empty($updatedFields["BU1FLSID"]);

			if ($updatedFields['Deactivated']=='True' && $curWO['Deactivated']=='False') {
				$updatedFields['DeactivatedDate'] = date('Y-m-d H:i:s');
				$updatedFields['DeactivatedBy'] = $user->getLogin();
			}
		}

		if (empty($updatedFields['Tech_ID'])) {
			$updatedFields['Tech_ID'] = NULL;
        	$updatedFields['Date_Assigned'] = "";
			$updatedFields["Tech_FName"] = "";
			$updatedFields["Tech_FName"] = "";
			$updatedFields["Tech_LName"] = "";
			$updatedFields["TechEmail"] = "";
			$updatedFields["TechPhone"] = "";
			$updatedFields["Tech_Bid_Amount"] = NULL;
			$updatedFields["FLS_ID"] = NULL;
            $updatedFields["Date_In"] = NULL;
            $updatedFields["Date_Out"] = NULL;
            $updatedFields["Time_In"] = NULL;    //2012
            $updatedFields["Time_Out"] = NULL;   //2012
			//$updatedFields["Duration"] = NULL;   //2012
            $updatedFields["calculatedTechHrs"] = NULL;   //2012
            $updatedFields["TechMiles"] = NULL;  
            $updatedFields['Paperwork_Received']=0;
            $updatedFields['Incomplete_Paperwork']=0;
            $updatedFields['TechMarkedComplete']= 'False';
            $updatedFields['CallClosed']=0;
            $updatedFields['TechComments']="";    
            $updatedFields['MissingComments']="";    
            $updatedFields['bid_comments']="";
		}

        if($updatedFields['Tech_ID']!=$curWO['Tech_ID'])  //2012
        {
            if($curWO[Status]==WO_STATE_ASSIGNED || $curWO[Status]==WO_STATE_WORK_DONE || $curWO[Status]==WO_STATE_INCOMPLETE)
            {
                $updatedFields["Date_In"] = NULL;
                $updatedFields["Date_Out"] = NULL;
                $updatedFields["Time_In"] = NULL;    
                $updatedFields["Time_Out"] = NULL;   
				//$updatedFields["Duration"] = NULL; 
	            $updatedFields["calculatedTechHrs"] = NULL;   //2012
                $updatedFields["TechMiles"] = NULL; 
                $updatedFields['Paperwork_Received']=0;
                $updatedFields['Incomplete_Paperwork']=0;
                $updatedFields['TechMarkedComplete']= 'False';
                $updatedFields['CallClosed']=0;     
                $updatedFields['TechComments']="";  
                //704
				if (!empty($updatedFields['ClientComments'])) $updatedFields['ClientComments'].= "\n";
				$updatedFields['ClientComments'] .= $updatedFields['MissingComments'];
                $updatedFields['MissingComments']="";    
                $updatedFields['ReminderIncomplete']=0;
                $updatedFields['DateIncomplete']=null;
                //end 704
                $updatedFields['bid_comments']="";                                             
            }
		}

        if ($techAssignedOrReassigned && !empty($curWO['Tech_ID']) || !empty($curWO['Tech_ID']) && empty($updatedFields['Tech_ID'])) {
			// clear if tech is reassigned or tech unassigned
			$updatedFields["MileageReimbursement"] = NULL;
			$updatedFields["TripCharge"] = NULL;

			if ($workOrderAdditionalFields['PushWM'] == 1 && $techData["AcceptTerms"] == 'WM') {
				// accept offer in WM
				$bidAmount = $WMClass->acceptOffer(NULL, $techData["WMID"]);
			}
        }
		// End field clean up, now updating

		// Update Status Field
		$updatedFields["TechPaid"] = $curWO["TechPaid"];
		$updatedFields["Invoiced"] = $curWO["Invoiced"];
		$updatedFields["Status"] = Core_WorkOrder_Status::getStatusWithWorkOrder($updatedFields);
        //13763
        if($updatedFields["CheckedIn"]=="True" && !empty($updatedFields['Date_Checked_in']))
        {
            $dateCheckedIn = $updatedFields['Date_Checked_in'];
            $tmpStamp = strtotime($dateCheckedIn);
            $updatedFields['Date_In'] = date("Y-m-d",$tmpStamp);
            $updatedFields['Time_In'] = date("h:i A",$tmpStamp);
        }
        //end 13763
        
		$updatedChangedFields = array();
		$updatedFieldsConverted = Core_Database::convertQueryResult($updatedFields, $fieldListType);

		foreach ($updatedFieldsConverted as $k => $v) {
			if (array_key_exists($k, $curWO ) && $curWO[$k] === $v) continue;
				$updatedChangedFields[$k] = $updatedFields[$k];
		}
		$result = false;
		
		$result = Core_Database::update(Core_Database::TABLE_WORK_ORDERS, $updatedChangedFields, $db->quoteInto("WIN_NUM = ?", $wo_id));
		if ($result < 1) {
	    		$err = $errors->getErrors();
	    		
	    		if (!empty($err)){
	    			$errArr = $errors->getErrorArray();
		    		foreach($errArr as $error){
		    			if(strpos($error['description'], 'SQLSTATE[23000]') !== false){
		    				$errors->resetErrors();
		    				$errors->addError(7, 'Client WO ID # already exist');
		    				return API_Response::fail();
		    			}
		    		}
		    		return API_Response::fail();
	    		}


			$errors->addError(7, 'Incorrect SQL request');
			return API_Response::fail();
		}

		if ($this->mode == WO_UPDATE_MODE_NORMAL) {
			// pull in cretification from project
			if ($curWO['Project_ID'] != $updatedFields['Project_ID']) {
				$projectId = $updatedFields['Project_ID'];
				$pcs = new Core_ProjectCertifications();
				$certs = $pcs->getProjectCertification($projectId);
				if (!empty($wo_id)) {
					$wcs = new Core_WorkOrderCertifications();
					$certswo = $wcs->getWorkOrderCertification($wo_id);
				}
				else
					$certswo = array();

				// delete certifications removed by update
				$certs = empty($certs[$projectId]) ? array() : array_values($certs[$projectId]);
				$certswo = empty($certswo[$wo_id]) ? array() : array_values($certswo[$wo_id]);

				$diff = array_diff($certswo, $certs);
                   
				foreach ($diff as $cid) {
					$pc = new Core_WorkOrderCertification($wo_id, $cid);
					$pc->delete();
				}
				// save client credential
				foreach ($certs as $cid) {
					$pc = new Core_WorkOrderCertification($wo_id, $cid);
					$pc->save();
				}
			}		
		
			//--- 13418 update Total_Reimbursable_Expense
			$Api_WosClass = new Core_Api_WosClass();
			$result = $Api_WosClass->update_Total_Reimbursable_Expense($wo_id);
			$workOrderAdditionalFields['WINNUM'] = $wo_id;
			$this->insertWorkOrderAdditionalFields($workOrderAdditionalFields);

			$changed = array();
			$uploadingFields = API_WorkOrder::uploadFileFields(); 
                
			foreach ($updatedFieldsConverted as $key=>$value) {
				if ($value == $curWO[$key]) continue;
			
				if (in_array($key, $uploadingFields) ) {
					$changed[] = $key . "({$value})";
				} else {
					$changed[] = $key;
				}
			}
		}

		$changed = implode(",", $changed);
		Core_TimeStamp::createTimeStamp($wo_id, $user->getLogin(), "Work Order Updated: $changed", $updatedFields["Company_ID"], $updatedFields["Company_Name"], $updatedFields['Project_Name'], "", "", "");

		if ($obsync = Core_WorkOrder_OutboundSync_Factory::build($curWO["Company_ID"])) {
			$obsync->syncWithWorkOrder($wo_id, $curWO, $updatedFields, Core_WorkOrder_OutboundSync_Abstract::CONTEXT_CLIENT, $user->getLogin());
		}

		if ($this->mode == WO_UPDATE_MODE_NORMAL) {
			$this->baseVisitSave($wo_id, $baseVisit);
		}

		// End Updating, now send emails and do timestamps

		$techEmailed = false;

		$techInfo = NULL;
		if (!empty($updatedFields['Tech_ID']))

		if (!empty($updatedFields['Tech_ID']) && $changedFields == true) {
			$techInfo = new Core_Tech((int)$updatedFields["Tech_ID"]);
			if ($projectInfo === NULL) {
				$projectInfo = new Core_Project((int)$updatedFields["Project_ID"]);
				$projectInfo->setData();
			}
			$woChangedMail->setTech($techInfo);
			$woChangedMail->setProject($projectInfo);
			//$woChangedMail->loadProject($updatedFields['Project_ID']);
			//$woChangedMail->loadTech($updatedFields['Tech_ID']);
            $woChangedMail->assign('WorkOrderNo', $updatedFields['WO_ID']);
            $woChangedMail->assign('TB_UNID', $wo_id);
            $woChangedMail->assign('Approved', $updatedFields['Approved']);

            // send mail only after all conditions
		}
		
		if ($isrepublish || $is_reactivation) {
			if ($workOrderAdditionalFields['PushWM'] == 1) {
				$wm = new Core_WorkMarket($wo_id);
				$wm->stopWMAssignment(true, 0.00, 'FS Republished'); // void or cancel assignment
			}
		}

        if (($woChangedToPublished && $updatedFields['Deactivated'] == "False" && empty($updatedFields['Tech_ID']) || ($sentworknotificationmail==1) || $isrepublish) || $is_reactivation) {
			if ($updatedFields['isProjectAutoAssign']=='True' || $updatedFields['isProjectAutoAssign'] == 1) {
				$this->createAtJob($wo_id);
				if ($workOrderAdditionalFields['PushWM'] == 1) {
					$wm = new Core_WorkMarket($wo_id);
					$wm->startWMAssignment();
				}
			}
		}

        if ($woChangedToPublished && $updatedFields['Deactivated'] == "False" && empty($updatedFields['Tech_ID']) || ($sentworknotificationmail==1) || $isrepublish) {
			//if ($updatedFields['AutoBlastOnPublish'] == "False" && !empty($updatedFields['Zipcode'])) {
    		    $mail = new Core_BlastRecruitmentEmail();
    		    $mail->setWorkOrder($wo_id);
    		    $mail->setCompanyId($updatedFields["Company_ID"]);
                    $mail->setUser($user->getLogin());
    		    $mail->sendMail();
			//}
			if (!empty($updatedFields['Country']) && $updatedFields['Country'] != 'US' && $updatedFields['Country'] != 'CA') {
				// send notification of foreign work order published if needed
				$mail = new Core_EmailForPublishedForeignCountryWO();
				$mail->setWorkOrder($wo_id);
				$mail->setUserFullName($user->getContactName());
				$mail->sendMail();
			}
            if($sentworknotificationmail==1)
            {
                    Core_TimeStamp::createTimeStamp($wo_id, $user->getLogin(), "Work Order Re-Published", $updatedFields["Company_ID"], $updatedFields["Company_Name"], $updatedFields['Project_Name'], "", "", "");
            }
			else
			{
				Core_TimeStamp::createTimeStamp($wo_id, $user->getLogin(), "Work Order Published", $updatedFields["Company_ID"], $updatedFields["Company_Name"], $updatedFields['Project_Name'], "", "", "");
                $NoticeClass = new Core_Api_NoticeClass();
                $NoticeClass->createWOChangeStatusLog($wo_id,Core_Api_NoticeClass::WOStatusID_FOR_PUBLISHED);
				//--- 13457
				$apiUser = new Core_Api_User();
				if($user->isAdmin() && $user->isPM())
				{
					//$apiUser->ResetAccessAndMoreWhenPublishing($updatedFields['Company_ID'],$wo_id,$user->getLogin());//13995
				}
			}
        }

		// Missing wosEmailTechWhenCompleteUnchecked?

		$recalcList = array();
		if ($curWO['Tech_ID'] != $updatedFields['Tech_ID'] && !empty($curWO['Tech_ID']))
			$recalcList[$curWO['Tech_ID']] = 1;
		if ($curWO['BackOut_Tech'] != $updatedFields['BackOut_Tech'] && !empty($curWO['BackOut_Tech']))
			$recalcList[$curWO['BackOut_Tech']] = 1;
		if ($curWO['NoShow_Tech'] != $updatedFields['NoShow_Tech'] && !empty($curWO['NoShow_Tech']))
			$recalcList[$curWO['NoShow_Tech']] = 1;

/*		if (!empty($updatedFields['Tech_ID']))
			$recalcList[$updatedFields['Tech_ID']] = 1;
		if (!empty($updatedFields['BackOut_Tech']))
			$recalcList[$updatedFields['BackOut_Tech']] = 1;
		if (!empty($updatedFields['NoShow_Tech']))
			$recalcList[$updatedFields['NoShow_Tech']] = 1;
		
		if (sizeof($recalcList) > 0)
			Core_TechStats::recalcLifetimeStatsWithTechID(array_keys($recalcList));*/

			
		if ($techReassigned || $isrepublish) {
			// hide the previous tech's bids on reassign / republish
			Core_Database::update(Core_Database::TABLE_WORK_ORDER_BIDS, array("Hide" => 1), $db->quoteInto("WorkOrderID = ?", $wo_id) . " AND " . $db->quoteInto("TechID = ?", $curWO['Tech_ID']));
		}
		
        if ($techAssignedOrReassigned) {
            
            $mail  = new Core_EmailForAssignedTech();
			$mail->setWOData($updatedFields);
			$mail->setTech($updatedFields['Tech_ID']);
            $mail->setWorkOrder($wo_id);
            $mail->setProject($updatedFields['Project_ID']);
            $mail->setCompanyId($updatedFields["Company_ID"]);
            $mail->setCompanyName($updatedFields["Company_Name"]);
           
			//if(empty($curWO['Tech_ID']))
				$mail->setReasigned (0);
			//else if($curWO['Tech_ID']!=$updatedFields['Tech_ID'])
			//	$mail->setReasigned (1);

            $mail->sendMail();
           	//14064
          	Core_TimeStamp::createTimeStamp($wo_id, $user->getLogin(), "Work Order Assigned: Tech ID# ". $updatedFields['Tech_ID'], $updatedFields["Company_ID"], $updatedFields["Company_Name"], $updatedFields['Project_Name'], "", "", "");
            $NoticeClass = new Core_Api_NoticeClass();
            $NoticeClass->createWOChangeStatusLog($wo_id,Core_Api_NoticeClass::WOStatusID_FOR_ASSIGNED);
          	// mark as tech mailed, and dont send change mail
            $techEmailed = true;
            
            //743
            $isFullRestricted_old = $Core_Api_User->AreFullRestriction($companyID);
            $isPartialRestricted_old = $Core_Api_User->ArePartialRestriction($companyID);
            $isFullOpened_old = $Core_Api_User->AreFullorPartialRestriction($companyID);

            $beginDate = date("Y-m-d");
            $fieldvals=array('BeginDateForCountingNDaysRule'=>$beginDate);
            $Core_Api_User->saveClientCompanyAdditionalFields($curWO['Company_ID'],$fieldvals);
           // $Core_Api_User->setAllAccessOpened($curWO['Company_ID']); //13995
            
            $isFullRestricted = $Core_Api_User->AreFullRestriction($companyID);
            $isPartialRestricted = $Core_Api_User->ArePartialRestriction($companyID);
            $isFullOpened=$Core_Api_User->AreFullorPartialRestriction($companyID);
            $wonum = $wo_id;
            $wostatus = $curWO["Status"];
            $restrictedtype = 5;

            $issendmail=FALSE;
            if(($isFullRestricted != $isFullRestricted_old)
                    || ($isPartialRestricted != $isPartialRestricted_old)
                    || ($isFullOpened!=$isFullOpened_old))
            {
                $issendmail = TRUE;
        }

            if($issendmail)
            {
                $CompanyName = $user->getCompanyName();
                $apiemailrestricted->setCompanyId($companyID);
                $apiemailrestricted->setCompanyName($CompanyName);
                $apiemailrestricted->settoMail($AM.",".$CSD);
                $apiemailrestricted->setusername($user->getLogin());
                $apiemailrestricted->setrestrictedtype($restrictedtype);
                $apiemailrestricted->setwonum($wonum);
                $apiemailrestricted->setwostatus($wostatus);
                $apiemailrestricted->sendMail();
            }
            //end 743
        }
        if($techBackup){
            $mail  = new Core_EmailForBackupTech();
            $mail->setWOData($updatedFields);
            $mail->setTech($updatedFields['BU1FLSID']);
            $mail->setWorkOrder($wo_id);
            $mail->setProject($updatedFields['Project_ID']);
            $mail->setCompanyId($updatedFields["Company_ID"]);
            $mail->setCompanyName($updatedFields["Company_Name"]);
            $mail->sendMail();
			//Core_TimeStamp::createTimeStamp($wo_id, $user->getLogin(), "Work Order Assigned", $updatedFields["Company_ID"], $updatedFields["Company_Name"], $updatedFields['Project_Name'], "", "", "");

            // mark as tech mailed, and dont send change mail
			//$techEmailed = true;
        }

		if ($requireACSReset) {
			Core_IVR_Redial::resetCalls($wo_id);
			Core_IVR_Log::resetCalls($wo_id);
            Core_TimeStamp::createTimeStamp($wo_id, $user->getLogin(), "Reset ACS Calls", $updatedFields["Company_ID"], $updatedFields["Company_Name"], $updatedFields['Project_Name'], "", "", "");
            Core_TimeStamp::createWOACSTimeStamp($wo_id,$updatedFields["Company_ID"]);
        }

        if (!empty($updatedFields['WorkOrderReviewed']) && $updatedFields['WorkOrderReviewed']=='True' && $curWO['WorkOrderReviewed']=='False') {
          	Core_TimeStamp::createTimeStamp($wo_id, $user->getLogin(), "Work Order Reviewed", $updatedFields["Company_ID"], $updatedFields["Company_Name"], $updatedFields['Project_Name'], "", "", "");
        }

        if (!empty($updatedFields['TechCheckedIn_24hrs']) && $updatedFields['TechCheckedIn_24hrs']=='True' && $curWO['TechCheckedIn_24hrs']=='False') {
          	Core_TimeStamp::createTimeStamp($wo_id, $user->getLogin(), "Work Order Confirmed", $updatedFields["Company_ID"], $updatedFields["Company_Name"], $updatedFields['Project_Name'], "", "", "");
        }

        if (!empty($updatedFields["Tech_ID"]) && $updatedFields['TechMarkedComplete'] != $curWO['TechMarkedComplete']) {
			$val = $updatedFields['TechMarkedComplete']=='True' ? "Completed" : "Not Completed";
	        Core_TimeStamp::createTimeStamp($wo_id, $user->getLogin(), "Work Order Marked: $val", $updatedFields["Company_ID"], $updatedFields["Company_Name"], $updatedFields['Project_Name'], "", "", "");
                if($val == 'Completed'){
                    $NoticeClass = new Core_Api_NoticeClass();
                    $NoticeClass->createWOChangeStatusLog($wo_id,Core_Api_NoticeClass::WOStatusID_FOR_WORKDONE);
                }
			if ($updatedFields['TechMarkedComplete'] == "False") {
				$updatedFields['DateIncomplete'] = date('Y-m-d H:i:s');
		        Core_TimeStamp::createTimeStamp($wo_id, $user->getLogin(), "Work Order Marked Incomplete", $updatedFields["Company_ID"], $updatedFields["Company_Name"], $updatedFields['Project_Name'], "", "", "");
				$WMClass = new Core_WorkMarket($wo_id);
				$WMClass->rejectForPayment();
	            $woIncompleteMail = new Core_Mail_WoStatus_Incomplete();
				if ($techInfo == NULL)
					$techInfo = new Core_Tech((int)$updatedFields["Tech_ID"]);
				if ($projectInfo == NULL) {
					$projectInfo = new Core_Project((int)$updatedFields["Project_ID"]);
					$projectInfo->setData();
				}
		        $woIncompleteMail->assign('TB_UNID', $wo_id);
		        $woIncompleteMail->assign('WorkOrderNo', $updatedFields["WO_ID"]);
		        $woIncompleteMail->assign('missingInfo', $updatedFields["MissingComments"]);

				$woIncompleteMail->setTech($techInfo);
				$woIncompleteMail->setProject($projectInfo);

	            $woIncompleteMail->send();

	            // mark as tech mailed, and dont send change mail
	            $techEmailed = true;

			}
			else if (!empty($updatedFields['Approved']) && $updatedFields["Approved"] == "False") {
				//$techPayCalculator = new Core_TechPay();
				//$techPayCalculator->setWinNum($wo_id);
				//$techPayCalculator->updateTechPay();
			}
        }

        if ($woChangedToApproved) {
			$WMClass = new Core_WorkMarket($wo_id);
			$WMClass->approveForPayment();
	        Core_TimeStamp::createTimeStamp($wo_id, $user->getLogin(), "Work Order Approved", $updatedFields["Company_ID"], $updatedFields["Company_Name"], $updatedFields['Project_Name'], "", "", "");
                $NoticeClass = new Core_Api_NoticeClass();
                $NoticeClass->createWOChangeStatusLog($wo_id,Core_Api_NoticeClass::WOStatusID_FOR_APPROVED);
            $mail = new Core_EmailForApprovedTech();
			$mail->setCompanyId($updatedFields["Company_ID"]);
            $mail->setCompanyName($updatedFields["Company_Name"]);
            $mail->setTech($updatedFields['Tech_ID']);
            $mail->setWorkOrder($wo_id);
            $mail->setProject($updatedFields['Project_ID']);
            $mail->sendMail();

            // mark as tech mailed, and dont send change mail
            $techEmailed = true;
        }

        if (!empty($updatedFields['Deactivated']) && $updatedFields['Deactivated'] == "True" && $curWO['Deactivated'] == "False") {

            if (!empty($updatedFields['Tech_ID'])) {
                $mail  = new Core_EmailForDeactivatedWO();
    			$mail->setWOData($updatedFields);
    			$mail->setTech($updatedFields['Tech_ID']);
                $mail->setWorkOrder($wo_id);
                $mail->setProject($updatedFields['Project_ID']);
                $mail->setCompanyId($updatedFields["Company_ID"]);
                $mail->setCompanyName($updatedFields["Company_Name"]);
                $mail->sendMail();

                // mark as tech mailed, and dont send change mail
                $techEmailed = true;
            }
			if ($workOrderAdditionalFields['PushWM'] == 1) {
				$wm = new Core_WorkMarket($wo_id);
				$wm->stopWMAssignment(true, 0.00, $updatedFields['Reason_Deactivated']); // void or cancel assignment
			}
	        Core_TimeStamp::createTimeStamp($wo_id, $user->getLogin(), "Work Order Deactivated", $updatedFields["Company_ID"], $updatedFields["Company_Name"], $updatedFields['Project_Name'], "", "", "");

        }

        if (!empty($updatedFields['AbortFee']) && $updatedFields['AbortFee'] != $curWO['AbortFee']) {
	        Core_TimeStamp::createTimeStamp($wo_id, $user->getLogin(), "Abort Fee ". ($updatedFields['AbortFee']=='True'?'Checked':'Un-checked'), $updatedFields["Company_ID"], $updatedFields["Company_Name"], $updatedFields['Project_Name'], "", "", "");
        }

        if (!empty($updatedFields['Tech_ID']) && $changedFields) {
            // if $techEmailed == true, dont send to tech more mails
            $woChangedMail->send($techEmailed);
        }

		if ($updatedFields["Company_ID"] == "FLS") {
			if ($updatedFields['Update_Requested'] == "True" && $curWO['Update_Requested'] != $updatedFields['Update_Requested']) {
				$descForLog = "FLS Update Requested: ".$updatedFields['Update_Reason'];    
				Core_TimeStamp::createTimeStamp($wo_id,$user->getLogin(),$descForLog,$updatedFields["Company_ID"], $updatedFields["Company_Name"],$updatedFields['Project_Name'],"",$updatedFields['Tech_ID'],"");
                    
			    $mail  = new Core_EmailForUpdateRequested;
			    $objDateNow = new Zend_Date();
			    $date = $objDateNow->toString('MM/dd/yyyy');

			    $mail->setWOData(array(
					"DateTime_Stamp" => $date,
					"StartDate" => $updatedFields['StartDate'],
					"WO_ID" => $updatedFields['WO_ID'],
					"Project_Name" => $updatedFields['Project_Name'],
					"Update_Reason" => $updatedFields['Update_Reason']
				));
			    $mail->sendEmail();
			}
		}

        //--- 13763: EmailForTechAcceptedWO
        if(isset($updatedFields["WorkOrderReviewed"])){
            if($updatedFields["WorkOrderReviewed"] == "True" && $curWO['WorkOrderReviewed']=="False" ){
                $emailCore = new Core_EmailForTechAcceptedWO();
                $emailCore->sendMail($wo_id);
            }            
        }
        //--- 13763: EmailForTechConfirmedWO
        if(isset($updatedFields["TechCheckedIn_24hrs"])){
            if($updatedFields["TechCheckedIn_24hrs"] == "True" && $curWO['TechCheckedIn_24hrs']=="False" ){
                $emailCore = new Core_EmailForTechConfirmedWO();
                $emailCore->sendMail($wo_id);
            }            
        }
        //--- 13763: EmailForTechCheckedIN
        if(isset($updatedFields["CheckedIn"])){
            if($updatedFields["CheckedIn"] == "True" && $curWO['CheckedIn']=="False" ){
                $emailCore = new Core_EmailForTechCheckedIN();
                $emailCore->sendMail($wo_id);
            }            
        }

		return API_Response::success(array(new API_WorkOrder($wo_id)));
	}


	/**
	 * Function get list of WO Categories
	 *
	 * @param string $user
	 * @param string $pass
	 * @return API_Response
	 */
	public function getWOCategories($user, $pass)
	{
       	$logger = Core_Api_Logger::getInstance();
		$logger->setUrl('getWOCategories');
        $logger->setLogin($user);
		$logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');

		$authData = array('login'=>$user, 'password'=>$pass);
		$errors = Core_Api_Error::getInstance();
		$errors->resetErrors();

		$user = new Core_Api_User();
		$user->checkAuthentication($authData);

		if (!$user->isAuthenticate())
			return API_Response::fail();

		$common = new Core_Api_CommonClass;
		$result = $common->getWoCategories();
		$err = $errors->getErrors();
		if (!empty($err))
			return API_Response::fail();

		return API_Response::success($this->makeWOCategoryArray($result));
	}

	/**
	 * Function for getting Projects for current usre
	 *
	 * @param string $user
	 * @param string $pass
	 * @param array $project_ids
	 * @param string $sort
	 * @return API_Response
	 */
	public function getProjects($user, $pass, $project_ids, $sort)
	{
       	$logger = Core_Api_Logger::getInstance();
		$logger->setUrl('getProjects');
        $logger->setLogin($user);
		$logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');

		$authData = array('login'=>$user, 'password'=>$pass);
		$errors = Core_Api_Error::getInstance();

		$user = new Core_Api_User();
		$user->checkAuthentication($authData);

		if (!$user->isAuthenticate())
			return API_Response::fail();

		if (!is_array($project_ids)) $project_ids = array($project_ids);

		$db = Core_Database::getInstance();
		$select = $db->select()->from(Core_Database::TABLE_PROJECTS);


		$companyId  = $user->getContextCompanyId();
		$select->where('Project_Company_ID=?', $companyId);
		if (count($project_ids)) $select->where('Project_ID IN (?)', $project_ids);

		$result = Core_Database::fetchAll($select);

		$err = $errors->getErrors();
		if (!empty($err))
			return API_Response::fail();

//	    $xml = $this->makeProjectsXML($result);
//		return $xml;

		return API_Response::success($this->makeProjectsArray($result));
	}
	/**
	 * Function for getting Projects for current usre
	 *
	
	 * @param array $project_ids
	
	 * @return API_Response
	 */

	public function getProjectByID($project_ids)
	{

		if (!is_array($project_ids)) $project_ids = array($project_ids);

		$db = Core_Database::getInstance();
		$select = $db->select()->from(Core_Database::TABLE_PROJECTS);
		if (count($project_ids)) $select->where('Project_ID IN (?)', $project_ids);

		$result = Core_Database::fetchAll($select);

		return $result;
	}
	
	/**
	 * Function for getting Projects for current usre
	 *
	 * @param string $user
	 * @param string $pass
	 * @param array $project_ids
	 * @param string $sort
	 * @return API_Response
	 */
	public function getProjectsList($user, $pass, $filters, $sort, $offset = 0, $limit = 0, &$count)
	{
       	$logger = Core_Api_Logger::getInstance();
		$logger->setUrl('getProjects');
        $logger->setLogin($user);
		$logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');

		$authData = array('login'=>$user, 'password'=>$pass);
		$errors = Core_Api_Error::getInstance();

		$user = new Core_Api_User();
		$user->checkAuthentication($authData);

		if (!$user->isAuthenticate())
			return API_Response::fail();

		$db = Core_Database::getInstance();
		$select = $db->select()->from(Core_Database::TABLE_PROJECTS);

		$companyId  = $user->getContextCompanyId();
		$select->where('Project_Company_ID=?', $companyId);
		
		// apply filters
		if (is_array($filters) ) {
		    if (isset($filters['hideInactive']) && strtolower($filters['hideInactive']) == 'true') {
		        $select->where('Active = ?', 1);
		    }
		    
		    if (!empty($filters['letter'])) {
		        if ($filters['letter'] == '#') {
                    // first letter is numeric
		            $select->where("?", new Zend_Db_Expr("ORD(Project_Name) >= ORD(0) AND ORD(Project_Name) <= ORD(9)") );
		        } else {
		            $select->where("Project_Name LIKE ?", "{$filters['letter']}%");
		        }
		    }
		    
		}
		
		if (!empty($sort) ) {
	    	$select->order($sort);
		}
    		
		if ($offset != 0 || $limit != 0) {
			$select->limit($limit, $offset);
		}
		$result = new Core_Filter_Result($select);
		if ($count !== null) $count = count($result);

		$err = $errors->getErrors();
		if (!empty($err))
			return API_Response::fail();

//	    $xml = $this->makeProjectsXML($result);
//		return $xml;

		return API_Response::success($this->makeProjectsArray($result->toArray()));
	}

	/**
	 * Function for getting all Projects for current usre
	 *
	 * @param string $user
	 * @param string $pass
	 * @return API_Response
	 */
	public function getAllProjects($user, $pass, $createdByUser = NULL, $filter = NULL)
	{
/*       	$logger = Core_Api_Logger::getInstance();
		$logger->setUrl('getAllProjects');
        $logger->setLogin($user);
		$logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');

		$authData = array('login'=>$user, 'password'=>$pass);
		$errors = Core_Api_Error::getInstance();

		$user = new Core_Api_User();
		$user->checkAuthentication($authData);

		if (!$user->isAuthenticate())
			return API_Response::fail();

		$fieldsList = array('Project_ID','Project_Name','Active');
		$companyId  = $user->getContextCompanyId();

		$db = Core_Database::getInstance();
		$select = $db->select();

		$select->from(Core_Database::TABLE_PROJECTS, $fieldsList)
			->where("Project_Company_ID = ?", $companyId)
            ->order("Project_Name");

		$result = Core_Database::fetchAll($select);

		$err = $errors->getErrors();
		if (!empty($err))
			return API_Response::fail();

//	    $xml = $this->makeProjectsXML($result);
//		return $xml;

		return API_Response::success($this->makeProjectsArray($result));*/
		$search = new Core_Api_ProjectClass;
		return $search->getAllProjects($user, $pass, $createdByUser, $filter);
	}


	/**
	 * Function for getting list of countries
	 *
	 * @param string $user
	 * @param string $pass
	 * @return API_Response
	 */
	public function getCountries($user, $pass)
	{
       	$logger = Core_Api_Logger::getInstance();
		$logger->setUrl('getAllProjects');
        $logger->setLogin($user);
		$logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');

		$authData = array('login'=>$user, 'password'=>$pass);
		$errors = Core_Api_Error::getInstance();

		$user = new Core_Api_User();
		$user->checkAuthentication($authData);

		if (!$user->isAuthenticate())
			return API_Response::fail();

		$fieldsList = array('Code','Name');

		$db = Core_Database::getInstance();
		$select = $db->select();

		$select->from(Core_Database::TABLE_COUNTRIES, $fieldsList)
			->order('DisplayPriority ASC')
			->order('Name ASC');

		$result = Core_Database::fetchAll($select);

		$err = $errors->getErrors();
		if (!empty($err))
			return API_Response::fail();

//	    $xml = $this->makeProjectsXML($result);
//		return $xml;

		return API_Response::success($this->makeCountryArray($result));
	}


    /**
     * getDeactivationReasons
     *
     * @access public
     * @return API_Response
     */
	public function getDeactivationCodes()
	{
        return API_Response::success(array(
            1 => 'Work order kicked back',
            2 => 'Work order declined',
            3 => 'Not Sourced',
            4 => 'No bidding techs (within time needed)"',
            5 => 'Not approved',
            6 => 'Reassigned with a new work order',
            7 => 'Recruiting work order',
            8 => 'Rescheduled work',
            9 => 'Sourced elsewhere',
            10 => 'Tech no showed',
            11 => 'Work order cancelled',
            12 => 'Duplicate Work Order',
            13 => 'EU cxl - maximum call attempts'
        ));
	}
    /**
     * getPenaltyReasons
     *
     * @access public
     * @return API_Response
     */
	public function getPenaltyReasons()
	{
        return API_Response::success(array(
            1 => 'Early Arrival',
            2 => 'Late Arrival',
            3 => 'Admin Process'
        ));
	}
    /**
     * getCallTypes
     *
     * @access public
     * @return API_Response
     */
	public function getCallTypes()
	{
        return API_Response::success(array(
            1 => 'Install',
            2 => 'Service Call',
            3 => 'Recruiting'
        ));
	}
    /**
     * getStates
     *
     * @access public
     * @return API_Response
     * @deprecated since ALFA.C use Core_Api_CommonClass methods
     */
    public function getStates()
    {
        $search = new Core_Api_CommonClass();
        return $search->getStatesArray();
    }

    
    /**
     * getStates
     *
     * @access public
     * @return API_Response
     * @deprecated since ALFA.C use Core_Api_CommonClass methods
     */
    public function getCanadianStates()
    {
        $search = new Core_Api_CommonClass();
        return $search->getCanadianStatesArray();
    }
    

    public function getDistances()
    {
        return API_Response::success(array(
            "10" => "10 Miles",
            "25" => "25 Miles",
        	"50" => "50 Miles",
        	"75" => "75 Miles",
        	"100" => "100 Miles",
        	"150" => "150 Miles",
        	"225" => "225 Miles",
        	"300" => "300 Miles",
        	"500" => "500 Miles",
        	"1000" => "1,000 Miles",
        	"2500" => "2,500 Miles",
        	"5000" => "5,000 Miles",
        	"10000" => "10,000 Miles"
        ));
    }


	/**
	 * get parts for a work order
	 *
	 * @param string $user
	 * @param string $pass
	 * @param string $winNum
	 *
	 * @return API_Response
	 */
	public function getParts($user, $pass, $winNum) {
       		$logger = Core_Api_Logger::getInstance();
		$logger->setUrl('getParts');
        	$logger->setLogin($user);
		$logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');

		$authData = array('login'=>$user, 'password'=>$pass);
		$errors = Core_Api_Error::getInstance();

	        $user = new Core_Api_User();
	        $user->checkAuthentication($authData);

		if (!$user->isAuthenticate())
			return API_Response::fail();
	        $companyID = $user->getContextCompanyId();

		$parts = new Core_PartEntry(null, $winNum, $companyID);
		$objects = $parts->toAPI_PartEntry();

		$err = $errors->getErrors();
		if (!empty($err))
			return API_Response::fail();

		return API_Response::success($objects);
	}

	/**
	 * get part entry
	 *
	 * @param string $user
	 * @param string $pass
	 * @param string $winNum
	 * @param string $partID
	 *
	 * @return API_Response
	 */

	public function getPartEntry($user, $pass, $winNum, $partID)
	{
	       	$logger = Core_Api_Logger::getInstance();
		$logger->setUrl('getPartEntry');
	        $logger->setLogin($user);
		$logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');

		$authData = array('login'=>$user, 'password'=>$pass);
		$errors = Core_Api_Error::getInstance();

		$user = new Core_Api_User();
		$user->checkAuthentication($authData);

		if (!$user->isAuthenticate())
			return API_Response::fail();
	        $companyID = $user->getContextCompanyId();

		$parts = new Core_PartEntry(array($partID), $winNum, $companyID);

		$err = $errors->getErrors();
		if (!empty($err))
			return API_Response::fail();

		$objects = $parts->toAPI_PartEntry();
		return API_Response::success($objects);
	}

	private function updatePartCount($winNum) {
		$p = new Core_PartEntry();
		return $p->updatePartCount($winNum);
	}
	/**
	 * create part entry
	 *
	 * @param string $user
	 * @param string $pass
	 * @param string $winNum
	 * @param API_PartEntry $partEntry
	 *
	 * @return API_Response
	 */
public function createPartEntry($user, $pass, $winNum, $partEntry) {

		if (is_string($partEntry)) $data = $partEntry;
		else {
			$dom = new DOMDocument("1.0");
			$dom->appendChild($partEntry->toXMLElement($dom));
			$data = $dom->saveXML();
		}

	       	$logger = Core_Api_Logger::getInstance();
		$logger->setUrl('createPartEntry');
	        $logger->setLogin($user);
		$logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');

		$authData = array('login'=>$user, 'password'=>$pass);
		$errors = Core_Api_Error::getInstance();

		$user = new Core_Api_User();
		$user->checkAuthentication($authData);

		if (!$user->isAuthenticate())
			return API_Response::fail();
	        $companyID = $user->getContextCompanyId();

		if (empty($data))
			$errors->addError(2, 'parts XML is empty but required');

		$err = $errors->getErrors();
		if (!empty($err))
			return API_Response::fail();

		if ($partEntry instanceof API_PartEntry)
			$partData = $partEntry;
		else {
			$partData = $this->getDataFromXMLGeneric($data);
			$entry = array();
			$entry[0] = $partData["NewPart"];
			$entry[0]["IsNew"] = "True";
			$entry[0]["Consumable"] = $partData["Consumable"];
			$entry[0]["PartEntryID"] = $partID;
			$entry[1] = $partData["ReturnPart"];
			$entry[1]["IsNew"] = "False";
			$partData = API_PartEntry::partEntryWithResult($entry);
		}

		$result = Core_PartEntry::create($partData, $winNum);

		$err = $errors->getErrors();
		if (!empty($err))
			return API_Response::fail();

		$objects = array(new API_PartEntry($result, NULL));

		return API_Response::success($objects);
	}	

	/**
	 * update part entry
	 *
	 * @param string $user
	 * @param string $pass
	 * @param string $winNum
	 * @param string $partID
	 * @param API_PartEntry $partEntry
	 *
	 * @return API_Response
	 */
	public function updatePartEntry($user, $pass, $winNum, $partID, $partEntry) {
		if (is_string($partEntry)) $data = $partEntry;

		else {
			$dom = new DOMDocument("1.0");
			$dom->appendChild($partEntry->toXMLElement($dom));
			$data = $dom->saveXML();
		}

       		$logger = Core_Api_Logger::getInstance();
		$logger->setUrl('updatePartEntry');
	        $logger->setLogin($user);
		$logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');

		$authData = array('login'=>$user, 'password'=>$pass);
		$errors = Core_Api_Error::getInstance();

		$user = new Core_Api_User();
		$user->checkAuthentication($authData);

		if (!$user->isAuthenticate())
			return API_Response::fail();
	        $companyID = $user->getContextCompanyId();

		if (empty($data))
			$errors->addError(2, 'parts XML is empty but required');

		$err = $errors->getErrors();
		if (!empty($err))
			return API_Response::fail();

		if ($partEntry instanceof API_PartEntry)
			$partData = $partEntry;
		else {
			$partData = $this->getDataFromXMLGeneric($data);
			$entry = array();
			$entry[0] = $partData["NewPart"];
			$entry[0]["IsNew"] = "True";
			$entry[0]["Consumable"] = $partData["Consumable"];
			$entry[0]["PartEntryID"] = $partID;
			$entry[1] = $partData["ReturnPart"];
			$entry[1]["IsNew"] = "False";
			$partData = API_PartEntry::partEntryWithResult($entry);
		}

		Core_PartEntry::update($partData, $winNum);

		$err = $errors->getErrors();
		if (!empty($err))
			return API_Response::fail();

		$objects = array(new API_PartEntry($partID, NULL));

		return API_Response::success($objects);
	}

	/**
	 * update part entry
	 *
	 * @param string $user
	 * @param string $pass
	 * @param string $winNum
	 * @param string $partID
	 *
	 * @return API_Response
	 */
	public function deletePart($user, $pass, $winNum, $partID) {
	       	$logger = Core_Api_Logger::getInstance();
		$logger->setUrl('deletePart');
	        $logger->setLogin($user);
		$logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');

		$authData = array('login'=>$user, 'password'=>$pass);
		$errors = Core_Api_Error::getInstance();

		$user = new Core_Api_User();
		$user->checkAuthentication($authData);

		if (!$user->isAuthenticate())
			return API_Response::fail();
        $companyID = $user->getContextCompanyId();

		$partData = new API_PartEntry($partID, false);

		Core_PartEntry::delete($partData, $winNum);

		$err = $errors->getErrors();
		if (!empty($err))
			return API_Response::fail();

		$objects = array(new API_PartEntry($partID, NULL));

		return API_Response::success($objects);
	}

	/* ------------------------------- */

	private function doesPartExist($companyID, $winNum, $partEntryID) {
		$errors = Core_Api_Error::getInstance();
		// check work order exists
		$result = $this->getWorkOrdersWithWinNum($winNum, "WIN_NUM", "", $companyID);

        if (empty($result[0])) {
        	$errors->addError(2, 'WinNum '.$winNum.' does not exist');
        	return FALSE;
        }

		$resultPart = Core_Caspio::caspioSelectAdv(TABLE_PART_ENTRIES, "id", "id = '$partEntryID' AND WinNum = '$winNum' AND Deleted = '0'", '');

		if (@$resultPart[0]["id"] != $partEntryID) {
		    $errors->addError(2, "PartEntryID $partEntryID does not exist");
		    return FALSE;
		}
		return TRUE;
	}

	private function insertPart($data) {
		$errors = Core_Api_Error::getInstance();
		$data["ETA"] = empty($data["ETA"]) ? NULL : $data["ETA"];

		$values = array_values($data);
/*		foreach ($values as $k => $val) {
			if ($val == NULL) continue;
			$values[$k] = Core_Caspio::caspioEscape($val);
		}*/

		// create New Part
		$result = Core_Caspio::caspioInsert(TABLE_PARTS, array_keys($data), $values);

		if (!$result) {
			$err = $errors->getErrors();
			if (!empty($err))
				return FALSE;

			$errors->addError(7, 'Incorrect SQL request');
			return FALSE;
		}

		return TRUE;
	}

	private function updatePart($data) {
		$errors = Core_Api_Error::getInstance();
		$data["ETA"] = empty($data["ETA"]) ? NULL : $data["ETA"];
		$isNew = isset($data["IsNew"]) && $data["IsNew"] == "True" ? 1 : 0;
		$partEntryID = $data["PartEntryID"];

		$values = array_values($data);
/*		foreach ($values as $k => $val) {
			if ($val == NULL) continue;
			$values[$k] = Core_Caspio::caspioEscape($val);
		}*/

		$result = Core_Caspio::caspioUpdate(TABLE_PARTS, array_keys($data), $values, "PartEntryID = '$partEntryID' AND IsNew = '$isNew'");

		if ($result == 0) {
			$result = Core_Caspio::caspioInsert(TABLE_PARTS, array_keys($data), array_values($data));
		}

		if (!$result) {
			$err = $errors->getErrors();
			if (!empty($err))
				return FALSE;

			$errors->addError(7, 'Incorrect SQL request');
			return FALSE;
		}

		return TRUE;
	}

	private static function errorsToAPIErrors($errors) {
		$ret = array();
		foreach ($errors as $key => $val) {
			$ret[] = new API_Error($val["error_id"], $val["id"], $val["description"]);
		}
		return $ret;
	}

	private function makePartsArray($result) {
		$parts = array();
		$entryList = array();

	    if (!empty($result[0])) {
			foreach ($result as $key=>$value) {
				if (!array_key_exists($value['PartEntryID'], $entryList)) {
					// New Part Entry
					$entryList[$value['PartEntryID']] = array();
				}
				$entryList[$value['PartEntryID']][] = $value;
			}
			foreach ($entryList as $key=>$value) {
				$part = API_PartEntry::partEntryWithResult($value);
				$parts[] = $part;
			}
	    }
		return $parts;
	}

/*	private function makeWorkOrderArray($result) {
		$wo = array();
	    if (!empty($result[0])) {
			foreach ($result as $key=>$value) {
				$wo[] = API_WorkOrder::workOrderWithResult($value);
			}
	    }

		return $wo;
	}*/

	private function makeWOCategoryArray($result)
	{
		$wo = array();
	    if (!empty($result[0])) {
			foreach ($result as $key=>$value) {
				$wo[] = API_WOCategory::woCategoryWithResult($value);
			}
	    }
		return $wo;
	}
    private function makeCustomersArray($result)
	{
		$wo = array();
	    if (!empty($result[0])) {
			foreach ($result as $key=>$value) {
				$wo[] = Core_Api_CustomerUser::customerWithResult($value);
			}
	    }
		return $wo;
	}
    private function makeClientsArray($result)
	{
		$wo = array();
	    if (!empty($result[0])) {
			foreach ($result as $key=>$value) {
				$wo[] = Core_Api_User::clientWithResult($value);
			}
	    }
		return $wo;
	}
	private function makeProjectsArray($result)
	{
		$wo = array();
	    if (!empty($result[0])) {
			foreach ($result as $key=>$value) {
				$wo[] = API_Project::projectWithResult($value);
			}
	    }
		return $wo;
	}
    private function makeContactLogArray($result)
	{
		$wo = array();
	    if (!empty($result[0])) {
			foreach ($result as $key=>$value) {
				$wo[] = API_Project::projectWithResult($value);
			}
	    }
		return $wo;
	}


    private function makeCountryArray($result)
	{
		$wo = array();
	    if (!empty($result[0])) {
			foreach ($result as $key=>$value) {
				$wo[] = API_Country::countryWithResult($value);
			}
	    }
		return $wo;
	}

	private function makePartsXML($result)
	{
		$dom = new DOMDocument("1.0");
		$root = $dom->createElement('Parts');
		$entryList = array();
		$entry = NULL;

	    if (!empty($result[0])) {
		    foreach ($result as $key=>$value) {

				if (!array_key_exists($value['PartEntryID'], $entryList)) {
					// New Part Entry
	                $entry = $dom->createElement('PartEntry');
					$entryList[$value['PartEntryID']] = "1";
	                $id = $dom->createAttribute('id');
	                $id->appendChild($dom->createTextNode($value['PartEntryID']));
	                $entry->appendChild($id);

			if (array_key_exists("Consumable", $value)) {
				$consumable = $value["Consumable"];
				unset($value["Consumable"]);
				$element = $dom->createElement("Consumable");
				$element->appendChild($dom->createTextNode($consumable));
				$entry->appendChild($element);
			}


	                $root->appendChild($entry);
				}

				$this->makePartDOM($dom, $entry, $value);
            }
	    }
	    $dom->appendChild($root);
		return $dom->saveXML();

	}

	private function makePartXML($result)
	{
		$dom = new DOMDocument("1.0");
        $entry = $dom->createElement('PartEntry');
		if (!empty($result[0])) {
			$value = $result[0];
	        $id = $dom->createAttribute('id');
	        $id->appendChild($dom->createTextNode($value['PartEntryID']));
	        $entry->appendChild($id);

		    foreach ($result as $k=>$value) {
				if (array_key_exists("Consumable", $value)) {
					$consumable = $value["Consumable"];
					unset($value["Consumable"]);
					$element = $dom->createElement("Consumable");
					$element->appendChild($dom->createTextNode($consumable));
					$entry->appendChild($element);
				}

				$this->makePartDOM($dom, $entry, $value);
			}
		}
		$dom->appendChild($entry);
		return $dom->saveXML();
	}

	private function makePartDOM($dom, &$entry, $value)
	{
		$ignorePartsField = array("id" => 1, "IsNew" => 1, "PartEntryID" => 1);
		$ignoreNewPartsField = array("ShippingAddress" => 1, "ShipTo" => 1);
		$ignoreReturnPartsField = array("RMA" => 1, "Instructions" => 1);
		$dateFields = array("ETA" => 1);

		$isNew = $value["IsNew"] == "True";
		$part =  $isNew ? $dom->createElement("NewPart") : $dom->createElement("ReturnPart");
		$ignoreList = $isNew ? $ignoreReturnPartsField : $ignoreNewPartsField;

		foreach ($value as $k=>$v) {
			if (array_key_exists($k, $ignorePartsField) || array_key_exists($k, $ignoreList)) continue;
			if ($v == "NULL")
				$val = "";
			else if (!empty($v) && array_key_exists($k, $dateFields )) {
				if ($ts = strtotime($v))
					$val = date("m/d/Y", $ts);
				else
					$val = NULL;
			}
			else
				$val = $v;

			$element = $dom->createElement($k);
			$element->appendChild($dom->createTextNode( htmlspecialchars($val, ENT_QUOTES) ));
			$part->appendChild($element);
			$entry->appendChild($part);
		}
	}

	private function makeProjectsXML($result)
	{
		$dom = new DOMDocument("1.0");
		$root = $dom->createElement('Projects');
	    if (!empty($result[0])) {
		    foreach ($result as $key=>$value) {
                $pj = $dom->createElement('Project');

                $id = $dom->createAttribute('ID');
                $name = $dom->createAttribute('NAME');

                $id->appendChild($dom->createTextNode($value['Project_ID']));
                $name->appendChild($dom->createTextNode($value[' Project_Name']));

                $pj->appendChild($id);
                $pj->appendChild($name);

                $root->appendChild($pj);
            }
	    } else {
//			$root->appendChild($dom->createTextNode('No results'));
	    }
	    $dom->appendChild($root);
		return $dom->saveXML();
	}

	private function makeWorkOrdersXML($result)
	{
		$dom = new DOMDocument("1.0");
		$root = $dom->createElement('WOS');
	    if (!empty($result[0])) {
		    foreach ($result as $key=>$value) {
			    $wo = $dom->createElement('WO');
			    foreach ($value as $k=>$val) {
			        if (!in_array( strtoupper($k), $this->internalFields)) {
    			    	$field = $k == 'TB_UNID' ? $dom->createElement('WIN_NUM') :$dom->createElement($k);
    			    	$field->appendChild($dom->createTextNode($val));
    			    	$wo->appendChild($field);
			        }
			    }
			    $root->appendChild($wo);
		    }
	    }
	    $dom->appendChild($root);
		return $dom->saveXML();
	}

	private function makeWorkOrderXML($result)
	{
		$dom = new DOMDocument("1.0");
		$wo = $dom->createElement('WO');
   		if (!empty($result)) {
		    foreach ($result as $k=>$val) {
		        if (!in_array(strtoupper($k), $this->internalFields)) {
    			    $field = $k == 'TB_UNID' ? $dom->createElement('WIN_NUM') :$dom->createElement($k);

			    	$field->appendChild($dom->createTextNode( htmlspecialchars($val, ENT_QUOTES) ));
			    	$wo->appendChild($field);
		        }
		    }
   		}
   		$dom->appendChild($wo);
		return $dom->saveXML();
	}

	private function getTableDesign($tableName)
	{
		$columns = Core_Caspio::caspioGetTableDesign($tableName);
        $fieldList = array();


        foreach ($columns as $info) {
	        $fieldInfo = explode(",",$info);
	        $fieldList[] = trim($fieldInfo[0]);
        }

        $fieldList = implode(',', $fieldList);

        return $fieldList;
	}

	private function getWorkOrderrequiredFields()
	{
		$requiredFields = array('WO_ID','Headline');
		return $requiredFields;
	}

	public function createAtJob($woId)
	{
//	    $cache = Core_Cache_Manager::factory();
//        $cfgSite = $cache->load('cfg_site_xml');
		$cfgSite = Zend_Registry::get('CFG_SITE');
	    $secureStr = md5($cfgSite->site->autoassign->secure_string . $woId);
	    $url = $cfgSite->site->autoassign->url . "?TB_UNID=".urlencode($woId)."&secure_str=$secureStr";

	    $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url); // set url to post to
        curl_setopt($ch, CURLOPT_FAILONERROR, 0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);// allow redirects
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1); // return into a variable
        curl_setopt($ch, CURLOPT_TIMEOUT, 20); // times out after 4s
        curl_setopt($ch, CURLOPT_POST, 0); // set POST method
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        $result = curl_exec($ch); // run the whole process
        curl_close($ch);
	}

	private function excludeFields(&$fieldsList)
	{
		$ro = API_WorkOrder::readOnlyFields();
	    foreach ($fieldsList as $key=>$field) {
	    	if ( in_array( strtoupper($key), $ro) ) {
	    		unset($fieldsList[$key]);
	    	}
	    }
	}

	private function resetFieldsForCopyWO(&$data)
	{
	    //$data[''] = trim($arr[0], "'") . " - " . date("mdYHis"); // This is a unique field, adding dateTime in order to ensure uniqueness of the newly created work order.
	    date_default_timezone_set('US/Central');
        $data['DateEntered'] = date('m/d/Y h:i:s A');
		$data['StartDate'] = NULL; // Reset StartDate
		$data['EndDate'] = NULL; // Reset EndDate
		$data['Tech_FName'] = NULL; // Reset Tech_FName
		$data['Approved'] = '0'; // Reset Approved
		$data['ShowTechs'] = '0'; // Reset ShowTechs
		$data['Tech_ID'] = NULL; // Reset Tech_ID
		$data['FLS_ID'] = NULL; // Reset FLS_ID
		$data['TechMarkedComplete'] = '0'; // Reset TechMarkedComplete
		$data['TechComments'] = ''; // Reset TechComments
		$data['MissingComments'] = ''; // Reset MissingComments
		$data['Tech_LName'] = ''; // Reset Tech_LName
		$data['TechPaid'] = '0'; // Reset TechPaid
		$data['DatePaid'] = NULL; // Reset DatePaid
		$data['PayAmount'] = ''; // Reset PayAmount
		$data['CheckedIn'] = '0'; // Reset CheckedIn
		$data['Checkedby'] = ''; // Reset Checkedby
		$data['CheckInNotes'] = ''; // Reset CheckInNotes
		$data['StartTime'] = ''; // Reset StartTime
		$data['StoreNotified'] = '0'; // Reset StoreNotified
		$data['Tech_Bid_Amount'] = NULL; // Reset Tech_Bid_Amount
		$data['Qty_Applicants'] = '0'; // Reset Qty_Applicants
		$data['Invoiced'] = '0'; // Reset Invoiced
		$data['DateInvoiced'] = ''; // Reset DateInvoiced
		$data['DateApproved'] = NULL; // Reset DateApproved
		$data['PayApprove'] = ''; // Reset PayApprove
		$data['WorkOrderReviewed'] = ''; // Reset WorkOrderReviewed
		$data['Deactivated'] = '0'; // Reset Deactivated70
		$data['TechCheckedIn_24hrs'] = '0'; // Reset TechCheckedIn_24hrs
		$data['TechCalled'] = ''; // Reset TechCalled72
		$data['CalledBy'] = ''; // Reset CalledBy73
		$data['CallDate'] = ''; // Reset CallDate74
		$data['NoShow_Tech'] = NULL; // Reset NoShow_Tech
		$data['Time_In'] = ''; // Reset Time_In
		$data['Time_Out'] = ''; // Reset Time_Out
		$data['TechEmail'] = ''; // Reset TechEmail
		$data['TechPhone'] = ''; // Reset TechPhone
		$data['Date_Assigned'] = NULL; // Reset Date_Assigned
		$data['Update_Requested'] = '0'; // Reset Update_Requested
		$data['Update_Reason'] = ''; // Reset Update_Reason
		$data['Additional_Pay_Amount'] = ''; // Reset Additional_Pay_Amount
		$data['Add_Pay_AuthBy'] = ''; // Reset Add_Pay_AuthBy
		$data['Work_Out_of_Scope'] = '0'; // Reset Work_Out_of_Scope
		$data['OutOfScope_Reason'] = ''; // Reset OutOfScope_Reason
		$data['Site_Complete'] = '0'; // Reset Site_Complete
		$data['Add_Pay_Reason'] = ''; // Reset Add_Pay_Reason
		$data['BackOut_Tech'] = NULL; // Reset BackOut_Tech
		$data['EndTime'] = ''; // Reset EndTime
		$data['lastModBy'] = ''; // Reset lastModBy
		$data['lastModDate'] = ''; // Reset lastModDate
		$data['approvedBy'] = ''; // Reset approvedBy
		$data['Date_Completed'] = ''; // Reset Date_Completed
		$data['Deactivated_Reason'] = ''; // Reset Deactivated_Reason
		$data['OutofScope_Amount'] = ''; // Reset OutofScope_Amount101
		$data['PayAmount_Final'] = ''; // Reset PayAmount_Final102
		$data['Site_Contact_Name'] = ''; // Reset Site_Contact_Name
		$data['Check_Number'] = ''; // Reset Check_Number
		$data['MovedToMysql'] = '0'; // Reset MovedToMysql106
		$data['StartRange'] = ''; // Reset StartRange108
		$data['Duration'] = ''; // Reset Duration
		$data['calculatedInvoice'] = ''; // Reset calculatedInvoice
		$data['calculatedTotalTechPay'] = ''; // Reset calculatedTotalTechPay
		$data['calculatedTechHrs'] = ''; // Reset calculatedTechHrs
		$data['baseTechPay'] = ''; // Reset baseTechPay
		$data['PricingCalculationText'] = ''; // Reset PricingCalculationText
		$data['TripCharge'] = NULL; // Reset TripCharge
		$data['MileageReimbursement'] = NULL; // Reset MileageReimbursement
		$data['MaterialsReimbursement'] = NULL; // Reset MaterialsReimbursement
		$data['TechnicianFee'] = ''; // Reset TechnicianFee
		$data['TechnicianFeeComments'] = ''; // Reset TechnicianFeeComments
		$data['ExpediteFee'] = '0'; // Reset ExpediteFee
		$data['AbortFee'] = '0'; // Reset AbortFee
		$data['AfterHoursFee'] = '0'; // Reset AfterHoursFee
		$data['OtherFSFee'] = ''; // Reset OtherFSFee
		$data['TechMiles'] = ''; // Reset TechMiles
		$data['PricingRuleApplied'] = ''; // Reset PricingRuleApplied
		$data['AbortFeeAmount'] = '0'; // Reset AbortFeeAmount
		$data['PricingRan'] = '0'; // Reset PricingRan
		$data['General1'] = ''; // Reset General1
		$data['General2'] = ''; // Reset General2
		$data['General3'] = ''; // Reset General3
		$data['General4'] = ''; // Reset General4
		$data['General5'] = ''; // Reset General5
		$data['General6'] = ''; // Reset General6
		$data['General7'] = ''; // Reset General7
		$data['General8'] = ''; // Reset General8
		$data['General9'] = ''; // Reset General9
		$data['General10'] = ''; // Reset General10
		return true;
	}

	private function getDataFromXML($xml)
	{
	    $result = array();
		$dom = new DOMDocument("1.0");
		$dom->loadXML($xml);
		$nodes = $dom->getElementsByTagName('WO')->item(0)->childNodes;

		foreach ($nodes as $node)
		{
			$result[$node->nodeName] = $node->nodeValue;
		}

		return $result;
	}
    private function getDataFromProjectXML($xml)
	{
	    $result = array();
		$dom = new DOMDocument("1.0");
		$dom->loadXML($xml);
		$nodes = $dom->getElementsByTagName('Project')->item(0)->childNodes;

		foreach ($nodes as $node)
		{
			$result[$node->nodeName] = $node->nodeValue;
		}

		return $result;
	}

	private function getDataFromXMLGeneric($xml)
	{
	    $result = array();
		$dom = new DOMDocument("1.0");
		$dom->loadXML($xml);

		$result = $this->nodeToArray($dom->documentElement);

		return $result;
	}

	private function nodeToArray($node) {
	    $childs = array();
	    $text = '';
		if ($node->hasChildNodes()) {
			$result = array();
			$children = $node->childNodes;
			foreach ($children as $child) {
			    if ($child->nodeName == "#text") {
                    $text .= $child->nodeValue;
                } else {
			        $childs[$child->nodeName] = $this->nodeToArray($child);
                }
			}
		}
		return empty($childs) ? $text : $childs;
	}

/*	private function getDataFromXML($xml)
	{
	    if (is_string($xml)) {
	    	$xml = simplexml_load_string($xml);
	    }
	    $result = array();
		foreach ($xml->parameter as $key=>$parameter) {
			$f_name = trim($parameter['fieldName'],"`'");
		    $result["$f_name"] = trim($parameter['fieldValue'], "`'");
		}
		return $result;
	}*/

	private function numberWeekDays($startDate, $endDate) {
			$startDateTS = strtotime($startDate);
			$endDateTS = strtotime($endDate);

			$startDayOfWeek = date("w", $startDateTS);
			$endDayOfWeek = date("w", $endDateTS);

			$daysToNextSunday = 7 - $startDayOfWeek;
			$daysToPreviousSunday = $endDayOfWeek;
			$nextSundayTS = strtotime("+$daysToNextSunday days", $startDateTS);
			$previousSundayTS = strtotime("-$daysToPreviousSunday days", $endDateTS);

			$numWeekDays = 0;

			if ($nextSundayTS < $endDateTS) {
				// date range is more than one week
				$startWeekNumWeekDays = 5 - ($startDayOfWeek - 1 <= -1 ? 0 : $startDayOfWeek - 1);
				$endWeekNumWeekDays = ($endDayOfWeek == 6 ? 5 : $endDayOfWeek);

				$numDaysBetweenFirstAndLastWeek = ($previousSundayTS - $nextSundayTS) / 60 / 60 / 24 / 7 * 5;

				$numWeekDays = $startWeekNumWeekDays + $endWeekNumWeekDays + $numDaysBetweenFirstAndLastWeek;
			}
			else {
				$startWeekNumWeekDays = 5 - ($startDayOfWeek - 1 <= -1 ? 0 : $startDayOfWeek - 1);
				$endWeekNumWeekDaysNotIncluded = 5 - ($endDayOfWeek == 6 ? 5 : $endDayOfWeek);
				$numWeekDays = $startWeekNumWeekDays - $endWeekNumWeekDaysNotIncluded;
			}
			return $numWeekDays;
	}

	private function calculateSourceByDate($startDateTS, $endDateTS, $dateEntered, $numWeekDays) {
		$sourceByDateTS = $endDateTS; // use Project Start date by default

       //NEW CODE #12275 #12199
        if ($numWeekDays<16) {
            $sourceByDateTS = $startDateTS;
        }else{
            $sourceByDateTS = strtotime("-1 day",strtotime(date('m/d/Y',$endDateTS)));
        }
        return $sourceByDateTS;

        //OLD CODE
//		if ($numWeekDays > 5 && $numWeekDays < 16) {
//			// use Date record created
//			$sourceByDateTS = strtotime(date("m/d/Y", strtotime($dateEntered)));
//
//			// Can't count start day so subtract, initally start with Project Date
//			$sourceByDateTS = strtotime("-1 day", $sourceByDateTS);
//		}
//
//		if ($numWeekDays < 6)
//			$sourceByDateTS = strtotime(date("m/d/Y", strtotime($dateEntered)));
//		else if ($numWeekDays > 5 && $numWeekDays < 16) {
//			for ($i = 0; $i <= 5; ++$i) {
//				$dayNum = date("w", $sourceByDateTS);
//				if ($dayNum == 0 || $dayNum == 6)
//					--$i;
//				$sourceByDateTS = strtotime("+1 days", $sourceByDateTS);
//			}
//		}
//		else {
//			$sourceByDateTS = strtotime("-1 days", $sourceByDateTS);
//			for ($i = 10; $i >= 1; $i--) {
//				$dayNum = date("w", $sourceByDateTS);
//				if ($dayNum == 0 || $dayNum == 6)
//					++$i;
//				$sourceByDateTS = strtotime("-1 days", $sourceByDateTS);
//
//			}
//		}
//
//		return $sourceByDateTS;
	}
	
   //13970	
   public function updateClientProfile($login, $hash, $data,
            $useUserSession=true,
            $VALIDATION_TYPE = Core_Validation_Factory::VALIDATION_TYPE_CLIENT_USER_UPDATE)
   {
        $errors = Core_Api_Error::getInstance();

        $authData = array('login'=>$login, 'password'=>$hash);
        $user = new Core_Api_User();
        $user->checkAuthentication($authData);
        
        if (!$user->isAuthenticate()) return API_Response::fail();

        $logger = Core_Api_Logger::getInstance();
        $logger->setUrl('updateClient');
        $logger->setLogin($login);
        $logger->addLog(LOGGER_ERROR_NO, null, 'db request');

        //13970
        $clientInfo = $user->load(
            array(
                'login' => $login,
                'password' => $hash
            ),$useUserSession
        );
  
       $id = $clientInfo->ClientID;
       if (!empty($id) ) {
            $user = new Core_Api_User($clientInfo->ClientID);
            $user->loadById();

            $datatoupdate = array();
			$datatovalidate = array();
            foreach($data as $field=>$val){
                if($val != $user->$field) {
					$datatoupdate[$field] = $val;
					$datatovalidate[$field] = $val;
		}
				else $datatovalidate[$field] = $user->$field;
           }

			if (empty($datatovalidate['Password'])) $datatovalidate['Password'] = 'bypass78'; // bypass password validation for old password

			//$datatoupdate['CompanyName'] = $user->CompanyName;
            if(empty($datatoupdate)) return API_Response::success(array($user));
            //13970
            $factory = new Core_Validation_Factory($VALIDATION_TYPE);
            //end 13970
    	    $validation = $factory->getValidationObject($datatovalidate);

            if ($validation->validate() === false) {
                $errors->addErrors('6', $validation->getErrors());
    			return API_Response::fail();
            }
    
            $user = new Core_Api_User($id);
            $user->loadById();
            $result = $user->save($datatoupdate);
            $err = $errors->getErrors();
      		if (!empty($err))
    			return API_Response::fail();
    
            return API_Response::success(array($result));
        } else {
            $errors->addErrors('7', 'Incorrect user');
            return API_Response::fail();
        }
    }
public function createCustomer($login, $hash, $data){
        $errors = Core_Api_Error::getInstance();

        $authData = array('login'=>$login, 'password'=>$hash);
		$user = new Core_Api_User();
        $user->checkAuthentication($authData);
        if (!$user->isAuthenticate()) return API_Response::fail();

        $logger = Core_Api_Logger::getInstance();
        $logger->setUrl('createCustomer');
        $logger->setLogin($login);
        $logger->addLog(LOGGER_ERROR_NO, null, 'db request');

        
        unset($data['PasswordConfirm']);
        
        $customer = new Core_Api_CustomerUser();
        foreach( $data as $f=>$v ) {
            $customer->$f = $v;
        }

		if (isset($user->contextId)){
			$customer->CompanyId = $user->contextId;
			
		}else{
		$customer->CustomerName = $user->getContextCompanyName($company);
		}
	try {
	        $result = $customer->save(); 
	} 
	catch (Exception $e) {
		if ($e->getCode() == 23000) 
			$errors->addError(7, 'Username already exists');
	}

        $err = $errors->getErrors();
  		if (!empty($err))
			return API_Response::fail();

        return API_Response::success(array($result));
    }
	public function updateCustomerProfile($login, $hash, $data){
        $errors = Core_Api_Error::getInstance();
	$errors->resetErrors();

        $authData = array('login'=>$login, 'password'=>$hash);
        $user = new Core_Api_User();
        $user->checkAuthentication($authData);
        
        if (!$user->isAuthenticate()) return API_Response::fail();

        $logger = Core_Api_Logger::getInstance();
        $logger->setUrl('updateCustomer');
        $logger->setLogin($login);
        $logger->addLog(LOGGER_ERROR_NO, null, 'db request');

        $clientInfo = $user->load(
            array(
                'login' => $login,
                'password' => $hash
            )
        );
  
       $cid = $clientInfo->customerId;
       if (!empty($cid) ) {
            $user = new Core_Api_CustomerUser($customerInfo->customerId);
            $user->loadById($cid);

            $datatoupdate = array();

		$datatoupdate = array();
            foreach($data as $field=>$val){
                if($val != $user->$field) $datatoupdate[$field] = $val;
            }

            if(empty($datatoupdate)) return API_Response::success(array($user));

            /*$factory = new Core_Validation_Factory(Core_Validation_Factory::VALIDATION_TYPE_CLIENT_PROFILE_UPDATE);
    	    $validation = $factory->getValidationObject($datatoupdate);
    
            if ($validation->validate() === false) {
                $errors->addErrors('6', $validation->getErrors());
    			return API_Response::fail();
            }*/
    
            $user = new Core_Api_CustomerUser($cid);
            $user->loadById($cid);
            $result = $user->save($datatoupdate);

            $err = $errors->getErrors();
      		if (!empty($err))
    			return API_Response::fail();
    
            return API_Response::success(array($result));
        } else {
            $errors->addErrors('7', 'Incorrect user');
            return API_Response::fail();
        }
    }

	/**
	 * Get Recent Work Orders and Techs
	 *
	 * @param string $user
	 * @param string $pass
	 * @param int $winNum
	 * @return API_Response
	 */
	public function getRecentWorkOrder($user, $pass, $companyId)
	{
       	$logger = Core_Api_Logger::getInstance();
		$logger->setUrl('getRecentWorkOrder ' . $companyId);
        $logger->setLogin($user);
		$logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');

		$authData = array('login'=>$user, 'password'=>$pass);
		$errors = Core_Api_Error::getInstance();

		if (empty($companyId))
			$errors->addError(2, 'Company ID parameter is empty but required');

		$err = $errors->getErrors();
		if (!empty($err))
			return API_Response::fail();

		$user = new Core_Api_User();
		$user->checkAuthentication($authData);

		if (!$user->isAuthenticate())
			return API_Response::fail();

		$fieldList = array("WIN_NUM", "WO_ID", "lastModDate");

		$db = Core_Database::getInstance();
		$select = $db->select();
		$select->from(Core_Database::TABLE_WORK_ORDERS, $fieldList)
					->where("Company_ID = ?", $companyId);

		$result = Core_Database::fetchAll($select);

    	$err = $errors->getErrors();
		if (!empty($err))
			return API_Response::fail();

		return API_Response::success(API_WorkOrder::makeWorkOrderArray($result, new API_WorkOrder(NULL)));
	}
    /**
    *  updateProfileForClient
    *  @param array $data
    *  @param array $dataExt
    *  @param int $client_id
    *  @return API_Response
    */
    public function updateProfileForClient($data,$dataExt,$client_id) {
        $errors = Core_Api_Error::getInstance();

        $user = new Core_Api_User();
        $clientInfo = $user->loadById($client_id);
        $id = $clientInfo->ClientID;
        if (!empty($id)) {
            $user = new Core_Api_User($clientInfo->ClientID);
            $user->loadById();

            $datatoupdate = array();
            $datatovalidate = array();
            foreach ($data as $field => $val) {
                if ($val != $user->$field) {
                    $datatoupdate[$field] = $val;
                    $datatovalidate[$field] = $val;
                }
                else
                    $datatovalidate[$field] = $user->$field;
            }

            if (empty($datatovalidate['Password']))
                $datatovalidate['Password'] = 'bypass78'; // bypass password validation for old password

            $datatoupdate['CompanyName'] = $user->CompanyName;
            if (empty($datatoupdate))
                return API_Response::success(array($user));

            $factory = new Core_Validation_Factory(Core_Validation_Factory::VALIDATION_TYPE_CLIENT_USER_UPDATE);
            $validation = $factory->getValidationObject($datatovalidate);

            if ($validation->validate() === false) {
                $errors->addErrors('6', $validation->getErrors());
                return API_Response::fail();
            }

            $user = new Core_Api_User($id);
            $user->loadById();
            $result = $user->save($datatoupdate);
            $err = $errors->getErrors();
            if (!empty($err))
            {
                return API_Response::fail();
            }
            else
            {   
                $resultExt = $user->editExt($dataExt);      
                return API_Response::success(array($result));
            }
        } else {
            $errors->addErrors('7', 'Incorrect user');
            return API_Response::fail();
        }
    }

    /**
    *  updateProfileForClientFromAdminSide
    *  @param array $data
    *  @param array $dataExt
    *  @param int $client_id
    *  @return API_Response
    */
    public function updateProfileForClientFromAdminSide($data,$dataExt,$client_id
            ,$adminUserName
            ,$needValid=1,$needSendEmail=0,$writetolog=1) 
    {
        //test
        //echo("<pre>");print_r($dataExt);die();
        //test: end

        $errors = Core_Api_Error::getInstance();
        $objDateNow = new Zend_Date();
        $date = $objDateNow->toString('yyyy-MM-dd HH:mm:ss');
         
        $user = new Core_Api_User();
        $clientInfo = $user->loadById($client_id);
        $id = $clientInfo->ClientID;
        
        if (!empty($id)) 
        {
            $user = new Core_Api_User($clientInfo->ClientID);
            $user->loadById();

            $datatoupdate = array();
            $datatovalidate = array();
            $olddata = array();
            $oldExtData = array();
            foreach ($data as $field => $val) 
            {
                if ($val != $user->$field) {
                    $datatoupdate[$field] = $val;
                    $datatovalidate[$field] = $val;
                    $olddata[$field] = $user->$field;
                }
                else
                {
                    $datatovalidate[$field] = $user->$field;
                }
            }
            if (empty($datatovalidate['Password']))
                $datatovalidate['Password'] = 'bypass78'; // bypass password validation for old password

            //echo("datatoupdate: <pre>");print_r($datatoupdate);die();  
            //echo("datatovalidate: <pre>");print_r($datatovalidate);die();                
            if (empty($datatoupdate)) 
            {
                $extActionMsg = $user->editExt($dataExt,$needSendEmail);
                //echo("extActionMsg: $extActionMsg");die();                
                if(!empty($extActionMsg)) 
                {   
                    $extActionMsg = "Edited Client Profile:".$extActionMsg;
                    if($writetolog==1)
                    {
                        $result = Core_Database::insert("cliprofile_action_log",
                                        array("clientid"=>$client_id,
                                        "username"=>$adminUserName,
                                        "logdate"=>$date,
                                        "action"=>"$extActionMsg")
                            );       
                    }
                }
                return API_Response::success(array($user));                
            }
            else
            {
                if($needValid==1)
                {
                    //13970
                    $factory = new Core_Validation_Factory(Core_Validation_Factory::VALIDATION_TYPE_CLIENT_PROFILE_UPDATE);
                    //end 13970
                    $validation = $factory->getValidationObject($datatovalidate);

                    
                    if ($validation->validate() === false) {
                        $errors->addErrors('6', $validation->getErrors());
                        
                        return API_Response::fail();
                    }
                }
                //echo("validation: <pre>");print_r($validation);die();            
                $user = new Core_Api_User($id);
                $user->loadById();
                $result = $user->save($datatoupdate);
                $err = $errors->getErrors();
                //print_r($err);die("err");//test
                if (!empty($err))
                {
                    return API_Response::fail();
                }
                else
                {                      
                    $extActionMsg = $user->editExt($dataExt,$needSendEmail);  
                    //print_r($resultExt);die("resultExt");//test    
                      
                    //----  write to log
                    //echo("<pre>");print_r($datatoupdate);  //test
                    //echo("<pre>");print_r($olddata); //test
                    //$actionMsg="Edited Client Profile:";                
                    
                    foreach($datatoupdate as $field => $val)
                    {
                        $fieldDesc = $user->getFieldDescription($field);
                        $actionMsg.="<br/>Changed ".$fieldDesc." from: ".$olddata[$field];
                        $actionMsg.="<br/>To: ".$val;
                    }
                    $actionMsg.=$extActionMsg;
                    
                    if(!empty($actionMsg))
                    {
                        $actionMsg = "Edited Client Profile:".$actionMsg;
                        if($writetolog==1)
                        {
                            $result = Core_Database::insert("cliprofile_action_log",
                                        array("clientid"=>$client_id,
                                        "username"=>$adminUserName,
                                        "logdate"=>$date,
                                        "action"=>"$actionMsg")
                            );                            
                        }
                        // return  
                        return API_Response::success(array($result));
                    }
                }
            }
        } else {
            $errors->addErrors('7', 'Incorrect user');
            return API_Response::fail();
        }
    }

    public function updateProfileForRepClientFromAdminSide($data,$dataExt,$client_id,$adminUserName) 
    {
        //--- update for rep. client
        //$this->updateProfileForClientFromAdminSide($data,$dataExt,$client_id,$adminUserName);
        //--- get rep. client record.
        $db = Core_Database::getInstance();    
        $select = $db->select();
        $select->from(Core_Database::TABLE_CLIENTS)
                ->where("ClientID = ?", $client_id);
        $repClient = Core_Database::fetchAll($select);  
        //echo("<pre>");print_r($repClient);
        if(!empty($repClient))
        {
            $companyID = $repClient[0]['Company_ID'];   
            $companyLogo = $repClient[0]['Company_Logo'];   
            //--- companyData
            $companyData = array();
            $companyData['Company_Logo']=$companyLogo;
            foreach ($data as $field => $val) 
            {
                if($field=='CompanyName' || $field=='EIN'||$field=='CompanyDescription' || $field=='WebsiteURL')
                {
                    $companyData[$field]=$val;
                }
            }
            //--- update company info. for other clients
            $select = $db->select();
            $select->from(Core_Database::TABLE_CLIENTS)
                ->where("Company_ID = '$companyID'")
                ->where("ClientID != ?", $client_id);
            $otherClients = Core_Database::fetchAll($select);     
            //echo("<pre>");print_r($otherClients);print_r($companyData);die();
            if(!empty($otherClients))
            {
                foreach($otherClients as $oClient)
                {
                    $oClientId = $oClient['ClientID']; 
                    $this->updateProfileForClientFromAdminSide($companyData,$dataExt,$oClientId,$adminUserName,0,0,0);
                }
            }
        }
    }
    
    public function getClientAccessControlValues($clientId)
    {
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from("cliaccessctl");
        $select->where("clientid=?",$clientId);
        $result = Core_Database::fetchAll($select);
        return $result;
    }
    
    public function getCountriesList()
    {
        $errors = Core_Api_Error::getInstance();

        $fieldsList = array('Code','Name');
        $db = Core_Database::getInstance();
        $select = $db->select();

        $select->from(Core_Database::TABLE_COUNTRIES, $fieldsList)
            ->order('DisplayPriority ASC')
            ->order('Name ASC');

        $result = Core_Database::fetchAll($select);

        $err = $errors->getErrors();
        if (!empty($err))
            return API_Response::fail();

        return API_Response::success($this->makeCountryArray($result));
    }
    
    public function updatePartEntryTechIdUpd($partID, $techid) {
        if(empty($partID)) return false;
        if(empty($techid)) return false;
        $db = Core_Database::getInstance();
        $crit = "Deleted = 0 AND " . $db->quoteInto("id = ?", $partID);
        Core_Database::update(Core_Database::TABLE_PART_ENTRIES
                        , array("TechIdLastUpd" => $techid),$crit);
    }

    public function isGPM($clientID)
    {
        $db = Core_Database::getInstance();
        $select = $db->select();
        $select ->from(Core_Database::TABLE_CLIENTS);
        $select ->where("ClientID = '$clientID'");
        $select ->where("Admin = 1");
        $select ->where("ProjectManager = 1");        
        $records = Core_Database::fetchAll($select);  
        if(!empty($records)) return true;
        else return false;
    }
    
    public function isGPMByUserName($username)
    {
        $db = Core_Database::getInstance();
        $select = $db->select();
        $select ->from(Core_Database::TABLE_CLIENTS);
        $select ->where("UserName = '$username'");
        $select ->where("Admin = 1");
        $select ->where("ProjectManager = 1");        
        $records = Core_Database::fetchAll($select);  
        if(!empty($records)) return true;
        else return false;
    }
    public function getClientByID($clientID)
    {
        $db = Core_Database::getInstance();
        $select = $db->select();
        $select ->from(Core_Database::TABLE_CLIENTS);
        $select ->where("ClientID = '$clientID'");
        $records = Core_Database::fetchAll($select);  
        if(!empty($records)) return $records[0];
        else return null;        
    }

    public function getClientByCompanyID($companyID)
    {
        $db = Core_Database::getInstance();
        $select = $db->select();
        $select ->from(Core_Database::TABLE_CLIENTS);
        $select ->where("Company_ID = '$companyID'");
        $select->where ("NOT ISNULL (CustomSignOff)");
        $select->where ("CustomSignOff <> ''");
        $records = Core_Database::fetchAll($select);  
        if(!empty($records)) return $records[0];
        else return null;        
	}

    //--- 13646
    public function getClientByUsername($username)
    {
        $db = Core_Database::getInstance();
        $select = $db->select();
        $select ->from(Core_Database::TABLE_CLIENTS);
        $select ->where("UserName = '$username'");
        $records = Core_Database::fetchAll($select);  
        if(!empty($records)) return $records[0];
        else return null;        
    }

    function formatTimeValue($_date,$_time,$_timezone)
    {
        // for time
        $StartTime = $_time;
        $time = array();
        if(strrpos(strtolower($_time),'am') > 0)
        {
            $StartTime = str_replace(' am', "" ,strtolower($_time));

        }  
        $time = explode(":", $StartTime);
        if(strrpos(strtolower($_time),'pm')  > 0)
        {
            $StartTime = str_replace(' pm', "" ,strtolower($_time));
            $time = explode(":", $StartTime);
            if((int)$time[0] < 12)
            $time[0] = $time[0] + 12;
        }     
        $time[0] = (int)$time[0] + $_timezone;
        $time[1] = (int)$time[1];
        if(count($time) == 2) $time[2] = 0;
        // date
        $date = explode("-", $_date);
        $year = $date[0];
        $mon = $date[1];
        $day = $date[2];
        $date  = mktime($time[0], $time[1], $time[2], $mon  , $day, $year);
        $StartTime = date( "Ymd His",$date);
        $StartTime = str_replace(" ", "T", $StartTime);
        return $StartTime;
    }
    public function getAttachFileScheduleForWOAssigned($woid)
    {
       
        $timezone = 5;
/*        $authData = array('login' => $_SESSION["UserName"], 'password' => $_SESSION["UserPassword"]);
        $tech = new Core_Api_TechUser;
        $tech->checkAuthentication($authData);

        if (!$tech->isAuthenticate()) {
            return "";
        }
        $TechID = $tech->getTechId();*/
        
        $TechClass = new Core_Api_TechClass();
        $wos = $TechClass->getWos($woid);
$ics = <<<EOT
BEGIN:VCALENDAR
METHOD:PUBLISH
X-WR-TIMEZONE:America/Chicago
CALSCALE:GREGORIAN
X-WR-CALNAME:FieldSolutions
VERSION:2.0
X-WR-RELCALID:F1B33276-2C67-473C-B2C5-FFEC990B7C15
X-APPLE-CALENDAR-COLOR:#E51717
BEGIN:VTIMEZONE
TZID:America/Chicago
BEGIN:DAYLIGHT
TZOFFSETFROM:-0600
TZOFFSETTO:-0500
DTSTART:20070311T020000
RRULE:FREQ=YEARLY;BYMONTH=3;BYDAY=2SU
TZNAME:CDT
END:DAYLIGHT
BEGIN:STANDARD
TZOFFSETFROM:-0500
TZOFFSETTO:-0600
DTSTART:20071104T020000
RRULE:FREQ=YEARLY;BYMONTH=11;BYDAY=1SU
TZNAME:CST
END:STANDARD
END:VTIMEZONE
EOT;
if (!empty($wos)) {
        $wo = $wos[0];
        $win = $wo['WIN_NUM'];
        $wo_id = $wo['WO_ID'];
        $Headline = $wo['Headline'];
        $SiteName = $wo['SiteName'];
        $City = $wo['City'];
        $Zipcode = $wo['Zipcode'];
        $PayMax= $wo['PayMax'];
        $State = $wo['State'];
        $Address = $wo['Address'];
        $WO_Category = $wo['WO_Category'];
        $Amount_Per = $wo['Amount_Per'];
        if(empty($wo['StartDate']) || empty($wo['StartTime']) || empty($wo['EndDate']) || empty($wo['EndTime']))  return false;
        $StartDate = $wo['StartDate'];
        $StartTime = $wo['StartTime'];
        $EndDate = $wo['EndDate'];
        $EndTime = $wo['EndTime'];
		$TechID = $wo['Tech_ID'];
              
        $DTSTART = $this->formatTimeValue($StartDate,$StartTime,0);
        $DTSTARTZ = $this->formatTimeValue($StartDate,$StartTime,$timezone)."Z";
        $DTEND = $this->formatTimeValue($EndDate,$EndTime,0);
        $charn = nl2br('\n');
        $UID = $win.$TechID;
        $ics.= <<<EOT
\nBEGIN:VEVENT
SEQUENCE:2
TRANSP:OPAQUE
UID:$UID
DTSTART;TZID=America/Chicago:$DTSTART
DTSTAMP:$DTSTARTZ
SUMMARY:WIN#$win - $Headline � $City, $State
CREATED:$DTSTARTZ
CATEGORIES:FieldSolutions
DESCRIPTION:WIN#:$win $charn Client WO #: $wo_id $charn $WO_Category - $Headline $charn $SiteName $charn $Address $charn $City, $State $Zipcode $charn Pay: $PayMax PER $Amount_Per $charn
DTEND;TZID=America/Chicago:$DTEND
END:VEVENT
EOT;
        
}

$ics .=<<<EOT
\nEND:VCALENDAR
EOT;
        $filename = realpath(dirname(__FILE__) . '/../../../../htdocs/techs/documents/').'/Myschedule_'.  uniqid() .'.ics';
        $handle = fopen($filename, 'w') or $filename = ""; 
        fwrite($handle, $ics);
        fclose($handle);
        return $filename;
    }   
    public function getAttachFileScheduleForWOBackupTech($woid)
    {
        $timezone = 5;
/*        $authData = array('login' => $_SESSION["UserName"], 'password' => $_SESSION["UserPassword"]);
        $tech = new Core_Api_TechUser;
        $tech->checkAuthentication($authData);

        if (!$tech->isAuthenticate()) {
            return "";
        }
        $TechID = $tech->getTechId();*/
        $TechClass = new Core_Api_TechClass();
        $wos = $TechClass->getWos($woid);
$ics = <<<EOT
BEGIN:VCALENDAR
METHOD:PUBLISH
X-WR-TIMEZONE:America/Chicago
CALSCALE:GREGORIAN
X-WR-CALNAME:FieldSolutions
VERSION:2.0
X-WR-RELCALID:F1B33276-2C67-473C-B2C5-FFEC990B7C15
X-APPLE-CALENDAR-COLOR:#E51717
BEGIN:VTIMEZONE
TZID:America/Chicago
BEGIN:DAYLIGHT
TZOFFSETFROM:-0600
TZOFFSETTO:-0500
DTSTART:20070311T020000
RRULE:FREQ=YEARLY;BYMONTH=3;BYDAY=2SU
TZNAME:CDT
END:DAYLIGHT
BEGIN:STANDARD
TZOFFSETFROM:-0500
TZOFFSETTO:-0600
DTSTART:20071104T020000
RRULE:FREQ=YEARLY;BYMONTH=11;BYDAY=1SU
TZNAME:CST
END:STANDARD
END:VTIMEZONE
EOT;
if (!empty($wos)) {
    foreach ($wos as $wo) {
        $win = $wo['WIN_NUM'];
        $wo_id = $wo['WO_ID'];
        $Headline = $wo['Headline'];
        $SiteName = $wo['SiteName'];
        $City = $wo['City'];
        $Zipcode = $wo['Zipcode'];
        $PayMax= $wo['PayMax'];
        $State = $wo['State'];
        $Address = $wo['Address'];
        $WO_Category = $wo['WO_Category'];
        $Amount_Per = $wo['Amount_Per'];
        if(empty($wo['StartDate']) || empty($wo['StartTime']) || empty($wo['EndDate']) || empty($wo['EndTime']))  return false;
        $StartDate = $wo['StartDate'];
        $StartTime = $wo['StartTime'];
        $EndDate = $wo['EndDate'];
        $EndTime = $wo['EndTime'];
		$TechID =  $wo['Tech_ID'];
              
        $DTSTART = $this->formatTimeValue($StartDate,$StartTime,0);
        $DTSTARTZ = $this->formatTimeValue($StartDate,$StartTime,$timezone)."Z";
        $DTEND = $this->formatTimeValue($EndDate,$EndTime,0);
        $charn = nl2br('\n');
        $UID = $win.$TechID;
        $ics.= <<<EOT
\nBEGIN:VEVENT
SEQUENCE:2
TRANSP:OPAQUE
UID:$UID
DTSTART;TZID=America/Chicago:$DTSTART
DTSTAMP:$DTSTARTZ
SUMMARY:WIN#$win - $Headline � $City, $State
CREATED:$DTSTARTZ
CATEGORIES:FieldSolutions
DESCRIPTION:Current Status: Back Up Technician$charn WIN#:$win $charn Client WO #: $wo_id $charn $WO_Category - $Headline $charn $SiteName $charn $Address $charn $City, $State $Zipcode $charn Pay: $PayMax PER $Amount_Per $charn
DTEND;TZID=America/Chicago:$DTEND
END:VEVENT
EOT;
    }
}
$ics .=<<<EOT
\nEND:VCALENDAR
EOT;
        
        $filename = realpath(dirname(__FILE__) . '/../../../../htdocs/techs/documents/').'/Myschedule_'.  uniqid() .'.ics';
        $handle = fopen($filename, 'w') or $filename = ""; 
        fwrite($handle, $ics);
        fclose($handle);
        return $filename;
    }  
    public function getAttachFileScheduleForWOAssignedToAnotherTech($woid)
    {
        $timezone = 5;
/*        $authData = array('login' => $_SESSION["UserName"], 'password' => $_SESSION["UserPassword"]);
        $tech = new Core_Api_TechUser;
        $tech->checkAuthentication($authData);

        if (!$tech->isAuthenticate()) {
            return "";
        }
        $TechID = $tech->getTechId();*/
        $TechClass = new Core_Api_TechClass();
        $wos = $TechClass->getWos($woid);
$ics = <<<EOT
BEGIN:VCALENDAR
METHOD:PUBLISH
X-WR-TIMEZONE:America/Chicago
CALSCALE:GREGORIAN
X-WR-CALNAME:FieldSolutions
VERSION:2.0
X-WR-RELCALID:F1B33276-2C67-473C-B2C5-FFEC990B7C15
X-APPLE-CALENDAR-COLOR:#E51717
BEGIN:VTIMEZONE
TZID:America/Chicago
BEGIN:DAYLIGHT
TZOFFSETFROM:-0600
TZOFFSETTO:-0500
DTSTART:20070311T020000
RRULE:FREQ=YEARLY;BYMONTH=3;BYDAY=2SU
TZNAME:CDT
END:DAYLIGHT
BEGIN:STANDARD
TZOFFSETFROM:-0500
TZOFFSETTO:-0600
DTSTART:20071104T020000
RRULE:FREQ=YEARLY;BYMONTH=11;BYDAY=1SU
TZNAME:CST
END:STANDARD
END:VTIMEZONE
EOT;
if (!empty($wos)) {
    foreach ($wos as $wo) {
        $win = $wo['WIN_NUM'];
        $wo_id = $wo['WO_ID'];
        $Headline = $wo['Headline'];
        $SiteName = $wo['SiteName'];
        $City = $wo['City'];
        $Zipcode = $wo['Zipcode'];
        $PayMax= $wo['PayMax'];
        $State = $wo['State'];
        $Address = $wo['Address'];
        $WO_Category = $wo['WO_Category'];
        $Amount_Per = $wo['Amount_Per'];
        if(empty($wo['StartDate']) || empty($wo['StartTime']) || empty($wo['EndDate']) || empty($wo['EndTime']))  return false;
        $StartDate = $wo['StartDate'];
        $StartTime = $wo['StartTime'];
        $EndDate = $wo['EndDate'];
        $EndTime = $wo['EndTime'];
		$TechID = $wo['Tech_ID'];
              
        $DTSTART = $this->formatTimeValue($StartDate,$StartTime,0);
        $DTSTARTZ = $this->formatTimeValue($StartDate,$StartTime,$timezone)."Z";
        $DTEND = $this->formatTimeValue($EndDate,$EndTime,0);
        $charn = nl2br('\n');
        $UID = $win.$TechID;
        $ics.= <<<EOT
\nBEGIN:VEVENT
SEQUENCE:2
TRANSP:OPAQUE
UID:$UID
DTSTART;TZID=America/Chicago:$DTSTART
DTSTAMP:$DTSTARTZ
SUMMARY:WIN#$win - $Headline � $City, $State
CREATED:$DTSTARTZ
CATEGORIES:FieldSolutions
DESCRIPTION:This Work Order has been assigned to another technician$charn WIN#:$win $charn Client WO #: $wo_id $charn $WO_Category - $Headline $charn $SiteName $charn $Address $charn $City, $State $Zipcode $charn
DTEND;TZID=America/Chicago:$DTEND
END:VEVENT
EOT;
    } 
}
$ics .=<<<EOT
\nEND:VCALENDAR
EOT;
        
        $filename = realpath(dirname(__FILE__) . '/../../../../htdocs/techs/documents/').'/Myschedule_'.  uniqid() .'.ics';
        $handle = fopen($filename, 'w') or $filename = ""; 
        fwrite($handle, $ics);
        fclose($handle);
        return $filename;
    }
    
    public function getAttachFileScheduleForWODeactivatedToAnotherTech($woid)
    {
        $timezone = 5;
/*        $authData = array('login' => $_SESSION["UserName"], 'password' => $_SESSION["UserPassword"]);
        $tech = new Core_Api_TechUser;
        $tech->checkAuthentication($authData);

        if (!$tech->isAuthenticate()) {
            return "";
        }
        $TechID = $tech->getTechId();*/
        $TechClass = new Core_Api_TechClass();
        $wos = $TechClass->getWos($woid);
$ics = <<<EOT
BEGIN:VCALENDAR
METHOD:PUBLISH
X-WR-TIMEZONE:America/Chicago
CALSCALE:GREGORIAN
X-WR-CALNAME:FieldSolutions
VERSION:2.0
X-WR-RELCALID:F1B33276-2C67-473C-B2C5-FFEC990B7C15
X-APPLE-CALENDAR-COLOR:#E51717
BEGIN:VTIMEZONE
TZID:America/Chicago
BEGIN:DAYLIGHT
TZOFFSETFROM:-0600
TZOFFSETTO:-0500
DTSTART:20070311T020000
RRULE:FREQ=YEARLY;BYMONTH=3;BYDAY=2SU
TZNAME:CDT
END:DAYLIGHT
BEGIN:STANDARD
TZOFFSETFROM:-0500
TZOFFSETTO:-0600
DTSTART:20071104T020000
RRULE:FREQ=YEARLY;BYMONTH=11;BYDAY=1SU
TZNAME:CST
END:STANDARD
END:VTIMEZONE
EOT;
if (!empty($wos)) {
    foreach ($wos as $wo) {
        $win = $wo['WIN_NUM'];
        $wo_id = $wo['WO_ID'];
        $Headline = $wo['Headline'];
        $SiteName = $wo['SiteName'];
        $City = $wo['City'];
        $Zipcode = $wo['Zipcode'];
        $PayMax= $wo['PayMax'];
        $State = $wo['State'];
        $Address = $wo['Address'];
        $WO_Category = $wo['WO_Category'];
        $Amount_Per = $wo['Amount_Per'];
        if(empty($wo['StartDate']) || empty($wo['StartTime']) || empty($wo['EndDate']) || empty($wo['EndTime']))  return false;
        $StartDate = $wo['StartDate'];
        $StartTime = $wo['StartTime'];
        $EndDate = $wo['EndDate'];
        $EndTime = $wo['EndTime'];
		$TechID = $wo['Tech_ID'];
              
       $DTSTART = $this->formatTimeValue($StartDate,$StartTime,0);
        $DTSTARTZ = $this->formatTimeValue($StartDate,$StartTime,$timezone)."Z";
        $DTEND = $this->formatTimeValue($EndDate,$EndTime,0);
        $charn = nl2br('\n');
        $UID = $win.$TechID;
        $ics.= <<<EOT
\nBEGIN:VEVENT
SEQUENCE:2
TRANSP:OPAQUE
UID:$UID
DTSTART;TZID=America/Chicago:$DTSTART
DTSTAMP:$DTSTARTZ
SUMMARY:WIN#$win - $Headline � $City, $State
CREATED:$DTSTARTZ
CATEGORIES:FieldSolutions
DESCRIPTION:This Work Order has been deactivated by the client.$charn WIN#:$win $charn Client WO #: $wo_id $charn $WO_Category - $Headline $charn $SiteName $charn $Address $charn $City, $State $Zipcode $charn
DTEND;TZID=America/Chicago:$DTEND
END:VEVENT
EOT;
    } 
}
$ics .=<<<EOT
\nEND:VCALENDAR
EOT;
        
        $filename = realpath(dirname(__FILE__) . '/../../../../htdocs/techs/documents/').'/Myschedule_'.  uniqid() .'.ics';
        $handle = fopen($filename, 'w') or $filename = ""; 
        fwrite($handle, $ics);
        fclose($handle);
        return $filename;
    }   
    
    public function deleteFile($filename)
    {
        if(is_file($filename))
        {
            unlink($filename);
        }    
    }        
    //--- 360    
    public function updateWorkOrderAdditionalFields($winNum, $additionalWO)
    { 
    	if(empty($additionalWO) || $winNum == "")return false;	
    	
    	$projClass = new Core_Api_ProjectClass();
    	$proj = $projClass->getProjectByWin($winNum);
    	
    	foreach($additionalWO as $k=>$v){
    		if($additionalWO[$k] == ''){
			if ($k == 'PushWM') continue;
    			if($proj[$k] != '')$additionalWO[$k] = $proj[$k];
    		}
    	}
    	
        $db = Core_Database::getInstance();
        try{
            $db->beginTransaction();
            $criteria = "WINNUM = '$winNum'";
            $db->update("work_orders_additional_fields",$additionalWO,$criteria);            
            $db->commit();
        }catch(Zend_Exception $e){
        	error_log(print_r($e->getMessage(),true), 3, "/home/p6356/serviceTypeCheck.log");
            $db->rollBack();
        }
        
        //Monitoring for blank service type id's, will remove when not an issue anymore
        $select = $db->select();
        $select->from("work_orders_additional_fields", array("ServiceTypeId"));
        $select->where("WINNUM = ?", $winNum);
        $curWo = Core_Database::fetchAll($select);
        if(empty($curWo[0]['ServiceTypeId']) || is_null($curWo[0]['ServiceTypeId']) ||  $curWo[0]['ServiceTypeId'] == ""){
        	error_log(print_r(debug_backtrace(),true), 3, "/home/p6356/serviceTypeCheck.log");
        	$msg = print_r(debug_backtrace(),true);
        }
    }
    //--- 360    
    public function saveWorkOrderAuditData($winNum,$auditNeeded,$auditNeededBy,$auditNeededDate,$auditComplete,$auditCompleteBy,$auditCompleteDate)
    {
        $db = Core_Database::getInstance();
        $fieldList = array('Company_ID','Company_Name','Project_Name');
        $select = $db->select();
        $select->from(Core_Database::TABLE_WORK_ORDERS, $fieldList)
            ->where("WIN_NUM = '$winNum'");
        $curWO = Core_Database::fetchAll($select);

        $currentAdditionalWO = $this->getWorkOrderAdditionalFields($winNum);
        
        $additionalWO = array();
        
        if(!empty($auditNeeded) && $auditNeeded==1)
        {
            $additionalWO['AuditNeeded'] = 1;
            $additionalWO['AuditNeededBy'] = $auditNeededBy;
            $additionalWO['AuditNeededDate'] = $auditNeededDate;
        }
        else
        {
            $additionalWO['AuditNeeded'] = 0;
            $additionalWO['AuditNeededBy'] = "";
            $additionalWO['AuditNeededDate'] = null;
        }
        
        if(!empty($auditComplete) && $auditComplete==1)
        {
            $additionalWO['AuditComplete'] = 1;
            $additionalWO['AuditCompleteBy'] = $auditCompleteBy;
            $additionalWO['AuditCompleteDate'] = $auditCompleteDate;
        }
        else        
        {
            $additionalWO['AuditComplete'] = 0;
            $additionalWO['AuditCompleteBy'] = "";
            $additionalWO['AuditCompleteDate'] = null;                
        }
        
        
        if(!empty($currentAdditionalWO))
        {
            if($currentAdditionalWO['AuditNeeded']==$additionalWO['AuditNeeded'])
            {
                unset($additionalWO['AuditNeeded']);
                unset($additionalWO['AuditNeededBy']);
                unset($additionalWO['AuditNeededDate']);                    
            }
            else
            {
                Core_TimeStamp::createTimeStamp($winNum, 
                    $auditNeededBy,
                    "Work Order Updated: Audit Needed",
                    $curWO["Company_ID"], 
                    $curWO["Company_Name"], 
                    $curWO['Project_Name'], "", "", ""
                );                
            }
            
            
            if($currentAdditionalWO['AuditComplete']==$additionalWO['AuditComplete'])
            {
                unset($additionalWO['AuditComplete']);
                unset($additionalWO['AuditCompleteBy']);
                unset($additionalWO['AuditCompleteDate']);                    
            }
            else
            {
                Core_TimeStamp::createTimeStamp($winNum, 
                    $auditCompleteBy,
                    "Work Order Updated: Audit Complete",
                    $curWO["Company_ID"], 
                    $curWO["Company_Name"], 
                    $curWO['Project_Name'], "", "", ""
                );                                
            }
            
            $this->updateWorkOrderAdditionalFields(
                    $winNum,
                    $additionalWO
            );
        }                
        else//insert
        {
            if($additionalWO['AuditNeeded']==1)
            {
                Core_TimeStamp::createTimeStamp($winNum, 
                    $auditNeededBy,
                    "Work Order Updated: Audit Needed",
                    $curWO["Company_ID"], 
                    $curWO["Company_Name"], 
                    $curWO['Project_Name'], "", "", ""
                );                                
            }
            
            if($additionalWO['AuditComplete']==1)
            {
                Core_TimeStamp::createTimeStamp($winNum, 
                    $auditCompleteBy,
                    "Work Order Updated: Audit Complete",
                    $curWO["Company_ID"], 
                    $curWO["Company_Name"], 
                    $curWO['Project_Name'], "", "", ""
                );                                
            }
            
            $additionalWO['WINNUM'] = $winNum;
            $this->insertWorkOrderAdditionalFields($additionalWO);
        }        
    }
    
    //--- 360   
    public function updateAddPayReason($winNum, $addPayReason)
    {
         $db = Core_Database::getInstance();
         $criteria = "WIN_NUM = '$winNum'";
         $updateData = array("Add_Pay_Reason"=>$addPayReason);
         $db->update(Core_Database::TABLE_WORK_ORDERS,$updateData,$criteria);          
    }
    
    //--- 360   
    public function insertWorkOrderAdditionalFields($additionalWO)
    {
		$db = Core_Database::getInstance();
		try {
	        $result = $db->insert("work_orders_additional_fields", $additionalWO);
		} catch (Exception $e) {
				$win = $additionalWO['WINNUM'];
				unset($additionalWO['WINNUM']);
				$this->updateWorkOrderAdditionalFields($win, $additionalWO);
		}
        return $result;
    }
    
    //--- 360   
    public function getWorkOrderAdditionalFields($winNum)
    {
        $db = Core_Database::getInstance();
        $query = $db->select();
        $query->from("work_orders_additional_fields");
        $query->where("WINNUM = '$winNum'");            
        $records =  $db->fetchAll($query);
        if(empty($records)) return null;
        else return $records[0];
    }

    public function getWorkOrdersAdditionalFields($winNums)
    {
		if (empty($winNums) || !is_array($winNums)) return NULL;
        $db = Core_Database::getInstance();
        $query = $db->select();
        $query->from("work_orders_additional_fields");
        $query->where("WINNUM IN (?)", $winNums);
        $records =  $db->fetchAll($query);
        if(empty($records)) return null;
        else return $records;
    }

	//--- 360
    public function getWorkOrder_AddPayReasonField($winNum)
    {
        $fieldArray = array('WIN_NUM','Add_Pay_Reason');         
        $db = Core_Database::getInstance();
        $query = $db->select(); 
        $query->from(Core_Database::TABLE_WORK_ORDERS,$fieldArray);
        $query->where("WIN_NUM = '$winNum'");        
        $records = Core_Database::fetchAll($query);        
        if(!empty($records)) return $records[0];
        else return null;
    }

    //--- 1:AuditNeeded=1; 2:AuditComplete=1; 3:AuditNeeded=AuditComplete=0 //360
    public function getWinsByAuditStatus($auditStatus)
    {
        $db = Core_Database::getInstance();
        $query = $db->select();
        $query->from("work_orders_additional_fields");
        if($auditStatus==1)
        {
            $query->where("AuditNeeded = 1");            
        }
        else if($auditStatus==2)
        {
            $query->where("AuditComplete = 1");
        }
        else if($auditStatus==3)
        {
            $query->where("AuditNeeded = 0 AND AuditComplete=0");
        }                
        $records =  $db->fetchAll($query);
        $wins='';        
        if(!empty($records))
        {
            $wins='0';
            foreach($records as $rec)
            {
                $wins.=",".$rec['WINNUM'];
            }               
        }   
        else
        {
            $wins='0';
        }     
        return $wins;
    }
    
    //--- 360
    public function getWinsWithAuditNeededAndWOComplete()
    {
        $db = Core_Database::getInstance();
        $query = $db->select();
        $query->from("work_orders_additional_fields");
        $query->where("AuditNeeded = 1");            
        $query->where("AuditComplete = 0");
        
        $records =  $db->fetchAll($query);
        $wins='';        
        if(!empty($records))
        {
            $wins='0';
            foreach($records as $rec)
            {
                $wins.=",".$rec['WINNUM'];
            }               
        }   
        else
        {
            $wins='0';
        }     
        return $wins;
    }
     //--- 360
    public function getWinsWithAuditComplete()
    {
        $db = Core_Database::getInstance();
        $query = $db->select();
        $query->from("work_orders_additional_fields");
        $query->where("AuditComplete = 1");
        
        $records =  $db->fetchAll($query);
        $wins='';        
        if(!empty($records))
        {
            $wins='0';
            foreach($records as $rec)
            {
                $wins.=",".$rec['WINNUM'];
            }               
        }   
        else
        {
            $wins='0';
        }     
        return $wins;
    }
    //--- 360
    public function getWinsWithAuditCompleteAndWONeeded()
    {
        $db = Core_Database::getInstance();
        $query = $db->select();
        $query->from("work_orders_additional_fields");
        $query->where("AuditNeeded = 0");            
        $query->where("AuditComplete = 1");
        
        $records =  $db->fetchAll($query);
        $wins='';        
        if(!empty($records))
        {
            $wins='0';
            foreach($records as $rec)
            {
                $wins.=",".$rec['WINNUM'];
            }               
        }   
        else
        {
            $wins='0';
        }     
        return $wins;
    }
    //--- 360
    public function getWinsWithAuditCompleteORNeeded()
    {
        $db = Core_Database::getInstance();
        $query = $db->select();
        $query->from("work_orders_additional_fields");
        $query->where("AuditNeeded = 1 OR AuditComplete = 1");            
        
        $records =  $db->fetchAll($query);
        $wins='';        
        if(!empty($records))
        {
            $wins='0';
            foreach($records as $rec)
            {
                $wins.=",".$rec['WINNUM'];
            }               
        }   
        else
        {
            $wins='0';
        }     
        return $wins;
    }
    
    //--- 360
    public function getWinsByAuditNeededAuditComplete($auditNeeded,$auditComplete)
    {
        $db = Core_Database::getInstance();
        $query = $db->select();
        $query->from("work_orders_additional_fields");
        $isfilter=0;
        
        if(isset($auditNeeded) && ($auditNeeded==1 || $auditNeeded==0))
        {
            $query->where("AuditNeeded = $auditNeeded");            
            $isfilter=1;
        }
        
        if(isset($auditComplete) && ($auditComplete==1 || $auditComplete==0))
        {
            $query->where("AuditComplete = $auditComplete");
            $isfilter=1;
        }
        
        if($isfilter==1)
        {
            $records =  $db->fetchAll($query);
            $wins='0';        
            if(!empty($records))
            {
                foreach($records as $rec)
                {
                    $wins.=",".$rec['WINNUM'];
                }               
            }   
            else
            {
                $wins='0';
            }     
        }
        else
        {
            $wins='';
        }
        
        return $wins;
    }
    //--- 330
    public function getSiteListByProject($projectID, $sort, $offset = 0, $limit = 0, &$count)
    {
        $db = Core_Database::getInstance();
        $select = $db->select();
        $select->from(array('s' => Core_Database::TABLE_SITES));
        $select->join(array('p' => Core_Database::TABLE_PROJECTS), '`s`.Project_ID = `p`.Project_ID', array());
        $select->where("s.Project_ID='$projectID'");

        if (!empty($sort) )
            $select->order($sort);

        if ($offset != 0 || $limit != 0)
            $select->limit($limit, $offset);
        
        $records = Core_Database::fetchAll($select);
        if(empty($records)) return null;
        else  
        {
            if ($count !== null) $count = count($records);
            return $records;   
}
    }
    
    public function getSiteWOCheck($siteNumber, $projectID)
    {
        $db = Core_Database::getInstance();
        $select = $db->select();
        $select->from(array('s' => Core_Database::TABLE_SITES));
        $select->join(array('p' => Core_Database::TABLE_PROJECTS), '`s`.Project_ID = `p`.Project_ID', array());
        $select->where("s.Project_ID = '$projectID'");
        $select->where("s.SiteNumber = '$siteNumber'");
        $records = Core_Database::fetchAll($select);
        if(empty($records)) return null;
        else return $records[0];
    }

    public function deleteBid_WithTechAndWinnum($techID,$winNum)
    {
        if(empty($techID)) return;
        if(empty($winNum)) return;
        //---
        $criteria = "TechID = '$techID' AND WorkOrderID = '$winNum'";
        $db = Core_Database::getInstance();
        $result = $db->delete(Core_Database::TABLE_WORK_ORDER_BIDS,$criteria);
        return $result;
}

    public static function sendGGETechEvaluation($techID)
    {
		//require_once("smtpMail.php");

		$techInfo = Core_Tech::getProfile($techID, true, API_Tech::MODE_TECH, "");
        $Core_Api_TechClass = new Core_Api_TechClass();    
        $GGEStatusArray = $Core_Api_TechClass->getStatusArrayForGGECert($techID);
        $GGE1stAttemptStatus = $GGEStatusArray['GGE1stAttemptStatus'];
        $GGE1stAttemptScore = $GGEStatusArray['GGE1stAttemptScore'];
    
        $GGE2ndAttemptStatus = $GGEStatusArray['GGE2ndAttemptStatus'];
        $GGE2ndAttemptScore = $GGEStatusArray['GGE2ndAttemptScore'];
    
        
        $toEmail = $techInfo["PrimaryEmail"];
        
        $fromName = "FieldSolutions";
        $fromEmail = "no-replies@fieldsolutions.com";

        $subject = "GGE Tech Evaluation Results";
        $fname = $techInfo["Firstname"];
        $lname = $techInfo["Lastname"];
        $issend = false;    
        if($GGE1stAttemptStatus == "Pass" || $GGE2ndAttemptStatus == "Pass")
        {
            $score = $GGE1stAttemptStatus == "Pass"?$GGE1stAttemptScore:$GGE2ndAttemptScore;
            $body = "Dear $fname $lname,
                
                    Congratulations! You've passed the GGE Tech Evaluation with a score of $score/20 and have been flagged as a knowledgeable technician by GGE Techs.  They'll come to you first when they have work in your area!
                    
                    Great Job!

                    Your FieldSolutions Team
                    ";
            $issend = true;
        }  else if($GGE1stAttemptStatus == "Fail" && ($GGE2ndAttemptStatus == "" || $GGE2ndAttemptStatus == "N/A"))
        {
            $body = "Dear $fname $lname,
                
                    Unfortunately you have not passed the GGE Tech Evaluation. Your score was $GGE1stAttemptScore/20. You have only one remaining attempt. Please log into www.FieldSolutions.com and click on the 'GGE Tech Evaluation' link in the left sidebar to try again.
                    
                    Good luck!

                    Your FieldSolutions Team
                    ";
            $issend = true;
        } else if($GGE1stAttemptStatus == "Fail" && $GGE2ndAttemptStatus == "Fail")
        {
            $body = "Dear $fname $lname,
                
                    Unfortunately you have not passed the GGE Tech Evaluation. Your score was $GGE2ndAttemptScore/20. You do not have any remaining attempts. 
                    
                    Thank you,

                    Your FieldSolutions Team
                    ";
            $issend = true;
//        if($issend)
//        {
//            $mail = new Core_Mail();
//            $mail->setBodyText($body);
//            $mail->setFromName($fromName);
//            $mail->setFromEmail($fromEmail);
//            $mail->setToEmail($toEmail);
//            $mail->setSubject($subject);
//            $mail->send();
//        }
        }
    }
   function saveLanguageSkils($techID,$json){
        if(!empty($json)){
            $data = json_decode($json);
            if(!empty($data)){
                $Core_TechLanguages = new Core_TechLanguages();
                foreach($data as $item){
                    $selected = 0;
                    if($item->checked){
                        $selected = $item->selected;
                    }
                    $Core_TechLanguages->saveTechLanguage($techID,$item->value,$selected);
                }
            }
        }
   }
    function getFromEmailWoAssign($project,$futureWoInfo,$win = null){
        if(empty($futureWoInfo)){
            return $project->getFrom_Email();
        }else{
            return $futureWoInfo['SystemGeneratedEmailFrom'];
		}
        return "";
    } 
    
    //--- 13622, 13713,13728
    public function saveWorkOrderTagsData($winNum,$wo,$fieldValues,$updatedByUserName)
    {        
        $tagFields = array('EntityTypeId','ServiceTypeId','FSClientServiceDirectorId','FSAccountManagerId','AllowDeVryInternstoObserveWork','AllowDeVryInternstoPerformField','ACSNotifiPOC','ACSNotifiPOC_Phone','ACSNotifiPOC_Email',
						'SiteContactBackedOutChecked','NotifiedTime'
		);
        if(empty($winNum)) return;
        if(empty($fieldValues) || count($fieldValues)==0) return;
        
        $updateFields = array();
        foreach($fieldValues as $k=>$v)
        {
            if(in_array($k,$tagFields))
            {
                if(empty($wo->$k) && empty($v)){}
                else if($wo->$k != $v) {
                    $updateFields[$k]=$v;                    
                }
            }
            
        }
        //--- update 
        if(count($updateFields) > 0)
        {
            $this->updateWorkOrderAdditionalFields($winNum,$updateFields);
        }
        //--- write to log
        if(count($updateFields) > 0)
        {
            $message = "";
            foreach($updateFields as $k->$v)
            {
                $message.= empty($message)? $k:','.$k;
            }
            $message = "Work Order Update: ".$message;
            Core_TimeStamp::createTimeStamp($winNum,$updatedByUserName,$message,
                        $wo->Company_ID,$wo->Company_Name, $wo->Project_Name, 
                        "", "", ""
            );                                
        }        
    }
    //--- 13713,13728
    public function getWinsWithDeVryObserveORDeVryPerform()
    {
        $db = Core_Database::getInstance();
        $query = $db->select();
        $query->from("work_orders_additional_fields");
        $query->where("AllowDeVryInternstoObserveWork = 1 OR AllowDeVryInternstoPerformField = 1");         
        $records =  $db->fetchAll($query);
        $wins='';        
        if(!empty($records))
        {
            $wins='0';
            foreach($records as $rec)
            {
                $wins.=",".$rec['WINNUM'];
            }               
        }   
        else
        {
            $wins='0';
        }     
        return $wins;
    }
    //--- 13732
    public function getWinsWithSiteContactBackedOutChecked()
    {
        $db = Core_Database::getInstance();
        $query = $db->select();
        $query->from("work_orders_additional_fields");
        $query->where("SiteContactBackedOutChecked = 1");         
        $records =  $db->fetchAll($query);
        $wins='';        
        if(!empty($records))
        {
            $wins='0';
            foreach($records as $rec)
            {
                $wins.=",".$rec['WINNUM'];
            }               
        }   
        else
        {
            $wins='0';
        }     
        return $wins;
    }

	public function getTechFileLinks($login, $hash, $win) {
		// get links for the links for all tech uploaded files
        	$authData = array('login'=>$login, 'password'=>$hash);
        	$user = new Core_Api_User();
        	$user->checkAuthentication($authData);
        	if (!$user->isAuthenticate()) return API_Response::fail();
		$files = new Core_Files();
		$signOffFiles = $files->getDocList(array($win), Core_Files::SIGNOFF_FILE);
		$techFiles = $files->getDocList(array($win), Core_Files::TECH_FILE);
		$linkList = array();
		foreach ($signOffFiles as $file) {
			$linkList[] = array('url' => $this->makeWorkOrderFileDownloadLink($win, $file['id']), 'type' => $file['type']);
		}
		foreach ($techFiles as $file) {
			$linkList[] = array('url' => $this->makeWorkOrderFileDownloadLink($win, $file['id']), 'type' => $file['type']);
		}
		return API_Response::success(array($linkList));
	}
	
	public function makeWorkOrderFileDownloadLink($winNum, $fileId) {
		// create a link which expires after 1 day
		$cfgSite = Zend_Registry::get('CFG_SITE');
		$site_name_sub_domain = $cfgSite->get("site")->get("site_name_sub_domain");
		if (empty($site_name_sub_domain))$site_name_sub_domain = "www";
		$token = md5($winNum . 'offSite#Access' . date('Ymd') . $fileId);
		return 'https://' . $site_name_sub_domain . '.fieldsolutions.com/widgets/dashboard/do/file-wos-documents-download/?win='. $winNum . '&id=' . $fileId . '&token=' . $token;
	}

    //--- 13970
    public function updateCompanyProfile($companyID,$fields)
    {
        $avaiFields = array('DeniedTech_NoShowGreater','DeniedTech_NoShowPeriod','DeniedTech_BackOutGreater','DeniedTech_BackOutPeriod','DeniedTech_PreferenceRatingSmaller','DeniedTech_PreferenceRatingPeriod','DeniedTech_PerformanceRatingSmaller','DeniedTech_PerformanceRatingPeriod');
        if(empty($fields) || count($fields)==0){
            return;
        }
        
        $updateFields = array();
        foreach($fields as $k=>$v){
            if(in_array($k,$avaiFields)){
                $updateFields[$k]=$v;
            }
        }
        
        //--- DeniedTech_NoShowGreater
        if(isset($updateFields['DeniedTech_NoShowGreater']) && empty($updateFields['DeniedTech_NoShowGreater'])){
            $updateFields['DeniedTech_NoShowGreater'] = 0;
            $updateFields['DeniedTech_NoShowPeriod'] = '';
        }
        //--- DeniedTech_BackOutGreater
        if(isset($updateFields['DeniedTech_BackOutGreater']) && empty($updateFields['DeniedTech_BackOutGreater'])){
            $updateFields['DeniedTech_BackOutGreater'] = 0;
            $updateFields['DeniedTech_BackOutPeriod'] = '';
        }
        //--- DeniedTech_PreferenceRatingSmaller
        if(isset($updateFields['DeniedTech_PreferenceRatingSmaller']) && empty($updateFields['DeniedTech_PreferenceRatingSmaller'])){
            $updateFields['DeniedTech_PreferenceRatingSmaller'] = 0;
            $updateFields['DeniedTech_PreferenceRatingPeriod'] = '';
        }
        //--- DeniedTech_PreferenceRatingSmaller
        if(isset($updateFields['DeniedTech_PerformanceRatingSmaller']) && empty($updateFields['DeniedTech_PerformanceRatingSmaller'])){
            $updateFields['DeniedTech_PerformanceRatingSmaller'] = 0;
            $updateFields['DeniedTech_PerformanceRatingPeriod'] = '';
        }
        
        
        //--- update
        //echo("class: ");print_r($updateFields);
        if(count($updateFields) > 0){
            $criteria = "Company_ID = '$companyID'";
            $result = Core_Database::update("clients",
                                $updateFields,$criteria);
        }
        return $result;
    }

	public function setWOVisitDisabled($login, $hash, $woVisitID, $flag) {
		// API wrapper of WosClass.php methods --- don't use unless for external API calls needing API_Response
		// flag - boolean
		$woVisit = array('VisitDisable' => ($flag ? 1 : 0));
		return $this->updateWOVisit($login, $hash, $woVisitID, $woVisit);
	}
	
	public function createWOVisit($login, $hash, $winNum, $woVisit) {
		// API wrapper of WosClass.php methods --- don't use unless for external API calls needing API_Response
		// woVisit is array containing keys for StartDate, StartTime, EndDate, EndTime, Duration, TechArrivalInstructions
       	$authData = array('login'=>$login, 'password'=>$hash);
       	$user = new Core_Api_User();
       	$user->checkAuthentication($authData);
       	if (!$user->isAuthenticate()) return API_Response::fail();
        $errors = Core_Api_Error::getInstance();
		$company = $user->getContextCompanyId();
		$wosClass = new Core_Api_WosClass();
		
		$wo = $this->getWorkOrdersWithWinNum($winNum,'WIN_NUM', "", $company);
		if (empty($wo)) {
			$errors->addError('6', 'Work order not found');
			return API_Response::fail();
		}
		if (empty($woVisit['StartTypeID'])) $woVisit['StartTypeID'] = 1;
		@$wosClass->saveWOVisit(NULL,$winNum,$woVisit['StartTypeID'],
			$woVisit['StartDate'],$woVisit['StartTime'],$woVisit['EndDate'],$woVisit['EndTime'],$woVisit['EstimatedDuration'],$woVisit['TechArrivalInstructions'],
			NULL,NULL,NULL, 1, $woVisit['VisitDisable']);
		$db = Core_Database::getInstance();
		$woVisitID = $db->lastInsertId('work_order_visit');
		
        $err = $errors->getErrors();
  		if (!empty($err))
			return API_Response::fail();
		return API_Response::success(array($woVisitID));
	}

	public function updateWOVisit($login, $hash, $woVisitID, $woVisit) {
		// API wrapper of WosClass.php methods --- don't use unless for external API calls needing API_Response
		// woVisit is array containing keys StartDate, StartTime, EndDate, EndTime, Duration, TechArrivalInstructions
		// NULL values will not be updated
       	$authData = array('login'=>$login, 'password'=>$hash);
       	$user = new Core_Api_User();
       	$user->checkAuthentication($authData);
       	if (!$user->isAuthenticate()) return API_Response::fail();
        $errors = Core_Api_Error::getInstance();
		if (!is_numeric($woVisitID)) {
			$errors->addError('2', 'Work Order Visist ID required');
			return API_Response::fail();
		}
		$company = $user->getContextCompanyId();

		$wosClass = new Core_Api_WosClass();
		$woVisitInfo = $wosClass->getWOVisitInfo($woVisitID);
		$wo = $this->getWorkOrdersWithWinNum($woVisitInfo['WIN_NUM'], 'Company_ID', "");
		
		if (empty($wo)) {
			$errors->addError('6', 'Work order not found');
			return API_Response::fail();
		}
		if (empty($woVisitInfo) || $wo[0]['Company_ID'] != $company) {
			$errors->addError('6', 'Work Order Visit not found');
			return API_Response::fail();
		}
		
		if (empty($woVisit)) {
			$errors->addError('2', 'No information to update');
			return API_Response::fail();
		}

		foreach ($woVisit as $key => $val) {
			// sets fields to update
			if (!array_key_exists($key, $woVisitInfo)) continue;
			$woVisitInfo[$key] = $val;
		}
		
		@$wosClass->saveWOVisit($woVisitInfo['WoVisitID'],$woVisitInfo['WIN_NUM'],$woVisitInfo['StartTypeID'],
			$woVisitInfo['StartDate'],$woVisitInfo['StartTime'],$woVisitInfo['EndDate'],$woVisitInfo['EndTime'],$woVisitInfo['EstimatedDuration'],$woVisitInfo['TechArrivalInstructions'],
			NULL,NULL,NULL, 1, $woVisitInfo['VisitDisable']);

		@$wosClass->saveWOVisitCheckInOutForClient($woVisitInfo['WoVisitID'],$woVisitInfo['WIN_NUM'],$woVisitInfo['CORCheckInDate'],$woVisitInfo['CORCheckInTime'],$woVisitInfo['CORCheckOutDate'],$woVisitInfo['CORCheckOutTime'],$woVisitInfo['OptionCheckInOut'],$woVisitInfo['Type'],$woVisitInfo['VisitDisable']);
		
        $err = $errors->getErrors();
  		if (!empty($err))
			return API_Response::fail();
		return API_Response::success(array($woVisitID));
	}
}
