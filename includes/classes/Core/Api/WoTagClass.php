<?php
class Core_Api_WoTagClass
{
    const TAG_ITEM_ID_NOSHOW = 38;
    const TAG_ITEM_ID_BACKOUT = 39;
    const TAG_ITEM_ID_SCHEDULE = 42;
    const TAG_ITEM_ID_Complete1stVisit = 25;
    const TAG_ITEM_ID_CompleteRevisit = 26;
    const TAG_ITEM_ID_CompleteReassigned = 27;
    const TAG_ITEM_ID_CompleteRescheduled = 28;
    
    //676
    const SITE_STATUS_ID_SCHEDULED = 1;
    const SITE_STATUS_ID_COMPLETE = 2;
    const SITE_STATUS_ID_CANCELED = 3;
    const SITE_STATUS_ID_RESCHEDULED = 4;
    //end 676
    //622
    const SYSTEM_STATUS_ID_ACTIVE = 1;
    const SYSTEM_STATUS_ID_PARTIAL_RESTRICTION = 2;
    const SYSTEM_STATUS_ID_RESTRICT = 3;
    const SYSTEM_STATUS_ID_DEACTIVED = 4;//576
    //end 622
    //13754     
    const WINTAG_STATUS_COMPLETE = 'Complete';
    const WINTAG_STATUS_CANCELLED = 'Cancelled';
    const WINTAG_STATUS_INCOMPLETE = 'Incomplete';
    const WINTAG_STATUS_OTHER = 'Other';  
    const WINTAG_STATUS_RESCHEDULED = 'Rescheduled';
    
    const WINTAG_OWNER_SITE_CLIENT = 'Site / Client';
    const WINTAG_OWNER_TECH_PROVIDER = 'Tech / Provider';
    const WINTAG_OWNER_TECH_NATURAL_CAUSES = 'Natural Causes';      
    //end 13754 Natural Causes
    
    public $tagCodeIdArray = array('ClientStatusId','ClientRelationshipId','SegmentId','OriginalSellerId','ExecutiveId','EntityTypeId','ServiceTypeId','SystemStatusId','FSClientServiceDirectorId','FSAccountManagerId');
    public $tagCodeIDArray_forProject = array('FSClientServiceDirectorId','EntityTypeId','ServiceTypeId','FSAccountManagerId');
    public $tagCodeIDArray_forWO = array('FSClientServiceDirectorId','EntityTypeId','ServiceTypeId','FSAccountManagerId');

    public function getSystemStatusList()
    {
        $code = "SystemStatus";
        return $this->getWoTagList_byCode($code);
    }
  

    public function getClientStatusList()
    {
        $code = "ClientStatus";
        return $this->getWoTagList_byCode($code);
    }
    public function getClientRelationShipList()
    {
        $code = "ClientRelationship";
        return $this->getWoTagList_byCode($code);
    }
    public function getSegmentList()
    {
        $code = "Segment";
        return $this->getWoTagList_byCode($code);
    }
    public function getOriginalSellerList()
    {
        $code = "OriginalSeller";
        return $this->getWoTagList_byCode($code);
    }
    public function getExecutiveList()
    {
        $code = "Executive";
        return $this->getWoTagList_byCode($code);
    }
    public function getEntityTypeList()
    {
        $code = "EntityType";
        return $this->getWoTagList_byCode($code);
    }
    public function getServiceTypeList()
    {
        $code = "ServiceType";
        return $this->getWoTagList_byCode($code);
    }
    
    public function getWoTagList_byCode($code)
    {
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from(array("woti"=>"wo_tag_items"),
                array('wo_tag_item_id'=>'id','wo_tag_item_code','wo_tag_item_name','wo_tag_code')
        );
        $select->join(array('wot'=>'wo_tags'),
                'wot.wo_tag_code = woti.wo_tag_code',
                array('wo_tag_name')
        );
        $select->where("woti.wo_tag_code='$code'");
        $select->where("woti.active=1");        
        $select->order("woti.display_order asc");
        $records = Core_Database::fetchAll($select);  
        return $records;                      
    }
    
    public function getTagItemInfo($tagItemId)
    {
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from(array('ti'=>'wo_tag_items'), array('*'));
        $select->join(array('t'=>'wo_tags'),
                't.wo_tag_code = ti.wo_tag_code',
                array('wo_tag_name')
        );
        $select->where("ti.id = '$tagItemId'");
        $records = Core_Database::fetchAll($select);  
        if(empty($records) || count($records)==0) return null;
        else return $records[0];
    }
    
    public function getTagInfo_ByTagCode($tagCode)
    {
        //id     wo_tag_code     wo_tag_name
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from('wo_tags');
        $select->where("wo_tag_code= '$tagCode'");
        $records = Core_Database::fetchAll($select);  
        if(empty($records) || count($records)==0) return null;
        else return $records[0];
    }

    public function getTagName_ByTagCode($tagCode)
    {
        //id     wo_tag_code     wo_tag_name
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from('wo_tags');
        $select->where("wo_tag_code= '$tagCode'");
        $records = Core_Database::fetchAll($select);  
        if(empty($records) || count($records)==0) return '';
        else return $records[0]['wo_tag_name'];
    }

    public function getOperationsStaffInfo($tagItemId)
    {
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from('operations_staff');
        $select->where("id = '$tagItemId'");
        $records = Core_Database::fetchAll($select);  
        if(empty($records) || count($records)==0) return null;
        else return $records[0];
    }
    
    //--- 13622
    public function saveClientTagValues($clientID,$tagArray,$updateBy='',$writeToLog=1)
    {
        //FSClientServiceDirectorId     
        /*array(
            [0] => array('TagCodeId'=>'ClientStatusId', 'TagItemId'=>'6'),
            [1] => array('TagCodeId'=>'ClientStatusId', 'TagItemId'=>'6')
        )*/
        /*array(
            [0] => array('TagCodeId'=>'FSClientServiceDirectorId', 'TagItemId'=>'9'),
        )*/
        if(empty($tagArray) || count($tagArray)==0) return;
        
        //--- get old data
        $oldClientTagValues = $this->getClientTagValues($clientID);
         
        //--- save new data
        $updateFields=array();
        foreach($tagArray as $tagRecord)
        {
            $tagCodeID = $tagRecord['TagCodeId'];
            $tagItemID = $tagRecord['TagItemId'];
            if(in_array($tagCodeID,$this->tagCodeIdArray))
            {
                if($tagItemID != $oldClientTagValues[$tagCodeID]['TagItemId'])
                {
                    $updateFields[$tagCodeID]=$tagItemID;
                }
                
            }
        }
        
        if(count($updateFields)>0)
        {
            if($this->exists_clientext_record($clientID)==1)
            {
                $criteria = "ClientId = " . $clientID;
                $result = Core_Database::update(Core_Database::TABLE_CLIENTS_EXT,
                                    $updateFields,
                                    $criteria                                           
                    );   
            }
            else
            {
                $updateFields['ClientId'] = $clientID;
                $result = Core_Database::insert(Core_Database::TABLE_CLIENTS_EXT,$updateFields);
            }
        }        
        //--- write to log
        if(!empty($updateBy) && $writeToLog==1 && count($updateFields)>0)
        {
            //Edited Client Profile:<br/>To: empty<br/>Changed FS Account Manager from: Aaron Acker<br/>To: Stephanie Bledsoe
            $logMessage = "Edited Client Profile:<br/>";
            foreach($updateFields as $k=>$v)
            {
                $tagCodeID = $oldClientTagValues[$k]['TagCodeId'];
                $tagCodeName = $oldClientTagValues[$k]['TagCodeName'];
                
                $oldTagItemId = $oldClientTagValues[$k]['TagItemId'];
                $oldTagItemName = $oldClientTagValues[$k]['TagItemName'];
                
                $newTagItemId = $v;
                $newTagItemName = $this->getTagItemName_ByTagItemId($tagCodeID,$newTagItemId);
                if(empty($oldTagItemName)) $oldTagItemName = 'empty';
                if(empty($newTagItemName)) $newTagItemName = 'empty';
                $logMessage.="Change $tagCodeName from $oldTagItemName <br/> To: $newTagItemName <br/>";
            }
            $result=Core_Database::insert("cliprofile_action_log",
                array('clientid'=>$clientID,'username'=>$updateBy,
                    'action'=>$logMessage,'logdate'=>date("Y-m-d H:i:s"))
            );
        }        
    }
    //
    //--- 13622
    public function saveProjectTagValues($projectID,$tagCodeID,$tagCodeValue)
    {
        if(empty($projectID) || empty($tagCodeID)) return;
        
        $updateFields=array();
        if(in_array($tagCodeID,$this->tagCodeIDArray_forProject))
        {
             $updateFields[$tagCodeID]=$tagCodeValue;
        }
        
        if(count($updateFields)>0)
        {
             $criteria = "Project_ID = '$projectID'";
             $result = Core_Database::update(Core_Database::TABLE_PROJECTS,
                                    $updateFields,
                                    $criteria                                          
             );              
        }
    }
    //--- 13622
    public function saveProjectTagArrayValues($projectTagArray,$appAllWO=false)
    {
         /*array(
            [0] => array('Project_ID'=>'123','TagCodeId'='ServiceTypeId', 'TagCodeValue'=>'15'),
            [1] => array('Project_ID'=>'45678',TagCodeId='ServiceTypeId', 'TagCodeValue'=>'14')
        )*/
        
         /*array(
            [0] => array('Project_ID'=>'123','TagCodeId'='FSClientServiceDirectorId', 'TagCodeValue'=>'9'),
            [1] => array('Project_ID'=>'45678',TagCodeId='FSClientServiceDirectorId', 'TagCodeValue'=>'10')
        )*/
        if(empty($projectTagArray) || count($projectTagArray)==0)   return;
        
        
        foreach($projectTagArray as $projectTag)
        {
            $projectID = $projectTag['Project_ID'];
            $tagCodeID = $projectTag['TagCodeId'];
            $tagCodeValue = $projectTag['TagCodeValue'];
            $this->saveProjectTagValues($projectID,$tagCodeID,$tagCodeValue);            
        }
        
        
        if($appAllWO)
        {
             foreach($projectTagArray as $projectTag)
             {
                 $projectID = $projectTag['Project_ID'];
                 $tagCodeID = $projectTag['TagCodeId'];
                 $tagCodeValue = $projectTag['TagCodeValue'];
                                  
                 if(in_array($tagCodeID,$this->tagCodeIDArray_forWO))
                 {
                     //--- get non-invoiced winnum of the project
                     $db = Core_Database::getInstance();  
                     $select = $db->select();
                     $select->from('work_orders',array('WIN_NUM'));
                     $select->where("Invoiced = 0");
                     $select->where("Project_ID ='$projectID'");
                     $records = Core_Database::fetchAll($select);  
                     //13809
                     $winNums = "";
                     if(!empty($records))
                     {
                         foreach($records as $rec)
                         {
                             $winNum = $rec['WIN_NUM'];
                             $winNums .= empty($winNums) ? $winNum: ','.$winNum;
                             /*
                             if($this->exists_wo_additional_fields_record($winNum)==0)
                             {
                                $insertFields = array("WINNUM"=>$winNum);                                
                                $result = Core_Database::insert("work_orders_additional_fields",$insertFields);
                             }
                             */
                         }
                     }
                     //--- update
                     if(!empty($winNums))
                     {
                         $updateFields = array();
                         $updateFields[$tagCodeID]=$tagCodeValue;
                         $criteria = "WINNUM IN ($winNums)";
                         $result = Core_Database::update("work_orders_additional_fields",$updateFields,$criteria);
                     } 
                     //end 13809
                 }
                 
             }
        }        
    }
    
    public function saveWOTag_forAllWOsInProject($projectID,$tagCodeID,$tagCodeValue)
    { 
        if(empty($projectID) || empty($tagCodeID)) return;
        $updateFields=array();
        if(in_array($tagCodeID,$this->tagCodeIDArray_forWO))
        {
             $updateFields[$tagCodeID]=$tagCodeValue;
        }
        
        if(count($updateFields)>0)
        {
            //--- get WIN_NUM in Project
            $winNumArray = $this->getWinNums_inProject($projectID);
            
            if(count($winNumArray) > 0)
            {
                $winNums = "";
                foreach($winNumArray as $winNum)
                {
                    if($this->exists_wo_additional_fields_record($winNum)==0)
                    {
                         $insertFields = array("WINNUM"=>$winNum);                                
                         $result = Core_Database::insert("work_orders_additional_fields",$insertFields);
                    }
                    if($winNums != "") $winNums .= ",";
                    $winNums .= $winNum;
                }
                
                $criteria = "WINNUM IN ($winNums)";
                $result = Core_Database::update("work_orders_additional_fields",
                                    $updateFields,
                                    $criteria                                          
                );                
            }                        
        }        
    }
    
    //--- 13634
    public function saveWOTags_fromProjectTags($winNum,$projectID)
    { 
        
        if(empty($projectID) || empty($winNum)) return;
        
        $coreProject = new Core_Api_ProjectClass();
        $projectInfo = $coreProject->getProjectById($projectID);
        if(empty($projectInfo)) return;
        
        //--- update WO tags
        $ServiceTypeId = empty($projectInfo['ServiceTypeId'])?0:$projectInfo['ServiceTypeId'];
        $EntityTypeId = empty($projectInfo['EntityTypeId'])?0:$projectInfo['EntityTypeId'];
        $FSClientServiceDirectorId = empty($projectInfo['FSClientServiceDirectorId'])?0:$projectInfo['FSClientServiceDirectorId']; 
        $FSAccountManagerId = empty($projectInfo['FSClientServiceDirectorId'])?0:$projectInfo['FSAccountManagerId'];
        
        $updateFields = array();
        $updateFields['ServiceTypeId']=$ServiceTypeId;
        $updateFields['EntityTypeId']=$EntityTypeId;
        $updateFields['FSClientServiceDirectorId']=$FSClientServiceDirectorId;
        $updateFields['FSAccountManagerId']=$FSAccountManagerId;
        if($this->exists_wo_additional_fields_record($winNum)==0)
        {
            $updateFields['WINNUM'] = $winNum;                                
            $result = Core_Database::insert("work_orders_additional_fields",$updateFields);
        }
        else
        {
            $criteria = "WINNUM='$winNum'";
            $result = Core_Database::update("work_orders_additional_fields",
                                    $updateFields,$criteria
            );
        }
        return;
    }
    
    public function saveWOTag($winNum,$tagCodeID,$tagCodeValue)
    {
        if(empty($winNum) || empty($tagCodeID)) return;
        
        $updateFields=array();
        if(in_array($tagCodeID,$this->tagCodeIDArray_forWO))
        {
             $updateFields[$tagCodeID]=$tagCodeValue;
        }
        
        if(count($updateFields)>0)
        {
            if($this->exists_wo_additional_fields_record($winNum)==0)
            {
                $updateFields['WINNUM'] = $winNum;                                
                $result = Core_Database::insert("work_orders_additional_fields",$updateFields);
            }
            else
            {
                $criteria = "WINNUM='$winNum'";
                $result = Core_Database::update("",$updateFields,$criteria);
            }
        }        
    }

    public function saveWOTags($winNum,$tagArray)
    {
        if(empty($winNum) || count($tagArray)==0) return;

        /*array(
            [0] => array('TagCodeId'=>'ServiceTypeId', 'TagCodeValue'=>'15'),
            [1] => array('TagCodeId'=>'EntityTypeId', 'TagCodeValue'=>'14')
        )*/
        $updateFields=array();
        foreach($tagArray as $tag)
        {
            $tagCodeID = $tag['TagCodeId'];
            $tagCodeValue = $tag['TagCodeValue'];
            if(in_array($tagCodeID,$this->tagCodeIDArray_forWO))
            {
                 $updateFields[$tagCodeID]=$tagCodeValue;
            }
        }
        
        if(count($updateFields)>0)
        {
            if($this->exists_wo_additional_fields_record($winNum)==0)
            {
                $updateFields['WINNUM'] = $winNum;                                
                $result = Core_Database::insert("work_orders_additional_fields",$updateFields);
            }
            else
            {
                $criteria = "WINNUM='$winNum'";
                $result = Core_Database::update("work_orders_additional_fields",
                                        $updateFields,$criteria
                );
            }
        }        
    }

    public function getWOTags($winNum)
    {
        $tagValues = array();
        $woAdditionInfo = $this->get_wo_additional_fields_info($winNum);
        
        foreach($this->tagCodeIDArray_forWO as $field)
        {
            $tagCodeId = $field;
            $tagItemId = "";
            $tagItemName = "";
            if(!empty($woAdditionInfo)) 
            {
                $tagItemId = $woAdditionInfo[$field];
            }
            if(!empty($tagItemId))
            {
                if($field == 'FSClientServiceDirectorId' || $field == 'FSAccountManagerId')
                {
                     $tagItemInfo = $this->getOperationsStaffInfo($tagItemId);
                     if(!empty($tagItemInfo)) $tagItemName = $tagItemInfo['name'];              
                }
                else
                {
                    $tagItemInfo = $this->getTagItemInfo($tagItemId);
                    if(!empty($tagItemInfo)) $tagItemName = $tagItemInfo['wo_tag_item_name'];
                }                
            }
            $item = array();
            $item['TagCodeId'] = $tagCodeId;
            $item['TagItemId'] = $tagItemId;
            $item['TagItemName'] = $tagItemName;                    
            $tagValues[$field] = $item;
        } 
        return $tagValues;
    }
    
    public function getWinNums_inProject($projectID)
    {
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from(Core_Database::TABLE_WORK_ORDERS,array('WIN_NUM'));
        $select->where("Project_ID = ?", $projectID);
        $records = Core_Database::fetchAll($select);  
        
        $winNumArray = array();
        if(!empty($records) && count($records)>0)
        {
            foreach($records as $rec)
            {
                $winNumArray[]=$rec['WIN_NUM'];
            }                
        }                
        return $winNumArray;
    }
    //--- 13622
    public function getCompanyTagValues($companyID)    
    {
        $coreUser = new Core_Api_User();
        $repClientID = $coreUser->getRepClientIDforCompany($companyID);
        return $this->getClientTagValues($repClientID);
    }
            
    //--- 13622
    public function getClientTagValues($clientID)
    {
        //--- clientInfo
        $coreUser = new Core_Api_User();
        $clientInfo = $coreUser->loadById($clientID);
        $companyID = $coreUser->getCompanyId();
        //echo("<br/>companyID: $companyID <br/>");
        
        //---
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from("clients_ext");
        $select->where("ClientId = ?", $clientID);
        $records = Core_Database::fetchAll($select);  
        if(empty($records) || count($records)==0) {
            return null;   
        } else  {
            $rec = $records[0];
            $tagValues = array();
            foreach($this->tagCodeIdArray as $field)
            {
                $item = array();
                if($field == 'FSClientServiceDirectorId' || $field == 'FSAccountManagerId')
                {
                    $tagCodeName = "";
                    $tagCodeId = $field;
                    $tagItemId = $rec[$field];
                    $tagItemName = "";
                    if($field == 'FSClientServiceDirectorId') 
                    {
                        $tagCodeName = 'FS Client Service Director';
                    }
                    else if($field == 'FSAccountManagerId')
                    {
                        $tagCodeName = 'FS Account Manager';
                    }
                    
                    if(!empty($tagItemId))
                    {
                        $tagItemInfo = $this->getOperationsStaffInfo($tagItemId);
                        if(!empty($tagItemInfo)) $tagItemName = $tagItemInfo['name'];
                    }
                    $item['TagCodeId'] = $tagCodeId;
                    $item['TagCodeName'] = $tagCodeName;
                    $item['TagItemId'] = $rec[$field];
                    $item['TagItemName'] = $tagItemName;         
				}
                else if($field=='SystemStatusId')
                {
					$tagCodeId = $field;
                    $tagItemId = $rec[$field];
                    $tagItemName = "";
                          
                    //13622
                    $systemStatusID = $coreUser->getSystemStatus($companyID);
                    if($systemStatusID==self::SYSTEM_STATUS_ID_RESTRICT)
                    {
                        $tagItemId=64;
                        $tagItemName = 'Restricted';  
                    }  else if($systemStatusID==self::SYSTEM_STATUS_ID_ACTIVE) {
                        $tagItemId=63;
                        $tagItemName = 'Full Access';//576                     
                    }  else if($systemStatusID==self::SYSTEM_STATUS_ID_PARTIAL_RESTRICTION) {
                        $tagItemId=41;
                        $tagItemName = 'Partial Restriction'; 
                    }  else {
                        $tagItemId=63;
                        $tagItemName = 'Full Access';//576                                                
                    }
                    $item['TagCodeId'] = $tagCodeId;
                    $item['TagCodeName'] = 'System Status';
                    $item['TagItemId'] = $tagItemId;
                    $item['TagItemName'] = $tagItemName;
                }
                else
                {
                    $tagCodeId = $field;
                    $tagCode = substr($tagCodeId,0,strlen($tagCodeId)-2);
                    $tagCodeName = $this->getTagName_ByTagCode($tagCode);
                    $tagItemId = $rec[$field];
                    $tagItemName = "";
                    if(!empty($tagItemId))
                    {
                        $tagItemInfo = $this->getTagItemInfo($tagItemId);
                        if(!empty($tagItemInfo)) 
                        {
                            $tagItemName = $tagItemInfo['wo_tag_item_name'];
                        }
                    }
                    
                    $item['TagCodeId'] = $tagCodeId;
                    $item['TagCodeName'] = $tagCodeName;
                    $item['TagItemId'] = $tagItemId;
                    $item['TagItemName'] = $tagItemName;
                }
                                
                $tagValues[$field] = $item;                
            }
            return $tagValues;
        }
    }
        
    public function getTagItemName_ByTagItemId($tagCodeID,$tagItemID)
    {
        $tagItemName = '';
        if(in_array($tagCodeID,$this->tagCodeIdArray))
        {
             if($tagCodeID == 'FSClientServiceDirectorId' || $field == 'FSAccountManagerId')
             {
                 $tagItemInfo = $this->getOperationsStaffInfo($tagItemID);
                 if(!empty($tagItemInfo)) $tagItemName = $tagItemInfo['name'];                 
             }
             else
             {
                 $tagItemInfo = $this->getTagItemInfo($tagItemID);
                 if(!empty($tagItemInfo)) $tagItemName = $tagItemInfo['wo_tag_item_name'];
             }
        }
        else
        {
            $tagItemName = $tagCodeID.'('.$tagItemID.')';
        }
        return $tagItemName;
    }
        
    public function exists_clientext_record($clientID)
    {
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from("clients_ext");
        $select->where("ClientId = ?", $clientID);
        $records = Core_Database::fetchAll($select);  
        if(empty($records) || count($records) == 0) return 0;
        else return 1;
    }
    
    public function exists_wo_additional_fields_record($winNum)
    {
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from("work_orders_additional_fields");
        $select->where("WINNUM = '$winNum'");
        $records = Core_Database::fetchAll($select);  
        if(empty($records) || count($records) == 0) return 0;
        else return 1;
    }
    
    public function get_wo_additional_fields_info($winNum)
    {
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from("work_orders_additional_fields");
        $select->where("WINNUM = '$winNum'");
        $records = Core_Database::fetchAll($select);  
        if(empty($records) || count($records) == 0) return null;
        else return $records[0];
    }
    
    public function create_clientext_record_ifnotexist($clientID)
    {
        if($this->exists_clientext_record($clientID)==0)
        {
            $insertFields = array("ClientId"=>$clientID,
                                "FSClientServiceDirectorId"=>0,
                                "FSAccountManagerId"=>0
                                );                                
            $result = Core_Database::insert(Core_Database::TABLE_CLIENTS_EXT,$insertFields);
        }
    }

    //--- 13741
    /*
    *  $payISO: '',0,1
    */
    public function getWinArray_ByISOInfo($payISO,$isoID,$isoIDText)
    {
        if(empty($payISO) && $payISO!==0 && empty($isoID) && empty($isoIDText)) return null;
        
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from(array('w'=>Core_Database::TABLE_WORK_ORDERS)
                ,array('WIN_NUM')
        );            
        
        //--- payISO
        if($payISO==0 || $payISO==1)
        {
            $winNumForPayISO = $this->getWinNumArray_ByPayISO($payISO);
            if($winNumForPayISO != null && count($winNumForPayISO)==0)
            {
                return array();
            }
            else if(!empty($winNumForPayISO) && count($winNumForPayISO)>0)
            {
                 $select->where("WIN_NUM IN (?)",$winNumForPayISO);
            }
        }
        //--- isoID
        if(!empty($isoID))
        {
            $companyID_1 = $this->getCompanyID_ByISOID($isoID);
            if(empty($companyID_1)) {
                return array();   
            } else {
                $select->where("Company_ID = '$companyID_1'");
            }
        }
        //--- isoid-text
        if(!empty($isoIDText))
        {
            $companyID_2 = $this->getCompanyID_ByISOID($isoIDText);
            if(empty($companyID_2)) {
                return array();   
            } else {
                $select->where("Company_ID = '$companyID_2'");
            }
        }
        $records = Core_Database::fetchAll($select);         
        //--- return   
        $wins = array();    
        if(!empty($records) && count($records)>0)
        {
            foreach($records as $rec)
            {
                $wins[]=$rec['WIN_NUM'];
            }
        } 

        return $wins; 
    }
    
    public function getWinNumArray_ByPayISO($payISO)
    {
        if(empty($payISO) && $payISO!==0) return null;

        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from(array('w'=>Core_Database::TABLE_WORK_ORDERS)
                ,array('WIN_NUM')
        );            
        $select->join(array('t'=>Core_Database::TABLE_TECH_BANK_INFO),
                't.TechID=w.Tech_ID'
        );
        if (!empty($payISO)){
            $select->where("t.ISO_Affiliation_ID IS NOT NULL AND t.ISO_Affiliation_ID <> 0");            
        }
        else if($payISO === '0')
        {
            $select->where("t.ISO_Affiliation_ID IS NULL OR t.ISO_Affiliation_ID = 0");                        
        }
        
        $records = Core_Database::fetchAll($select); 
        //--- return  
        $wins = array();
        if(!empty($records) && count($records)>0) return array();
        {
            foreach($records as $rec)
            {
                $wins[]=$rec['WIN_NUM'];
            }
        } 
        return $wins; 
    }
    //--- 13741
    public function getCompanyID_ByISOID($isoID)
    {
        if(empty($isoID)) return null;
        
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from(Core_Database::TABLE_ISO,array('Client_ID'));            
        $select->where("UNID='$isoID'");
        $records = Core_Database::fetchAll($select);  
        if(empty($records) || count($records)==0) return null;
        else return $records[0]['Client_ID'];
    }
    
    public function getWinArray_ByTagsInfo($clientStatusID,$clientRelationshipID,
        $segmentID,$originalSellerID,$executiveID,$CSD_ID,$AM_ID,$entityID,$serviceTypeID,$systemStatusID)
    {
        if(empty($clientStatusID) && empty($clientRelationshipID) && empty($segmentID) && empty($originalSellerID) && empty($executiveID) && empty($systemStatusID)) return null;
        
        $wins = array();
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from("work_orders",array('WIN_NUM'));
        
        //--- for client-tags condition
        $companyIDArray = $this->getCompanyArray_ByClientTagsInfo($clientStatusID,
            $clientRelationshipID,$segmentID,$originalSellerID,
            $executiveID,$systemStatusID
        );
        
        if(!empty($companyIDArray) && count($companyIDArray)>0)
        {
             $select->where("Company_ID IN (?)",$companyIDArray);
        }
        //--- for project-tags condition
        $projectIDArray = $this->getProjectIDArray_ByProjectTagsInfo($CSD_ID,$AM_ID,$serviceTypeID);
        
        if(!empty($projectIDArray) && count($projectIDArray)>0)
        {
             $select->where("Project_ID IN (?)",$projectIDArray);
        }
        //--- for wo-tags condition
        $winNumArray = $this->getWinNumArray_ByWOTagsInfo($entityID);
        if(!empty($winNumArray) && count($winNumArray)>0)
        {
             $select->where("WIN_NUM IN (?)",$winNumArray);
        }
                        
        $records = Core_Database::fetchAll($select); 
        //--- return       
        $wins = array();
        if(!empty($records) && count($records)>0) return array();
        {
            foreach($records as $rec)
            {
                $wins[]=$rec['WIN_NUM'];
            }
        } 

        return $wins; 
    }
    
    public function getWinNumArray_ByWOTagsInfo($entityID)    
    {
        if(empty($entityID)) return null;
        
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from('work_orders_additional_fields',array('WINNUM'));
        if(!empty($entityID))
        {
            $select->where("EntityTypeId = '$entityID'");
        }  
        $records = Core_Database::fetchAll($select);  
        
        $ret = array();
        if(empty($records) || count($records)==0) return null;
        else
        {
            foreach($records as $rec)
            {
                $ret[]=$rec['WINNUM'];
            }
        }
        return $ret;        
    }
    
    public function getProjectIDArray_ByProjectTagsInfo($CSD_ID,$AM_ID,$serviceTypeID)
    {
        if(empty($CSD_ID) && empty($AM_ID) && empty($serviceTypeID)) return null;
          
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from('projects',array('Project_ID'));
        
        if(!empty($CSD_ID))
        {
            $select->where("FSClientServiceDirectorId = '$CSD_ID'");
        }  
        if(!empty($AM_ID))
        {
            $select->where("FSAccountManagerId = '$AM_ID'");
        }  
        if(!empty($serviceTypeID))
        {
            $select->where("ServiceTypeId = '$serviceTypeID'");
        }  
        $records = Core_Database::fetchAll($select);  
        
        $ret = array();
        if(empty($records) || count($records)==0) return null;
        else
        {
            foreach($records as $rec)
            {
                $ret[]=$rec['Project_ID'];
            }
        }
        return $ret;        
  
    }
    
    public function getCompanyArray_ByClientTagsInfo($clientStatusId,
        $clientRelationshipId,$segmentId,$originalSellerId,
        $executiveId,$systemStatusId
    )
    {

        if(empty($clientStatusId) && empty($clientRelationshipId)) return null;
        
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from(array('c'=>'clients'),array('Company_ID'));
        $select->join(array('ce'=>'clients_ext'),
                'ce.ClientId = c.ClientID',
                array()
        );
        if(!empty($clientStatusId))
        {
            $select->where("ce.ClientStatusId = '$clientStatusId'");
        }
        if(!empty($clientRelationshipId))
        {
            $select->where("ce.ClientRelationshipId = '$clientRelationshipId'");
        }
        if(!empty($segmentId))
        {
            $select->where("ce.SegmentId = '$segmentId'");
        }
        if(!empty($originalSellerId))
        {
            $select->where("ce.OriginalSellerId = '$originalSellerId'");
        }
        if(!empty($executiveId))
        {
            $select->where("ce.ExecutiveId = '$executiveId'");
        }
        if(!empty($systemStatusId))
        {
            $select->where("ce.SystemStatusId = '$systemStatusId'");
        }
        $select->group("c.Company_ID");
        $records = Core_Database::fetchAll($select);  
        
        $ret = array();
        if(empty($records) || count($records)==0) return null;
        else
        {
            foreach($records as $rec)
            {
                $ret[]=$rec['Company_ID'];
            }
        }
        return $ret;        
    }
    
    public function updateSystemStatus($companyID)
    {
        $apiUser = new Core_Api_User();
        $clientIDArray = $apiUser->getClientIDArray_InCompany($companyID);
        $clientIDs = $apiUser->getClientIDs_InCompany($companyID);

        //--- insert if empty
        foreach($clientIDArray as $clientID)
        {
            //--- does client ext info exist?
            $db = Core_Database::getInstance();  
            $select = $db->select();
            $select->from('clients_ext');
            $select->where("ClientId = '$clientID'");
            $records = Core_Database::fetchAll($select);
            if(empty($records) && count($records)==0)
            {
                $insertFields = array("ClientId"=>$clientID,
                                    "FSClientServiceDirectorId"=>0,
                                    "FSAccountManagerId"=>0
                                    );                                
                $result = Core_Database::insert('clients_ext',$insertFields);
            }
        }
        //--- update SystemStatus        
        $tagItemId='';
        $systemStatusID = $apiUser->getSystemStatus($companyID);
        if($systemStatusID==self::SYSTEM_STATUS_ID_RESTRICT)
        {
            $tagItemId=64;
        }      
        else if($systemStatusID==self::SYSTEM_STATUS_ID_ACTIVE)
        {
            $tagItemId=63;
        }
        else if($systemStatusID==self::SYSTEM_STATUS_ID_PARTIAL_RESTRICTION)
        {
            $tagItemId=41;
        }       
        else if($systemStatusID==self::SYSTEM_STATUS_ID_DEACTIVED)//576
        {
            $tagItemId=90;
        }
        else 
        {
            $tagItemId=21;
        }
            
        $updateFields=array('SystemStatusId'=>$tagItemId);
        $criteria = "ClientId IN ($clientIDs)";
        $result = Core_Database::update("clients_ext",$updateFields,$criteria);
    }

    //--- 13672, 13686
    public function getCompletionStatusTagList($excludeTagItemCodes='')
    {
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from('wo_tag_items',array('wo_tag_item_id'=>'id','wo_tag_item_name'));
        $select->where("wo_tag_code = 'CompletionStatus'");        
		$select->where("active = 1");
        //13686
        if(!empty($excludeTagItemCodes) && is_array($excludeTagItemCodes) && count($excludeTagItemCodes) > 0) 
        {
            $select->where("wo_tag_item_code NOT IN (?)",$excludeTagItemCodes);
        }
        $select->order("display_order asc");
        $select->order("subdisplay_order asc");
        //end 13686
        //13686
        if(!empty($excludeTagItemCodes) && is_array($excludeTagItemCodes) && count($excludeTagItemCodes) > 0) 
        {
            $select->where("wo_tag_item_code NOT IN (?)",$excludeTagItemCodes);
        }
        //end 13686
        $records = Core_Database::fetchAll($select);                
        return $records;
    }
    //--- 13672, 13676, 13686
    /*
    *   value_id = 1: Not Invoiceable; 2: Invoiceable
    */
    public function getCompletionStatusForWin($winNum, $SaveBackoutNoShow = true)
    {
		$is_multiple_wos = true;

		if (!is_array ($winNum)) {
			//---- Create scheduled tag
			//13853
			//$this->createScheduledTag($winNum);
			//----  Are there BackOut Tech and NoShow Tech in this WO?
			if ($SaveBackoutNoShow) {
				$db = Core_Database::getInstance();  
				$select = $db->select();
				$select->from(Core_Database::TABLE_WORK_ORDERS,array('BackOut_Tech','NoShow_Tech'));
				$select->where("WIN_NUM='$winNum'");
				$records = Core_Database::fetchAll($select);
				if(empty($records) || count($records)==0) return null;
				else
				{
					$NoShow_Tech = $records[0]['NoShow_Tech'];
					$BackOut_Tech = $records[0]['BackOut_Tech'];
				}

				if(!empty($NoShow_Tech)){
					$this->saveWoTagItemValue($winNum,self::TAG_ITEM_ID_NOSHOW,1,0);
				}
				if(!empty($BackOut_Tech)){
					$this->saveWoTagItemValue($winNum,self::TAG_ITEM_ID_BACKOUT,1,0);
				}
			}

			$winNum = array ($winNum);
			$is_multiple_wos = false;
		}
                        
        //--- 13686, 13754,  13913
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from(array('wtiv'=>'work_order_tagitem_values'),
                array('wo_tag_item_value_id'=>'id','WIN_NUM','wo_tag_item_id','value_id','notes')                
        );
        $select->join(array('wti'=>'wo_tag_items'),
                'wti.id = wtiv.wo_tag_item_id',
                array('wo_tag_item_code','wo_tag_item_name','item_sortdesc','item_details','item_owner')
        );
        $select->where("wtiv.WIN_NUM IN (?)", $winNum);
        $select->where("wti.wo_tag_code = 'CompletionStatus'");
        $select->where("wti.active = 1");
        $records = Core_Database::fetchAll($select);  

		if ($is_multiple_wos) {
			$wos = array ();

			foreach ($records as $rec) {
				$win = $rec["WIN_NUM"];
				if (!isset ($wos[$win])) $wos[$win] = array ();
				$wos[$win][$rec['wo_tag_item_code']] = $rec;
			}

			return $wos;
		}
		else {
			if(empty($records) || count($records)==0) return null;
			$items = array();

			foreach($records as $rec) {
				$wo_tag_item_code = $rec['wo_tag_item_code'];
				$items[$wo_tag_item_code] = $rec;                
			}

			return $items;           
		}
    }

    //--- 13672
    public function deleteWoTagItemValue($woTagItemValueID)
    {
        if(empty($woTagItemValueID)) return;
        $db = Core_Database::getInstance();  
        $result = $db->delete('work_order_tagitem_values',"id='$woTagItemValueID'");   
        return $result;     
    }
    
    public function deleteWoTagItemValueIDs($woTagItemValueIDArray)
    {
        if(count($woTagItemValueIDArray)==0) return;
        $criteria = "";
        foreach($woTagItemValueIDArray as $woTagItemValueID)
        {
            $this->deleteWoTagItemValue($woTagItemValueID);
        }
    }
    //--- 13672
    public function deleteWoTagItemValue_byWin($winNum, $WoTagItemID)
    {
        $db = Core_Database::getInstance();  
        $critearia = "WIN_NUM='$winNum' AND wo_tag_item_id='$WoTagItemID'";
        $result = $db->delete('work_order_tagitem_values',$critearia);   
        return $result;     
    }
    //--- 13686
    public function deleteWoTagItemValue_byWinAndItemCode_forCompletionStatus($winNum, $WoTagItemCode)
    {
        //--- get WoTagItemID
        $WoTagItemID='';
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from('wo_tag_items');
        $select->where("wo_tag_code = 'CompletionStatus'");
        $select->where("wo_tag_item_code = '$WoTagItemCode'");
        $records = Core_Database::fetchAll($select); 
        if(empty($records) || count($records) == 0)
        {
            return;
        }
        else
        {
            $WoTagItemID=$records[0]['id'];
        }
        //--- delete
        $db = Core_Database::getInstance();  
        $critearia = "WIN_NUM='$winNum' AND wo_tag_item_id='$WoTagItemID'";
        $result = $db->delete('work_order_tagitem_values',$critearia);   
        return $result;     
    }
    //--- 13754
    /*
    *   $wtStatus:  Incomplete, Rescheduled, Complete, Cancelled
    *   $wtOwner:   Tech / Provider, Site / Client
    */
    public function deleteWoTagItemValue_byWinAndWtStatusAndWtOwner($winNum, $wtStatus,$wtOwner)
    {
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from(array('wotv'=>'work_order_tagitem_values'),
            array('id','WIN_NUM')
        );
        $select->join(array('woti'=>'wo_tag_items'),
            'woti.id = wotv.wo_tag_item_id',
            array()
        );
        $select->where("woti.wo_tag_code = 'CompletionStatus'");
        $select->where("wotv.WIN_NUM = '$winNum'");
        $select->where("woti.item_sortdesc = '$wtStatus'");
        $select->where("woti.item_owner = '$wtOwner'");
        $records = Core_Database::fetchAll($select); 
        
        if(empty($records) || count($records)==0){return;}
        
        $ids = '';
        foreach($records as $rec)
        {
            if($ids != ''){$ids.=',';}
            $ids .= $rec['id'];
        }
        
        //--- delete
        $db = Core_Database::getInstance();  
        $critearia = "id IN ($ids)";
        $result = $db->delete('work_order_tagitem_values',$critearia);   
        return $result;     
    }
    //--- 13754
    public function deleteWoTagItemValue_byWinAndIncompleteTechProvider($winNum)
    {
        //echo("hihihi");die();
        return $this->deleteWoTagItemValue_byWinAndWtStatusAndWtOwner($winNum,
            self::WINTAG_STATUS_INCOMPLETE,self::WINTAG_OWNER_TECH_PROVIDER
        );        
    }
    //--- 13754
    public function deleteWoTagItemValue_byWinAndIncompleteSiteClient($winNum)
    {
        return $this->deleteWoTagItemValue_byWinAndWtStatusAndWtOwner($winNum,
            self::WINTAG_STATUS_INCOMPLETE,self::WINTAG_OWNER_SITE_CLIENT
        );        
    }
    //--- 13754
    public function deleteWoTagItemValue_byWinAndRescheduledTechProvider($winNum)
    {
        return $this->deleteWoTagItemValue_byWinAndWtStatusAndWtOwner($winNum,
            self::WINTAG_STATUS_RESCHEDULED,self::WINTAG_OWNER_TECH_PROVIDER
        );        
    }
    //--- 13754 
    public function deleteWoTagItemValue_byWinAndRescheduledSiteClient($winNum)
    {
        return $this->deleteWoTagItemValue_byWinAndWtStatusAndWtOwner($winNum,
            self::WINTAG_STATUS_RESCHEDULED,self::WINTAG_OWNER_SITE_CLIENT
        );        
    }
    
    //--- 13672, 13676, 13686, 13827, 13828, 13871
    /*
    *   value_id = 1: Not Invoiceable; 2: Invoiceable
    */    
    public function saveWoTagItemValue($winNum,$WoTagItemID,$valueID,$updateIfExists=1,$dateAdded='',$addNew=0,$notes='',$WoTagItemValueID=0)
    {
        //--- does WoTagItemValue exist?
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from('work_order_tagitem_values');
        $select->where("WIN_NUM = '$winNum'");
        $select->where("wo_tag_item_id = '$WoTagItemID'");
        $records = Core_Database::fetchAll($select);     
        if((empty($records) || count($records)==0) || $addNew==1)
        {
            if(empty($dateAdded)) $dateAdded = date("Y-m-d H:i:s");
            $updateFields = array('WIN_NUM'=>$winNum,
                'wo_tag_item_id'=>$WoTagItemID,
                'value_id'=>$valueID,
                'DateAdded'=>$dateAdded);
            if(!empty($notes)){
                $updateFields['notes'] = $notes;
            }
            $result = Core_Database::insert("work_order_tagitem_values",$updateFields);
            
        }
        else if($updateIfExists==1)
        {
            //13828
            $wotivInfo = $records[0];
            $oldDateAdd = $wotivInfo['DateAdded'];
            
            $updateFields = array('value_id'=>$valueID);
            if(!empty($notes)){
                $updateFields['notes'] = $notes;
            }
            
            if(!empty($dateAdded)){
                $updateFields['DateAdded'] = $dateAdded;
            } else if(empty($oldDateAdd)) {
                $updateFields['DateAdded'] = date("Y-m-d H:i:s");
            }
            $criteria = "WIN_NUM = '$winNum' AND wo_tag_item_id = '$WoTagItemID'";
            //13871
            if(!empty($WoTagItemValueID)){
                $criteria.= " AND id = $WoTagItemValueID";
            }
            $result = Core_Database::update("work_order_tagitem_values",
                $updateFields,$criteria);
        }
        return $result;
    }
    
    //--- 13686, 13828, 13847
    public function saveWoTagItemValue_forCompletionStatus($winNum,$WoTagItemCode,$valueID,$updateIfExists=1,$dateAdded='',$notes='')
    {
        //--- get WoTagItemID
        $WoTagItemID='';
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from('wo_tag_items');
        $select->where("wo_tag_code = 'CompletionStatus'");
        $select->where("wo_tag_item_code = '$WoTagItemCode'");
        $records = Core_Database::fetchAll($select); 
        if(empty($records) || count($records) == 0)  {
            return;
        }  else {
            $WoTagItemID=$records[0]['id'];
        }
        //--- save 
        return $this->saveWoTagItemValue($winNum,$WoTagItemID,$valueID,$updateIfExists,$dateAdded,0,$notes);        
    }
    //--- 13676
    public function updateWoTagItemValue_ByID($woTagItemValueID,$updateWinNum,$updateWoTagItemID,$updateValueID,$updateDateAdd)
    {
        //id  WIN_NUM  wo_tag_item_id  value_id  DateAdded 
        $criteria = "id = '$woTagItemValueID'";
        $updateFields = array();
        if(!empty($updateWinNum)) $updateFields['WIN_NUM']=$updateWinNum;
        if(!empty($updateWoTagItemID)) $updateFields['wo_tag_item_id']=$updateWoTagItemID;
        if(!empty($updateValueID)) $updateFields['value_id']=$updateValueID;
        if(!empty($updateDateAdd)) $updateFields['DateAdded']=$updateDateAdd;
        
        if(count($updateFields)==0) return 0;
        $result = Core_Database::update("work_order_tagitem_values",$updateFields,$criteria);
        return $result;        
    }
    //--- 13676
    public function insertWoTagItemValue_ByID($updateWinNum,$updateWoTagItemID,$updateValueID,$updateDateAdd)
    {
        //id  WIN_NUM  wo_tag_item_id  value_id  DateAdded 
        $updateFields = array();
        if(!empty($updateWinNum)) $updateFields['WIN_NUM']=$updateWinNum;
        if(!empty($updateWoTagItemID)) $updateFields['wo_tag_item_id']=$updateWoTagItemID;
        if(!empty($updateValueID)) $updateFields['value_id']=$updateValueID;
        if(!empty($updateDateAdd)) 
        {
            $updateFields['DateAdded']=$updateDateAdd;    
        }
        else
        {
            $updateFields['DateAdded']=date("Y-m-d H:i:s");
        }
        
        if(count($updateFields)==0) return 0;
        
        $result = Core_Database::insert("work_order_tagitem_values",$updateFields);
        return $result;        
    }
    
    //--- 13672, 13826, 13871
    public function GetOnTimePerformceList_forWO($winNum, $skip_first_visit_check = false)
    {
		$get_total = function ($total_DurationScheduled_InMinutes, $total_DurationActual_InMinutes, $totalResultID) {
			$total_DurationScheduled_Minutes = $total_DurationScheduled_InMinutes % 60;
			$total_DurationScheduled_Hours = ($total_DurationScheduled_InMinutes - $total_DurationScheduled_Minutes)/60;            

			$total_DurationActual_Minutes = $total_DurationActual_InMinutes % 60;
			$total_DurationActual_Hours = ($total_DurationActual_InMinutes - $total_DurationActual_Minutes)/60;            
			//--- total_result_id
			$minutes = $total_DurationActual_InMinutes - $total_DurationScheduled_InMinutes;
			if($minutes < 0) $totalResultID = 1; //Under Duration
			else if($minutes==0) $totalResultID = 2; //Correct Duration
			else $totalResultID = 3; //Over Duration            
			//--- 

			return array('TotalSchedured_forDisplay'=>"$total_DurationScheduled_Hours hr $total_DurationScheduled_Minutes min",
				'TotalSchedured_InMinutes'=>$total_DurationScheduled_InMinutes,
				'TotalActual_forDisplay'=>"$total_DurationActual_Hours hr $total_DurationActual_Minutes min",
				'TotalActual_InMinutes'=>$total_DurationActual_InMinutes,
				'total_result_id'=>$totalResultID
			);
		};

		$result_is_array = true;

		if (!is_array ($winNum)) {
			$winNum = array ($winNum);
			$result_is_array = false;
		}

		if (!$skip_first_visit_check) {
			$apiWosClass = new Core_Api_WosClass();
			foreach ($winNum as $win) {
				$apiWosClass->createWOVisitIfEmpty($win);
			}
		}
        
        $criteria = "wovi.StartDate is not null and wovi.StartTime is not null
 and wovi.EndDate is not null and wovi.EndTime is not null";
 
/*wovi.CORCheckInDate is not null and wovi.CORCheckInTime is not null
 and wovi.CORCheckOutDate is not null and wovi.CORCheckOutTime is not null)
OR (wovi.TechCheckInDate is not null and wovi.TechCheckInTime is not null
 and wovi.TechCheckOutDate is not null and wovi.TechCheckOutTime is not null)*/
        $db = Core_Database::getInstance();
        $query = $db->select();
        $query->from(array('wovi'=>'work_order_visits'),array('*'));  
        $query->joinLeft(array('tipe'=>'work_order_timeperform_results'),
                'tipe.wovisit_id = wovi.WoVisitID',
                array('start_result_id','end_result_id','duration_result_id')
        );                         
        $query->where("wovi.WIN_NUM IN (?)", $winNum);
        $query->where("wovi.StartDate is not null OR wovi.StartTime is not null");
        //$query->where("wovi.EndDate is not null OR wovi.EndTime is not null");
        //13871
        $query->group(array ("wovi.WIN_NUM", "wovi.WoVisitID"));
        $query->distinct();
        $records = Core_Database::fetchAll($query);
        
        if(empty($records) || count($records) == 0) {
            return null;   
        } else {
			$last_win = false;
			$performanceResult = false;
			$result = array();

            //WoVisitID,WIN_NUM,StartScheduled
            foreach($records as $rec)
            {
				if ($rec["WIN_NUM"] != $last_win) {
					if ($performanceResult !== false) {
						$performanceResult['Total'] = $get_total ($total_DurationScheduled_InMinutes, $total_DurationActual_InMinutes, $totalResultID);
						$result[$last_win] = $performanceResult;
					}

					$performanceResult=array();
					$total_DurationScheduled_InMinutes = 0;
					$total_DurationActual_InMinutes = 0;
				}

                $startDate = $rec['StartDate'];
                $startTime = $rec['StartTime'];
                $endDate = $rec['EndDate'];
                $endTime = $rec['EndTime'];  
                $startTypeID = $rec['StartTypeID'];              
                //--- startDate is null
                if(empty($startDate)){
                    if(!empty($rec['TechCheckInDate'])){
                        $startDate = $rec['TechCheckInDate'];
                    } else if(!empty($rec['CORCheckInDate'])){
                        $startDate = $rec['CORCheckInDate'];
                    } else {
                        continue;
                    }
                }

                //---
                $ontimePerform = array(); 
                               
                $startScheTimestamp = strtotime($startDate.' '.$startTime);
                $startSche =  date('m/d/Y h:i A',$startScheTimestamp);
                
                $endSche = "";
                $endScheTimestamp = 0;
                if(!empty($endDate)){
                    $endScheTimestamp = strtotime($endDate.' '.$endTime);
                    $endSche =  date('m/d/Y h:i A',$endScheTimestamp);                    
                }
                
                $startActual="";
                $endActual="";
                
                if($rec['OptionCheckInOut']==1){
                    $startActualTimestamp=0;
                    if(!empty($rec['TechCheckInDate']) && !empty($rec['TechCheckInTime'])){
                        $startActualTimestamp = strtotime($rec['TechCheckInDate'].' '.$rec['TechCheckInTime']);
                        $startActual =  date('m/d/Y h:i A',$startActualTimestamp);
                    }
                    
                    $endActualTimestamp=0;
                    if(!empty($rec['TechCheckOutDate']) && !empty($rec['TechCheckOutTime'])){
                        $endActualTimestamp = strtotime($rec['TechCheckOutDate'].' '.$rec['TechCheckOutTime']);
                        $endActual =  date('m/d/Y h:i A',$endActualTimestamp);
                    }                  
                } else if($rec['OptionCheckInOut']==2){
                    $startActualTimestamp=0;
                    if(!empty($rec['CORCheckInDate']) && !empty($rec['CORCheckInTime'])){
                        $startActualTimestamp = strtotime($rec['CORCheckInDate'].' '.$rec['CORCheckInTime']);
                        $startActual =  date('m/d/Y h:i A',$startActualTimestamp);
                    }
                    
                    $endActualTimestamp=0;
                    if(!empty($rec['CORCheckOutDate']) && !empty($rec['CORCheckOutTime'])){
                        $endActualTimestamp = strtotime($rec['CORCheckOutDate'].' '.$rec['CORCheckOutTime']);
                        $endActual =  date('m/d/Y h:i A',$endActualTimestamp);
                    }                  
                }
                                
                
                $ontimePerform['WoVisitID']=$rec['WoVisitID'];
                $ontimePerform['WIN_NUM']=$rec['WIN_NUM'];
                
                //'start_result_id','end_result_id','duration_result_id', StartTypeID;                
                $ontimePerform['start_result_id']=$rec['start_result_id'];
                $ontimePerform['end_result_id']=$rec['end_result_id'];
                $ontimePerform['duration_result_id']=$rec['duration_result_id'];
                $ontimePerform['StartTypeID']=$rec['StartTypeID'];
                
                $ontimePerform['StartScheduled']=$startSche;
                $ontimePerform['EndScheduled']=$endSche;
                
                if($endScheTimestamp > 0 && $startScheTimestamp > 0){
                    $durationSche_InMinutes = round(abs($endScheTimestamp - $startScheTimestamp)/60,0);
                    $durationSche_Minutes = $durationSche_InMinutes % 60;
                    $durationSche_Hours = ($durationSche_InMinutes - $durationSche_Minutes)/60;                
                    $ontimePerform['DurationScheduled']=round(abs($endScheTimestamp - $startScheTimestamp)/3600,2);
                    $ontimePerform['DurationScheduled_forDisplay']= "$durationSche_Hours hr $durationSche_Minutes min";                    
                } else {
                    $ontimePerform['DurationScheduled']= 0;
                    $ontimePerform['DurationScheduled_forDisplay']= "";                    
                }
                
                if(!empty($startActual))
                {
                    $ontimePerform['StartActual']=$startActual;
                    
                }
                else
                {
                    $ontimePerform['StartActual']='';
                }
                
                if(!empty($endActual))
                {
                    $ontimePerform['EndActual']=$endActual;
                }
                else
                {
                    $ontimePerform['EndActual']='';
                }
                
                if(!empty($startActual) && !empty($endActual))                
                {
                    $durationActual_InMinutes = round(abs($endActualTimestamp - $startActualTimestamp)/60,0);
                    $durationActual_Minutes = $durationActual_InMinutes % 60;
                    $durationActual_Hours = ($durationActual_InMinutes - $durationActual_Minutes)/60;                
                    $ontimePerform['DurationActual']=round(abs($endActualTimestamp - $startActualTimestamp)/3600,2);
                    $ontimePerform['DurationActual_forDisplay']= "$durationActual_Hours hr $durationActual_Minutes min";
                }
                else
                {
                    $durationActual_InMinutes = 0;
                    $ontimePerform['DurationActual']=0;
                    $ontimePerform['DurationActual_forDisplay']='';
                }

                                
                //--- start_result_id
                //-- 13686, 13871
                if(empty($ontimePerform['start_result_id']) && !empty($startActual)) { 
                    //13871
                    if($startTypeID==1) { //Firm Start Time
                        $minutes = ($startActualTimestamp-$startScheTimestamp)/60;        
                        if($minutes < -15 ) $ontimePerform['start_result_id'] = 2; //Early
                        else if($minutes < 1 ) $ontimePerform['start_result_id'] = 1; //On Time
                        else $ontimePerform['start_result_id'] = 3; //Late-Major                         
                    } else if($startTypeID==2){ //Start Time Window
                        if($startActualTimestamp < $startScheTimestamp) {
                            $ontimePerform['start_result_id'] = 2; //Early
                        } else if($startScheTimestamp <= $startActualTimestamp && $startActualTimestamp <= $endScheTimestamp ) {
                            $ontimePerform['start_result_id'] = 1; //On Time   
                        } else if($endScheTimestamp < $startActualTimestamp) {
                            $ontimePerform['start_result_id'] = 3; //Late-Major                         
                        }
                    }        
                }
                else if($ontimePerform['start_result_id'] == 4){
                    $ontimePerform['start_result_id'] = 3;
                }
                                
                if($ontimePerform['start_result_id']==1) {
                    $ontimePerform['start_result_name'] = "On Time" ;                   
                } else if($ontimePerform['start_result_id']==2) {
                    $ontimePerform['start_result_name'] = "Early" ;                   
                } else if($ontimePerform['start_result_id']==3) {
                    $ontimePerform['start_result_name'] = "Late" ;                   
                } else {
                    $ontimePerform['start_result_name'] = "" ;                   
                }
                
                //--- end_result_id
                if(empty($ontimePerform['end_result_id']) && !empty($endActual)) {
                    //13871
                    if($startTypeID==1) { //Firm Start Time                  
                        $minutes = ($endActualTimestamp-$endScheTimestamp)/60;                 
                        if($minutes <= 0 ) $ontimePerform['end_result_id'] = 1; //On Time
                        else $ontimePerform['end_result_id'] = 2; //Late
                    } else if($startTypeID==2){ //Start Time Window
                        if($endActualTimestamp < $startScheTimestamp) {
                            $ontimePerform['end_result_id'] = -1; //Early
                        } else if($startScheTimestamp <= $endActualTimestamp && $endActualTimestamp <= $endScheTimestamp ) {
                            $ontimePerform['end_result_id'] = 1; //On Time   
                        } else if($endScheTimestamp < $endActualTimestamp) {
                            $ontimePerform['end_result_id'] = 2; //Late-Major 
                        }                        
                    }                    
                } else if($ontimePerform['end_result_id'] == 3) {
                    $ontimePerform['end_result_id'] = 2;
                }
                
                //13871
                if(empty($endTime) || empty($endDate)){
                    $ontimePerform['end_result_id'] = 0;                    
                }
                if(empty($endTime) && !empty($endDate)){
                    $timestamp = strtotime($endDate);
                    $endSche =  date('m/d/Y',$timestamp);
                    $ontimePerform['EndScheduled']=$endSche;
                }
                                
                if($ontimePerform['end_result_id']==1) {
                    $ontimePerform['end_result_name'] = "On Time" ;                   
                } else if($ontimePerform['end_result_id']==2) {
                    $ontimePerform['end_result_name'] = "Late" ;                   
                } else if($ontimePerform['end_result_id']==-1) {
                    $ontimePerform['end_result_name'] = "Early" ;                   
                } else {
                    $ontimePerform['end_result_name'] = "" ;                   
                }                
                //-- end 13686
                
                //--- duration_result_id
                if(empty($ontimePerform['duration_result_id']) && !empty($startActual) && !empty($endActual))
                {   
                    $minutes = $durationActual_InMinutes - $durationSche_InMinutes;                 
                    if($minutes < 0 ) $ontimePerform['duration_result_id'] = 1; //Under Duration
                    else if($minutes==0) $ontimePerform['duration_result_id'] = 2; //Correct Duration
                    else $ontimePerform['duration_result_id'] = 3; //Over Duration
                }
                
                if($ontimePerform['duration_result_id']==1) {
                    $ontimePerform['duration_result_name'] = "Under Duration" ;                   
                } else if($ontimePerform['duration_result_id']==2) {
                    $ontimePerform['duration_result_name'] = "Correct Duration" ;                   
                } else if($ontimePerform['duration_result_id']==3) {
                    $ontimePerform['duration_result_name'] = "Over Duration" ;                   
                } else {
                    $ontimePerform['duration_result_name'] = "" ;                   
                }

                $total_DurationScheduled_InMinutes += $durationSche_InMinutes;
                $total_DurationActual_InMinutes += $durationActual_InMinutes;
                $performanceResult[]=$ontimePerform;

				$last_win = $rec["WIN_NUM"];
            } //end of for           
            
            $performanceResult['Total'] = $get_total ($total_DurationScheduled_InMinutes, $total_DurationActual_InMinutes, $totalResultID);
			$result[$last_win] = $performanceResult;

			if (!$result_is_array) $result = $result[$winNum[0]];

			return $result;
        }//end of else
    }
    //--- 13672, 13686
    public function getStartResultList()
    {
        return array('0'=>'Select','2'=>'Early','1'=>'On Time','3'=>'Late');
    }
    //--- 13672, 13686
    public function getEndResultList()
    {
        return array('0'=>'Select','-1'=>'Early','1'=>'On Time','2'=>'Late');
    }
    //--- 13672, 13686
    public function getDurationResultList()
    {
        return array('0'=>'Select','1'=>'Short','2'=>'Expected','3'=>'Long');
    }
    
    //--- 13672
    public function saveOntimePerformanceResult($winNum,$woVisitID,$startResult,$endResult,$durationResult)
    {
        //--- does work_order_timeperform_results exist?
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from('work_order_timeperform_results');
        $select->where("wovisit_id = '$woVisitID'");
        $select->where("WIN_NUM = '$winNum'");
        $records = Core_Database::fetchAll($select);   
        
        $updateFields =  array('WIN_NUM'=>$winNum,'wovisit_id'=>$woVisitID);
        if(!empty($startResult)) $updateFields['start_result_id'] = $startResult;
        if(!empty($endResult)) $updateFields['end_result_id'] = $endResult;
        if(!empty($durationResult)) $updateFields['duration_result_id'] = $durationResult;
        
        
        if(empty($records) || count($records)==0)
        {
            $result=Core_Database::insert("work_order_timeperform_results",$updateFields);
        }
        else
        {
            $criteria = "WIN_NUM = '$winNum' AND wovisit_id = '$woVisitID'";
            $result=Core_Database::update("work_order_timeperform_results",$updateFields,$criteria);
        }
        return $result;
    }
    
    //---- 13672
    public function saveQualityPerformance_forWin($winNum,$SATRecommended,$SATPerformance)
    {
        if(empty($winNum)) return;
        if(empty($SATRecommended) && empty($SATPerformance)) return;
        
        $result = Core_Database::update(Core_Database::TABLE_WORK_ORDERS,
                array('SATRecommended'=>$SATRecommended,'SATPerformance'=>$SATPerformance),
                "WIN_NUM = '$winNum'"
        );
        return $result;        
    }
    
    //---- 13672
    public function getGeography($zipCode)
    {
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from('zipcode_sitetypes');
        $select->where("ZipCode = '$zipCode'");
        $records = Core_Database::fetchAll($select);   
        
        if(empty($records) || count($records) == 0) return '';
        else return $records[0]['SiteType'];
    }
    
    //--- 13676
    public function getCompletionStatusWinTags_InSiteOfProject($siteNumber,$projectID)
    {       
        $wosClass = new Core_Api_WosClass();
        $winNumArray = $wosClass->getWinNumArray_InSiteOfProject($siteNumber,$projectID);         
        if(empty($winNumArray) || count($winNumArray)==0) return null;
                
        //----  Add tags if there are BackOut Tech and NoShow Tech in the WO
        foreach($winNumArray as $winNum)
        {
            $this->createWinTag_NoShow_Backout_IfNotExists($winNum);
            //13853
            //$this->createScheduledTag($winNum);
        }
        //--- get list
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from(array('wtiv'=>'work_order_tagitem_values'),
                array('wo_tag_item_value_id'=>'id','WIN_NUM','wo_tag_item_id','value_id','DateAdded','notes')                
        );
        //13853
        $select->join(array('wti'=>'wo_tag_items'),
                'wti.id = wtiv.wo_tag_item_id',
                array('wo_tag_item_name','wo_tag_item_code','item_sortdesc','item_owner','item_details')
        );
        //13827
        $select->join(array('w'=>Core_Database::TABLE_WORK_ORDERS),
                'w.WIN_NUM = wtiv.WIN_NUM',
                array('WO_ID','SiteNumber','Status','SiteName','City','State','Zipcode','StartDate','StartTime','Tech_ID','Tech_FName','Tech_LName','FLS_ID')
        );
        //end 13827

        $select->where("wti.wo_tag_code = 'CompletionStatus'");
        //13853
        $select->where("wti.wo_tag_item_code <> 'Scheduled'");
        $select->where("wtiv.WIN_NUM IN (?)",$winNumArray);
        $select->order("DateAdded DESC");
        $records = Core_Database::fetchAll($select);     
        return $records;           
    }
    
    //--- 13676
    public function createWinTag_NoShow_Backout_IfNotExists($winNum)
    {
        //----  Are there BackOut Tech and NoShow Tech in this WO?
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from(Core_Database::TABLE_WORK_ORDERS,array('BackOut_Tech','NoShow_Tech'));
        $select->where("WIN_NUM='$winNum'");
        $records = Core_Database::fetchAll($select);
        if(empty($records) || count($records)==0) return null;
        else
        {
            $NoShow_Tech = $records[0]['NoShow_Tech'];
            $BackOut_Tech = $records[0]['BackOut_Tech'];
        }
        if(!empty($NoShow_Tech)){
            $this->saveWoTagItemValue($winNum,self::TAG_ITEM_ID_NOSHOW,1,0);
        }
        if(!empty($BackOut_Tech)){
            $this->saveWoTagItemValue($winNum,self::TAG_ITEM_ID_BACKOUT,1,0);
        }        
    }
    //--- 13906
    public function detailSiteStatusReport($projectID,$onlySiteNumber='',
            $startDateGreater='',$startDateLess='',
            $techMarkedCompleteDateGreater='',$techMarkedCompleteDateLess='',
            $approvedDateGreater='',$approvedDateLess='',
            $invoicedDateGreater='',$invoicedDateLess=''
        )
    {        
        //13857
        $projectIDs = "";
        if(is_array($projectID) && count($projectID) > 0){            
            foreach($projectID as $prjID){
                $projectIDs.= $projectIDs == ""? $prjID : ','.$prjID;
            }
        }else{
            $projectIDs = $projectID;
        }
        if(empty($projectIDs)) return null;        
        //end 13857        
        
        $wosClass = new Core_Api_WosClass();
        if(!empty($onlySiteNumber))
        {
            $winNumArray = $wosClass->getWinNumArray_InSiteOfProject($onlySiteNumber,$projectID);
            if(empty($winNumArray) || count($winNumArray)==0) return null;
        }
        else
        {
            $winNumArray = $wosClass->getWinNumArray_OfProject($projectID,
                $startDateGreater,$startDateLess,
                $techMarkedCompleteDateGreater,$techMarkedCompleteDateLess,
                $approvedDateGreater,$approvedDateLess,
                $invoicedDateGreater,$invoicedDateLess
            );
            if(empty($winNumArray) || count($winNumArray)==0) return null;    
        }
                
        //----  Add tags if there are BackOut Tech and NoShow Tech in the WO
        foreach($winNumArray as $winNum)
        {
            $this->createWinTag_NoShow_Backout_IfNotExists($winNum);
        }
        //13906        
        //--- GetOnTimePerformceList_forWO 
        $OnTimePerformceList = $this->GetOnTimePerformceList_forWO($winNumArray);
        
        //--- for first visit info of wins
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from(array('w'=>Core_Database::TABLE_WORK_ORDERS),
                array('WIN_NUM','Zipcode')
        );
        $select->joinLeft(array('wovi'=>'work_order_visits'),
            'wovi.WIN_NUM = w.WIN_NUM AND Type=1',
            array('CheckInDate'=>'(CASE WHEN OptionCheckInOut=2 THEN CORCheckInDate ELSE TechCheckInDate END)','CheckInTime'=>'(CASE WHEN OptionCheckInOut=2 THEN CORCheckInTime ELSE TechCheckInTime END)','CheckOutDate'=>'(CASE WHEN OptionCheckInOut=2 THEN CORCheckOutDate ELSE TechCheckOutDate END)','CheckOutTime'=>'(CASE WHEN OptionCheckInOut=2 THEN CORCheckOutTime ELSE TechCheckOutTime END)')
        );
        $select->where("w.WIN_NUM IN (?)",$winNumArray);        
        $select->order("w.WIN_NUM");        
        $select->distinct();
        $visitsInfoRecords = Core_Database::fetchAll($select);     
        $visitsInfoArray = array();
        $zipCodeArray = array();
        if(!empty($visitsInfoRecords)){
            foreach($visitsInfoRecords as $rec)
            {
                $visitsInfoArray[$rec['WIN_NUM']] = $rec;
                $zipCodeArray[]=$rec['Zipcode'];
            }
            $zipCodeArray = array_unique($zipCodeArray);
        }
        
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from("zipcode_sitetypes", array('ZipCode','SiteType'));
        $select->where("ZipCode IN (?)",$zipCodeArray);
        $zip_records = Core_Database::fetchAll($select);     
        $SiteTypeArray = array();
        if(!empty($zip_records) && count($zip_records)>0){
            foreach($zip_records as $rec) {
                $zip = $rec['ZipCode'];
                $SiteTypeArray["$zip"] = $rec['SiteType'];
            }
        }
        //return $SiteTypeArray;//test
        //end 13906

        //--- get list_1, 13871
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from(array('w'=>Core_Database::TABLE_WORK_ORDERS),
                array('WIN_NUM','WO_ID','SiteNumber','Status','SiteName','City','State','Zipcode','StartDate','StartTime','Tech_ID','Tech_FName','Tech_LName','FLS_ID','SATRecommended','SATPerformance','ShowTechs','DateEntered','calculatedTechHrs','Amount_Per','Qty_Devices')
        );
        $select->join(array('p'=>Core_Database::TABLE_PROJECTS),
                'p.Project_ID =  w.Project_ID',
                array('Project_Name')
        );
        $select->joinLeft(array('s'=>'sites'),
                "s.SiteNumber = w.SiteNumber AND s.Project_ID IN ($projectIDs)",
                array('Status_ID','StatusName'=>"(CASE WHEN Status_ID=2 THEN 'Completed' WHEN Status_ID=3 THEN 'Cancelled' WHEN Status_ID=4 THEN 'Rescheduled' ELSE 'Scheduled' END)")
        );        
        
        $select->where("w.WIN_NUM IN (?)",$winNumArray);        
        $select->order("w.WIN_NUM");
        
        $select->distinct();
        $records_1 = Core_Database::fetchAll($select);     
        
        //--- get list
        $db = Core_Database::getInstance();  
        $select = $db->select();
        //13827, 13854, 13871
        $select->from(array('w'=>Core_Database::TABLE_WORK_ORDERS),
                array('WIN_NUM','WO_ID','SiteNumber','Status','SiteName','City','State','Zipcode','StartDate','StartTime','Tech_ID','Tech_FName','Tech_LName','FLS_ID','SATRecommended','SATPerformance','ShowTechs','DateEntered','calculatedTechHrs','Amount_Per','Qty_Devices')
        );
        //end 13827, 13854
        $select->join(array('p'=>Core_Database::TABLE_PROJECTS),
                'p.Project_ID =  w.Project_ID',
                array('Project_Name')
        );

        $select->joinLeft(array('s'=>'sites'),
                "s.SiteNumber = w.SiteNumber AND s.Project_ID IN ($projectIDs)",
                array('Status_ID','StatusName'=>"(CASE WHEN Status_ID=2 THEN 'Completed' WHEN Status_ID=3 THEN 'Cancelled' WHEN Status_ID=4 THEN 'Rescheduled' ELSE 'Scheduled' END)")
        );
                
        $select->joinLeft(array('wtiv'=>'work_order_tagitem_values'),
               'w.WIN_NUM = wtiv.WIN_NUM',
                array('wo_tag_item_value_id'=>'id','wo_tag_item_id','value_id','DateAdded','notes')                
        );
        //13853
        $select->joinLeft(array('wti'=>'wo_tag_items'),
                'wti.id = wtiv.wo_tag_item_id',
                array('wo_tag_item_name','wo_tag_item_code','item_sortdesc','item_owner','item_details')
        );
        //13857
        $select->where("wti.wo_tag_code is null OR wti.wo_tag_code = 'CompletionStatus'");
        //13906
        $select->where("wti.wo_tag_item_code is null OR wti.wo_tag_item_code <> 'Scheduled'");        
        $select->where("w.WIN_NUM IN (?)",$winNumArray);        
        $select->order("w.WIN_NUM");
        $select->distinct();
        $records_withtag = Core_Database::fetchAll($select);  
        $records = array();
        if(!empty($records_withtag) && count($records_withtag)>0){
            foreach($records_withtag as $rec){
                $records[$rec['WIN_NUM']] = $rec;
            }
        }
        //end 13906
        
        if(empty($records_1) || count($records_1)==0) return null;

        $result = array();
        foreach($records_1 as $rec_1)
        {
            $winNum_1 = $rec_1['WIN_NUM'];
            $zipCode = $rec_1['Zipcode'];
            $geography = "";
            if(!empty($SiteTypeArray["$zipCode"])){
                $geography = $SiteTypeArray["$zipCode"];
            }
            $calculatedTechHrs = $rec_1['calculatedTechHrs'];
            if($rec_1['Amount_Per'] == 'Visit') {
                $calculatedTechHrs = $rec_1['Qty_Devices'];
            }
            $DatePart = Core_Api_CommonClass::WODatePart($rec_1['StartDate'], $rec_1['StartTime']);
            $LeadTime = Core_Api_CommonClass::WOLeadTime($rec_1['WIN_NUM'], $rec_1['StartDate'], $rec_1['ShowTechs'], $rec_1['DateEntered'], $rec_1['Status']);

            $tmp = null;
            if(!empty($records[$winNum_1])){
                $tmp=$records[$winNum_1];
            } else {
                $tmp=$rec_1;
            }
            if(!empty($visitsInfoArray[$winNum_1])){
                $visitInfo = $visitsInfoArray[$winNum_1];
                $tmp['CheckInDate'] = $visitInfo['CheckInDate'];
                $tmp['CheckInTime'] = $visitInfo['CheckInTime'];
                $tmp['CheckOutDate'] = $visitInfo['CheckOutDate'];
                $tmp['CheckOutTime'] = $visitInfo['CheckOutTime'];
            } else {
                $tmp['CheckInDate'] = "";
                $tmp['CheckInTime'] = "";
                $tmp['CheckOutDate'] = "";
                $tmp['CheckOutTime'] = "";
            }
            $tmp['calculatedTechHrs'] = $calculatedTechHrs;
            $tmp['Geography'] = $geography;
            $tmp['DatePart'] = $DatePart;
            $tmp['LeadTime'] = $LeadTime;
            
            if ($tmp['item_sortdesc'] == 'Cancelled') {
                $tmp['item_sortdesc'] = 'Site Cancelled ';
            }
            $find = array("<sup>", "</sup>");
            $tmp['item_details'] = str_replace($find, "", $tmp['item_details']);

            $value = "";
            if ($tmp['value_id'] == 1)
            {
                $value = "No";
            } else if ($tmp['value_id'] == 2)
            {
                $value = "Billable";
            }
            $tmp['value'] = $value;
            if(empty($tmp['SATRecommended']) || $tmp['SATRecommended'] < 0){
                $tmp['SATRecommended'] = 3;
            }
            if(empty($tmp['SATPerformance']) || $tmp['SATPerformance'] < 0){
                $tmp['SATPerformance'] = 3;
            }
        
            if(!empty($OnTimePerformceList[$winNum_1]))
            {                
                $OnTimePerformce = $OnTimePerformceList[$winNum_1][0];
                $OnTimePerformceTotal = $OnTimePerformceList[$winNum_1]['Total'];
                
                $tmp['start_result_name'] = $OnTimePerformce['start_result_name'];
                $tmp['end_result_name'] = $OnTimePerformce['end_result_name'];
                $tmp['duration_result_name'] = $OnTimePerformce['duration_result_name'];
                $tmp['ExpectedDuration'] = $OnTimePerformce['DurationScheduled_forDisplay'];
                $tmp['ActualDuration'] = $OnTimePerformce['DurationActual_forDisplay'];
                $tmp['TotalDuration'] = $OnTimePerformceTotal['TotalActual_forDisplay'];
            } else {
                $tmp['start_result_name'] = "";
                $tmp['end_result_name'] = "";
                $tmp['duration_result_name'] = "";
                $tmp['ExpectedDuration'] = "";
                $tmp['ActualDuration'] = "";
                $tmp['TotalDuration'] = "";
            }
            $result[]=$tmp;
            //end 13906            
        }
        return $result; 
                         
    }
    // end 13906
    
    //--- 13676, 13754, 13857
    public function highLevelProjectStatusReport($projectID,
            $startDateGreater='',$startDateLess='',
            $techMarkedCompleteDateGreater='',$techMarkedCompleteDateLess='',
            $approvedDateGreater='',$approvedDateLess='',
            $invoicedDateGreater='',$invoicedDateLess=''
        )
    {
        //13857
        $projectIDs = "";
        if(is_array($projectID)){
            foreach($projectID as $prjID){
                $projectIDs.= $projectIDs == ""? $prjID : ','.$prjID;
            }
        }else{
            $projectIDs = $projectID;
        }
        if(empty($projectIDs)) return null;
        //end 13857        
        
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from(array('s'=>Core_Database::TABLE_SITES),
            array('SiteNumber','Status_ID','StatusName'=>"(CASE WHEN Status_ID=2 THEN 'Completed' WHEN Status_ID=3 THEN 'Cancelled' WHEN Status_ID=4 THEN 'Rescheduled' ELSE 'Scheduled' END)",
                'Address','City','State','Zipcode','Region'=>"(CASE WHEN 1=1 THEN '' END)",
                'StartDate','CompleteDate','CanceledDate'
            )
        );
        $select->join(array('p'=>Core_Database::TABLE_PROJECTS),
            'p.Project_ID = s.Project_ID',
            array('Project_Name')
        );
        //City     State
        //13857
        $select->where("s.Project_ID IN ($projectIDs)");
        $select->order("s.SiteNumber ASC");
        $records = Core_Database::fetchAll($select);     
        if(empty($records) || count($records)==0){
            return null;
        } else {
            $siteArray = array();
            foreach($records as $rec)
            {
                $siteNumber = $rec['SiteNumber'];
                $this->updateStartDateForSite_fromWOdata($siteNumber,$projectID);
                
                $db = Core_Database::getInstance();  
                $select = $db->select();
                $select->from(array('w'=>Core_Database::TABLE_WORK_ORDERS),array('WIN_NUM'));
                if(!empty($techMarkedCompleteDateGreater) || !empty($techMarkedCompleteDateLess))
                {
                    $select->joinLeft(array('ts'=>Core_Database::TABLE_TIMESTAMPS),
                        "w.WIN_NUM = ts.WIN_NUM",
                        array('DateTime_Stamp','Description')
                    );
                }
                //13857
                $select->where("w.Project_ID IN ($projectIDs)");
                $select->where("w.SiteNumber = '$siteNumber'");
                                
                if(!empty($startDateGreater))
                {
                    $select->where("w.StartDate >= '$startDateGreater'");
                }                    
                if(!empty($startDateLess))
                {
                    $select->where("w.StartDate <= '$startDateLess'");
                }                                        
                
                if(!empty($techMarkedCompleteDateGreater) || !empty($techMarkedCompleteDateLess))
                {
                    $select->where("ts.Description = 'Work Order Marked: Completed'");
                }

                if(!empty($techMarkedCompleteDateGreater))
                {
                    $select->where("ts.DateTime_Stamp  >= '$techMarkedCompleteDateGreater'");
                }
                if(!empty($techMarkedCompleteDateLess))
                {
                    $select->where("ts.DateTime_Stamp  <= '$techMarkedCompleteDateLess'");
                }
                
                $select->order("w.WIN_NUM DESC");
                $select->distinct();                   
                $wos = Core_Database::fetchAll($select);     
                if(!empty($wos) && count($wos) > 0)
                {
                    $siteArray[]=$rec;                        
                }
            }                
            return $siteArray;
        }
    }
    
    
    //--- 13622
    /*
    *   return: array with keys: 'id','wo_tag_code','wo_tag_item_code','wo_tag_item_name'
    */
    public function getDefaultServiceType()
    {
        $defaultServiceTypeCode = "SelfService";
        
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from("wo_tag_items",
            array('id','wo_tag_code','wo_tag_item_code','wo_tag_item_name')
        );
        $select->where("wo_tag_code = 'ServiceType' ");
        $select->where("wo_tag_item_code = '$defaultServiceTypeCode' ");        
        $records = Core_Database::fetchAll($select);     
        if(empty($records) || count($records)==0){
            return null;
        } else {
            return $records[0];
        }       
    }

    //--- 13622
    /*
    *   return: the array with keys: id, groupid, name, email, phone
    */
    public function getDefaultCSD()
    {
        $tagItemId= 10;
        return $this->getOperationsStaffInfo($tagItemId);        
    }
    
    //--- 13686, 13676
    public function createScheduledTag($winNum)
    {
        //--- get wo info
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from(Core_Database::TABLE_WORK_ORDERS);
        $select->where("WIN_NUM = '$winNum'");
        $records = Core_Database::fetchAll($select);     
        if(empty($records) || count($records)==0){
                return null;
        }
        
        $wo = $records[0];
        $siteNumber = $wo['SiteNumber'];
        $projectID = $wo['Project_ID'];
                
        //--- publish date        
        $assignedDate = "";
        if(!empty($wo['Tech_ID'])) 
        {
            $lastAssignedDate = API_Timestamp::getLastWinAssignedTimestamp($wo['WIN_NUM']);    
            $assignedtime = Core_Api_CommonClass::displayDate('h:i A', $lastAssignedDate);
            $assignedtime = (substr($assignedtime,0,1)==0)?substr($assignedtime,(0-strlen($assignedtime)+1)):$assignedtime;
            $assignedDate = Core_Api_CommonClass::displayDate('n/j/Y', $lastAssignedDate).' '.$assignedtime;
        }
        
        $publishedDate = "";
        if($wo['ShowTechs']==1)
        {
            $publishedDate = $wo['DateEntered'];
            $publishedime = Core_Api_CommonClass::displayDate('h:i A', $lastAssignedDate);
            $publishedime = (substr($publishedime,0,1)==0)?substr($publishedime,(0-strlen($publishedime)+1)):$publishedime;
            $publishedDate = Core_Api_CommonClass::displayDate('n/j/Y', $publishedDate).' '.$publishedime;
        }
        else if($wo['ShowTechs']!=1 && strtolower($wo['Status'] == "assigned"))
        {
            $publishedDate = API_Timestamp::getLastWinAssignedTimestamp($wo['WIN_NUM']);
            $publishedime = Core_Api_CommonClass::displayDate('h:i A', $publishedDate);
            $publishedime = (substr($publishedime,0,1)==0)?substr($publishedime,(0-strlen($publishedime)+1)):$publishedime;
            $publishedDate = Core_Api_CommonClass::displayDate('n/j/Y', $publishedDate).' '.$publishedime;
        }
        
        //--- create scheduled tab
        if(!empty($publishedDate))
        {
            $WoTagItemID = self::TAG_ITEM_ID_SCHEDULE;
            
            //--- does WoTagItemValue exist?
            $db = Core_Database::getInstance();  
            $select = $db->select();
            $select->from('work_order_tagitem_values');
            $select->where("WIN_NUM = '$winNum'");
            $select->where("wo_tag_item_id = '$WoTagItemID'");
            $records = Core_Database::fetchAll($select);     
            if(empty($records) || count($records)==0)
            {
                //--- insert
                $updateFields = array('WIN_NUM'=>$winNum,
                    'wo_tag_item_id'=>$WoTagItemID,
                    'value_id'=> 0 ,
                    'DateAdded'=>date("Y-m-d H:i:s"));                    
                $result = Core_Database::insert("work_order_tagitem_values",$updateFields);
                //--- update Site Status
                $prjClass = new Core_Api_ProjectClass();
                $prjClass->saveSiteStatus_BySiteOfProject($siteNumber,$projectID,
                            self::SITE_STATUS_ID_SCHEDULED);
            }            
        }        
        return;
    }
    
    //--- 13676
    public function getWinNumArray_BySiteStatus($companyID,$siteStatusID)
    {        
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from(array('w'=>Core_Database::TABLE_WORK_ORDERS),
            array('WIN_NUM','Company_ID','Project_ID','SiteNumber')
        );
        $select->join(array('s'=>Core_Database::TABLE_SITES),
            's.SiteNumber = w.SiteNumber AND s.Project_ID=w.Project_ID',
            array('Status_ID')
        );
        $select->join(array('p'=>'projects'),
            'p.Project_ID = w.Project_ID',
            array('EnableSiteStatusReporting')
        );
        //EnableSiteStatusReporting, Project_ID  projects
        $select->where("w.Company_ID = '$companyID'");
        $select->where("s.Status_ID = '$siteStatusID'");
        if($siteStatusID==self::SITE_STATUS_ID_SCHEDULED)
        {
            $select->where("p.EnableSiteStatusReporting = 1");
        }
        $records = Core_Database::fetchAll($select);  
        
        $ret = array();
        if(empty($records) || count($records)==0) {
            return null;   
        }  else {
            foreach($records as $rec)
            {
                $ret[]=$rec['WIN_NUM'];
            }
        }
        return $ret;                
    }
    
    //--- 13676
    public function updateStartDateForSite_fromWOdata($siteNumber, $projectID)
    {
        if(empty($siteNumber) || empty($projectID)) return;
        //--- min StartDate of WO
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from(Core_Database::TABLE_WORK_ORDERS,
            array('StartDateMin'=>'(MIN(StartDate))')
        );
        $select->where("Project_ID = '$projectID'");
        $select->where("SiteNumber = '$siteNumber'");
        $records = Core_Database::fetchAll($select);  
        
        if(!empty($records) && count($records) > 0)
        {
            $startDateMin = $records[0]['StartDateMin'];
            if(!empty($startDateMin))
            {
                $criteria = "Project_ID='$projectID' AND SiteNumber = '$siteNumber'";
                $updateFields = array('StartDate'=>$startDateMin);
                $result = Core_Database::update(Core_Database::TABLE_SITES,$updateFields,$criteria);
                return $result;
            }
        }
    }
    //--- 13622
    public function updateAM_ofWO_fromAM_ofCompany($winNum,$companyID)
    {
        if(empty($winNum) || empty($companyID)) { return 0;}
        //--- get AM_ID of company
        $companyTagArray = $this->getCompanyTagValues($companyID);
        if(empty($companyTagArray) || count($companyTagArray)==0) {return 0;}
        
        $FSAccountManagerId = $companyTagArray['FSAccountManagerId']['TagItemId'];        
        
        //--- update AM of WO
        if(!empty($FSAccountManagerId))
        {
            $criteria = "WINNUM = '$winNum' ";
            $updateFields = array('FSAccountManagerId'=>$FSAccountManagerId);
            $result = Core_Database::update("work_orders_additional_fields",
                                $updateFields,$criteria);
            return $result;                    
        }else{
            return 0;
        }        
    }
    
    //--- 13686
    public function adjustSiteStatus($winNum)
    {
        //--- get Sitenumber, Project_ID of the WO
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from(Core_Database::TABLE_WORK_ORDERS,
            array('Project_ID','SiteNumber')
        );
        $select->where("WIN_NUM = '$winNum'");
        $records = Core_Database::fetchAll($select);  
        
        if(empty($records) || count($records)==0) return;
        
        $wo = $records[0];
        $projectID = $wo['Project_ID'];
        $siteNumber = $wo['SiteNumber'];
        
        if(empty($projectID) || empty($siteNumber)) return;
        
        //--- Site Status Reporting enabled  ?
        $projectClass = new Core_Api_ProjectClass();
        $projectInfo = $projectClass->getProject($projectID);
        
        if(empty($projectInfo) || count($projectInfo)==0) return;
        
        $EnableSiteStatusReporting = $projectInfo['EnableSiteStatusReporting'];
        
        if(empty($EnableSiteStatusReporting) || $EnableSiteStatusReporting != 1) return;
        
        //--- do wo have a tag in 'Complete' header ?
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from('work_order_tagitem_values');
        $select->where("WIN_NUM = '$winNum'");
        $select->where("wo_tag_item_id IN (25,26,27,28)");
        $records = Core_Database::fetchAll($select);  
        
        if(!empty($records) && count($records)>0)
        {
            //--- set site-status to complete
            $projectClass->saveSiteStatus_BySiteOfProject($siteNumber,$projectID,self::SITE_STATUS_ID_COMPLETE);
        }
    }

    //--- 13686
    public function getIncompleteSiteTagList()
    {
        return $this->getSubTagList("IncompleteSiteOrClientDriven_");
    }
    //--- 13686
    public function getIncompleteTechTagList()
    {
        return $this->getSubTagList("IncompleteTechDriven_");
    }
    //--- 13686
    public function getRescheduledSiteTagList()
    {
        return $this->getSubTagList("RescheduledSiteOrClientDriven_");
    }
    //--- 13686
    public function getRescheduledTechTagList()
    {
        return $this->getSubTagList("RescheduledTechDriven_");
    }
    //--- 13686
    public function getSubTagList($subTagItemCode)
    {
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from('wo_tag_items',array('wo_tag_item_id'=>'id','wo_tag_item_code','wo_tag_item_name','item_details'));
        $select->where("wo_tag_code = 'CompletionStatus'");
        $select->where("wo_tag_item_code LIKE '$subTagItemCode"."%'");
        $select->order("display_order asc");
        $select->order("subdisplay_order asc");        
        $records = Core_Database::fetchAll($select);                
        return $records;
    }
    //--- 13686
    /*
    *   value_id = 1: Not Invoiceable; 2: Invoiceable
    */
    public function getSubTagListForWin($winNum,$subTagItemCode)
    {            
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from(array('wtiv'=>'work_order_tagitem_values'),
                array('wo_tag_item_value_id'=>'id','WIN_NUM','wo_tag_item_id','value_id','notes')                
        );
        $select->join(array('wti'=>'wo_tag_items'),
                'wti.id = wtiv.wo_tag_item_id',
                array('wo_tag_item_code','wo_tag_item_name','item_sortdesc','item_owner','item_details')
        );
        $select->where("wtiv.WIN_NUM='$winNum'");
        $select->where("wti.wo_tag_code = 'CompletionStatus'");
        $select->where("wti.wo_tag_item_code LIKE '$subTagItemCode"."%'");
        $select->order("display_order asc");
        $select->order("subdisplay_order asc");
        $records = Core_Database::fetchAll($select);  
        
        if(empty($records) || count($records)==0)
        {
            return null;
        }   
        else
        {
            $items = array();
            foreach($records as $rec)
            {
                $wo_tag_item_code = $rec['wo_tag_item_code'];
                $items[] = $rec;                
            }
        }
        return $items;           
    }

    //--- 13686
    public function getIncomplete_SiteClient_TagListForWin($winNum)
    {            
        return $this->getSubTagListForWin($winNum,"IncompleteSiteOrClientDriven_");
    }
    //--- 13686
    public function getIncomplete_TechProvider_TagListForWin($winNum)
    {            
        return $this->getSubTagListForWin($winNum,"IncompleteTechDriven_");
    }
    //--- 13686
    public function getRescheduled_SiteClient_TagListForWin($winNum)
    {            
        return $this->getSubTagListForWin($winNum,"RescheduledSiteOrClientDriven_");
    }
    //--- 13686
    public function getRescheduled_TechProvider_TagListForWin($winNum)
    {            
        return $this->getSubTagListForWin($winNum,"RescheduledTechDriven_");
    }
    //--- 13754
    /*
    Return: Complete
            Cancelled
            Incomplete
            Other

    Notes:
            If a ?Complete? WIN-Tag is added for a WO and saved, display tag with green checkmark (overrides all others)
            If an Incomplete WIN-Tag is added for a WO and saved, display tag with red ?x?
            If the ?Cancelled? wintag is added for a WO and saved, display tag with grey circle (overrides ?Incomplete?)
            If WO is ?Scheduled? or has only ?Rescheduled? tags, display the same basic tag as we do currently
    */
    public function getWinTagStatusForWin($winNum)
    {
		$result = $this->getWinTagStatusSummaryByRecords (
			$this->getCompletionStatusForWin($winNum)
		);

		return $result["code"];
    }

    //13847
    public function saveNotesForWoTagItem($WoTagItemValueID,$notes)
    {
        if(empty($WoTagItemValueID)) return;
        $updateFields = array('notes'=>$notes);
        $criteria = "id = '$WoTagItemValueID'";
        $result = Core_Database::update("work_order_tagitem_values",
                $updateFields,$criteria);
    }
    
    //13843
    public function updateSiteStatus_ByWinTagStatus($woTagItemValueID)
    {
        //--- get  woTagItemValueInfo
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from(array('wotiv'=>'work_order_tagitem_values'),
            array('id','WIN_NUM','wo_tag_item_id')
        );
        $select->join(array('woti'=>'wo_tag_items'),
            'woti.id=wotiv.wo_tag_item_id',
            array('wotistatus'=>'item_sortdesc','')
        );        
        $select->where("wotiv.id = '$woTagItemValueID'");
        $select->where("woti.wo_tag_code = 'CompletionStatus'");
        $records = Core_Database::fetchAll($select);  
        if(empty($records) || count($records)==0) return;
                
        $wotiv = $records[0];
        $wotistatus = $wotiv['wotistatus'];
        $winNum = $wotiv['WIN_NUM'];
        $siteStatusID = 0;
        if($wotistatus==self::WINTAG_STATUS_COMPLETE) {
            $siteStatusID = self::SITE_STATUS_ID_COMPLETE;
        } else if($wotistatus==self::WINTAG_STATUS_RESCHEDULED) {
            $siteStatusID = self::SITE_STATUS_ID_RESCHEDULED;
        } else if($wotistatus==self::WINTAG_STATUS_CANCELLED) {
            $siteStatusID = self::SITE_STATUS_ID_CANCELED;
        }       
        
        //--- get Sitenumber, Project_ID
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from(Core_Database::TABLE_WORK_ORDERS,
            array('Project_ID','SiteNumber')
        );
        $select->where("WIN_NUM = '$winNum'");
        $records = Core_Database::fetchAll($select);  
        
        if(empty($records) || count($records)==0) return;
        
        $wo = $records[0];
        $projectID = $wo['Project_ID'];
        $siteNumber = $wo['SiteNumber'];
        
        if(empty($projectID) || empty($siteNumber)) return;

        //--- update site status
        $projectClass = new Core_Api_ProjectClass();
        if(!empty($siteStatusID)){
            $projectClass->saveSiteStatus_BySiteOfProject($siteNumber,$projectID,$siteStatusID);            
        }        
    }

    //13843
    public function updateSiteStatus_ByWin($winNum)
    {
        //--- get Sitenumber, Project_ID
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from(Core_Database::TABLE_WORK_ORDERS,
            array('Project_ID','SiteNumber')
        );
        $select->where("WIN_NUM = '$winNum'");
        $records = Core_Database::fetchAll($select);  
        
        if(empty($records) || count($records)==0) return;
        
        $wo = $records[0];
        $projectID = $wo['Project_ID'];
        $siteNumber = $wo['SiteNumber'];
        
        //--- get last wintagitem of site
        $wosClass = new Core_Api_WosClass();
        $winNumArray = $wosClass->getWinNumArray_InSiteOfProject($siteNumber,$projectID);         
        if(empty($winNumArray) || count($winNumArray)==0) return null;

        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from(array('wtiv'=>'work_order_tagitem_values'),
                array('wo_tag_item_value_id'=>'id','WIN_NUM','wo_tag_item_id','value_id','DateAdded','notes')                
        );
        $select->join(array('wti'=>'wo_tag_items'),
                'wti.id = wtiv.wo_tag_item_id',
                array('wo_tag_item_name','item_sortdesc','item_owner','item_details')
        );
        $select->where("wti.wo_tag_code = 'CompletionStatus'");
        $select->where("wtiv.WIN_NUM IN (?)",$winNumArray);
        $select->order("DateAdded DESC");
        $records = Core_Database::fetchAll($select);     
        if(empty($records) || count($records)==0) return;
        
        $wtiv=$records[0];
        $wotistatus = $wtiv['item_sortdesc'];
        //13834
        $siteStatusID = 0;  
        foreach($records as $rec) {
            $wotistatus = $rec['item_sortdesc'];
            $siteStatusID = $this->getSiteStatusID_ByWintagStatusID($wotistatus);
            if(!empty($siteStatusID)){
                break;
            }else {
                continue;
            }
        }
        //--- update site status
        $projectClass = new Core_Api_ProjectClass();
        if(!empty($siteStatusID)){
            $projectClass->saveSiteStatus_BySiteOfProject($siteNumber,$projectID,$siteStatusID);            
        }        
        //echo("WOTagClass->updateSiteStatus: ");print_r($wtiv);die();//test
    }
    //13834
    private function getSiteStatusID_ByWintagStatusID($wintagStatusID)
    {
        $siteStatusID = 0;        
        if($wintagStatusID==self::WINTAG_STATUS_COMPLETE) {
            $siteStatusID = self::SITE_STATUS_ID_COMPLETE;
        } else if($wintagStatusID==self::WINTAG_STATUS_RESCHEDULED) {
            $siteStatusID = self::SITE_STATUS_ID_RESCHEDULED;
        } else if($wintagStatusID==self::WINTAG_STATUS_CANCELLED) {
            $siteStatusID = self::SITE_STATUS_ID_CANCELED;
        } else if($wintagStatusID==self::WINTAG_STATUS_INCOMPLETE){
            $siteStatusID = 0;
        }
        return $siteStatusID;
    }
    
    //13898
    public function exists_WinTagForWOWithTagItemCode($winNum,$WoTagItemCode)
    {
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from(array('wtiv'=>'work_order_tagitem_values'),
                array('wo_tag_item_value_id'=>'id','WIN_NUM','wo_tag_item_id','value_id','DateAdded','notes')                
        );
        $select->join(array('wti'=>'wo_tag_items'),
                'wti.id = wtiv.wo_tag_item_id',
                array('wo_tag_item_code')
        );
        $select->where("wti.wo_tag_item_code = '$WoTagItemCode'");
        $select->where("wtiv.WIN_NUM = '$winNum'");
        $records = Core_Database::fetchAll($select);     
        if(empty($records) || count($records)==0){
            return false;
        } else {
            return true;
        }
    }    
    
    //13898
    public function getWinTagItemValueInfo_ByID($woTagItemValueID)
    {
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from(array('wtiv'=>'work_order_tagitem_values'),
                array('wo_tag_item_value_id'=>'id','WIN_NUM','wo_tag_item_id','value_id','DateAdded','notes')                
        );
        $select->join(array('wti'=>'wo_tag_items'),
                'wti.id = wtiv.wo_tag_item_id',
                array('wo_tag_item_code','wo_tag_item_name','item_sortdesc','item_owner','item_details')
        );
        $select->where("wtiv.id = '$woTagItemValueID'");
        $records = Core_Database::fetchAll($select);     
        if(empty($records) || count($records)==0){
            return null;
        } else {
            return $records[0];
        }
    }    

    //13898
    public function getWinTagItemValueInfo_ByWinAndWintagitemcode($winNum,$winTagItemCode)
    {
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from(array('wtiv'=>'work_order_tagitem_values'),
                array('wo_tag_item_value_id'=>'id','WIN_NUM','wo_tag_item_id','value_id','DateAdded','notes')                
        );
        $select->join(array('wti'=>'wo_tag_items'),
                'wti.id = wtiv.wo_tag_item_id',
                array('wo_tag_item_code','wo_tag_item_name','item_sortdesc','item_owner','item_details')
        );
        $select->where("wtiv.WIN_NUM = '$winNum'");
        $select->where("wti.wo_tag_item_code = '$winTagItemCode'");
        $records = Core_Database::fetchAll($select);     
        if(empty($records) || count($records)==0){
            return null;
        } else {
            return $records[0];
        }
    }    
    
    //13913
    public function getWinTagStatusSummaryForWin($winNum)
    {
		$result = $this->getWinTagStatusSummaryByRecords (
			$this->getCompletionStatusForWin($winNum)
		);

		return $result["desc"];
    }

    public function getWinTagStatusSummaryByRecords ($winTags)
    {   
        if(empty($winTags) || count($winTags)==0) {
            return array (
				code => self::WINTAG_STATUS_OTHER,
				desc => "WIN-Tags Performance Report"
			);
        }
        
        //--- complete?
        foreach($winTags as $winTag)
        {
            if($winTag['item_sortdesc']==self::WINTAG_STATUS_COMPLETE) {
				return array (
					code => self::WINTAG_STATUS_COMPLETE,
					desc => 'Complete - '.$winTag['item_details']
				);
            }
        }
        //--- cancelled?
        foreach($winTags as $winTag)
        {
            if($winTag['item_sortdesc']==self::WINTAG_STATUS_CANCELLED) {
				return array (
					code => self::WINTAG_STATUS_CANCELLED,
					desc => 'Site Cancelled'
				);
            }
        }
        //--- rescheduled?, 13890
        foreach($winTags as $winTag)
        {
            if($winTag['item_sortdesc']==self::WINTAG_STATUS_RESCHEDULED) {
				return array (
					code => self::WINTAG_STATUS_RESCHEDULED,
					desc => 'Rescheduled due to '.$winTag['item_owner'].": ".$winTag['item_details']
				);
            }
        }
        
        //--- incomplete?
        foreach($winTags as $winTag)
        {
            if($winTag['item_sortdesc']==self::WINTAG_STATUS_INCOMPLETE) {
				return array (
					code => self::WINTAG_STATUS_INCOMPLETE,
					desc => 'Incomplete due to '.$winTag['item_owner'].": ".$winTag['item_details']
				);
            }
        }
        //--- other        
        return "WIN-Tags Performance Report";
    }    
    //13760
    public function getTechList_HasPayISO()
    {       
        $db = Core_Database::getInstance();  
        $select = $db->select();
             
        $select->from(Core_Database::TABLE_TECH_BANK_INFO,
                array('TechID')
        );       
        $select->where("ISO_Affiliation_ID >0 ");                   
        $select->order("TechID");
        $records = Core_Database::fetchAll($select);   
        $ret = array();
        if(empty($records))
        {
            return null;
        }
        else
        {
            foreach($records as $rec)
            {
                if($rec['TechID'] > 0)
                {
                    $ret[]=$rec['TechID'];
                }
            }
        }
        return $ret;            
    }
}  
