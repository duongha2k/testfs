<?php
ini_set('memory_limit', '220M');

defined("WO_STATE_CREATED") || define ("WO_STATE_CREATED", "created");
defined ("WO_STATE_PUBLISHED") || define ("WO_STATE_PUBLISHED", "published");

defined ("WO_STATE_ASSIGNED") || define ("WO_STATE_ASSIGNED", "assigned");
defined ("WO_STATE_WORK_DONE") || define ("WO_STATE_WORK_DONE", "work done");
defined ("WO_STATE_INCOMPLETE") || define ("WO_STATE_INCOMPLETE", "incomplete");
defined ("WO_STATE_APPROVED") || define ("WO_STATE_APPROVED", "approved");
defined ("WO_STATE_IN_ACCOUNTING") || define ("WO_STATE_IN_ACCOUNTING", "in accounting");
defined ("WO_STATE_COMPLETED") || define ("WO_STATE_COMPLETED", "completed");
defined ("WO_STATE_DEACTIVATED") || define ("WO_STATE_DEACTIVATED", "deactivated");
defined ("WO_STATE_ALL") || define ("WO_STATE_ALL", "");

defined ("WO_STATE_ALL_ACTIVE") || define ("WO_STATE_ALL_ACTIVE", "all active");
defined ("WO_STATE_COMPLETED_TAB") || define ("WO_STATE_COMPLETED_TAB", "completed tab");
defined ("WO_STATE_TECH_PAID_TAB") || define ("WO_STATE_TECH_PAID_TAB", "tech paid tab");

defined("WO_UPLOAD_DIR") || define ("WO_UPLOAD_DIR",1630);
defined("WO_UPLOAD_DIR_NAME") || define ('WO_UPLOAD_DIR_NAME','/Work Order Pics');

class Core_Api_CustomerClass
{
	public function __construct() {
		Core_Caspio::init();
	}

    /**
     * uploadFileAWS
     *
     * @param string $user
     * @param string $pass
     * @param string $winNum
     * @param string $fName
     * @param string $fData
     * @param int $type
     * @param string $descr
     * @access public
     * @return mixed
     */
    public function uploadFileAWS( $user, $pass, $winNum, $fName, $fData, $type, $descr)
    {
        require_once 'Core/Api/Logger.php';
        require_once 'Core/Api/Error.php';
        require_once 'API/Response.php';

        $fName = ltrim(trim($fName), '/');

        /* Log request */
       	$logger = Core_Api_Logger::getInstance();
		$logger->setUrl('Upload file from Core_Api_Class ' . $winNum);
        $logger->setLogin($user);
        $logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');

        $error = Core_Api_Error::getInstance();

/*        $wo = $this->getWorkOrder($user, $pass, $winNum);
        if ( $error->getErrors() ) {
            $error->addError(6, 'There is no such WO');
            return API_Response::fail();
        }*/
		
        $files = new Core_File();
        $uploadRes = $files->uploadFile($fName, $fData, S3_CLIENTS_DOCS_DIR);

        if(!$uploadRes){
            $error->addError(6, 'Upload file '.$fName.' failed');
            $errorsDetails = $error->getErrors();
            error_log('FileUploading: (client:WO:uploadFileAWS) ' . implode('|', $errorsDetails));
            return API_Response::fail();
        }

        if ( $error->getErrors() ) {
            return API_Response::fail();
        }

        $files = new Core_Files();
        $lastId = $files->insert(array(
            'WIN_NUM' => $winNum,
            'path'    => $fName,
            'type'    => $type,
            'description' => mysql_escape_string($descr)
        ));

        Core_TimeStamp::createTimeStamp($winNum, $user, "Work Order Updated: File {$fName} was uploaded.", '', '', '', "", "", "");
        
        if($lastId) $res = new API_Response (1, 0, $lastId);
        else $res = API_Response::fail();

        return $res;
    }
	
    public function uploadFile( $user, $pass, $winNum, $fName, $fData, $fMime, $saveToField = 'Pic1' ) {
        require_once 'Core/Api/Logger.php';
        require_once 'Core/Api/Error.php';
        require_once 'API/Response.php';
        require_once 'Core/Caspio.php';

        /* Log request */
       	$logger = Core_Api_Logger::getInstance();
		$logger->setUrl('Upload file from Core_Api_Class ' . $winNum);
        $logger->setLogin($user);
        $logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');
        /* Log request */

        $error = Core_Api_Error::getInstance();

        /* Check field */
        if ( !in_array($saveToField, API_WorkOrder::uploadFileFields()) ) {
            $error->addError(6, 'Incorrect field name "'.$saveToField.'" for file saving');
            return API_Response::fail();
        }
        /* Check field */

        /* Check auth and get WO with file field we save want */
        $wo = $this->getWorkOrder($user, $pass, $winNum, join(',', API_WorkOrder::uploadFileFields()).',TB_UNID');
        if ( $error->getErrors() ) {
            return API_Response::fail();
        }
        $wo = $wo->data[0];
        /* Check auth and get WO with file field we save want */

        /* save new file if we have no errors */
        /* Detect User's file with the same name */
        $fReplace = false;
        foreach ( API_WorkOrder::uploadFileFields() as $_field ) {
            if ( ltrim($wo->$_field, '/') === WO_UPLOAD_DIR_NAME.$fName ) {
                $fReplace    = true;
                $saveToField = $_field; //  Save new file to field where file with the same name
                break;
            }
        }

        /* Detect User's file with the same name */
        Core_Caspio::caspioUploadFile($fName, $fData, $fMime, WO_UPLOAD_DIR, $fReplace);

        if ( $error->getErrors() ) {
            return API_Response::fail();
        }

        /* save new file if we have no errors */

        /* Remove old file name and save new one */
        if ( !$fReplace && $wo->$saveToField ) {
            // TODO Remove file from Caspio
        }

        if ( !$fReplace ) {
            $woUp = new API_WorkOrder;
            $woUp->$saveToField = WO_UPLOAD_DIR_NAME.'/'.ltrim($fName, '/') ;
            return $this->updateWorkOrder($user, $pass, $winNum, $woUp);
        } else {
            return API_Response::success(array($wo));
        }
        /* Remove old file name and save new one */
    }

	/**
	 * pull basic tech info
	 *
	 * @param string $user
	 * @param string $pass
	 * @param string $fileName
	 * @return API_Response
	 */

    public function getFile( $user, $pass, $fileName)
    {
       	$logger = Core_Api_Logger::getInstance();
		$logger->setUrl('getWorkOrders');
        $logger->setLogin($user);
		$logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');

		$authData = array('login'=>$user, 'password'=>$pass);
		$errors = Core_Api_Error::getInstance();

		$user = new Core_Api_CustomerUser();
        $user->checkAuthentication($authData);

		if (!$user->isAuthenticate())
			return API_Response::fail();
        $companyID = $user->getContextCompanyId();

        $file = Core_Caspio::caspioDownloadFile( $fileName );

		$err = $errors->getErrors();
		if (!empty($err))
			return API_Response::fail();

        return $file;
    }

    private function blankIfInvalidDate($date) {
        if (empty($date)) return '';
        if (strtotime($date) === FALSE) return null;
        return $date;
    }


	/**
	 * create Work Order
	 *
	 * @param string $user
	 * @param string $pass
	 * @param boolean $publish
	 * @param API_WorkOrder $workOrder
	 * @param boolean $ignoreWarnings
	 * @return API_Response
	 */
	public function createWorkOrder($user, $pass, $publish, $workOrder, $ignoreWarnings = false)
	{
	$benchmark  = Core_Benchmark::getInstance();

	$benchmark->point('Method: "createWorkOrder"; Start log method call');

        if (is_string($workOrder)) $woXML = $workOrder;
		else {
			$dom = new DOMDocument("1.0");
			$dom->appendChild($workOrder->toXMLElement($dom));
			$woXML = $dom->saveXML();
		}

		$ignoreWarnings = $ignoreWarnings === true;

       	$logger = Core_Api_Logger::getInstance();
		$logger->setUrl('createWorkOrder');
        $logger->setLogin($user);
		$logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');

		$authData = array('login'=>$user, 'password'=>$pass);
		$params = array('xml'=>$woXML);
		$errors = Core_Api_Error::getInstance();
		$errors->resetErrors();

		if (empty($params['xml']))
			$errors->addError(2, 'XML parameter is empty but required');

		$err = $errors->getErrors();
		if (!empty($err))
			return API_Response::fail();

	$benchmark->point('Auth');
		$user = new Core_Api_CustomerUser();
		$user->checkAuthentication($authData);
	$benchmark->point('Auth Done');

		if (!$user->isAuthenticate())
			return API_Response::fail();

                
        $woData = $this->getDataFromXML($params['xml']);
        
        $this->excludeFields($woData);

        $fieldListType = Core_Database::getFieldListType(Core_Database::TABLE_WORK_ORDERS);
        
        $woData = Core_Database::convertQueryResult($woData, $fieldListType);

        $woData['ShowTechs'] = ($publish === TRUE ? "True" : "False");
		$woData['Company_ID']   = $user->getContextCompanyId();
		$company = $user->getCompanyId();
		//$woData['Company_Name'] = $user->getContextCompanyName();

        $woData['Tech_ID'] = NULL;
        $woData['Date_Assigned'] = "";
        $woData["Tech_FName"] = "";
        $woData["Tech_FName"] = "";
        $woData["Tech_LName"] = "";
        $woData["TechEmail"] = "";
        $woData["TechPhone"] = "";
        $woData["Tech_Bid_Amount"] = NULL;
        $woData["FLS_ID"] = NULL;
                
        $coutry = $woData['Country'];
	$benchmark->point('Combine Project Info');
		$combiner = new Core_Combiner_WorkOrder($woData);
		$combiner->setCompanyId($woData['Company_ID']);
		//$combiner->setCompanyName($woData['Company_Name']);
		$combiner->setUserName($user->getLogin());
		if ($combiner->combine()) {
			$woData = $combiner->getData();
		} else {
		    $errors->addErrors('6', $combiner->getErrors());
			return API_Response::fail();
		}
        $woData['Country'] = $coutry;       
	$benchmark->point('Combiner Project Info Done');

		$woData["Username"] = $user->getLogin();

		if (array_key_exists("StartDate", $woData))
			$woData["StartDate"] = $this->blankIfInvalidDate($woData["StartDate"]);
		if (array_key_exists("EndDate", $woData))
			$woData["EndDate"] = $this->blankIfInvalidDate($woData["EndDate"]);

	$benchmark->point('Validation');
		$factory = new Core_Validation_Factory(Core_Validation_Factory::VALIDATION_TYPE_CREATE);
	   $requiredFields = $woData;    
        $Country = Core_Country::getCountryInfoByCode($requiredFields["Country"]);
        if(!empty($Country))
        {
            if($Country["ZipRequired"] != "1" && trim($requiredFields["Zipcode"]) == "")
            {
                $requiredFields["Zipcode"] = "12345";
            } 
        }
        else
        {
            $requiredFields["Zipcode"] = "12345";
        }    
        $errorPhone = false;
//        if(($requiredFields["Country"] == 'US' || $requiredFields["Country"] == 'CA') 
//                && trim($requiredFields["SitePhone"]) == "" && trim($requiredFields["Country"]) != "")
//        {
//            $errorPhone = true;
//        }
        $errorCountry = false;
//        if(trim($requiredFields["Country"]) == "")
//        {
//            $errorCountry = true;
//        }    
        $validation = $factory->getValidationObject($requiredFields);
        if ($validation->validate() === false) {
               
            $errors->addErrors('6', $validation->getErrors());
			
            if($errorPhone)
            {
                $errors->addError(7,"SitePhone empty but required");
            }  

            if($errorCountry)
            {
                $errors->addError(7,"Country empty but required");
                
            }    
            return API_Response::fail();
        }
        else
        {
            $er = false;
            if($errorPhone)
            {
                $errors->addError(7,"SitePhone empty but required");
                $er = true;
            } 
            if($errorCountry)
            {
                $errors->addError(7,"Country empty but required");
                $er = true;
            } 
            if($er)
                return API_Response::fail();
        }    

	$benchmark->point('Validation Done');

        date_default_timezone_set('US/Central');
        $woData['DateEntered'] = date('Y-m-d H:i:s');

        if (!empty($woData["StartDate"])) {
            // auto calc sourced by date and auto mark short notice
            $numWeekDays = $this->numberWeekDays(date("Y-m-d"), $woData["StartDate"]);
            if ($numWeekDays <= 5)
                $woData["ShortNotice"] = "True";

            if ($woData["Type_ID"] == "1") {
                $startDateTS = strtotime(date("Y-m-d"));
                $endDateTS = strtotime($woData["StartDate"]);
                $sourceByDateTS = $this->calculateSourceByDate($startDateTS, $endDateTS, $woData['DateEntered'], $numWeekDays);
                $woData["SourceByDate"] = date("Y-m-d", $sourceByDateTS);
            }
        }

    $benchmark->point('Geocode');
        if (empty($woData['Latitude']) || empty($woData['Longitude'])) {
			$geocode = new Core_Geocoding_Google();
			if (!empty($woData['Address']))
				$geocode->setAddress($woData['Address']);
			if (!empty($woData['City']))
				$geocode->setCity($woData['City']);
			if (!empty($woData['Zipcode']))
				$geocode->setZip($woData['Zipcode']);
			$woData['Latitude'] = $geocode->getLat();
			$woData['Longitude'] = $geocode->getLon();
	    }

    $benchmark->point('Geocode done');

		$woData['lastModDate'] = date('Y-m-d H:i:s');
		$woData['lastModBy'] = $user->getLogin();

        $fieldsList = implode(',',array_keys($woData));

    $benchmark->point('Inserting WO');
        $db = Core_Database::getInstance();

		// Update Status Field
		$woData["Status"] = Core_WorkOrder_Status::getStatusWithWorkOrder($woData);
		$woData["customer_ID"] = $user->getCustomerId();

		$result = false;
		$result = Core_Database::insert(Core_Database::TABLE_WORK_ORDERS, $woData);
		$wo_id = $db->lastInsertId(Core_Database::TABLE_WORK_ORDERS);

    $benchmark->point('Inserting WO done');
        if (empty($woData["Project_Name"]))
            $woData["Project_Name"] = "";

        if (!$result) {
    		$err = $errors->getErrors();
    		if (!empty($err))
				return API_Response::fail();

		    $errors->addError(7, 'Incorrect SQL request');
			return API_Response::fail();
		}

    $benchmark->point('Time stamps');
       	Core_TimeStamp::createTimeStamp($wo_id, $user->getLogin(), "Work Order Created", $woData['Company_ID'], $woData['Company_Name'], $woData['Project_Name'], "", "", "");
        if (!empty($woData['AbortFee']) && $woData['AbortFee']=="True") {
	        Core_TimeStamp::createTimeStamp($wo_id, $user->getLogin(), "Abort Fee Checked", $woData['Company_ID'], $woData['Company_Name'], $woData['Project_Name'], "", "", "");
        }

	$benchmark->point('Time stamps done');
		$devicesText = "";
		if (!empty($woData["Amount_Per"]) && $woData["Amount_Per"] == "Device") {
			$devicesText = "
			Devices: {$woData["Qty_Devices"]}";
		}

		if (defined('NO_CREATE_WORK_ORDER_EMAIL')) {
			$benchmark->point('Emails');

			if (!empty($woData["StartDate"]))
				$startDate = date("m/d/Y", strtotime($woData["StartDate"]));
			else
				$startDate = NULL;
			$newWOMail = new Core_Mail();
			$newWOMail->setFromName("Work Order Created");
			$newWOMail->setFromEmail("nobody@fieldsolutions.com");
			$newWOMail->setToName("NewWOsNote");
			$newWOMail->setToEmail("NewWOsNote@fieldsolutions.com");
			$newWOMail->setSubject("New $company WO - {$woData["Project_Name"]}");
			@$newWOMail->setBodyText("
				WorkOrderID: {$woData["WO_ID"]}
				Username: {$woData["Username"]}
				Max Pay: {$woData["PayMax"]}
				Amount Per: {$woData["Amount_Per"]} $devicesText
				Start Date: $startDate
				Start Time: {$woData["StartTime"]}
				Project Name: {$woData["Project_Name"]}
				Address: {$woData["Address"]}
				City: {$woData["City"]}
				State: {$woData["State"]}
				Zip: {$woData["Zipcode"]}

				Description:

				{$woData["Description"]}

				Special Instructions:

				{$woData["SpecialInstructions"]}");

			$newWOMail->send();
			$benchmark->point('Emails done');
		}

/*		if ($user->getUserType() != "" && $company == "FLS") {
		        $mail  = new Core_EmailForNewInstallDeskWorkOrder();
		        $mail->setAckFrom("gbaugh@ftxs.fujitsu.com");
		        $mail->setAckTo($user->getEmailPrimary());
		        $mail->setNotiFrom("support@fieldsolutions.com");
		        $mail->setNotiTo("jparoline@ftxs.fujitsu.com,gbaugh@ftxs.fujitsu.com");
		        $mail->setWorkOrder($wo_id);
		        $mail->sendMail();
		}*/

		return API_Response::success(array(new API_WorkOrder($wo_id)));
	}

	/**
	 * Function get list of WO Categories
	 *
	 * @param string $user
	 * @param string $pass
	 * @return API_Response
	 */
	public function getWOCategories($user, $pass)
	{
       	$logger = Core_Api_Logger::getInstance();
		$logger->setUrl('getWOCategories');
        $logger->setLogin($user);
		$logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');

		$authData = array('login'=>$user, 'password'=>$pass);
		$errors = Core_Api_Error::getInstance();

		$user = new Core_Api_CustomerUser();
		$user->checkAuthentication($authData);

		if (!$user->isAuthenticate())
			return API_Response::fail();

		$fieldsList = array('Category_ID','Category');

		$db = Core_Database::getInstance();
		$select = $db->select();

		$select->from(Core_Database::TABLE_WO_CATEGORIES, $fieldsList);

		$result = Core_Database::fetchAll($select);

		$err = $errors->getErrors();
		if (!empty($err))
			return API_Response::fail();

		return API_Response::success($this->makeWOCategoryArray($result));
	}
	/**
	 * Function fot getting Customers for current user
	 *
	 * @param string $user
	 * @param string $pass
	 * @param array $customer_ids
	 * @param string $sort
	 * @return API_Response
	 */
	 public function getCustomers($user, $pass, $customer_ids, $sort)
	 {
	 $logger = Core_Api_Logger::getInstance();
		$logger->setUrl('getCustomers');
        $logger->setLogin($user);
		$logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');

		$authData = array('login'=>$user, 'password'=>$pass);
		$errors = Core_Api_Error::getInstance();

		$user = new Core_Api_CustomerUser();
		$user->checkAuthentication($authData);

		if (!$user->isAuthenticate())
			return API_Response::fail();

		if (!is_array($customer_ids)) $customer_ids = array($customer_ids);

		$db = Core_Database::getInstance();
		$select = $db->select()->from(Core_Database::TABLE_CUSTOMERS);

		$customerId = $user->getCustomerId();
		$select->where('UNID=?', $customerId);

		$result = Core_Database::fetchAll($select);

		$err = $errors->getErrors();
		if (!empty($err))
			return API_Response::fail();
		return API_Response::success($this->makeCustomerArray($result));
	 }
	 
	/**
	 * Function for getting Projects for current usre
	 *
	 * @param string $user
	 * @param string $pass
	 * @param array $project_ids
	 * @param string $sort
	 * @return API_Response
	 */
	public function getProjects($user, $pass, $project_ids, $sort)
	{
       	$logger = Core_Api_Logger::getInstance();
		$logger->setUrl('getProjects');
        $logger->setLogin($user);
		$logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');

		$authData = array('login'=>$user, 'password'=>$pass);
		$errors = Core_Api_Error::getInstance();

		$user = new Core_Api_CustomerUser();
		$user->checkAuthentication($authData);

		if (!$user->isAuthenticate())
			return API_Response::fail();

		if (!is_array($project_ids)) $project_ids = array($project_ids);

		$db = Core_Database::getInstance();
		$select = $db->select()->from(Core_Database::TABLE_PROJECTS);

		$clientId  = $user->getCompanyId();
		$select->where('Project_Company_ID=?', $clientId);
		if (count($project_ids)) $select->where('Project_ID IN (?)', $project_ids);

		$result = Core_Database::fetchAll($select);
        
		$err = $errors->getErrors();
		if (!empty($err))
			return API_Response::fail();
		return API_Response::success($this->makeProjectsArray($result));
	}

    	/**
	 * Function for getting all Projects for current usre
	 *
	 * @param string $user
	 * @param string $pass
	 * @return API_Response
	 */
	public function getAllProjects($user, $pass)
	{
		ini_set('error_log', '/home/p3559/error.log');

       	$logger = Core_Api_Logger::getInstance();
		$logger->setUrl('getAllProjects');
        $logger->setLogin($user);
		$logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');

		$authData = array('login'=>$user, 'password'=>$pass);
		$errors = Core_Api_Error::getInstance();

		$user = new Core_Api_CustomerUser();
		$user->checkAuthentication($authData);

		if (!$user->isAuthenticate())
			return API_Response::fail();

		$fieldsList = array('Project_ID','Project_Name','Active');
		$companyId  = $user->getCompanyId();
		$customerId = $user->getCustomerId();
		
		$db = Core_Database::getInstance();
		$select = $db->select();

		$select->from(Core_Database::TABLE_PROJECTS, $fieldsList)
			->where("Project_Company_ID = ?", $companyId)
            ->order("Project_Name");

		$result = Core_Database::fetchAll($select);

		$err = $errors->getErrors();
		if (!empty($err))
			return API_Response::fail();
            
		return API_Response::success($this->makeProjectsArray($result));
	}

    /**
     * getCallTypes
     *
     * @access public
     * @return API_Response
     */
	public function getCallTypes()
	{
        return API_Response::success(array(
            1 => 'Install',
            2 => 'Service Call',
            3 => 'Recruiting'
        ));
	}
    /**
     * getStates
     *
     * @access public
     * @return API_Response
     */
    public function getStates()
    {
        return API_Response::success(array(
            'AL' => 'Alabama',
            'AK' => 'Alaska',
            //'AB' => 'Alberta',
            'AZ' => 'Arizona',
            'AR' => 'Arkansas',
            //'BC' => 'British Columbia',
            'CA' => 'California',
            'CN' => 'Canada',
            'CO' => 'Colorado',
            'CT' => 'Connecticut',
            'DE' => 'Delaware',
            'DC' => 'District of Columbia',
            'FL' => 'Florida',
            'GA' => 'Georgia',
            'GM' => 'Guam',
            'HI' => 'Hawaii',
            'ID' => 'Idaho',
            'IL' => 'Illinois',
            'IN' => 'Indiana',
            'IA' => 'Iowa',
            'KS' => 'Kansas',
            'KY' => 'Kentucky',
            'LA' => 'Louisiana',
            'ME' => 'Maine',
            //'MB' => 'Manitoba',
            'MD' => 'Maryland',
            'MA' => 'Massachusetts',
            'MI' => 'Michigan',
            'MN' => 'Minnesota',
            'MS' => 'Mississippi',
            'MO' => 'Missouri',
            'MT' => 'Montana',
            'NE' => 'Nebraska',
            'NV' => 'Nevada',
            //'NB' => 'New Brunswick',
            'NH' => 'New Hampshire',
            'NJ' => 'New Jersey',
            'NM' => 'New Mexico',
            'NY' => 'New York',
            //'NL' => 'Newfoundland and Labrador',
            'NC' => 'North Carolina',
            'ND' => 'North Dakota',
            //'NT' => 'Northwest Territories',
            //'NS' => 'Nova Scotia',
            //'NU' => 'Nunavut',
            'OH' => 'Ohio',
            'OK' => 'Oklahoma',
            //'ON' => 'Ontario',
            'OR' => 'Oregon',
            'XX' => 'Other',
            'PA' => 'Pennsylvania',
            //'PE' => 'Prince Edward Island',
            'PR' => 'Puerto Rico',
            //'QC' => 'Quebec',
            'RI' => 'Rhode Island',
            //'SK' => 'Saskatchewan',
            'SC' => 'South Carolina',
            'SD' => 'South Dakota',
            'TN' => 'Tennessee',
            'TX' => 'Texas',
            'UT' => 'Utah',
            'VT' => 'Vermont',
            'VI' => 'Virgin Islands',
            'VA' => 'Virginia',
            'WA' => 'Washington',
            'WV' => 'West Virginia',
            'WI' => 'Wisconsin',
            'WY' => 'Wyoming'
        ));
    }

    /**
	 * Function get list of Customers
	 *
	 * @param int $company_id
	 * @return API_Response
	 */
	public function getCustomersByCompanyId($company_id)
	{
        $errors = Core_Api_Error::getInstance();
        
       	$logger = Core_Api_Logger::getInstance();
		$logger->setUrl('get Customers');
        $logger->setLogin($user);
		$logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');

		$fieldsList = array('UNID','Customer_Name');

		$db = Core_Database::getInstance();
		$select = $db->select();

		$select->from(Core_Database::TABLE_CUSTOMERS, $fieldsList)
               ->where('Company_Id = ?', $company_id);
		$result = Core_Database::fetchAll($select);

		$err = $errors->getErrors();
		if (!empty($err))
			return API_Response::fail();

		return API_Response::success($this->makeCustomerArray($result));
	}

    public function getDistances()
    {
        return API_Response::success(array(
            "10" => "10 Miles",
            "25" => "25 Miles",
        	"50" => "50 Miles",
        	"75" => "75 Miles",
        	"100" => "100 Miles",
        	"150" => "150 Miles",
        	"225" => "225 Miles",
        	"300" => "300 Miles",
        	"500" => "500 Miles",
        	"1000" => "1,000 Miles",
        	"2500" => "2,500 Miles",
        	"5000" => "5,000 Miles",
        	"10000" => "10,000 Miles"
        ));
    }

	private function makeWOCategoryArray($result)
	{
		$wo = array();
	    if (!empty($result[0])) {
			foreach ($result as $key=>$value) {
				$wo[] = API_WOCategory::woCategoryWithResult($value);
			}
	    }
		return $wo;
	}

    private function makeCustomerArray($result)
	{
		$cst = array();
	    if (!empty($result[0])) {
			foreach ($result as $key=>$value) {
				$cst[] = API_Customer::customerWithResult($value);
			}
	    }
		return $cst;
	}

	private function makeProjectsArray($result)
	{
		$wo = array();
	    if (!empty($result[0])) {
			foreach ($result as $key=>$value) {
				$wo[] = API_Project::projectWithResult($value);
			}
	    }
		return $wo;
	}

	private function makeProjectsXML($result)
	{
		$dom = new DOMDocument("1.0");
		$root = $dom->createElement('Projects');
	    if (!empty($result[0])) {
		    foreach ($result as $key=>$value) {
                $pj = $dom->createElement('Project');

                $id = $dom->createAttribute('ID');
                $name = $dom->createAttribute('NAME');

                $id->appendChild($dom->createTextNode($value['Project_ID']));
                $name->appendChild($dom->createTextNode($value[' Project_Name']));

                $pj->appendChild($id);
                $pj->appendChild($name);

                $root->appendChild($pj);
            }
	    } else {

	    }
	    $dom->appendChild($root);
		return $dom->saveXML();
	}

	private function getTableDesign($tableName)
	{
		$columns = Core_Caspio::caspioGetTableDesign($tableName);
        $fieldList = array();

        foreach ($columns as $info) {
	        $fieldInfo = explode(",",$info);
	        $fieldList[] = trim($fieldInfo[0]);
        }

        $fieldList = implode(',', $fieldList);

        return $fieldList;
	}

	private function getWorkOrderrequiredFields()
	{
		$requiredFields = array('WO_ID','Headline');
		return $requiredFields;
	}

	private function excludeFields(&$fieldsList)
	{
		$ro = API_WorkOrder::readOnlyFields();
	    foreach ($fieldsList as $key=>$field) {
	    	if ( in_array( strtoupper($key), $ro) ) {
	    		unset($fieldsList[$key]);
	    	}
	    }
	}

	private function resetFieldsForCopyWO(&$data)
	{
	    date_default_timezone_set('US/Central');
        $data['DateEntered'] = date('m/d/Y h:i:s A');
		$data['StartDate'] = NULL; // Reset StartDate
		$data['EndDate'] = NULL; // Reset EndDate
		$data['Tech_FName'] = NULL; // Reset Tech_FName
		$data['Approved'] = '0'; // Reset Approved
		$data['ShowTechs'] = '0'; // Reset ShowTechs
		$data['Tech_ID'] = NULL; // Reset Tech_ID
		$data['FLS_ID'] = NULL; // Reset FLS_ID
		$data['TechMarkedComplete'] = '0'; // Reset TechMarkedComplete
		$data['TechComments'] = ''; // Reset TechComments
		$data['MissingComments'] = ''; // Reset MissingComments
		$data['Tech_LName'] = ''; // Reset Tech_LName
		$data['TechPaid'] = '0'; // Reset TechPaid
		$data['DatePaid'] = NULL; // Reset DatePaid
		$data['PayAmount'] = ''; // Reset PayAmount
		$data['CheckedIn'] = '0'; // Reset CheckedIn
		$data['Checkedby'] = ''; // Reset Checkedby
		$data['CheckInNotes'] = ''; // Reset CheckInNotes
		$data['StartTime'] = ''; // Reset StartTime
		$data['StoreNotified'] = '0'; // Reset StoreNotified
		$data['Tech_Bid_Amount'] = NULL; // Reset Tech_Bid_Amount
		$data['Qty_Applicants'] = '0'; // Reset Qty_Applicants
		$data['Invoiced'] = '0'; // Reset Invoiced
		$data['DateInvoiced'] = ''; // Reset DateInvoiced
		$data['DateApproved'] = NULL; // Reset DateApproved
		$data['PayApprove'] = ''; // Reset PayApprove
		$data['WorkOrderReviewed'] = ''; // Reset WorkOrderReviewed
		$data['Deactivated'] = '0'; // Reset Deactivated70
		$data['TechCheckedIn_24hrs'] = '0'; // Reset TechCheckedIn_24hrs
		$data['TechCalled'] = ''; // Reset TechCalled72
		$data['CalledBy'] = ''; // Reset CalledBy73
		$data['CallDate'] = ''; // Reset CallDate74
		$data['NoShow_Tech'] = NULL; // Reset NoShow_Tech
		$data['Time_In'] = ''; // Reset Time_In
		$data['Time_Out'] = ''; // Reset Time_Out
		$data['TechEmail'] = ''; // Reset TechEmail
		$data['TechPhone'] = ''; // Reset TechPhone
		$data['Date_Assigned'] = NULL; // Reset Date_Assigned
		$data['Update_Requested'] = '0'; // Reset Update_Requested
		$data['Update_Reason'] = ''; // Reset Update_Reason
		$data['Additional_Pay_Amount'] = ''; // Reset Additional_Pay_Amount
		$data['Add_Pay_AuthBy'] = ''; // Reset Add_Pay_AuthBy
		$data['Work_Out_of_Scope'] = '0'; // Reset Work_Out_of_Scope
		$data['OutOfScope_Reason'] = ''; // Reset OutOfScope_Reason
		$data['Site_Complete'] = '0'; // Reset Site_Complete
		$data['Add_Pay_Reason'] = ''; // Reset Add_Pay_Reason
		$data['BackOut_Tech'] = NULL; // Reset BackOut_Tech
		$data['EndTime'] = ''; // Reset EndTime
		$data['lastModBy'] = ''; // Reset lastModBy
		$data['lastModDate'] = ''; // Reset lastModDate
		$data['approvedBy'] = ''; // Reset approvedBy
		$data['Date_Completed'] = ''; // Reset Date_Completed
		$data['Deactivated_Reason'] = ''; // Reset Deactivated_Reason
		$data['OutofScope_Amount'] = ''; // Reset OutofScope_Amount101
		$data['PayAmount_Final'] = ''; // Reset PayAmount_Final102
		$data['Site_Contact_Name'] = ''; // Reset Site_Contact_Name
		$data['Check_Number'] = ''; // Reset Check_Number
		$data['MovedToMysql'] = '0'; // Reset MovedToMysql106
		$data['StartRange'] = ''; // Reset StartRange108
		$data['Duration'] = ''; // Reset Duration
		$data['calculatedInvoice'] = ''; // Reset calculatedInvoice
		$data['calculatedTotalTechPay'] = ''; // Reset calculatedTotalTechPay
		$data['calculatedTechHrs'] = ''; // Reset calculatedTechHrs
		$data['baseTechPay'] = ''; // Reset baseTechPay
		$data['PricingCalculationText'] = ''; // Reset PricingCalculationText
		$data['TripCharge'] = NULL; // Reset TripCharge
		$data['MileageReimbursement'] = NULL; // Reset MileageReimbursement
		$data['MaterialsReimbursement'] = NULL; // Reset MaterialsReimbursement
		$data['TechnicianFee'] = ''; // Reset TechnicianFee
		$data['TechnicianFeeComments'] = ''; // Reset TechnicianFeeComments
		$data['ExpediteFee'] = '0'; // Reset ExpediteFee
		$data['AbortFee'] = '0'; // Reset AbortFee
		$data['AfterHoursFee'] = '0'; // Reset AfterHoursFee
		$data['OtherFSFee'] = ''; // Reset OtherFSFee
		$data['TechMiles'] = ''; // Reset TechMiles
		$data['PricingRuleApplied'] = ''; // Reset PricingRuleApplied
		$data['AbortFeeAmount'] = '0'; // Reset AbortFeeAmount
		$data['PricingRan'] = '0'; // Reset PricingRan
		$data['General1'] = ''; // Reset General1
		$data['General2'] = ''; // Reset General2
		$data['General3'] = ''; // Reset General3
		$data['General4'] = ''; // Reset General4
		$data['General5'] = ''; // Reset General5
		$data['General6'] = ''; // Reset General6
		$data['General7'] = ''; // Reset General7
		$data['General8'] = ''; // Reset General8
		$data['General9'] = ''; // Reset General9
		$data['General10'] = ''; // Reset General10
		return true;
	}

	private function getDataFromXML($xml)
	{
	    $result = array();
		$dom = new DOMDocument("1.0");
		$dom->loadXML($xml);
		$nodes = $dom->getElementsByTagName('WO')->item(0)->childNodes;

		foreach ($nodes as $node)
		{
			$result[$node->nodeName] = $node->nodeValue;
		}

		return $result;
	}

	private function getDataFromXMLGeneric($xml)
	{
	    $result = array();
		$dom = new DOMDocument("1.0");
		$dom->loadXML($xml);

		$result = $this->nodeToArray($dom->documentElement);

		return $result;
	}

	private function nodeToArray($node) {
	    $childs = array();
	    $text = '';
		if ($node->hasChildNodes()) {
			$result = array();
			$children = $node->childNodes;
			foreach ($children as $child) {
			    if ($child->nodeName == "#text") {
                    $text .= $child->nodeValue;
                } else {
			        $childs[$child->nodeName] = $this->nodeToArray($child);
                }
			}
		}
		return empty($childs) ? $text : $childs;
	}

	private function numberWeekDays($startDate, $endDate) {
			$startDateTS = strtotime($startDate);
			$endDateTS = strtotime($endDate);

			$startDayOfWeek = date("w", $startDateTS);
			$endDayOfWeek = date("w", $endDateTS);

			$daysToNextSunday = 7 - $startDayOfWeek;
			$daysToPreviousSunday = $endDayOfWeek;
			$nextSundayTS = strtotime("+$daysToNextSunday days", $startDateTS);
			$previousSundayTS = strtotime("-$daysToPreviousSunday days", $endDateTS);

			$numWeekDays = 0;

			if ($nextSundayTS < $endDateTS) {
				// date range is more than one week
				$startWeekNumWeekDays = 5 - ($startDayOfWeek - 1 <= -1 ? 0 : $startDayOfWeek - 1);
				$endWeekNumWeekDays = ($endDayOfWeek == 6 ? 5 : $endDayOfWeek);

				$numDaysBetweenFirstAndLastWeek = ($previousSundayTS - $nextSundayTS) / 60 / 60 / 24 / 7 * 5;

				$numWeekDays = $startWeekNumWeekDays + $endWeekNumWeekDays + $numDaysBetweenFirstAndLastWeek;
			}
			else {
				$startWeekNumWeekDays = 5 - ($startDayOfWeek - 1 <= -1 ? 0 : $startDayOfWeek - 1);
				$endWeekNumWeekDaysNotIncluded = 5 - ($endDayOfWeek == 6 ? 5 : $endDayOfWeek);
				$numWeekDays = $startWeekNumWeekDays - $endWeekNumWeekDaysNotIncluded;
			}
			return $numWeekDays;
	}

	private function calculateSourceByDate($startDateTS, $endDateTS, $dateEntered, $numWeekDays) {
		$sourceByDateTS = $endDateTS; // use Project Start date by default

       //NEW CODE #12275 #12199
        if ($numWeekDays<16) {
            $sourceByDateTS = $startDateTS;
        }else{
            $sourceByDateTS = strtotime("-1 day",strtotime(date('m/d/Y',$endDateTS)));
        }
        return $sourceByDateTS;
	}
    
	public function createCustomer($login, $hash, $data){
        $errors = Core_Api_Error::getInstance();

        $authData = array('login'=>$login, 'password'=>$hash);
        $user->checkAuthentication($authData);
        if (!$user->isAuthenticate()) return API_Response::fail();

        $logger = Core_Api_Logger::getInstance();
        $logger->setUrl('createCustomer');
        $logger->setLogin($login);
        $logger->addLog(LOGGER_ERROR_NO, null, 'db request');

        unset($data['PasswordConf']);
        
        $customer = new Core_Api_CustomerUser();
        foreach( $data as $f=>$v ) {
            $customer->$f = $v;
        }

		if (isset($user->contextId)){
			$customer->CompanyId = $user->contextId;
		}else{
		$customer->CustomerName = $user->getContextCompanyName($company);
		}
        $result = $customer->save();

        $err = $errors->getErrors();
  		if (!empty($err))
			return API_Response::fail();

        return API_Response::success(array($result));
    }
    
	public function updateCustomerProfile($login, $hash, $data){
        $errors = Core_Api_Error::getInstance();
	$errors->resetErrors();

        $authData = array('login'=>$login, 'password'=>$hash);
        $user = new Core_Api_CustomerUser();
        $user->checkAuthentication($authData);
        
        if (!$user->isAuthenticate()) return API_Response::fail();

        $logger = Core_Api_Logger::getInstance();
        $logger->setUrl('updateCustomer');
        $logger->setLogin($login);
        $logger->addLog(LOGGER_ERROR_NO, null, 'db request');

        $customerInfo = $user->load(
            array(
                'login' => $login,
                'password' => $hash
            )
        );
  
       $cid = $customerInfo->customerId;
       if (!empty($cid) ) {
            $user = new Core_Api_CustomerUser($customerInfo->customerId);
            $user->loadById($cid);

            $datatoupdate = array();

		$datatoupdate = array();
            foreach($data as $field=>$val){
                if($val != $user->$field) $datatoupdate[$field] = $val;
            }

            if(empty($datatoupdate)) return API_Response::success(array($user));

            /*$factory = new Core_Validation_Factory(Core_Validation_Factory::VALIDATION_TYPE_CLIENT_PROFILE_UPDATE);
    	    $validation = $factory->getValidationObject($datatoupdate);
    
            if ($validation->validate() === false) {
                $errors->addErrors('6', $validation->getErrors());
    			return API_Response::fail();
            }*/
    
            $user = new Core_Api_CustomerUser($cid);
            $user->loadById($cid);
            $result = $user->save($datatoupdate);

            $err = $errors->getErrors();
      		if (!empty($err))
    			return API_Response::fail();
    
            return API_Response::success(array($result));
        } else {
            $errors->addErrors('7', 'Incorrect user');
            return API_Response::fail();
        }
    }
    /********** FOR QUICK_TICKET_VIEW ******************/
    public function getQTVListForCompanyId($companyId) 
    {
        $db = Core_Database::getInstance();
        //--- get CompanyName
        $companyName = $companyId;
        $query = $db->select();
        $query->from(Core_Database::TABLE_CLIENTS,array("CompanyName"));
        $query->where("Company_ID = '$companyId'");
        //$query->order("Admin desc, ProjectManager desc");
        $query->order("Admin desc");
        $query->order("ProjectManager desc");
        $records = Core_Database::fetchAll($query);        
        
        if(!empty($records)) $companyName = $records[0]["CompanyName"];
        //---
        $db = Core_Database::getInstance();
        $query = $db->select();
        $query->from(array('qtv'=>'qtviews'),
                    array('id','Company_ID','CustomerName',
                    'active','ClientEmail',
                    'CompanyName'=>"(Case When 1=1 Then '$companyName' Else '' End)"));
        $query->where("qtv.Company_ID = '$companyId'");
        $records = Core_Database::fetchAll($query);        
        return $records;
    }
       
    
    public function getQTV($id)
    {
        $db = Core_Database::getInstance();
        $query = $db->select();
        $query->from('qtviews');
        $query->where("id = ?",$id);
        $records = Core_Database::fetchAll($query);        
        if(!empty($records)) return $records[0];
        else return null;
    }
    

    public function getQTV_ByCustomerID($customerID)
    {
        $db = Core_Database::getInstance();
        $query = $db->select();
        $query->from(array('c'=>'customers'),array('UNID'));
        $query->join(array('qu'=>'qtview_user'),
                    'qu.cus_user_id = c.UNID',
                    array()
        );
        $query->join(array('q'=>'qtviews'),
                    'q.id = qu.qtview_id',
                    array('*')
        );        
        $query->where("c.UNID='$customerID'");
        //print_r($query->__toString());
        $records = Core_Database::fetchAll($query);        
        if(!empty($records)) return $records[0];
        else return null;
    }


    public function saveQTV($id,$companyId,$customerName,$active,$email='')
    {
        if($id>0) //update
        {
            $criteria = "id = '$id'";
            $result = Core_Database::update("qtviews",
                            array("Company_ID"=>$companyId,
                                "CustomerName"=>$customerName,
                                "active"=>$active,
                                "ClientEmail"=>$email
                            ),
                            $criteria                                           
                    );
        }
        else //insert
        {
            $result = Core_Database::insert("qtviews",
                            array("Company_ID"=>$companyId,
                                "CustomerName"=>$customerName,
                                "active"=>$active,
                                "ClientEmail"=>$email
                            )
                    );
        }
    }
    

    public function updateQTV_1f($id,$field,$value)
    {
        $criteria = "id = '$id'";
        $result = Core_Database::update("qtviews",
                            array($field=>$value),
                            $criteria                                           
        );
        return $result;
    }
    
    
    public function getPrjListForCompanyIdWithQTVCheck($companyId,$qtvId)
    {
        //'Project_Company_ID','Project_ID','Project_Name','Headline','Project_Manager','Active','CustomerName','IsInQTV'        
/*select p.Project_Company_ID,p.Project_ID,p.Project_Name,p.Headline,
p.Project_Manager,p.Active,
qtv.CustomerName,
(Case WHEN qtvp.qtview_id=1 THEN 1 ELSE 0 END) AS 'IsInQTV'
from projects p
left join qtview_project qtvp on qtvp.project_id = p.Project_ID
left join qtviews qtv on qtv.id = qtvp.qtview_id
where p.Project_Company_ID='BW'*/
        $db = Core_Database::getInstance();
        $query = $db->select();
        $query->from(array('p'=>Core_Database::TABLE_PROJECTS),
                    array('Project_Company_ID','Project_ID','Project_Name','Headline',
'Project_Manager','Active'));
        $query->joinLeft(array('qtvp'=>'qtview_project'),
                    "qtvp.project_id = p.Project_ID AND qtvp.qtview_id=$qtvId",
                    array('IsInQTV'=>"(Case WHEN qtvp.qtview_id=$qtvId THEN 1 ELSE 0 END)"));
        $query->joinLeft(array('qtv'=>'qtviews'),
                    'qtv.id = qtvp.qtview_id',
                    array('CustomerName'));                                        
        $query->where("p.Project_Company_ID = '$companyId'");
        $query->order("p.Project_Name");
        
        $records = Core_Database::fetchAll($query);  
        return $records;
    }
    
    public function getPrjListForQTV($qtvId)
    {
        //'Project_Company_ID','Project_ID','Project_Name','Headline','Project_Manager','Active','CustomerName'
        
        $db = Core_Database::getInstance();
        $query = $db->select();
        $query->from(array('p'=>Core_Database::TABLE_PROJECTS),
                    array('Project_Company_ID','Project_ID','Project_Name','Headline',
'Project_Manager','Active'));
        $query->join(array('qtvp'=>'qtview_project'),
                    'qtvp.project_id = p.Project_ID',
                    array());
        $query->join(array('qtv'=>'qtviews'),
                    'qtv.id = qtvp.qtview_id',
                    array('CustomerName'));                                        
        $query->where("qtv.id = ?",$qtvId);
        $records = Core_Database::fetchAll($query);
        return $records;
    }
    
    public function savePrjSelectedForQTV($qtvId,$prjIds)
    {
        //--- delete
        $db = Core_Database::getInstance();
        $db->delete('qtview_project',"qtview_id='$qtvId'");
        //--- add
		if(!empty($prjIds))
		{
			$prjIdArr = explode(",", $prjIds);
			foreach($prjIdArr as $k=>$prjId)
			{
				$result = Core_Database::insert("qtview_project",
								array("qtview_id"=>$qtvId,
									"project_id"=>$prjId
								)
						);
			}
		}        
    }    
    public function getQTVId_ForProject($prjId)
    {
        $qtvId=0;
        $db = Core_Database::getInstance();
        $query = $db->select();
        $query->from("qtview_project");
        $query->where("project_id='$prjId'");   
        $query->order("id desc");
        $records = Core_Database::fetchAll($query);              
        if(!empty($records))
        {
            $qtvId=$records[0]['qtview_id'];
        }
        return $qtvId;
    }

    public function addProjectToQTVOnlyOne($qtvId,$prjId)
    {
        if(!empty($prjId))
        {            
            //--- delete
            $db = Core_Database::getInstance();
            $db->delete('qtview_project',"qtview_id='$qtvId' AND project_id='$prjId'");
            //--- add
            $result = Core_Database::insert("qtview_project",
                                array("qtview_id"=>$qtvId,
                                    "project_id"=>$prjId
                                )
                        );
        }        
    }    
    
    public function getUserListForQTV($qtvId)
    {
        $db = Core_Database::getInstance();
        $query = $db->select();
        $query->from(array('qtvu'=>'qtview_user'),array());
        $query->join(array('cus'=>'customers'),
                        'cus.UNID=qtvu.cus_user_id',
                        array('*'));
        $query->where("qtvu.qtview_id='$qtvId'");                
        $records = Core_Database::fetchAll($query);              
        return $records;        
                
    }    
    
    public function removeCusUser($cusUserId)
    {
	    $db = Core_Database::getInstance();		
        $db->delete(Core_Database::TABLE_CUSTOMERS,"UNID='$cusUserId'");     
    }
    
    public function addCusUserToQTV($cusUserName,$qtvId)
    {         
        //--- get userid
        $cusUserId = 0;
        $db = Core_Database::getInstance();        
        $query = $db->select();
        $query->from(Core_Database::TABLE_CUSTOMERS);           
        $query->where("Username='$cusUserName'");
        $records = Core_Database::fetchAll($query);
        if(!empty($records)) $cusUserId=$records[0]['UNID'];
        else return;
        //--- insert
        $result = Core_Database::insert("qtview_user",
                            array("qtview_id"=>$qtvId,
                                "cus_user_id"=>$cusUserId
                            )
                    );        
    }    
    
    
    public function getAllProjectsForCustomer($user, $pass)
    {

        $logger = Core_Api_Logger::getInstance();
        $logger->setUrl('getAllProjects');
        $logger->setLogin($user);
        $logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');

        $authData = array('login'=>$user, 'password'=>$pass);
        $errors = Core_Api_Error::getInstance();

        $user = new Core_Api_CustomerUser();
        $user->checkAuthentication($authData);

        if (!$user->isAuthenticate())
            return API_Response::fail();

        $cusId  = $user->getCustomerId();
//echo("<br/>cusId: ".$cusId);
        $db = Core_Database::getInstance();
        //---
        /*select qu.cus_user_id,qp.* from qtview_user qu
inner join qtview_project qp on qp.qtview_id = qu.qtview_id
where qu.cus_user_id='234';*/
        
        $select = $db->select();
        $select->from(array('qu'=>'qtview_user'),
                        array('cus_user_id'));
        $select->join(array('qp'=>'qtview_project'),
                      'qp.qtview_id = qu.qtview_id',
                      array('*')  
        );
        $select->where("qu.cus_user_id = '$cusId'");
        $projects = Core_Database::fetchAll($select);
        $projectIds = "0";
        if(!empty($projects))
        {
            foreach($projects as $k=>$v)
            {
                $projectIds.=','.$v['project_id'];
            }
        }
//echo("<br/>projectIds: ".$projectIds);        
        //---                
        $select = $db->select();

        $select->from(Core_Database::TABLE_PROJECTS)
            ->where("Project_ID in ($projectIds)")
            ->order("Project_Name");

        $result = Core_Database::fetchAll($select);

        $err = $errors->getErrors();
        if (!empty($err))
            return API_Response::fail();
        //return $projectIds;    
        return API_Response::success($this->makeProjectsArray($result));        
    }
    
    public function getAllProjectsForCustomerId($cusId)
    {
        $db = Core_Database::getInstance();
        
        $select = $db->select();
        $select->from(array('qu'=>'qtview_user'),
                        array('cus_user_id'));
        $select->join(array('qp'=>'qtview_project'),
                      'qp.qtview_id = qu.qtview_id',
                      array('*')  
        );
        $select->where("qu.cus_user_id = '$cusId'");
        $projects = Core_Database::fetchAll($select);
        $projectIds = "-10";
        if(!empty($projects))
        {
            foreach($projects as $k=>$v)
            {
                $projectIds.=','.$v['project_id'];
            }
        }
        return $projectIds;
    }
    
    public function getClientForCompanyId($companyId)
    {
        $db = Core_Database::getInstance();
        $select = $db->select();
        $select->from(Core_Database::TABLE_CLIENTS);
        $select->where("AccountEnabled = 1");
        $select->where("Company_ID = '$companyId'");
        $select->order("ProjectManager DESC");
        $clients = Core_Database::fetchAll($select);
        if(empty($clients)) return null;
        else return $clients[0];
    }
    
    //--- 330
    public function getCustomerQTInfo($customerID)
    {
        $db = Core_Database::getInstance();
        $select = $db->select();
        $select->from(array("qvu"=>"qtview_user"),array());
        $select->join(array("qv"=>"qtviews"),
                    "qv.id = qvu.qtview_id",
                    array("ClientEmail")
        );
        $select->join(array("cus"=>"customers"),
                    "cus.UNID=qvu.cus_user_id",
                    array('UNID','FS_Client_Name','Contact_Name','Customer_Name','Company_ID')
        );
        $select->join(array("cli"=>"clients"),
                    "cli.Company_ID = cus.Company_ID",
                    array("CompanyName")
        );
        $select->where("qvu.cus_user_id='$customerID'");
        $select->distinct();
                
        $records = Core_Database::fetchAll($select);
        
        if(empty($records)) return null;
        else return $records[0];
    }
    //--- 13449
    // return 1; 0
    public function IsShownClientPO($cusID)
    {
        $db = Core_Database::getInstance();
        $select = $db->select();
        $select->from(array('qtvu'=>'qtview_user'),array());
        $select->join(array('qtv'=>'qtviews'),
                'qtv.id=qtvu.qtview_id',
                array('id','ShowClientPONum')
        );
        $select->where("qtv.ShowClientPONum=1");
        $select->where("qtvu.cus_user_id = '$cusID'");
        $records = Core_Database::fetchAll($select);
        if(empty($records)) return 0;
        else return 1;
    }
}
