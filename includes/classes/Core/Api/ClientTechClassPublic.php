<?php
// Exposed methods for public usage.

class Core_Api_ClientTechClassPublic {

	private function getUser($user, $pass) {
		$authData = array('login'=>$user, 'password'=>$pass);
	        $user = new Core_Api_User();
	        $user->checkAuthentication($authData);
		return $user;
	}

	private function getError() {
		$errors = Core_Api_Error::getInstance();
		return $errors;
	}

	private function processResult($result, $flip = FALSE) {
		$ret = array();
		if (empty($result)) return $ret;

		foreach ($result as $k=>$v) {
			if (!is_array($v)) {
				if (!$flip) $ret[$k] = $v;
				else $ret[$v] = 1;
			}
			else $ret = array_merge($ret, $this->processResult($v, $flip || $k === 'equipmentInfo'));
		}
		return $ret;
	}

	 /**
         * find techs
         *
         * @param string $user
         * @param string $pass
         * @param array $filterKeys
         * @param array $filterValues
         * @return API_Response
         */
        public function findTechs($user, $pass, $filterKeys, $filterValues)
        {
		$user = $this->getUser($user, $pass);
		if (!$user->isAuthenticate()) return API_Response::fail();
		$errors = $this->getError();
		if (empty($filterKeys) || empty($filterValues) || count($filterKeys) != count($filterValues)) {
			$errors->addErrors('2', 'filterKeys and filterValues are required');
			return API_Response::fail();
		}
		$filters = array_combine($filterKeys, $filterValues);
		$companyID = $user->getContextCompanyId();
		$filters["v"] = $companyID;
		$matches = Core_Techt::findTechs($filters);
		$values = array();
		$keys = NULL;
		foreach ($matches as $m) {
			if (empty($keys)) {
				$k = array_keys($m);
				$keys = $k;
			}

			$values[] = array_values($m);
		}
		return API_Response::success(array($keys, array($values)));
        }

	 /**
         * pull tech info
         *
         * @param string $user
         * @param string $pass
         * @param string $techID
         * @return API_Response
         */
        public function getTechProfile($user, $pass, $techID)
        {
		$user = $this->getUser($user, $pass);
		if (!$user->isAuthenticate()) return API_Response::fail();
		$errors = $this->getError();
		$companyID = $user->getContextCompanyId();
		$p = Core_Techt::getProfile($techID, false, API_Tech::MODE_TECH, $companyID);
		$p = $this->processResult($p);

		$filter = array('NCR_Basic_Cert', 'FLSCSP_Rec', 'flsCSPStatus', 'flsCSPDate', 'FLSID', 'Cert_Hallmark_POS', 'Starbucks_Cert', 'Cert_Maurices_POS', 'FLSstatus', 'ncrBasicStatus', 'ncrBasicDate', 'flsIdStatus', 'flsBadgeDate', 'flsWhseStatus', 'flsWhseDate', 'flsCSPStatus', 'flsCSPDate', 'hallmarkCertStatus', 'hallmarkCertNum', 'starbucksCertStatus', 'mauricesCertStatus','hallmarkCertDate', 'starbucksCertDate', 'mauricesCertDate', 'FLSStatus', 'FLStitle', 'FLSBadge', 'FLS_Photo_ID');

		foreach ($p as $k=>$v) {
			if (!in_array($k, $filter)) continue;
			unset($p[$k]);
		}

		return API_Response::success(array(array_keys($p), array_values($p)));
	}

}


