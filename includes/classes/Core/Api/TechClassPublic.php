<?php
// Exposed methods for public usage. Use Core_Api_TechClass for calls made internally

class Core_Api_TechClassPublic {

	 /**
         * pull basic tech info
         *
         * @param string $user
         * @param string $pass
         * @return API_Response
         */
        public function getProfile($user, $pass)
        {
		$api = new Core_Api_TechClass;
		return $api->techLookup($user, $pass);
        }

	 /**
         * get tech profile pic
         *
         * @param string $user
         * @param string $pass
         * @param boolean $base64encode
         * @return API_Response
         */
        public function getProfilePic($user, $pass, $base64encode = true)
        {
			if ($base64encode === NULL) $base64encode = true;
		$api = new Core_Api_TechClass;
			$ret = $api->getProfilePic($user, $pass);
			if (!$ret->success) return $ret;
			if ($base64encode) $ret->data[0] = base64_encode($ret->data[0]);
			return $ret;
        }

	/**
	 * get parts for a work order
	 *
	 * @param string $user
	 * @param string $pass
	 * @param int $winNum
	 *
	 * @return API_Response
	 */
	public function getParts($user, $pass, $winNum) {
		$api = new Core_Api_TechClass;
		return $api->getParts($user, $pass, $winNum);
	}

    /**
     * get a work order by win number
     *
     * @param string $techLogin
     * @param string $techPass
     * @param int $winNum
     * @access public
     * @return API_Response
     */
    public function getWorkOrder( $techLogin, $techPass, $winNum )
    {
	$api = new Core_Api_TechClass;
	return $api->getWorkOrder( $techLogin, $techPass, $winNum);
    }

    /**
     * get matching work orders
     *
     * @param string $techLogin
     * @param string $techPass
     * @param API_WorkOrderFilter $filters
     * @param int $offset
     * @param int $limit
     * @param string $sort
     * @access public
     * @return API_Response
     */
    public function getWorkOrders( $techLogin, $techPass, $filters=NULL, $offset=0, $limit=0, $sort="" )
    {
	$api = new Core_Api_TechClass;
	$countRows = 0;
	return $api->getWorkOrders( $techLogin, $techPass, NULL, $filters, $offset, $limit, $countRows, $sort );
    }

    /**
     * update a work order
     *
     * @param string $user
     * @param string $pass
     * @param int $winNum
     * @param API_TechWorkOrder $workOrder
     * @access public
     * @return API_Response
     */
    public function updateWorkOrder( $user, $pass, $winNum, $workOrder )
    {
    if(!empty($workOrder->Date_In) || !empty($workOrder->Time_In)){
                $workOrder->CheckedIn = 1;
        }
	$api = new Core_Api_TechClass;
	return $api->updateWorkOrder( $user, $pass, $winNum, $workOrder );
    }

    /**
     * tech mark a work order complete and send all completion info
     *
     * @param string $user
     * @param string $pass
     * @param int $winNum
     * @param string $checkInDate
     * @param string $checkInTime
     * @param string $checkOutDate
     * @param string $checkOutTime
     * @param string $comments
     * @param string $mileage
     * @param string $helpdeskContacted
     * @param string $whoContacted
     * @access public
     * @return API_Response
     */
    public function markWorkOrderComplete(
        $user,
        $pass,
        $winNum,
        $checkInDate,
        $checkInTime,
        $checkOutDate,
        $checkOutTime,
        $comments,
        $mileage            = null,
        $helpdeskContacted  = null,
        $whoContacted       = null
    )
    {
	$api = new Core_Api_TechClass;
	return $api->markWorkOrderComplete(
        $user,
        $pass,
        $winNum,
        $checkInDate,
        $checkInTime,
        $checkOutDate,
        $checkOutTime,
        $comments,
        $mileage,
        $helpdeskContacted,
        $whoContacted,
        NULL,
        NULL,
        NULL);
    }

    /**
     * tech accept a work order
     *
     * @param string $user
     * @param string $pass
     * @param int $winNum
     * @access public
     * @return API_Response
     */
    public function acceptWorkOrder( $user, $pass, $winNum )
    {
	$api = new Core_Api_TechClass;
	return $api->acceptWorkOrder( $user, $pass, $winNum );
    }
    /**
     * tech confirm a work order
     *
     * @param string $user
     * @param string $pass
     * @param int $winNum
     * @access public
     * @return API_Response
     */
    public function confirmWorkOrder( $user, $pass, $winNum )
    {
	$api = new Core_Api_TechClass;
	return $api->confirmWorkOrder( $user, $pass, $winNum );
    }
    
    /**
     * upload a file to AWS
     *
     * @param string $user
     * @param string $pass
     * @param int $winNum
     * @param string $fName
     * @param string $fData
     * @param string $descr
     * @access public
     * @return API_Response
     */
    
    public function uploadFileAWS( $user, $pass, $winNum, $fName, $fData, $descr)
    {
    	if(!is_file($fName))$fName = $fName.".png";
	$api = new Core_Api_TechClass;
		$fData = base64_decode($fData);
	return $api->uploadFileAWS( $user, $pass, $winNum, $fName, $fData, $descr);
    }
    
    /**
     * getFile
     *
     * @param string $user
     * @param string $pass
     * @param string $fileName
     * @access public
     * @return API_Response
     */
/*    public function downloadFile( $user, $pass, $fileName)
    {
	$api = new Core_Api_TechClass;
	return $api->downloadFile( $user, $pass, $fileName);
    }*/
	/**
	 * get parts for a work order
	 *
	 * @param string $user
	 * @param string $pass
	 * @param int $winNum
	 *
	 * @return API_Response
	 */
	public function getTechParts($user, $pass, $winNum)
	{
		$api = new Core_Api_TechClass;
		return $api->getTechParts($user, $pass, $winNum);
	}


	/**
	 * get list of WO Categories
	 *
	 * @param string $user
	 * @param string $pass
	 * @return API_Response
	 */
	public function getWOCategory($user, $pass, $categoryId)
	{
		$api = new Core_Api_TechClass;
		return $api->getWOCategory($user, $pass, $categoryId);
	}


	/**
	 * get part entry
	 *
	 * @param string $user
	 * @param string $pass
	 * @param int $winNum
	 * @param string $partID
	 *
	 * @return API_Response
	 */
	public function getTechPartEntry($user, $pass, $winNum, $partID)
	{
		$api = new Core_Api_TechClass;
		return $api->getTechPartEntry($user, $pass, $winNum, $partID);
	}

	/**
	 * update part entry
	 *
	 * @param string $user
	 * @param string $pass
	 * @param int $winNum
	 * @param string $partID
	 * @param API_ReturnPart $part
	 *
	 * @return API_Response
	 */
	public function updateTechPartEntry($user, $pass, $winNum, $partID, $part) {
		$api = new Core_Api_TechClass;
		return $api->updateTechPartEntry($user, $pass, $winNum, $partID, $part);
	}

    /**
     * save / create bid for tech
     * @param string $user
     * @param string $pass
     * @param int $winNum
     * @param double $bidAmount
     * @param string $comments
     * @return API_Response
     * @author Artem Sukharev
     */
    public function createBid( $user, $pass, $winNum, $bidAmount, $comments = '' )
    {
	$api = new Core_Api_TechClass;
	return $api->createBid( $user, $pass, $winNum, $bidAmount, $comments );
    }
    /**
     * update bid for tech
     * @param string $user
     * @param string $pass
     * @param int $winNum
     * @param double $bidAmount
     * @param string $comments
     * @return API_Response
     * @author Alex Scherba
     */
    public function updateBid( $user, $pass, $winNum, $bidAmount, $comments = '' )
    {
	$api = new Core_Api_TechClass;
	return $api->updateBid( $user, $pass, $winNum, $bidAmount, $comments);
    }

    /**
     * get bid for tech
     * @param string $user
     * @param string $pass
     * @param int $winNum
     * @return API_Response
     * @author Alex Scherba
     */
    public function getBid( $user, $pass, $winNum )
    {
	$api = new Core_Api_TechClass;
	return $api->getBid( $user, $pass, $winNum );
    }

     /**
         * Function for getting projects
         *
         * @param string $user
         * @param string $pass
         * @param array $project_ids
         * @param string $sort
         * @return API_Response
         */

	public function getProjects($user, $pass, $project_ids, $sort) {
		$api = new Core_Api_TechClass;
		return $api->getProjects($user, $pass, $project_ids, $sort);
	}

     /**
	 * get list of documents for a work order
	 *
	 * @param string $user
	 * @param string $pass
	 * @param int $win
	 *
	 * @return API_Response
	 */
	public function getWODocList($user, $pass, $win)
	{
		$api = new Core_Api_TechClass;
		return $api->getWODocList($user, $pass, $win);
	}
	
	 /**
     * download file from amazon
     *
     * @param string $user
     * @param string $pass
     * @param string $fileName
     * @param string $type
     * @access public
     * @return API_Response
     */
    public function downloadFileAWS( $user, $pass, $fileName, $type) {
		$api = new Core_Api_TechClass;
		$ret = $api->downloadFileAWS($user, $pass, $fileName, $type);
		if (!$ret->success) return $ret;
		$ret->data[0] = base64_encode($ret->data[0]);
		return $ret;
	}	

     /**
     * call to record tech login and pass additional login history info
     *
     * @param string $user
     * @param string $pass
     * @param string $platform
     * @param string $mobileVersion
     * @access public
     * @return API_Response
     */
	public function login($user, $pass, $platform = NULL, $mobileVersion = NULL)
	{
		$class = new Core_Api_TechClass;
		return $class->login($user, $pass, $platform, $mobileVersion);
	}

     /**
	 * Dummy function to include API_TechWorkOrder in SOAP wsdl
	 *
	 * @return API_TechWorkOrder
	 */
    public function dummy()
    {
	return null;
    }
     /**
	 * Dummy function to include API_Tech in SOAP wsdl
	 *
	 * @return API_Tech
	 */
    public function dummy2()
    {
	return null;
    }
}


