<?php
//need for defines .
require_once 'Core/Api/Class.php';

class Core_Api_TechClass {

    const ERROR_WO_ASSIGNED_ANOTHER_TECH = 'This work order is no longer available, it has been assigned to another technician.';
    const ERROR_WO_DEACTIVATED = 'The work order is no longer available.';
    const ERROR_WO_NOT_PUBLISHED = 'This work order is no longer available.';
    const ERROR_WO_INVALID_WIN = 'This is not a valid WIN#.';
    const ERROR_WO_INVALID_WO = 'This is not a valid Client Work Order#.';
    const ERROR_WO_INCORRECT_ID = 'WIN# not found or is no longer available';
    const PRIMARY_PHONE = 1;
    const SECONDARY_PHONE = 2;
    const PHONE_VALID = 1;
    const PHONE_INVALID = 2;
    const I9_PENDING = 1;
    const I9_VERIFIED = 2;
    const I9_AUTHORIZED = 2;
    const I9_NON_CONFIRMATION = 3;
    
    const VOP_CERT_ID = 18;
    const NCRBT_CERT_ID = 19;
    const LMSPreferred_CERT_ID = 20; /*13329*/
    const LMSPlusKey_CERT_ID = 21;/*13329*/
    const FlextronicsContractor_CERT_ID = 22;/*13367*/
    const FlextronicsScreened_CERT_ID = 23;/*13367*/
    const ComputerPlus_CERT_ID = 24;/*13371*/
    const TechForceICAgreement=32;
    //13761
    const BlackBoxCablingHelper_CERT_ID=46;
    const BlackBoxCablingAdvanced_CERT_ID=47;
    const BlackBoxCablingLead_CERT_ID=48;
    const BlackBoxTelecomHelper_CERT_ID=49;
    const BlackBoxTelecomAdvanced_CERT_ID=50;
    const BlackBoxCablingLevel_Helper=1;
    const BlackBoxCablingLevel_Advanced=2;
    const BlackBoxCablingLevel_Lead=3;
    const BlackBoxTelecomLevel_Helper=1;
    const BlackBoxTelecomLevel_Advanced=2;
    const BBCertIDs = '46,47,48,49,50';
    const BBCablingCertIDs = '46,47,48';
    const BBTelecomCertIDs = '49,50';
    //end 13761
    //13970
    const Period_Last90Days = "Last90Days";
    const Period_Last12Months = "Last12Months";
    const Period_Lifetime = "Lifetime";
    const Interval_Last90Days = "90 Day";
    const Interval_Last12Months = "12 Month";
    const Interval_Lifetime = "";
    //end 13970
    
	private $gge_modified = false;

    //13702
    private $_subdomains=null;
    private $_subfolders=null;
    //end 13702
    
    /**
     * __construct
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
    	Core_Caspio::init();
        //13702
        $this->_subdomains = array();
        $this->_subdomains['LMS']='LMS';
        $this->_subdomains['ErgoMotion']='ErgoMotion';
        $this->_subdomains['DeVry']='DeVry';
        $this->_subdomains['Purple']='Purple';
        $this->_subdomains['Endeavor']='Endeavor';
        //894
        $this->_subdomains['pb']='pb';
        // because of pitneybowes (changed to pb) sundomain does not exists.
        // in subdomain test, include parameter v to recognize pitneybowes
        $this->_subdomains['test']='test'; 
        //end 894
        $this->_subfolders=array();
        $this->_subfolders['field-service-technician-jobs']='field service technician jobs';
        $this->_subfolders['find-IT-technician-jobs']='find IT technician jobs';
        $this->_subfolders['technology-service-jobs']='technology service jobs';
        $this->_subfolders['nationwide-field-service-jobs']='nationwide field service jobs';
        $this->_subfolders['independent-contractor-technicians']='independent contractor technicians';
        $this->_subfolders['tech-jobs-available']='tech jobs available';
        $this->_subfolders['technician-cabling-jobs']='technician cabling jobs';
        $this->_subfolders['technician-telephony-jobs']='technician telephony jobs';
        $this->_subfolders['technician-printer-jobs']='technician printer jobs';
        $this->_subfolders['tech-jobs-nationwide']='tech jobs nationwide';
        //end 13702
    }
    //14024
    public function isTechHideBids($techId)
    {         
            $db = Core_Database::getInstance();
            $fieldArray = array('TechID');
            $s = $db->select();         
            $s->from(Core_Database::TABLE_TECH_BANK_INFO,$fieldArray);
            $s->where('TechID = (?)',$techId);          
            $s->where('HideBids = 1');
            $records = Core_Database::fetchAll($s);  
            if(!empty($records)) return 1;
            else return 0;
    }
    /**
     * get count of WO by status
     * @param string $user
     * @param string $pass
     * @param string $counters
     * @return API_Response
     * @author Artem Sukharev
     */
    public function getWorkOrdersCountByStatus($user, $pass, &$counters)
    {
           $logger = Core_Api_Logger::getInstance();
        $logger->setUrl('getWorkOrders');
        $logger->setLogin($user);
        $logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');

        $authData = array('login'=>$user, 'password'=>$pass);
        $errors = Core_Api_Error::getInstance();

        $user = new Core_Api_TechUser();
        $user->checkAuthentication($authData);

        if ( !$user->isAuthenticate() ) return API_Response::fail();

        $techID = $user->getTechId();

        $counters = array('total' => 0);
        $count = 0;

         // assigned
        $filters = new API_WorkOrderFilter;
        $filters->Deactivated = false;
        $filters->WO_State = WO_STATE_ASSIGNED;
        $filters->Tech = $techID;
        $result = $this->getWorkOrders($user->getLogin(), $user->getPassword(), 'WIN_NUM', $filters,0,1, $count,'',"techassigned");
        $counters[WO_STATE_ASSIGNED] = isset($result->data) ? $count : 0;
       

        // available
        $filters = new API_WorkOrderFilter;
        $filters->WO_State = WO_STATE_PUBLISHED;
        $filters->Tech = null;
        $filters->Distance = "<= 50";
        $result = $this->getWorkOrders($user->getLogin(), $user->getPassword(), 'WIN_NUM', $filters,0,1, $count,'',"techavailable");
        $counters['available'] = isset($result->data) ? $count : 0;

        // applied
        /*
        $filters = new API_WorkOrderFilter;
        $filters->BidAmount = '>0';
        $filters->Tech = $techID;
        $result = $this->getWorkOrders($user->getLogin(), $user->getPassword(), 'WIN_NUM', $filters,0,1, $count);
        $counters['applied'] = isset($result->data) ? $count : 0;
        */
        $result = $this->getBidCount($user->getLogin(), $user->getPassword());
        $counters['applied'] = isset($result) ? $result : 0;
 
        //published
        $filters = new API_WorkOrderFilter();
        $filters->Deactivated = false;
        $filters->WO_State = WO_STATE_PUBLISHED;
        $filters->Distance = '<= 50';
        $filters->StartDateFrom = date('m/d/Y');
        $result = $this->getWorkOrders($user->getLogin(), $user->getPassword(), 'WIN_NUM', $filters,0,1, $count,'',"techavailable");
        $counters[WO_STATE_PUBLISHED] = isset($result->data) ? $count : 0;
        
        $filters = new API_WorkOrderFilter();
        $filters->Deactivated = false;
        $filters->WO_State = WO_STATE_PUBLISHED;
        $filters->Distance = '<= 50';
        $filters->StartDateFrom = date('01/01/1960');
        $result = $this->getWorkOrders($user->getLogin(), $user->getPassword(), 'WIN_NUM', $filters,0,1, $count,'',"techavailable");
        $counters[WO_STATE_PUBLISHED."_all"] = isset($result->data) ? $count : 0;
       
        //Incomplete
        $filters = new API_WorkOrderFilter();
        $filters->WO_State = WO_STATE_INCOMPLETE;
        $filters->Tech = $techID;
        $result = $this->getWorkOrders($user->getLogin(), $user->getPassword(), 'WIN_NUM', $filters,0,1, $count);
        $counters[WO_STATE_INCOMPLETE] = isset($result->data) ? $count : 0;
        
        
        //Complete, Not Yet Approved
        $filters = new API_WorkOrderFilter();
        $filters->WO_State = WO_STATE_WORK_DONE;
        $filters->Tech = $techID;
        $result = $this->getWorkOrders($user->getLogin(), $user->getPassword(), 'WIN_NUM', $filters,0,1, $count);
        $counters[WO_STATE_WORK_DONE] = isset($result->data) ? $count : 0;
        
        //Approved, Not Yet Paid
        $filters = new API_WorkOrderFilter();
        $filters->WO_State = WO_STATE_IN_ACCOUNTING;
        $filters->Tech = $techID;
        $result = $this->getWorkOrders($user->getLogin(), $user->getPassword(), 'WIN_NUM', $filters,0,1, $count);
        $counters[WO_STATE_IN_ACCOUNTING] = isset($result->data) ? $count : 0;
        
        //All Active
        $filters = new API_WorkOrderFilter();
        $filters->WO_State = WO_STATE_ALL_ACTIVE;
        $filters->Tech = $techID;
        $result = $this->getWorkOrders($user->getLogin(), $user->getPassword(), 'WIN_NUM', $filters,0,1, $count);
        $counters[WO_STATE_ALL_ACTIVE] = isset($result->data) ? $count : 0;
        
        //All
        $filters = new API_WorkOrderFilter();
         $filters->WO_State = "all active";
        $filters->Tech = $techID;
        $filters->StartDateFrom = date('m/d/Y');
        $result = $this->getWorkOrders($user->getLogin(), $user->getPassword(), 'WIN_NUM', $filters,0,1, $count);
        $counters["all_current"] = isset($result->data) ? $count : 0;
        
         $filters = new API_WorkOrderFilter();
         $filters->WO_State = "all active";
        $filters->Tech = $techID;
        $filters->StartDateFrom = date('01/01/1960');
        $result = $this->getWorkOrders($user->getLogin(), $user->getPassword(), 'WIN_NUM', $filters,0,1, $count);
        $counters["all"] = isset($result->data) ? $count : 0;

        if ( $errors->hasErrors() ) return API_Response::fail();

        return API_Response::success($counters);
    }

    /**
     * get work orders using win num
     *
     * @param int $winNum
     * @param string $fields
     * @param string $sort
     * @param string $companyId
     * @access protected
     * @return API_Response
     */
    protected function getWorkOrdersWithWinNum($winNum, $fields, $sort, $companyId = null)
    {
        if (is_numeric($winNum)) $winNum = array((int)$winNum);
        if (!is_array($winNum)) return false;
        if (!empty($fields)) {
            $fieldList = str_replace(' ', '', $fields);
            $fieldListParts = explode(",", $fieldList);
        }
        else
            $fieldListParts = Core_Database::getFieldList(Core_Database::TABLE_WORK_ORDERS);

        $fieldListMap = array_combine($fieldListParts, $fieldListParts);

/*        if (!empty($fieldListMap["Status"]) || empty($fields))
            $fieldListMap["Status"] = Core_WorkOrder_Status::getStatusQueryAsName();*/

        $db = Core_Database::getInstance();
        $select = $db->select();

        $select->from(Core_Database::TABLE_WORK_ORDERS, $fieldListMap )
            ->where("WIN_NUM IN (?)", $winNum)
            ->order($sort);

        if (!empty($companyId)) {
            $select->where("Company_ID = ?", $companyId);
        }

        $result = Core_Database::fetchAll($select);

        return $result;
    }
    /**
     * get parts for a work order
     *
     * @param string $user
     * @param string $pass
     * @param string $winNum
     *
     * @return API_Response
     */
    public function getParts($user, $pass, $winNum) {
               $logger = Core_Api_Logger::getInstance();
        $logger->setUrl('getParts');
            $logger->setLogin($user);
        $logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');

        $authData = array('login'=>$user, 'password'=>$pass);
        $errors = Core_Api_Error::getInstance();

            $user = new Core_Api_TechUser();
            $user->checkAuthentication($authData);

        if (!$user->isAuthenticate())
            return API_Response::fail();

        $err = $errors->getErrors();
        if (!empty($err))
            return API_Response::fail();

        $techID = $user->getTechID();

        $parts = new Core_PartEntry(null, $winNum, null, $techID);

        $err = $errors->getErrors();
        if (!empty($err))
            return API_Response::fail();

        $objects = $parts->toAPI_PartEntry();

        return API_Response::success($objects);
    }
    /**
     * getWorkOrder
     *
     * @param string $techLogin
     * @param string $techPass
     * @param mixed $winNum
     * @param string $woFields
     * @access public
     * @return API_Response
     */
    public function getWorkOrder( $techLogin, $techPass, $winNum, $woFields = null )
    {
        require_once 'Core/Api/Class.php';
        require_once 'Core/Api/TechUser.php';
        require_once 'API/Response.php';
        require_once 'API/TechWorkOrder.php';

        /* Log request */
           $logger = Core_Api_Logger::getInstance();
        $logger->setUrl('getWorkOrder from Core_Api_TechClass ' . $winNum);
        $logger->setLogin($techLogin);
        $logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');
        /* Log request */

        /* Error object */
        $error  = Core_Api_Error::getInstance();
        /* Error object */

        /* Check authentication */
        $authData = array('login' => $techLogin, 'password' => $techPass);
        $tech = new Core_Api_TechUser;
        $tech->checkAuthentication($authData);

        if ( !$tech->isAuthenticate() ) {
            return API_Response::fail();
        }

        $techID = $tech->getTechID();
        $latitude = 0.0;
        $longitude = 0.0;
        if ( $tech->getLatitude() && $tech->getLongitude() ) {
            $latitude   = $tech->getLatitude();
            $longitude  = $tech->getLongitude();
        } else if ( $tech->getZipcode() ) {
            require_once 'Core/Geocoding/Google.php';
            $gserarch = new Core_Geocoding_Google;
            $gserarch->setZip($tech->getZipcode());
            $latitude   = $gserarch->getLat();
            $longitude  = $gserarch->getLon();
            unset($gserarch);
        }

        /* Check authentication */

        /* Check winNum */
        $winNum = 0 + $winNum;  //  WIN should be a number
        if ( !$winNum ) $error->addError(2, self::ERROR_WO_INVALID_WIN);
        /* Check winNum */

        if ( $error->hasErrors() ) {
            return API_Response::fail();
        }

        /* Prepare WO fields */
//        if ( !empty($woFields) && is_string($woFields) ) {
//            $fieldList   = explode(',', trim(str_replace(' ', '', $woFields), ','));
//            $fieldList[] = 'Tech_ID';       //  Field need for check permissions
//            $fieldList[] = 'Deactivated';   //  Field need for check permissions
//            $fieldList[] = 'ShowTechs';     //  Field need for check permissions
//            $fieldList[] = 'FLS_ID';        //  Field need for check permissions
//            $fieldList   = array_unique($fieldList, SORT_STRING);
//        } else {
            $fieldList = Core_Database::getFieldList(Core_Database::TABLE_WORK_ORDERS);
//        }

        //---  13713, 13728
        $wofFields =  array('AllowDeVryInternstoObserveWork','AllowDeVryInternstoPerformField');

        
        $db = Core_Database::getInstance();
        $select = $db->select();
        $select->from(array('w'=>Core_Database::TABLE_WORK_ORDERS), $fieldList)
                        ->where("WIN_NUM = ?", $winNum)
                        ->where("IFNULL(Tech_ID,'') = '' OR Tech_ID = '$techID'");
        $select->joinLeft(array('waf'=>'work_orders_additional_fields'),
                'waf.WINNUM = w.WIN_NUM',
                $wofFields
        );                                
        $select = Core_Database::whereProximity($select, $latitude, $longitude, "Latitude", "Longitude", 50, "Distance",true);  
//        $companyQuery = "SELECT Company_ID FROM " . Core_Database::TABLE_CLIENT_DENIED_TECHS . " WHERE TechID = ?";
                        
//		if ($tech->getDeactivated() == 1)
//			$select->where("Status IN ('" . WO_STATE_ASSIGNED . "', '" . WO_STATE_INCOMPLETE . "', '" . WO_STATE_WORK_DONE . "', '" . WO_STATE_APPROVED . "')");
//		else
//			$select->where("(Status IN ('" . WO_STATE_ASSIGNED . "', '" . WO_STATE_INCOMPLETE . "', '" . WO_STATE_WORK_DONE . "', '" . WO_STATE_APPROVED . "') OR Company_ID NOT IN ($companyQuery))", $techID);
//        print_r("<pre>");
//        print_r($tech->getDeactivated());die;
        $fieldListType = Core_Database::getFieldListType(Core_Database::TABLE_WORK_ORDERS);
        $result = Core_Database::fetchAll($select);
        $result = Core_Database::convertQueryResult($result, $fieldListType);
// print_r("tesdt <pre>");
//print_r($result);die;
        if ( is_array($result) && sizeof($result) === 1 ) {
            $worder = $result[0];

//    error_log("********* $winNum " . print_r($worder,true));
            if ($worder['Status'] == WO_STATE_CREATED ) {
                        $error->addError(5, self::ERROR_WO_NOT_PUBLISHED);
            }

			if ($worder['Status'] == WO_STATE_PUBLISHED && $tech->getDeactivated() == 1) {
                        $error->addError(5, self::ERROR_WO_NOT_PUBLISHED);
            }

            if ( !defined('FLSVersion') && ( $worder['Tech_ID'] && 0+$tech->getTechID() !== 0+$worder['Tech_ID'] ) ) { //Not FLS, WO assigned
                $error->addError(5, self::ERROR_WO_ASSIGNED_ANOTHER_TECH);
            } else if ( defined('FLSVersion') && 0 + $worder['FLS_ID'] && 0 + $tech->getFlsId() !== 0 + $worder['FLS_ID'] ) { // FLS, WO assigned
                $error->addError(5, self::ERROR_WO_ASSIGNED_ANOTHER_TECH);
            } else if ( $worder['Deactivated'] + 0 ) { // FLS, WO deactivated
                $error->addError(5, self::ERROR_WO_DEACTIVATED);
            } else if ( !(strtolower($worder['ShowTechs']) !== "true") && $worder['Tech_ID'] != $tech->getTechID() && $worder['FLS_ID'] != $tech->getFlsId()) { // Not published
                $error->addError(5, self::ERROR_WO_NOT_PUBLISHED);
            }
            unset($worder);
        } else {
            
            $select = $db->select();
            $select->from(Core_Database::TABLE_WORK_ORDERS, $fieldList)
                        ->where("WIN_NUM = ?", $winNum);
			if ($tech->getDeactivated() == 1)
				$select->where("Status IN ('" . WO_STATE_ASSIGNED . "', '" . WO_STATE_INCOMPLETE . "', '" . WO_STATE_WORK_DONE . "', '" . WO_STATE_APPROVED . "')");
			else
                        {
                            if(!empty($companyQuery))
                            {
			$select->where("(Status IN ('" . WO_STATE_ASSIGNED . "', '" . WO_STATE_INCOMPLETE . "', '" . WO_STATE_WORK_DONE . "', '" . WO_STATE_APPROVED . "') OR Company_ID NOT IN ($companyQuery))", $techID);
                            }
                            else
                            {
                                $select->where("(Status IN ('" . WO_STATE_ASSIGNED . "', '" . WO_STATE_INCOMPLETE . "', '" . WO_STATE_WORK_DONE . "', '" . WO_STATE_APPROVED . "'))");
                            }
                        }
                        //->where("IFNULL(Tech_ID,'') = '' OR Tech_ID = '$techID'");
    
            $select = Core_Database::whereProximity($select, $latitude, $longitude, "Latitude", "Longitude", 50, "Distance",true);                

            $result = Core_Database::fetchAll($select);
            $result = Core_Database::convertQueryResult($result, $fieldListType);
            
            if ( is_array($result) && sizeof($result) === 1 ) {
                $worder = $result[0];
				if ($worder['Status'] == WO_STATE_PUBLISHED && $tech->getDeactivated() == 1) {
                        $error->addError(5, self::ERROR_WO_NOT_PUBLISHED);
                }

                if ( !defined('FLSVersion') && ( $worder['Tech_ID'] && 0+$tech->getTechID() !== 0+$worder['Tech_ID'] ) ) { //Not FLS, WO assigned
                    $error->addError(5, self::ERROR_WO_ASSIGNED_ANOTHER_TECH);
                } else if ( defined('FLSVersion') && 0 + $worder['FLS_ID'] && 0 + $tech->getFlsId() !== 0 + $worder['FLS_ID'] ) { // FLS, WO assigned
                    $error->addError(5, self::ERROR_WO_ASSIGNED_ANOTHER_TECH);
                } else if ( $worder['Deactivated'] + 0 ) { // FLS, WO deactivated
                    $error->addError(5, self::ERROR_WO_DEACTIVATED);
                } else if ( !(strtolower($worder['ShowTechs']) !== "true") && $worder['Tech_ID'] != $tech->getTechID() && $worder['FLS_ID'] != $tech->getFlsId()) { // Not published
                    $error->addError(5, self::ERROR_WO_NOT_PUBLISHED);
                }
            unset($worder);
            }else{
                $error->addError(6, self::ERROR_WO_INVALID_WIN);
            }
        }

        if ( $error->hasErrors() ) {
            return API_Response::fail();
        } else {
            return API_Response::success(API_WorkOrder::makeWorkOrderArray($result, new API_TechWorkOrder(NULL)));
        }
    }
    /**
     * getWorkOrders
     *
     * @param string $techLogin
     * @param string $techPass
     * @param string $woFields
     * @param API_WorkOrderFilter $filters
     * @param int $offset
     * @param int $limit
     * @param int $countRows
     * @param string $sort
     * @access public
     * @return API_Response
     */
     //--- 13713
    public function getWorkOrders( $techLogin, $techPass, $woFields=NULL, $filters=NULL, $offset=0, $limit=0, &$countRows=null, $sort="",$tab="" )
    {
        require_once 'Core/Api/Class.php';
        require_once 'Core/Api/TechUser.php';
        require_once 'API/Response.php';
        require_once 'API/TechWorkOrder.php';
        require_once 'API/TechWorkOrderFilter.php';

        /* Log request */
           $logger = Core_Api_Logger::getInstance();
        $logger->setUrl('getWorkOrders from Core_Api_TechClass');
        $logger->setLogin($techLogin);
        $logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');
        /* Log request */

        /* Error object */
        $error  = Core_Api_Error::getInstance();
        /* Error object */

        /* Check authentication */
        $authData = array('login' => $techLogin, 'password' => $techPass);
        $tech = new Core_Api_TechUser;
        $tech->checkAuthentication($authData);

        if ( !$tech->isAuthenticate() ) {
            return API_Response::fail();
        }
        /* Check authentication */

        /* Prepare WO fields */
        if ( !empty($woFields) && is_string($woFields) ) {
            $fieldList = trim(str_replace(' ', '', $woFields), ',');
            $fieldListParts = explode(",", $fieldList);
        } else {
            $fieldListParts = Core_Database::getFieldList(Core_Database::TABLE_WORK_ORDERS);
        }
        /* Prepare WO fields */

        /* Check & Prepare Filters */
        if ( $filters === NULL ) {
            $filters = new API_WorkOrderFilter;
        } else if ( !$filters instanceof API_WorkOrderFilter ) {
            $error->addError(6, 'filters parameter should be instance of API_WorkOrderFilter class');
        }
        if ( $error->hasErrors() ) {
            return API_Response::fail();
        }
        /* Check & Prepare Filters */

        /* Filter results by Distance */

//              print_r($filters);


//		if ($filters->WO_State == WO_STATE_PUBLISHED && $tech->getDeactivated() == 1) {
//            return API_Response::success(API_WorkOrder::makeWorkOrderArray(array(), new API_TechWorkOrder(NULL)));
//        }
		if ($filters->WO_State == WO_STATE_PUBLISHED && $tech->getDeactivated() == 1) {
            return API_Response::success(API_WorkOrder::makeWorkOrderArray(array(), new API_TechWorkOrder(NULL)));
        }
      $pointGiven = !empty($filters->Zipcode) || (NULL !== $filters->Latitude && NULL !== $filters->Longitude);
      $filters->availableOrAssignedToTech = $tech->getTechId();
    
      //13728
      if($tab=="techavailable"){
            $filters->hideAvaiableIfClientDeniedForTech = $tech->getTechId();
      }
     //end 13728
	if ($tech->getDeactivated() == 1)
		$filters->deactivatedTechLimitedAccess = true;

      if ( !empty($filters->Distance) || $pointGiven) {
            /*if ( empty($filters->Distance) && !empty($filters->Zipcode)) {
                $filters->Distance = '<= 0.59';
            }*/

            $latitude = $longitude = 0.0;
            if ( NULL !== $filters->Zipcode ) {
                $gserarch = new Core_Geocoding_Google;
                $gserarch->setZip($filters->Zipcode);
                $latitude   = $gserarch->getLat();
                $longitude  = $gserarch->getLon();
                unset($gserarch);
            } 
            else if (NULL !== $filters->Latitude && NULL !== $filters->Longitude) {
                $latitude   = $filters->Latitude;
                $longitude  = $filters->Longitude;
            }
            else if ( $tech->getLatitude() && $tech->getLongitude() ) {
                $latitude   = $tech->getLatitude();
                $longitude  = $tech->getLongitude();
            } else if ( $tech->getZipcode() ) {
                require_once 'Core/Geocoding/Google.php';
                $gserarch = new Core_Geocoding_Google;
                $gserarch->setZip($tech->getZipcode());
                $latitude   = $gserarch->getLat();
                $longitude  = $gserarch->getLon();
                unset($gserarch);
            }


            if (!empty($filters->Distance) && $filters->Distance != "<= 9999999" && !empty($filters->Zipcode))
                $filters->Zipcode = NULL;

            //--- 13713
            $techID = $tech->getTechId();
/*            if($tab=="techavailable" || $tab=="techassigned"){
                if($this->hasDeVryTag($techID)==1) {
                    $filters->DeVryObserveORPerform = 1;
                }
            }             */
            //--- end 13713    
 //--- 13728
            if($tab=="techavailable"){
                if($this->ViewOnlyGroupWOonDashboard($techID)==1) {
                    $filters->ViewOnlyGroupWOForTechID = $tech->getTechId();
                }
            }                         
            //--- end 13728   
            $additionalCriteria = API_WorkOrderFilter::assembleCriteria('', '', '', '', $filters);

            $realDist = preg_replace('/[^0-9\.]/', '', $filters->Distance);

            if ($filters->Distance == "<= 9999999") $filters->Distance .= " OR (IFNULL(Zipcode,'') = '' OR IFNULL(Latitude,'') = '' OR IFNULL(Longitude,'') = '')";

            $db = Core_Database::getInstance();
            $select = $db->select();
//        $select = new Core_Database_SelectOptimized($db);

            $fieldListMap = array_combine($fieldListParts, $fieldListParts);

            $select->from(Core_Database::TABLE_WORK_ORDERS, $fieldListMap);

            $select->where($additionalCriteria);

            $select = Core_Database::whereProximity($select, $latitude, $longitude, "Latitude", "Longitude", $realDist, "Distance");
            $order = API_WorkOrderFilter::assembleSort($sort, $select);

            //$select->order(API_WorkOrderFilter::assembleSort($sort));

//            if ($realDist != 9999999)
//                $select->indexHint(array(Core_Database::TABLE_WORK_ORDERS => "USE INDEX(Latitude,Longitude)"));

            if ( $offset != 0 || $limit != 0)
                $select->limit($limit, $offset);
            $wos = Core_Database::fetchAll($select, $countRows);

            foreach ( $wos as &$wo ) {
                if (empty($wo['Zipcode']) || $wo['Latitude'] === null || $wo['Longitude'] === null) $wo['Distance'] = 999999;
            }

            $result = $wos;

            unset($wos);

            /* Filter results by Distance */
        } else {
            try {
                $result = API_WorkOrderFilter::filter('', $fieldList, '', '', '', $filters, $sort, $limit, $offset, $countRows)->toArray();

            } catch (Exception $e) {
                $error->addError(7, 'SQL Error: '. $e);
                error_log($e);
            }
        }

        if (!empty($filters->biddedOnByTech) || !empty($filters->notBiddedOnByTech)) {
            $woList = array();
            foreach ($result as $wo) {
                if (empty($wo["WIN_NUM"])) continue;
                $woList[] = $wo["WIN_NUM"];
            }
            $db = Core_Database::getInstance();
            $bSelect = $db->select();
            $bSelect->from(array('b' => Core_Database::TABLE_WORK_ORDER_BIDS), "WorkOrderID")
                ->distinct()
                ->where('WorkOrderID IN (?)', $woList);
            $bSelect->where('TechID = ?', $tech->getTechId());
            $match = $db->fetchCol($bSelect);
            $resultNew = array();
            $includeMatch = !empty($filters->biddedOnByTech);
            foreach ($result as $k=>$wo) {
                if (($includeMatch && !in_array($wo["WIN_NUM"], $match)) ||
                (!$includeMatch && in_array($wo["WIN_NUM"], $match))) continue;
                $resultNew[] = $wo;
            }
            unset($result);
            $result = $resultNew;
            unset($resultNew);
        }

        if ( $error->hasErrors() ) {
            
            return API_Response::fail();
        } else {
            
            $arrObj = API_WorkOrder::makeWorkOrderArray($result, new API_TechWorkOrder(NULL));
            //--- 13713,13728
            $Core_Api_WosClass = new Core_Api_WosClass();
            $Core_Api_Class = new Core_Api_Class();
            for($i=0;$i<count($arrObj);$i++)
            {
                $winNum = $arrObj[$i]->WIN_NUM;
                $additionalWO = $Core_Api_Class->getWorkOrderAdditionalFields($winNum);

                $arrObj[$i]->NumOfVisits=$Core_Api_WosClass->getNumOfVisits($winNum);
                $arrObj[$i]->AllowDeVryInternstoObserveWork = $additionalWO['AllowDeVryInternstoObserveWork'];
                $arrObj[$i]->AllowDeVryInternstoPerformField = $additionalWO['AllowDeVryInternstoPerformField'];
            }

            return API_Response::success($arrObj);
        }
    }
    public function checkClientDeniedTech($techID,$Company_ID,$Status)
    {
        if($Status == WO_STATE_COMPLETED)
        {
            $db = Core_Database::getInstance();
            $select = $db->select();
            $select->from(Core_Database::TABLE_CLIENT_DENIED_TECHS)
                        ->where("Company_ID = ?", $Company_ID)
                        ->where("TechID = ?",$techID);
            $fieldListType = Core_Database::getFieldListType(Core_Database::TABLE_CLIENT_DENIED_TECHS);
            $result = Core_Database::fetchAll($select, $fieldListType);
            if(!empty($result) && count($result) > 0)
            {
                return true;
            }    
        }   
        return false; 

    }  
    
    //14055
    public function isDeniedTech($techID,$companyID)
    {
        $db = Core_Database::getInstance();
        $select = $db->select();
        $select->from(Core_Database::TABLE_CLIENT_DENIED_TECHS);
        $select->where("Company_ID = '$companyID'");
        $select->where("TechID = '$techID'");        
        $result = Core_Database::fetchAll($select); 
                
        if(!empty($result) && count($result) > 0) {
            return true;
        } else {
            return false; 
        }   
    }        
    //end 14055      
    
    /**
     * updateWorkOrder
     *
     * @param string $user
     * @param string $pass
     * @access public
     * @return API_Response
     */
    public function updateWorkOrder( $user, $pass, $winNum, $workOrder )
    {
        require_once 'Core/Api/Logger.php';
        require_once 'Core/Api/TechUser.php';
        require_once 'Core/Api/Error.php';
        require_once 'API/TechWorkOrder.php';

        /* Log action */
           $logger = Core_Api_Logger::getInstance();
        $logger->setUrl('Tech updateWorkOrder ' . $winNum);
        $logger->setLogin($user);
        $logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');
        /* Log action */

        if (is_string($workOrder)) {
            $woXML = $workOrder;
        } else if ( $workOrder instanceof API_TechWorkOrder ) {
            $dom = new DOMDocument("1.0");
            $dom->appendChild($workOrder->toXMLElement($dom));
            $woXML = $dom->saveXML();
        }

        $authData = array('login'=>$user, 'password'=>$pass);

        $tech = new Core_Api_TechUser();
        $tech->checkAuthentication($authData);

        if (!$tech->isAuthenticate())
            return API_Response::fail();

        $params = array();
        $params['TB_UNID'] = $winNum;
        $params['xml'] = $woXML;

        $errors = Core_Api_Error::getInstance();
        if (empty($params['TB_UNID']))
            $errors->addError(2, 'WIN_NUM parameter is empty but required');

        if (empty($params['xml']))
            $errors->addError(2, 'XML parameter is empty but required');

        $err = $errors->getErrors();
        if (!empty($err))
            return API_Response::fail();

        $woData = $this->getDataFromXML($params['xml']);
        return $this->updateWorkOrderPostXMLParse($tech, $params, $woData);

    }
    /**
     * getDataFromXML
     *
     * @param mixed $xml
     * @access private
     * @return void
     */
    private function getDataFromXML($xml)
    {
        $result = array();
        $dom = new DOMDocument("1.0");
        $dom->loadXML($xml);
        $nodes = $dom->getElementsByTagName('WO')->item(0)->childNodes;

        foreach ($nodes as $node) {
            $result[$node->nodeName] = $node->nodeValue;
        }

        return $result;
    }
    /**
     * updateWorkOrderPostXMLParse
     *
     * @param Core_Api_TechUser $tech
     * @param array $params
     * @param array $woData
     * @access private
     * @return API_Response
     */
    private function updateWorkOrderPostXMLParse(Core_Api_TechUser $tech, array $params, array $woData) {
        $errors = Core_Api_Error::getInstance();

        $origKeys = array_keys($woData);

        /* Tech can change follow fields (keys) */
        $fields = API_TechWorkOrder::getWritableFields();
        $changableFields = array_combine($fields, array_fill(0, sizeof($fields), NULL));
        unset($fields);
        /* Tech can change fields */

        if ( $invalid = array_diff_key($woData, $changableFields) ) {
            $errors->addError(6, 'ACCESS DENIED: Tech can\'t change following fields: "' . join(', ', array_keys($invalid)).'"');
            return API_Response::fail();
        }

        /* Make full scope of changable fields */
        $woData = array_merge($changableFields, $woData);

        /* Load WO and check Tech access rights */
        $fields = join(',', array_merge(
            array('Tech_ID','TB_UNID','Tech_FName','Tech_LName','Company_ID','Company_Name'),
            array_keys($changableFields)
        ));

        $wo = $this->getWorkOrder($tech->getLogin(), $tech->getPassword(), $params['TB_UNID'], null);

        if ( !$wo->success || ! $wo->data[0] instanceof API_TechWorkOrder || 0 + $wo->data[0]->Tech_ID !== 0 + $tech->getTechId() ) {
            $errors->addError(5, 'Work Order not found or Tech has no enough permissions');
            return API_Response::fail();
        } else {
            $wo = $wo->data[0];
        }
        /* Load WO */

    $curWO = $updatedFields = (array)$wo;
    if ($wo->Status != 'assigned' && $wo->Status != 'work done' && $wo->Status != 'incomplete') {
        // clear any attempt to update tech hrs after approved
        $woData['Date_In'] = $curWO['Date_In'];
        $woData['Date_Out'] = $curWO['Date_Out'];
        $woData['Time_In'] = $curWO['Time_In'];
        $woData['Time_Out'] = $curWO['Time_Out'];
        $woData['calculatedTechHrs'] = $curWO['calculatedTechHrs'];
    }
	
	unset($woData['PushWM']);
    
    foreach ($updatedFields as $k => $v) {
        if (array_key_exists($k, $woData))
            $updatedFields[$k] = $woData[$k];
        if (is_bool($updatedFields[$k]))
            $updatedFields[$k] = $updatedFields[$k] ? "True" : "False";
        else if ($v === NULL)
            $updatedFields[$v] = "NULL";
    }

    if ($woData['TechMarkedComplete'] === NULL) $woData['TechMarkedComplete'] = $curWO['TechMarkedComplete'] == 1 ? "True" : "False";

    if ($woData['TechMarkedComplete'] == "True" && !$wo->Approved) {
        $techPayCalculator = new Core_TechPay();
        $payData = $techPayCalculator->updateTechPay($updatedFields);
        foreach ($payData as $k => $v) {
            if (!array_key_exists($k, $updatedFields) || $payData[$k] != $updatedFields[$k]) {
                $woData[$k] = $payData[$k];
                $origKeys[] = $k;
            }
        }
    }
    //--- 13571
    if(($woData['TechMarkedComplete'] == "True" && $curWO['TechMarkedComplete'] != 1)){
        //743
        $Core_Api_User = new Core_Api_User();
        $companyID = $curWO['Company_ID'];
        $isFullRestricted_old = $Core_Api_User->AreFullRestriction($companyID);
        $isPartialRestricted_old = $Core_Api_User->ArePartialRestriction($companyID);
        $isFullOpened_old = $Core_Api_User->AreFullorPartialRestriction($companyID);
        //end 743
        
        $clients = $Core_Api_User->getRepClientforCompany($companyID, true);
        $client_id = $clients["ClientID"];
        $user = $Core_Api_User->loadById($client_id);
//        $companyID = $user->getCompanyId();
        $beginDate = date("Y-m-d");
        $fieldvals=array('BeginDateForCountingNDaysRule'=>$beginDate);
        $Core_Api_User->saveClientCompanyAdditionalFields($curWO['Company_ID'],$fieldvals);
       // $Core_Api_User->setAllAccessOpened($curWO['Company_ID']);    //13995
        //743
        $apiemailrestricted = new Core_EmailForRestricted();
        

        //$clientInfo = $Core_Api_User->loadWithExt(
        //                array(
        //                    'login' => $clients[0]['UserName'],
        //                    'password' => $clients[0]['Password']
        //                ));
        //$CSD =$clientInfo->FSClientServiceDirectorEmail;
        //$AM =$clientInfo->FSAccountManagerEmail;
        $CSD = $user->FSClientServiceDirectorEmail;
        $AM = $user->FSAccountManagerEmail;
        $isFullRestricted = $Core_Api_User->AreFullRestriction($companyID);
        $isPartialRestricted = $Core_Api_User->ArePartialRestriction($companyID);
        $isFullOpened=$Core_Api_User->AreFullorPartialRestriction($companyID);
        $wonum = $params['TB_UNID'];
        $wostatus = $curWO["Status"];
        $restrictedtype = 5;
        
        $issendmail=FALSE;
        if(($isFullRestricted != $isFullRestricted_old)
                || ($isPartialRestricted != $isPartialRestricted_old)
                || ($isFullOpened!=$isFullOpened_old))
        {
            $issendmail = TRUE;
    }

        if($issendmail)
        {
            $CompanyName = $user->getCompanyName();
            $apiemailrestricted->setCompanyId($companyID);
            $apiemailrestricted->setCompanyName($CompanyName);
            $apiemailrestricted->settoMail($AM.",".$CSD);
            $apiemailrestricted->setusername($tech->getLogin());
            $apiemailrestricted->setrestrictedtype($restrictedtype);
            $apiemailrestricted->setwonum($wonum);
            $apiemailrestricted->setwostatus($wostatus);
            $apiemailrestricted->sendMail();
        }
        //end 743
    }
    
    //---
        /* Validate Tech changes */
        $factory = new Core_Validation_Factory(Core_Validation_Factory::VALIDATION_TYPE_TECH_UPDATE);
        $validation = $factory->getValidationObject($woData);

        if ( !$validation->validate() ) {
            $errors->addErrors(6, $validation->getErrors());
            return API_Response::fail();
        }
        /* Validate Tech changes */

        date_default_timezone_set('US/Central');
        $curTime = date('m/d/Y h:i:s A');

        /* Check for new data provided */
        $changed = array();
        foreach ($woData as $key => $value) {
            if ( $this->strtobool($value) === $this->strtobool($wo->$key) )
                continue;
            $changed[] = $key;
        }
        if ( !$changed ) {
            $errors->addError(6, 'No new data provied. Nothing to update');
            return API_Response::fail();
        }
        /* Check for new dta provied */

        $woData['lastModDate']  = date('m/d/Y h:i:s A');
        $woData['lastModBy']    = $tech->getLogin();



        /* Update Work Order */
    $db = Core_Database::getInstance();

    $curWO = $this->getWorkOrdersWithWinNum($params['TB_UNID'], "", "");
    $curWO = $curWO[0];

    $fieldListType = Core_Database::getFieldListType(Core_Database::TABLE_WORK_ORDERS);

    $woData = Core_Database::convertQueryResult($woData, $fieldListType);
    $curWO = Core_Database::convertQueryResult($curWO, $fieldListType);

    $updatedFields = array();
    foreach ($curWO as $key=>$val) {
        if (isset($woData[$key]) && $woData[$key]!==null) {
            $updatedFields[$key] = $woData[$key];
        } else {
            $updatedFields[$key] = $val;
        }
    }

        foreach ( $woData as $k=>$v )
            if ( !in_array($k, $origKeys) )
                unset($woData[$k]);

        if ($woData['TechMarkedComplete'] == "True" && !$wo->TechMarkedComplete )
            $woData['Date_Completed'] = $curTime;
        else if ($woData['TechMarkedComplete'] == "False")
            $woData['Date_Completed'] = null;
            
    /* Check for new data provided */
        $changed = array();
        $uploadingFields = API_WorkOrder::uploadFileFields(); 
        foreach ($woData as $key => $value) {
            if ($value == $curWO[$key]) continue;
            
            if (in_array($key, $uploadingFields) ) {
                $changed[] = $key . "({$value})";
            } else {
                $changed[] = $key;
            }
        }
        
        $changed = implode(",", $changed);
    
    $woData["Status"] = Core_WorkOrder_Status::getStatusWithWorkOrder($updatedFields);
    $updatedFields['Status'] = $woData['Status'];

        $result = Core_Database::update(Core_Database::TABLE_WORK_ORDERS, $woData, $db->quoteInto("WIN_NUM = ?", $wo->WIN_NUM));

        /*if (!$result)
            return API_Response::fail();
        */
        if ($obsync = Core_WorkOrder_OutboundSync_Factory::build($wo->Company_ID)) {
            $obsync->syncWithWorkOrder($wo->WIN_NUM, $curWO, $updatedFields, Core_WorkOrder_OutboundSync_Abstract::CONTEXT_TECH, $tech->getLogin());
        }
        Core_TimeStamp::createTimeStamp(
            $wo->WIN_NUM,
            $tech->getLogin(),
            "Work Order Updated: $changed",
            $wo->Company_ID,
            $wo->Company_Name,
            $wo->Project_Name,
            "",
            $tech->getTechId(),
            $wo->Tech_FName.' '.$wo->Tech_LName
        );
        /* Update Work Order */

        if ( !empty($woData['WorkOrderReviewed']) && $this->strtobool($woData['WorkOrderReviewed']) && !$wo->WorkOrderReviewed ) {
            Core_TimeStamp::createTimeStamp(
                $wo->WIN_NUM,
                $tech->getLogin(),
                "Work Order Reviewed",
                $wo->Company_ID,
                $wo->Company_Name,
                $wo->Project_Name,
                "",
                $tech->getTechId(),
                $wo->Tech_FName.' '.$wo->Tech_LName
            );
        }
        if ( !empty($woData['TechCheckedIn_24hrs']) && $this->strtobool($woData['TechCheckedIn_24hrs']) && !$wo->TechCheckedIn_24hrs ) {
            Core_TimeStamp::createTimeStamp(
                $wo->WIN_NUM,
                $tech->getLogin(),
                "Work Order Confirmed",
                $wo->Company_ID,
                $wo->Company_Name,
                $wo->Project_Name,
                "",
                $tech->getTechId(),
                $wo->Tech_FName.' '.$wo->Tech_LName
            );
        }
        
        if ( isset($woData['TechMarkedComplete']) && $this->strtobool($woData['TechMarkedComplete']) !== $wo->TechMarkedComplete ) {
            $val = ( $this->strtobool($woData['TechMarkedComplete']) ) ? 'Completed' : 'Not Completed';
            Core_TimeStamp::createTimeStamp(
                $wo->WIN_NUM,
                $tech->getLogin(),
                "Work Order Marked: $val",
                $wo->Company_ID,
                $wo->Company_Name,
                $wo->Project_Name,
                "",
                $tech->getTechId(),
                $wo->Tech_FName.' '.$wo->Tech_LName
            );
            if($val == 'Completed'){
                $NoticeClass = new Core_Api_NoticeClass();
                $NoticeClass->createWOChangeStatusLog($wo->WIN_NUM,Core_Api_NoticeClass::WOStatusID_FOR_WORKDONE);
            }
			if($val == 'Completed' && !empty($wo->Project_ID)) {
				$search = new Core_Api_TechClass();
				$project = $search->getProjects($tech->getLogin(), $tech->getPassword(), array($wo->Project_ID), '');
				if (!empty($project->data[0]->Project_ID))
				{
                    $woNew = $this->getWorkOrder($tech->getLogin(), $tech->getPassword(), $wo->WIN_NUM, null);
                    $woNew = $woNew->data[0];
        //echo("<pre>before send email: ");print_r($woNew);echo("</pre>");
        //die();
                    
					$mail = new Core_EmailForCompleteByTech();
					$mail->setWorkOrder($woNew);
					$mail->setProject($project->data[0]);
					$mail->sendMail();
				}
			}
        }

        //--- 13763: EmailForTechAcceptedWO
        if(isset($updatedFields["WorkOrderReviewed"])){
            if($updatedFields["WorkOrderReviewed"] == "True" && $curWO['WorkOrderReviewed']=="False" ){
                $emailCore = new Core_EmailForTechAcceptedWO();
                $emailCore->sendMail($curWO['WIN_NUM']);
            }            
        }
        //--- 13763: EmailForTechConfirmedWO
        if(isset($updatedFields["TechCheckedIn_24hrs"])){
            if($updatedFields["TechCheckedIn_24hrs"] == "True" && $curWO['TechCheckedIn_24hrs']=="False" ){
                $emailCore = new Core_EmailForTechConfirmedWO();
                $emailCore->sendMail($curWO['WIN_NUM']);
            }            
        }
        
        //--- 13763: EmailForTechCheckedIN
        if(isset($updatedFields["CheckedIn"]) || isset($updatedFields["Date_In"]) || isset($updatedFields["Date_Out"]) || isset($updatedFields["Time_In"]) || isset($updatedFields["Time_Out"])){
		$techCheckInDate = NULL;
		$techCheckInTime = NULL;
		$techCheckOutDate = NULL;
		$techCheckOutTime = NULL;
            if($updatedFields["CheckedIn"] == "True" && $curWO['CheckedIn'] == "False"  ||
		$updatedFields["Date_In"] != $curWO["Date_In"] ||
		$updatedFields["Time_In"] != $curWO["Time_In"] 
		){
//                $emailCore = new Core_EmailForTechCheckedIN();
//                $emailCore->sendMail($curWO['WIN_NUM']);
		$techCheckInDate = $updatedFields["Date_In"];
		$techCheckInTime = $updatedFields["Time_In"];
            }
            if(	$updatedFields["Date_Out"] != $curWO["Date_Out"] ||
		$updatedFields["Time_Out"] != $curWO["Time_Out"]
		){
//                $emailCore = new Core_EmailForTechCheckedOUT();
//                $emailCore->sendMail($curWO['WIN_NUM']);
		$techCheckOutDate = $updatedFields["Date_Out"];
		$techCheckOutTime = $updatedFields["Time_Out"];
            }

		if ($techCheckInDate != NULL || $techCheckInTime != NULL || $techCheckOutDate != NULL || $techCheckOutTime != NULL) {
			// update visit info related to check in / out
		        $wosClass = new Core_Api_WosClass;
			$base = $wosClass->getBaseWOVisitInfo($curWO['WIN_NUM']);
			$base = empty($base[$curWO['WIN_NUM']]) ? array() : $base[$curWO['WIN_NUM']];
			$techCheckInDate = empty($techCheckInDate) ? $base['TechCheckInDate'] : date_format(new DateTime($techCheckInDate), "Y-m-d");
			$techCheckInTime = empty($techCheckInTime) ? $base['TechCheckInTime'] : $techCheckInTime;
			$techCheckOutDate = empty($techCheckOutDate) ? $base['TechCheckOutDate'] : date_format(new DateTime($techCheckOutDate), "Y-m-d");
			$techCheckOutTime = empty($techCheckOutTime) ? $base['TechCheckOutTime'] : $techCheckOutTime;
			$wosClass->saveWOVisitCheckInOutForTech($base['WoVisitID'],$curWO['WIN_NUM'],$techCheckInDate,$techCheckInTime,$techCheckOutDate,$techCheckOutTime);
		}
        }

        
        return API_Response::success(array($wo));
    }
    /**
     * markWorkOrderComplete
     *
     * @param string $user
     * @param string $pass
     * @param string $winNum
     * @param string $checkInDate
     * @param string $checkInTime
     * @param string $checkOutDate
     * @param string $checkOutTime
     * @param string $comments
     * @param string $mileage
     * @param string $helpdeskContacted
     * @param string $whoContacted
     * @param string $pic1
     * @param string $pic2
     * @param string $pic3
     * @access public
     * @return API_Response
     */
    public function markWorkOrderComplete(
        $user,
        $pass,
        $winNum,
        $checkInDate,
        $checkInTime,
        $checkOutDate,
        $checkOutTime,
        $comments,
        $mileage            = null,
        $helpdeskContacted  = null,
        $whoContacted       = null,
        $pic1               = null,
        $pic2               = null,
        $pic3               = null
    )
    {
        require_once 'API/TechWorkOrder.php';

        $workOrder                      =   new API_TechWorkOrder;
        $workOrder->Date_In             =   $checkInDate;
        $workOrder->Time_In             =   $checkInTime;
        $workOrder->Date_Out            =   $checkOutDate;
        $workOrder->Time_Out            =   $checkOutTime;
        $workOrder->TechComments        =   $comments;
        $workOrder->TechMiles           =   $mileage;
        $workOrder->ContactHD           =   $helpdeskContacted;
        $workOrder->HDPOC               =   $whoContacted;
        $workOrder->TechMarkedComplete  =   'True';

        return $this->updateWorkOrder( $user, $pass, $winNum, $workOrder );
    }
    /**
     * acceptWorkOrder
     *
     * @param string $user
     * @param string $pass
     * @param int $winNum
     * @access public
     * @return API_Response
     */
    public function acceptWorkOrder( $user, $pass, $winNum )
    {
        require_once 'API/TechWorkOrder.php';

        $workOrder                      = new API_TechWorkOrder;
        $workOrder->WorkOrderReviewed   = 'True';

        return $this->updateWorkOrder( $user, $pass, $winNum, $workOrder );
    }
    /**
     * confirmWorkOrder
     *
     * @param string $user
     * @param string $pass
     * @param int $winNum
     * @access public
     * @return API_Response
     */
    public function confirmWorkOrder( $user, $pass, $winNum )
    {
        require_once 'API/TechWorkOrder.php';

        $workOrder                      = new API_TechWorkOrder;
        $workOrder->TechCheckedIn_24hrs = 'True';

        return $this->updateWorkOrder( $user, $pass, $winNum, $workOrder );
    }
    /**
     * uploadFile
     *
     * @param string $user
     * @param string $pass
     * @param string $winNum
     * @param string $fName
     * @param string $fData
     * @param string $fMime
     * @param string $saveToField ENUM {Pic1_FromTech, Pic2_FromTech, Pic3_FromTech}
     * @access public
     * @return API_Response
     */
    public function uploadFile( $user, $pass, $winNum, $fName, $fData, $fMime, $saveToField = 'Pic1_FromTech' )
    {
        require_once 'Core/Api/Logger.php';
        require_once 'Core/Api/Error.php';
        require_once 'API/Response.php';
        require_once 'Core/Caspio.php';

        /* Log request */
           $logger = Core_Api_Logger::getInstance();
        $logger->setUrl('Upload file from Core_Api_TechClass ' . $winNum);
        $logger->setLogin($user);
        $logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');
        /* Log request */

        $error = Core_Api_Error::getInstance();

        /* Check field */
        if ( !in_array($saveToField, API_TechWorkOrder::techUploadFileFields()) ) {
            $error->addError(6, 'Incorrect field name "'.$saveToField.'" for file saving');
            return API_Response::fail();
        }
        /* Check field */

        /* Check auth and get WO with file field we save want */
        $wo = $this->getWorkOrder($user, $pass, $winNum, join(',', API_TechWorkOrder::techUploadFileFields()).',TB_UNID,WorkOrderReviewed');
        if ( $error->getErrors() ) {
            return API_Response::fail();
        }
        $wo = $wo->data[0];
        /* Check auth and get WO with file field we save want */

        /* Check Filename */
        $fields = array_combine(API_TechWorkOrder::getWritableFields(), array_fill(0, count(API_TechWorkOrder::getWritableFields()), NULL));
        $fields = array_merge($fields, array($saveToField => $fName, 'WorkOrderReviewed' => ($wo->WorkOrderReviewed) ? 'True' : 'False'));
        $validation = new Core_Validation_Factory(Core_Validation_Factory::VALIDATION_TYPE_TECH_UPDATE);
        $validation = $validation->getValidationObject($fields);

        if ( !$validation->validate() ) {
            $error->addErrors(6, $validation->getErrors());
            return API_Response::fail();
        }
        /* Check Filename */

        /* save new file if we have no errors */
        /* Detect User's file with the same name */
        $fReplace = false;
        foreach ( API_TechWorkOrder::techUploadFileFields() as $_field ) {
            if ( ltrim($wo->$_field, '/') === $fName ) {
                $fReplace    = true;
                $saveToField = $_field; //  Save new file to field where file with the same name
                break;
            }
        }
        /* Detect User's file with the same name */
        Core_Caspio::caspioUploadFile($fName, $fData, $fMime, -1, $fReplace);
        if ( $error->getErrors() ) {
            return API_Response::fail();
        }
        /* save new file if we have no errors */

        /* Remove old file name and save new one */
        if ( !$fReplace && $wo->$saveToField ) {
            // TODO Remove file from Caspio
        }

        if ( !$fReplace ) {
            $woUp = new API_TechWorkOrder;
            $woUp->$saveToField = '/'.ltrim($fName, '/') ;
            $woUp->WorkOrderReviewed = $wo->WorkOrderReviewed;
            return $this->updateWorkOrder($user, $pass, $winNum, $woUp);
        } else {
            return API_Response::success(array($wo));
        }
        /* Remove old file name and save new one */
    }
    
    
    /**
     * uploadFile
     *
     * @param string $user
     * @param string $pass
     * @param string $winNum
     * @param string $fName
     * @param string $fData
     * @param string $descr
     * @access public
     * @return API_Response
     */
    public function uploadFileAWS( $user, $pass, $winNum, $fName, $fData, $descr, $WMFileID = NULL)
    {
        require_once 'Core/Api/Logger.php';
        require_once 'Core/Api/Error.php';
        require_once 'API/Response.php';

        /* Log request */
           $logger = Core_Api_Logger::getInstance();
        $logger->setUrl('Upload file from Core_Api_TechClass ' . $winNum);
        $logger->setLogin($user);
        $logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');
        /* Log request */

        $error = Core_Api_Error::getInstance();

        $wo = $this->getWorkOrder($user, $pass, $winNum);
        if ( $error->getErrors() ) {
            $error->addError(6, 'There is no such WO');
            return API_Response::fail();
        }

        $fName = ltrim(trim($fName), '/');

        $file = new Core_File();
        $uploadResult = $file->uploadFile($fName, $fData, S3_CLIENTS_DOCS_DIR);

//    $uploadResult = Core_File::uploadFile($fName, $fData, S3_CLIENTS_DOCS_DIR);
        
        if(!$uploadResult){
            $error->addError(6, 'Upload file '.$fName.' failed');
            $errorsDetails = $error->getErrors();
            error_log('FileUploading: (techs:WO:uploadFileAWS) ' . implode('|', $errorsDetails));
            return API_Response::fail();
        }
        
        if ( $error->getErrors() ) {
            return API_Response::fail();
        }

        $files = new Core_Files();
		$data = array(
            'WIN_NUM' => $winNum,
            'path'    => $fName,
            'type'    => Core_Files::TECH_FILE,
            'description' => mysql_escape_string($descr)
        );
		if (!empty($WMFileID)) $data['WMFileID'] = $WMFileID;
        $lastId = $files->insert($data);

		$db = Core_Database::getInstance();
		$select = $db->select();
		$select->from(Core_Database::TABLE_WORK_ORDERS, array('Company_ID'))
			->where('WIN_NUM = ?', $winNum);
		$company_id = $db->fetchOne($select);
		// perform any outbound sync
		if ($obsync = Core_WorkOrder_OutboundSync_Factory::build($company_id)) {
			$note = new Core_WorkOrder_OutboundSync_Note(NULL, array($lastId), Core_WorkOrder_OutboundSync_Note::AUDIENCE_ALL);
			$obsync->sync($winNum, $note, Core_WorkOrder_OutboundSync_Abstract::STATUS_WO_FILE_ATTACHED, Core_WorkOrder_OutboundSync_Abstract::CONTEXT_TECH, Core_Api_TechUser::getLoginByAuth($user));
		}
        
        Core_TimeStamp::createTimeStamp($winNum, Core_Api_TechUser::getLoginByAuth($user), "Work Order Updated: File {$fName} was uploaded.", '', '', '', "", "", "");

        if($lastId) $res = new API_Response (1, 0, $lastId);
        else $res = API_Response::fail();
        
        return $res;
    }
    
    /**
     * getProfilePic
     *
     * @param string $user
     * @param string $pass
     * @access public
     * @return API_Response
     */
    public function getProfilePic($user, $pass) {
        $authData = array('login'=>$user, 'password'=>$pass);
        $tech = new Core_Api_TechUser();
        $tech->checkAuthentication($authData);
    
        if (!$tech->isAuthenticate())
            return API_Response::fail();

        $db = Core_Database::getInstance();
        $select = $db->select();
        $select->from(Core_Database::TABLE_TECH_BANK_INFO,
            array('Picture')
        );

        $select->where('TechID = ?', $tech->getTechId());

        $fileName = $db->fetchCol($select);
        if (!$fileName || empty($fileName[0])) { return API_Response::success(array()); }
        return API_Response::success(array($this->downloadFile($user, $pass, $fileName[0])));
    }
    
    /**
     * getFile
     *
     * @param string $user
     * @param string $pass
     * @param string $fileName
     * @access public
     * @return API_Response
     */
    public function downloadFile( $user, $pass, $fileName)
    {

           $logger = Core_Api_Logger::getInstance();
        $logger->setUrl('downloadFile');
        $logger->setLogin($user);
        $logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');

        $authData = array('login'=>$user, 'password'=>$pass);
        $errors = Core_Api_Error::getInstance();
        $errors->resetErrors();
        $tech = new Core_Api_TechUser();
        $tech->checkAuthentication($authData);

        if (!$tech->isAuthenticate())
            return API_Response::fail();

        $file = Core_Caspio::caspioDownloadFile( $fileName );

        $err = $errors->getErrors();
        if (!empty($err))
            return API_Response::fail();

        return $file;
    }
    /**
     * get parts for a work order
     *
     * @param string $user
     * @param string $pass
     * @param string $winNum
     *
     * @return API_Response
     */
    public function getTechParts($user, $pass, $winNum)
    {
               $logger = Core_Api_Logger::getInstance();
        $logger->setUrl('getParts');
            $logger->setLogin($user);
        $logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');

        $authData = array('login'=>$user, 'password'=>$pass);
        $errors = Core_Api_Error::getInstance();

        $user = new Core_Api_TechUser();
        $user->checkAuthentication($authData);

        if (!$user->isAuthenticate())
            return API_Response::fail();

        $techID = $user->getTechID();

        $user = Core_Caspio::caspioEscape($authData["login"]);
        $pass = Core_Caspio::caspioEscape($pass);

        $parts = new Core_PartEntry(null, $winNum, null, $techID);
        $objects = $parts->toAPI_PartEntry();

        $err = $errors->getErrors();
        if (!empty($err))
            return API_Response::fail();

        return API_Response::success($objects);
    }


    /**
     * Function get list of WO Categories
     *
     * @param string $user
     * @param string $pass
     * @return API_Response
     */
    public function getWOCategory($user, $pass, $categoryId)
    {
           $logger = Core_Api_Logger::getInstance();
        $logger->setUrl('getWOCategories');
        $logger->setLogin($user);
        $logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');

        $authData = array('login'=>$user, 'password'=>$pass);
        $errors = Core_Api_Error::getInstance();

        $user = new Core_Api_User();
        $user->checkAuthentication($authData);

        if (!$user->isAuthenticate())
            return API_Response::fail();

        $fieldsList = 'Category_ID,Category';

        $criteria   = "Category_ID = '" . (int)$categoryId . "'";

        $result = Core_Caspio::caspioSelectAdv(TABLE_WO_CATEGORIES, $fieldsList, $criteria, '');

        $err = $errors->getErrors();
        if (!empty($err))
            return API_Response::fail();

        return API_Response::success($this->makeWOCategoryArray($result));
    }



    /**
     * get part entry
     *
     * @param string $user
     * @param string $pass
     * @param string $winNum
     * @param string $partID
     *
     * @return API_Response
     */
    public function getTechPartEntry($user, $pass, $winNum, $partID)
    {
           $logger = Core_Api_Logger::getInstance();
        $logger->setUrl('getPartEntry');
        $logger->setLogin($user);
        $logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');

        $authData = array('login'=>$user, 'password'=>$pass);
        $errors = Core_Api_Error::getInstance();

        $user = new Core_Api_TechUser();
        $user->checkAuthentication($authData);

        if (!$user->isAuthenticate())
            return API_Response::fail();

        $techID = $user->getTechID();

        $user = Core_Caspio::caspioEscape($authData["login"]);
        $pass = Core_Caspio::caspioEscape($pass);

        $parts = new Core_PartEntry(array($partID), $winNum, null, $techID);

        $err = $errors->getErrors();
        if (!empty($err))
            return API_Response::fail();

        $objects = $parts->toAPI_PartEntry();

        return API_Response::success($objects);
    }

    /**
     * update part entry
     *
     * @param string $user
     * @param string $pass
     * @param string $winNum
     * @param string $partID
     * @param API_ReturnPart $part
     *
     * @return API_Response
     */
    public function updateTechPartEntry($user, $pass, $winNum, $partID, $part) {
        if (is_string($part)) $data = $part;
        else {
            $dom = new DOMDocument("1.0");
            $dom->appendChild($part->toXMLElement($dom));
            $data = $dom->saveXML();
        }

           $logger = Core_Api_Logger::getInstance();
        $logger->setUrl('updateTechPartEntry');
        $logger->setLogin($user);
        $logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');

        $authData = array('login'=>$user, 'password'=>$pass);
        $errors = Core_Api_Error::getInstance();

        $user = new Core_Api_TechUser();
        $user->checkAuthentication($authData);

        if (!$user->isAuthenticate())
            return API_Response::fail();

        $techID = $user->getTechID();

        $user = Core_Caspio::caspioEscape($authData["login"]);
        $pass = Core_Caspio::caspioEscape($pass);

        if (empty($data))
            $errors->addError(2, 'parts XML is empty but required');

        $err = $errors->getErrors();
        if (!empty($err))
            return API_Response::fail();

        $partData = $this->getDataFromXMLGeneric($data);

        $parts = new Core_PartEntry(array($partID), $winNum, null, $techID);
        $objects = $parts->toAPI_PartEntry();
        
        $err = $errors->getErrors();
        if (!empty($err))
            return API_Response::fail();

        if ($part instanceof API_PartEntry)
            $partData = $partEntry;
        else {
            $partData = $this->getDataFromXMLGeneric($data);
            $entry = array();
            $entry[0] = $partData["NewPart"];
            $entry[0]["IsNew"] = "True";
            $entry[0]["Consumable"] = $partData["Consumable"];
            $entry[0]["PartEntryID"] = $partID;
            $entry[1] = $partData["ReturnPart"];
            $entry[1]["IsNew"] = "False";
            $partData = API_PartEntry::partEntryWithResult($entry);
        }

        if ($partID) {
	        Core_PartEntry::update($partData, $winNum);
        }
        else {
	        $partID = Core_PartEntry::create($partData, $winNum);
        }

        $err = $errors->getErrors();
        if (!empty($err))
            return API_Response::fail();

        $objects = array(new API_PartEntry($partID, NULL));

        return API_Response::success($objects);
    }

    /**
     * Function for getting Projects for current usre
     *
     * @param string $user
     * @param string $pass
     * @param array $project_ids
     * @param string $sort
     * @return API_Response
     */
    public function getProjects($user, $pass, $project_ids, $sort)
    {
           $logger = Core_Api_Logger::getInstance();
        $logger->setUrl('getProjects');
        $logger->setLogin($user);
        $logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');

        $authData = array('login'=>$user, 'password'=>$pass);
        $errors = Core_Api_Error::getInstance();

        $user = new Core_Api_TechUser();
        $user->checkAuthentication($authData);

        if (!$user->isAuthenticate())
            return API_Response::fail();

        if (!is_array($project_ids)) $project_ids = array($project_ids);

        $db = Core_Database::getInstance();
        $select = $db->select()->from(Core_Database::TABLE_PROJECTS);
        if (count($project_ids)) $select->where('Project_ID IN (?)', $project_ids);

        $result = Core_Database::fetchAll($select);

        $err = $errors->getErrors();
        if (!empty($err))
            return API_Response::fail();

        return API_Response::success($this->makeProjectsArray($result));
    }

    /**
     * pull basic tech info
     *
     * @param string $user
     * @param string $pass
     * @param int $id
     * @return API_Response
     */
    public function techLookup($user, $pass, $id = NULL)
    {
        $authData = array('login'=>$user, 'password'=>$pass);
        $errors = Core_Api_Error::getInstance();

        $user = new Core_Api_TechUser();
        $user->checkAuthentication($authData);

        if (!$user->isAuthenticate())
            return API_Response::fail();
        if ($id == NULL) $id = $user->getTechId();
        $tech = new API_Tech();
        $tech->lookupID($id);
        return API_Response::success(array($tech));
    }

    private function makePartsArray($result) {
        $parts = array();
        $entryList = array();

        if (!empty($result[0])) {
            foreach ($result as $key=>$value) {
                if (!array_key_exists($value['PartEntryID'], $entryList)) {
                    // New Part Entry
                    $entryList[$value['PartEntryID']] = array();
                }
                $entryList[$value['PartEntryID']][] = $value;
            }
            foreach ($entryList as $key=>$value) {
                $part = API_PartEntry::partEntryWithResult($value);
                $parts[] = $part;
            }
        }
        return $parts;
    }

    private function getTableDesign($tableName)
    {
        $columns = Core_Caspio::caspioGetTableDesign($tableName);
        $fieldList = array();

        foreach ($columns as $info) {
            $fieldInfo = explode(",",$info);
            $fieldList[] = trim($fieldInfo[0]);
        }

        $fieldList = implode(',', $fieldList);

        return $fieldList;
    }

    private function doesPartExist($companyID, $winNum, $partEntryID) {
        $errors = Core_Api_Error::getInstance();
        // check work order exists
        $result = $this->getWorkOrdersWithWinNum($winNum, "WIN_NUM", "", $companyID);

        if (empty($result[0])) {
            $errors->addError(2, 'WinNum '.$winNum.' does not exist');
            return FALSE;
        }

        $resultPart = Core_Caspio::caspioSelectAdv(TABLE_PART_ENTRIES, "id", "id = '$partEntryID' AND WinNum = '$winNum' AND Deleted = '0'", '');

        if (@$resultPart[0]["id"] != $partEntryID) {
            $errors->addError(2, "PartEntryID $partEntryID does not exist");
            return FALSE;
        }
        return TRUE;
    }

    private function getDataFromXMLGeneric($xml)
    {
        $result = array();
        $dom = new DOMDocument("1.0");
        $dom->loadXML($xml);

        $result = $this->nodeToArray($dom->documentElement);

        return $result;
    }

    private function nodeToArray($node) {
        if ($node->hasChildNodes()) {
            $result = array();
            $children = $node->childNodes;
            foreach ($children as $child) {
                $childVal = $this->nodeToArray($child);
                if ($child->nodeName == "#text")
                    $result = $childVal;
                else
                    $result[$child->nodeName] = $childVal;
            }
            return $result;
        }
        return $node->nodeValue;
    }

    private function updatePart($data) {
        $errors = Core_Api_Error::getInstance();
        $data["ETA"] = empty($data["ETA"]) ? NULL : $data["ETA"];
        $isNew = isset($data["IsNew"]) && $data["IsNew"] == "True" ? 1 : 0;
        $partEntryID = $data["PartEntryID"];

        $result = Core_Caspio::caspioUpdate(TABLE_PARTS, array_keys($data), array_values($data), "PartEntryID = '$partEntryID' AND IsNew = '$isNew'");

        if ($result == 0) {
            $result = Core_Caspio::caspioInsert(TABLE_PARTS, array_keys($data), array_values($data));
        }

        if (!$result) {
            $err = $errors->getErrors();
            if (!empty($err))
                return FALSE;

            $errors->addError(7, 'Incorrect SQL request');
            return FALSE;
        }

        return TRUE;
    }


    private function strtobool( $val )
    {
        if ( !is_string($val) )
            return $val;

        $val = strtolower(trim($val));

        if ( !empty($val) ) {
            switch ( $val ) {
                case 'true' :
                    return TRUE;
                case 'false':
                case '0' :
                    return FALSE;
                case 'null':
                    return NULL;
            }
        }
        if ( empty($val) )
            return FALSE;
        return TRUE;
    }
    private function makeProjectsArray($result)
    {
        $wo = array();
        if (!empty($result[0])) {
            foreach ($result as $key=>$value) {
                $wo[] = API_Project::projectWithResult($value);
            }
        }
        return $wo;
    }
    private function makeApplicantsArray($result)
    {
        $wo = array();
        if (!empty($result[0])) {
            foreach ($result as $key=>$value) {
                $wo[] = API_Applicant::applicantWithResult($value);
            }
        }
        return $wo;
    }

    /**
     * save bid for tech and order
     * @param string $user
     * @param string $pass
     * @param int $winNum
     * @param double $bidAmount
     * @param string $comments
     * @return API_Response
     * @author Artem Sukharev
     */ //14061
    public function createBid( $user, $pass, $winNum, $bidAmount, $comments = '', $techBidID = false)
    {
        $errors = Core_Api_Error::getInstance();

        $logger = Core_Api_Logger::getInstance();
        $logger->setUrl('createBid');
        $logger->setLogin($user);
        $logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');

        $authData = array('login'=>$user, 'password'=>$pass);

        $tech = new Core_Api_TechUser();
        $tech->checkAuthentication($authData);

        if ( !$tech->isAuthenticate() ) return API_Response::fail();

        $techID = $tech->getTechID();
		if (!$techBidID) $techBidID = $techID;
        $techBibInfo =$this->getTechInfoByID($techBidID);
        if(!empty($techBibInfo))
        {       
            $techFirstName = $techBibInfo["FirstName"];
            $techLastName = $techBibInfo["LastName"];
        }

        $user = Core_Caspio::caspioEscape($authData["login"]);
        $pass = Core_Caspio::caspioEscape($pass);
        $winNum = Core_Caspio::caspioEscape($winNum);

        $wo = $this->getWorkOrder($user, $pass, $winNum);

        if ( $errors->hasErrors() ) return API_Response::fail();

        $wo = $wo->data[0];

        $project = ( $wo->Project_ID ) ? $this->getProjects($user, $pass, array($wo->Project_ID), '') : null;
        $project = ( null !== $project && $project->success && $project->data[0] ) ? $project->data[0] : null;

        $clent_email = '';
        if ( null !== $project && $project->AutoSMSOnPublish == 'True' ) {
            $client = new Core_Api_User();
            if ( $client->lookupUsername( $wo->Username ) ) $clent_email = $client->getEmailPrimary();
        }

        $wo->WO_ID = str_replace(array("(", ")"), array("", ""), $wo->WO_ID);

        $objDateNow = new Zend_Date();
        $data = array(
            'WorkOrderID' => $wo->WIN_NUM,
            'WorkOrderNum' => $wo->WO_ID,
            'TechID' => $techBidID,
            'Tech_FName' => $techFirstName,
            'Tech_LName' => $techLastName,
            'Company_ID' => $wo->Company_ID,
            'BidAmount' => $bidAmount,
            'Bid_Date' => $objDateNow->toString('yyyy-MM-dd HH:mm:ss'),
            'BidModifiedDate' => $objDateNow->toString('yyyy-MM-dd HH:mm:ss'),
            'Comments' => $comments,
            'ClientEmail' => $clent_email, //E-mail address of the creator of the WO
            'DeactivatedTech' => 0,
            'CreatedByUser' => $wo->Username,
            'Hide' => 0
        );
        //--- get last bid //2012-05
        $db = Core_Database::getInstance();
        $s = $db->select()
            ->from(Core_Database::TABLE_WORK_ORDER_BIDS, array('*'))
            ->where(Core_Database::TABLE_WORK_ORDER_BIDS.'.WorkOrderID = ?', $wo->WIN_NUM)
            ->where(Core_Database::TABLE_WORK_ORDER_BIDS.'.TechID = ?', $techBidID);
        $s->order("id desc");
        $res = $db->fetchAll($s);
        $lastBid = null;
        if(!empty($res))
        {
            // update last bid
            $lastBid = $res[0];
            $lastBidId = $lastBid['id'];
            $criteria = 'id = '.$lastBidId;
            Core_Database::update(Core_Database::TABLE_WORK_ORDER_BIDS, $data, $criteria);
        }
        else
        {
       
            //new bid added to table: work_order_bids        
            Core_Database::insert(Core_Database::TABLE_WORK_ORDER_BIDS, $data);            
        }

		$notice = new Core_Api_NoticeClass ();
		$notice->createWOChangeStatusLog ($wo->WIN_NUM, Core_Api_NoticeClass::WOStatusID_FOR_TECH_BID, NULL, $techBidID, $techFirstName." ".$techLastName, $bidAmount);

		//handle the Qty Applicants Update
        $db = Core_Database::getInstance();
        try{
          $db->beginTransaction();
            //get current Qty Applicant total
            $query = $db->select();
            $query->from(Core_Database::TABLE_WORK_ORDERS, 'Qty_Applicants');
            $query->where('WIN_NUM = ?',$wo->WIN_NUM);
            $currentApplicantCount = Core_Database::fetchAll($query);
            //update table:work_orders with +1
            $data = array('Qty_Applicants'=>$currentApplicantCount[0]['Qty_Applicants']+1);
            $criteria = 'WIN_NUM = '.$wo->WIN_NUM.'';
           $db->update(Core_Database::TABLE_WORK_ORDERS,$data,$criteria);
         $db->commit();
        }catch(Zend_Exception $e){
          //fail path
          $db->rollBack();
        }
        $ab = new Core_AfterBid(array($wo->WIN_NUM,$techBidID,$bidAmount,$comments,$techFirstName,$techLastName, $wo, $tech));
        $ab->run();

    //return value...
         return API_Response::success(null);
    }


    /**
     * update bid for tech and order
     * @param string $user
     * @param string $pass
     * @param int $winNum
     * @param double $bidAmount
     * @param string $comments
     * @return API_Response
     * @author Alex Scherba
     */
    public function updateBid( $user, $pass, $winNum, $bidAmount, $comments = '' )
    {
        $errors = Core_Api_Error::getInstance();

        $logger = Core_Api_Logger::getInstance();
        $logger->setUrl('updateBid');
        $logger->setLogin($user);
        $logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');

        $authData = array('login'=>$user, 'password'=>$pass);

        $tech = new Core_Api_TechUser();
        $tech->checkAuthentication($authData);

        if ( !$tech->isAuthenticate() ) return API_Response::fail();

        $techID = Core_Caspio::caspioEscape($tech->getTechID());
        $user = Core_Caspio::caspioEscape($authData["login"]);
        $pass = Core_Caspio::caspioEscape($pass);
        $winNum = Core_Caspio::caspioEscape($winNum);
        $bidAmount = Core_Caspio::caspioEscape($bidAmount);

        $data = array(
            'BidAmount' => $bidAmount,
            'Comments' => $comments
        );
        $criteria = 'WorkOrderID = '.$winNum.' AND TechID = '.$techID;
        Core_Database::update(Core_Database::TABLE_WORK_ORDER_BIDS, $data, $criteria);
        return API_Response::success(null);
    }

    /**
     * get bid for tech and order
     * @param string $user
     * @param string $pass
     * @param int $winNum
     * @return API_Response
     * @author Alex Scherba
     */
    public function getBid( $user, $pass, $winNum )
    {
        $errors = Core_Api_Error::getInstance();

           $logger = Core_Api_Logger::getInstance();
        $logger->setUrl('createBid');
        $logger->setLogin($user);
        $logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');

        $authData = array('login'=>$user, 'password'=>$pass);

        $tech = new Core_Api_TechUser();
        $tech->checkAuthentication($authData);

        if ( !$tech->isAuthenticate() ) return API_Response::fail();

        $techID = $tech->getTechID();

        $db = Core_Database::getInstance();
        $query = $db->select();
        $query->from(Core_Database::TABLE_WORK_ORDER_BIDS, array('*'));
        $query->where('WorkOrderID = ?', $winNum);
        $query->where('TechID = ?', $techID);
        $result = Core_Database::fetchAll($query);

        if ($result === false) {
            if ( $errors->hasErrors() ) return API_Response::fail();
            $errors->addError(7, 'Incorrect SQL request');
            return API_Response::fail();
        }
        if ( $errors->hasErrors() ) return API_Response::fail();

        return API_Response::success($this->makeApplicantsArray($result));
    }
    public function getLastBid( $user, $pass, $winNum )
    {
        $errors = Core_Api_Error::getInstance();

           $logger = Core_Api_Logger::getInstance();
        $logger->setUrl('createBid');
        $logger->setLogin($user);
        $logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');

        $authData = array('login'=>$user, 'password'=>$pass);

        $tech = new Core_Api_TechUser();
        $tech->checkAuthentication($authData);

        if ( !$tech->isAuthenticate() ) return API_Response::fail();

        $techID = $tech->getTechID();

        $db = Core_Database::getInstance();
        $query = $db->select();
        $query->from(Core_Database::TABLE_WORK_ORDER_BIDS, array('*'));
        $query->where('WorkOrderID = ?', $winNum);
        $query->where('TechID = ?', $techID);
        $query->order("id desc");
        $result = Core_Database::fetchAll($query);

        if ($result === false) {
            if ( $errors->hasErrors() ) return API_Response::fail();
            $errors->addError(7, 'Incorrect SQL request');
            return API_Response::fail();
        }
        if ( $errors->hasErrors() ) return API_Response::fail();
        
        if(!empty($result)) return $result[0];
        else return null;
    }    
/**
     * get bid count for tech
     * @param string $user
     * @param string $pass
     * @return Bid Count
     * @author Jose Cintron
     */
    public function getBidCount( $user, $pass)
    {
        $errors = Core_Api_Error::getInstance();

           $logger = Core_Api_Logger::getInstance();
        $logger->setUrl('createBid');
        $logger->setLogin($user);
        $logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');

        $authData = array('login'=>$user, 'password'=>$pass);

        $tech = new Core_Api_TechUser();
        $tech->checkAuthentication($authData);

        if ( !$tech->isAuthenticate() ) return API_Response::fail();

        $techID = $tech->getTechID();

        $db = Core_Database::getInstance();
        $query = $db->select();
        $query->from(Core_Database::TABLE_WORK_ORDER_BIDS, array('COUNT(*)'));
        $query->where('TechID = ?', $techID);
        $result = Core_Database::fetchAll($query);

        if ( !$result ) {
            if ( $errors->hasErrors() ) return API_Response::fail();
            $errors->addError(7, 'Incorrect SQL request');
            return API_Response::fail();
        }
        if ( $errors->hasErrors() ) return API_Response::fail();
        
        $count = $result[0]["COUNT(*)"];
        
        return $count;
    }
    
    
    
     /**
     * Function get list of WO Categories
     *
     * @param string $user
     * @param string $pass
     * @param string $conpanyId
     * @return API_Response
     */
    public function getCompanyLogo($user, $pass, $companyId)
    {
            $errors = Core_Api_Error::getInstance();
            $errors->resetErrors();
            
               $logger = Core_Api_Logger::getInstance();
            $logger->setUrl('createBid');
            $logger->setLogin($user);
            $logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');
    
            $authData = array('login'=>$user, 'password'=>$pass);
    
            $tech = new Core_Api_TechUser();
            $tech->checkAuthentication($authData);

            $db = Core_Database::getInstance();
            $select = $db->select();
            $select->where('Company_ID = ?', $companyId);
            $select->where('Company_Logo != ""');
            $select->from(Core_Database::TABLE_CLIENTS, 'Company_Logo');

            $result = Core_Database::fetchAll($select);

            $err = $errors->getErrors();
            if (!empty($err)) return API_Response::fail();
            if (empty($result[0]['Company_Logo']) )  return API_Response::fail();
            return API_Response::success($result[0]['Company_Logo']);
    }
    
	/**
	 * Returns the company default work order signoff sheet, if
	 * one exists.
	 *
	 * @param string $user
	 * @param string $pass
	 * @param string $companyId
	 * @return API_Response
	 */
	public function getSignoffSheet($user, $pass, $companyId)
	{
		$errors = Core_Api_Error::getInstance();
		$errors->resetErrors();

		$logger = Core_Api_Logger::getInstance();
		$logger->setUrl('createBid');
		$logger->setLogin($user);
		$logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');

		$authData = array('login'=>$user, 'password'=>$pass);

		$tech = new Core_Api_TechUser();
		$tech->checkAuthentication($authData);

		$db = Core_Database::getInstance();
		$select = $db->select();
		$select->from(Core_Database::TABLE_CLIENTS, 'CustomSignOff');
		$select->where('Company_ID = ?', $companyId);
		$select->where('CustomSignOff != ""');
		$select->where('NOT ISNULL (CustomSignOff)');

		$result = Core_Database::fetchAll($select);

		$err = $errors->getErrors();
		if (!empty($err)) return API_Response::fail();
		return API_Response::success($result[0]['CustomSignOff']);
	}
    
    /**
     * Function get list of Documents
     *
     * @param string $user
     * @param string $pass
     * @param int $win
     * @return API_Response
     */
    function getWODocList($user, $pass, $win) {
        $authData = array('login'=>$user, 'password'=>$pass);

        $tech = new Core_Api_TechUser();
        $tech->checkAuthentication($authData);

        if ( !$tech->isAuthenticate() ) return API_Response::fail();

        $techID = $tech->getTechID();
        
        $errors = Core_Api_Error::getInstance();
        $wo = $this->getWorkOrdersWithWinNum($win, null, "");
        $wo = $wo[0];
        if (empty($wo) || $wo['Tech_ID'] != $techID) {
            $errors->addError(2, 'WinNum '.$win.' does not exist');
            return API_Response::fail();
}
        
        $files = new Core_Files();
        $select = $files->select();
        $select->where('WIN_NUM = ?', $win);
        $filesList = $files->fetchAll($select);
        $list = array();
        foreach ($filesList as $f) {
            switch ($f->type) {
                case Core_Files::WOS_FILE:
                    $fType = 'wo';
                    break;
                case Core_Files::TECH_FILE:
                    $fType = 'tech';
                    break;
                case Core_Files::SIGNOFF_FILE:
                    $fType = 'signoff';
                    break;
                default:
                    continue;
            }
            $list[] = array(
                'path' => $f->path, 
                'description' => $f->description,
                'type' => $fType
            );
        }
        
        $prjFilesObj = new Core_ProjectsFiles();
        $select = $prjFilesObj->select();
        $select->where('Project_ID = ?', $wo['Project_ID']);
        $prjFiles = $prjFilesObj->fetchAll($select);
        
        foreach ($prjFiles as $f) {
            $list[] = array(
                'path' => $f->path, 
                'description' => $f->description,
                'type' => 'project'
            );
        }
        
        return API_Response::success($list);
    }

    /**
     * download file from amazon
     *
     * @param string $user
     * @param string $pass
     * @param string $fileName
     * @param string $type
     * @access public
     * @return API_Response
     */
    public function downloadFileAWS( $user, $pass, $fileName, $type)
    {
        ini_set('memory_limit', '512M');
        $authData = array('login'=>$user, 'password'=>$pass);
        $errors = Core_Api_Error::getInstance();
        $errors->resetErrors();
        $tech = new Core_Api_TechUser();
        $tech->checkAuthentication($authData);

        if (!$tech->isAuthenticate())
            return API_Response::fail();
        
        switch ($type) {
            case 'wo':
            case 'tech':
                $file = API_WorkOrder::getFile($fileName);

                break;
            case 'project':
                $getter = new Core_Api_ProjectClass();
                $file = $getter->getFile($filename);
                break;
            default:
                $file = '';
        }

        $err = $errors->getErrors();
        if (!empty($err))
            return API_Response::fail();

        return API_Response::success(array($file));
    }

     /**
     * login
     *
     * @param string $user
     * @param string $pass
     * @param string $platform
     * @param string $mobileVersion
     * @access public
     * @return API_Response
     */
    public function login($user, $pass, $platform = NULL, $mobileVersion = NULL)
    {        
        $authData = array('login'=>$user, 'password'=>$pass);
        $tech = new Core_Api_TechUser();
        $tech->checkAuthentication($authData);

        if (!$tech->isAuthenticate())
            return API_Response::fail();
            
        $class = new Core_Api_Class;
        $class->loginHistory($user, "Tech", NULL, $platform, $mobileVersion);
        return API_Response::success();
    }
    
    /**
     * getNetPaidYearToDate
     *
     * @param string $techLogin
     * @param string $techPass
     * @access public
     * @return $netPaidYear
    */
    public function getNetPaidYearToDate($techLogin, $techPass)
    {
        require_once 'Core/Api/Class.php';
        require_once 'Core/Api/TechUser.php';
        require_once 'API/Response.php';
        require_once 'API/TechWorkOrder.php';
        require_once 'API/TechWorkOrderFilter.php';
        
        /* Error object */
        $error  = Core_Api_Error::getInstance();

        /* Check authentication */
        $authData = array('login' => $techLogin, 'password' => $techPass);
        $tech = new Core_Api_TechUser;
        $tech->checkAuthentication($authData);

        if ( !$tech->isAuthenticate() ) {
            return API_Response::fail();
        }

        $techID = $tech->getTechID();

        /* Prepare WO fields */
        $db = Core_Database::getInstance();
        $query = $db->select();
        $query->from(Core_Database::TABLE_WORK_ORDERS, new Zend_Db_Expr('SUM(PayAmount_Final - PayAmount_Final*PcntDeductPercent)'));/*334*/
        $query->where("Tech_ID = ?", $techID);
        
        $netPaidYear = $db->fetchOne($query);

        if(!isset($netPaidYear)) $netPaidYear=0;
        else $netPaidYear = round($netPayForYear, 2); /*334*/
        $netPayForYear = number_format($netPayForYear,2,'.',''); /*334*/
        return $netPaidYear; 
    }
    
    
    public function getPayStubsForPeriod($techLogin, $techPass,$startDate,$endDate, $order = "")
    {
        require_once 'Core/Api/Class.php';
        require_once 'Core/Api/TechUser.php';
        require_once 'API/Response.php';
        require_once 'API/TechWorkOrder.php';
        require_once 'API/TechWorkOrderFilter.php';
        
        /* Error object */
        $error  = Core_Api_Error::getInstance();

        /* Check authentication */
        $authData = array('login' => $techLogin, 'password' => $techPass);
        $tech = new Core_Api_TechUser;
        $tech->checkAuthentication($authData);

        if ( !$tech->isAuthenticate() ) {
            return API_Response::fail();
        }

        $techID = $tech->getTechID();
                
        /* Look up database */
        
        $fieldListParts = Core_Database::getFieldList(Core_Database::TABLE_WORK_ORDERS);
        $fieldListMap = array_combine($fieldListParts, $fieldListParts);
        $db = Core_Database::getInstance();
        $select = $db->select();

        $select->from(Core_Database::TABLE_WORK_ORDERS, $fieldListMap )
                ->where("Tech_ID = ?", $techID)
                ->where("DatePaid >= '$startDate'")
                ->where("DatePaid <= '$endDate'");                
        if($order != "")
        {
            $select->order($order);
        }
        $result = Core_Database::fetchAll($select);
        return $result;
    }
    
    
    /**
     * getNetPayForPeriod
     *
     * @param string $techLogin
     * @param string $techPass
     * @param string $startDate
     * @param string $endDate
     * @access public
     * @return $netPayForPeriod
    */
    public function getNetPayForPeriod($techLogin, $techPass,$startDate,$endDate)
    {
        require_once 'Core/Api/Class.php';
        require_once 'Core/Api/TechUser.php';
        require_once 'API/Response.php';
        require_once 'API/TechWorkOrder.php';
        require_once 'API/TechWorkOrderFilter.php';
        
        /* Error object */
        $error  = Core_Api_Error::getInstance();

        /* Check authentication */
        $authData = array('login' => $techLogin, 'password' => $techPass);
        $tech = new Core_Api_TechUser;
        $tech->checkAuthentication($authData);
        
        if ( !$tech->isAuthenticate() ) {
            return API_Response::fail();
        }

        $techID = $tech->getTechID();
                
        /* Look up database */
        $db = Core_Database::getInstance();
        $query = $db->select();
        $query->from(Core_Database::TABLE_WORK_ORDERS, new Zend_Db_Expr('SUM(PayAmount_Final)'));/*334*/
        $query->where("Tech_ID = ?", $techID);
        $query->where("Approved = 1");        
        $query->where("Status='completed'");                
        $query->where("DatePaid >= '$startDate'");        
        $query->where("DatePaid <= '$endDate'");        
        
        $netPayForPeriod = $db->fetchOne($query);

        if(!isset($netPayForPeriod)) $netPayForPeriod=0;
        else $netPayForPeriod = round($netPayForPeriod,2);/*334*/
        $netPayForPeriod = number_format($netPayForPeriod,2,'.',''); /*334*/
        return $netPayForPeriod; 
    }

    /**
     * getNetPayYTD
     *
     * @param string $techLogin
     * @param string $techPass
     * @param string $startDate
     * @param string $endDate
     * @access public
     * @return $netPayYTD
    */
    public function getNetPayYTD($techLogin, $techPass,$startDate,$endDate)
    {
        require_once 'Core/Api/Class.php';
        require_once 'Core/Api/TechUser.php';
        require_once 'API/Response.php';
        require_once 'API/TechWorkOrder.php';
        require_once 'API/TechWorkOrderFilter.php';
        
        /* Error object */
        $error  = Core_Api_Error::getInstance();

        /* Check authentication */
        $authData = array('login' => $techLogin, 'password' => $techPass);
        $tech = new Core_Api_TechUser;
        $tech->checkAuthentication($authData);
        
        if ( !$tech->isAuthenticate() ) {
            return API_Response::fail();
        }

        $techID = $tech->getTechID();
        /* eyear */
        $eyear = date('Y',strtotime($endDate));
                        
        /* Look up database */
        $db = Core_Database::getInstance();
        $query = $db->select();
        $query->from(Core_Database::TABLE_WORK_ORDERS, new Zend_Db_Expr('SUM(PayAmount_Final)'));/*334*/
        $query->where("Tech_ID = ?", $techID);
        $query->where("Approved = 1");        
        $query->where("Status='completed'");                
        $query->where("DatePaid <= '$endDate'");        
        $query->where("year(DatePaid) = ? ",$eyear);        
        
        $netPayYTD = $db->fetchOne($query);

        if(!isset($netPayYTD)) $netPayYTD=0;
        else $netPayYTD=round($netPayYTD,2); /*334*/
        $netPayForYear = number_format($netPayForYear,2,'.',''); /*334*/
        return $netPayYTD; 
    }    
    /**
     * getGrossPayForPeriod
     *
     * @param string $techLogin
     * @param string $techPass
     * @param string $startDate
     * @param string $endDate
     * @access public
     * @return $grossPayForPeriod
    */
    public function getGrossPayForPeriod($techLogin, $techPass,$startDate,$endDate)
    {
        require_once 'Core/Api/Class.php';
        require_once 'Core/Api/TechUser.php';
        require_once 'API/Response.php';
        require_once 'API/TechWorkOrder.php';
        require_once 'API/TechWorkOrderFilter.php';
        
        /* Error object */
        $error  = Core_Api_Error::getInstance();

        /* Check authentication */
        $authData = array('login' => $techLogin, 'password' => $techPass);
        $tech = new Core_Api_TechUser;
        $tech->checkAuthentication($authData);

        if ( !$tech->isAuthenticate() ) {
            return API_Response::fail();
        }

        $techID = $tech->getTechID();
                
        /* Look up database */
        $db = Core_Database::getInstance();
        $query = $db->select();
        $query->from(Core_Database::TABLE_WORK_ORDERS, new Zend_Db_Expr('SUM(PayAmount)'));
        $query->where("Tech_ID = ?", $techID);
        $query->where("Approved = 1");
        $query->where("Status='completed'");        
        $query->where("DatePaid >= '$startDate'");        
        $query->where("DatePaid <= '$endDate'");        
        
        $grossPayForPeriod = $db->fetchOne($query);

        if(!isset($grossPayForPeriod)) $grossPayForPeriod=0;

        return $grossPayForPeriod; 
    }
    
    /**
     * getNetPayForYear
     *
     * @param string $techLogin
     * @param string $techPass
     * @param int $year
     * @access public
     * @return $netPayForYear
    */
    public function getNetPayForYear($techLogin, $techPass, $year)
    {
        require_once 'Core/Api/Class.php';
        require_once 'Core/Api/TechUser.php';
        require_once 'API/Response.php';
        require_once 'API/TechWorkOrder.php';
        require_once 'API/TechWorkOrderFilter.php';
        
        /* Error object */
        $error  = Core_Api_Error::getInstance();

        /* Check authentication */
        $authData = array('login' => $techLogin, 'password' => $techPass);
        $tech = new Core_Api_TechUser;
        $tech->checkAuthentication($authData);

        if ( !$tech->isAuthenticate() ) {
            return API_Response::fail();
        }

        $techID = $tech->getTechID();
                
        /* Look up database */
        $db = Core_Database::getInstance();
        $query = $db->select();
        //13711,13667 
        $query->from(Core_Database::TABLE_WORK_ORDERS, new Zend_Db_Expr('SUM(IFNULL(PayAmount_Final,PayAmount*(1-(PcntDeductPercent+0))))'));//13711,13667 
        $query->where("Tech_ID = ?", $techID);
        //$query->where("Approved = 1");        
        //$query->where("Status='completed'");  
        // end 13711,13667   
        $query->where("year(DatePaid) = ? ",$year);        		     
        $netPayForYear = $db->fetchOne($query);
        if(!isset($netPayForYear)) $netPayForYear=0;
        else $netPayForYear = round($netPayForYear, 2); /*334*/
        $netPayForYear = number_format($netPayForYear,2,'.',''); /*334*/
        return $netPayForYear; 
    }

    /**
     * getGrossPayForYear
     *
     * @param string $techLogin
     * @param string $techPass
     * @param int $year
     * @access public
     * @return $grossPayForYear
    */    
    public function getGrossPayForYear($techLogin, $techPass, $year)
    {
        require_once 'Core/Api/Class.php';
        require_once 'Core/Api/TechUser.php';
        require_once 'API/Response.php';
        require_once 'API/TechWorkOrder.php';
        require_once 'API/TechWorkOrderFilter.php';
        
        /* Error object */
        $error  = Core_Api_Error::getInstance();

        /* Check authentication */
        $authData = array('login' => $techLogin, 'password' => $techPass);
        $tech = new Core_Api_TechUser;
        $tech->checkAuthentication($authData);

        if ( !$tech->isAuthenticate() ) {
            return API_Response::fail();
        }

        $techID = $tech->getTechID();
                
        /* Look up database */
        $db = Core_Database::getInstance();
        $query = $db->select();
 $query->from(Core_Database::TABLE_WORK_ORDERS, new Zend_Db_Expr('SUM(PayAmount)'));//13711,13667 
        $query->where("Tech_ID = ?", $techID);
        //$query->where("Approved = 1");//13711,13667        
        //$query->where("Status='completed'");//13711,13667 
        $query->where("year(DatePaid) = ? ",$year);        
        
        $grossPayForYear = $db->fetchOne($query);

        if(!isset($grossPayForYear)) $grossPayForYear=0;

        return $grossPayForYear; 
    }
    public function getExts($techId)
    {
        $db = Core_Database::getInstance();
        $query = $db->select();
        $fieldList = array('TechID','PrimaryPhoneStatus','SecondaryPhoneStatus','MyWebSite','I9Status','I9Date','I9TestRequested','I9RequestedDate','I9ResultsEnterBy','GGE1stAttemptStatus','GGE1stAttemptScore','GGE2ndAttemptStatus','GGE2ndAttemptScore');
        $query->from(array('t'=>Core_Database::TABLE_TECH_BANK_INFO),array('FirstName','LastName','PrimaryPhone','SecondaryPhone'));
        $query->joinLeft(array('tx'=>'tech_ext'),
                    't.TechID = tx.TechID',
                    $fieldList
        );
        $query->where("t.TechID = ?", $techId);
        //print_r($query->__toString());die();
        $result = Core_Database::fetchAll($query);
        
        if(!empty($result)) return $result[0];
        else return null;                
    }
    
    public function getProperties($techId)
    {
        $db = Core_Database::getInstance();
        $query = $db->select();
        $query->from(Core_Database::TABLE_TECH_BANK_INFO);
        $query->where("TechID = ?", $techId);
        $result = Core_Database::fetchAll($query);
        if(!empty($result)) return $result[0];
        else return null;        
    }
                   
    public function editMyWebSite($techId,$myWebsite)
    {
        $db = Core_Database::getInstance();  
        $query = $db->select();
        $query->from("tech_ext");
        $query->where("TechID = ?", $techId);
        $techExt = Core_Database::fetchAll($query);        
        if(empty($techExt))
        {            
            $result = Core_Database::insert("tech_ext",
                            array("TechID"=>$techId,
                                "PrimaryPhoneStatus"=>self::PHONE_VALID,
                                "SecondaryPhoneStatus"=>self::PHONE_VALID,
                                "MyWebSite"=>$myWebsite
                                )
                            );
        }
        else
        {
            $criteria = "TechID = '$techId'";
            $result = Core_Database::update("tech_ext",
                                array("MyWebSite"=>$myWebsite
                                ),
                                $criteria                                           
                );            
        }        
    }
    
    public function editExts($techId,$primaryPhoneStatus,$secondPhoneStatus)
    {                                      
        $db = Core_Database::getInstance();  
        $query = $db->select();
        $query->from("tech_ext");
        $query->where("TechID = ?", $techId);
        $techExt = Core_Database::fetchAll($query);
        
        if(empty($techExt))
        {            
            $result = Core_Database::insert("tech_ext",
                            array("TechID"=>$techId,
                                "PrimaryPhoneStatus"=>$primaryPhoneStatus,
                                "SecondaryPhoneStatus"=>$secondPhoneStatus
                                )
                            );
        }
        else
        {
            $criteria = "TechID = '$techId'";
            $result = Core_Database::update("tech_ext",
                                array("PrimaryPhoneStatus"=>$primaryPhoneStatus,
                                      "SecondaryPhoneStatus"=>$secondPhoneStatus
                                ),
                                $criteria                                           
                );
        }       
        return  $result;
    }
    
    public function setPrimaryPhoneInvalid($techId,$reportClientLoginame)
    {                                      
        $this->setPhoneInvalid(self::PRIMARY_PHONE,$techId,$reportClientLoginame);        
        //--- return
        $db = Core_Database::getInstance();  
        $query = $db->select();
        $query->from("tech_ext");
        $query->where("TechID = ?", $techId);
        $techExt1 = Core_Database::fetchAll($query);
                
        return  $techExt1;
    }
    
    public function setSecondaryPhoneInvalid($techId,$reportClientLoginame)
    {                                      
        $this->setPhoneInvalid(self::SECONDARY_PHONE,$techId,$reportClientLoginame);        
        //--- return
        $db = Core_Database::getInstance();  
        $query = $db->select();
        $query->from("tech_ext");
        $query->where("TechID = ?", $techId);
        $techExt1 = Core_Database::fetchAll($query);
        return  $techExt1;
    }
    
    public function setPrimaryPhoneValid($techId,$userType,$loginame)
    {                                      
        $this->setPhoneValid(self::PRIMARY_PHONE,$techId,$userType,$loginame);        
        //--- return
        $db = Core_Database::getInstance();  
        $query = $db->select();
        $query->from("tech_ext");
        $query->where("TechID = ?", $techId);
        $techExt1 = Core_Database::fetchAll($query);                
        return  $techExt1;
    }

    public function setSecondaryPhoneValid($techId,$userType,$loginame)
    {                                      
        $this->setPhoneValid(self::SECONDARY_PHONE,$techId,$userType,$loginame);        
        //--- return
        $db = Core_Database::getInstance();  
        $query = $db->select();
        $query->from("tech_ext");
        $query->where("TechID = ?", $techId);
        $techExt1 = Core_Database::fetchAll($query);                
        return  $techExt1;
    }
    
    public function setPhoneValid($phoneType,$techId,$userType,$loginame) 
    {
        $db = Core_Database::getInstance();  
        //--- technician info.
        $query = $db->select();
        $query->from(Core_Database::TABLE_TECH_BANK_INFO); 
        $query->where("TechID = '$techId'");
        $techInfo = Core_Database::fetchAll($query);
        $techPhoneValue="";$techEmail="";$techName=""; 
        if(!empty($techInfo))
        {
            $techEmail=$techInfo[0]['PrimaryEmail'];
            $techName=$techInfo[0]['FirstName']." ".$techInfo[0]['LastName'];            
            if($phoneType==self::PRIMARY_PHONE) {$techPhoneValue= $techInfo[0]['PrimaryPhone'];}
            if($phoneType==self::SECONDARY_PHONE) {$techPhoneValue= $techInfo[0]['SecondaryPhone'];}                
            
        }            
        //--- technician phone status
        $query = $db->select();
        $query->from("tech_ext");
        $query->where("TechID = ?", $techId);
        $techExt = Core_Database::fetchAll($query);
        
        //--- edit techExt        
        if(empty($techExt))
        {            
            $result = Core_Database::insert("tech_ext",
                    array("TechID"=>$techId,
                        "PrimaryPhoneStatus"=>1,
                        "SecondaryPhoneStatus"=>1
                        )
                    );
        }
        else
        {
            $criteria = "TechID = '$techId'";
            if($phoneType==self::PRIMARY_PHONE)
            {
                $result = Core_Database::update("tech_ext",
                                    array("PrimaryPhoneStatus"=>1
                                    ),
                                    $criteria                                           
                    );
            }
            else
            {
                $result = Core_Database::update("tech_ext",
                                    array("SecondaryPhoneStatus"=>1
                                    ),
                                    $criteria                                           
                    );
            }
        }    
        
        //--- phone audit log
        $auditdate = date('Y-m-d H:i:s');
        
        $reportingClient = empty($clientInfo)?"":$clientInfo[0]['CompanyName'];
        $reportingClientId = empty($clientInfo)?"":$clientInfo[0]['Company_ID'];
        $result = Core_Database::insert("phone_audit_log",
                            array("auditdate"=>$auditdate,
                                "phonetype"=>$phoneType,
                                "techphonevalue"=>$techPhoneValue,
                                "actionid"=>2,
                                "techid"=>$techId,
                                "reportingclient"=>"",
                                "reportingclientid"=>"",
                                "reportinguser"=>$loginame,
                                "reportingusertype"=>$userType
                                )
         );                    
    }
    public function setPhoneInvalid($phoneType,$techId,$reportClientLoginame)
    {
        $db = Core_Database::getInstance();  
        //--- technician info.
        $query = $db->select();
        $query->from(Core_Database::TABLE_TECH_BANK_INFO); 
        $query->where("TechID = '$techId'");
        $techInfo = Core_Database::fetchAll($query);
        $techPhoneValue="";$techEmail="";$techName=""; 
        if(!empty($techInfo))
        {
            $techEmail=$techInfo[0]['PrimaryEmail'];
            $techName=$techInfo[0]['FirstName']." ".$techInfo[0]['LastName'];            
            if($phoneType==self::PRIMARY_PHONE) {$techPhoneValue= $techInfo[0]['PrimaryPhone'];}
            if($phoneType==self::SECONDARY_PHONE) {$techPhoneValue= $techInfo[0]['SecondaryPhone'];}                
            
        }            
        //---  client info
        $query = $db->select();
        $query->from("clients"); 
        $query->where("UserName = ?", $reportClientLoginame);
        $clientInfo = Core_Database::fetchAll($query);
                
        //--- technician phone status
        $query = $db->select();
        $query->from("tech_ext");
        $query->where("TechID = ?", $techId);
        $techExt = Core_Database::fetchAll($query);
         
        //--- edit techExt        
        if(empty($techExt))
        {            
            if($phoneType==self::PRIMARY_PHONE)
            {
                    $result = Core_Database::insert("tech_ext",
                            array("TechID"=>$techId,
                                "PrimaryPhoneStatus"=>2,
                                "SecondaryPhoneStatus"=>1
                                )
                            );
            }
            else
            {
                    $result = Core_Database::insert("tech_ext",
                            array("TechID"=>$techId,
                                "PrimaryPhoneStatus"=>1,
                                "SecondaryPhoneStatus"=>2
                                )
                            );                
            }
        }
        else
        {
            $criteria = "TechID = '$techId'";
            if($phoneType==self::PRIMARY_PHONE)
            {
                $result = Core_Database::update("tech_ext",
                                    array("PrimaryPhoneStatus"=>2
                                    ),
                                    $criteria                                           
                    );
            }
            else
            {
                $result = Core_Database::update("tech_ext",
                                    array("SecondaryPhoneStatus"=>2
                                    ),
                                    $criteria                                           
                    );
            }
        }       
        //--- phone audit log
        $auditdate = date('Y-m-d H:i:s');
        
        $reportingClient = empty($clientInfo)?"":$clientInfo[0]['CompanyName'];
        $reportingClientId = empty($clientInfo)?"":$clientInfo[0]['Company_ID'];
        $result = Core_Database::insert("phone_audit_log",
                            array("auditdate"=>$auditdate,
                                "phonetype"=>$phoneType,
                                "techphonevalue"=>$techPhoneValue,
                                "actionid"=>1,
                                "techid"=>$techId,
                                "reportingclient"=>$reportingClient,
                                "reportingclientid"=>$reportingClientId,
                                "reportinguser"=>$reportClientLoginame,
                                "reportingusertype"=>1                                
                                )
         ); 
         //--- send email to technician
         //$techEmail='duongha2k@yahoo.com';//test
         $this->sendNotificationMailForPhoneInvalid_ToTech($phoneType,$techId
                    ,$techEmail,$techName,$techPhoneValue);
                    
         //--- send email to support
         $this->sendNotificationMailForPhoneInvalid_ToSupport($phoneType,$techId
                    ,$techEmail,$techName,$techPhoneValue);
           
    }
    
    public function sendNotificationMailForPhoneInvalid_ToTech($phoneType
            ,$techId,$techEmail,$techName,$techPhoneNumber)
    {
        $toEmail = $techEmail;
        $fromName = "Support@fieldsolutions.com";
        $fromEmail = "support@fieldsolutions.com";
        $phoneTypeText = "";
        if($phoneType==self::PRIMARY_PHONE)
        {
            $phoneTypeText = "primary";
        }
        else if($phoneType==self::SECONDARY_PHONE)
        {
            $phoneTypeText = "secondary";
        }
        $subject = "Invalid $phoneTypeText phone number reported.";
        $body = "
Dear $techName, 

Your $phoneTypeText phone number $techPhoneNumber  has been reported as invalid.  
Please confirm your phone number and update your profile as needed to ensure clients can contact you about future work. 

Thank you.
FieldSolutions Support Team
support@fieldsolutions.com 
        ";
        $mail = new Core_Mail();
        $mail->setBodyText($body);
        $mail->setFromName($fromName);
        $mail->setFromEmail($fromEmail);
        $mail->setToEmail($toEmail);
        $mail->setSubject($subject);
        $mail->send();
    }
    
    public function sendNotificationMailForPhoneInvalid_ToSupport($phoneType
            ,$techId,$techEmail,$techName,$techPhoneNumber)
    {
        $toEmail = "support@fieldsolutions.com";//$techEmail;
        $fromName = "no-replies@fieldsolutions.com";
        $fromEmail = "no-replies@fieldsolutions.com";
        $phoneTypeText = "";
        if($phoneType==self::PRIMARY_PHONE)
        {
            $phoneTypeText = "primary";
        }
        else if($phoneType==self::SECONDARY_PHONE)
        {
            $phoneTypeText = "secondary";
        }
        $subject = "Invalid $phoneTypeText phone number reported for Tech ID #$techId.";
        $body = "

The $phoneTypeText phone number $techPhoneNumber  has been reported as invalid for Tech #$techId, $techName.  

Thank you.
FieldSolutions Support Team
support@fieldsolutions.com 
        ";
        $mail = new Core_Mail();
        $mail->setBodyText($body);
        $mail->setFromName($fromName);
        $mail->setFromEmail($fromEmail);
        $mail->setToEmail($toEmail);
        $mail->setSubject($subject);
        $mail->send();
    }
    
   
    public function getPhoneAuditLog($arrayFilter,$sort="",$limit=0,$offset=0)
    {
        $db = Core_Database::getInstance();  
        $query = $db->select();
        $query->from(array('p'=>'phone_audit_log')
                    ,array('auditdate','techphonevalue','phonetype','techid','reportingclientid','reportingclient','reportinguser'));
        $query->join(array('t'=>Core_Database::TABLE_TECH_BANK_INFO)
                        ,'t.TechID = p.techid'
                        ,array('FirstName','LastName','PrimaryPhone','SecondaryPhone')); 
        $query->join(array('tx'=>'tech_ext')
                        ,'tx.TechID = p.techid'
                        ,array('PrimaryPhoneStatus','SecondaryPhoneStatus','CurrentStatus'=>'(CASE p.phonetype WHEN 1 THEN tx.PrimaryPhoneStatus ELSE tx.SecondaryPhoneStatus END)')); 
        $query->where("actionid=1");
        
        if(!empty($arrayFilter))
        {
            foreach($arrayFilter as $key => $value) 
            {                                
                if($key=='ReportedStartDate' && !empty($value))
                {
                    $value .= ' 00:00:00';
                    $query->where("auditdate >= '$value'");                                
                }                
                else if($key=='ReportedEndDate' && !empty($value))
                {
                    $value .= ' 23:59:59';
                    $query->where("auditdate <= '$value'");                                                
                }
                else if($key=='IncludeValidatedPhone')
                {
                    if($value==0)
                    {
                        $query->where("(p.phonetype=1 AND p.techphonevalue=t.PrimaryPhone AND PrimaryPhoneStatus=2) OR (p.phonetype=2 AND p.techphonevalue=t.SecondaryPhone AND SecondaryPhoneStatus=2)"); 
                    }
                    //$query->where("auditdate <= '$value'");                                                
                }                
                else if($key=='PhoneNumber' && !empty($value))
                {
                    $query->where("techphonevalue = '$value'");                                                
                }
                else if($key=='PhoneType' && !empty($value))
                {
                    if($value > 0) $query->where("phonetype = ?",$value); 
                }
                else if($key=='PhoneNumber' && !empty($value))
                {
                    $query->where("techphonevalue = '$value'");                                                
                }
                else if($key=='TechID' && !empty($value))
                {
                    $query->where("p.techid = ?",$value);                                                
                }
                else if($key=='TechLastName' && !empty($value))
                {
                    $query->where("LastName = '$value'");                                                
                }
                else if($key=='TechFisrtName' && !empty($value))
                {
                    $query->where("FirstName = '$value'");                                                
                }
                else if($key=='ReportingClientId' && !empty($value))
                {
                    $query->where("reportingclientid = '$value'");                                                
                }
                else if($key=='ReportingUser' && !empty($value))
                {
                    $query->where("reportinguser = '$value'");                                                
                }   
            }
        }
        if(!empty($sort)){
            $query->order($sort);
        }
        if($limit>0)
        {
            $query->limit($limit, $offset);
        }
        
        $result = Core_Database::fetchAll($query);      
        return $result;        
    }
    
    public function getTechIdsByI9Status($i9Status)
    {
        $db = Core_Database::getInstance();  
        $query = $db->select();
        $query->from("tech_ext",array("TechID"));
        $query->where("I9Status = ?", $i9Status);        
        $techExt = Core_Database::fetchAll($query);      
        $techIds="";
        if(!empty($techExt))
        {
            for($i=0;$i<count($techExt);$i++)
            {
               if($i==0) {$techIds.=$techExt[$i]['TechID'];}
               else {$techIds.=','.$techExt[$i]['TechID'];}
            }
        }  
        return  $techIds;       
    }
    public function getTechIdsByUSAuthorized($greaterDate,$lessDate)
    {
        $db = Core_Database::getInstance();  
        $query = $db->select();
        $query->from("tech_ext",array("TechID"));
        $query->where("I9Status = 2");        
        if(!empty($greaterDate)){
           $query->where("I9Date >='$greaterDate'");         
        }
        if(!empty($lessDate)){
           $query->where("I9Date <='$lessDate'");         
        }
        $techExt = Core_Database::fetchAll($query); 
        $techIds="";
        if(!empty($techExt))
        {
            for($i=0;$i<count($techExt);$i++)
            {
               if($i==0) {$techIds.=$techExt[$i]['TechID'];}
               else {$techIds.=','.$techExt[$i]['TechID'];}
            }
        }  
        return  $techIds;       
    }
    public function getTechIdsWithUSAuthorizedOrPending()
    {
        $db = Core_Database::getInstance();  
        $query = $db->select();
        $query->from("tech_ext",array("TechID"));
        $query->where("I9Status=1 OR I9Status=2");        
        $techExt = Core_Database::fetchAll($query);      
        $techIds="";
        if(!empty($techExt))
        {
            for($i=0;$i<count($techExt);$i++)
            {
               if($i==0) {$techIds.=$techExt[$i]['TechID'];}
               else {$techIds.=','.$techExt[$i]['TechID'];}
            }
        }  
        return  $techIds;       
    }
    public function editI9($techId,$I9Status,$I9Date=null
            ,$I9TestRequested=0,$I9RequestedDate=null,$I9ResultsEnterBy='')
    {
        $db = Core_Database::getInstance();  
        $query = $db->select();
        $query->from("tech_ext");
        $query->where("TechID = ?", $techId);
        $techExt = Core_Database::fetchAll($query);        
        if(empty($techExt))
        {      
            $result = Core_Database::insert("tech_ext",
                        array("TechID"=>$techId
                            ,"PrimaryPhoneStatus"=>self::PHONE_VALID
                            ,"SecondaryPhoneStatus"=>self::PHONE_VALID
                            ,"MyWebSite"=>''
                            ,"I9Status"=>$I9Status
                            ,"I9Date"=>$I9Date
                            ,"I9TestRequested"=>$I9TestRequested
                            ,"I9RequestedDate"=>$I9RequestedDate
                            ,"I9ResultsEnterBy"=>$I9ResultsEnterBy
                            )
                        );
        }
        else
        {
            $criteria = "TechID = '$techId'";
            $result = Core_Database::update("tech_ext",
                            array("I9Status"=>$I9Status
                                ,"I9Date"=>$I9Date
                                ,"I9TestRequested"=>$I9TestRequested
                                ,"I9RequestedDate"=>$I9RequestedDate
                                ,"I9ResultsEnterBy"=>$I9ResultsEnterBy                                
                            ),
                            $criteria                                           
                    );            
        }        
    }
    
     public function approvePhoto($techId)
    {
           //select * from techfiles where techID=15527 and fileType='Profile Pic'
        $criteria = "techID = '$techId' AND fileType='Profile Pic'";
        $result = Core_Database::update(Core_Database::TABLE_TECH_FILES,
                                array("approved"=>1),
                                $criteria                                           
                );
    }
    
    public function approveBatchPhoto($uploadedDateGreater=null,$uploadedDateLess=null,$pending=1)
    {
        $db = Core_Database::getInstance();  
        $query = $db->select();
        $query->from(Core_Database::TABLE_TECH_FILES);
        $query->where("fileType = 'Profile Pic'");        
        if(!empty($uploadedDateGreater))
                $query->where("dateUpdated >= '$uploadedDateGreater'");        
        if(!empty($uploadedDateLess))
                $query->where("dateUpdated <= '$uploadedDateLess'");        
        if($pending==1)
                $query->where("approved <>1");        
        if ( $offset != 0 || $limit != 0)
                $query->limit($limit, $offset);                          
        $techFiles = Core_Database::fetchAll($query);
        
        if(!empty($techFiles))
        {      
            $techIds="";      
            for($i=0;$i<count($techFiles);$i++)
            {
                $techId=$techFiles[$i]["techID"];           
                if($i==0) $techIds = $techId;
                else $techIds.=",".$techId;                     
            }
            $criteria = "techID IN ($techIds) AND fileType='Profile Pic'";
            $result = Core_Database::update(Core_Database::TABLE_TECH_FILES,
                                array("approved"=>1),
                                $criteria                                           
                );                
        }
    }
    
    public function getPhotoRecord($techId) 
    {
        $db = Core_Database::getInstance();  
        $query = $db->select();
        $query->from(Core_Database::TABLE_TECH_FILES);
        $query->where("techID = ?", $techId);
        $query->where("fileType = 'Profile Pic'");        
        $techFile = Core_Database::fetchAll($query);
        if(!empty($techFile)) return  $techFile[0];
        else return null;        
    }
    
    public function searchPhotos($uploadedDateGreater=null,
            $uploadedDateLess=null,$pending=1,$offset=0, $limit=0) 
    {
        
        //echo("<br/>uploadedDateGreater: ".$uploadedDateGreater);
        //echo("<br/>uploadedDateLess: ".$uploadedDateLess);
        $db = Core_Database::getInstance();  
        $query = $db->select();
        $query->from(Core_Database::TABLE_TECH_FILES);
        $query->where("fileType = 'Profile Pic'");  
        if($pending==1)
                $query->where("approved <>1");        
        
        if(!empty($uploadedDateGreater))
        {
            $query->where("dateUpdated >= '$uploadedDateGreater'");        
        }
        if(!empty($uploadedDateLess))
        {                                  
            $query->where("dateUpdated < '$uploadedDateLess 23:59:59'");  
        }
        $query->order("techID asc");
                
        if ( $offset != 0 || $limit != 0)
                $query->limit($limit, $offset);
                
                          
        $techFiles = Core_Database::fetchAll($query);
        return  $techFiles;
        //print_r($techFiles);die();
    }
    
    public function searchPhotos_Total($uploadedDateGreater=null,
            $uploadedDateLess=null,$pending=1) 
    {
        $db = Core_Database::getInstance();  
        $query = $db->select();
        $query->from(Core_Database::TABLE_TECH_FILES, new Zend_Db_Expr('COUNT(*)'));
        $query->where("fileType = 'Profile Pic'");        
        if($pending==1)
                $query->where("approved <>1");        
        if(!empty($uploadedDateGreater))
        {
                $query->where("dateUpdated >= '$uploadedDateGreater'");        
        }
        if(!empty($uploadedDateLess))
        {
            $query->where("dateUpdated < '$uploadedDateLess 23:59:59'");  
        }
                        
        $techCount = $db->fetchOne($query);
        if(!isset($techCount)) $techCount=0;
        return $techCount;                         
    }
    
     public function getNumOfBidsByTechAndCompanyAndProject($techId,
            $companyId,$projectId)
    {
        
        //--- select workorders
        //select * from work_orders  where Project_ID = 3730
        $WO_STATE_PUBLISHED = WO_STATE_PUBLISHED;
        $db = Core_Database::getInstance();
        $query = $db->select();
        $query->from(Core_Database::TABLE_WORK_ORDERS)
            ->where("Project_ID = ?",$projectId)
            ->where("Status='$WO_STATE_PUBLISHED'");
        $workOrders = Core_Database::fetchAll($query); 
        if(empty($workOrders)) return 0;
        //--- $wo_ids    
        $wo_ids = array();
        foreach($workOrders as $wo) $wo_ids[] = $wo['WIN_NUM'];  
        //--- count
        $s = $db->select()
                ->from(Core_Database::TABLE_WORK_ORDER_BIDS, array('c'=>'COUNT(id)'))
                ->where(Core_Database::TABLE_WORK_ORDER_BIDS.'.WorkOrderID IN (?)', $wo_ids)
                ->where(Core_Database::TABLE_WORK_ORDER_BIDS.'.Company_ID = ?', $companyId)
                ->where(Core_Database::TABLE_WORK_ORDER_BIDS.'.TechID = ?', $techId);
        $res = Core_Database::fetchAll($s); 
        $count=0;
        if(!empty($res))
        {
              $count = $res[0]['c'];     
        }    
        return $count;
    }
    
    public function getWOs_BidsByTechAndCompanyAndProject($techId,
            $companyId,$projectId)
    {  
		//-- Temp fix for fls bid box issue
        if($projectId == NULL || $projectId == "")return null;
    	
        //--- select workorders
        $WO_STATE_PUBLISHED = WO_STATE_PUBLISHED;
        $db = Core_Database::getInstance();
        $query = $db->select();
        $query->from(Core_Database::TABLE_WORK_ORDERS);
        if(is_array($projectId)){
            $query->where("Project_ID IN(?)", $projectId);
        }else{
        	$query->where("Project_ID = ?",$projectId);
        }
        $query->where("Status='$WO_STATE_PUBLISHED'");
        $workOrders = Core_Database::fetchAll($query); 
        if(empty($workOrders)) return "";
        //--- $wo_ids    
        $wo_ids = array();
        foreach($workOrders as $wo) $wo_ids[] = $wo['WIN_NUM'];  
        //--- count
        $s = $db->select()
                ->from(Core_Database::TABLE_WORK_ORDER_BIDS)
                ->where(Core_Database::TABLE_WORK_ORDER_BIDS.'.WorkOrderID IN (?)', $wo_ids)
                ->where(Core_Database::TABLE_WORK_ORDER_BIDS.'.Company_ID = ?', $companyId)
                ->where(Core_Database::TABLE_WORK_ORDER_BIDS.'.TechID = ?', $techId);
        $res = Core_Database::fetchAll($s); 
        $WOs="";
        if(!empty($res))
        {
            for($i=0;$i<count($res);$i++)
            {
                if($i>0) {$WOs.= ',';}
                $WOs.=$res[$i]['WorkOrderID'];                
            }
        }    
        return $WOs;
    }
    
    public function getProjectIdByWinNum($winnum)
    {
        $db = Core_Database::getInstance();
        $query = $db->select();
        $query->from(Core_Database::TABLE_WORK_ORDERS)
            ->where("WIN_NUM = ?",$winnum);
        $workOrders = Core_Database::fetchAll($query);  
        
        $projectId=0;   
        if(!empty($workOrders))
        {
            $projectId =  $workOrders[0]['Project_ID'];
        }
        return $projectId;            
    }
    
    public function getDeVryTechIdList()
    {
        $DeVry_CertId = 2;  
        $db = Core_Database::getInstance();
        $query = $db->select();        
        $query->from(Core_Database::TABLE_TECH_CERTIFICATION);                
        $query->where("certification_id = ?", $DeVry_CertId);        
        $result = Core_Database::fetchAll($query);
        
        $idList = "";
        if(!empty($result))
        {
            for($i=0;$i<count($result);$i++)
            {
                if($i > 0) $idList .=',';
                
                $idList .= $result[$i]['TechID'];
            }
            return $idList;
        }
        else return $idList;
    }
    
    public function updateTMC($techId,$TMCStatus,$TMCDate)
    {
        $TMC_CertId = 5;  
        $techCert = new Core_TechCertification($techId,$TMC_CertId,null,$TMCDate,null);
        if($TMCStatus==0)
        {
            $techCert->delete();
        }
        else
        {
            $techCert->save();                        
        }
    }
    public function getTechIdsWithGenaralCertified($TMC_ID)
    {
            $db = Core_Database::getInstance();  
            $query = $db->select();
            $query->from(Core_Database::TABLE_TECH_CERTIFICATION,array("TechID"));
            $query->where("certification_id in ($TMC_ID)");        
            $techs = Core_Database::fetchAll($query);      
    
            $techIds="";
            if(!empty($techs))
            {
                    for($i=0;$i<count($techs);$i++)
                    {
                        if($i==0) {$techIds.=$techs[$i]['TechID'];}
                        else {$techIds.=','.$techs[$i]['TechID'];}

                    }
            }  

            return  $techIds;       
    }
    public function getTechIdsWithTrapolloMedicalCertified()
    {
			$TMC_ID = 5;
			$db = Core_Database::getInstance();  
			$query = $db->select();
			$query->from(Core_Database::TABLE_TECH_CERTIFICATION,array("TechID"));
			$query->where("certification_id=$TMC_ID");        
			$techs = Core_Database::fetchAll($query);      
			
			$techIds="";
			if(!empty($techs))
			{
				for($i=0;$i<count($techs);$i++)
				{
				   if($i==0) {$techIds.=$techs[$i]['TechID'];}
				   else {$techIds.=','.$techs[$i]['TechID'];}
				}
			}  
			
			return  $techIds;       
	}
		
	
    public static function getTechIdsArray_ByI9Status($i9Status)
    {
        $db = Core_Database::getInstance();
        $query = $db->select();
        $query->from('tech_ext');
        $query->where("I9Status = ?",$i9Status);
        $techs = Core_Database::fetchAll($query);
        if(empty($techs)) return null;
        $result = array();
        foreach ($techs as $t) {
            $result[] = $t['TechID'];
        }
        return $result;        
    }
    public static function getI9VerifiedTechIdsArray()
    {
        return Core_Api_TechClass::getTechIdsArray_ByI9Status(2);
    }
    public static function getI9PendingTechIdsArray()
    {
        return Core_Api_TechClass::getTechIdsArray_ByI9Status(1);
    }        
    
    /*** for assessment ***/
    public function getQuestions($assId)
    {
        $db = Core_Database::getInstance();
        $query = $db->select();
        $query->from('ass_questions');
        $query->where("ass_id = ?",$assId);
        $query->order("presentation_order");
        $records = Core_Database::fetchAll($query);
        return $records;
}
    public function getItemsOfQuestion($questionId)
    {
        $db = Core_Database::getInstance();
        $query = $db->select();
        $query->from('ass_question_items');
        $query->where("ass_question_id = ?",$questionId);
        $query->order("presentation_order");
        $records = Core_Database::fetchAll($query);
        return $records;
    }
    public function getItemsOfQuestionWithTech($techId,$questionId)
    {
        $db = Core_Database::getInstance();
        $query = $db->select();
        $query->from('tech_ass_answers');
        $query->where("tech_id = ?",$techId);
        $query->where("ass_question_id = ?",$questionId);        
        $records = Core_Database::fetchAll($query);
        return $records;
    }
    
    //--- saveSelectedItems
    // $questionItemIds: array(key=>value): key: questionId, value: array(selectedItem1Id,selectedItem2Id) 
    public function saveSelectedItems($techId,$assId,$questionItemIds)
    {
        //echo("<pre>");print_r($questionItemIds);
        //--- insert into tech_ass if not exists
        $db = Core_Database::getInstance();
        $query = $db->select();
        $query->from('tech_ass');
        $query->where("tech_id=?",$techId);
        $query->where("ass_id=?",$assId);
        $records = Core_Database::fetchAll($query);
        $ispassed=0;
        if(empty($records))
        {            
            $result = Core_Database::insert("tech_ass",
                            array("tech_id"=>$techId,
                                "ass_id"=>$assId,                                
                                "ispassed"=>0,
                                "tryingtimes"=>0
                                )
                            );
        }        
        else
        {
            $ispassed=$records[0]['ispassed'];
        }
        
        //--- get current info
        $techAssessment = $this->getTechAssessment($techId,$assId);
        $tryingtimes = $techAssessment['tryingtimes'];
        
        //--- insert into tech_ass_answers
                    /*tech_id
                ass_question_id
                ass_question_item_id
                date_taken*/
        //$questions for $assId
         $db = Core_Database::getInstance();
         $query = $db->select();
         $query->from('ass_questions');
         $query->where("ass_id = ?",$assId);
         $questions = Core_Database::fetchAll($query);
         //echo("questions: <pre>");print_r($questions);
         //echo("questionItemIds: <pre>");print_r($questionItemIds);
         if(!empty($questions) && !empty($questionItemIds))
         {
             $this->delete_Tech_ass_answers($techId);
             for($i=0;$i<count($questions);$i++)
             {
                 $questionId = $questions[$i]['id'];                 
                 $arrItemIds = $questionItemIds[$questionId];                 
                 if(!empty($arrItemIds))
                 {      
                     //echo("questionId:$questionId ,arrItemIds: <pre>");print_r($arrItemIds);                                
                     //insert item for this technician and this question
                     foreach($arrItemIds as $itemId)
                     {
                         $result = Core_Database::insert("tech_ass_answers",
                            array("tech_id"=>$techId,
                                "ass_question_id"=>$questionId,                                
                                "ass_question_item_id"=>$itemId,
                                "date_taken"=>date('Y-m-d')
                                )
                            );
                     }
                 }
             }
         }                
        //--- calculate score
        $score = $this->getScore($techId,$assId);
        $passingScore = $this->getPassingScore($assId);
        $numOfScoredQuestions = $this->getNumOfScoredQuestions($assId);

        //--- update tech_ass
        if($score >= $passingScore)
        {
            $answerkeys="";
            foreach($questionItemIds as $key=>$value)
            {
                if(!empty($value))
                {
                    foreach($value as $itemId)
                    {
                        if(!empty($answerkeys)) $answerkeys .= ",";
                        
                        $answerkeys .= $itemId;
                    }
                }                
            }
            $data = array('ispassed'=>1,
                        'passed_date'=>date('Y-m-d'),
                        'passed_score'=>$score,
                        'last_score'=>$score,
                        'passed_answerkeys'=>$answerkeys,
                        'tryingtimes'=> $tryingtimes + 1                        
            );
            $criteria = 'tech_id = '.$techId.' AND ass_id = '.$assId;
            $db->update('tech_ass',$data,$criteria);            
        }
        else
        {
            //---
            $data = array('last_score'=>$score,
                         'tryingtimes'=> $tryingtimes + 1 
            );                        
            $criteria = 'tech_id = '.$techId.' AND ass_id = '.$assId;
            $db->update('tech_ass',$data,$criteria);     
            //--- delete answers
            //$this->delete_Tech_ass_answers($techId);                   
            $this->delete_Tech_ass_answers_exceptSurvey($techId,$assId);
        }
    }
    public function getSurveyQuestionIds($assId)
    {
        $ids="";
        if($assId==1){$ids="1,2,3,4";}
        return $ids;
    }
    
    public function delete_Tech_ass_answers($techId)
    {
        $db = Core_Database::getInstance();
        $db->delete('tech_ass_answers',"tech_id='$techId'");
    }
    public function delete_Tech_ass_answers_exceptSurvey($techId,$assId)
    {
        $surveyQuestionId = $this->getSurveyQuestionIds($assId);
        $db = Core_Database::getInstance();
        if(!empty($surveyQuestionId))
        {
            $db->delete('tech_ass_answers',"tech_id='$techId' and ass_question_id not in ($surveyQuestionId)");
        }
        else
        {
            $db->delete('tech_ass_answers',"tech_id='$techId'");
        }
        
    }
    
    public function getScore($techId,$assId)
    {
        $db = Core_Database::getInstance();
        $query = $db->select();
        $query->from(array('tas'=>'tech_ass_answers'),
                    array( 
                        'score'=>'sum(aqi.is_answer_key)'
                    )
        );
        $query->join(array('aqi'=>'ass_question_items'),
                        'tas.ass_question_item_id=aqi.id',
                        array()
        );
        $query->join(array('aq'=>'ass_questions'),
                        'aq.id = aqi.ass_question_id',
                        array()
        );
        $query->where("tas.tech_id='$techId'");        
        $query->where("aq.ass_id='$assId'");        
        $records = Core_Database::fetchAll($query);
        $score=0;
        if(!empty($records)) $score = $records[0]['score'];
        return $score;                
    }
    
    public function getPassingScore($assId)
    {
        $db = Core_Database::getInstance();
        $query = $db->select();
        $query->from('assessments');
        $query->where("id = ?",$assId);
        $records = Core_Database::fetchAll($query);
        $passingScore=0;
        if(!empty($records)) $passingScore = $records[0]['passing_score'];
        return $passingScore;                        
    }
    

    public function getNumOfScoredQuestions($assId)
    {
        /*select Count(*) scored_num from ass_questions
where isscored=1
*/
        $db = Core_Database::getInstance();
        $query = $db->select();
        $query->from('ass_questions',
                    array('scored_num'=>'Count(*)')
                );
        $query->where("isscored=1 and ass_id=$assId");
        $records = Core_Database::fetchAll($query);
        $num=0;
        if(!empty($records)) $num = $records[0]['scored_num'];
        return $num;                        
    }
    
    public function getTechAssessment($techId,$assId)
    {
        $db = Core_Database::getInstance();
        $query = $db->select();
        $query->from('tech_ass');
        $query->where("tech_id=?",$techId);
        $query->where("ass_id=?",$assId);
        $records = Core_Database::fetchAll($query);
        if(!empty($records)) return $records[0];
        else return null;
    }
    
    public function getAnsweredItems($techId,$questionId)
    {
        $db = Core_Database::getInstance();
        $query = $db->select();
        $query->from(array('tas'=>'tech_ass_answers'),
                    array( 'item_descr'=>'aqitm.descr',
                           'item_id' => 'tas.ass_question_item_id'
                    )
        );
        $query->join(array('aqitm'=>'ass_question_items'),
                        'aqitm.id = tas.ass_question_item_id',
                        array()
        );
        $query->where("tas.tech_id='$techId'");        
        $query->where("tas.ass_question_id='$questionId'");        
        $records = Core_Database::fetchAll($query);
        return $records;
    }
    
    //$status = 1: PASS, 0: FAIL
    public function getTechIds_AssessmentStatus($assId,$status,$greaterDate=null,$lessDate=null)
    {
        $db = Core_Database::getInstance();  
        $query = $db->select();
        $query->from("tech_ass",array("tech_id"));
        $query->where("ispassed = $status");    
        $query->where("ass_id = $assId");            
        if(!empty($greaterDate)){
           $query->where("passed_date >='$greaterDate'");         
        }
        if(!empty($lessDate)){
           $query->where("passed_date <='$lessDate'");         
        }
        $techs = Core_Database::fetchAll($query); 
        $techIds="";
        if(!empty($techs))
        {
            for($i=0;$i<count($techs);$i++)
            {
               if($i==0) {$techIds.=$techs[$i]['tech_id'];}
               else {$techIds.=','.$techs[$i]['tech_id'];}
            }
        }  
        return  $techIds;       
    }
    public function getTechIds_Assessment($assId,$greaterDate=null,$lessDate=null)
    {
        $db = Core_Database::getInstance();  
        $query = $db->select();
        $query->from("tech_ass",array("tech_id"));
        $query->where("ass_id = $assId");            
        if(!empty($greaterDate)){
           $query->where("passed_date >='$greaterDate'");         
        }
        if(!empty($lessDate)){
           $query->where("passed_date <='$lessDate'");         
        }
        $techs = Core_Database::fetchAll($query); 
        $techIds="";
        if(!empty($techs))
        {
            for($i=0;$i<count($techs);$i++)
            {
               if($i==0) {$techIds.=$techs[$i]['tech_id'];}
               else {$techIds.=','.$techs[$i]['tech_id'];}
            }
        }  
        return  $techIds;       
    }
    
    public function getTechIds_PassCopierSkills($greaterDate=null,$lessDate=null)
    {
        $techIds =  $this->getTechIds_AssessmentStatus(1,1,$greaterDate,$lessDate);
        return  $techIds;       
    }
    public function getTechIds_FailCopierSkills()
    {
        $techIds =  $this->getTechIds_AssessmentStatus(1,0);
        return  $techIds;       
    }
    
    public function getTechIds_CopierSkills()
    {
        $techIds =  $this->getTechIds_Assessment(1,null,null);
        return  $techIds;       
    }
    
    /*** assessment end ***/
    
    /*** EMC certifications ***/
    public function getEMCList()
    {
        $db = Core_Database::getInstance();  
        $query = $db->select();
        $query->from("certifications");
        $query->where("groupid = 'EMC'");            
        $records = Core_Database::fetchAll($query); 
        return $records;
    }
    public function getEMCListExcNames($excNames)
    {
        $db = Core_Database::getInstance();  
        $query = $db->select();
        $query->from("certifications");
        $query->where("groupid = 'EMC'");    
        if(!empty($excNames))        
        {
            $query->where("`name` NOT IN ($excNames)");
        }
        $records = Core_Database::fetchAll($query); 
        return $records;
    }
    public function saveTechCertification($techId,$cerName,$cerDate)
    {
        //--- filter
        if(empty($cerName)) return;
        if(empty($techId)) return;
        //--- cerId
        $db = Core_Database::getInstance();  
        $query = $db->select();
        $query->from("certifications");
        $query->where("name = '$cerName'");            
        $records = Core_Database::fetchAll($query); 
        if(empty($records)) return;
        
        $cerId = $records[0]['id'];
        //--- insert/update
        $query = $db->select();
        $query->from("tech_certification");
        $query->where("TechID = '$techId'"); 
        $query->where("certification_id = '$cerId'"); 
        $tcRecords = Core_Database::fetchAll($query);    
        if(empty($tcRecords))  
        {
            $result = Core_Database::insert("tech_certification",
                            array("TechID"=>$techId,
                                "certification_id"=>$cerId,
                                "date"=>$cerDate
                                )
                            );
        }   
        else
        {
            $criteria = "TechID = '$techId' AND certification_id = '$cerId'";
            $result = Core_Database::update("tech_certification",
                                array("date"=>$cerDate),
                                $criteria                                           
                );                        
        }   
    }
    
    public function selectOnlyEMCNames($techId,$EMCNames)
    {
        if(empty($EMCNames)) return;
        $db = Core_Database::getInstance();  
        $query = $db->select();
        $query->from("certifications");
        $query->where("groupid = 'EMC'");    
        if(!empty($EMCNames))        
        {
            $query->where("(`name` NOT IN ($EMCNames))");
        }
        $excRecords = Core_Database::fetchAll($query); 

        $db = Core_Database::getInstance();        
        if(empty($excRecords)) return;
        $excCertIds='0';
        foreach($excRecords as $k=>$v)
        {
            $cerId = $v['id'];
            $excCertIds .=','.$v['id'];

        }
        $db->delete('tech_certification',"TechID='$techId' AND certification_id IN ($excCertIds)");
    }
    
    public function getEMCListForTechnician($techId)
    {
        /*select tc.TechID,c.* from tech_certification tc
inner join certifications c on c.id=tc.certification_id
where tc.TechID='15527' AND c.groupid='EMC';
*/
        $db = Core_Database::getInstance();  
        $query = $db->select();
        $query->from(array("tc"=>"tech_certification"),array('TechID','date'));
        $query->join(array("c"=>"certifications"),
                    'c.id=tc.certification_id',
                    array('*')
                );
        $query->where("tc.TechID = '$techId'");    
        $query->where("c.groupid = 'EMC'");    
        $records = Core_Database::fetchAll($query); 
        return $records;
    }    
    
    public function getTechIdsForCertification($cerName,$greaterDate,$lessDate)
    {
        //--- filter
        if(empty($cerName)) return;
        //--- cerId
        $db = Core_Database::getInstance();  
        $query = $db->select();
        $query->from("certifications");
        $query->where("name = '$cerName'");            
        $records = Core_Database::fetchAll($query); 
        if(empty($records)) return '';
        
        $cerId = $records[0]['id'];
        //---
        $query = $db->select();
        $query->from("tech_certification");
        $query->where("certification_id='$cerId'");
        if(!empty($greaterDate))
        {
            $query->where("date >= '$greaterDate'");
        }
        if(!empty($lessDate))
        {
            $query->where("date <= '$lessDate'");
        }
        $tcRecords = Core_Database::fetchAll($query); 
        if(empty($tcRecords)) return '0';
        else
        {
            $techIds='';
            foreach($tcRecords as $k=>$v)
            {
                $techIds.= empty($techIds)? $v['TechID']:','.$v['TechID'];
            }
            return $techIds;
        }                
    }
    /*** EMC certifications end ***/
    /*** VonagePlus certifications ***/
    public function getTechIdsWithVOP()
    {
            $db = Core_Database::getInstance();  
            $query = $db->select();
            $query->from(Core_Database::TABLE_TECH_CERTIFICATION,array("TechID"));
            $query->where("certification_id=".self::VOP_CERT_ID);        
            $techs = Core_Database::fetchAll($query);      
            
            $techIds="";
            if(!empty($techs))
            {
                for($i=0;$i<count($techs);$i++)
                {
                   if($i==0) {$techIds.=$techs[$i]['TechID'];}
                   else {$techIds.=','.$techs[$i]['TechID'];}
                }
            }              
            return  $techIds;       
    }

    public function VOP_Exists($techId)
    {
            $db = Core_Database::getInstance();  
            $query = $db->select();
            $query->from(Core_Database::TABLE_TECH_CERTIFICATION,array("TechID"));
            $query->where("certification_id=".self::VOP_CERT_ID);        
            $query->where("TechID='$techId'");        
            $techs = Core_Database::fetchAll($query);      
            
            if(!empty($techs))
            {
                return true;
            }            
            else
            {
                return false;
            }  
    }
		
    public function updateVOP($techId,$VOPStatus,$VOPDate)
    {
        $techCert = new Core_TechCertification($techId,self::VOP_CERT_ID,null,$VOPDate,null);
        if($VOPStatus==0)
        {
            $techCert->delete();
        }
        else
        {
            $techCert->save();                        
        }
    }

    public static function getTechIdsArray_ByVOP()
    {
        $db = Core_Database::getInstance();
        $query = $db->select();
        $query->from(Core_Database::TABLE_TECH_CERTIFICATION,array("TechID"));
        $query->where("certification_id=".self::VOP_CERT_ID);        
        $techs = Core_Database::fetchAll($query);
        if(empty($techs)) return null;
        $result = array();
        foreach ($techs as $t) {
            $result[] = $t['TechID'];
        }
        return $result;        
    }
    /*** VonagePlus certifications end ***/
    
    public function getTechIdsForNUMWO($categoryID,$numWO)
    {
        $db = Core_Database::getInstance();  
        $query = $db->select();
        $query->from("work_orders",array('Tech_ID'));
        $query->where("WO_Category_ID = '$categoryID'");
        $query->where("Tech_ID > 0");
        $query->where("Approved=1");
        $query->where("STATUS = 'approved' OR STATUS = 'completed'");
        $query->group('Tech_ID');
        $query->having("Count( * ) > $numWO");
        $tcRecords = Core_Database::fetchAll($query); 
                
        if(empty($tcRecords)) return '0';
        else
        {
            $techIds='';
            foreach($tcRecords as $k=>$v)
            {
                $techIds.= empty($techIds)? $v['Tech_ID']:','.$v['Tech_ID'];
            }
            return $techIds;
        } 
    }
    /*** NCR Badged Technician certifications ***/
    public function getTechIdsWithNCRBT()
    {
        $db = Core_Database::getInstance();  
        $query = $db->select();
        $query->from(Core_Database::TABLE_TECH_CERTIFICATION,array("TechID"));
        $query->where("certification_id=".self::NCRBT_CERT_ID);        
        $techs = Core_Database::fetchAll($query);      
        
        $techIds="";
        if(!empty($techs))
        {
            for($i=0;$i<count($techs);$i++)
            {
               if($i==0) {$techIds.=$techs[$i]['TechID'];}
               else {$techIds.=','.$techs[$i]['TechID'];}
            }
        }              
        return  $techIds;       
    }

    public static function getTechIdsArray_ByNCRBT()
    {
        $db = Core_Database::getInstance();
        $query = $db->select();
        $query->from(Core_Database::TABLE_TECH_CERTIFICATION,array("TechID"));
        $query->where("certification_id=".self::NCRBT_CERT_ID);        
        $techs = Core_Database::fetchAll($query);
        if(empty($techs)) return null;
        $result = array();
        foreach ($techs as $t) {
            $result[] = $t['TechID'];
        }
        return $result;        
    }
    /*** NCR Badged Technician end ***/
    public static function getTechIdsArray_ByCertiId($certId,$dateGreater='',$dateLess='')
    {
        $db = Core_Database::getInstance();
        $query = $db->select();
        $query->from(Core_Database::TABLE_TECH_CERTIFICATION,array("TechID"));
        $query->where("certification_id='$certId'");        
        if(!empty($dateGreater))       
        {
            $query->where("date >='$dateGreater'");        
        }
        if(!empty($dateLess))       
        {
            $query->where("date <='$dateLess'");        
        }        
        
        $techs = Core_Database::fetchAll($query);
        if(empty($techs)) return null;
        $result = array();
        foreach ($techs as $t) {
            $result[] = $t['TechID'];
        }
        return $result;        
    }
    public function CertificationId_Exists($certId,$techId)
    {
            $db = Core_Database::getInstance();  
            $query = $db->select();
            $query->from(Core_Database::TABLE_TECH_CERTIFICATION,array("TechID"));
            $query->where("certification_id='$certId'");        
            $query->where("TechID='$techId'");        
            $techs = Core_Database::fetchAll($query);      
            
            if(!empty($techs))
            {
                return true;
            }            
            else
            {
                return false;
            }  
    }
    /*** LMS certifications ***/
    public static function getTechIdsArray_ByLMSPreferred()
    {
        return self::getTechIdsArray_ByCertiId(self::LMSPreferred_CERT_ID);
    }
    public static function getTechIdsArray_ByLMSPlusKey()
    {
        return self::getTechIdsArray_ByCertiId(self::LMSPlusKey_CERT_ID);
    }
    public function LMSPreferred_Exists($techId)
    {
        return $this->CertificationId_Exists(self::LMSPreferred_CERT_ID,$techId);
    }
    public function LMSPlusKey_Exists($techId)
    {
        return $this->CertificationId_Exists(self::LMSPlusKey_CERT_ID,$techId);
    }    
    /*** LMS end ***/
    //14016
    public function isRestrictedByClientAndWoPublished($techID,$win,$woId)
    {
        $companyQuery = "SELECT Company_ID FROM " . Core_Database::TABLE_CLIENT_DENIED_TECHS . " WHERE TechID = $techID";
        $db = Core_Database::getInstance();
        $fieldArray = array('WIN_NUM');
        if(!empty($win)) 
        {
            $query = $db->select();         
            $query->from("work_orders",$fieldArray);
            $query->where("WIN_NUM = '$win'");
            $query->where("Status = 'published'");  
            $query->where("Company_ID IN ($companyQuery)");
            $records = Core_Database::fetchAll($query);                
            if(!empty($records)) return true;           
        }elseif(!empty($woId)) 
        {
            $query = $db->select();         
            $query->from("work_orders",$fieldArray);
            $query->where("WO_ID = '$woId'");
            $query->where("Status = 'published'");  
            $query->where("Company_ID IN ($companyQuery)");
            $records = Core_Database::fetchAll($query);                
            if(!empty($records)) return true;            
        }
        return false;
   }

    //14019
    public function isRestrictedByClient($techID,$win,$woID='')
    {
        $companyQuery = "SELECT Company_ID FROM " . Core_Database::TABLE_CLIENT_DENIED_TECHS . " WHERE TechID = $techID";
        $db = Core_Database::getInstance();
        $query = $db->select();
        $fieldArray = array('WIN_NUM');         
        $query->from("work_orders",$fieldArray);
        
        //14019
        if(!empty($win)){
        $query->where("WIN_NUM = '$win'");
        }
        if(!empty($woID)){
            $query->where("WO_ID = '$woID'");            
        }    
        $query->where("Status = 'completed'");  
        //end 14019
        
        $query->where("Company_ID IN ($companyQuery)");
        $records = Core_Database::fetchAll($query);     
           
        if(!empty($records)) return true;
        else return false;
    }
    public function getWorkOrderByClientWorkOrder($woId)
    {       
        $db = Core_Database::getInstance();
        $fieldArray = array('WIN_NUM');
        $query = $db->select();         
        $query->from("work_orders",$fieldArray);
        $query->where("WO_ID = '$woId'");   
        $records = Core_Database::fetchAll($query);                
        if(!empty($records)) return $records[0]; 
        return null;
    }

    //end 14016
    //--- 324 
    public function deleteCertification($techId,$certId)
    {
        $techCert = new Core_TechCertification($techId,$certId);
        $techCert->delete();
    }
    public function deleteCertificationByName($techId,$certName)
    {
        $db = Core_Database::getInstance();
        $query = $db->select();
        $query->from("certifications");
        $query->where("name = '$certName'");
        $records = Core_Database::fetchAll($query);
        if(empty($records)) return;
        
        $certId = $records[0]['id'];
        
        $techCert = new Core_TechCertification($techId,$certId);
        $techCert->delete();
    }
    /*** 296 ***/
    public function getBids($techID)
    {
        $db = Core_Database::getInstance();
        $query = $db->select();
        $query->from("work_orders__bids",array('WorkOrderID'));
        $query->where("TechID = '$techID'");
        $query->order("WorkOrderID desc");
        $result = Core_Database::fetchAll($query);

        
        return $result;
    }
    
    public function getWos($woslist)
    {
        $db = Core_Database::getInstance();
        $query = $db->select();
        $fieldArray = array('WIN_NUM','WO_ID','Headline','SiteName','City','State','Zipcode','Amount_Per','PayMax','Tech_ID','StartDate','EndDate','StartTime','EndTime','WO_Category','Address');         
        $query->from("work_orders",$fieldArray);
        $query->where("WIN_NUM IN ($woslist)");        
        $result = Core_Database::fetchAll($query);        
        return $result;
    }
    

    //--- 367
    public static function getTechIdsArray_ByFlextronicsContractor()
    {
        return self::getTechIdsArray_ByCertiId(self::FlextronicsContractor_CERT_ID);
    }
    public static function getTechIdsArray_ByFlextronicsScreened()
    {
        return self::getTechIdsArray_ByCertiId(self::FlextronicsScreened_CERT_ID);
    }
    public function FlextronicsContractor_Exists($techId)
    {
        return $this->CertificationId_Exists(self::FlextronicsContractor_CERT_ID,$techId);
    }
    public function FlextronicsScreened_Exists($techId)
    {
        return $this->CertificationId_Exists(self::FlextronicsScreened_CERT_ID,$techId);
    }    

    public function getTechCertInfo($certId,$techId)
    {
        $db = Core_Database::getInstance();  
        $query = $db->select();
        $query->from(Core_Database::TABLE_TECH_CERTIFICATION);
        $query->where("certification_id='$certId'");        
        $query->where("TechID='$techId'");        
        $techs = Core_Database::fetchAll($query);      
        
        if(!empty($techs))
        {
            return $techs[0];
        }            
        else
        {
            return null;
        }  
    }

    public function getFLSTestDatePass($techId)
    {
        $db = Core_Database::getInstance();  
        $query = $db->select();
        $query->from("FLS_Test");
        $query->where("tech_id='$techId'");        
        $query->where("score=100");        
        $records = Core_Database::fetchAll($query); 
        
        $ret = '';
        if(empty($records)) 
        {
            $ret = '';
        }
        else 
        { 
            $date = new DateTime($records[0]['date_taken']);
            $ret = $date->format('m/d/Y');
        }       
        return $ret;
    }

    //--- 13371
    public static function getTechIdsArray_ByComputerPlus()
    {
        return self::getTechIdsArray_ByCertiId(self::ComputerPlus_CERT_ID);
    }

    //--- 13393
    public function getGGETest($techID,$times)
    {
        $db = Core_Database::getInstance();  
        $query = $db->select();
        $query->from("gge_test");
        $query->where("testtimes='$times'");        
        $query->where("tech_id='$techID'");        
        $records = Core_Database::fetchAll($query); 
        if(empty($records)) return null;

        else return $records[0];
    }

    public function getTimesForGGETest($techID)
    {
        $db = Core_Database::getInstance();  
        $query = $db->select();
        $query->from("gge_test",array('times'=>'max(testtimes)'));
        $query->where("tech_id='$techID'");        
        $records = Core_Database::fetchAll($query);
        if(empty($records)) return 0;
        else
        { 
            $times = $records[0]['times'];
            if($times<0) $times=0;
            return $times;                
        }
    }

    public function getMinDBTesttimesForGGETest($techID)
    {
        $db = Core_Database::getInstance();  
        $query = $db->select();
        $query->from("gge_test",array('times'=>'min(testtimes)'));
        $query->where("tech_id='$techID'");        
        $records = Core_Database::fetchAll($query);
        if(empty($records)) return 0;
        else
        { 
            $times = $records[0]['times'];
            return $times;                
        }
    }
        
    public function saveAnswerForGGETest($techID,$dateTaken,
            $array_kv_answers)
    {
        $times = $this->getTimesForGGETest($techID);
        if($times==0 || $times==1)
        {
            $array_kv_answers['tech_id'] = $techID;
            $array_kv_answers['testtimes'] = $times+1;
            $array_kv_answers['date_taken']= $dateTaken;
            $result = Core_Database::insert('gge_test',$array_kv_answers);
            return $result;
        }
        else if($times = 2)
        {
            $criteria = "tech_id='$techID' AND testtimes=2";
            $array_kv_answers['date_taken']= $dateTaken;
            $result = Core_Database::update('gge_test',$array_kv_answers,$criteria);
            return $result;            
        }
    }
    public function allowedAnotherAttemptForGGETest($techID)
    {
        $negativeTimes = $this->getMinDBTesttimesForGGETest($techID);
        if($negativeTimes > 0) $negativeTimes=-1;
        else $negativeTimes = $negativeTimes -1;

        $criteria = "tech_id='$techID' AND testtimes=2";
        $fields = array('testtimes'=>$negativeTimes);
        $result = Core_Database::update('gge_test',$fields,$criteria);
        return $result;                            
    }
    
    public function getStatusArrayForGGECert($techID)
    {
        /*AttemptStatus:    Pass; Fail; Pending; N/A
AttemptScore:     0,1,2,3,...20
*/
        $firstAttempt = $this->getGGETest($techID,1);
        $secondAttempt = $this->getGGETest($techID,2);
        $techExts = $this->getExts($techID);
        $GGE1stAttemptDate='';
        if( !empty($firstAttempt))
        {
            $GGE1stAttemptDate = $firstAttempt['date_taken'];
        }
        $GGE2ndAttemptDate='';
        if( !empty($secondAttempt))
        {
            $GGE2ndAttemptDate = $secondAttempt['date_taken'];
        }        
        $GGE1stAttemptStatus="N/A";
        $GGE1stAttemptScore=0;        
        $GGE2ndAttemptStatus="N/A";
        $GGE2ndAttemptScore=0;               
        if(!empty($techExts))        
        {
            if(!empty($techExts['GGE1stAttemptStatus'])) 
                $GGE1stAttemptStatus=$techExts['GGE1stAttemptStatus'];
            if(!empty($techExts['GGE1stAttemptScore'])) 
                $GGE1stAttemptScore=$techExts['GGE1stAttemptScore'];                
            if(!empty($techExts['GGE2ndAttemptStatus'])) 
                $GGE2ndAttemptStatus=$techExts['GGE2ndAttemptStatus'];
            if(!empty($techExts['GGE2ndAttemptScore'])) 
                $GGE2ndAttemptScore=$techExts['GGE2ndAttemptScore'];      
        }
        /*GGE1stAttemptStatus,GGE1stAttemptScore,GGE1stAttemptDate,GGE2ndAttemptStatus,GGE2ndAttemptScore,GGE2ndAttemptDate*/
        $ret = array('GGE1stAttemptStatus'=>$GGE1stAttemptStatus,
            'GGE1stAttemptScore'=>$GGE1stAttemptScore,
            'GGE1stAttemptDate'=>$GGE1stAttemptDate,
            'GGE2ndAttemptStatus'=>$GGE2ndAttemptStatus,

            'GGE2ndAttemptScore'=>$GGE2ndAttemptScore,
            'GGE2ndAttemptDate'=>$GGE2ndAttemptDate,
        );
        
        return $ret;
    }
    function saveFirstAttemptScoreOrStatus($techID,$scoreOrStatus,$dateComplete='')
    {
    	/*AttemptStatus:    Pass; Fail; Pending; N/A*/
        $passScore = 13;
        //--- $score
        $score=0;
        $status='';
        if(is_numeric($scoreOrStatus))
        {
            $score = $scoreOrStatus;
            if($score < $passScore) $status = 'Fail';
            else $status = 'Pass';
            $result = $this->saveExts($techID,
                    array('GGE1stAttemptStatus'=>$status,'GGE1stAttemptScore'=>$score)
            );
        }
        else
        {
            $status = $scoreOrStatus;
            $result = $this->saveExts($techID,
                            array('GGE1stAttemptStatus'=>$status)
            );
        }
        /*N/A; Pending; 1; 2; � ;20*/
        //--- dateCompete
        if(!empty($dateComplete))
        {
            $firstAttempt = $this->getGGETest($techID,1);
            //--- if not exist then insert, else update
            if(empty($firstAttempt))
            {
                //--- insert
                $fieldvalues = array('tech_id'=>$techID,'testtimes'=>1,
                        'date_taken'=>$dateComplete);
                $result = Core_Database::insert('gge_test',$fieldvalues);
            }
            else
            {
                //--- update
                $criteria = "tech_id='$techID' AND testtimes=1";
                $fieldvalues = array('date_taken'=>$dateComplete);
                $result = Core_Database::update('gge_test',$fieldvalues,$criteria);
            }
        }
        //--- return
        return $result;        
    }
    function saveSecondAttemptScoreOrStatus($techID,$scoreOrStatus,$dateComplete='')
    {
        /*AttemptStatus:    Pass; Fail; Pending; N/A*/
        $passScore = 13;
        //--- $score
        $score=0;
        $status='';
        if(is_numeric($scoreOrStatus))
        {
            $score = $scoreOrStatus;
            if($score < $passScore) $status = 'Fail';
            else $status = 'Pass';
            $result = $this->saveExts($techID,
                    array('GGE2ndAttemptStatus'=>$status,'GGE2ndAttemptScore'=>$score)
            );
        }
        else
        {
            $status = $scoreOrStatus;
            $result = $this->saveExts($techID,
                            array('GGE2ndAttemptStatus'=>$status)
            );
        }
        /*N/A; Pending; 1; 2; � ;20*/
        //--- dateCompete
        if(!empty($dateComplete))
        {
            $secondAttempt = $this->getGGETest($techID,2);
            if(empty($secondAttempt))
            {
                //--- insert
                $fieldvalues = array('tech_id'=>$techID,'testtimes'=>2,
                        'date_taken'=>$dateComplete);
                $result = Core_Database::insert('gge_test',$fieldvalues);
            }
            else
            {
                //--- update
                $criteria = "tech_id='$techID' AND testtimes=2";
                $fieldvalues = array('date_taken'=>$dateComplete);
                $result = Core_Database::update('gge_test',$fieldvalues,$criteria);
            }
        }
        //--- return
        return $result;        
    }
    public function saveExts($techID,$fields)
    {                                      
        $db = Core_Database::getInstance();  
        $query = $db->select();
        $query->from("tech_ext");
        $query->where("TechID = ?", $techID);
        $techExt = Core_Database::fetchAll($query);
        
        if(empty($techExt))
        {            
            $fields['TechID'] = $techID;
            $result = Core_Database::insert("tech_ext",$fields);
            $this->gge_modified = true;
        }
        else
        {
        	$keys = array_keys ($fields);
        	foreach ($keys as $key) {
        		if (!$this->gge_modified && $techExt[0][$key] != $fields[$key])
        			$this->gge_modified = true;
        	}

        	if ($this->gge_modified) {
	            $criteria = "TechID = '$techID'";
    	        $result = Core_Database::update("tech_ext",$fields,$criteria);
        	}
        }       
        return  $result;
    }        

    public function isGGEModified () {
    	return $this->gge_modified;
    }

    //--- 13393
    /*
    $status: Pass,Pending,Fail-1,Fail-2
    */
    public function getTechIdsArray_ByGGECertifified()
    {
        $db = Core_Database::getInstance();  
        $query = $db->select();
        $query->from("tech_ext",array('TechID'));
        $query->where("GGE1stAttemptStatus = 'Pass' OR GGE2ndAttemptStatus = 'Pass'");
        $records = Core_Database::fetchAll($query);
        if(empty($records)) return null;
        else
        {
            $techIDs = "";
            foreach($records as $rec)
            {
                if(!empty($techIDs)) $techIDs .= ',';
                $techIDs .= $rec['TechID'];                
            }
            return $techIDs;
        }
    }

    //$status: Pass, Fail-1,Fail-2,Pending
    //$dateGreater, $dateLess: yyyy-mm-dd
    public function getTechIdsArray_ByGGEStatus($status,$dateGreater='',$dateLess='')
    {
       $statusArray = array('Pass','Fail-1','Fail-2','Pending');
       if (in_array($status, $statusArray)) {
           /*select te.TechID,gte.testtimes,gte.date_taken from tech_ext te
left join gge_test gte on gte.tech_id = te.TechID*/
            $db = Core_Database::getInstance();  
            $query = $db->select();
            $query->from(array('te'=>'tech_ext'),
                        array('TechID','GGE1stAttemptStatus','GGE2ndAttemptStatus')
            );
            $query->joinLeft(array('gte'=>'gge_test'),
                        'gte.tech_id = te.TechID',
                        array('testtimes','date_taken')
            );
            
            //--- criteria
            $criteria = "";
            if($status=='Fail-1')
            {
                $criteria = "(te.GGE1stAttemptStatus = 'Fail') ";
                if(!empty($dateGreater)) 
                {
                    $criteria .= " AND (gte.testtimes=1) AND (gte.date_taken >= '$dateGreater')";
                }
                if(!empty($dateLess)) 
                {
                    $criteria .= " AND (gte.testtimes=1) AND (gte.date_taken <= '$dateLess')";
                }                
                $criteria .= " AND (te.GGE2ndAttemptStatus is null)";                  
            }
            else if($status=='Fail-2')
            {
                $criteria = "(te.GGE1stAttemptStatus = 'Fail') ";
                $criteria .= " AND (te.GGE2ndAttemptStatus = 'Fail') ";
                if(!empty($dateGreater)) 
                {
                    $criteria .= " AND (gte.testtimes=2) AND (gte.date_taken >= '$dateGreater')";
                }
                if(!empty($dateLess)) 
                {
                    $criteria .= " AND (gte.testtimes=2) AND (gte.date_taken <= '$dateLess')";
                }                
            }
            else if($status == 'Pass')
            {
                $criteria_1 = " te.GGE1stAttemptStatus = 'Pass' ";
                $criteria_2 = " te.GGE2ndAttemptStatus = 'Pass' ";
                if(!empty($dateGreater))
                {
                    $criteria_1 .= " AND gte.testtimes=1 AND gte.date_taken >= '$dateGreater'";
                    $criteria_2 .= " AND gte.testtimes=2 AND gte.date_taken >= '$dateGreater'";
                }
                if(!empty($dateGreater))
                {
                    $criteria_1 .= " AND gte.testtimes=1 AND gte.date_taken <= '$dateLess'";
                    $criteria_2 .= " AND gte.testtimes=2 AND gte.date_taken <= '$dateLess'";
                }
                $criteria_1 = "($criteria_1)";
                $criteria_2 = "($criteria_2)";
                $criteria = "$criteria_1 OR $criteria_2";
            }
            else if($status == 'Pending')
            {
                $criteria_1 = " te.GGE1stAttemptStatus = 'Pending' ";
                $criteria_2 = " te.GGE2ndAttemptStatus = 'Pending' ";
                if(!empty($dateGreater))
                {
                    $criteria_1 .= " AND gte.testtimes=1 AND gte.date_taken >= '$dateGreater'";
                    $criteria_2 .= " AND gte.testtimes=2 AND gte.date_taken >= '$dateGreater'";
                }
                if(!empty($dateGreater))
                {
                    $criteria_1 .= " AND gte.testtimes=1 AND gte.date_taken <= '$dateLess'";
                    $criteria_2 .= " AND gte.testtimes=2 AND gte.date_taken <= '$dateLess'";
                }
                $criteria_1 = "($criteria_1)";
                $criteria_2 = "($criteria_2)";
                $criteria = "$criteria_1 OR $criteria_2";                
            }            
            $query->where($criteria);
            
            //--- fetch
            $records = Core_Database::fetchAll($query);
            
            if(empty($records)) return "0";
            else
            {
                $techIDs = "";
                foreach($records as $rec)
                {
                    if(!empty($techIDs)) $techIDs .= ',';
                    $techIDs .= $rec['TechID'];                
                }
                return $techIDs;
            }
       }
       else
       {
           return "0"; 
       }        
    }    

    public function createWithdraw($user, $pass, $winNum, $bidAmount, $comments = '')
    {
        $errors = Core_Api_Error::getInstance();

        $logger = Core_Api_Logger::getInstance();
        $logger->setUrl('widthdraw');
        $logger->setLogin($user);
        $logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');

        $authData = array('login' => $user, 'password' => $pass);

        $tech = new Core_Api_TechUser();
        $tech->checkAuthentication($authData);

        if (!$tech->isAuthenticate())
            return API_Response::fail();

        $techID = $tech->getTechID();

        $user = Core_Caspio::caspioEscape($authData["login"]);
        $pass = Core_Caspio::caspioEscape($pass);
        $winNum = Core_Caspio::caspioEscape($winNum);

        //--- get last bid
        $db = Core_Database::getInstance();
        $s = $db->select()
                ->from(Core_Database::TABLE_WORK_ORDER_BIDS, array('*'))
                ->where(Core_Database::TABLE_WORK_ORDER_BIDS . '.WorkOrderID = ?', $winNum)
                ->where(Core_Database::TABLE_WORK_ORDER_BIDS . '.TechID = ?', $techID);
        $s->order("id desc");
        $res = $db->fetchAll($s);

        $lastBid = null;

        if (!empty($res))
        {
            $lastBid = $res[0];
            $objDateNow = new Zend_Date();
            $techwithdrawbidsdata = array(
                'WorkOrderID' => $lastBid['WorkOrderID'],
                'TechID' => $lastBid['TechID'],
                'WithDrawDate' => $objDateNow->toString('yyyy-MM-dd HH:mm:ss'),
                'LastBid_Amount' => $lastBid['BidAmount'],
                'LastBid_Date' => $lastBid['Bid_Date'],
                'LastBid_Comments' => $lastBid['Comments'],
                'LastBid_DeactivatedTech' => $lastBid['DeactivatedTech'],
                'LastBid_Hide' => $lastBid['Hide'],
                'LastBid_CreateByUser' => $lastBid['CreatedByUser'],
                'LastBid_caspio_id' => $lastBid['caspio_id']
            );

            Core_Database::insert('work_orders_techwithdrawbids', $techwithdrawbidsdata, $criteria);
            $db->delete (Core_Database::TABLE_WORK_ORDER_BIDS, array('WorkOrderID = ?' => $winNum,'TechID = ?' => $techID));
            //get current Qty Applicant total
            $query = $db->select();
            $query->from(Core_Database::TABLE_WORK_ORDERS, 'Qty_Applicants');
            $query->where('WIN_NUM = ?', $winNum);
            $currentApplicantCount = Core_Database::fetchAll($query);
            //update table:work_orders with -1
            
            if($currentApplicantCount[0]['Qty_Applicants']>0)
                $Qty_Applicants = $currentApplicantCount[0]['Qty_Applicants'] - 1;
            else
                $Qty_Applicants = $currentApplicantCount[0]['Qty_Applicants'];
            
            $data = array('Qty_Applicants' => $Qty_Applicants);
            $criteria = 'WIN_NUM = ' . $winNum . '';
            $db->update(Core_Database::TABLE_WORK_ORDERS, $data, $criteria);
        }

        //return value...
        if ( $errors->hasErrors() ) return API_Response::fail();
        
            return API_Response::success(null);
    }
    public function getTechIdsArray_HasEMCCert($dateGreater,$dateLess)
    {
        $db = Core_Database::getInstance();  

        $query = $db->select();
        $query->distinct ();
        $query->from (array ("t" => Core_Database::TABLE_TECH_BANK_INFO), 
        	array ("t.TechID"));
        $query->join (array ('sr' => Core_Database::TABLE_SERVRIGHT_ELECTROMECHTEST),   // 14033
        	"t.TechID = sr.tech_id", array ());
        $query->where ("t.ServRight_ElectroMech_Cert <> 0");// 14033
        $query->where ("t.ServRight_ElectroMech_Cert IS NOT NULL");

        if(!empty($dateGreater)) {
			$stamp = strtotime($dateGreater);
			$compareDate = date("Y-m-d", $stamp);
			$query->where ("sr.date_taken >= '$compareDate'");            
		}
		if(!empty($dateLess)) {
			$stamp = strtotime($dateLess);
			$compareDate = date("Y-m-d", $stamp);
			$query->where ("sr.date_taken <= '$compareDate'");            
		}
       // print_r($query->__toString());

		$records = Core_Database::fetchAll($query);
        //14033
        $techIDs = array(); 
        if(!empty($records) && count($records)>0)
        {
               foreach($records as $rec)
               {
                     $techIDs[]=$rec['TechID'];
               }
        }
         return $techIDs; 
         //end 14033
      
        /*

		if(empty($records)) return null;
        else
        {
            $techIDs = array();
            foreach($records as $rec) $techIDs[]=$rec['TechID'];
            return $techIDs;
        }*/  
    }

    public function getTechIdsArray_NotHaveEMCCert($dateGreater,$dateLess)
    {
       // $techIDArray = $this->getTechIdsArray_HasEMCCert($dateGreater,$dateLess);
        
        $db = Core_Database::getInstance();  
        $query = $db->select();
        /*
        $query->from(Core_Database::TABLE_TECH_BANK_INFO,
                    array('TechID')
        );
        $query->where("TechID is not null");
        $query->where("TechID <> ''");
        $query->where("TechID NOT IN (?)",$techIDArray);  */
       // 14033
        $query->from (array ("t" => Core_Database::TABLE_TECH_BANK_INFO), 
            array ("t.TechID"));
        
        $query->join (array ('sr' => Core_Database::TABLE_SERVRIGHT_ELECTROMECHTEST),   
            "t.TechID = sr.tech_id",array());
       $query->where ("t.ServRight_ElectroMech_Cert = 0 OR t.ServRight_ElectroMech_Cert IS NULL");
       if(!empty($dateGreater)) {
            $stamp = strtotime($dateGreater);
            $compareDate = date("Y-m-d", $stamp);
            $query->where ("sr.date_taken >= '$compareDate'");            
        }
        if(!empty($dateLess)) {
            $stamp = strtotime($dateLess);
            $compareDate = date("Y-m-d", $stamp);
            $query->where ("sr.date_taken <= '$compareDate'");            
        }
        //end 14033
        
       // $query->where ("t.ServRight_ElectroMech_Cert IS NULL");
       // print_r($query->__toString());
        $records = Core_Database::fetchAll($query);
        //14033
        $techIDs = array(); 
        if(!empty($records) && count($records)>0)
        {
               foreach($records as $rec)
               {
                     $techIDs[]=$rec['TechID'];
               }
        }
         return $techIDs; 
         //end 14033
       /* if(empty($records)) return null;
        else
        {
            $techIDs = array();
            foreach($records as $rec) $techIDs[]=$rec['TechID'];
            return $techIDs;
        }   */
    }

    public function getTechIdsArray_ByEMCStatus($status,$dateGreater,$dateLess)
    {
        if($status==1) 
        {
            $techIDArray=$this->getTechIdsArray_HasEMCCert($dateGreater,$dateLess);
            return $techIDArray;
        }
        else if($status==0) 
        {
            $techIDArray=$this->getTechIdsArray_NotHaveEMCCert($dateGreater,$dateLess);   
            return $techIDArray;
        }
        else return null;
    }
    
    //--- 13469
    public function getTechCertificationInfo($certID,$techID)
    {
        $db = Core_Database::getInstance();
        $query = $db->select();
        $query->from(Core_Database::TABLE_TECH_CERTIFICATION);
        $query->where("certification_id='$certID'");        
        $query->where("TechID='$techID'");        
        $techs = Core_Database::fetchAll($query);
        
        if(empty($techs)) return null;
        else return $techs[0];
    }
    //--- 13469
    public function updateTechExts($techID,$keyVals)
    {
        $criteria = "TechID = '$techID'";
        $result = Core_Database::update("tech_ext",
                                $keyVals,$criteria);            
        return $result;                        
    }
    //--- 13469
    public function insertTechExts($techID,$keyVals)
    {
        $keyVals['TechID'] = $techID;
        //13728
        if(!isset($keyVals['MyWebSite']))
        {
            $keyVals['MyWebSite']='';
        }
        $result = Core_Database::insert("tech_ext",$keyVals);            
        return $result;                        
    }
    //--- 13469
    public function existTechExts($techID)
    {
        $db = Core_Database::getInstance();  
        $query = $db->select();
        $query->from("tech_ext");
        $query->where("TechID = ?", $techID);
        $result = Core_Database::fetchAll($query);        
        if(empty($result)) return false;
        else return true;
    }
    //--- 13469
    public function getTechExts($techID)
    {
        $db = Core_Database::getInstance();  
        $query = $db->select();
        $query->from("tech_ext");
        $query->where("TechID = ?", $techID);
        $result = Core_Database::fetchAll($query);        
        if(empty($result)) return null;
        else return $result[0];
    }
    //--- 13469
    public function saveTechExts($techID,$keyVals)
    {
        if($this->existTechExts($techID))
        {
            return $this->updateTechExts($techID,$keyVals);
        }
        else
        {
            return $this->insertTechExts($techID,$keyVals);
        }
    }    
    //--- 13469
    public function saveTech_TECFICAgreement($techID,$value)
    {
        $keyVals = array('TECF_ICAgreement'=>$value);
        $this->saveTechExts($techID,$keyVals);
        
        $techCert = new Core_TechCertification($techID,self::     TechForceICAgreement,null,date('Y-m-d'));
        
        if($value==1)
        {            
            $techCert->save();
        }
        else if($value==0)
        {
            $techCert->delete();
        }
        
        return;
    }    
    
    public function getTechsByProject($projects){
    	if(empty($projects) || $projects == null) return false;
    	
    	$db = Core_Database::getInstance();
		$query = $db->select()->distinct();
		$query->from(array("t"=>Core_Database::TABLE_TECH_BANK_INFO), array('t.*'));
		$query->joinLeft(array("w" => Core_Database::TABLE_WORK_ORDERS), "t.id = (SELECT id FROM TechBankInfo AS z WHERE w.Tech_ID = z.TechID LIMIT 1) ",array());
		$query->where('w.Project_ID IN (?)', $projects);
		$result = Core_Database::fetchAll($query);
		
		return $result;
    }
    
    public function getTechsByBids($projects){
    	if(empty($projects) || $projects == null) return false;
    	$db = Core_Database::getInstance();
    	
    	$query = $db->select()->distinct();
    	$query->from(array("b"=>Core_Database::TABLE_WORK_ORDER_BIDS), array('b.TechID'));
    	$query->joinLeft(array("w" => Core_Database::TABLE_WORK_ORDERS), "b.WorkOrderID = w.WIN_NUM",array());
    	$query->where("w.Project_ID IN (?)", $projects);
    	$techResult = Core_Database::fetchAll($query);
    	
    	$techArr = array();
    	foreach($techResult as $t){
    		$techArr[] = $t['TechID'];
    	}
    	    	
		$query = $db->select()->distinct();
		$query->from(array("t"=>Core_Database::TABLE_TECH_BANK_INFO), array('t.*'));
		$query->where("t.TechID IN (?)", implode("','",$techArr));
		$result = Core_Database::fetchAll($query);
		
		return $result;
    }
    //--- 13595
    public function getTechIDs_HaveResume()
    {
        //select techID from techFiles where `fileType`='Resume'
        $db = Core_Database::getInstance();  
        $query = $db->select();
        $query->from("techFiles",array("techID"));
        $query->where("fileType = 'Resume'");
        $records = Core_Database::fetchAll($query); 
        $techIds=array();
        if(!empty($records))
        {
            for($i=0;$i<count($records);$i++)
            {
                $techIds[]=$records[$i]['techID'];
            }
        }  
        return  $techIds;       
    } 
    //---13620
    public function getTechInfoByEmail($techEmail)
    {
        $db = Core_Database::getInstance();  
        $query = $db->select();
        $query->from(Core_Database::TABLE_TECH_BANK_INFO,array('TechID','FirstName','LastName'));
        $query->where("PrimaryEmail = '$techEmail'");        
        $records = Core_Database::fetchAll($query);  
        if(empty($records) || count($records)==0) return null;
        else return $records[0];
    }
       public function getPaidDateForPeriod($techLogin, $techPass,$startDate,$endDate)
    {
        require_once 'Core/Api/Class.php';
        require_once 'Core/Api/TechUser.php';
        require_once 'API/Response.php';
        require_once 'API/TechWorkOrder.php';
        require_once 'API/TechWorkOrderFilter.php';
        
        /* Error object */
        $error  = Core_Api_Error::getInstance();

        /* Check authentication */
        $authData = array('login' => $techLogin, 'password' => $techPass);
        $tech = new Core_Api_TechUser;
        $tech->checkAuthentication($authData);
        
        if ( !$tech->isAuthenticate() ) {
            return API_Response::fail();
        }

        $techID = $tech->getTechID();
                
        /* Look up database */
        $db = Core_Database::getInstance();
        $query = $db->select();
        $query->from(Core_Database::TABLE_WORK_ORDERS, array('DatePaid'));
        $query->where("Tech_ID = '$techID'");
        $query->where("Approved = 1");        
        $query->where("Status='completed'");                
        $query->where("DatePaid >= '$startDate'");        
        $query->where("DatePaid <= '$endDate'");        
        $query->order("DatePaid DESC");
        
        $result = Core_Database::fetchAll($query);
        if(empty($result) || count($result)==0) return null;
        else 
        {
            $paidDate = $result[0]['DatePaid'];
            if($paidDate < '1970-01-01') return null;
            else return $paidDate;
        }
    }

    //--- 13713
    public static function hasDeVryTag($techID)
    {
        $db = Core_Database::getInstance();
        $query = $db->select();
        $query->from(Core_Database::TABLE_TECH_CERTIFICATION);
        $query->where("TechID = '$techID'");
        $query->where("certification_id = 2");
        $result = Core_Database::fetchAll($query);
        if(empty($result) || count($result)==0){
            return 0;
        } else {
            return 1;
        }        
    }

    //--- 13728
    public static function hasOneOfTags($techID,$certIDs)
    {
        if(empty($certIDs)) return 0;
        $db = Core_Database::getInstance();
        $query = $db->select();
        $query->from(Core_Database::TABLE_TECH_CERTIFICATION);
        $query->where("TechID = '$techID'");
        $query->where("certification_id IN ($certIDs)");
        $result = Core_Database::fetchAll($query);
        if(empty($result) || count($result)==0){
            return 0;
        } else {
            return 1;
}
    }
    //--- 13728
    public static function hasOneOfTags_DeVry_Ergo_Purple_LMS_Endeavor_andMore($techID)
    {
        $certIDs = "2,20,21,40,41,42,44";
        return Core_Api_TechClass::hasOneOfTags($techID,$certIDs); 
    }
    //--- 13728
    public static function hasErgoMotionTag($techID)
    {
        $certIDs = "41,42";
        return Core_Api_TechClass::hasOneOfTags($techID,$certIDs); 
    }
    //--- 13728
    public static function hasPurpleTag($techID)
    {
        $certIDs = "40";
        return Core_Api_TechClass::hasOneOfTags($techID,$certIDs); 
    }
    //--- 13728
    public static function hasLMSTag($techID)
    {
        $certIDs = "20,21";
        return Core_Api_TechClass::hasOneOfTags($techID,$certIDs); 
    }
    //--- 13728
    public static function hasEndeavorTag($techID)
    {
        $certIDs = "44";
        return Core_Api_TechClass::hasOneOfTags($techID,$certIDs); 
    }
    
    //--- 13728
    public function ReceiveBlashEmailAndSMS_forGroupWOOnly($techID)
    {
        $techextInfo = $this->getTechExts($techID);
        return $techextInfo['ReceAvaiWorkNotiEmaiAndSMSMess_forGroupWOonly'];
    }
	
    public function getTechsNotVisibleToClient($client) {
		// get techs that should only be visible to a client based on tech profile settings
		$db = Core_Database::getInstance();
        $query = $db->select();
        $query->from("tech_ext", array('TechID'));
        $query->where("ReceAvaiWorkNotiEmaiAndSMSMess_forGroupWOonly = 1 AND ViewOnlyGroupWOonMyDash = 1");
		$certs = array();
		if ($client != 'ERG' && $client != 'MDS') {
			$certs[] = 41;
			$certs[] = 42;
		}
		if ($client != 'PURP') {
			$certs[] = 40;
		}
		if ($client != 'ENDV') {
			$certs[] = 44;
		}
		if ($client != 'LMS') {
			$certs[] = 20;
			$certs[] = 21;
		}
		$certSearch = new Core_TechCertifications;
		$certResult = $certSearch->getTechsWithCertificationAny($certs);
		if (!empty($certResult)) {
			$query->where('TechID IN (?)', $certResult);
		}
		else
			return array();
/*			if ($client == $client) {
				$certSearch = new Core_TechCertifications;
				$certResult = $certSearch->getTechsWithCertificationAny(array(2));
				if (!empty($certResult)) {
					$query->where('TechID IN (?)', $certResult);
				}
				else
					return array();
			}*/
		return $db->fetchCol($query);
	}

    //--- 13728
    public function ViewOnlyGroupWOonDashboard($techID)
    {
        $techextInfo = $this->getTechExts($techID);
        return $techextInfo['ViewOnlyGroupWOonMyDash'];
    }
    //--- 13728
    public function doesTechNeedToReceiveBlashEmailAndSMS($winNum,$techID)
    {
        if($this->ReceiveBlashEmailAndSMS_forGroupWOOnly($techID))
        {
            //--- for DeVry Group
            if(Core_Api_TechClass::hasDeVryTag($techID))
            {
                $db = Core_Database::getInstance();
                $query = $db->select();
                $query->from("work_orders_additional_fields");
                $query->where("WINNUM = '$winNum'");
                $query->where("AllowDeVryInternstoObserveWork = 1 OR AllowDeVryInternstoPerformField = 1");
                $result = Core_Database::fetchAll($query);
                if(!empty($result) && count($result)>0)
                {
                    return true;
                }
            }
            //--- ERG
            if(Core_Api_TechClass::hasErgoMotionTag($techID)) {    
                if($this->doesWoBelongToCompany($winNum,array("ERG", "MDS"))){
                    return true;
                }
            } 
            //--- PURP
            if(Core_Api_TechClass::hasPurpleTag($techID)) {  
                if($this->doesWoBelongToCompany($winNum,"PURP")){
                    return true;
                }
            }
            //--- ENDV
            if(Core_Api_TechClass::hasEndeavorTag($techID)) {  
                if($this->doesWoBelongToCompany($winNum,"ENDV")){
                    return true;
                }
            }
            //--- LMS
            if(Core_Api_TechClass::hasLMSTag($techID)) {  
                if($this->doesWoBelongToCompany($winNum,"LMS")){
                    return true;
                }
            }
            return false;            
        }
        else
        {
            return true;
        }
        
    }
    
    //--- 13728
    public function getWins_TechNeedToViewOnlyOnDashboard($techID)
    {
        if($this->ViewOnlyGroupWOonDashboard($techID))
        {
            $wins='0';        
            //--- for DeVry Group
            if(Core_Api_TechClass::hasDeVryTag($techID))
            {
                $db = Core_Database::getInstance();
                $query = $db->select();
                $query->from(array("woaf"=>"work_orders_additional_fields"),array('WINNUM'));
                $query->join(array("w"=>Core_Database::TABLE_WORK_ORDERS),
                        'w.WIN_NUM = woaf.WINNUM',
                        array()
                );
//                $query->where("woaf.AllowDeVryInternstoObserveWork = 1 OR woaf.AllowDeVryInternstoPerformField = 1");
                $query->where("w.Status = 'published'");
                $records = Core_Database::fetchAll($query);
                if(!empty($records))
                {
                    foreach($records as $rec) { $wins.=",".$rec['WINNUM'];}
                }   
            }
            else
            {
                //hasErgoMotionTag ERG     ERG
                if(Core_Api_TechClass::hasErgoMotionTag($techID)) {    
                    $wins_1 = $this->getWinsOfCompany("ERG");
                    if(!empty($wins_1))  {
                        $wins.=",".$wins_1;
                    }   
                }                
                       
                //hasPurpleTag PURP     PURPLE
                if(Core_Api_TechClass::hasPurpleTag($techID)) {    
                    $wins_1 = $this->getWinsOfCompany("PURP");
                    if(!empty($wins_1))  {
                        $wins.=",".$wins_1;
                    }   
                }                 

                //hasEndeavorTag ENDV     Endeavor    
                if(Core_Api_TechClass::hasEndeavorTag($techID)) {    
                    $wins_1 = $this->getWinsOfCompany("ENDV");
                    if(!empty($wins_1))  {
                        $wins.=",".$wins_1;
                    }   
                }                 
                  
                //hasLMSTag LMS Logical Maintenance Solutions
                if(Core_Api_TechClass::hasLMSTag($techID)) {    
                    $wins_1 = $this->getWinsOfCompany("LMS");
                    if(!empty($wins_1))  {
                        $wins.=",".$wins_1;
                    }   
                }                 
            }
            return $wins;
        }
        else{
            return '';
        }
    }
    
    //--- 13728
    public function getWinsOfCompany($companyID)
    {
        $wins = "";
                    $db = Core_Database::getInstance();
                    $query = $db->select();
                    $query->from(Core_Database::TABLE_WORK_ORDERS,
                            array('WIN_NUM','Company_ID'));                        
        $query->where("Company_ID = '$companyID'");    
                    $records = Core_Database::fetchAll($query);                    
        if(!empty($records) && count($records) > 0)
                    {
            foreach($records as $rec) { 
                if($wins != ""){$wins .= ",";}
                $wins.= $rec['WIN_NUM'];
                    }   
                }                 
            return $wins;
        }
    //--- 13728
    public function doesWoBelongToCompany($winNum,$companyID)
    {
        $db = Core_Database::getInstance();
		if (!is_array($companyID)) $companyID = array($companyID);
        $query = $db->select();
        $query->from(Core_Database::TABLE_WORK_ORDERS,
                array('WIN_NUM','Company_ID'));                        
        $query->where("WIN_NUM = '$winNum'");   
        $query->where("Company_ID IN (?)", $companyID);    
        $result = Core_Database::fetchAll($query);
        if(!empty($result) && count($result)>0) {
            return true;
        } else {
            return false;
        }
    }
    //--- 13761
    public function getBlackBoxCablingStatus($techID)
    {
        //--- does tech has cabling cert?
        $cablingIDs = "46,47,48";
        $db = Core_Database::getInstance();
        $query = $db->select();
        $query->from(array('tc'=>Core_Database::TABLE_TECH_CERTIFICATION),
                array('TechID','certification_id','number','date'));
        $query->join(array('c'=>Core_Database::TABLE_CERTIFICATIONS),
                'c.id = tc.certification_id',
                array('name','label')
        );
        $query->where("tc.TechID = '$techID'");   
        $query->where("tc.certification_id IN ($cablingIDs)");    
        $query->order("tc.certification_id DESC");
        $cablingRecords = Core_Database::fetchAll($query);
        $hasCablingCert = false;
        if(!empty($cablingRecords) && count($cablingRecords)>0)
        {
            $hasCablingCert = true;
            $cablingInfo = $cablingRecords[0];            
            //array('Status'=>,'Level'=>,'LevelText'=>,'Date'=>)
            $returnArray = array('Status'=>'Cert',
                    'LevelText'=>$cablingInfo['label'],
                    'Date'=>$cablingInfo['date'],
                    'CertID'=>$cablingInfo['certification_id']
            );
            return $returnArray;
        }                
        //--- is texh pending cabling-cert?
        if(!$hasCablingCert)
        {
            $db = Core_Database::getInstance();
            $query = $db->select();
            $query->from("BlackBoxCabling_Test",
                    array('id','tech_id','date_taken','Level')
            );
            $query->where("tech_id = '$techID'");   
            $testRecords = Core_Database::fetchAll($query);
            if(!empty($testRecords) && count($testRecords)>0)
            {
                $testRec = $testRecords[0];
                $prop = $this->getBBCablingTagProperties_ByLevel($testRec['Level']);                
                //array('LevelText'=>$levelText,'CertID'=>$certID);                
                $returnArray = array('Status'=>'Pending',
                        'LevelText'=>$prop['LevelText'],
                        'Date'=>$testRec['date_taken'],
                        'CertID'=>$prop['CertID']
                );
                return $returnArray;
            }
        }
        //--- other
        return '';
    }    

    //--- 13761
    public function getBlackBoxTelecomStatus($techID)
    {
        //--- does tech has telecom cert?
        $telecomIDs = "49,50";
        $db = Core_Database::getInstance();
        $query = $db->select();
        $query->from(array('tc'=>Core_Database::TABLE_TECH_CERTIFICATION),
                array('TechID','certification_id','number','date'));
        $query->join(array('c'=>Core_Database::TABLE_CERTIFICATIONS),
                'c.id = tc.certification_id',
                array('name','label')
        );
        $query->where("tc.TechID = '$techID'");   
        $query->where("tc.certification_id IN ($telecomIDs)");    
        $query->order("tc.certification_id DESC");
        $telecomRecords = Core_Database::fetchAll($query);
        $hasTelecomCert = false;
        if(!empty($telecomRecords) && count($telecomRecords)>0)
        {
            $hasTelecomCert = true;
            $telecomInfo = $telecomRecords[0];            
            //array('Status'=>,'Level'=>,'LevelText'=>,'Date'=>,'CertID'=>)
            $returnArray = array('Status'=>'Cert',
                    'LevelText'=>$telecomInfo['label'],
                    'Date'=>$telecomInfo['date'],
                    'CertID'=>$telecomInfo['certification_id']
            );
            return $returnArray;
        }                
        //--- is texh pending telecom-cert?
        if(!$hasTelecomCert)
        {
            $db = Core_Database::getInstance();
            $query = $db->select();
            $query->from("BlackBoxTelephony_Test",
                    array('id','tech_id','date_taken','Level')
            );
            $query->where("tech_id = '$techID'");   
            $testRecords = Core_Database::fetchAll($query);
            if(!empty($testRecords) && count($testRecords)>0)
            {
                $testRec = $testRecords[0];
                $prop = $this->getBBTelecomTagProperties_ByLevel($testRec['Level']);
                //array('LevelText'=>$levelText,'CertID'=>$certID);
                $returnArray = array('Status'=>'Pending',
                        'LevelText'=>$prop['LevelText'],
                        'Date'=>$testRec['date_taken'],
                        'CertID'=>$prop['CertID']
                );
                return $returnArray;
            }
        }
        //--- other
        return '';
    }    
    //13761    
    public function getBBCablingTagProperties_ByLevel($level)
    {
        $levelText='';$certID='';
        if($level==self::BlackBoxCablingLevel_Helper){
            $levelText='Helper (C1)'; $certID = self::BlackBoxCablingHelper_CERT_ID;
        } else if($level==self::BlackBoxCablingLevel_Advanced){
            $levelText='Advanced (C2)'; $certID = self::BlackBoxCablingAdvanced_CERT_ID;
        } else if($level==self::BlackBoxCablingLevel_Lead){
            $levelText='Lead (C3)'; $certID = self::BlackBoxCablingLead_CERT_ID;
        }
        return array('LevelText'=>$levelText,'CertID'=>$certID);        
    }

    //13757
    public function getBasicToolsList()
    {
        $db = Core_Database::getInstance();  
        $query = $db->select();
        $query->from('equipment',
            array('id','name','label','description','label_ext'=>"label"));
        $query->where("id in (2,1,25,27,30,28,5,6,7,23,36,37,38,39) ");
        $query->order("label ASC");            
        $records = Core_Database::fetchAll($query); 
        return $records;
    }
	


    public function getTelephonyToolsList()
    {
        $db = Core_Database::getInstance();  
        $query = $db->select();
        $query->from('equipment',
            array('id','name','label','description','label_ext'=>"label"));
        $query->where("id in (8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,31,32,33,34,35,40,41,42) ");
        $query->order("label ASC");            
        $records = Core_Database::fetchAll($query); 
		if(!empty($records) && count($records)>0)
        {
            for($i=0;$i<count($records);$i++) {
                $name = $records[$i]['name'];
                if($name=='GoferPoles'){
                    $records[$i]['label_ext']="Gofer Poles (or Glow Rods)";
                } else if($name=='FishTape50') {
                    $records[$i]['label_ext']="50' Fish Tape";
                }
            }
        }        
        return $records;
    }
	
    public function getCablingExperienceList()
    {
        $db = Core_Database::getInstance();  
        $query = $db->select();
        $query->from('skills',
            array('id','name','label','description','label_ext'=>"label"));
        $query->where("id in (2,3,41,42,45,46,47,48,49,51,52,53,54,55) ");
        $query->order("label_ext ASC");            
        $records = Core_Database::fetchAll($query); 
        if(!empty($records) && count($records)>0)
        {
            for($i=0;$i<count($records);$i++) {
                $name = $records[$i]['name'];
                if($name=='ReadWiringDiagrams'){
                    $records[$i]['label_ext']="Can read wiring diagrams";
                }else if($name=='SkilledCat5Fiber'){
                    $records[$i]['label_ext']="Is skilled with Cat5 and fiber cabling";
                }else if($name=='ExpInstallSurv'){
                    $records[$i]['label_ext']="Has experience with installing surveillance DVR/Cam systems";
                }else if($name=='40HrsLast6Months'){
                    $records[$i]['label_ext']="Has worked at least 40 hours of Cat5 cabling contracts in the past 6 months";
                }else if($name=='PossessAllLicenses'){
                    $records[$i]['label_ext']="Possesses all licenses required in city/state to perform low voltage work";
                }else if($name=='CableTVTelcoCLECExperience'){
                    $records[$i]['label_ext']="Has Cable TV Co., Telco, and/or CLEC work Experience";
                }else if($name=='AdvCableLacStitchSkills'){
                    $records[$i]['label_ext']="Has advanced cable skills (including lacing and stitching)";
                }else if($name=='BatteryDistributionFuseBay'){
                    $records[$i]['label_ext']="Has worked in a Battery Distribution Fuse Bay";
                }else if($name=='ReadingMOPsExperience'){
                    $records[$i]['label_ext']="Has experience reading MOPs (Methods of Procedures)";
                }else if($name=='InstallationMultipleCableExperience'){
                    $records[$i]['label_ext']="Has experience with multiple cable installation methods (installing in conduit using fish tape, pull string, or shop vac)";
                }
				else if($name=='OperationScissorLiftExperience'){
                    $records[$i]['label_ext']="Knows how to operate a scissor lift and has extensive experience using a ladder";
                }
				else if($name=='InstallingSleevesRacewaysExperience'){
                    $records[$i]['label_ext']="Has experience installing sleeves firestops, J-Hooks, Panduit, and Wire Mold raceways
";
                }
				else if($name=='InstallationVoiceDataEquipmentExperience'){
                    $records[$i]['label_ext']="Has experience with voice and data equipment room installation and build out (ex: data cabinets and racks, plywood, 66/110 blocks, patch panels, etc)
";
                }
				else if($name=='SingleAndMultiModeTerminationTestingExp'){
                    $records[$i]['label_ext']="Has experience in single-mode and multi-mode termination and testing
";
                }
				else if($name=='UsingLaptopForSwitchConfigurationExp'){
                    $records[$i]['label_ext']="Has experience using a laptop for basic switch configuration (HyperTerminal, Terra Term, etc.)
";
                }
            }
        }
        return $records;
    }
	
	public function getTelephonyExperienceList()
    {
        $db = Core_Database::getInstance();  
        $query = $db->select();
        $query->from('skills',
            array('id','name','label','description','label_ext'=>"label"));
        $query->where("id in (4,6,50,56,57) ");
        $query->order("label ASC");            
        $records = Core_Database::fetchAll($query); 
        if(!empty($records) && count($records)>0)
        {
            for($i=0;$i<count($records);$i++) {
                $name = $records[$i]['name'];
                if($name=='InstallingTelephony'){
                    $records[$i]['label_ext']=" Experience installing/maintaining (break-fix) telephony system";
                }else if($name=='ServerTelephony'){
                    $records[$i]['label_ext']="Installed/performed 'break-fix' activity on a server or VOIP based telephony system";
                }else if($name=='UnderstandTelecomColorCodeExperience'){
                    $records[$i]['label_ext']="Understands the telecom color code and has experience with termination jacks and patch panels";
                }else if($name=='DMARCRoomsEquipmentExperience'){
                    $records[$i]['label_ext']="Has experience with DMARC rooms and equipment such as Rj-21X, Smart jacks, lightning protectors, and cross connect fields";
				}else if($name=='OperationPBXExperience'){
                    $records[$i]['label_ext']="Has experience with basic PBX / Key system operation (ex: general programming, station card and CO line card replacement, station / extension assignment)";
                
                }
				}
        }        
        return $records;
    }
	
    public function getToolsList()
    {
        $db = Core_Database::getInstance();  
        $query = $db->select();
        $query->from('equipment',
            array('id','name','label','description'));        
        $query->order("label ASC");            
        $records = Core_Database::fetchAll($query); 
        return $records;
    }
 
    public function getSkillsList()
    {
        $db = Core_Database::getInstance();  
        $query = $db->select();
        $query->from('skills',
            array('id','name','label','description'));        
        $query->order("label ASC");            
        $records = Core_Database::fetchAll($query); 
        return $records;
    }
    
    //--- 13757
    public function getTechIdsForTool($toolName)
    {
        //select te.TechID from tech_equipment te join equipment gte on gte.id= te.equipment_id  where gte.name = '$toolName'
      //--- filter
        if(empty($toolName)) return;
       
        $db = Core_Database::getInstance();  
        $query = $db->select();
            
        $query->from(array('te'=>'tech_equipment'),
                            array('TechID')
                );
        $query->join(array('gte'=>'equipment'),
                            'gte.id = te.equipment_id',
                            array('name')
         ); 
        $query->where("name = '$toolName'");            
             
        $tcRecords = Core_Database::fetchAll($query); 
        if(empty($tcRecords)) return '0';
        else
        {
            $techIds='';
            foreach($tcRecords as $k=>$v)
            {
                $techIds.= empty($techIds)? $v['TechID']:','.$v['TechID'];
            }
            return $techIds;
        }                
    }
    //--- 13757
    public function getTechIdsForSkill($skillName)
    {
       
        if(empty($skillName)) return;
       
        $db = Core_Database::getInstance();  
        $query = $db->select();
        
  $query->from(array('te'=>'tech_skill'),
                        array('TechID')
            );
            $query->join(array('gte'=>'skills'),
                        'gte.id = te.skill_id',
                        array('name')
     ); 
  $query->where("name = '$skillName'");            
      
        $tcRecords = Core_Database::fetchAll($query); 
        if(empty($tcRecords)) return '0';
        else
        {
            $techIds='';
            foreach($tcRecords as $k=>$v)
            {
                $techIds.= empty($techIds)? $v['TechID']:','.$v['TechID'];
            }
            return $techIds;
        }                
    }
 	//End 13757
    
    //13761
    public function getBBTelecomTagProperties_ByLevel($level)
    {
        $levelText='';$certID=0;
        if($level==self::BlackBoxTelecomLevel_Helper){
            $levelText='Helper (T1)'; $certID = self::BlackBoxTelecomHelper_CERT_ID;
        } else if($level==self::BlackBoxTelecomLevel_Advanced){
            $levelText='Advanced (T2)'; $certID = self::BlackBoxTelecomAdvanced_CERT_ID;
        }
        return array('LevelText'=>$levelText,'CertID'=>$certID);        
    }
    //13761
    public function confirmBBCert($certID,$techID)
    {
        $BBCertIDArray = split(',',self::BBCertIDs);
        if(!in_array($certID,$BBCertIDArray)){ return;}
        
        if($this->CertificationId_Exists($certID,$techID))
        {
            $criteria = "certification_id='$certID' AND TechID='$techID'";
            $fields = array('date'=>date("Y-m-d"));
            $result = Core_Database::update(Core_Database::TABLE_TECH_CERTIFICATION,$fields,$criteria);            
        }
        else
        {
            $fields = array('TechID'=>$techID,'certification_id'=>$certID,'date'=>date("Y-m-d"));   
            $result = Core_Database::insert(Core_Database::TABLE_TECH_CERTIFICATION,$fields);
        }
        return $this->getTechCertInfo($certID,$techID);        
    }
    //13761
    public function denyBBCert($certID,$techID)
    {
        $BBCertIDArray = split(',',self::BBCertIDs);
        if(!in_array($certID,$BBCertIDArray)){ return;}
        $BBCablingCertIDArray = split(',',self::BBCablingCertIDs);
        
        //--- delete cert
        $criteria = "certification_id='$certID' AND TechID='$techID'";
        $db = Core_Database::getInstance();
        $result = $db->delete(Core_Database::TABLE_TECH_CERTIFICATION,$criteria);
        //--- delete test
        if(in_array($certID,$BBCablingCertIDArray))
        {
            $criteria = "tech_id='$techID'";
            $db = Core_Database::getInstance();
            $result = $db->delete("BlackBoxCabling_Test",$criteria);            
        }
        else
        {
            $criteria = "tech_id='$techID'";
            $db = Core_Database::getInstance();
            $result = $db->delete("BlackBoxTelephony_Test",$criteria);                        
        }        
    }
    //13761
    public function saveBBCert($certID,$techID,$date)
    {
        $BBCertIDArray = split(',',self::BBCertIDs);
        if(!in_array($certID,$BBCertIDArray)){ return;}
        $BBCablingCertIDArray = split(',',self::BBCablingCertIDs);
        $BBTelecomCertIDArray = split(',',self::BBTelecomCertIDs);
        
        if(!empty($date)){
            $date = date("Y-m-d",strtotime($date));
        }else{
            $date = date("Y-m-d");
        }
        
        if(in_array($certID,$BBCablingCertIDArray)) {
            //--- delete cabling-certs
            $criteria = "TechID='$techID' AND certification_id IN (".self::BBCablingCertIDs.")";
            $db = Core_Database::getInstance();
            $result = $db->delete(Core_Database::TABLE_TECH_CERTIFICATION,$criteria);
            //--- insert
            $fields = array('TechID'=>$techID,'certification_id'=>$certID,'date'=>$date);   
            $result = Core_Database::insert(Core_Database::TABLE_TECH_CERTIFICATION,$fields);
        } else if(in_array($certID,$BBTelecomCertIDArray)) {
            //--- delete cabling-certs
            $criteria = "TechID='$techID' AND certification_id IN (".self::BBTelecomCertIDs.")";
            $db = Core_Database::getInstance();
            $result = $db->delete(Core_Database::TABLE_TECH_CERTIFICATION,$criteria);
            //--- insert
            $fields = array('TechID'=>$techID,'certification_id'=>$certID,'date'=>$date);   
            $result = Core_Database::insert(Core_Database::TABLE_TECH_CERTIFICATION,$fields); 
        }        
        return $result;
    }
    //13761
    public function saveBBPending($certID,$techID,$date)
    {
        $BBCertIDArray = split(',',self::BBCertIDs);
        if(!in_array($certID,$BBCertIDArray)){ return;}
        $BBCablingCertIDArray = split(',',self::BBCablingCertIDs);
        $BBTelecomCertIDArray = split(',',self::BBTelecomCertIDs);
        
        if(!empty($date)){
            $date = date("Y-m-d",strtotime($date));
        } else {
            $date = date("Y-m-d");
        }
        
        if(in_array($certID,$BBCablingCertIDArray)) {
            //--- delete cabling-certs
            $criteria = "TechID='$techID' AND certification_id IN (".self::BBCablingCertIDs.")";
            $db = Core_Database::getInstance();
            $result = $db->delete(Core_Database::TABLE_TECH_CERTIFICATION,$criteria);
            //--- level
            $level=0;
            if($certID==self::BlackBoxCablingHelper_CERT_ID){
                $level = 1;
            } else if($certID==self::BlackBoxCablingAdvanced_CERT_ID) {
                $level = 2;
            } else if($certID==self::BlackBoxCablingLead_CERT_ID) {
                $level = 3;
            }
            //--- has test or not?
            $db = Core_Database::getInstance();
            $query = $db->select();
            $query->from("BlackBoxCabling_Test",
                    array('id','tech_id','date_taken','Level')
            );
            $query->where("tech_id = '$techID'");   
            $testRecords = Core_Database::fetchAll($query);
            if(!empty($testRecords) && count($testRecords)>0)  {                
                //update 
                $criteria = "tech_id = '$techID'";
                $fields = array("Level"=>$level,"date_taken"=>$date);
                $result = Core_Database::update("BlackBoxCabling_Test",$fields,$criteria);
            } else {
                //insert
                $fields = array("tech_id"=>$techID,"Level"=>$level,"date_taken"=>$date);
                $result = Core_Database::insert("BlackBoxCabling_Test",$fields);
            }
        } else if(in_array($certID,$BBTelecomCertIDArray)) {
            //--- delete telecom-certs
            $criteria = "TechID='$techID' AND certification_id IN (".self::BBTelecomCertIDs.")";
            $db = Core_Database::getInstance();
            $result = $db->delete(Core_Database::TABLE_TECH_CERTIFICATION,$criteria);
            //--- level
            $level=0;
            if($certID==self::BlackBoxTelecomHelper_CERT_ID){
                $level = 1;
            } else if($certID==self::BlackBoxTelecomAdvanced_CERT_ID) {
                $level = 2;
            } 
            //--- has test or not?
            $db = Core_Database::getInstance();
            $query = $db->select();
            $query->from("BlackBoxTelephony_Test",
                    array('id','tech_id','date_taken','Level')
            );
            $query->where("tech_id = '$techID'");   
            $testRecords = Core_Database::fetchAll($query);
            if(!empty($testRecords) && count($testRecords)>0)  {                
                //update 
                $criteria = "tech_id = '$techID'";
                $fields = array("Level"=>$level,"date_taken"=>$date);
                $result = Core_Database::update("BlackBoxTelephony_Test",$fields,$criteria);
            } else {
                //insert
                $fields = array("tech_id"=>$techID,"Level"=>$level,"date_taken"=>$date);
                $result = Core_Database::insert("BlackBoxTelephony_Test",$fields);
            }            
        }
        
    }
    //13761        
    public function getTechIdsArray_ByBBCablingStatus($status,$dateGreater,$dateLess)
    {
        //Yes,Helper,Advanced,Lead,Pending
        $statusArray = array('Yes','Helper','Advanced','Lead','Pending');
        if(!empty($dateGreater)){
            $dateGreater = date("Y-m-d",strtotime($dateGreater));
        }
        if(!empty($dateLess)){
            $dateLess = date("Y-m-d",strtotime($dateLess));
        }        
        if (in_array($status, $statusArray)) {
            $db = Core_Database::getInstance();  
            $query = $db->select();
            if(in_array($status,array('Yes','Helper','Advanced','Lead')))  {
                $query->from("tech_certification",
                        array('TechID','certification_id','date')
                );    
                if(!empty($dateGreater)){
                    $query->where("date >= '$dateGreater'");
                }
                if(!empty($dateLess)){
                    $query->where("date <= '$dateLess'");
                }                            
            }else if($status=='Pending'){
                // BlackBoxCabling_Test
                $techIDs_haveCablingTag = $this->getTechIds_haveBBCablingTag();
                $query->from(array("BlackBoxCabling_Test"),
                        array('TechID'=>'tech_id','date_taken')); 
                $query->where("tech_id NOT IN ($techIDs_haveCablingTag)");
                if(!empty($dateGreater)){
                    $query->where("date_taken >= '$dateGreater'");
                }
                if(!empty($dateLess)){
                    $query->where("date_taken <= '$dateLess'");
                }
            }
            
            if($status=='Yes') {
                $query->where("certification_id IN (".self::BBCablingCertIDs.")");
            }else if($status=='Helper'){
                $query->where("certification_id = '".self::BlackBoxCablingHelper_CERT_ID."'"); 
            }else if($status=='Advanced'){
                $query->where("certification_id = '".self::BlackBoxCablingAdvanced_CERT_ID."'"); 
            }else if($status=='Lead'){
                $query->where("certification_id = '".self::BlackBoxCablingLead_CERT_ID."'"); 
            }
            
            
            $records = Core_Database::fetchAll($query);
            if(empty($records) || count($records)==0) {
                return "0";
            } else {
                $techIDs = "";
                foreach($records as $rec)
                {
                    if(!empty($techIDs)) $techIDs .= ',';
                    $techIDs .= $rec['TechID'];                
                }
                return $techIDs;
            }                            
        }  else  {
           return "0"; 
        }        
    }
    
    //13761        
    public function getTechIdsArray_ByBBTelecomStatus($status,$dateGreater,$dateLess)
    {
        //Yes,Helper,Advanced,Pending
        $statusArray = array('Yes','Helper','Advanced','Pending');
        if(!empty($dateGreater)){
            $dateGreater = date("Y-m-d",strtotime($dateGreater));
        }
        if(!empty($dateLess)){
            $dateLess = date("Y-m-d",strtotime($dateLess));
        }        
        if (in_array($status, $statusArray)) {
            $db = Core_Database::getInstance();  
            $query = $db->select();
            if(in_array($status,array('Yes','Helper','Advanced')))  {
                $query->from("tech_certification",
                        array('TechID','certification_id','date')
                );    
                if(!empty($dateGreater)){
                    $query->where("date >= '$dateGreater'");
                }
                if(!empty($dateLess)){
                    $query->where("date <= '$dateLess'");
                }                            
            }else if($status=='Pending'){
                // BlackBoxCabling_Test
                $techIDs_haveTelecomTag = $this->getTechIds_haveBBTelecomTag();
                $query->from(array("BlackBoxTelephony_Test"),
                        array('TechID'=>'tech_id','date_taken')); 
                $query->where("tech_id NOT IN ($techIDs_haveCablingTag)");
                if(!empty($dateGreater)){
                    $query->where("date_taken >= '$dateGreater'");
                }
                if(!empty($dateLess)){
                    $query->where("date_taken <= '$dateLess'");
                }
            }
            
            if($status=='Yes') {
                $query->where("certification_id IN (".self::BBTelecomCertIDs.")");
            }else if($status=='Helper'){
                $query->where("certification_id = '".self::BlackBoxTelecomHelper_CERT_ID."'"); 
            }else if($status=='Advanced'){
                $query->where("certification_id = '".self::BlackBoxTelecomAdvanced_CERT_ID."'"); 
            }
            
            $records = Core_Database::fetchAll($query);
            if(empty($records) || count($records)==0) {
                return "0";
            } else {
                $techIDs = "";
                foreach($records as $rec)
                {
                    if(!empty($techIDs)) $techIDs .= ',';
                    $techIDs .= $rec['TechID'];                
                }
                return $techIDs;
            }                            
        }  else  {
           return "0"; 
        }        
    }
    //13761
    public function getTechIds_BBSearch($BBFilters)
    {
        if(empty($BBFilters)){
            return '';
        }
        //$BBFilters = array('BlackBoxCablingHelper','BlackBoxCablingAdvanced','BBCablingAdvancedIncludePendingRequests','BlackBoxCablingLead','BBCablingLeadIncludePendingRequests','BlackBoxTelecomHelper','BlackBoxTelecomAdvanced')
        $IDFilters = array();
        if(in_array('BlackBoxCablingHelper',$BBFilters)){
            $IDFilters[]=self::BlackBoxCablingHelper_CERT_ID;
			$IDFilters[]=self::BlackBoxCablingAdvanced_CERT_ID;
			$IDFilters[]=self::BlackBoxCablingLead_CERT_ID;
        }
        if(in_array('BlackBoxCablingAdvanced',$BBFilters)){
            $IDFilters[]=self::BlackBoxCablingAdvanced_CERT_ID;
			$IDFilters[]=self::BlackBoxCablingLead_CERT_ID;
        }
        if(in_array('BlackBoxCablingLead',$BBFilters)){
            $IDFilters[]=self::BlackBoxCablingLead_CERT_ID;
        }
        if(in_array('BlackBoxTelecomHelper',$BBFilters)){
            $IDFilters[]=self::BlackBoxTelecomHelper_CERT_ID;
			$IDFilters[]=self::BlackBoxTelecomAdvanced_CERT_ID;
        }
        if(in_array('BlackBoxTelecomAdvanced',$BBFilters)){
            $IDFilters[]=self::BlackBoxTelecomAdvanced_CERT_ID;
        }
        $techIDs = "";
        $techIDsArray = array();
        if(count($IDFilters) > 0)
        {
            $db = Core_Database::getInstance();  
            $query = $db->select();
            $query->from("tech_certification",
                            array('TechID','certification_id','date')
            );                
            $query->where("certification_id IN (?)",$IDFilters);
            $records = Core_Database::fetchAll($query);
            $techIDs = "";
            if(!empty($records) || count($records) > 0) {
                foreach($records as $rec) {
                    $techIDsArray[] = $rec['TechID'];                    
                }
            }  
        }
        
        //--- BBCablingHelperIncludePendingRequests
        if(in_array('BBCablingAdvancedIncludePendingRequests',$BBFilters))
        {
            $tempTechIDArray = $this->getTechIdArray_ArePendingForBBCablingTag_ByLevel(1);
            $techIDsArray = array_merge($techIDsArray,$tempTechIDArray);
        }        
        //--- BBCablingAdvancedIncludePendingRequests
        if(in_array('BBCablingAdvancedIncludePendingRequests',$BBFilters))
        {
            $tempTechIDArray = $this->getTechIdArray_ArePendingForBBCablingTag_ByLevel(2);
            $techIDsArray = array_merge($techIDsArray,$tempTechIDArray);
        }
        //--- BBCablingLeadIncludePendingRequests
        if(in_array('BBCablingLeadIncludePendingRequests',$BBFilters))
        {
            $tempTechIDArray = $this->getTechIdArray_ArePendingForBBCablingTag_ByLevel(3);
            $techIDsArray = array_merge($techIDsArray,$tempTechIDArray);
        }
        //--- BBTelecomHelperIncludePendingRequests
        if(in_array('BBTelecomHelperIncludePendingRequests',$BBFilters))
        {
            $tempTechIDArray = $this->getTechIdArray_ArePendingForBBTelecomTag_ByLevel(1);
            $techIDsArray = array_merge($techIDsArray,$tempTechIDArray);
        }
        //--- BBTelecomAdvancedIncludePendingRequests
        if(in_array('BBTelecomAdvancedIncludePendingRequests',$BBFilters))
        {
            $tempTechIDArray = $this->getTechIdArray_ArePendingForBBTelecomTag_ByLevel(2);
            $techIDsArray = array_merge($techIDsArray,$tempTechIDArray);
        }
        //--- return
        if(count($techIDsArray) > 0) {
            $techIDs = implode(",",$techIDsArray);            
        } else {
            $techIDs = "";
        }        
        return $techIDs;
    }
    //13761
    public function getBBCablingTestInfo($techID)
    {
        $testInfo = $this->getBBCablingTest_ByTech($techID);
        if(empty($testInfo) || count($testInfo)==0)
        {
            $testInfo = array();
            $testInfo['tech_id']=$techID;          
            $testInfo['Level'] = 0;
        }
        //--- get info from tech_equiment
        $db = Core_Database::getInstance();  
        $query = $db->select();
        $query->from(array('te'=>"tech_equipment"),
                array('TechID','equipment_id')
        );
        $query->join(array('e'=>'equipment'),
                'e.id = te.equipment_id',
                array('name')
        );
        $query->where("TechID = '$techID'");
        $records = Core_Database::fetchAll($query);
        if(!empty($records) && count($records) > 0)
        {
            $techEquip = array();
            foreach($records as $rec){                
                $name = $rec['name'];
                $techEquip[$name] = $rec;                    
            }
            
            $map = $this->getMapping_BBCablingTest_RequiredTools_Equiment();
            foreach($map as $k=>$v)
            {
                if($k!='C1RT_2' && $k!='C2RT_1' && $k!='C3RT_1') {
                    
                    $value = !empty($techEquip[$v]) ? 1 : 0;
                    $testInfo[$k] = $value;
                }
            }
        } 
        //--- get info from tech_skill
        $db = Core_Database::getInstance();  
        $query = $db->select();
        $query->from(array('ts'=>"tech_skill"),
                array('TechID','skill_id')
        );
        $query->join(array('s'=>'skills'),
                's.id = ts.skill_id',
                array('name')
        );
        $query->where("TechID = '$techID'");
        $records = Core_Database::fetchAll($query);
        if(!empty($records) && count($records) > 0)
        {
            $techSkill = array();
            foreach($records as $rec){                
                $name = $rec['name'];
                $techSkill[$name] = $rec;                    
            }                
            $map = $this->getMapping_BBCablingTest_RequiredExperience_Skill();
            foreach($map as $k=>$v)
            {
                if($k!='C2RE_1' && $k!='C3RE_1') {                        
                    $value = !empty($techSkill[$v]) ? 1 : 0;
                    $testInfo[$k] = $value;
                }
            }
        } 
        
        return $testInfo;
    }

    public function getBBCablingTest_ByTech($techID)
    {
        $db = Core_Database::getInstance();  
        $query = $db->select();
        $query->from("BlackBoxCabling_Test");
        $query->where("tech_id = '$techID'");
        $records = Core_Database::fetchAll($query);
        if(!empty($records) || count($records) > 0) {
            return $records[0];
        } else {
            return null;
        }
    }
    //13761
    public function getBBTelecomTestInfo($techID)
    {
        $testInfo = $this->getBBTelecomTest_ByTech($techID);
        if(empty($testInfo) || count($testInfo)==0)
        {
            $testInfo = array();
            $testInfo['tech_id']=$techID;          
            $testInfo['Level'] = 0;
        }     
        //--- get info from tech_equiment
        $db = Core_Database::getInstance();  
        $query = $db->select();
        $query->from(array('te'=>"tech_equipment"),
                array('TechID','equipment_id')
        );
        $query->join(array('e'=>'equipment'),
                'e.id = te.equipment_id',
                array('name')
        );
        $query->where("TechID = '$techID'");
        $records = Core_Database::fetchAll($query);
        if(!empty($records) && count($records) > 0)
        {
            $techEquip = array();
            foreach($records as $rec){                
                $name = $rec['name'];
                $techEquip[$name] = $rec;                    
            }
            
            $map = $this->getMapping_BBTelecomTest_RequiredTools_Equiment();
            foreach($map as $k=>$v)
            {
                if($k!='T1RT_2' && $k!='T2RT_1') {
                    
                    $value = !empty($techEquip[$v]) ? 1 : 0;
                    $testInfo[$k] = $value;
                }
            }
        } 
        //--- get info from tech_skill
        $db = Core_Database::getInstance();  
        $query = $db->select();
        $query->from(array('ts'=>"tech_skill"),
                array('TechID','skill_id')
        );
        $query->join(array('s'=>'skills'),
                's.id = ts.skill_id',
                array('name')
        );
        $query->where("TechID = '$techID'");
        $records = Core_Database::fetchAll($query);
        if(!empty($records) && count($records) > 0)
        {
            $techSkill = array();
            foreach($records as $rec){                
                $name = $rec['name'];
                $techSkill[$name] = $rec;                    
            }                
            $map = $this->getMapping_BBTelecomTest_RequiredExperience_Skill();
            foreach($map as $k=>$v)
            {
                if($k!='T2RE_1') {                        
                    $value = !empty($techSkill[$v]) ? 1 : 0;
                    $testInfo[$k] = $value;
                }
            }
        } 
        
        return $testInfo;        
    }
    //13761
    public function getBBTelecomTest_ByTech($techID)
    {
        $db = Core_Database::getInstance();  
        $query = $db->select();
        $query->from("BlackBoxTelephony_Test");
        $query->where("tech_id = '$techID'");
        $records = Core_Database::fetchAll($query);
        if(!empty($records) || count($records) > 0) {
            return $records[0];
        } else {
            return null;
        }
    }
    //13761
    public function saveBBCablingTest($techID,$fieldValues)
    {
        if(empty($techID) || empty($fieldValues) || count($fieldValues)==0) { return; }
        //--- delete
        $db = Core_Database::getInstance();
        $db->delete('BlackBoxCabling_Test',"tech_id='$techID'");
        //--- insert to BlackBoxCabling_Test
        $fieldValues['tech_id'] = $techID;
        $result = Core_Database::insert("BlackBoxCabling_Test",$fieldValues);
        //--- update tech_equipment : TechID equipment_id
        $this->updateTechEquipment_from_BBCablingTest($techID,$fieldValues);
        //--- update tech_skill (TechID  skill_id)
        $this->updateTechSkill_from_BBCablingTest($techID,$fieldValues);
    }
    //13761
    public function saveBBTelephonyTest($techID,$fieldValues)
    {
        if(empty($techID) || empty($fieldValues) || count($fieldValues)==0) { return; }
        //--- delete
        $db = Core_Database::getInstance();
        $db->delete('BlackBoxTelephony_Test',"tech_id='$techID'");
        //--- insert to BlackBoxTelephony_Test
        $fieldValues['tech_id'] = $techID;
        $result = Core_Database::insert("BlackBoxTelephony_Test",$fieldValues);
        //--- update tech_equipment : TechID equipment_id
        $this->updateTechEquipment_from_BBTelecomTest($techID,$fieldValues);
        //--- update tech_skill (TechID  skill_id)
        $this->updateTechSkill_from_BBTelecomTest($techID,$fieldValues);                
    }
    
    
    //--- update tech_equipment 
    public function updateTechEquipment_from_BBCablingTest($techID,$BBCT_FieldValues)
    {
        $fieldValues = $BBCT_FieldValues;        
        $map_RequiredTools_Equiment = $this->getMapping_BBCablingTest_RequiredTools_Equiment();
        $BBCablingTest_RequiredTools = $this->getBBCablingTest_RequiredTools();
        $Level = $fieldValues['Level'];
        $RT_Fields_Level_1 = array('C1RT_1','C1RT_2','C1RT_3','C1RT_4','C1RT_5');
        $RT_Fields_Level_2 = array('C2RT_1','C2RT_2','C2RT_3','C2RT_4','C2RT_5','C2RT_6','C2RT_7','C2RT_8','C2RT_9','C2RT_10','C2RT_11');
        $RT_Fields_Level_3 = array('C3RT_1','C3RT_2','C3RT_3','C3RT_4');
        //echo("updateTechEquipment_from_BBCablingTest->Level: $Level");die();//test
        if($Level==1)
        {
            foreach($RT_Fields_Level_1 as $rqField) {
                $rqValue = $fieldValues[$rqField];
                $equiName = $map_RequiredTools_Equiment[$rqField];
                if(!empty($equiName)){
                    if($rqField != 'C1RT_2') {
                        $this->saveTechEquiment($techID,$equiName,$rqValue);                    
                    } else {
                        $equips = split(',',$equiName);
                        foreach($equips as $equip){
                            $this->saveTechEquiment($techID,$equip,$rqValue); 
    }
                    }
                }                
            }            
        }
        else if($Level==2)
        {
             foreach($RT_Fields_Level_2 as $rqField) {
                $rqValue = $fieldValues[$rqField];
                $equiName = $map_RequiredTools_Equiment[$rqField];
                if(!empty($equiName)){
                    if($rqField != 'C2RT_1') {
                        $this->saveTechEquiment($techID,$equiName,$rqValue);                    
                    } else if($rqValue == 1){
                        $equips = split(',',$equiName);
                        foreach($equips as $equip){
                            $this->saveTechEquiment($techID,$equip,$rqValue); 
                        }
                    }
                }                
            }            
        } 
        else if($Level==3)
        {            
             foreach($RT_Fields_Level_3 as $rqField) {
                 
                $rqValue = $fieldValues[$rqField];
                $equiName = $map_RequiredTools_Equiment[$rqField];
                if(!empty($equiName)){
                    if($rqField != 'C3RT_1') {
                        $this->saveTechEquiment($techID,$equiName,$rqValue);                    
                    } else if($rqValue == 1){
                        $equips = split(',',$equiName);
                        foreach($equips as $equip){
                            $this->saveTechEquiment($techID,$equip,$rqValue); 
                        }
                    }
                }                
            }                        
        }            
    }
    //--- update tech_skill 
    public function updateTechSkill_from_BBCablingTest($techID,$BBCT_FieldValues)
    {
        $fieldValues = $BBCT_FieldValues;                
        $map_RequiredExperiences_Skill = $this->getMapping_BBCablingTest_RequiredExperience_Skill();        
        $BBCablingTest_RequiredExperences = $this->getBBCablingTest_RequiredExperences();        
        $Level = $fieldValues['Level'];
        
        $RE_Fields_Level_1 = split(',',"C1RE_1,C1RE_2");
        $RE_Fields_Level_2 = split(',',"C2RE_1,C2RE_2,C2RE_3,C2RE_4");
        $RE_Fields_Level_3 = split(',',"C3RE_1,C3RE_2,C3RE_3");
        if($Level==1)
        {
            foreach($RE_Fields_Level_1 as $rqField) {
                $rqValue = $fieldValues[$rqField];
                $skillName = $map_RequiredExperiences_Skill[$rqField];
                if(!empty($skillName)){
                    $this->saveTechSkill($techID,$skillName,$rqValue);                    
                }                
            }            
        }
        else if($Level==2)
        {
             foreach($RE_Fields_Level_2 as $rqField) {
                $rqValue = $fieldValues[$rqField];
                $skillName = $map_RequiredExperiences_Skill[$rqField];
                if(!empty($skillName)){
                    if($rqField != 'C2RE_1') {
                        $this->saveTechSkill($techID,$skillName,$rqValue);
                    } else if($rqValue==1) {
                        $skills = split(',',$skillName);
                        foreach($skills as $skill){
                            $this->saveTechSkill($techID,$skill,$rqValue); 
                        }
                    }
                }                                 
             }            
        } 
        else if($Level==3)
        {            
             foreach($RE_Fields_Level_3 as $rqField) {
                $rqValue = $fieldValues[$rqField];
                $skillName = $map_RequiredExperiences_Equiment[$rqField];
                if(!empty($skillName)){
                    if($rqField != 'C3RE_1') {
                        $this->saveTechSkill($techID,$skillName,$rqValue);
                    } else if($rqValue==1) {
                        $skills = split(',',$skillName);
                        foreach($skills as $skill){
                            $this->saveTechSkill($techID,$skill,$rqValue); 
                        }
                    }
                }                                 
             }
        }            
    }
    
    //--- update tech_equipment 
    public function updateTechEquipment_from_BBTelecomTest($techID,$BBTT_FieldValues)
    {
        $fieldValues = $BBTT_FieldValues;                
        $map_RequiredTools_Equiment = $this->getMapping_BBTelecomTest_RequiredTools_Equiment();        
        $BBTelecomTest_RequiredTools = $this->getBBTelecomTest_RequiredTools();        
        $Level = $fieldValues['Level'];
        
        $RT_Fields_Level_1 = split(',',"T1RT_1,T1RT_2,T1RT_3,T1RT_4,T1RT_5");
        $RT_Fields_Level_2 = split(',',"T2RT_1,T2RT_2,T2RT_3,T2RT_4,T2RT_5,T2RT_6,T2RT_7,T2RT_8,T2RT_9,T2RT_10,T2RT_11,T2RT_12");
        if($Level==1)
        {
            foreach($RT_Fields_Level_1 as $rqField) {
                $rqValue = $fieldValues[$rqField];
                $equiName = $map_RequiredTools_Equiment[$rqField];
                if(!empty($equiName)){
                    if($rqField != 'T1RT_2') {
                        $this->saveTechEquiment($techID,$equiName,$rqValue);                    
                    } else {
                        $equips = split(',',$equiName);
                        foreach($equips as $equip){
                            $this->saveTechEquiment($techID,$equip,$rqValue); 
                        }
                    }
                }                
            }            
        }
        else if($Level==2)
        {
             foreach($RT_Fields_Level_2 as $rqField) {
                $rqValue = $fieldValues[$rqField];
                $equiName = $map_RequiredTools_Equiment[$rqField];
                if(!empty($equiName)){
                    if($rqField != 'T2RT_1') {
                        $this->saveTechEquiment($techID,$equiName,$rqValue);                    
                    } else if($rqValue == 1){
                        $equips = split(',',$equiName);
                        foreach($equips as $equip){
                            $this->saveTechEquiment($techID,$equip,$rqValue); 
                        }
                    }
                }                
            }//end for 
        } 
    }

    //--- update tech_skill 
    public function updateTechSkill_from_BBTelecomTest($techID,$BBTT_FieldValues)
    {
        $fieldValues = $BBTT_FieldValues;                        
        $map_RequiredExperiences_Skill = $this->getMapping_BBTelecomTest_RequiredExperience_Skill();        
        $BBTelecomTest_RequiredExperences = $this->getBBTelecomTest_RequiredExperences();        
        $Level = $fieldValues['Level'];
        
        $RE_Fields_Level_1 = split(',',"T1RE_1,T1RE_2,T1RE_3,T1RE_4,T1RE_5,T1RE_6");
        $RE_Fields_Level_2 = split(',',"T2RE_1,T2RE_2");
        if($Level==1)
        {
            foreach($RE_Fields_Level_1 as $rqField) {
                $rqValue = $fieldValues[$rqField];
                $skillName = $map_RequiredExperiences_Skill[$rqField];
                if(!empty($skillName)){
                    $this->saveTechSkill($techID,$skillName,$rqValue);                    
                }                
            }            
        }
        else if($Level==2)
        {
             foreach($RE_Fields_Level_2 as $rqField) {
                $rqValue = $fieldValues[$rqField];
                $skillName = $map_RequiredExperiences_Skill[$rqField];
                if(!empty($skillName)){
                    if($rqField != 'T2RE_1') {
                        $this->saveTechSkill($techID,$skillName,$rqValue);
                    } else if($rqValue==1) {
                        $skills = split(',',$skillName);
                        foreach($skills as $skill){
                            $this->saveTechSkill($techID,$skill,$rqValue); 
                        }
                    }
                }                                 
             }  // end for
        } 
    }
    
    
    public function saveTechEquiment($techID,$equiName,$value)
    {
        $equipmentID = $this->getEquipmentID_ByName($equiName);
        if(empty($equipmentID)){ return;}
        //$value = '',null,0,1
        if(empty($value)) { $value = 0;}
        
        if($value==0) {
            $db = Core_Database::getInstance();  
            $db->delete("tech_equipment","TechID = '$techID' AND equipment_id = '$equipmentID'");
        } else if($value==1) {
            if(!$this->existEquipment($techID,$equipmentID)) {
                Core_Database::insert("tech_equipment",
                            array('TechID'=>$techID,'equipment_id'=>$equipmentID)
                        );
            }
        }
    }

    public function saveTechSkill($techID,$skillName,$value)
    {
        $skillID = $this->getSkillID_ByName($skillName);
        if(empty($skillID)){ return;}
        //$value = '',null,0,1
        if(empty($value)) { $value = 0;}
        
        if($value==0) {
            $db = Core_Database::getInstance();  
            $db->delete("tech_skill","TechID = '$techID' AND skill_id = '$skillID'");
        } else if($value==1) {
            if(!$this->existSkill($techID,$skillID)) {
                Core_Database::insert("tech_skill",
                            array('TechID'=>$techID,'skill_id'=>$skillID)
                        );
            }
        }
    }
    
    public function getEquipmentID_ByName($equiName)
    {
        $db = Core_Database::getInstance();  
        $query = $db->select();
        $query->from("equipment",array('id'));
        $query->where("name = '$equiName'");
        $records = Core_Database::fetchAll($query);
        if(!empty($records) || count($records) > 0) {
            return $records[0]['id'];
        } else {
            return 0;
        }                        
    }
    public function getSkillID_ByName($skillName)
    {
        $db = Core_Database::getInstance();  
        $query = $db->select();
        $query->from("skills",array('id'));
        $query->where("name = '$skillName'");
        $records = Core_Database::fetchAll($query);
        if(!empty($records) || count($records) > 0) {
            return $records[0]['id'];
        } else {
            return 0;
        }                        
    }
    
    public function existEquipment($techID,$equipmentID)
    {
        $db = Core_Database::getInstance();  
        $query = $db->select();
        $query->from("tech_equipment");
        $query->where("TechID = '$techID'");
        $query->where("equipment_id = '$equipmentID'");
        $records = Core_Database::fetchAll($query);
        if(!empty($records) || count($records) > 0) {
            return true;
        } else {
            return false;
        }                        
    }

    public function existSkill($techID,$skillID)
    {
        $db = Core_Database::getInstance();  
        $query = $db->select();
        $query->from("tech_skill");
        $query->where("TechID = '$techID'");
        $query->where("skill_id = '$skillID'");
        $records = Core_Database::fetchAll($query);
        if(!empty($records) || count($records) > 0) {
            return true;
        } else {
            return false;
        }                        
    }
    
    private function getBBCablingTest_RequiredTools()
    {
        $str = "C1RT_1,C1RT_2,C1RT_3,C1RT_4,C1RT_5,C2RT_1,C2RT_2,C2RT_3,C2RT_4,C2RT_5,C2RT_6,C2RT_7,C2RT_8,C2RT_9,C2RT_10,C2RT_11,C3RT_1,C3RT_2,C3RT_3,C3RT_4";
        return split(',',$str);
        
    }    
    private function getBBTelecomTest_RequiredTools()
    {
        $str = "T1RT_1,T1RT_2,T1RT_3,T1RT_4,T1RT_5,T2RT_1,T2RT_2,T2RT_3,T2RT_4,T2RT_5,T2RT_6,T2RT_7,T2RT_8,T2RT_9,T2RT_10,T2RT_11,T2RT_12";
        return split(',',$str);        
    }    
    private function getBBCablingTest_RequiredExperences()
    {
        $str = "C1RE_1,C1RE_2,C2RE_1,C2RE_2,C2RE_3,C2RE_4,C3RE_1,C3RE_2,C3RE_3";
        return split(',',$str);
    }    
    private function getBBTelecomTest_RequiredExperences()
    {
        $str = "T1RE_1,T1RE_2,T1RE_3,T1RE_4,T1RE_5,T1RE_6,T2RE_1,T2RE_2";
        return split(',',$str);
    }    
    private function getMapping_BBCablingTest_RequiredTools_Equiment()
    {
        /*C1RT_1     ProtectionEquipment
 C1RT_2    PunchTool66,PunchTool110,PunchToolBix,PunchToolKrone
 C1RT_3    HandTools
 C1RT_4    CordlessDrill
 C1RT_5    MachineLabelMaker
 
 C2RT_1    ProtectionEquipment,PunchTool66,PunchTool110,PunchToolBix,PunchToolKrone,HandTools,CordlessDrill,MachineLabelMaker
 C2RT_2   ToneGeneratorAndWand
 C2RT_3    DigitalVOMMeter
 C2RT_4   CordMasonryDrillHammer
 C2RT_5   FishTape50
 C2RT_6   CableTesters
 C2RT_7   GoferPoles
 C2RT_8   ButtSet
 C2RT_9   CellPhone
 C2RT_10  DigitalCam
 C2RT_11 Ladder_6
 
 C3RT_1    ProtectionEquipment,PunchTool66,PunchTool110,PunchToolBix,PunchToolKrone,HandTools,CordlessDrill,MachineLabelMaker,ToneGeneratorAndWand,DigitalVOMMeter,CordMasonryDrillHammer,FishTape50,CableTesters,GoferPoles,ButtSet,CellPhone,DigitalCam,Ladder_6
 C3RT_2  FiberOpticdBLossMeter
 C3RT_3  Laptop
 C3RT_4  FiberOpticTerminationKit*/
        return array('C1RT_1'=>'ProtectionEquipment','C1RT_2'=>'PunchTool66,PunchTool110,PunchToolBix,PunchToolKrone','C1RT_3'=>'HandTools','C1RT_4'=>'CordlessDrill','C1RT_5'=>'MachineLabelMaker','C2RT_1'=>'ProtectionEquipment,PunchTool66,PunchTool110,PunchToolBix,PunchToolKrone,HandTools,CordlessDrill,MachineLabelMaker','C2RT_2'=>'ToneGeneratorAndWand','C2RT_3'=>'DigitalVOMMeter','C2RT_4'=>'CordMasonryDrillHammer','C2RT_5'=>'FishTape50','C2RT_6'=>'CableTesters','C2RT_7'=>'GoferPoles','C2RT_8'=>'ButtSet','C2RT_9'=>'CellPhone','C2RT_10'=>'DigitalCam','C2RT_11'=>'Ladder_6','C3RT_1'=>'ProtectionEquipment,PunchTool66,PunchTool110,PunchToolBix,PunchToolKrone,HandTools,CordlessDrill,MachineLabelMaker,ToneGeneratorAndWand,DigitalVOMMeter,CordMasonryDrillHammer,FishTape50,CableTesters,GoferPoles,ButtSet,CellPhone,DigitalCam,Ladder_6','C3RT_2'=>'FiberOpticdBLossMeter','C3RT_3'=>'Laptop','C3RT_4'=>'FiberOpticTerminationKit');
    }
    
    private function getMapping_BBCablingTest_RequiredExperience_Skill()
    {
        /*
 C1RE_1,C1RE_2
C1RE_1    OperationScissorLiftExperience
C1RE_2    UnderstandTelecomColorCodeExperience

C2RE_1,C2RE_2,C2RE_3,C2RE_4
C2RE_1    OperationScissorLiftExperience,UnderstandTelecomColorCodeExperience
C2RE_2    InstallationMultipleCableExperience
C2RE_3    InstallingSleevesRacewaysExperience
C2RE_4    InstallationVoiceDataEquipmentExperience
        
C3RE_1,C3RE_2,C3RE_3
C3RE_1    OperationScissorLiftExperience,UnderstandTelecomColorCodeExperience,InstallationMultipleCableExperience,InstallingSleevesRacewaysExperience,InstallationVoiceDataEquipmentExperience
C3RE_2    SingleAndMultiModeTerminationTestingExp
C3RE_3    UsingLaptopForSwitchConfigurationExp
*/
       return array('C1RE_1'=>'OperationScissorLiftExperience','C1RE_2'=>'UnderstandTelecomColorCodeExperience','C2RE_1'=>'OperationScissorLiftExperience,UnderstandTelecomColorCodeExperience','C2RE_2'=>'InstallationMultipleCableExperience','C2RE_3'=>'InstallingSleevesRacewaysExperience','C2RE_4'=>'InstallationVoiceDataEquipmentExperience','C3RE_1'=>'OperationScissorLiftExperience,UnderstandTelecomColorCodeExperience,InstallationMultipleCableExperience,InstallingSleevesRacewaysExperience,InstallationVoiceDataEquipmentExperience','C3RE_2'=>'SingleAndMultiModeTerminationTestingExp','C3RE_3'=>'UsingLaptopForSwitchConfigurationExp');
    }
    
    private function getMapping_BBTelecomTest_RequiredTools_Equiment()
    {
        /*
T1RT_1,T1RT_2,T1RT_3,T1RT_4,T1RT_5,T2RT_1,T2RT_2,T2RT_3,T2RT_4,T2RT_5,T2RT_6,T2RT_7,T2RT_8,T2RT_9,T2RT_10,T2RT_11,T2RT_12

T1RT_1,T1RT_2,T1RT_3,T1RT_4,T1RT_5
T1RT_1    ProtectionEquipment
T1RT_2    PunchTool66,PunchTool110,PunchToolBix,PunchToolKrone
T1RT_3    HandTools
T1RT_4    CordlessDrill
T1RT_5    MachineLabelMaker

T2RT_1,T2RT_2,T2RT_3,T2RT_4,T2RT_5,T2RT_6,T2RT_7,T2RT_8,T2RT_9,T2RT_10,T2RT_11,T2RT_12
T2RT_1    ProtectionEquipment,PunchTool66,PunchTool110,PunchToolBix,PunchToolKrone,HandTools,CordlessDrill,MachineLabelMaker
T2RT_2   ToneGeneratorAndWand
T2RT_3    DigitalVOMMeter
T2RT_4   CordMasonryDrillHammer
T2RT_5   FishTape50
T2RT_6   CableTesters
T2RT_7   GoferPoles
T2RT_8   ButtSet
T2RT_9   CellPhone
T2RT_10  DigitalCam
T2RT_11 Ladder_6
T2RT_12    Laptop 
*/
        return array('T1RT_1'=>'ProtectionEquipment','T1RT_2'=>'PunchTool66,PunchTool110,PunchToolBix,PunchToolKrone','T1RT_3'=>'HandTools','T1RT_4'=>'CordlessDrill','T1RT_5'=>'MachineLabelMaker','T2RT_1'=>'ProtectionEquipment,PunchTool66,PunchTool110,PunchToolBix,PunchToolKrone,HandTools,CordlessDrill,MachineLabelMaker','T2RT_2'=>'ToneGeneratorAndWand','T2RT_3'=>'DigitalVOMMeter','T2RT_4'=>'CordMasonryDrillHammer','T2RT_5'=>'FishTape50','T2RT_6'=>'CableTesters','T2RT_7'=>'GoferPoles','T2RT_8'=>'ButtSet','T2RT_9'=>'CellPhone','T2RT_10'=>'DigitalCam','T2RT_11'=>'Ladder_6','T2RT_12'=>'Laptop');
    }
    
    private function getMapping_BBTelecomTest_RequiredExperience_Skill()
    {
        /*
* T1RE_1,T1RE_2,T1RE_3,T1RE_4,T1RE_5,T1RE_6,T2RE_1,T2RE_2

T1RE_1,T1RE_2,T1RE_3,T1RE_4,T1RE_5,T1RE_6
T1RE_1    OperationScissorLiftExperience
T1RE_2    UnderstandTelecomColorCodeExperience
T1RE_3    InstallationMultipleCableExperience
T1RE_4    InstallingSleevesRacewaysExperience
T1RE_5    InstallationVoiceDataEquipmentExperience
T1RE_6    DMARCRoomsEquipmentExperience

T2RE_1,T2RE_2
T2RE_1    OperationScissorLiftExperience,UnderstandTelecomColorCodeExperience,InstallationMultipleCableExperience,InstallingSleevesRacewaysExperience,InstallationVoiceDataEquipmentExperience,DMARCRoomsEquipmentExperience
T2RE_2    OperationPBXExperience

*/
       return array('T1RE_1'=>'OperationScissorLiftExperience','T1RE_2'=>'UnderstandTelecomColorCodeExperience','T1RE_3'=>'InstallationMultipleCableExperience','T1RE_4'=>'InstallingSleevesRacewaysExperience','T1RE_5'=>'InstallationVoiceDataEquipmentExperience','T1RE_6'=>'DMARCRoomsEquipmentExperience','T2RE_1'=>'OperationScissorLiftExperience,UnderstandTelecomColorCodeExperience,InstallationMultipleCableExperience,InstallingSleevesRacewaysExperience,InstallationVoiceDataEquipmentExperience,DMARCRoomsEquipmentExperience','T2RE_2'=>'OperationPBXExperience');
    }
 
 
    //13761
    private function getTechIdArray_ArePendingForBBCablingTag_ByLevel($level)
    {
        //$level: 1: Helper, 2: Advanced, 3: Lead
        $techIds_haveBBCablingTag =  $this->getTechIds_haveBBCablingTag();
        $db = Core_Database::getInstance();  
        $query = $db->select();
        $query->from("BlackBoxCabling_Test",
                        array('tech_id')
        );                
        $query->where("Level = '$level'");
        $query->where("tech_id NOT IN ($techIds_haveBBCablingTag)");
        $records = Core_Database::fetchAll($query);
        $techIDArray = array();
        if(!empty($records) || count($records) > 0) {
            foreach($records as $rec) {
                $techIDArray[]= $rec['tech_id']; 
            }                
        }
        return $techIDArray;
    }
    //13761
    private function getTechIdArray_ArePendingForBBTelecomTag_ByLevel($level)
    {
        //$level: 1: Helper, 2: Advanced
        $techIds_haveBBTelecomTag =  $this->getTechIds_haveBBTelecomTag();
        $db = Core_Database::getInstance();  
        $query = $db->select();
        $query->from("BlackBoxTelephony_Test",
                        array('tech_id')
        );                
        $query->where("Level = '$level'");
        $query->where("tech_id NOT IN ($techIds_haveBBTelecomTag)");
        $records = Core_Database::fetchAll($query);
        $techIDArray = array();
        if(!empty($records) || count($records) > 0) {
            foreach($records as $rec) {
                $techIDArray[]= $rec['tech_id']; 
            }                
        }
        return $techIDArray;
    }
    //13761
    private function getTechIds_haveBBTelecomTag()
    {
        $db = Core_Database::getInstance();  
        $query = $db->select();
        $query->from("tech_certification",
                        array('TechID','certification_id','date')
        );                
        $query->where("certification_id IN (".self::BBTelecomCertIDs.")");
        $records = Core_Database::fetchAll($query);
        if(empty($records) || count($records)==0) {
                return "0";
        } else {
            $techIDs = "";
            foreach($records as $rec)
            {
                if(!empty($techIDs)) $techIDs .= ',';
                $techIDs .= $rec['TechID'];                
            }
            return $techIDs;
        }                 
    }
    //13761
    private function getTechIds_haveBBCablingTag()
    {
        $db = Core_Database::getInstance();  
        $query = $db->select();
        $query->from("tech_certification",
                        array('TechID','certification_id','date')
        );                
        $query->where("certification_id IN (".self::BBCablingCertIDs.")");
        $records = Core_Database::fetchAll($query);
        if(empty($records) || count($records)==0) {
                return "0";
        } else {
            $techIDs = "";
            foreach($records as $rec)
            {
                if(!empty($techIDs)) $techIDs .= ',';
                $techIDs .= $rec['TechID'];                
            }
            return $techIDs;
        }                 
    }

    //--- 13761
    /*
    $level = 1: C1, 2: C2, 3: C3
    */    
    public function getTechList_RequestBBCablingTag($level)   
    {
        $techIds_haveBBCablingTag =  $this->getTechIds_haveBBCablingTag();//13799
        $db = Core_Database::getInstance();  
        $query = $db->select();
        $query->from(array('bbt'=>'BlackBoxCabling_Test'),
                    array('tech_id','date_taken','Level')
        );        
        $query->join(array('t'=>Core_Database::TABLE_TECH_BANK_INFO),
                't.TechID = bbt.tech_id',
                array('FirstName','LastName','PrimaryPhone','PrimaryEmail')
        );
        $query->where("Level = '$level'");
        if(!empty($techIds_haveBBCablingTag)) {     
		    $query->where("tech_id NOT IN ($techIds_haveBBCablingTag)");//13799
        }        
        $query->order("tech_id ASC");            
        $records = Core_Database::fetchAll($query); 
        return $records;
    }
    //13799 
    /*
    $level = 1: T1, 2: T2
    */    
    public function getTechList_RequestBBTelephonyTag($level)   
    {
        $techIds_haveBBTelecomTag =  $this->getTechIds_haveBBTelecomTag();//13799
        $db = Core_Database::getInstance();  
        $query = $db->select();
        $query->from(array('bbt'=>'BlackBoxTelephony_Test'),
                    array('tech_id','date_taken','Level')
        );        
        $query->join(array('t'=>Core_Database::TABLE_TECH_BANK_INFO),
                't.TechID = bbt.tech_id',
                array('FirstName','LastName','PrimaryPhone','PrimaryEmail')
        );
        $query->where("Level = '$level'");
        if(!empty($techIds_haveBBTelecomTag)){
            $query->where("tech_id NOT IN ($techIds_haveBBTelecomTag)");//13799            
        }        
        $query->order("tech_id ASC");            
        $records = Core_Database::fetchAll($query); 
        return $records;
    }
    
    //13700
    public function updateSilverDrugFromGoldDrug($techID)
    {
        $db = Core_Database::getInstance();  
        $query = $db->select();
        $query->from(Core_Database::TABLE_TECH_BANK_INFO,
                array('TechID','DrugPassed','DrugTest_Pass','DatePassDrug','SilverDrugPassed')
        );        
        $query->where("TechID = '$techID'");
        $records = Core_Database::fetchAll($query); 
        if(!empty($records) && count($records)>0){
            $rec = $records[0];
            $drugPassed = $rec['DrugPassed'];
            $silverDrugPassed = $rec['SilverDrugPassed'];
            if($drugPassed==1 && empty($silverDrugPassed)){
                $criteria = "TechID = '$techID'";
                $updateFields = array();
                $updateFields['SilverDrugPassed']=$rec['DrugPassed'];
                $updateFields['SilverDrugTest_Pass']=$rec['DrugTest_Pass'];
                $updateFields['DatePassedSilverDrug']=$rec['DatePassDrug'];
                Core_Database::update(Core_Database::TABLE_TECH_BANK_INFO,$updateFields,$criteria);
            }
        }
    }

    //13702
    public function getTechSourceList($excludeArray='')
    {
        $sourceList = array('AnyoneHiring.com','Beyond.com','Craigslist.com','Dice.com','GetHired.com','Indeed.com','SimplyHired.com','All Search Engine Results','Search Engine');
        foreach($this->_subfolders as $key=>$value){
            $sourceList[]='Search Engine - '.$value;
}
        $sourceList[] = 'Contacted by FieldSolutions';
        $sourceList[] = 'Referral - Client';
        $sourceList[] = 'Referral - Technician';
        $sourceList[] = 'All Private Techs';
        foreach($this->_subdomains as $key=>$value){
            $sourceList[]='Private Tech - '.$value;
        }
        $sourceList[] = 'LinkedIn';
        $sourceList[] = 'Facebook';
        $sourceList[] = 'Twitter';
        $sourceList[] = 'Internet Forum';
        $sourceList[] = 'College Job Boards';
        $sourceList[] = 'Local Employment Agency';
        $sourceList[] = 'Misc Job Search Site';
        $sourceList[] = 'ISO';
        $sourceList[] = 'Other';

        $ret = array();
        if(empty($excludeArray)){
             $ret = $sourceList;
        } else if(is_array($excludeArray) && count($excludeArray) > 0)  {
            foreach($sourceList as $source){
                if(!in_array($source,$excludeArray)){
                    $ret[] = $source;
                }
            }                
        }         
        return $ret;
    }
    
    //13702
    public function getTechSourceList_forRegisterPage()
    {
        $sourceList = array('AnyoneHiring.com',
'Beyond.com',
'Craigslist.com',
'Dice.com',
'GetHired.com',
'Indeed.com',
'SimplyHired.com',
'Search Engine',
'Contacted by FieldSolutions',
'Referral - Client',
'Referral - Technician',
'LinkedIn',
'Facebook',
'Twitter',
'Internet Forum',
'College Job Boards',
'Local Employment Agency',
'Misc Job Search Site',
'ISO',
'Other'
);
        return $sourceList;
    }
//14145
public function getTechSourceList_forAdmin()
    {
        $sourceList = array('AnyoneHiring.com',
'Beyond.com',
'Craigslist.com',
'Dice.com',
'GetHired.com',
'Indeed.com',
'SimplyHired.com',
'Contacted by FieldSolutions',
'Referral - Client',
'Referral - Technician',
'LinkedIn',
'Facebook',
'Twitter',
'Internet Forum',
'College Job Boards',
'Local Employment Agency',
'Misc Job Search Site',
'ISO',
'Other'
);
        return $sourceList;
    }
//14145
    
    //13702
    public function getTechSource($httpHost,$requestUri)
    {      
    /*$httpHost = $_SERVER['HTTP_HOST'];
$requestUri = $_SERVER["REQUEST_URI"];*/  
        //--- Get subDomain
        $urlParts = explode('.', $httpHost);
        $subDomain = $urlParts[0];
        $subDomain = strtolower($subDomain);
        //--- Get subFolder
        $requestUriParts = explode('/',$requestUri);
        $subFolder = '';
        if(count($requestUriParts)>1){
            $subFolder = $requestUriParts[1];
        }
        $subFolder = strtolower($subFolder);
        //--- TechSource
        //(recruiting.fieldsolutions.com/welcometechs), pass ?Contacted by FieldSolutions?
        $techSource="";
        if($subDomain=='recruiting' && $subFolder=='welcometechs'){
            $techSource = 'Contacted by FieldSolutions';
        } else if(in_array($subDomain,$this->_subdomains)){
            //894
            //this code fixed for 13702 run correctly.
            if($subDomain !='test')
            {
                $techSource = 'Private Tech - '.$this->_subdomains[$subDomain];
            }
            //end 894
        } else if(in_array($subDomain,$this->_subfolders)){
            $techSource = 'Search Engine - '.$this->_subfolders[$subDomain];
        }
        return $techSource;
    }
    //894
    public function getTechSourcenew($httpHost,$requestUri,$v)
    {      
        //--- Get subDomain
        $urlParts = explode('.', $httpHost);
        $subDomain = $urlParts[0];
        $subDomain = strtolower($subDomain);
        //--- Get subFolder
        $requestUriParts = explode('/',$requestUri);
        $subFolder = '';
        if(count($requestUriParts)>1){
            $subFolder = $requestUriParts[1];
        }
        $subFolder = strtolower($subFolder);
        //--- TechSource
        //(recruiting.fieldsolutions.com/welcometechs), pass ?Contacted by FieldSolutions?
        $techSource="";
        if($subDomain=='recruiting' && $subFolder=='welcometechs'){
            $techSource = 'Contacted by FieldSolutions';
        } else if(in_array($subDomain,$this->_subdomains)){
            //894
            if($subDomain =='test' && ($v == "PITBC"))
            {
                $techSource = 'Private Tech - '.$this->_subdomains['pb'];
            }
            else
            {
                $techSource = 'Private Tech - '.$this->_subdomains[$subDomain];
            }
            //end 894
        } else if(in_array($subDomain,$this->_subfolders)){
            $techSource = 'Search Engine - '.$this->_subfolders[$subDomain];
        }
        return $techSource;
    }
    //end 894
    
    //13970
    public function applyDenyTechRule()
    {
        $companyIDArray = $this->getCompanyIDs_haveDenyTechRules();
        if(!empty($companyIDArray) && count($companyIDArray)>0){
            foreach($companyIDArray as $companyID){
                $this->applyDenyTechRule_ByCompany($companyID);
            }
        }
    }
    
    //13970
    public function getCompanyIDs_haveDenyTechRules()
    {
        $db = Core_Database::getInstance();
        $query = $db->select();
        $query->from("clients",array('Company_ID'));
        $query->orWhere("DeniedTech_NoShowGreater > 0"); 
        $query->orWhere("DeniedTech_BackOutGreater > 0"); 
        $query->orWhere("DeniedTech_PreferenceRatingSmaller > 0"); 
        $query->orWhere("DeniedTech_PerformanceRatingSmaller > 0"); 
        $query->group("Company_ID");
        $records = Core_Database::fetchAll($query);

        $companyIDArray = array();
        if(!empty($records) && count($records)>0){
            foreach($records as $rec){
                $companyIDArray[] = $rec['Company_ID'];
            }
        }
        
        return $companyIDArray;
    }
    
    //13970
    public function getInterval($period)
    {
        if($period==self::Period_Last90Days){
            return self::Interval_Last90Days;
        } else if($period==self::Period_Last12Months){
            return self::Interval_Last12Months;
        } else if($period==self::Period_Lifetime){
            return self::Interval_Lifetime;
        } else {
            return "";
        }
    }
    
    //--- 13970
    public function applyDenyTechRule_ByCompany($companyID)
    {
        $autoDenyComment = 'auto deny';

        $Core_User = new Core_Api_User();
        $Core_Comment = new Core_Comments();

        $clientInfo = $Core_User->getRepClientforCompany($companyID,true);
        
        //print_r($_SESSION);die();
        $clientID = 0;
        if(!empty($_SESSION['ClientID'])){
            $clientID = $_SESSION['ClientID'];
        } else {
            $clientID = $clientInfo['ClientID'];
        }
        
        $noShowGreater = $clientInfo['DeniedTech_NoShowGreater'];
        $noShowPeriod = $clientInfo['DeniedTech_NoShowPeriod'];        
        $backOutGreater = $clientInfo['DeniedTech_BackOutGreater'];
        $backOutPeriod = $clientInfo['DeniedTech_BackOutPeriod'];        
        $prefRateSmaller = $clientInfo['DeniedTech_PreferenceRatingSmaller'];
        $prefRatePeriod = $clientInfo['DeniedTech_PreferenceRatingPeriod'];        
        $perfRateSmaller = $clientInfo['DeniedTech_PerformanceRatingSmaller'];
        $perfRatePeriod = $clientInfo['DeniedTech_PerformanceRatingPeriod'];

        $noShowInterval = $this->getInterval($noShowPeriod);  
        $backOutInterval = $this->getInterval($backOutPeriod);  
        $prefRateInterval = $this->getInterval($prefRatePeriod);  
        $perfRateInterval = $this->getInterval($perfRatePeriod);  
        
        $availPeriods = array(self::Period_Last90Days,
                        self::Period_Last12Months,
                        self::Period_Lifetime);
        
        //--- currentAutoDenyList
        $db = Core_Database::getInstance();
        $query = $db->select();
        $query->from("client_denied_techs",array('TechID'));
        $query->where("Comments = '$autoDenyComment'"); 
        $query->where("Company_ID = '$companyID'"); 
        $query->group("TechID");
        $records = Core_Database::fetchAll($query);
        $currentAutoDenyList = array();
        if(!empty($records) && count($records)>0){
            foreach($records as $rec){
                $currentAutoDenyList[] = $rec['TechID'];
            }
        }

        
        if(empty($noShowGreater) && empty($backOutGreater) && empty($prefRateSmaller) && empty($perfRateSmaller)){

        //echo("currentAutoDenyList: ");print_r($currentAutoDenyList);return;

            $db = Core_Database::getInstance();
            $criteria = "Company_ID = '$companyID' AND Comments='$autoDenyComment'";
            $result = $db->delete('client_denied_techs',$criteria);
            
            //--- insert comment
            if(count($currentAutoDenyList) > 0){
                $comment = 'This technician is no longer client denied';
                foreach($currentAutoDenyList as $techID){
                        $Core_Comment->addComment($comment,$clientID,$techID,$companyID,null,Core_Comments::TYPE_CLIENT,Core_Comments::CLASSIFICATION_FAVORABLE);
                }
            }
            return;
        }
        
        //$companyID,$perfRateSmaller,$perfRateInterval
        //echo('<br/>companyID: '.$companyID.', $perfRateSmaller: '.$perfRateSmaller.',$perfRateInterval: '.$perfRateInterval);return;
        //echo('applyDenyTechRule_ByCompany->clientInfo: ');print_r($clientInfo);return;
        //echo('applyDenyTechRule_ByCompany->clientInfo: ');print_r($clientInfo);
        //echo('<br/>noShowGreater: ');print_r($noShowGreater);
        //echo('<br/>backOutGreater: ');print_r($backOutGreater);
        //echo('<br/>prefRateSmaller: ');print_r($prefRateSmaller);
        //echo('<br/>perfRateSmaller: ');print_r($perfRateSmaller);
        
        $Core_TechStats = new Core_TechStats();
                
        $denyTechIDs_NoShow = array();
        if(!empty($noShowGreater) && $noShowGreater > 0) {
            $denyTechIDs_NoShow = Core_TechStats::getDenyTechIDArray_NoShowGreater($companyID,$noShowGreater,$noShowInterval);   
        }
                        
        $denyTechIDs_BackOut = array();
        if(!empty($backOutGreater) && $backOutGreater > 0) {
            $denyTechIDs_BackOut = Core_TechStats::getDenyTechIDArray_BackOutGreater($companyID,$backOutGreater,$backOutInterval); 
        }
            
        $denyTechIDs_PrefRating = array();
        if(!empty($prefRateSmaller) && $prefRateSmaller > 0) {
            $denyTechIDs_PrefRating = Core_TechStats::getDenyTechIDArray_PreferenceRatingSmaller($companyID,$prefRateSmaller,$prefRateInterval);                
        }
            
        $denyTechIDs_PerfRating = array();
        if(!empty($perfRateSmaller) && $perfRateSmaller > 0) {
            $denyTechIDs_PerfRating = Core_TechStats::getDenyTechIDArray_PerformanceRatingSmaller($companyID,$perfRateSmaller,$perfRateInterval);                
        }
            
        $denyTechIDs = array_merge($denyTechIDs_NoShow,$denyTechIDs_BackOut,$denyTechIDs_PrefRating,$denyTechIDs_PerfRating);
        $denyTechIDs = array_unique($denyTechIDs);
            
        //echo("<br/>denyTechIDs: ");echo(count($denyTechIDs));
        //die();
                    
        if(count($denyTechIDs) > 0){
            $autoDenyComment = 'auto deny';
            
            $db = Core_Database::getInstance();
            $query = $db->select();
            $query->from("client_denied_techs",array('TechID'));
            $query->where("Company_ID = '$companyID'"); 
            $query->group("TechID");
            $records = Core_Database::fetchAll($query);
            $currentDenyList = array();
            if(!empty($records) && count($records)>0){
                foreach($records as $rec){
                    $currentDenyList[] = $rec['TechID'];
                }
            }
            
            $denyTechIDs_1 = array();
            $noDenyTech = array();
            if(count($currentDenyList) > 0){
                $equ = array_intersect ($denyTechIDs, $currentDenyList); 
                $denyTechIDs_1 = array_diff($denyTechIDs,$currentDenyList);
                $noDenyTech = array_diff($currentDenyList,$equ);
            } else {
                $denyTechIDs_1 = $denyTechIDs;
            }
           
            
           //echo('<br/>count(currentDenyList): ');echo(count($currentDenyList));
           //echo('<br/>count(denyTechIDs): ');echo(count($denyTechIDs));
           //echo('<br/>count($noDenyTech): ');echo(count($noDenyTech));
           //echo('<br/>count($denyTechIDs_1): ');echo(count($denyTechIDs_1));
           //echo('<br/>$noDenyTech: ');print_r($noDenyTech);
           //echo('<br/>$denyTechIDs_1: ');print_r($denyTechIDs_1);           
           //return;
           
            //--- insert            
            if(count($denyTechIDs_1) > 0)
            {
                foreach($denyTechIDs_1 as $techID){
                    Core_Database::insert("client_denied_techs",
                                array("TechID"=>$techID,
                                        'Comments'=>$autoDenyComment,
                                        'Company_ID'=>$companyID));                             
                //--- insert comment
                    $comment = "";
                    if(in_array($techID,$denyTechIDs_NoShow)){
                       $comment.= empty($comment)? "too many No Shows":", too many No Shows";
                    }
                    if(in_array($techID,$denyTechIDs_BackOut)){
                       $comment.= empty($comment)? "too many Back Outs":", too many Back Outs";
                    }
                    if(in_array($techID,$denyTechIDs_PrefRating)){
                       $comment.= empty($comment)? "low Preference Rating":", low Preference Rating";
                    }
                    if(in_array($techID,$denyTechIDs_PerfRating)){
                       $comment.= empty($comment)? "low Performance Rating":", low Performance Rating";
                    }                   
                    $comment = "Automatically Client Denied due to ".$comment;
                    
                    $Core_Comment->addComment($comment,$clientID,$techID,$companyID,null,Core_Comments::TYPE_CLIENT,Core_Comments::CLASSIFICATION_UNFAVORABLE);
                    
                }//end for     
                               
            }
            
            //--- delete
            if(count($noDenyTech) > 0)
            {
                $criteria = "Company_ID = '$companyID'";
                $criteria .= " AND TechID IN (". implode(',',$noDenyTech).")";
                $result = $db->delete('client_denied_techs',$criteria);
                //--- insert comment
                $comment = 'This technician is no longer client denied';
                foreach($noDenyTech as $techID){
                    $Core_Comment->addComment($comment,$clientID,$techID,$companyID,null,Core_Comments::TYPE_CLIENT,Core_Comments::CLASSIFICATION_FAVORABLE);
                }
            }
            
        } else {
            $db = Core_Database::getInstance();
            $criteria = "Company_ID = '$companyID' AND Comments='$autoDenyComment'";
            $result = $db->delete('client_denied_techs',$criteria);
        }//end of if
            
    } //end of function    
    //13932
    public function updateGovernmentSecurity($techID,$status,$statusDate,$type)
    {                   
            if (!empty($statusDate)) {
                    $statusDate = new Zend_Date($statusDate,'mm/dd/YYYY');
                    $statusDate = $statusDate->toString('YYYY-mm-dd');
            } else {
                    $statusDate = NULL;
            }	
            
            $criteria = "TechID = '$techID'";
            if($type==1)
            {
                $updateFields = array();
                $updateFields['InterimSecClear']=$status;
                $updateFields['InterimSecClearDate']=$statusDate;   
            }else if($type==2) 
            {
                $updateFields = array();
                $updateFields['TopSecClear']=$status;
                $updateFields['TopSecClearDate']=$statusDate; 
            }else if($type==3)
            {
                $updateFields = array();
                $updateFields['FullSecClear']=$status;
                $updateFields['FullSecClearDate']=$statusDate; 
            }    
            Core_Database::update(Core_Database::TABLE_TECH_BANK_INFO,$updateFields,$criteria);               
    }
     //14061
   public function getTechsByISO($techId,$winNum=''){
    	if(empty($techId)) return null; 
        
        $CoreApiWosClass = new Core_Api_WosClass();
        
        if(!empty($winNum)){
            $woInfo = $CoreApiWosClass->getWoInfo($winNum,
                array('Latitude','Longitude')
            );
            if(!empty($woInfo)){
                $woLatitude = $woInfo['Latitude'];
                $woLongtitude = $woInfo['Longitude'];
            }
        }                
    	$db = Core_Database::getInstance();
        $query = $db->select();
        $fieldArray=array('TechID','FirstName','LastName','City','ZipCode','State','Latitude','Longitude');
        $query->from(array("t"=>Core_Database::TABLE_TECH_BANK_INFO),$fieldArray);	
        
        if(!empty($winNum) && !empty($woLatitude) && !empty($woLongtitude)){
            $query = Core_Database::whereProximity($query, $woInfo['Latitude'], $woInfo['Longitude'], "Latitude", "Longitude", -1, "Distance",true);
        }
        
        $query->where('t.ISO_Affiliation_ID IN (SELECT ISO_Affiliation_ID FROM TechBankInfo AS z WHERE z.TechID = ?)', $techId);
        $query->order('LastName');
        $query->order('FirstName');
        $records = Core_Database::fetchAll($query);
        if(empty($records)) return null;
        return $records;
    }
    
    public function getClosestTech($repTechID,$winNum)
    {
        if(empty($winNum)) {
            return $this->getTechInfoByID($repTechID);
        }
        
        $CoreApiWosClass = new Core_Api_WosClass(); 
        $woInfo = $CoreApiWosClass->getWoInfo($winNum,
            array('Latitude','Longitude')
        );
        if(!empty($woInfo)){
            $woLatitude = $woInfo['Latitude'];
            $woLongtitude = $woInfo['Longitude'];
        } 
        if(empty($woLatitude) || empty($woLongtitude)) {
            return $this->getTechInfoByID($repTechID);
        }
        
        $db = Core_Database::getInstance();
        $query = $db->select();
        $fieldArray=array('TechID','FirstName','LastName','City','ZipCode','State');
        $query->from(array("t"=>Core_Database::TABLE_TECH_BANK_INFO),$fieldArray);    
        
        $query = Core_Database::whereProximity($query, $woInfo['Latitude'], $woInfo['Longitude'], "Latitude", "Longitude", -1, "Distance",true);
                
        $query->where('t.ISO_Affiliation_ID IN (SELECT ISO_Affiliation_ID FROM TechBankInfo AS z WHERE z.TechID = ?)', $repTechID);
        $query->order('Distance ASC');
        $records = Core_Database::fetchAll($query);
        if(!empty($records) && count($records)>0){
            return  $records[0];   
        } else {
            return $this->getTechInfoByID($repTechID);
        }
    }
    
    public function getTechInfoByID($techID)
    {
		$fieldArray=array('TechID','FirstName','LastName','City','ZipCode','State','Dispatch','ISO_Affiliation','UserName','Password','ISO_Affiliation_ID');
        $db = Core_Database::getInstance();  
        $query = $db->select();
        $query->from(Core_Database::TABLE_TECH_BANK_INFO, $fieldArray);
        $query->where("TechID = ?", $techID);
        $result = Core_Database::fetchAll($query);        
        if(empty($result)) return null;
        return $result[0];
    }
    
    public function uploadSurveySimple($surveyID,$fieldvals)
    {
        $dbColumnlist = Core_Database::getFieldList('SiteSurvey');
        $l5checkboxFields=array('S10Q1_StandAloneStore_Checked','S10Q1_IndoorsMall_Checked','S10Q1_OutdoorsMall_Checked','S10Q1_FoodCourt_Checked','S10Q1_Kiosk_Checked','S10Q1_InsideOtherRetailStore_Checked','S10Q1_SingleTenantOfficeBuil_Checked','S10Q1_MultiTenantOfficeBuil_Checked','S10Q1_AirportBehindSecu_Checked','S10Q1_AirportInFrontofSecu_Checked'
,'S10Q1_RestrictedAccessGovFaci_Checked','S10Q1_Other_Checked');
        
        $updateFields = array();
        foreach($fieldvals as $k=>$v){
            
            if(in_array($k,$dbColumnlist)){
                if(in_array($k,$l5checkboxFields)){
                    if($v == "1"){
                        $updateFields[$k] = 1;
                    } else {
                        $updateFields[$k] = 0;
                    }
                } else {
                    if(!empty($v) || $v==='0' || $v===0){
                        $updateFields[$k] = $v;
                    }                    
                }
            }
        }
        
        if(count($updateFields) > 0)    {
            $criteria = "SurveyID='$surveyID' ";  
            $result = Core_Database::update("SiteSurvey",$updateFields,$criteria);
            return $result;            
        } else {
            return 0;
        }

        
    }
    //14109  
    public function saveSurvey($surveyID,$winNum,$techID,$techUsername,$fieldvals)
    {
        //echo('$fieldvals: ');print_r($fieldvals);die('saveSurvey');
        $surveyInfo = $this->getSurveyByWinNum($winNum);
        
        $dbColumnlist = Core_Database::getFieldList('SiteSurvey');
        $l5checkboxFields=array('S10Q1_StandAloneStore_Checked','S10Q1_IndoorsMall_Checked','S10Q1_OutdoorsMall_Checked','S10Q1_FoodCourt_Checked','S10Q1_Kiosk_Checked','S10Q1_InsideOtherRetailStore_Checked','S10Q1_SingleTenantOfficeBuil_Checked','S10Q1_MultiTenantOfficeBuil_Checked','S10Q1_AirportBehindSecu_Checked','S10Q1_AirportInFrontofSecu_Checked'
,'S10Q1_RestrictedAccessGovFaci_Checked','S10Q1_Other_Checked');

        $updateFields = array();
        foreach($fieldvals as $k=>$v){
            
            if(in_array($k,$dbColumnlist)){
                if(in_array($k,$l5checkboxFields)){
                    if(!empty($v)){
                        $updateFields[$k] = 1;
                    } else {
                        $updateFields[$k] = 0;
                    }
                } else {
                    if(!empty($v) || $v==='0' || $v===0){
                        $updateFields[$k] = $v;
                    }                    
                }
            }
        }

        
        if($surveyID > 0 || !empty($surveyInfo)){ //update        
            $criteria = "SurveyID='$surveyID' OR WIN_NUM='$winNum'";  
            $updateFields['LastUpdateBy'] = $techUsername;
            $updateFields['LastUpdateByTechID'] = $techID;
            $updateFields['LastUpdatedDateTime'] = date("Y-m-d H:i:s");     
            $result = Core_Database::update("SiteSurvey",$updateFields,$criteria);
            
        } else { //insert
            $updateFields['WIN_NUM'] = $winNum;
            $updateFields['CreatedDateTime'] = date("Y-m-d H:i:s");
            $updateFields['CreatedBy'] = $techUsername;
            $updateFields['CreatedByTechID'] = $techID;
            $updateFields['LastUpdateBy'] = $techUsername;
            $updateFields['LastUpdateByTechID'] = $techID;
            $updateFields['LastUpdatedDateTime'] = date("Y-m-d H:i:s");  
                                 
            $result = Core_Database::insert("SiteSurvey",$updateFields); 
            
        }                
        $this->updateNumPicsAndOthers($winNum,$layout);
        //update other fields
        $this->updateSurveyTotalFields($winNum);
        //updateNumPics
        
        
        
        return $result;
    }
    //14109
    public function updateSurveyTotalFields($winNum)
    {
        //$surveyInfo = $this->getSurveyByWinNum($winNum);
        //if(empty($surveyInfo)) return;
        try{   
            $updateSQL = " UPDATE SiteSurvey SET ";
            $updateSQL .= " S3Q1f_SB_Total = IFNULL(S3Q1b_SB_Bline_Num,0) + IFNULL(S3Q1c_SB_Fline_Num,0) + IFNULL(S3Q1d_SB_DThru_Num,0) + IFNULL(S3Q1e_SB_Others_Num,0) ";

            $updateSQL .= ", S3Q1g_SB_WLeftHW_Total = IFNULL(S3Q1b_SB_Bline_WLeftHW_Num,0) + IFNULL(S3Q1c_SB_Fline_WLeftHW_Num,0) + IFNULL(S3Q1d_SB_DThru_WLeftHW_Num,0) +  IFNULL(S3Q1e_SB_Others_WLeftHW_Num,0) ";
            
            $updateSQL .= ", S3Q1h_SB_WRightHW_Total = IFNULL(S3Q1b_SB_Bline_WRightHW_Num,0) + IFNULL(S3Q1c_SB_Fline_WRightHW_Num,0) +           IFNULL(S3Q1d_SB_DThru_WRightHW_Num,0) + IFNULL(S3Q1e_SB_Others_WRightHW_Num,0) ";
       
            $updateSQL .= ", S3Q2f_DB_Total = IFNULL(S3Q2b_DB_Bline_Num,0) + IFNULL(S3Q2c_DB_Fline_Num,0) + IFNULL(S3Q2d_DB_DThru_Num,0) + IFNULL(S3Q2e_DB_Others_Num,0) ";

            $updateSQL .= ", S3Q2g_DB_WLeftHW_Total = IFNULL(S3Q2b_DB_Bline_WLeftHW_Num,0) + IFNULL(S3Q2c_DB_Fline_WLeftHW_Num,0) +            IFNULL(S3Q2d_DB_DThru_WLeftHW_Num,0) + IFNULL(S3Q2e_DB_Others_WLeftHW_Num,0) ";
            
            $updateSQL .= ", S3Q2h_DB_WRightHW_Total = IFNULL(S3Q2b_DB_Bline_WRightHW_Num,0) + IFNULL(S3Q2c_DB_Fline_WRightHW_Num,0) +  IFNULL(S3Q2d_DB_DThru_WRightHW_Num,0) + IFNULL(S3Q2e_DB_Others_WRightHW_Num,0) ";

            $updateSQL .= ", S3Q4j_SS_TotalUnused = IFNULL(S3Q4b_SS_Bline_NumUnused,0) + IFNULL(S3Q4d_SS_Fline_NumUnused,0) +            IFNULL(S3Q4f_SS_DThru_NumUnused,0) + IFNULL(S3Q4h_SS_Others_NumUnused,0) ";

            $updateSQL .= ", S3Q4k_SS_Total = IFNULL(S3Q4c_SS_Bline_Num,0) + IFNULL(S3Q4e_SS_Fline_Num,0) + IFNULL(S3Q4g_SS_DThru_Num,0) + IFNULL(S3Q4i_SS_Others_Num,0)";
            $updateSQL .= ", S3Q5j_DS_Unused_Total = IFNULL(S3Q5b_DS_Bline_NumUnused,0) + IFNULL(S3Q5d_DS_Fline_NumUnused,0) + IFNULL(S3Q5f_DS_DThru_NumUnused,0) + IFNULL(S3Q5h_DS_Others_NumUnused,0)"; 
            $updateSQL .= ", S3Q5k_DS_Total = IFNULL(S3Q5c_DS_Bline_Num,0) + IFNULL(S3Q5e_DS_Fline_Num,0) + IFNULL(S3Q5g_DS_DThru_Num,0) + IFNULL(S3Q5i_DS_Others_Num,0)";
            $updateSQL .= ", S3Q8f_GR_Total = IFNULL(S3Q8b_GR_Bline_Num,0) + IFNULL(S3Q8c_GR_Fline_Num,0) + IFNULL(S3Q8d_GR_DThru_Num,0) + IFNULL(S3Q8e_GR_Others_Num,0)";    
            
            $updateSQL .= ", S4Q2_Length = IFNULL(S4Q2_IntLengh,0) + IFNULL(S4Q2_Fraction,0)";
            $updateSQL .= ", S4Q3_Length = IFNULL(S4Q3_IntLengh,0) + IFNULL(S4Q3_Fraction,0)";
            $updateSQL .= ", S4Q4_Length = IFNULL(S4Q4_IntLengh,0) + IFNULL(S4Q4_Fraction,0)";
            $updateSQL .= ", S4Q5_Length = IFNULL(S4Q5_IntLengh,0) + IFNULL(S4Q5_Fraction,0)";
            $updateSQL .= ", S6Q2_Length = IFNULL(S6Q2_IntLength,0) + IFNULL(S6Q2_Fraction,0)";
                        
            $updateSQL .= ", S4Q8e_Total = IFNULL(S4Q8a_Num,0) + IFNULL(S4Q8b_Num,0) + IFNULL(S4Q8c_Num,0) + IFNULL(S4Q8d_Num,0)";
            $updateSQL .= ", S4Q9e_Total = IFNULL(S4Q9a_Num,0) + IFNULL(S4Q9b_Num,0) + IFNULL(S4Q9c_Num,0) + IFNULL(S4Q9d_Num,0)";
            $updateSQL .= ", S4Q10e_Total = IFNULL(S4Q10a_Num,0) + IFNULL(S4Q10b_Num,0) + IFNULL(S4Q10c_Num,0) + IFNULL(S4Q10d_Num,0)";
            
            if(!empty($winNum)){
                $updateSQL .= " WHERE WIN_NUM = '$winNum' ";                
            }
            
            $db = Core_Database::getInstance();
            $db->query($updateSQL);        
        } catch(Exception $ex){
            //print_r($ex);
        }        
    }
    
    public function updateNumPicsAndOthers($winNum,$layout)
    {
        $surveyInfo = $this->getSurveyByWinNum($winNum);
        if(empty($surveyInfo)) return;
        
        $numPics=0; $key = 'S3Q7d_BE_Others_Pic'; $max=3;
        for($i=1;$i<=$max;$i++){
            if(!empty($surveyInfo[$key.$i])) $numPics++;
        }
        $S3Q7d_BE_Others_NumPics = $numPics;

        $numPics=0; $key = 'S3Q8b_GR_Bline_Pic'; $max=10;
        for($i=1;$i<=$max;$i++){
            if(!empty($surveyInfo[$key.$i])) $numPics++;
        }
        $S3Q8b_GR_Bline_NumPics = $numPics;
        if(empty($surveyInfo['S3Q8b_GR_Bline_Num'])) {
            $S3Q8b_GR_Bline_NumPics = 0;   
        }
        
        $numPics=0; $key = 'S3Q8c_GR_Fline_Pic'; $max=10;
        for($i=1;$i<=$max;$i++){
            if(!empty($surveyInfo[$key.$i])) $numPics++;
        }
        $S3Q8c_GR_Fline_NumPics = $numPics;
        if(empty($surveyInfo['S3Q8c_GR_Fline_Num'])) {
            $S3Q8c_GR_Fline_NumPics = 0;   
        }

        $numPics=0; $key = 'S3Q8d_GR_DThru_Pic'; $max=10;
        for($i=1;$i<=$max;$i++){
            if(!empty($surveyInfo[$key.$i])) $numPics++;
        }
        $S3Q8d_GR_DThru_NumPics = $numPics;
        if(empty($surveyInfo['S3Q8d_GR_DThru_Num'])) {
            $S3Q8d_GR_DThru_NumPics = 0;   
        }
        
        
        $numPics=0; $key = 'S3Q8e_GR_Others_Pic'; $max=10;
        for($i=1;$i<=$max;$i++){
            if(!empty($surveyInfo[$key.$i])) $numPics++;
        }
        $S3Q8e_GR_Others_NumPics = $numPics;
        if(empty($surveyInfo['S3Q8e_GR_Others_Num'])) {
            $S3Q8e_GR_Others_NumPics = 0;   
        }
        
        $numPics=0; $key = 'S4Q12_Pic'; $max=20;
        for($i=1;$i<=$max;$i++){
            if(!empty($surveyInfo[$key.$i])) $numPics++;
        }
        $S4Q12_NumPics = $numPics;
        if(empty($surveyInfo['S4Q11_Num'])) {
            $S4Q12_NumPics = 0;   
        }
                
        $numPics=0; $key = 'S5Q2b_Pic'; $max=10;
        for($i=1;$i<=$max;$i++){
            if(!empty($surveyInfo[$key.$i])) $numPics++;
        }
        $S5Q2b_NumPics = $numPics;
        if(empty($surveyInfo['S5Q2a_Num'])) {
            $S5Q2b_NumPics = 0;   
        }

        $numPics=0; $key = 'S5Q3b_Pic'; $max=10;
        for($i=1;$i<=$max;$i++){
            if(!empty($surveyInfo[$key.$i])) $numPics++;
        }
        $S5Q3b_NumPics = $numPics;
        if(empty($surveyInfo['S5Q3a_Num'])) {
            $S5Q3b_NumPics = 0;   
        }
        
        $numPics=0; $key = 'S5Q4b_Pic'; $max=10;
        for($i=1;$i<=$max;$i++){
            if(!empty($surveyInfo[$key.$i])) $numPics++;
        }
        $S5Q4b_NumPics = $numPics;
        if(empty($surveyInfo['S5Q4a_Num'])) {
            $S5Q4b_NumPics = 0;   
        }

        
        $numPics=0; $key = 'S8Q2_Circuits_Pic'; $max=5;
        for($i=1;$i<=$max;$i++){
            if(!empty($surveyInfo[$key.$i])) $numPics++;
        }
        $S8Q2_Circuits_NumPics = $numPics;
        
        //S0_AllPhotos_Num
        
        $allNum = 0;
        $allNum += $S3Q7d_BE_Others_NumPics;
        $allNum += $S3Q8b_GR_Bline_NumPics;
        $allNum += $S3Q8c_GR_Fline_NumPics;
        $allNum += $S3Q8d_GR_DThru_NumPics;
        $allNum += $S3Q8e_GR_Others_NumPics;
        $allNum += $S4Q12_NumPics;
        $allNum += $S5Q2b_NumPics;
        $allNum += $S5Q3b_NumPics;
        $allNum += $S5Q4b_NumPics;
        $allNum += $S8Q2_Circuits_NumPics;
        if(!empty($surveyInfo['Sec1Q13_FrontofStore'])) $allNum++;        
        if(!empty($surveyInfo['S3Q7a_BE_Bline_Pic'])) $allNum++;
        if(!empty($surveyInfo['S3Q7b_BE_Fline_Pic'])) $allNum++;
        if(!empty($surveyInfo['S3Q7c_BE_DThru_Pic'])) $allNum++;
        if(!empty($surveyInfo['S3Q7c_BE_DThru_Pic'])) $allNum++;
        if(!empty($surveyInfo['S6Q3_Pic'])) $allNum++;
        if(!empty($surveyInfo['S6Q4_Pic'])) $allNum++;
        if(!empty($surveyInfo['S6Q5_Pic'])) $allNum++;
        if(!empty($surveyInfo['S7Q5_WaterFilter_Pic'])) $allNum++;
        if(!empty($surveyInfo['S7Q5_WaterFilter_Pic'])) $allNum++;        
        if(!empty($surveyInfo['S8Q1_ElecPanel_Pic'])) $allNum++;
        if(!empty($surveyInfo['S8Q3_PanelDiagram_Pic'])) $allNum++;
        
        $surveyID = $surveyInfo['SurveyID'];
        $criteria = "SurveyID='$surveyID'";          
        $updateFields = array();
        $updateFields['S3Q7d_BE_Others_NumPics'] = $S3Q7d_BE_Others_NumPics;
        $updateFields['S3Q8b_GR_Bline_NumPics'] = $S3Q8b_GR_Bline_NumPics;
        $updateFields['S3Q8c_GR_Fline_NumPics'] = $S3Q8c_GR_Fline_NumPics;
        $updateFields['S3Q8d_GR_DThru_NumPics'] = $S3Q8d_GR_DThru_NumPics;
        $updateFields['S3Q8e_GR_Others_NumPics'] = $S3Q8e_GR_Others_NumPics;
        $updateFields['S4Q12_NumPics'] = $S4Q12_NumPics;
        $updateFields['S5Q2b_NumPics'] = $S5Q2b_NumPics;
        $updateFields['S5Q3b_NumPics'] = $S5Q3b_NumPics;
        $updateFields['S5Q4b_NumPics'] = $S5Q4b_NumPics;
        $updateFields['S8Q2_Circuits_NumPics'] = $S8Q2_Circuits_NumPics;
        $updateFields['S0_AllPhotos_Num'] = $allNum;
        
        //--- for other fields
        if($surveyInfo['S4Q7_CounterTopMaterial'] != 'Other'){
            $updateFields['S4Q7_CounterTopMaterial_Other'] = '';
        }
        
        if(empty($surveyInfo['Sec1Q12_StoreHasDriveThru'])){
            $updateFields['S3Q1d_SB_DThru_Num'] = 0;
            $updateFields['S3Q1d_SB_DThru_WLeftHW_Num'] = 0;
            $updateFields['S3Q1d_SB_DThru_WRightHW_Num'] = 0;            
            $updateFields['S3Q2d_DB_DThru_Num'] = 0;
            $updateFields['S3Q2d_DB_DThru_WLeftHW_Num'] = 0;            
            $updateFields['S3Q2d_DB_DThru_WRightHW_Num'] = 0;            
            $updateFields['S3Q4f_SS_DThru_NumUnused'] = 0;
            $updateFields['S3Q4g_SS_DThru_Num'] = 0;            
            $updateFields['S3Q5f_DS_DThru_NumUnused'] = 0;
            $updateFields['S3Q5g_DS_DThru_Num'] = 0;            
            $updateFields['S3Q7c_BE_DThru_Pic'] = '';            
            $updateFields['S3Q8d_GR_DThru_Num'] = 0;
            $updateFields['S3Q8d_GR_DThru_NumPics'] = 0;            
        }
        
        if($surveyInfo['S4Q13_BOutlet_InUse_Num']==0){
            $updateFields['S4Q13a_BOutlet1_VMeter'] = 0;            
            $updateFields['S4Q13b_BOutlet2_VMeter'] = 0;            
            $updateFields['S4Q13c_BOutlet3_VMeter'] = 0;            
            $updateFields['S4Q13d_BOutlet4_VMeter'] = 0;            
            $updateFields['S4Q13e_BOutlet5_VMeter'] = 0;            
        } else if($surveyInfo['S4Q13_BOutlet_InUse_Num']==1){
            $updateFields['S4Q13b_BOutlet2_VMeter'] = 0;            
            $updateFields['S4Q13c_BOutlet3_VMeter'] = 0;            
            $updateFields['S4Q13d_BOutlet4_VMeter'] = 0;            
            $updateFields['S4Q13e_BOutlet5_VMeter'] = 0;                        
        } else if($surveyInfo['S4Q13_BOutlet_InUse_Num']==2){
            $updateFields['S4Q13c_BOutlet3_VMeter'] = 0;            
            $updateFields['S4Q13d_BOutlet4_VMeter'] = 0;            
            $updateFields['S4Q13e_BOutlet5_VMeter'] = 0;                                    
        } else if($surveyInfo['S4Q13_BOutlet_InUse_Num']==3){
            $updateFields['S4Q13d_BOutlet4_VMeter'] = 0;            
            $updateFields['S4Q13e_BOutlet5_VMeter'] = 0;                                                
        } else if($surveyInfo['S4Q13_BOutlet_InUse_Num']==4){
            $updateFields['S4Q13e_BOutlet5_VMeter'] = 0;            
        }
                
        //--- update
        $result = Core_Database::update("SiteSurvey",$updateFields,$criteria);

    }
    
    //14109
    public function getSurveyByWinNum($winNum)
    {
         $db = Core_Database::getInstance();  
         $select = $db->select();
         $select->from("SiteSurvey");
         $select->where("WIN_NUM = '$winNum'");
         $records = Core_Database::fetchAll($select);  
         if(!empty($records) && count($records) >0 ){
             return $records[0];
         } else {
             return null;
         }
    }
    //14109
    public function getSurveyInfo($surveyID)
    {
         $db = Core_Database::getInstance();  
         $select = $db->select();
         $select->from("SiteSurvey");
         $select->where("SurveyID = '$surveyID'");
         $records = Core_Database::fetchAll($select);  
         if(!empty($records) && count($records) >0 ){
             return $records[0];
         } else {
             return null;
         }
    }
    
    
    //for Google Calendar API
    public function getTechInfo_ForAPIUsing($techID){
        $db = Core_Database::getInstance();
        $query = $db->select();
        $query->from(Core_Database::TABLE_TECH_BANK_INFO);
        $query->where("TechID = '$techID'"); 
        $records = Core_Database::fetchAll($query);
        if(empty($records) || count($records)==0){
            return null;
        } else {
            return $records[0];
        }
    }
}
