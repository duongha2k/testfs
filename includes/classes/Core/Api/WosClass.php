<?php
class Core_Api_WosClass
{
    const VISIT_TYPE_BASE = 1;
    const VISIT_TYPE_TECH = 2;
    const VISIT_TYPE_CLIENT = 3;
    const WOQA_TYPE_QUESTION = 1;
    const WOQA_TYPE_ANSWER = 2;
    const WOQA_TYPE_NORMAL_CLIENT_MESSAGE = 3;
    const WOQA_GROUP_INTERNAL_NOTE = 'InternalNote';
    const WOQA_GROUP_GPM = 'GPM';
    
    public function __construct() {
        Core_Caspio::init();
    }
    public function Test()
    {
        return "helloabc";
    }
    
    public function getWorkOrderBCats($winNum)
    {
         $db = Core_Database::getInstance();  
         $select = $db->select();
         $select->from("work_orders_bcats");
         $select->where("winid = '$winNum'");
         $select->order("bcattype desc");
         $select->order("bcatname asc");
         $records = Core_Database::fetchAll($select);  
         return $records;              
    }

    public function exists_WinBCatName($winNum,$bcatname)
    {
         $db = Core_Database::getInstance();  
         $select = $db->select();
         $select->from("work_orders_bcats");
         $select->where("winid = '$winNum'");
         $select->where("bcatname = '$bcatname'");         
         $records = Core_Database::fetchAll($select);  
         if(!empty($records)) return 1;
         else return 0;
    }

    //--- 13863, 13871
    public function getWorkOrderTurnedOnBCats($winNum)
    {
         $db = Core_Database::getInstance();  
         $select = $db->select();
         $select->from("work_orders_bcats");
         $select->where("winid = '$winNum'");
         $select->where("ischecked = 1"); 
         $select->order("bcattype desc");
         $select->order("bcatname asc");
         $records = Core_Database::fetchAll($select);  
         if(!empty($records))
         {
             for($i=0;$i<count($records);$i++)
             {
                 $bcatid = $records[$i]['bcatid'];  
                 $approved = $records[$i]['approved'];
                 $reimbursable_thru_fs = $records[$i]['reimbursable_thru_fs'];
                 $approved_amount = $records[$i]['approved_amount'];
                 if(!isset($approved_amount)) $approved_amount=0;
                 
                 $approved_amount_thFS = 0;
                 $approved_amount_outFS = 0;
                 if($approved==1 && $reimbursable_thru_fs==1) 
                 {
                    $approved_amount_thFS = $approved_amount;  
                 }
                 if($approved==1 && $reimbursable_thru_fs==0) 
                 {
                    $approved_amount_outFS = $approved_amount;  
                 }                                                   
                 $docnum = $this->getDocNumForWOCat($bcatid);
                 
                 $records[$i]['approved_amount_thfs']=$approved_amount_thFS;
                 $records[$i]['approved_amount_outfs']=$approved_amount_outFS;
                 $records[$i]['docnum']=$docnum;
             }
             return $records;
         }
         else
         {
             //13871
             return null;
         }
    }
    
    public function getWorkOrderTurnedOnCustomBCats($winNum)
    {
         $db = Core_Database::getInstance();  
         $select = $db->select();
         $select->from("work_orders_bcats");
         $select->where("winid = '$winNum'");
         $select->where("ischecked = 1"); 
         $select->where("bcattype = 'custom'"); 
         $select->order("bcatname asc");
         $records = Core_Database::fetchAll($select);  
         if(!empty($records))
         {
             for($i=0;$i<count($records);$i++)
             {
                 $bcatid = $records[$i]['bcatid'];  
                 $approved = $records[$i]['approved'];
                 $reimbursable_thru_fs = $records[$i]['reimbursable_thru_fs'];
                 $approved_amount = $records[$i]['approved_amount'];
                 if(!isset($approved_amount)) $approved_amount=0;
                 
                 $approved_amount_thFS = 0;
                 $approved_amount_outFS = 0;
                 if($approved==1 && $reimbursable_thru_fs==1) 
                 {
                    $approved_amount_thFS = $approved_amount;  
                 }
                 if($approved==1 && $reimbursable_thru_fs==0) 
                 {
                    $approved_amount_outFS = $approved_amount;  
                 }                                                   
                 $docnum = $this->getDocNumForWOCat($bcatid);
                 
                 $records[$i]['approved_amount_thfs']=$approved_amount_thFS;
                 $records[$i]['approved_amount_outfs']=$approved_amount_outFS;
                 $records[$i]['docnum']=$docnum;
             }
         }

         return $records;              
    }
    
    public function getWorkOrderTurnedOnBCats_insertFromProjectIfNotExists($winNum)
    {
        //--- get projectId, companyId
        $projectId = "";
        $companyId = "";
        $fieldArray = array('WIN_NUM','Project_ID','Company_ID');
        $db = Core_Database::getInstance();
        $query = $db->select(); 
        $query->from(Core_Database::TABLE_WORK_ORDERS,$fieldArray);        
        $query->where("WIN_NUM = '$winNum'");                
        $records = Core_Database::fetchAll($query);        
        if(!empty($records))
        {
            $projectId = $records[0]['Project_ID'];  
            $companyId = $records[0]['Company_ID'];  
        } 
        //-----
        $current = $this->getWorkOrderBCats($winNum);
        if(empty($current))
        {
            if(!empty($projectId))
            {
                //--- get data from project
                $projectApi = new Core_Api_ProjectClass();
                $records = $projectApi->getProjectBCats_insertFromClientIfNotExists($projectId);
                //--- 
                foreach($records as $rec)            
                {
                    $fieldvals = array("winid"=>$winNum,
                        "bcatname"=>$rec["bcatname"],
                        "bcatdesc"=>$rec["bcatdesc"],
                        "ischecked"=>$rec["ischecked"],
                        "reimbursable_thru_fs"=>$rec["reimbursable_thru_fs"],
                        "require_doc_upl"=>$rec["require_doc_upl"],
                        "bcattype"=>$rec["bcattype"]);   
                    
                    $bcatname = $rec["bcatname"];
                    if($this->exists_WinBCatName($winNum,$bcatname)==0);    
                        $result = Core_Database::insert("work_orders_bcats",$fieldvals);        
                }            
            }
        }       
        else
        {
            if(!empty($projectId))
            {
                //--- get data from project
                $projectApi = new Core_Api_ProjectClass();
                $records = $projectApi->getProjectBCats_insertFromClientIfNotExists($projectId);
                //--- 
                foreach($records as $rec)            
                {
                    $projBCatname = $rec["bcatname"];
                    $fieldvals = array(
                        "ischecked"=>$rec["ischecked"],
                        "bcattype"=>$rec["bcattype"]);   
                    $criteria = "winid='$winNum' AND bcatname='$projBCatname'";  
                    $result = Core_Database::update("work_orders_bcats",$fieldvals,$criteria);
                }            
                $current = $this->getWorkOrderBCats($winNum);                            
            }
        } 
        $ret = $this->getWorkOrderTurnedOnBCats($winNum);
        return $ret;
    }    

    public function getWorkOrderTurnedOnCustomBCats_insertFromProjectIfNotExists____($winNum)
    {
        //`bcattype` = 'custom'
        
        //--- get projectId, companyId
        $projectId = "";
        $companyId = "";
        $fieldArray = array('WIN_NUM','Project_ID','Company_ID');
        $db = Core_Database::getInstance();
        $query = $db->select(); 
        $query->from(Core_Database::TABLE_WORK_ORDERS,$fieldArray);        
        $query->where("WIN_NUM = '$winNum'");                
        $records = Core_Database::fetchAll($query);        
        if(!empty($records))
        {
            $projectId = $records[0]['Project_ID'];  
            $companyId = $records[0]['Company_ID'];  
        } 
        //-----
        $current = $this->getWorkOrderBCats($winNum);
        if(empty($current))
        {
            if(!empty($projectId))
            {
                //--- get data from project
                $projectApi = new Core_Api_ProjectClass();
                $records = $projectApi->getProjectBCats_insertFromClientIfNotExists($projectId);
                //--- 
                foreach($records as $rec)            
                {
                    $fieldvals = array("winid"=>$winNum,
                        "bcatname"=>$rec["bcatname"],
                        "bcatdesc"=>$rec["bcatdesc"],
                        "ischecked"=>$rec["ischecked"],
                        "reimbursable_thru_fs"=>$rec["reimbursable_thru_fs"],
                        "require_doc_upl"=>$rec["require_doc_upl"],
                        "bcattype"=>$rec["bcattype"]);   
                    $result = Core_Database::insert("work_orders_bcats",$fieldvals);        
                }            
            }
        }       
        else
        {
            if(!empty($projectId))
            {
                //--- get data from project
                $projectApi = new Core_Api_ProjectClass();
                $records = $projectApi->getProjectBCats_insertFromClientIfNotExists($projectId);
                //--- 
                foreach($records as $rec)            
                {
                    $projBCatname = $rec["bcatname"];
                    $fieldvals = array(
                        "bcatdesc"=>$rec["bcatdesc"],
                        "ischecked"=>$rec["ischecked"],
                        "require_doc_upl"=>$rec["require_doc_upl"],
                        "bcattype"=>$rec["bcattype"]);   
                    $criteria = "winid='$winNum' AND bcatname='$projBCatname'";  
                    $result = Core_Database::update("work_orders_bcats",$fieldvals,$criteria);
                }            
                $current = $this->getWorkOrderBCats($winNum);                            
            }
        } 
        $ret = $this->getWorkOrderTurnedOnCustomBCats($winNum);
        return $ret;
    }    
    
    public function getDocumentListByWOBCatID($wobcatid)
    {
         $db = Core_Database::getInstance();  
         $select = $db->select();
         $select->from("files");
         $select->where("wobcatid = '$wobcatid'"); 
         $records = Core_Database::fetchAll($select);  
         return $records;  
    }

    
    public function addDocumentForWOBCat($winNum,$wobcatid,$path,$filename)
    {
        $files = new Core_Files();
        
        $fieldvals=array("WIN_NUM"=>$winNum,"path"=>$path,
                "type"=>"2",
                "time_stamp"=>date("Y-m-d"),
                "uplfilename"=>$filename,
                "wobcatid"=>$wobcatid
        );
        
        $result = $files->insert($fieldvals);
        return $result;        
    }
    
    public function updateTechClaimForWOBCat($wobcatid,$techClaim)
    {
        $fieldvals = array("tech_claim"=>$techClaim);
        $criteria = "bcatid = '$wobcatid'";
        $result = Core_Database::update("work_orders_bcats",$fieldvals,$criteria);
        return $result;
    }
    
    public function updateDataForWOBCat($wobcatid,$reimburseThFS,
            $approved,$approvedAmount)
    {
        $fieldvals = array("reimbursable_thru_fs"=>$reimburseThFS,
                        "approved"=>$approved,
                        "approved_amount"=>$approvedAmount
        );
        $criteria = "bcatid = '$wobcatid'";
        $result = Core_Database::update("work_orders_bcats",$fieldvals,$criteria);
        return $result;
    }
    
	//--- 13380
    public function updateBCatDataForWOs($woIDs, $projectID)
    {
        if(empty($woIDs) || empty($projectID)) return;

		$projectClass = new Core_Api_ProjectClass;
		$prjcat = $projectClass->getProjectBCats($projectID);
		if (empty($prjcat)) return;

		foreach ($prjcat as $pcat) {
			// insert for wo with missing cat
			$bcatName = $pcat['bcatname'];
			$db = Core_Database::getInstance();
			$fieldvals = array('bcatname' => $pcat['bcatname'], 'bcatdesc' => $pcat['bcatdesc'],
					'ischecked' => $pcat['ischecked'], 'reimbursable_thru_fs' => $pcat['reimbursable_thru_fs'], 'require_doc_upl' => $pcat['require_doc_upl'],
					'bcattype' => $pcat['bcattype'], 'display_order' => $pcat['display_order']);
			if (!empty($pcat['ischecked'])) {
				$select = $db->select();
				$select->from(Core_Database::TABLE_WORK_ORDERS, array('WIN_NUM'))
					->joinLeft("work_orders_bcats", "WIN_NUM = winid AND bcatname = " . $db->quoteInto('?', $bcatName), array())
					->where("WIN_NUM IN ($woIDs)")
					->where('bcatname IS NULL', $bcatName);
				$wosNeedInsert = $db->fetchCol($select);
				if (!empty($wosNeedInsert)) {
					foreach ($wosNeedInsert as $wo) {
						$fieldvals['winid'] = $wo;
						$db->insert('work_orders_bcats', $fieldvals);
					}
				}
			}
			unset($fieldvals['winid']);
			$criteria = "(bcatname = '$bcatName' AND winid IN ($woIDs))";
			$result = Core_Database::update("work_orders_bcats",$fieldvals,$criteria);
		}

        return $result;
    }

    public function getDocNumForWOCat($wobcatid)
    {
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from("files",array('c'=>'(COUNT(*))'));
        $select->where("wobcatid = '$wobcatid'");
        $records = Core_Database::fetchAll($select);  
        if(!empty($records)) return $records[0]['c'];
        else return 0;
    }
    
    public function getDocsForWOByWIN($win){
    	 $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from("files", array("path"));
        $select->where("WIN_NUM = '$win'");
        $records = Core_Database::fetchAll($select);  
        if(!empty($records)) return $records;
        else return 0;
    }
    
    public function enableExpenseReporting($winNum)
    {    
        //--- get projectId
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from(Core_Database::TABLE_WORK_ORDERS,array('WIN_NUM','Project_ID'));
        $select->where("WIN_NUM = '$winNum'");
        $records = Core_Database::fetchAll($select);  
        if(!empty($records)) $projectId =  $records[0]['Project_ID'];
        else $projectId=0;
        //--- get data from project
        $prjApi = new Core_Api_ProjectClass();
        $enable = $prjApi->enableExpenseReporting($projectId);            
        return $enable;                
    }   
      
    public function getTotalAmountArrayForWOBCats($winNum)
    {
        $records = $this->getWorkOrderTurnedOnBCats($winNum);
        $claimTotal = 0;
        $approvedThFSTotal = 0;
        $approvedOutFSTotal = 0;
        
        if(!empty($records))
        {
            foreach($records as $record)
            {
                if(!empty($record["tech_claim"])) $claimTotal += $record["tech_claim"];
                $approvedThFSTotal += $record["approved_amount_thfs"];
                $approvedOutFSTotal += $record["approved_amount_outfs"];
            }
        }
        
        $totalArray = array("ClaimTotal"=>$claimTotal,
                    "ApprovedThFSTotal"=>$approvedThFSTotal,
                    "ApprovedOutFSTotal"=>$approvedOutFSTotal
        );           
        return $totalArray;      
    }
    
    public function getTotal_ApReThFS($winNum)
    {
        $db = Core_Database::getInstance();  
        $select = $db->select();

		if (is_array ($winNum)) {
			$results = array();

			if (!empty ($winNum)) {
				$select->from("work_orders_bcats",array("WIN_NUM" => "winid", "c" => "(SUM(approved_amount))"));
				$select->where ("winid IN (" . implode (",", array_unique ($winNum)) . ")");
				$select->where("ischecked = 1");
				$select->where("approved = 1");
				$select->where("reimbursable_thru_fs = 1");
				$records = Core_Database::fetchAll($select);  

				foreach ($winNum as $win) {
					$results[$win] = 0;
				}
				foreach ($records as $record) {
					$results[$record->WIN_NUM] = $record->c;
				}
			}

			return $results;
		}
		else {
			$select->from("work_orders_bcats",array('c'=>'(SUM(approved_amount))'));
			$select->where("winid = '$winNum'");
			$select->where("ischecked = 1");
			$select->where("approved = 1");
			$select->where("reimbursable_thru_fs = 1");
			$records = Core_Database::fetchAll($select);  
			if(!empty($records)) 
			{
				if(!empty($records[0]['c'])) return $records[0]['c'];
				else return 0;
			}
			else return 0;
		}
    }
    
    //--- 13418 
    public function update_Total_Reimbursable_Expense($winnum)
    {
        $Total_Reimbursable_Expense = $this->getTotal_ApReThFS($winnum);
        $criteria = "WIN_NUM='$winnum'";
        $fieldvals = array('Total_Reimbursable_Expense'=>$Total_Reimbursable_Expense);
        $result = Core_Database::update(Core_Database::TABLE_WORK_ORDERS,$fieldvals,$criteria);
    }

	public function getWins_TechClaimNotApproved($arrayWinIds)
    {
         $retWins = "";
         if (is_array($arrayWinIds) && (sizeof($arrayWinIds) > 0)) 
         {
             //echo("getWins_TechClaimNotApproved1 :"); print_r($arrayWinIds);die();
             $countWOs = sizeof($arrayWinIds);
             for ($i = 0; $i < $countWOs; ++$i) 
             {
                 $winNum = $arrayWinIds[$i];
                 $isNotApproved = $this->isTechClaimNotApproved($winNum);
                 if($isNotApproved==1)
                 {
                     $retWins .= empty($retWins) ? $winNum : ",".$winNum;
                 }
             }
             return $retWins;
         }
         else 
         {
             //echo("getWins_TechClaimNotApproved2 :"); print_r($arrayWinIds);die();
             return "";
         }         
    }
    
    private function isTechClaimNotApproved($winNum)
    {
         $db = Core_Database::getInstance();  
         $select = $db->select();
         $select->from("work_orders_bcats");
         $select->where("winid = '$winNum'");
         $select->where("ischecked = 1"); 
         $select->where("tech_claim > 0"); 
         $select->where("approved is null OR approved = 0");
         $records = Core_Database::fetchAll($select);  
         if(!empty($records)) return 1;
         else return 0;
    }
    /*$approvalDateFrom,$approvalDateTo,$invoiceDateFrom,$invoiceDateTo: 'yyyy-mm-dd'*/   
    //--- 13411 
    public function getExReReport($companyID,$approvalDateFrom,$approvalDateTo,$invoiceDateFrom,$invoiceDateTo,$winID,$clientWOID,$techID,$projectIDs,$showApprovedReimbursements=0,$startDateFrom='',$startDateTo='')
    {
         if(empty($companyID)) return null;
         $fields=array('Company_ID','WIN_NUM','WO_ID','SiteNumber','StartDate','DateApproved','DateInvoiced','Tech_ID','Tech_LName','Tech_FName','baseTechPay','PayAmount','Approved','Invoiced','calculatedInvoice','Amount_Per','Tech_Bid_Amount');
         $db = Core_Database::getInstance();  
         $select = $db->select();
         $select->from(Core_Database::TABLE_WORK_ORDERS,$fields);
         $select->where("Company_ID = '$companyID'"); 
         if(!empty($approvalDateFrom)) $select->where("DateApproved >= '$approvalDateFrom'"); 
         if(!empty($approvalDateTo)) $select->where("DateApproved <= '$approvalDateTo'"); 
         if(!empty($invoiceDateFrom)) $select->where("DateInvoiced >= '$invoiceDateFrom'"); 
         if(!empty($invoiceDateTo)) $select->where("DateInvoiced <= '$invoiceDateTo'"); 
         if(!empty($startDateFrom)) $select->where("StartDate >= '$startDateFrom'"); 
         if(!empty($startDateTo)) $select->where("StartDate <= '$startDateTo'"); 
         if(!empty($winID)) $select->where("WIN_NUM = '$winID'"); 
         if(!empty($clientWOID)) $select->where("WO_ID = '$clientWOID'"); 
         if(!empty($techID)) $select->where("Tech_ID = '$techID'"); 
         if(!empty($projectIDs)) $select->where("Project_ID in ($projectIDs)"); 
         if($showApprovedReimbursements==1)
         {
             $woIDs = $this->getWinnums_haveAppReim($companyID);
             if(!empty($woIDs))
             {
                 $select->where("WIN_NUM in ($woIDs)"); 
}
         }         
         $records = Core_Database::fetchAll($select);
         if(empty($records)) return null;
         else
         {
            $Core_Api_User = new Core_Api_User();
            $fixedBCatNames = $Core_Api_User->getFixedBCatNames_ByCompany($companyID);
            $customBCatNames = $Core_Api_User->getCustomBCatNames_ByCompany($companyID);
            $bCatNames = array();
            $bCatNames = array_merge($fixedBCatNames,$customBCatNames);            
            $sum_base_tech_pay = 0;
            $sum_techclaim = 0;
            $sum_reimburse_throught_fs = 0;
            $sum_reimburse_outside_fs = 0;
            $sum_final_total_tech_pay = 0;
            $sum_final_invoice_amount = 0;   
            $sum_category_techclaim = array();     
            $sum_category_reimthroughfs = array();     
            $sum_category_reimoutsidefs = array();     
            
            foreach($bCatNames as $bCatName)    
            {
                $bCatCode = $this->getBCatCode($bCatName);
                $sum_category_techclaim[$bCatCode] = 0;
                $sum_category_reimthroughfs[$bCatCode] = 0;
                $sum_category_reimoutsidefs[$bCatCode] = 0;
            }
            
            for($i=0; $i<count($records); $i++)
            {
                $win = $records[$i]['WIN_NUM'];
                $grossPayAmount = $records[$i]['PayAmount']; 
                $approved = $records[$i]['Approved'];                
                $arrBCatInfo = $this->getWorkOrderBCatsArray($win);
                $totalApReim = $arrBCatInfo['total_reimThroughFS'];
                //--- gross pay amount                            
                if($approved==0) $grossPayAmount += $totalApReim;
                //--- sum
                $sum_base_tech_pay += $records[$i]['baseTechPay'];  
                if(!empty($arrBCatInfo) && count($arrBCatInfo)>0)
                {
                    $sum_techclaim += $arrBCatInfo['total_techClaim'];                    
                    $sum_reimburse_throught_fs += $arrBCatInfo['total_reimThroughFS'];                               $sum_reimburse_outside_fs += $arrBCatInfo['total_reimOutsiteFS'];        
                    foreach($bCatNames as $bCatName)    
                    {
                        $bCatCode = $this->getBCatCode($bCatName);
                        if(!empty($arrBCatInfo[$bCatCode]))
                        {
                            $bcatInfo = $arrBCatInfo[$bCatCode];
                            $sum_category_techclaim[$bCatCode] += $bcatInfo['tech_claim'];
                            $sum_category_reimthroughfs[$bCatCode] += $bcatInfo['reimburse_through_fs'];
                            $sum_category_reimoutsidefs[$bCatCode] += $bcatInfo['reimburse_outside_fs'];
                        }
                    }                                        
                }
                $sum_final_total_tech_pay += $grossPayAmount;
                $sum_final_invoice_amount += $records[$i]['calculatedInvoice'];  
                //---
                $records[$i]['bcatsinfo'] = $arrBCatInfo;
                $records[$i]['total_tech_pay'] = $grossPayAmount;                
            }
            
            $totalInfo = array('sum_base_tech_pay'=>$sum_base_tech_pay,
                            'sum_techclaim'=>$sum_techclaim,
                            'sum_reimburse_throught_fs'=>$sum_reimburse_throught_fs,
                            'sum_reimburse_outside_fs'=>$sum_reimburse_outside_fs,            
                            'sum_final_total_tech_pay'=>$sum_final_total_tech_pay,            
                            'sum_final_invoice_amount'=>$sum_final_invoice_amount,   
                            'sum_category_techclaim'=>$sum_category_techclaim,
                            'sum_category_reimthroughfs'=>$sum_category_reimthroughfs,
                            'sum_category_reimoutsidefs'=>$sum_category_reimoutsidefs
            );
            
            $records['total_footer'] = $totalInfo;
                        
            return $records;
         }
    }
    //--- 13411
    public function getWinnums_haveAppReim($companyID)
    {
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from(array('wobcat'=>'work_orders_bcats'),
                array('winid','sum_approved_amount'=>'(sum(approved_amount))')
        );
        $select->join(array('wo'=>Core_Database::TABLE_WORK_ORDERS),
                'wo.WIN_NUM = wobcat.winid',
                array()
        );
        $select->where("wobcat.approved =1"); 
        $select->where("wobcat.reimbursable_thru_fs =1"); 
        $select->where("wobcat.ischecked =1"); 
        $select->where("wo.Company_ID = '$companyID'"); 
        $select->group('wobcat.winid');
        $select->having('sum(wobcat.approved_amount) >0');
        $records = Core_Database::fetchAll($select);
        if(empty($records)) return null;
        {
            $woIDs = "";
            foreach($records as $rec)
            {
                if(!empty($woIDs)) $woIDs .= ',';
                $woIDs .= $rec['winid'];                
            }
            return $woIDs;
        }
    }
    public function getBCatCode($bcatname)
    {
        $ret = $bcatname;
        $ret = str_replace(' ','',$bcatname); 
        return $ret;  
    }    
    public function getWorkOrderBCatsArray($winNum)
    {
         $arr = array();
         $total_techClaim=0;        
         $total_reimThroughFS=0;        
         $total_reimOutsiteFS=0;        
         $records = $this->getWorkOrderBCats($winNum); 
         if(!empty($records))
         {
             foreach($records as $rec)
             {                
              
                 $bcatname = $rec['bcatname'];
                 $bcatname = $this->getBCatCode($bcatname);
                 $approved = $rec['approved'];   
                 $approved_amount = $rec['approved_amount'];   
                 $reimbursable_thru_fs = $rec['reimbursable_thru_fs'];   
                 
                 $techClaim = $rec['tech_claim'];
                 $reimThroughFS = 0;
                 $reimOutsiteFS = 0;
                 if($approved==1)
                 {
                     if($reimbursable_thru_fs==1) $reimThroughFS=$approved_amount;
                     else $reimOutsiteFS=$approved_amount;
                 }
                 $total_techClaim += $techClaim;
                 $total_reimThroughFS += $reimThroughFS;
                 $total_reimOutsiteFS += $reimOutsiteFS;
                 
                 $arr[$bcatname]=array('tech_claim'=>$techClaim,
                            'reimburse_through_fs'=>$reimThroughFS,
                            'reimburse_outside_fs'=>$reimOutsiteFS,
                 );
             }
             $arr['total_techClaim']=$total_techClaim;
             $arr['total_reimThroughFS']=$total_reimThroughFS;
             $arr['total_reimOutsiteFS']=$total_reimOutsiteFS;
         }
         return $arr;              
    }
    
    //--- 13389
    //$option: 1-All WorkOrders; 2-Unassigned WorkOrder
    public function updateTechDeduct_WorkOrders($companyID,$deductValue,$option)
    {
        if(empty($companyID)) return;
        //--- criteria
        $criteria = "(Company_ID = '$companyID')";
        if($option==1)
        {
            $criteria .= " AND (Deactivated = '0' AND TechMarkedComplete = '0' AND Approved = '0')";
        }
        else if($option==2)
        {
            $criteria .= " AND (Tech_ID IS NULL OR Tech_ID = '') AND TechMarkedComplete = '0' AND Approved = '0' AND  (Status = 'created' OR Status = 'published') ";
        }
        //--- fieldvals
        $pcntDeduct = $deductValue==0? 0:1;        
        $fieldvals = array("PcntDeduct"=>$pcntDeduct,
                        "PcntDeductPercent"=>$deductValue
        );             
        //--- update
        $result = Core_Database::update(Core_Database::TABLE_WORK_ORDERS,$fieldvals,$criteria);
        return $result;     
    }
    //--- 13389
    //$option: 1-All WorkOrders; 2-Unassigned WorkOrder
    public function updateEnableExpenseReporting_WorkOrders($companyID,$enableExpRep,$option)
    {
        if(empty($companyID)) return;
        //--- criteria
        $criteria = "(Company_ID = '$companyID')";
        if($option==1)
        {
            $criteria .= " AND (Deactivated = '0' AND TechMarkedComplete = '0' AND Approved = '0')";
        }
        else if($option==2)
        {
            $criteria .= " AND (Tech_ID IS NULL OR Tech_ID = '') AND TechMarkedComplete = '0' AND Approved = '0' AND  (Status = 'created' OR Status = 'published') ";
        }
        //--- get woIDs
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from(Core_Database::TABLE_WORK_ORDERS,array('WIN_NUM'));
        $select->where($criteria); 
        $records = Core_Database::fetchAll($select);  
        if(empty($records)) return;

        $woIDs = "";
        foreach($records as $rec)
        {
            if(!empty($woIDs)) $woIDs .=',';
            
            $woIDs .= $rec['WIN_NUM'];
        }        
        //--- update
        $fieldvals = array("enable_expense_reporting"=>$enableExpRep);
        $updCriteria = "WINNUM IN ($woIDs)";        

        $result = Core_Database::update("work_orders_additional_fields",$fieldvals,$updCriteria);
        return $result;     
    }
    
    //--- 13389
    //$option: 1-All WorkOrders; 2-Unassigned WorkOrder
    /* 
    $updateFieldsArray = array(    
        array('bcatname'=>'Airfare',
    'updateData'=>array('bcatdesc'=>'hehe','ischecked'=>1,'reimbursable_thru_fs'=>1,'require_doc_upl_ifclaimed'=>1)
        ),
        array('bcatname'=>'Car Rental',
            'updateData'=>array('ischecked'=>0,'reimbursable_thru_fs'=>1)
        )
    );  
    */
    public function updateBCategoriesInfo_WorkOrders($companyID,$updateFieldsArray,$option)
    {
        if(empty($companyID)) return;
        if(empty($updateFieldsArray)) return;
        if(!is_array($updateFieldsArray)) return;
        if(count($updateFieldsArray)==0) return;
        //--- criteria
        $criteria = "(Company_ID = '$companyID')";
        if($option==1)
        {
            $criteria .= " AND (Deactivated = '0' AND TechMarkedComplete = '0' AND Approved = '0')";
        }
        else if($option==2)
        {
            $criteria .= " AND (Tech_ID IS NULL OR Tech_ID = '') AND TechMarkedComplete = '0' AND Approved = '0' AND  (Status = 'created' OR Status = 'published') ";
        }
        //--- get woIDs
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from(Core_Database::TABLE_WORK_ORDERS,array('WIN_NUM'));
        $select->where($criteria); 
        $records = Core_Database::fetchAll($select);  
        if(empty($records)) return;

        $woIDs = "";
        foreach($records as $rec)
        {
            if(!empty($woIDs)) $woIDs .=',';            
            $woIDs .= $rec['WIN_NUM'];
            
            $woID = $rec['WIN_NUM'];
            $this->insertBCatsIfnotExist($woID);
            foreach($updateFieldsArray as $updateFields)
            {
                $bcatname = $updateFields['bcatname'];
                $updateData = $updateFields['updateData'];
                $criteria = "(winid = $woID) AND (bcatname = '$bcatname')";
                $result = Core_Database::update("work_orders_bcats",$updateData,$criteria);            
            }            
        }        
        return ;
    }
    
    public function insertBCatsIfnotExist($winNum)
    {
        //--- get projectId, companyId
        $projectId = "";
        $companyId = "";
        $fieldArray = array('WIN_NUM','Project_ID','Company_ID');
        $db = Core_Database::getInstance();
        $query = $db->select(); 
        $query->from(Core_Database::TABLE_WORK_ORDERS,$fieldArray);        
        $query->where("WIN_NUM = '$winNum'");                
        $records = Core_Database::fetchAll($query);        
        if(!empty($records))
        {
            $projectId = $records[0]['Project_ID'];  
            $companyId = $records[0]['Company_ID'];  
        } 
        //-----
        $current = $this->getWorkOrderBCats($winNum);
        if(empty($current))
        {
            if(!empty($projectId))
            {
                //--- get data from project
                $projectApi = new Core_Api_ProjectClass();
                $records = $projectApi->getProjectBCats_insertFromClientIfNotExists($projectId);
                //--- 
                foreach($records as $rec)            
                {
                    $fieldvals = array("winid"=>$winNum,
                        "bcatname"=>$rec["bcatname"],
                        "bcatdesc"=>$rec["bcatdesc"],
                        "ischecked"=>$rec["ischecked"],
                        "reimbursable_thru_fs"=>$rec["reimbursable_thru_fs"],
                        "require_doc_upl"=>$rec["require_doc_upl"],
                        "bcattype"=>$rec["bcattype"]);   
                    
                    $bcatname = $rec["bcatname"];
                    if($this->exists_WinBCatName($winNum,$bcatname)==0);    
                        $result = Core_Database::insert("work_orders_bcats",$fieldvals);        
                }            
            }
        }       
        return;        
    }
    //--- 13433  
    //return 0, 1      
    public function Exists_TechsInSite($siteNumber,$projectID)
    {
        $db = Core_Database::getInstance();
        $query = $db->select(); 
        $query->from("tech_site");             
        $query->where("SiteNumber = '$siteNumber'");
        $query->where("Project_ID = '$projectID'");
        $records = Core_Database::fetchAll($query);        
        if(empty($records)) return 0;
        else return 1;
    }
    
    //--- 13433  
    //return 0, 1      
    public function Exists_AssignedTech($winnum)
    {
        $db = Core_Database::getInstance();
        $query = $db->select(); 
        $query->from(Core_Database::TABLE_WORK_ORDERS,array('Tech_ID'));        $query->where("Tech_ID > 0");
        $query->where("WIN_NUM = '$winnum'");
        $records = Core_Database::fetchAll($query);        
        if(empty($records)) return 0;
        else return 1;
    }
    //--- 13433
    public function getTechsInSite($siteNumber,$projectID,$techIDs='')
    {
        $db = Core_Database::getInstance();
        $query = $db->select(); 
        
        if(!empty($projectID))
        {
            $query->from(array('ts'=>'tech_site'),
                 array()       
            );             
            $query->join(array('t'=>Core_Database::TABLE_TECH_BANK_INFO),
                't.TechID = ts.techid',
                array('techid'=>'TechID','FirstName','LastName','FLSID','PrimaryPhone','PrimaryEmail')
            );        
            $query->where("ts.SiteNumber = '$siteNumber'");
            $query->where("ts.Project_ID = '$projectID'");
            $records = Core_Database::fetchAll($query);  
            return $records;       
        }
        else if(!empty($techIDs))
        {
            $query->from(Core_Database::TABLE_TECH_BANK_INFO,
                array('techid'=>'TechID','FirstName','LastName','FLSID','PrimaryPhone','PrimaryEmail')
            );
            $query->where("TechID in ($techIDs)");
            $records = Core_Database::fetchAll($query);  
            return $records;                   
        }
        else
        {
            return null;
        }
    }    
    //--- 13433
    public function getTechInfo($techID,$FLSID=0)
    {
        if(empty($techID) && empty($FLSID)) return null;
        $db = Core_Database::getInstance();
        $query = $db->select(); 
        $query->from(Core_Database::TABLE_TECH_BANK_INFO,
             array('FirstName','LastName','Password','FLSID','PrimaryPhone','PrimaryEmail','PrimaryEmail','UserName','techid'=>'TechID')
        );        
        
        if(!empty($techID))
            $query->where("TechID='$techID'");

        if(!empty($FLSID))
            $query->where("FLSID='$FLSID'");
        
        $records = Core_Database::fetchAll($query);  
        if(empty($records)) return null;
        else return $records[0];        
    }
    //--- 13433
    public function saveTechToSite($techID,$siteNumber,$projectID)
    {
        if(empty($techID) || empty($siteNumber) || empty($projectID)) return;
        
        $db = Core_Database::getInstance();
        $query = $db->select(); 
        $query->from("tech_site");             
        $query->where("SiteNumber = '$siteNumber'");
        $query->where("Project_ID = '$projectID'");
        $query->where("techid = '$techID'");
        $records = Core_Database::fetchAll($query); 
               
        if(empty($records))
        {
           $fieldvalues = array("techid"=>$techID,'SiteNumber'=>$siteNumber,
                'Project_ID'=>$projectID);
           $result = Core_Database::insert("tech_site",$fieldvalues);      
        }
    }    
    //--- 13433
    public function saveTechsToSite($techIDArray,$siteNumber,$projectID)
    {
        if(empty($techIDArray) || empty($siteNumber) || empty($projectID)) return;
        
        //--- remove all tech from site
        $this->RemoveAllTechsFromSite($siteNumber,$projectID);
        //--- insert techs to site
        foreach($techIDArray as $techID)
        {
            if(!empty($techID))
            {
               $fieldvalues = array("techid"=>$techID,'SiteNumber'=>$siteNumber,
                    'Project_ID'=>$projectID);
               $result = Core_Database::insert("tech_site",$fieldvalues);
            }
        }
        return;
    }    

    //--- 13433
    public function Count_TechsInSite($siteNumber,$projectID)
    {
        $db = Core_Database::getInstance();
        $query = $db->select(); 
        $query->from("tech_site",array('numoftechs'=>'(Count(*))')); 
        $query->where("SiteNumber = '$siteNumber'");
        $query->where("Project_ID = '$projectID'");
        $records = Core_Database::fetchAll($query);
        if(empty($records))  return 0;
        else return $records[0]['numoftechs'];
    }
    //--- 13433
    public function RemoveTechFromSite($techID,$siteNumber,$projectID)
    {
        $criteria = "techid = '$techID' AND SiteNumber = '$siteNumber' AND Project_ID='$projectID'";
        $db = Core_Database::getInstance();
        $result = $db->delete('tech_site',$criteria);
        return $result;
    }
    //--- 13433
    public function RemoveAllTechsFromSite($siteNumber,$projectID)
    {
        $criteria = "SiteNumber = '$siteNumber' AND Project_ID='$projectID'";
        $db = Core_Database::getInstance();
        $result = $db->delete('tech_site',$criteria);
        return $result;
    }

    //--13375, 13691
    public function updateFutureInfoCommDataForWOs($woIDs,$futureUpdate){
    	if(empty($futureUpdate))return;

    	$woUpdateFields = array('ProjectManagerName', 'ProjectManagerPhone', 'ProjectManagerEmail', 'ResourceCoordinatorName', 'ResourceCoordinatorPhone', 'ResourceCoordinatorEmail', 'EmergencyName', 'EmergencyPhone', 'EmergencyEmail', 'CheckInOutName', 'CheckInOutNumber', 
								  'CheckInOutEmail', 'TechnicalSupportName', 'TechnicalSupportPhone', 'TechnicalSupportEmail', 'SystemGeneratedEmailTo', 'SystemGeneratedEmailFrom', 'WorkAvailableEmailFrom', 'BidNotificationEmailTo', 'P2TNotificationEmailTo', 'WorkAssignedEmailTo');
        $woaddiUpdateFields = array('ACSNotifiPOC','ACSNotifiPOC_Phone','ACSNotifiPOC_Email');
                                  
    	$woUpdate = array();
    	$woaddiUpdate = array();
    	foreach($woUpdateFields as $w){
			if($futureUpdate[$w] != "") {
                $woUpdate[$w] = $futureUpdate[$w];               
            } 
    	}
        foreach($woaddiUpdateFields as $w){
            if($futureUpdate[$w] != "") {
                $woaddiUpdate[$w] = $futureUpdate[$w];               
            } 
        }
        
        if(count($woUpdate) > 0){
    	    $criteria = "WIN_NUM IN ($woIDs)";
            $result = Core_Database::update(Core_Database::TABLE_WORK_ORDERS,$woUpdate,$criteria);
        }
        
        if(count($woaddiUpdate)>0){
            $criteria = "WINNUM IN ($woIDs)";
            $result = Core_Database::update("work_orders_additional_fields",$woaddiUpdate,$criteria);
        }
    }

    //--- 13308    
    public function getStartEndDateTimestamp($winID)
    {
        /*
        StartDate not empty
        StartTime not empty: 
        --- EndDate not empty: 
        ------  EndTime not empty:                
        ------  EndTime empty:
                EndTime = StartTime + 4h    
        --- EndDate empty: 
            EndDate = StartDate 
            EndTime = StartTime + 4h 
        StartTime empty: 
            StartTime = '00:00:01'
        --- EndDate not empty: 
        ------  EndTime not empty:                
        ------  EndTime empty:
            EndTime = '23:59:59'        
        --- EndDate empty: 
            EndDate = StartDate 
            EndTime = '23:59:59' 
       */ 
        
        $db = Core_Database::getInstance();
        $query = $db->select(); 
        $query->from(Core_Database::TABLE_WORK_ORDERS,
            array('StartDate','StartTime','EndDate','EndTime')
        ); 
        $query->where("WIN_NUM='$winID'");
        $records = Core_Database::fetchAll($query);
        if(empty($records)) return null;
        else
        {
            $wo = $records[0];
            $dbStartDate = $wo['StartDate'];
            $dbStartTime = $wo['StartTime'];
            $dbEndDate = $wo['EndDate'];
            $dbEndTime = $wo['EndTime']; 
            $startDateTime = '';
            $endDateTime = '';
            $startTimestamp = 0;
            $endTimestamp = 0;
            if(empty($dbStartDate)) return null;          
           // if (($timestamp = strtotime($sDateTime)) === false) {return null;}
           if(!empty($dbStartTime))
           {
               $startDateTime = $dbStartDate.' '.$dbStartTime;
               if (($startTimestamp = strtotime($startDateTime)) === false) 
               { return null; }
               $timeAfter4Hour = $startTimestamp + 4*60*60;
               
               if(!empty($dbEndDate))
               {                
                  if(!empty($dbEndTime))
                  {
                       $endDateTime = $dbEndDate.' '.$dbEndTime;
                  }
                  else //empty($dbEndTime)
                  {
                      $endDateTime = $dbEndDate.' '.date("H:i:s",$timeAfter4Hour);
                  }
               }
               else //empty($dbEndDate)
               {
                   $endDateTime = $dbStartDate.' '.date("H:i:s",$timeAfter4Hour);
               }
                if (($endTimestamp = strtotime($endDateTime)) === false) 
               { return null; }
           }
           else //empty($dbStartTime)
           {
               $startDateTime = $dbStartDate.' 00:00:01';
               if (($startTimestamp = strtotime($startDateTime)) === false) 
               { return null; }

               if(!empty($dbEndDate))
               {
                   if(!empty($dbEndTime))
                   {
                       $endDateTime = $dbEndDate.' '.$dbEndTime;
                   }
                   else //empty($dbEndTime)
                   {
                        $endDateTime = $dbEndDate.' 23:59:59';
                   }
               }
               else //empty($dbEndDate)
               {
                   $endDateTime = $dbStartDate.' 23:59:59';
               }
               if (($endTimestamp = strtotime($endDateTime)) === false) 
               { return null; }
           }//end of else
        }//end of else
        return array('StartTimestamp'=>$startTimestamp,
                'EndTimestamp'=>$endTimestamp,
                'StartDateTime'=>$startDateTime,
                'EndDateTime'=>$endDateTime,
                'WIN_NUM'=>$winID
        );
    }//end of function
    //--- 13308
    //return 1: 0
   
    public function getStartEndDateTimestamp_1($startDate,$startTime,$endDate,$endTime)
    {
        if(empty($startDate)) return null;          
        $startDateTime = '';
        $endDateTime = '';
        $startTimestamp = 0;
        $endTimestamp = 0;
       // if (($timestamp = strtotime($sDateTime)) === false) {return null;}
       if(!empty($startTime))
       {
           $startDateTime = $startDate.' '.$startTime;
           if (($startTimestamp = strtotime($startDateTime)) === false) 
           { return null; }
           $timeAfter4Hour = $startTimestamp + 4*60*60;
           
           if(!empty($endDate))
           {                
              if(!empty($endTime))
              {
                   $endDateTime = $endDate.' '.$endTime;
              }
              else //empty($dbEndTime)
              {
                  $endDateTime = $endDate.' '.date("H:i:s",$timeAfter4Hour);
              }
           }
           else //empty($endDate)
           {
               $endDate = $startDate;
               $endDateTime = $endDate.' '.date("H:i:s",$timeAfter4Hour);
           }
            if (($endTimestamp = strtotime($endDateTime)) === false) 
           { return null; }
       }
       else //empty($startTime)
       {
           $startDateTime = $startDate.' 00:00:01';
           if (($startTimestamp = strtotime($startDateTime)) === false) 
           { return null; }

           if(!empty($endDate))
           {
               if(!empty($endTime))
               {
                   $endDateTime = $endDate.' '.$endTime;
               }
               else //empty($endTime)
               {
                    $endDateTime = $endDate.' 23:59:59';
               }
           }
           else //empty($endDate)
           {
               $endDate = $startDate;
               $endDateTime = $startDate.' 23:59:59';
           }
           if (($endTimestamp = strtotime($endDateTime)) === false) 
           { return null; }
       }//end of else

        return array('StartTimestamp'=>$startTimestamp,
                'EndTimestamp'=>$endTimestamp,
                'StartDateTime'=>$startDateTime,
                'EndDateTime'=>$endDateTime,
                'StartDate'=>$startDate,
                'EndDate'=>$endDate
        );
    }//end of function
    
    public function getWosScheduleConflict($assigningWinIDArray,$techID)
    {
        if(empty($assigningWinIDArray) || count($assigningWinIDArray)==0) return null;
        //--- assigning win info        
        $db = Core_Database::getInstance();
        $query = $db->select(); 
        $query->from(Core_Database::TABLE_WORK_ORDERS,
            array('WIN_NUM','Headline','City','State','Zipcode','StartDate','StartTime','EndDate','EndTime')
        ); 
        $query->where("WIN_NUM IN (?)",$assigningWinIDArray);
        //echo($query->__toString());
        $assigningWos = Core_Database::fetchAll($query);
        //echo("<br/>$assigningWos: ");print_r($assigningWos);return;
        if(empty($assigningWos)) return null;
        
        //--- get tech workorders
        $db = Core_Database::getInstance();
        $query = $db->select(); 
        $query->from(Core_Database::TABLE_WORK_ORDERS,
            array('WIN_NUM','Headline','City','State','Zipcode','StartDate','StartTime','EndDate','EndTime')
        ); 
        $query->where("Tech_ID = '$techID'");
        $records = Core_Database::fetchAll($query);
        if(empty($records)) return null;
        
        //--- main
        $conflictWos = array();
        for($i=0;$i<count($assigningWos);$i++)
        {
            $assigningWinID = $assigningWos[$i]['WIN_NUM'];
            $assigningW0Timestamps = $this->getStartEndDateTimestamp($assigningWinID);
            $assigningWos[$i]['Conflict']=0;
            if(empty($assigningW0Timestamps)) continue;
            $S0 = $assigningW0Timestamps['StartTimestamp'];
            $E0 = $assigningW0Timestamps['EndTimestamp'];
            if($S0 > $E0) continue;
            
            //echo("<br/>assigningStam: ");print_r($assigningW0Timestamps);
            foreach($records as $wo)
            {
                $winID = $wo['WIN_NUM'];
                $woTimestamps=$this->getStartEndDateTimestamp($winID);
                //echo("<br/>$winID: ");print_r($woTimestamps);
                if(empty($woTimestamps)) continue;
                $S1 = $woTimestamps['StartTimestamp'];
                $E1 = $woTimestamps['EndTimestamp'];
                if($S1 > $E1) continue;
                if(($S1 <= $S0 && $S0 < $E1) ||
                   ($S1 < $E0 && $E0 <= $E1) ||
                   ($S0 <= $S1 && $E1 <= $E0)                    
                )
                {
                    $assigningWos[$i]['Conflict']=1;
                    $conflictWos[]=$wo;
                }
            }            

        }
        
        if(empty($conflictWos) || count($conflictWos)==0) return null;
        else
        {
            return array('assiningWos'=>$assigningWos,
                    'techConflictWos'=>$conflictWos
            );
        }

    }    
    public function getScheduleConflictWhenEditWO($assigningWOInfo,$techID)
    {
    	if(empty($assigningWOInfo) || empty($techID)) return null;
        
        //--- assigning win info  
/*'WIN_NUM','Headline','City','State','Zipcode','StartDate','StartTime','EndDate','EndTime')*/
        $assigningWos = array();
        $assigningWos[0]=array('WIN_NUM'=>$assigningWOInfo['WIN_NUM'],
            'Headline'=>$assigningWOInfo['Headline'],
            'City'=>$assigningWOInfo['City'],
            'State'=>$assigningWOInfo['State'],
            'Zipcode'=>$assigningWOInfo['Zipcode'],
            'StartDate'=>$assigningWOInfo['StartDate'],
            'StartTime'=>$assigningWOInfo['StartTime'],
            'EndDate'=>$assigningWOInfo['EndDate'],
            'EndTime'=>$assigningWOInfo['EndTime']
        );
            
        $startDate = $assigningWOInfo['StartDate'];
        $startTime = $assigningWOInfo['StartTime'];
        $endDate = $assigningWOInfo['EndDate'];
        $endTime = $assigningWOInfo['EndTime'];
                    
        //--- get tech workorders
        $db = Core_Database::getInstance();
        $query = $db->select(); 
        $query->from(Core_Database::TABLE_WORK_ORDERS,
            array('WIN_NUM','Headline','City','State','Zipcode','StartDate','StartTime','EndDate','EndTime')
        ); 
        $query->where("Tech_ID = '$techID'");
        $records = Core_Database::fetchAll($query);
        if(empty($records)) return null;

        //--- main
        $conflictWos = array();
        for($i=0;$i<count($assigningWos);$i++)
        {
            $assigningW0Timestamps = $this->getStartEndDateTimestamp($startDate,
                $startTime,$endDate,$endTime);
                
            $assigningWos[$i]['Conflict']=0;
            if(empty($assigningW0Timestamps)) continue;
            $S0 = $assigningW0Timestamps['StartTimestamp'];
            $E0 = $assigningW0Timestamps['EndTimestamp'];
            if($S0 > $E0) continue;
            
            //echo("<br/>assigningStam: ");print_r($assigningW0Timestamps);
            foreach($records as $wo)
            {
                $winID = $wo['WIN_NUM'];
                $woTimestamps=$this->getStartEndDateTimestamp($winID);
                //echo("<br/>$winID: ");print_r($woTimestamps);
                if(empty($woTimestamps)) continue;
                $S1 = $woTimestamps['StartTimestamp'];
                $E1 = $woTimestamps['EndTimestamp'];
                if($S1 > $E1) continue;
                if(($S1 <= $S0 && $S0 < $E1) ||
                   ($S1 < $E0 && $E0 <= $E1) ||
                   ($S0 <= $S1 && $E1 <= $E0)                    
                )
                {
                    $assigningWos[$i]['Conflict']=1;
                    $conflictWos[]=$wo;
                }
            }            
        }

        if(empty($conflictWos) || count($conflictWos)==0) return null;
        else
        {
            return array('assiningWos'=>$assigningWos,
                    'techConflictWos'=>$conflictWos
            );
        }

    }

    //--- 13211
    public function saveClientDeniedTechComment($techID,$clientID,$companyID,
            $deniedComments)
    {
        $db = Core_Database::getInstance();
        $query = $db->select(); 
        $query->from("client_denied_techs");
        $query->where("TechID='$techID' AND ClientID='$clientID' AND Company_ID='$companyID'");
        $records = Core_Database::fetchAll($query);
        if(empty($records))
        {
            $fields = array('TechID'=>$techID,'ClientID'=>$clientID,
                'Company_ID'=>$companyID,'Comments'=>$deniedComments
            );
            $result = Core_Database::insert("client_denied_techs",$fields);
        }
        else
        {
            $criteria = "TechID='$techID' AND ClientID='$clientID' AND Company_ID='$companyID'";
            $fields = array('Comments'=>$deniedComments);
            $result = Core_Database::update("client_denied_techs",$fields,$criteria);
        }
        //--- inserrt into comments table
        $comment = 'Client Denied: '. $deniedComments;
        $fields = array('comment'=>$comment,'date'=>date('Y-m-d H:i:s'),
                    'TechID'=>$techID,'type'=>1,'classification'=>2,
                    'clientID'=>$clientID,
                    'Company_ID'=>$companyID
        );
        $result = Core_Database::insert("comments",$fields);
        
        return;
    }
    
    //--- 13211    
    public function getClientDeniedTechComment($techID,$clientID,$companyID)
    {
        $db = Core_Database::getInstance();
        $query = $db->select(); 
        $query->from("client_denied_techs");
        $query->where("TechID='$techID' AND ClientID='$clientID' AND Company_ID='$companyID'");
        $records = Core_Database::fetchAll($query);
        if(empty($records)) return '';
        else return $records[0]['Comments'];
    }    
    
    //--- 13211    
    public function getClientDeniedTechInfo($techID,$companyID)
    {
        $db = Core_Database::getInstance();
        $query = $db->select(); 
        $query->from(array('cdt'=>"client_denied_techs"),
            array('id','TechID','ClientID','Comments','Company_ID')
        );
        $query->joinLeft(array('cli'=>'clients'),
            'cli.ClientID=cdt.clientID',
            array('ClientUserName'=>'cli.UserName')
        );
        $query->where("cdt.TechID='$techID' AND cdt.Company_ID='$companyID'");
        //$query->where("cdt.Comments is not null AND cdt.Comments !=''");
        $query->order("(Case When cdt.Comments is not null AND cdt.Comments !='' Then 1 Else 0 End) desc, cdt.id desc");
        $records = Core_Database::fetchAll($query);
        if(empty($records)) return null;
        else return $records[0];
    }
    //--- 13211    
    public function TechNoLongerClientDenied($techID,$clientID,$companyID)
    {
        //is denied before?        
        $db = Core_Database::getInstance();
        $query = $db->select(); 
        $query->from("client_denied_techs");
        $query->where("TechID='$techID' AND Company_ID='$companyID'");
        $records = Core_Database::fetchAll($query);
        if(empty($records))  return;
                   
        //--- insert into comments table
        $comment = 'This technician is no longer client denied.';
        $fields = array('comment'=>$comment,'date'=>date('Y-m-d H:i:s'),
                    'TechID'=>$techID,'type'=>1,'classification'=>0,
                    'clientID'=>$clientID,'Company_ID'=>$companyID
        );
        $result = Core_Database::insert("comments",$fields);
        //--- remove from client_denied_techs
        $criteria = "TechID='$techID' AND Company_ID='$companyID'";
        $db = Core_Database::getInstance();
        $result = $db->delete('client_denied_techs',$criteria);
        return;
    }
    //--- 13211
    public function ClientPreferTech($techID,$clientID,$companyID)
    {
        //--- insert into comments table
        $comment = 'Preferred Tech.';
        $fields = array('comment'=>$comment,'date'=>date('Y-m-d H:i:s'),
                    'TechID'=>$techID,'type'=>1,'classification'=>0,
                    'clientID'=>$clientID
        );
        $result = Core_Database::insert("comments",$fields);
        
        //--- insert into client_preferred_techs
        /*id,Tech_ID,PreferLevel=1,CompanyID*/
        $preferFields = array('Tech_ID'=>$techID,'PreferLevel'=>1,'CompanyID'=>$companyID);
        $db = Core_Database::getInstance();
        $result = $db->insert('client_preferred_techs',$preferFields);
    	if($result){
    		return $techID;
    	}else{
    		return false;
    	}
    }
    //--- 13211
    public function ClientRemovePreferTech($techID,$clientID,$companyID)
    {
        //--- is preferred before?
        $db = Core_Database::getInstance();
        $query = $db->select(); 
        $query->from("client_preferred_techs");
        $query->where("Tech_ID='$techID'");
        $records = Core_Database::fetchAll($query);
        if(empty($records))  return;
        
        //Removed from Preferred List
        //--- insert into comments table
        $comment = 'Removed from Preferred List.';
        $fields = array('comment'=>$comment,'date'=>date('Y-m-d H:i:s'),
                    'TechID'=>$techID,'type'=>1,'classification'=>2,
                    'clientID'=>$clientID
        );
        $result = Core_Database::insert("comments",$fields);
        //--- remove from client_preferred_techs
        /*id,Tech_ID,PreferLevel=1,CompanyID*/
        $criteria = "Tech_ID='$techID' AND CompanyID='$companyID'";
        $db = Core_Database::getInstance();
        $result = $db->delete('client_preferred_techs',$criteria);
        
    	if($result >= 1){
    		return $techID;
    	}else{
    		return false;
    	}
    }

   //--- 13440
    public function getAllWOs_ByTechID($startDateFrom,$startDateTo,$endDateFrom,$endDateTo,$techID)
    {
        $db = Core_Database::getInstance();
        $query = $db->select(); 
        $query->from(Core_Database::TABLE_WORK_ORDERS,
            array('WIN_NUM','Tech_ID','StartDate','EndDate',
                'calculatedTechHrs','Time_In','Time_Out','EndTime','StartTime'
            )
        );            
        $query->where("StartDate is not null");
        $query->where("EndDate is not null");
        if(!empty($startDateFrom))
        {
            $temp = strtotime($startDateFrom);
            $query->where("StartDate >= '".date("Y-m-d",$temp)."'");
        }
        if(!empty($startDateTo))
        {
            $temp = strtotime($startDateTo);
            $query->where("StartDate <= '".date("Y-m-d",$temp)."'");
        }
        if(!empty($endDateFrom))
        {
            $temp = strtotime($endDateFrom);
            $query->where("EndDate >= '".date("Y-m-d",$temp)."'");
        }
        if(!empty($endDateTo))
        {
            $temp = strtotime($endDateTo);
            $query->where("EndDate <= '".date("Y-m-d",$temp)."'");
        }
        if(!empty($techID))
        {
            $query->where("Tech_ID = '$techID' ");            
        }
        $query->order("StartDate ASC");
        //echo("<br/>query: ");print_r($query->__toString());
        $wos = Core_Database::fetchAll($query);
        return $wos;                
    }
    //--- 13440
    public function getWorkHours_ByWOInfo($wo)
    {
        $apiWosClass = new Core_Api_WosClass();
        $calculatedTechHrs=0;
        $tech_hrs = $wo["calculatedTechHrs"];
        $calculatedTechHrs = $tech_hrs;
        /*
        if (empty($tech_hrs)) 
        {
            if(!empty($wo["Time_In"]) && !empty($wo["Time_Out"]))
            {
                $startTime = strtotime($wo["Time_In"]);
                $endTime = strtotime($wo["Time_Out"]);                            
                // Calc hours
                if ($endTime < $startTime) {
                    $endTime += 86400;
                }
                $calculatedTechHrs = round(($endTime - $startTime)/3600, 2);
            }
        }
        else
        {
            $calculatedTechHrs = $tech_hrs;
        }
        */
        /*
        if(empty($calculatedTechHrs))
        {
            $calculatedTechHrs = $apiWosClass->getWorkHours($wo['StartDate'],$wo['StartTime'],$wo['EndDate'],$wo['EndTime']);                        
        }
//echo("<br/>WIN_NUM: ".$wo['WIN_NUM'].", calculatedTechHrs: $calculatedTechHrs");         */
        return $calculatedTechHrs;
    }

    //--- 13440, 13462, 13469, 13761
    public function getCertificationsByCompany($companyID)
    {
        $BBCompanies = "BBAL,BBGC,BBGE,BBCN,BCSST,BBGN,BBJC,BBLC,BBNSG,BBNC,BBS,BB";
        $BBCompanyArray = split(",",$BBCompanies);
        $certificationIDs = "";
        if($companyID=='FATT') $certificationIDs="22,23,30";
        else if($companyID=='MDS') $certificationIDs="5,18,31";
        else if($companyID=='VELO') $certificationIDs="29";
        else if($companyID=='LMS') $certificationIDs="20,21";
        else if($companyID=='TECF') $certificationIDs="32";
        else if($companyID=='COMPU') $certificationIDs="33,34,35";
        else if($companyID=='PURP') $certificationIDs="39,40";
        else if($companyID=='ERG') $certificationIDs="41,42";
        else if(in_array($companyID,$BBCompanyArray)) $certificationIDs="46,47,48,49,50";

        if(empty($certificationIDs)) return null;

        $db = Core_Database::getInstance();
        $query = $db->select(); 
        $query->from(Core_Database::TABLE_CERTIFICATIONS);
        $query->where("id IN ($certificationIDs)");
        $records = Core_Database::fetchAll($query);
        return $records;        
    }

    //--- 13440
    public function getHoursFilled($woIDArray,$techID)
    {
        if(empty($woIDArray) || count($woIDArray)==0) return null;
        if(empty($techID)) return null;

        $db = Core_Database::getInstance();
        $query = $db->select(); 
        $query->from(Core_Database::TABLE_WORK_ORDERS);
        $query->where("Tech_ID = '$techID'",$woIDArray);
        $query->where("WIN_NUM IN (?)",$woIDArray);
        $wos = Core_Database::fetchAll($query);
        
        if(empty($wos)) return null;
        $hourFilled = 0;
        foreach($wos as $wo)
        {
            $calculatedTechHrs=0;
            $tech_hrs = $wo["calculatedTechHrs"];
            $startTime = strtotime($wo["Time_In"]);
            $endTime = strtotime($wo["Time_Out"]);
            if (empty($tech_hrs)) 
            {
                // Calc hours
                if ($endTime < $startTime) {
                    $endTime += 86400;
                }
                $calculatedTechHrs = round(($endTime - $startTime)/3600, 2);
            }
            else
            {
                $calculatedTechHrs = $tech_hrs;
            }
            $hourFilled += $calculatedTechHrs;
        }
        return $hourFilled;
    }
    //--- 13440
    public function getAvailHours($startDate,$endDate)
    {
        if(empty($startDate) || empty($endDate)) return null;
        $startDateTimeStamp = strtotime($startDate);
        $endDateTimeStamp = strtotime($endDate);
        $availHours = 0;
        $startTimeStamp = $startDateTimeStamp;
        while($startTimeStamp <= $endDateTimeStamp)
        {
            $dw = date( "w", $startTimeStamp);
            if($dw!=0 && $dw!=6)
            {
                $availHours += 8;
            }
            $startTimeStamp += 24*60*60;
        }
        return $availHours;
    }
    //--- 13440,13190    
    public function getWOsWithConditions($startDateFrom,$startDateTo,$endDateFrom,$endDateTo,
       $projectIDArray,$techID,$contractorID,
       $certificationID,$companyID,
       $wofields='', $startDateNotNull=1,$endDateNotNull=1,$techIDNotNull=1,
       $createdDateFrom='',$createdDateTo='',
       $approvedDateFrom='',$approvedDateTo='',
       $invoicedDateFrom='',$invoicedDateTo='')
    {
        $db = Core_Database::getInstance();
        $query = $db->select(); 
        $fields =  array('WIN_NUM','Tech_ID','StartDate','EndDate',
                'calculatedTechHrs','Time_In','Time_Out','EndTime','StartTime'
        );            
        if(!empty($wofields))    
        {
            $fields = $wofields;    
        }
        
        $query->from(Core_Database::TABLE_WORK_ORDERS,$fields);
        
        if(!empty($startDateNotNull))
        {
        $query->where("StartDate is not null");
        }
        if(!empty($endDateNotNull))
        {
        $query->where("EndDate is not null");
        }
        if(!empty($techIDNotNull))
        {
        $query->where("Tech_ID is not null");
        }                    
        if(!empty($startDateFrom))
        {
            $temp = strtotime($startDateFrom);
            $query->where("StartDate >= '".date("Y-m-d",$temp)."'");
        }
        if(!empty($startDateTo))
        {
            $temp = strtotime($startDateTo);
            $query->where("StartDate <= '".date("Y-m-d",$temp)."'");
        }
        if(!empty($endDateFrom))
        {
            $temp = strtotime($endDateFrom);
            $query->where("EndDate >= '".date("Y-m-d",$temp)."'");
        }
        if(!empty($endDateTo))
        {
            $temp = strtotime($endDateTo);
            $query->where("EndDate <= '".date("Y-m-d",$temp)."'");
        }
        if(!empty($projectIDArray) && count($projectIDArray)>0)
        {
            $query->where("Project_ID IN (?)",$projectIDArray);
        }
        if(!empty($techID))
        {
            $query->where("Tech_ID = '$techID' ");            
        }
        if(!empty($contractorID))
        {
            if($companyID=='FLS'){
                $query->where("FLS_ID = '$contractorID' "); 
            }
            else if($companyID=='FATT')
            {
                $techIDArray = Core_TechCertifications::getTechIDArray_ByCertIdAndNum(22,$contractorID);
                if(!empty($techIDArray))
                {
                    $query->where("Tech_ID IN (?)",$techIDArray);            
                }
            }
            
        }        
        if(!empty($companyID))
        {
            $query->where("Company_ID = '$companyID' ");            
        }
        //$certificationID
        $api_FSTag = new Core_Api_FSTagClass();
        if(!empty($certificationID))
        {
            if(is_array($certificationID))
            {
                if(count($certificationID) > 0)
                {
                    //$techIDArray = Core_TechCertifications::getTechIdsArray_WithAnyCertId($certificationID);
                    $techIDArray = $api_FSTag->getTechIDArray_forFSTag($certificationID);
                    if(empty($techIDArray) || count($techIDArray)==0)
                    {
                        $query->where("Tech_ID = -1"); //force tech not found
                    }
                    else

                    {
                        $query->where("Tech_ID IN (?)",$techIDArray);            
                    }        
                }
            }
            else
            {
                //$techIDArray = Core_TechCertifications::getTechIdsArray_ByCertId($certificationID);                
                $techIDArray = $api_FSTag->getTechIDArray_forFSTag(array($certificationID));
                if(empty($techIDArray) || count($techIDArray)==0)
                {
                    $query->where("Tech_ID = -1"); //force tech not found
                }
                else
                {
                    $query->where("Tech_ID IN (?)",$techIDArray);            
                }        
            }            
        }        
        
        if(!empty($createdDateFrom))
        {
            $temp = strtotime($createdDateFrom);
            $query->where("DateEntered >= '".date("Y-m-d",$temp)."'");
        }
        
        if(!empty($createdDateTo))
        {
            $temp = strtotime($createdDateTo);
            $query->where("DateEntered <= '".date("Y-m-d",$temp)."'");
        }
        if(!empty($approvedDateFrom))
        {
            $temp = strtotime($approvedDateFrom);
            $query->where("DateApproved >= '".date("Y-m-d",$temp)."'");
        }
        
        if(!empty($approvedDateTo))
        {
            $temp = strtotime($approvedDateTo);
            $query->where("DateApproved <= '".date("Y-m-d",$temp)."'");
        }
        if(!empty($invoicedDateFrom))
        {
            $temp = strtotime($invoicedDateFrom);
            $query->where("DateInvoiced >= '".date("Y-m-d",$temp)."'");
        }
        
        if(!empty($invoicedDateTo))
        {
            $temp = strtotime($invoicedDateTo);
            $query->where("DateInvoiced <= '".date("Y-m-d",$temp)."'");
        }

        $query->order("StartDate ASC");
                
        $wos = Core_Database::fetchAll($query);
        return $wos;                
    }

    //--- 13440
    public function getTechs_ByTechIDsArray($techIDsArray)
    {
        if(!empty($techIDsArray) && count($techIDsArray)>0)
        {
            $db = Core_Database::getInstance();
            $query = $db->select();
            $query->from(array('t'=>Core_Database::TABLE_TECH_BANK_INFO),
                array('TechID','FirstName','LastName','FLSID')            
            );
            if($companyID=='FATT')
            {
                $query->joinLeft(array('tc'=>Core_Database::TABLE_TECH_CERTIFICATION),
                        'tc.TechID=t.TechID AND tc.certification_id=22',
                        array('ContractorID'=>'tc.number')
                );
            }
            $query->where("TechID IN (?)",$techIDsArray);
            $techInfos = Core_Database::fetchAll($query);
            return $techInfos;
        }
        else
        {
            return null;            
        }
    }
    //--- 13440
    public function getTechsWithConditions($startDateFrom,$startDateTo,$endDateFrom,$endDateTo,$projectIDArray,$techID,$contractorID,$certificationID,$companyID)
    {
        $db = Core_Database::getInstance();
        $query = $db->select();
        $query->from(array('w'=>Core_Database::TABLE_WORK_ORDERS),
            array('Tech_ID')
        );                 
        $query->where("w.StartDate is not null");
        $query->where("w.EndDate is not null");
        $query->where("w.Tech_ID is not null");
        if(!empty($startDateFrom))
        {
            $temp = strtotime($startDateFrom);
            $query->where("w.StartDate >= ".date("Y-m-d",$temp));
        }
        if(!empty($startDateTo))
        {
            $temp = strtotime($startDateTo);
            $query->where("w.StartDate <= ".date("Y-m-d",$temp));
        }
        if(!empty($endDateFrom))
        {
            $temp = strtotime($endDateFrom);
            $query->where("w.EndDate >= ".date("Y-m-d",$temp));
        }
        if(!empty($endDateTo))
        {
            $temp = strtotime($endDateTo);
            $query->where("w.EndDate <= ".date("Y-m-d",$temp));
        }
        if(!empty($projectIDs))
        {
            $query->where("w.Project_ID IN (?)",$projectIDArray);
        }
        if(!empty($techID))
        {
            $query->where("w.Tech_ID = '$techID' ");            
        }
        if(!empty($contractorID))
        {
            if($companyID=='FLS'){
                $query->where("w.FLS_ID = '$contractorID' "); 
            }
            else if($companyID=='FATT')
            {
                $techIDArray = Core_TechCertifications::getTechIDArray_ByCertIdAndNum(22,$contractorID);
                if(!empty($techIDArray))
                {
                    $query->where("w.Tech_ID IN (?)",$techIDArray);            
                }
            }
        }        
        if(!empty($companyID))
        {
            $query->where("w.Company_ID = '$companyID' ");            
        }
        //$certificationID
        if(!empty($certificationID))
        {
            $techIDArray = Core_TechCertifications::getTechIdsArray_ByCertId($certificationID);
            $query->where("w.Tech_ID IN (?)",$techIDArray);            
        }        
        $query->group("w.Tech_ID");
        $techs = Core_Database::fetchAll($query);
        if(!empty($techs))
        {
            $techIDs = array();
            foreach($techs as $tech) $techIDs[]=$tech['Tech_ID'];
            $db = Core_Database::getInstance();
            $query_1 = $db->select();
            $query_1->from(array('t'=>Core_Database::TABLE_TECH_BANK_INFO),
                array('TechID','FirstName','LastName','FLSID')            
            );
            if($companyID=='FATT')
            {
                $query_1->joinLeft(array('tc'=>Core_Database::TABLE_TECH_CERTIFICATION),
                        'tc.TechID=t.TechID AND tc.certification_id=22',
                        array('ContractorID'=>'tc.number')
                );
            }
            $query_1->where("TechID IN (?)",$techIDs);
            $techInfos = Core_Database::fetchAll($query_1);
            return $techInfos;
        }
        else
        {
            return null;            
        }
    }
    //13440
    public function getWorkHours($startDate,$startTime,$endDate,$endTime)
    {
        $timeStampInfo = $this->getStartEndDateTimestamp_1($startDate,$startTime,$endDate,$endTime);
        $start = $timeStampInfo['StartTimestamp'];        
        $end = $timeStampInfo['EndTimestamp'];  
        if($start >= $end)  return 0;
        
        $endDate = $timeStampInfo['EndDate'];    
        $start0 = strtotime($startDate.' 08:00:00');
        $end0 = strtotime($startDate.' 17:00:00');
        $start1 = strtotime($endDate.' 08:00:00');
        $end1 = strtotime($endDate.' 17:00:00');
        
        $startDate_tstamp = strtotime($startDate);
        $endDate_tstamp = strtotime($endDate);
        $days = floor(abs($endDate_tstamp - $startDate_tstamp)/ (60*60*24));
        
        //echo("<br/>days: $days");
        
        $hours = 0;
        if($days==0)
        {
            $hours = ($end-$start)/3600;
        }
        else if($days >=1)
        {
            //--- work-hours in the first day
            $dw = date( "w", $start);
            if($dw!=0 && $dw!=6)
            {
                if($start <= $start0) $hours += 8;  
                else if($start < $end0) $hours += ($end0-$start)/3600;            
            }
            //--- work-hours in the end day
            $ew = date( "w", $end);
            if($ew!=0 && $ew!=6)
            {
                if($end >= $end1) $hours += 8;  
                else if($end > $start1) $hours += ($end-$start1)/3600; 
            }
            //--- work-hours in the other days
            if($days >=2)
            {
                $startDate_Plus1 = date("Y-m-d",$start+3600*24);
                $endDate_Minus1 = date("Y-m-d",$end-3600*24);
                $availHours = $this->getAvailHours($startDate_Plus1,$endDate_Minus1);
                $hours += $availHours;
            }            
        }
        return $hours;
    }

    //--- 13458
    public function removeWOVisit($woVisitID)
    {
        if(empty($woVisitID)) return;
        //--- wo-visit info
        $woVisitInfo = $this->getWOVisitInfo($woVisitID);
        if(empty($woVisitInfo)) return;
        
        $winNum = $woVisitInfo['WIN_NUM'];
        
        //--- remove
        $criteria = "WoVisitID = '$woVisitID' AND Type <> 1 ";
        $db = Core_Database::getInstance();
        $result = $db->delete('work_order_visits',$criteria);
        
        //--- refresh Qty_Visits
        $this->refreshQtyVisits($winNum);        
                
    }
    //--- 13458
    public function saveWOVisit_arrayPara($woVisitID,$fieldvals)
    {
        
        if(empty($woVisitID)) // insert
        {
            $result = Core_Database::insert("work_order_visits",$fieldvals);  
            //13763: email
            $type = $fieldvals['Type'];
            $winNum = $fieldvals["WIN_NUM"];
            if(isset($type) && $type==self::VISIT_TYPE_BASE && !empty($fieldvals['CORCheckInDate']) && !empty($fieldvals['CORCheckInTime'])){
                $checkedInDateTime = $fieldvals['CORCheckInDate'] . ' ' . $fieldvals['CORCheckInTime'];
                $emailCore = new Core_EmailForTechCheckedIN();
                $emailCore->sendMail($winNum,$checkedInDateTime);
            }

            if(isset($type) && $type==self::VISIT_TYPE_BASE && !empty($fieldvals['CORCheckOutDate']) && !empty($fieldvals['CORCheckOutTime'])){
                    $emailCore = new Core_EmailForTechCheckedOUT();
                    $emailCore->sendMail($winNum);
            } 

            if(isset($type) && $type==self::VISIT_TYPE_BASE && !empty($fieldvals['TechCheckInDate']) && !empty($fieldvals['TechCheckInTime'])){
                $checkedInDateTime = $fieldvals['TechCheckInDate'] . ' ' . $fieldvals['TechCheckInTime'];
                $emailCore = new Core_EmailForTechCheckedIN();
                $emailCore->sendMail($winNum,$checkedInDateTime);
            }

            if(isset($type) && $type==self::VISIT_TYPE_BASE && !empty($fieldvals['TechCheckOutDate']) && !empty($fieldvals['TechCheckOutTime'])){
                    $emailCore = new Core_EmailForTechCheckedOUT();
                    $emailCore->sendMail($winNum);
            } 
            
        }
        else //update
        {            
            //13763: email
            $curVisit = $this->getWOVisitInfo($woVisitID);
            $winNum = $curVisit["WIN_NUM"];
            $criteria = "WoVisitID='$woVisitID' ";  
            $result = Core_Database::update("work_order_visits",$fieldvals,$criteria);
            
            if($curVisit['Type']==self::VISIT_TYPE_BASE && empty($curVisit['CORCheckInDate']) && empty($curVisit['CORCheckInTime']) && !empty($fieldvals['CORCheckInDate']) && !empty($fieldvals['CORCheckInTime']) ){
                $checkedInDateTime = $fieldvals['CORCheckInDate'] . ' ' . $fieldvals['CORCheckInTime'];
                $emailCore = new Core_EmailForTechCheckedIN();
                $emailCore->sendMail($winNum,$checkedInDateTime);   
            }

            
            if($curVisit['Type']==self::VISIT_TYPE_BASE && empty($curVisit['CORCheckOutDate']) && empty($curVisit['CORCheckOutTime']) && !empty($fieldvals['CORCheckOutDate']) && !empty($fieldvals['CORCheckOutTime']) ){
                $checkedInDateTime='';
                if(!empty($fieldvals['CORCheckInDate']) && !empty($fieldvals['CORCheckInTime'])){
                    $checkedInDateTime = $fieldvals['CORCheckInDate'] . ' ' . $fieldvals['CORCheckInTime'];
                }
                $checkedOutDateTime='';
                $checkedOutDateTime = $fieldvals['CORCheckOutDate'] . ' ' . $fieldvals['CORCheckOutTime'];
                
                $emailCore = new Core_EmailForTechCheckedOUT();
                $emailCore->sendMail($winNum,$checkedInDateTime,$checkedOutDateTime);                
            }
            
            if($curVisit['Type']==self::VISIT_TYPE_BASE && empty($curVisit['TechCheckInDate']) && empty($curVisit['TechCheckInTime']) && !empty($fieldvals['TechCheckInDate']) && !empty($fieldvals['TechCheckInTime']) ){
                $checkedInDateTime = $fieldvals['TechCheckInDate'] . ' ' . $fieldvals['TechCheckInTime'];
                $emailCore = new Core_EmailForTechCheckedIN();
                $emailCore->sendMail($winNum);                
            }
            
            if($curVisit['Type']==self::VISIT_TYPE_BASE && empty($curVisit['TechCheckOutDate']) && empty($curVisit['TechCheckOutTime']) && !empty($fieldvals['TechCheckOutDate']) && !empty($fieldvals['TechCheckOutTime']) ){
                $checkedInDateTime='';
                if(!empty($fieldvals['TechCheckInDate']) && !empty($fieldvals['TechCheckInTime'])){
                    $checkedInDateTime = $fieldvals['TechCheckInDate'] . ' ' . $fieldvals['TechCheckInTime'];
                }
                $checkedOutDateTime='';
                $checkedOutDateTime = $fieldvals['TechCheckOutDate'] . ' ' . $fieldvals['TechCheckOutTime'];

                $emailCore = new Core_EmailForTechCheckedOUT();
                $emailCore->sendMail($winNum,$checkedInDateTime,$checkedOutDateTime);                
            }
        }
    }
    //--- 13458
    public function existWOVisit($winNum)
    {
        $db = Core_Database::getInstance();
        $query = $db->select();
        $query->from('work_order_visits');                 
        $query->where("WIN_NUM = '$winNum'");
        $records = Core_Database::fetchAll($query);
        if(empty($records)) return false;
        else return true;
    }

    //--- 13436
    public function existBaseWOVisit($winNum)
    {
        $db = Core_Database::getInstance();
        $query = $db->select();
        $query->from('work_order_visits');                 
        $query->where("WIN_NUM = '$winNum'");
        $query->where("Type = 1");
        
        $records = Core_Database::fetchAll($query);
        if(empty($records)) return false;
        else return true;
    }
    
    //--- 13458
    public function getWOVisitInfo($woVisitID)
    {
		$this->connectWOVisitToWorkOrder(); // clean up unconnected visits
        $db = Core_Database::getInstance();
        $query = $db->select();
        $query->from('work_order_visits');                 
        $query->where("WoVisitID = '$woVisitID'");
        $records = Core_Database::fetchAll($query);
        if(empty($records)) return null;
        else return $records[0];
    }
    public function getBaseWOVisitInfo($winNums)
    {
		// returns multiple    
		if (empty($winNums)) $records = array();
		else {    
	        $db = Core_Database::getInstance();
	        $query = $db->select();
	        $query->from('work_order_visits');                 
	        $query->where("WIN_NUM IN (?)", $winNums);
	        $query->where("Type = 1");
	        $records = Core_Database::fetchAll($query);
		}
		$result = array();
		foreach ($records as $record) {
				$result[$record['WIN_NUM']] = $record;
		}

        if(empty($result)) return null;
        else return $result;
    }
    //--- 13458
    public function getBaseWOVisitInfo_ByWinnum($winNum, $create_if_empty = true)
    {
		$result = array ();
		$return_multiple = true;

		if (!is_array ($winNum)) {
			$winNum = array ($winNum);
			$return_multiple = false;
		}

		if ($create_if_empty) {
			foreach ($winNum as $win) {
				$this->createWOVisitIfEmpty($win);
			}
		}

		$db = Core_Database::getInstance();
		$query = $db->select();
		$query->from('work_order_visits');
		$query->where("WIN_NUM IN (?)", $winNum);
		$query->where("Type = 1");
		$query->order("WIN_NUM");
		$records = Core_Database::fetchAll($query);

		foreach ($records as $record) {
			$win = $record["WIN_NUM"];

			if (!isset ($result[$win])) {
				$result[$win] = $record;
			}
		}

		if ($return_multiple) {
			return $result;
		}
		else {
			if (empty ($result)) return null;
			return $result[$winNum[0]];
		}

		//if(empty($records)) return null;
		//else return $records[0];
    }
    //--- 13458
    public function createWOVisitIfEmpty($winNum,$updateTechCheckInOut=1)
    {
        //--- get WO info.
        $db = Core_Database::getInstance();
        $query = $db->select(); 
        $query->from(Core_Database::TABLE_WORK_ORDERS,
            array('WIN_NUM','StartDate','EndDate','EndTime','StartTime','Duration','Date_In','Time_In','Date_Out','Time_Out','calculatedTechHrs')
        );            
        $query->where("WIN_NUM = '$winNum'");
        $records = Core_Database::fetchAll($query);
        if(empty($records)) return;
        $wo = $records[0];

        if(!$this->existWOVisit($winNum))
        {
            //--- insert into work_order_visits
            //WIN_NUM,StartDate,StartTime,EndDate,EndTime,Duration
            $duration = $this->convertDuration($wo['Duration']);
            $fieldvals = array("WIN_NUM"=>$wo['WIN_NUM'],
                        "WOVisitOrder"=>1,
                        "StartTypeID"=>1,
                        "StartDate"=>$wo["StartDate"],
                        "StartTime"=>$wo["StartTime"],
                        "EndDate"=>$wo["EndDate"],
                        "EndTime"=>$wo["EndTime"],
//                        "TechArrivalInstructions"=>$wo["Duration"],
                        "TechCheckInDate"=>$wo["Date_In"],
                        "TechCheckInTime"=>$wo["Time_In"],
                        "TechCheckOutDate"=>$wo["Date_Out"],
                        "TechCheckOutTime"=>$wo["Time_Out"],
                        "OptionCheckInOut"=>1,
                        "Type"=>self::VISIT_TYPE_BASE);
                        
            if(!empty($duration)) $fieldvals['EstimatedDuration']=$duration;
            $result = Core_Database::insert("work_order_visits",$fieldvals);
        }
        else //update TechCheckInDate,...,TechCheckOutTime 
        {                
             if($updateTechCheckInOut==1)
             {           
                 $criteria = "WIN_NUM='$winNum' AND WOVisitOrder=1 AND Type=1";
                 $fieldvals = array("TechCheckInDate"=>$wo["Date_In"],
                            "TechCheckInTime"=>$wo["Time_In"],
                            "TechCheckOutDate"=>$wo["Date_Out"],
                            "TechCheckOutTime"=>$wo["Time_Out"]                        
                            );
                $result = Core_Database::update("work_order_visits",$fieldvals,$criteria);
             }
        }
    }
    //--- 13458
    public function getMaxWOVisitOrder($winNum, $companyID = NULL, $WO_ID = NULL)
    {
        //return 35;
        $db = Core_Database::getInstance();
        $query = $db->select();
        $query->from('work_order_visits',array('maxOrder'=>'(max(WOVisitOrder))'));
		if (!empty($winNum))
	        $query->where("WIN_NUM = '$winNum'");
		else if (!empty($WO_ID) && !empty($companyID)){
	        $query->where("Company_ID = ?", $companyID);
	        $query->where("WO_ID = ?", $WO_ID);
		}
        $records = Core_Database::fetchAll($query);
        
        if(empty($records)) return 0;
        else 
        {
            $ret = $records[0]['maxOrder'];
            if($ret==null || $ret==0)
            {
                $db = Core_Database::getInstance();
                $query = $db->select();
                $query->from('work_order_visits',array('num'=>'(Count(*))'));
                if (!empty($winNum))
                    $query->where("WIN_NUM = '$winNum'");
                else if (!empty($WO_ID) && !empty($companyID)){
                    $query->where("Company_ID = ?", $companyID);
                    $query->where("WO_ID = ?", $WO_ID);
                }
                $record_1s = Core_Database::fetchAll($query);
                return $record_1s[0]['num']; 
            }
            else return $ret;
        }
    }
	 // 14005
    public function getBaseWOVisitInfoDisplayrules($winNum)
    {   
        $db = Core_Database::getInstance();
        $query = $db->select();
        $arrayfields = array('WIN_NUM','WoVisitID','StartDate','StartTime','EndDate','EndTime','Duration' => 'EstimatedDuration',							
                            'Date_In' => '(CASE WHEN IFNULL(OptionCheckInOut,0) = 2 THEN CORCheckInDate ELSE TechCheckInDate END)',
                            'Time_In' => '(CASE WHEN IFNULL(OptionCheckInOut,0) = 2 THEN CORCheckInTime ELSE TechCheckInTime END)',
                            'Date_Out' => '(CASE WHEN IFNULL(OptionCheckInOut,0) = 2 THEN CORCheckOutDate ELSE TechCheckOutDate END)',
                            'Time_Out' => '(CASE WHEN IFNULL(OptionCheckInOut,0) = 2 THEN CORCheckOutTime ELSE TechCheckOutTime END)'
                            );
        $query->from('work_order_visits',$arrayfields);                 
        $query->where("WIN_NUM = '$winNum'");
        $query->where("Type = 1 OR Type = 3");
        $query->where("DATEDIFF(CURDATE(),StartDate) = 0");      
        $records = Core_Database::fetchAll($query);
            
        if(!empty($records)) return $records[0];
        if(empty($records))
        {
                $query = $db->select();
                 $query->from('work_order_visits',$arrayfields);                        
                $query->where("WIN_NUM = '$winNum'");
                $query->where("Type = 1 OR Type = 3");
                $query->where("DATEDIFF(CURDATE(),StartDate) < 0");
                $query->order("StartDate asc");
                $records = Core_Database::fetchAll($query);			
        }
        if(!empty($records)) 
        {
                return $records[0];
        }else 
        {
                $query = $db->select();
                 $query->from('work_order_visits',$arrayfields);                         
                $query->where("WIN_NUM = '$winNum'");
                $query->where("Type = 1 OR Type = 3");		
                $query->order("StartDate DESC");
                $records = Core_Database::fetchAll($query);
        }
		if(!empty($records)) 
        {
                return $records[0];
        }
        return null;
    }// End 14005

    //--- 13458
    //base visit and client-added-re-visits
    public function GetWOVisitList($winNum)
    {
        $this->createWOVisitIfEmpty($winNum);
        
        $db = Core_Database::getInstance();
        $query = $db->select();
        $query->from('work_order_visits');                 
        $query->where("WIN_NUM = '$winNum'");
        $query->where("Type = 1 OR Type = 3");
        $query->order("WOVisitOrder asc");
        $query->order("StartDate asc");
        $records = Core_Database::fetchAll($query);
        return $records;
    }
    
    //--- 13458
    public function GetAllWOVisitList($winNum,$forTechSite=0,$forAssignmentOnly=0,$fields=NULL)
    {
		$result = array ();
		$return_multiple = true;

		if (!is_array ($winNum)) {
			$winNum = array ($winNum);
			$return_multiple = false;

			if(empty($forTechSite)) {
				$this->createWOVisitIfEmpty($winNum);
			}
		}

        $db = Core_Database::getInstance();
        $query = $db->select();
		if (is_array($fields))
	        $query->from('work_order_visits', $fields);
		else
	        $query->from('work_order_visits');
        $query->where("WIN_NUM IN (?)", $winNum);
        if($forAssignmentOnly==1)
        {
            $query->where("Type=1 OR Type=3");
        }

		$query->order ("WIN_NUM");

        if($forTechSite==1)
            $query->order("(Case When Type=1 Then 1 When Type=3 Then 2 Else 3 End)");
        else
            $query->order("Type asc");

        $records = Core_Database::fetchAll($query);

		if (!empty($fields)) return $records;
		foreach ($records as $record) {
			$win = $record["WIN_NUM"];

			$techCheckInDate = $record['TechCheckInDate'];
			$techCheckInTime = $record['TechCheckInTime'];
			$techCheckOutDate = $record['TechCheckOutDate'];
			$techCheckOutTime = $record['TechCheckOutTime'];
			$CORCheckInDate = $record['CORCheckInDate'];
			$CORCheckInTime = $record['CORCheckInTime'];
			$CORCheckOutDate = $record['CORCheckOutDate'];
			$CORCheckOutTime = $record['CORCheckOutTime'];
                $techTotalHours = $this->getTotalHours($techCheckInDate,$techCheckInTime,$techCheckOutDate,$techCheckOutTime);
                $CORTotalHours = $this->getTotalHours($CORCheckInDate,$CORCheckInTime,$CORCheckOutDate,$CORCheckOutTime);
                
			$record['TechTotalHours'] = $techTotalHours;
			$record['DisplayTechTotalHours'] = $this->getDurationDesc($techTotalHours,true);
			$record['CORTotalHours'] = $CORTotalHours;
			$record['DisplayCORTotalHours'] = $this->getDurationDesc($CORTotalHours,true);

			if (!isset ($result[$win])) $result[$win] = array ();

			$result[$win][] = $record;
            }

		if ($return_multiple) {
			return $result;
		}
		else {
			if(isset ($result[$winNum[0]])) {
				return $result[$winNum[0]];
	        }
    	    else return null;
		}
    }
    //--- 13458    
    public function getWOVisitIDWithCompanyAndWOID($Company_ID,$WO_ID,$order = NULL,$startDate = NULL)
	{
        $db = Core_Database::getInstance();
        $query = $db->select();
        $query->from('work_order_visits');                 
        $query->where("Company_ID = ?", $Company_ID);
        $query->where("WO_ID = ?", $WO_ID);
		if (!empty($order)) $query->where("WOVisitOrder = ?", $order);
		if (!empty($startDate)) $query->where("StartDate = ?", $startDate);
        $records = Core_Database::fetchAll($query);
        if(empty($records)) return null;
        else return $records[0];
	}

    public function connectWOVisitToWorkOrder()
	// fill in WIN_NUM of all visits created w/o WIN_NUM
	{
        $db = Core_Database::getInstance();
		$db->query("UPDATE work_order_visits AS v JOIN work_orders AS w ON (v.Company_ID = w.Company_ID AND v.WO_ID = w.WO_ID) SET v.WIN_NUM = w.WIN_NUM WHERE v.WIN_NUM = 0");
	}

	public function copyWOVisits($winNumFrom, $winNumTo, $blankVisits = false, $max = 0) {
        $woapi = new Core_Api_WosClass;
        $visits = $woapi->GetWOVisitList($winNumFrom);
		$count = 0;
        foreach ($visits as $index => $visit) {
			if (!empty($max) && $count > $max - 1) continue;
			++$count;
                if ($visit["Type"] == Core_Api_WosClass::VISIT_TYPE_BASE) {
			if (!$blankVisits) {
                        $woapi->saveBaseWOVisit($winNumTo,
                                $visit["Type"],
                                $visit["StartDate"],
                                $visit["StartTime"],
                                $visit["EndDate"],
                                $visit["EndTime"],
                                $visit["EstimatedDuration"],
                                $visit["TechArrivalInstructions"]
                        );
                }
                else {
                        $woapi->saveBaseWOVisit($winNumTo,
                                NULL,
                                NULL,
                                NULL,
                                NULL,
                                NULL,
                                NULL,
                                NULL
                        );
			}
                }
                else {
			if (!$blankVisits) {
                        $woapi->saveWOVisit(NULL,
                                $winNumTo,
                                $visit["Type"],
                                $visit["StartDate"],
                                $visit["StartTime"],
                                $visit["EndDate"],
                                $visit["EndTime"],
                                $visit["EstimatedDuration"],
                                $visit["TechArrivalInstructions"],
				NULL,
				NULL,
				NULL,
				1,
				$visit["VisitDisable"]
                        );
                }
			else {
                        $woapi->saveWOVisit(NULL,
                                $winNumTo,
                                NULL,
                                NULL,
                                NULL,
                                NULL,
                                NULL,
                                NULL,
                                NULL,
				NULL,
				NULL,
				NULL,
				1,
				$visit["VisitDisable"]
                        );
        }
                }
        }
	}
	
    public function saveWOVisit($woVisitID,$winNum,$startType,
    $startDate,$startTime,$endDate,$endTime,$duration,$techArvInsts,
    $Company_ID = NULL,$WO_ID = NULL,$order = NULL, $Section = 1, $disabled = 0)
	// pass Company_ID and WO_ID if winNum is not known (ex. at wo creation time). Passing order will try to update a particular entry
    {
        //echo("saveWOVisit->winNum: $winNum");die();
        if(empty($winNum) && (empty($Company_ID) || empty($WO_ID) || empty($order))) return;
        
        //--- wo info
        $wo = $this->getWoInfo($winNum,array('isProjectAutoAssign'));
        if(empty($wo) && (empty($Company_ID) || empty($WO_ID) || empty($order))) 
			return;
		else {
	        $isProjectAutoAssign = $wo['isProjectAutoAssign'];

			//--- validate 
			$validfieldvals = array("StartTypeID"=>$startType,
							"StartDate"=>$startDate,
							"StartTime"=>$startTime,
							"EndDate"=>$endDate,
							"EndTime"=>$endTime,
							"EstimatedDuration"=>$duration,
							"TechArrivalInstructions"=>$techArvInsts,
							"isProjectAutoAssign"=> $isProjectAutoAssign,
							"VisitDisable"=> $disabled
							);

	        $errors = Core_Api_Error::getInstance();
	        $coreValid = new Core_Validation_WoVisit($validfieldvals);
			if ($coreValid->validate() === false) {
				$errors->addErrors('6', $coreValid->getErrors());
				return API_Response::fail();
			}
		}
        
        //--- save into work_order_visits table
        $maxWOVisitOrder = $this->getMaxWOVisitOrder($winNum, $Company_ID, $WO_ID);   
        
        $visitType = "";
		if (empty($duration) &&
			!empty($startDate) && !empty($startTime) &&
			!empty($endDate) && !empty($endTime) && $startType == 1) { // only do for start type firm
				$sTime = strtotime($startDate . ' ' . $startTime);
				$eTime = strtotime($endDate . ' ' . $endTime);
				$duration = round(($eTime - $sTime)/3600, 2);
		}		
        $fieldvals = array("StartTypeID"=>$startType,
                        "StartDate"=>$startDate,
                        "StartTime"=>$startTime,
                        "EndDate"=>$endDate,
                        "EndTime"=>$endTime,
                        "EstimatedDuration"=>$duration,
                        "TechArrivalInstructions"=>$techArvInsts,
						"VisitDisable"=> $disabled,
						"Company_ID"=>$Company_ID,
						"WO_ID"=>$WO_ID);
                        
        //--- 13543
        if(!empty($order))
        {
            $fieldvals['WOVisitOrder']=$order;
        }
        //---                
        if(empty($woVisitID)) // insert, 13543
        {
        	if ($Section == "1") {
	            //--- WIN_NUM
	            $fieldvals['WIN_NUM'] = $winNum;
	            //--- WOVisitOrder
	            $WOVisitOrder = $maxWOVisitOrder + 1;
	            $fieldvals['WIN_NUM'] = $winNum;
	            $fieldvals['WOVisitOrder'] = $WOVisitOrder;
	            //--- Type
	            if($WOVisitOrder==1 && !empty($winNum)) $visitType = self::VISIT_TYPE_BASE;
	            else $visitType = self::VISIT_TYPE_CLIENT;
	            $fieldvals['Type'] = $visitType;
            
	            $this->saveWOVisit_arrayPara($woVisitID,$fieldvals);
        	}
        }
        else
        {
            $visitInfo = $this->getWOVisitInfo($woVisitID);
            $visitType = $visitInfo['Type'];
            //error_log("19806a " . print_r($fieldvals,true));
            $this->saveWOVisit_arrayPara($woVisitID,$fieldvals);
        }
        
        //--- save StartDate,StartTime,EndDate,EndTime,Duration into work_orders table
        if($visitType==1)
        {
            $wofieldvals = array('StartDate'=>$startDate,
                            'StartTime'=>$startTime,
                            'EndDate'=>$endDate,
                            'EndTime'=>$endTime,
                            'Duration'=>$duration
            );
            $this->updateWoInfo($winNum,$wofieldvals);
        }
    }
    
    //--- 13458,13436
    public function saveBaseWOVisit($winNum,$startType,
    $startDate,$startTime,$endDate,$endTime,$duration,$techArvInsts,
    $Company_ID = NULL,$WO_ID = NULL)
    {
        //echo("saveWOVisit->winNum: $winNum");die();
        if(empty($winNum) && (empty($Company_ID) || empty($WO_ID))) return;
        
        //--- wo info
        $wo = $this->getWoInfo($winNum,array('isProjectAutoAssign'));
        if(empty($wo) && (empty($Company_ID) || empty($WO_ID) || empty($order))) 
            return;
        else {
            $isProjectAutoAssign = $wo['isProjectAutoAssign'];

            //--- validate 
            $validfieldvals = array("StartTypeID"=>$startType,
                            "StartDate"=>$startDate,
                            "StartTime"=>$startTime,
                            "EndDate"=>$endDate,
                            "EndTime"=>$endTime,
                            "EstimatedDuration"=>$duration,
                            "TechArrivalInstructions"=>$techArvInsts,
                            "isProjectAutoAssign"=> $isProjectAutoAssign
                            );
        
            $errors = Core_Api_Error::getInstance();
            $coreValid = new Core_Validation_WoVisit($validfieldvals);
            if ($coreValid->validate() === false) {
                $errors->addErrors('6', $coreValid->getErrors());
                return API_Response::fail();
            }
        }
        
        //--- save into work_order_visits table        
        $visitType = "";
        $fieldvals = array("StartTypeID"=>$startType,
                        "StartDate"=>$startDate,
                        "StartTime"=>$startTime,
                        "EndDate"=>$endDate,
                        "EndTime"=>$endTime,
                        "EstimatedDuration"=>$duration,
                        "TechArrivalInstructions"=>$techArvInsts,
                        "Company_ID"=>$Company_ID,
                        "WO_ID"=>$WO_ID,
                        "WOVisitOrder"=>$order);
                        
        if(!$this->existBaseWOVisit($winNum)) // insert
        {            
            $fieldvals['WIN_NUM'] = $winNum;
            $fieldvals['WOVisitOrder'] = 1;
            $fieldvals['Type'] = self::VISIT_TYPE_BASE;            
            $result = Core_Database::insert("work_order_visits",$fieldvals);  
        }
        else
        {              
            $fieldvals['WOVisitOrder'] = 1;      
            $criteria = "WIN_NUM='$winNum' AND Type=1 ";  
            $result = Core_Database::update("work_order_visits",$fieldvals,$criteria);            
        }
        
        //--- save StartDate,StartTime,EndDate,EndTime,Duration into work_orders table
        $wofieldvals = array('StartDate'=>$startDate,
                        'StartTime'=>$startTime,
                        'EndDate'=>$endDate,
                        'EndTime'=>$endTime,
                        'Duration'=>$duration
        );
        $this->updateWoInfo($winNum,$wofieldvals);
        
    }
    //--- 13458  
    public function validWOVisitCheckInOutForClient($woVisitID,$winNum,$CORCheckInDate,$CORCheckInTime,$CORCheckOutDate,$CORCheckOutTime,$optionCheckInOut,$type,$disable)
    {




        $validFieldvals = array("CORCheckInDate"=>$CORCheckInDate,
                        "CORCheckInTime"=>$CORCheckInTime,
                        "CORCheckOutDate"=>$CORCheckOutDate,
                        "CORCheckOutTime"=>$CORCheckOutTime,
                        "OptionCheckInOut"=>$optionCheckInOut
                        );            
                                    
        $errors = Core_Api_Error::getInstance();
        $coreValid = new Core_Validation_WoVisit($validFieldvals);
        if($disable==0)
        {
            if ($coreValid->validate() === false) {
                $errors->addErrors('6', $coreValid->getErrors());
                return API_Response::fail();
            }
        }
        return true;
    }  
  
    public function saveWOVisitCheckInOutForClient($woVisitID,$winNum,$CORCheckInDate,$CORCheckInTime,$CORCheckOutDate,$CORCheckOutTime,$optionCheckInOut,$type=3,$disable=0, $Section = NULL)
    {
        if(empty($winNum)) return;
        //if(empty($woVisitID)) return; 13561
        //--- validate
        $validFieldvals = array("CORCheckInDate"=>$CORCheckInDate,
                        "CORCheckInTime"=>$CORCheckInTime,
                        "CORCheckOutDate"=>$CORCheckOutDate,
                        "CORCheckOutTime"=>$CORCheckOutTime,
                        "OptionCheckInOut"=>$optionCheckInOut
                        );            
                                    
        $errors = Core_Api_Error::getInstance();
        $coreValid = new Core_Validation_WoVisit($validFieldvals);
        if($disable==0)
        {
            if ($coreValid->validate() === false) {
                $errors->addErrors('6', $coreValid->getErrors());
                return API_Response::fail();
            }
        }
                        
        //--- update
        $fieldvals = array("CORCheckInDate"=>$CORCheckInDate,
                        "CORCheckInTime"=>$CORCheckInTime,
                        "CORCheckOutDate"=>$CORCheckOutDate,
                        "CORCheckOutTime"=>$CORCheckOutTime,
                        "OptionCheckInOut"=>$optionCheckInOut,
                        "VisitDisable"=>$disable
                        );  
        if(empty($woVisitID))
        {
        	if ($Section == "6") {
	            $order = $this->getMaxWOVisitOrder($winNum) + 1;
	            $fieldvals["WIN_NUM"] = $winNum;
	            $fieldvals["Type"] = $type;
	            $fieldvals["WOVisitOrder"] = $order;
	            $fieldvals["StartTypeID"] = 1;            
		        $this->saveWOVisit_arrayPara($woVisitID,$fieldvals);
        	}
        }
        else {
	        $this->saveWOVisit_arrayPara($woVisitID,$fieldvals);
        } 
        
        //13881
        if(!empty($woVisitID))
        {
            $curVisit = $this->getWOVisitInfo($woVisitID);
            $type = $curVisit["Type"];
            if($type==self::VISIT_TYPE_BASE && $optionCheckInOut==2 && empty($CORCheckInDate) && empty($CORCheckInTime) && empty($CORCheckOutDate) && empty($CORCheckOutTime)){
                //update check in, check out of WO: CheckedIn, Date_In, Time_In, Date_Out, Time_Out  
                $criteria = "WIN_NUM='$winNum' ";  
                $fieldvals = array('CheckedIn'=>0);
                $result = Core_Database::update(Core_Database::TABLE_WORK_ORDERS,
                        $fieldvals,$criteria);
            }
        }
        //end 13881
        
        //13965
        if(!empty($woVisitID))
        {
            $curVisit = $this->getWOVisitInfo($woVisitID);
            $type = $curVisit["Type"];
            if($type==self::VISIT_TYPE_BASE && $optionCheckInOut==2 && !empty($CORCheckInDate) && !empty($CORCheckInTime) ){
                //update check in of WO: CheckedIn  
                $criteria = "WIN_NUM='$winNum' ";  
                $fieldvals = array('CheckedIn'=>1);
                $result = Core_Database::update(Core_Database::TABLE_WORK_ORDERS,
                        $fieldvals,$criteria);
            }
        }        
        //end 13965
        
    }
    //--- 13458
    public function saveWOVisitCheckInOutForTech($woVisitID,$winNum,$techCheckInDate,$techCheckInTime,$techCheckOutDate,$techCheckOutTime,$requiredTechDateTime=0)
    {
        if(empty($winNum)) return;
        //--- validate
        $validFieldvals = array("TechCheckInDate"=>$techCheckInDate,
                        "TechCheckInTime"=>$techCheckInTime,
                        "TechCheckOutDate"=>$techCheckOutDate,
                        "TechCheckOutTime"=>$techCheckOutTime
                        );            

                                    
        $errors = Core_Api_Error::getInstance();

        $validType = 0;
        if($requiredTechDateTime==1) $validType=Core_Validation_Abstract::TYPE_TECH;
        $coreValid = new Core_Validation_WoVisit($validFieldvals,$validType);
        
        if ($coreValid->validate() === false) {
			if (!(!empty ($techCheckInDate) && !empty ($techCheckInTime)) || $requiredTechDateTime==1) {
        		$validate_errors = $coreValid->getErrors();

        		foreach ($validate_errors as $error) {
        			$errors->addError('6', $error);
        		}
        		return API_Response::fail();
			}
        }

                        
        //--- update
        if(!empty($woVisitID))
        {
            $fieldvals = array("TechCheckInDate"=>$techCheckInDate,
                        "TechCheckInTime"=>$techCheckInTime,
                        "TechCheckOutDate"=>$techCheckOutDate,
                        "TechCheckOutTime"=>$techCheckOutTime
                        );            
            $this->saveWOVisit_arrayPara($woVisitID,$fieldvals); 
            
            //--- save to word_orders table
            $visitInfo = $this->getWOVisitInfo($woVisitID);
            $visitType = $visitInfo['Type'];
            if($visitType==self::VISIT_TYPE_BASE)
            {
                //Date_In,Date_Out,Time_In,Time_Out 
                $wofieldvals = array('Date_In'=>$techCheckInDate,'Time_In'=>$techCheckInTime,
                                'Date_Out'=>$techCheckOutDate,'Time_Out'=>$techCheckOutTime
                );
                $this->updateWoInfo($winNum,$wofieldvals,0);
            }
            //echo($woVisitID);die();
        }
        else //insert
        {            
            $this->createWOVisitIfEmpty($winNum,0);            
            $maxWOVisitOrder = $this->getMaxWOVisitOrder($winNum);   
            $WOVisitOrder = $maxWOVisitOrder + 1;
            
            $fieldvals = array("TechCheckInDate"=>$techCheckInDate,"TechCheckInTime"=>$techCheckInTime,
                        "TechCheckOutDate"=>$techCheckOutDate,"TechCheckOutTime"=>$techCheckOutTime,
                        "WIN_NUM"=>$winNum,
                        "WOVisitOrder"=>$WOVisitOrder,
                        "StartTypeID"=>1,
                        "Type"=>self::VISIT_TYPE_TECH
                        );                        
            $this->saveWOVisit_arrayPara($woVisitID,$fieldvals);             
            
        }
    }
    //--- 13458, 13969
    public function validWOVisit($woVisitID,$winNum,$startType,$startDate,$startTime,$endDate,$endTime,$duration,$techArvInsts,$projectID=0)
    {
        $fieldvals = array("StartTypeID"=>$startType,
                        "StartDate"=>$startDate,
                        "StartTime"=>$startTime,
                        "EndDate"=>$endDate,
                        "EndTime"=>$endTime,
                        "EstimatedDuration"=>$duration,
                        "TechArrivalInstructions"=>$techArvInsts);
        //13969
        $ProjectClass = new Core_Api_ProjectClass();
        
        if(!empty($projectID)) {
            $projectInfo = $ProjectClass->getProject($projectID);
            $fieldvals['RequiredStartDateTime'] = $projectInfo['RequiredStartDateTime'];
            $fieldvals['RequiredEndDateTime'] = $projectInfo['RequiredEndDateTime'];
        }
        //end 13969
        //--- validate
        $errors = Core_Api_Error::getInstance();
        $coreValid = new Core_Validation_WoVisit($fieldvals);
        if ($coreValid->validate() === false) {
        	$errors->addErrors('6', $coreValid->getErrors());
            return API_Response::fail();
        }   
        else
        {
        	return "";
        }     
    }
    //--- 13458
    private function convertDuration($duration)
    {
        //3.5 hrs, 2.5, 4.0, 43 hrs, 1 hr, 4, 10 hrs, 1 hour, NULL, empty
        $ret = "";
        if(is_numeric($duration)) $ret=$duration;
        else
        {
            $split = split(' ',$duration);
            if(is_numeric($split[0])) $ret=$split[0];
        }
        return $ret;
    }
    //--- 13458
    public function getDurationDesc($num,$sort=false)
    {        
        if(empty($num)) return '';
        $num = trim($num);
        if (!is_numeric ($num)) return $num; 
        
        $hour = floor($num);
        $decMinute = $num-$hour;
        $minute = floor($decMinute*60);
        
        $ret = "";
        if($hour==1) $ret = "1 hour";
        //14054
        else if($hour==0) $ret = "0 hours";
        //End 14054
        else if($hour > 1) $ret = "$hour hours";
        
        $txtmins = 'minutes';
        if($sort) $txtmins = 'min';
        $txtmin = 'minute';
        if($sort) $txtmin = 'min';
        
        if($minute==1) $ret.=" 1 $txtmin";
        else if($minute > 1) $ret.=" $minute $txtmins";
                
        return $ret;
    }
    //--- 13458
    public function getEstDurationArray()
    {
        $ret = array();
        $num = 0.25;
        while($num <= 12)
        {
            $ret[] = array('value'=>$num,'desc'=>$this->getDurationDesc($num));            
            $num += 0.25;
        }
        return $ret;        
    }
    //--- 13458
    public function getNumOfVisits($winNum)
    {
        $db = Core_Database::getInstance();
        $query = $db->select();

		if (is_array ($winNum)) {
			$results = array();

			if (!empty ($winNum)) {
				//foreach ($winNum as $win) {
				//	$this->createWOVisitIfEmpty($win, 0);
				//}

				$query->distinct();
				$query->from("work_order_visits",array(
					"WIN_NUM" => "WIN_NUM",
					"num" => "(SELECT COUNT(*) FROM work_order_visits v WHERE v.WIN_NUM = work_order_visits.WIN_NUM AND v.VisitDisable = 0)"
				));
				$query->where("WIN_NUM IN (" . implode (",", array_unique ($winNum)) . ")", $winNum);
				$query->where("VisitDisable = 0");
				$records = Core_Database::fetchAll($query);
error_log (var_export ($records, true));

				foreach ($winNum as $win) {
					$results[$win] = 1;
				}
				if (!empty ($records)) {
					foreach ($records as $record) {
						$results[$record["WIN_NUM"]] = $record["num"];
					}
				}
			}

			return $results;
		}
		else {
			$this->createWOVisitIfEmpty($winNum,0);

			$query->from('work_order_visits',array('num'=>'(count(*))'));
			$query->where("WIN_NUM = ?", $winNum);
			$query->where("VisitDisable = 0");
			$records = Core_Database::fetchAll($query);

			if(empty($records)) return 1;
			else return $records[0]['num'];        
		}
    }
    
    public function getWOVisitCount($winNum)
    {
        $this->createWOVisitIfEmpty($winNum,0);
        
        $db = Core_Database::getInstance();
        $query = $db->select();
        $query->from('work_orders', array('Qty_Visits'));
        $query->where("WIN_NUM = '$winNum'");
        $records = Core_Database::fetchAll($query);

        if(empty($records)) return 0;

        $count = $records[0]['Qty_Visits'];

        return $count ? $count : 1;        
    }
    
    //--- 13458
    public function synNumVisits($winNum)
    {
        //--- wo info
        $wo = $this->getWoInfo($winNum,array('Amount_Per','Qty_Devices'));
        if(empty($wo)) return;
        $amountPer = $wo['Amount_Per'];
                
        //--- update 'Qty_Devices'  field
        if($amountPer=='Visit')
        {
            $numOfVisits = $this->getNumOfVisits($winNum);            
            $wofieldvals = array('Qty_Devices'=>$numOfVisits);
        }     
        return;
    }
    
	public function syncNumVisits($winNum)
    {
        //--- wo info
        $wo = $this->getWoInfo($winNum,array('Amount_Per','Qty_Devices'));
        if(empty($wo)) return;
        $amountPer = $wo['Amount_Per'];
                
        //--- update 'Qty_Devices'  field
       // if($amountPer=='Visit')
       // {
            $numOfVisits = $this->getNumOfVisits($winNum);            
            $wofieldvals = array('Qty_Visits'=>$numOfVisits);
       // }                                    
         return $this->updateWoInfo($winNum, $wofieldvals);    
       
    }
    //--- 13458
    public function getWoInfo($winNum,$fields)
    {
        $db = Core_Database::getInstance();
        $query = $db->select();
        $query->from(Core_Database::TABLE_WORK_ORDERS,$fields);     
        $query->where("WIN_NUM = '$winNum'");
        $records = Core_Database::fetchAll($query);
        if(empty($records)) return;
        else return $records[0];        
    }
    //--- 13458, 13763
    public function updateWoInfo($winNum,$wofieldvals,
            $needSendEmail=1,$needSendCheckedInEmail=1,$needSendCheckedOutEmail=1)
    {
        $apiclass = new Core_Api_Class();
        $wo = $apiclass->getWorkOrdersWithWinNum($winNum, '', 'WIN_NUM', '');
        $wo = $wo[0];

        $wocriteria = "WIN_NUM='$winNum'";
        $result = Core_Database::update(Core_Database::TABLE_WORK_ORDERS,$wofieldvals,$wocriteria);
        //send email 13763
        if($needSendEmail==1)
        {
            if(isset($wofieldvals['WorkOrderReviewed'])){
                if($wofieldvals['WorkOrderReviewed']==1 && $wo['WorkOrderReviewed']==0)
                {
                    $emailCore = new Core_EmailForTechAcceptedWO();
                    $emailCore->sendMail($winNum);
                }
            }
            if(isset($wofieldvals['TechCheckedIn_24hrs'])){
                if($wofieldvals['TechCheckedIn_24hrs']==1 && $wo['TechCheckedIn_24hrs']==0) 
                {
                    $emailCore = new Core_EmailForTechConfirmedWO();
                    $emailCore->sendMail($winNum);
                }
            }   
            if($needSendCheckedInEmail==1)
            {     
                if(($wofieldvals['CheckedIn']==1 && $wo['CheckedIn']==0) ||
                   (!empty($wofieldvals['Date_In']) && !empty($wofieldvals['Time_In']) && empty($wo['Date_In']) && empty($wo['Time_In'])) 
                )
                {
                    $emailCore = new Core_EmailForTechCheckedIN();
                    $emailCore->sendMail($winNum);
                }
            }        
            
            if($needSendCheckedOutEmail==1)
            {            
                if(!empty($wofieldvals['Date_Out']) && !empty($wofieldvals['Time_Out']) && empty($wo['Date_Out']) && empty($wo['Time_Out'])
                )
                {
                    $emailCore = new Core_EmailForTechCheckedOUT();
                    $emailCore->sendMail($winNum);
                }
            }
        }
        return $result;
    }
    //--- 13458
    public function displayTotalHours($dateIn, $timeIn, $dateOut, $timeOut)
    {
        if(empty($dateIn) || empty($dateOut)) return '';//13555
        $dateIn = date("Y-m-d", strtotime($dateIn));
        $dateOut = date("Y-m-d", strtotime($dateOut));
        $inStamp  = strtotime(trim($dateIn).' '.$timeIn);
        $outStamp = strtotime(trim($dateOut).' '.$timeOut);
        $total = $outStamp - $inStamp;
        if ($total) {
            $ost = $total%3600;
            $result = (!$ost) ? ($total/3600).' hours' : (($total-$ost)/3600).' hours '.round($ost/60).' min';;
            return $result;
        }
        return '';
    }

    public function getTotalHours($dateIn, $timeIn, $dateOut, $timeOut)
    {
        if(empty($dateIn) || empty($dateOut)) return null;//13555
        $dateIn = date("Y-m-d", strtotime($dateIn));
        $dateOut = date("Y-m-d", strtotime($dateOut));
        $inStamp  = strtotime(trim($dateIn).' '.$timeIn);
        $outStamp = strtotime(trim($dateOut).' '.$timeOut);
        $total = $outStamp - $inStamp;        
        if ($total) {
            $hours = $total/3600;
            return $hours;
        }
        else 
        {
            return null;
        }
    }

    public function getTotalWorkHours($dateIn, $timeIn, $dateOut, $timeOut)
    {
        $dateIn = date("Y-m-d", strtotime($dateIn));
        $dateOut = date("Y-m-d", strtotime($dateOut));
        $inStamp  = strtotime(trim($dateIn).' '.$timeIn);
        $inStamp_5PM  = strtotime(trim($dateIn).' 5:00:00 PM');
        $outStamp = strtotime(trim($dateOut).' '.$timeOut);
        $outStamp_8AM = strtotime(trim($dateOut).' 8:00:00 AM');
        
        $days = floor(abs($outStamp - $inStamp)/(60*60*24));
        
        //echo("<br/>inStamp: $inStamp, ".date("Y-m-d g:i:s",$inStamp));
        //echo("<br/>outStamp: $outStamp, ".date("Y-m-d g:i:s",$outStamp));
        //echo("<br/>days: $days");
        $total = $outStamp - $inStamp;        
        $hours = 0;
        if($days==0)
        {
            $hours = ($outStamp-$inStamp)/3600;
        }
        else if($days >=1)
        {
            //--- work-hours in the first day
            $dw = date( "w", $inStamp);
            if($dw!=0 && $dw!=6)
            {
                
                if($inStamp <= $inStamp_5PM) 
                {
                    $hours += ($inStamp_5PM-$inStamp)/3600;     
                }
            }
            //--- work-hours in the end day
            $ew = date( "w", $outStamp);
            if($ew!=0 && $ew!=6)
            {
                if($outStamp > $outStamp_8AM)
                {
                    $hours += ($outStamp - $outStamp_8AM)/3600;  
                }                
            }
            //--- work-hours in the other days
            if($days >=2)
            {
                $in_Plus1 = date("Y-m-d",$inStamp+3600*24);
                $out_Minus1 = date("Y-m-d",$outStamp-3600*24);
                $availHours = $this->getAvailHours($in_Plus1,$out_Minus1);
                $hours += $availHours;
            }            
        }
        return $hours;
    }
    //--- 13458
    public function getTotatDurationOnSite($winNum)
    {
		if (is_array ($winNum)) {
			$result = array ();
			$visits = $this->GetAllWOVisitList ($winNum);

			foreach ($winNum as $win) {
				$total = 0;

				foreach ($visits[$win] as $visit) {
					$techTotalHours = $visit['TechTotalHours'];
					$CORTotalHours = $visit['CORTotalHours'];
					$OptionCheckInOut = $visit['OptionCheckInOut'];

					if ($CORTotalHours != null && $CORTotalHours > 0)
						$visitHours = $CORTotalHours;
					else
						$visitHours = $techTotalHours;

					$total += $visitHours;
				}

				$result[$win] = $total;
			}
		}
		else {
			if(empty($winNum)) return null;
			$woVisits = $this->GetAllWOVisitList($winNum);
			if(empty($woVisits)) return null;
			$total = 0;
			foreach($woVisits as $visit)
			{
				$techTotalHours = $visit['TechTotalHours'];
				$CORTotalHours = $visit['CORTotalHours'];
				$OptionCheckInOut = $visit['OptionCheckInOut'];
				//if($OptionCheckInOut==1) $visitHours = $techTotalHours;
				//else if($OptionCheckInOut==2) $visitHours = $CORTotalHours;
				//else $visitHours=0;
				if ($CORTotalHours != null && $CORTotalHours > 0)
					$visitHours = $CORTotalHours;
				else
					$visitHours = $techTotalHours;
				
				$total += $visitHours;
			}
			return $total;
		}
    }
    //--- 13458
    public function getTotatDurationOnSiteDesc($winNum)
    {
        $total = $this->getTotatDurationOnSite($winNum);
        $totalDesc = $this->getDurationDesc($total);
        return $totalDesc;
    }
    //--- 13480, 13634
    public function OverwriteWOERDataFromProjectERData($winNum,$projectId,$companyID='')
    {
        $mapping_array = array(
    'Project_Manager' => 'ProjectManagerName',      
    'Project_Manager_Email' => 'ProjectManagerEmail',
    'Project_Manager_Phone' => 'ProjectManagerPhone',
    'Resource_Coordinator' => 'ResourceCoordinatorName',
    'Resource_Coord_Email' => 'ResourceCoordinatorEmail',
    'Resource_Coord_Phone' => 'ResourceCoordinatorPhone',
    'Qty_Devices'=>'Qty_Devices',
    'Amount_Per'=>'Amount_Per',
    'Default_Amount'=>'PayMax',
    'Description'=>'Description',
    'Requirements'=>'Requirements',
    'SpecialInstructions'=>'SpecialInstructions',
    'AutoBlastOnPublish'=>'AutoBlastOnPublish',
    'RecruitmentFromEmail'=>'RecruitmentFromEmail',
    'WO_Category_ID'=>'WO_Category_ID',
    'PO'=>'PO',
    'General1'=>'General1',
    'General2'=>'General2',
    'General3'=>'General3',
    'General4'=>'General4',
    'General5'=>'General5',
    'General6'=>'General6',
    'General7'=>'General7',
    'General8'=>'General8',
    'General9'=>'General9',
    'General10'=>'General10',
    'Headline'=>'Headline',
    'CheckInOutEmail'=>'CheckInOutEmail',
    'CheckInOutName'=>'CheckInOutName',
    'CheckInOutNumber'=>'CheckInOutNumber',
    'EmergencyName'=>'EmergencyName',
    'EmergencyPhone'=>'EmergencyPhone',
    'EmergencyEmail'=>'EmergencyEmail',
    'TechnicalSupportName'=>'TechnicalSupportName',
    'TechnicalSupportPhone'=>'TechnicalSupportPhone',
    'TechnicalSupportEmail'=>'TechnicalSupportEmail',
    'Reminder24Hr'=>'Reminder24Hr',
    'Reminder1Hr'=>'Reminder1Hr',          
    'ReminderAcceptance'=>'ReminderAcceptance',
    'ReminderNotMarkComplete'=>'ReminderNotMarkComplete',
    'P2TPreferredOnly'=>'P2TPreferredOnly',
    'isWorkOrdersFirstBidder'=>'isWorkOrdersFirstBidder',
    'MaximumAllowableDistance'=>'MaximumAllowableDistance',
    'MinimumSelfRating'=>'MinimumSelfRating',
    'ReminderIncomplete'=>'ReminderIncomplete',
    'CheckOutCall'=>'CheckOutCall',
    'CheckInCall'=>'CheckInCall',
    'SignOffSheet_Required'=>'SignOffSheet_Required',
    'P2TNotificationEmail'=>'P2TNotificationEmailTo',
    'PcntDeduct'=>'PcntDeduct',
    'MinutesRemainPublished'=>'MinutesRemainPublished',
    'StartDate'=>'StartDate',
    'StartTime'=>'StartTime',
    'EndDate'=>'EndDate',
    'EndTime'=>'EndTime',
    'Duration'=>'Duration',
    'PartManagerUpdate_Required'=>'PartManagerUpdate_Required',
    'isProjectAutoAssign'=>'isProjectAutoAssign',
    'PcntDeductPercent'=>'PcntDeductPercent', 
//    'CustomSignOff_Enabled' => 'CustomSignOff_Enabled',
    'Type_ID' => 'Type_ID',
    'Qty_Visits' => 'Qty_Visits'
);
        if(empty($projectId)) return;
        
        $projectApi = new Core_Api_ProjectClass();
        $projectInfo = $projectApi->getProjectById($projectId);
        if(empty($projectInfo)) return;
        
        //--- get bcat data from project
        $prjBCatsRecords = $projectApi->getProjectBCats_insertFromClientIfNotExists($projectId);
        if(empty($prjBCatsRecords)) return;
        
        //--- enable_expense_reporting from project
        $enable = $projectApi->enableExpenseReporting($projectId);            

        //--- update enable_expense_reporting of WO
        $fieldvals = array("enable_expense_reporting"=>$enable);
        $updCriteria = "WINNUM = '$winNum'";        
        $result = Core_Database::update("work_orders_additional_fields",$fieldvals,$updCriteria);
        
        //--- delete old bcat data of WO
        $criteria = "winid='$winNum'";
        $db = Core_Database::getInstance();
        $result = $db->delete('work_orders_bcats',$criteria);
        
        //--- insert bcat data from project into WO
        foreach($prjBCatsRecords as $rec)            
        {
            $fieldvals = array("winid"=>$winNum,
                "bcatname"=>$rec["bcatname"],
                "bcatdesc"=>$rec["bcatdesc"],
                "ischecked"=>$rec["ischecked"],
                "reimbursable_thru_fs"=>$rec["reimbursable_thru_fs"],
                "require_doc_upl"=>$rec["require_doc_upl"],
                "bcattype"=>$rec["bcattype"]);   
            
            $bcatname = $rec["bcatname"];
            if($this->exists_WinBCatName($winNum,$bcatname)==0)
            {        
                $result = Core_Database::insert("work_orders_bcats",$fieldvals);
            }
        }            
        
        if($companyID=='FLS')
        {
            //--- tag data            
            //$apiWoTag = new Core_Api_WoTagClass();
            //$apiWoTag->saveWOTags_fromProjectTags($winNum,$projectId);            
            //--- wo fields
            $woUpdateFields = array();
            foreach($mapping_array as $prjfield=>$wofield)
            {
                $woUpdateFields[$wofield] = $projectInfo[$prjfield];
            }

            $criteria = "WIN_NUM = '$winNum' ";

            $errors = Core_Api_Error::getInstance();
            $result = Core_Database::update(Core_Database::TABLE_WORK_ORDERS,
                    $woUpdateFields,$criteria) ;  
            $err = $errors->getErrors();
        }   
        return;
    }    

    /*
    public function UpdateCalculatedTechHrs ($wo) {
		$api_wos = new Core_Api_WosClass();
		$visits = $api_wos->GetWOVisitList ($wo->WIN_NUM);

		foreach ($visits as $visit) {
			$start_type = $visit["StartTypeID"] == 1 ? "Firm Start Time" : "Start Time Window";
			$start_time = $this->displayDate ('n/j/Y', $visit["StartDate"]) . "-" . $visit["StartTime"];
			$end_time = $this->displayDate ('n/j/Y', $visit["EndDate"]) . "-" . $visit["EndTime"];
			$duration = $api_wos->getDurationDesc ($visit["EstimatedDuration"]);
		}
    }
    //*/
    //--- 13484
    public function doesTechHaveMatchedCOMPUCertForWO($winNum,$techId)
    {
        //--- $certsWO
        $wcs = new Core_WorkOrderCertifications();
        $certsWO = $wcs->getWorkOrderCertification($winNum);
        $certsWO = !empty($certsWO[$winNum]) ? array_values($certsWO[$winNum]) : array();
                
        if(empty($certsWO) || count($certsWO)==0) return false;
                
        //---  $certsTech
        $db = Core_Database::getInstance();
        $select = $db->select();
        $select->from(Core_Database::TABLE_TECH_CERTIFICATION, array('certification_id'));
        $select->where("TechID='$techId'");
        $records = Core_Database::fetchAll($select);

        if(empty($records)) return false;
        $certsTech = array();
        foreach($records as $rec)
        {
            $certsTech[]=$rec['certification_id'];
        }

        //--- if empty
        if(empty($certsWO) || count($certsWO)==0) return false;
        if(empty($certsTech) || count($certsTech)==0) return false;
        if(!in_array(33,$certsWO) && !in_array(34,$certsWO) && !in_array(35,$certsWO))
        {
            return false;
        }
        if(!in_array(33,$certsTech) && !in_array(34,$certsTech) && !in_array(35,$certsTech))
        {
            return false;
        }        
        //--- COMPUCerts
        if(in_array(33,$certsWO))
        {
            if(in_array(33,$certsTech) || in_array(34,$certsTech) || in_array(35,$certsTech))
            {
                return true;
            }
        }
        else if(in_array(34,$certsWO))
        {
            if(in_array(34,$certsTech) || in_array(35,$certsTech))
            {
                return true;
            }
        }
        else if(in_array(35,$certsWO))
        {
            if(in_array(35,$certsTech))
            {
                return true;
            }
        }        
    }
    //--- 13190
    /* isNew    = 0: Returned or Old
                = 1: New
    */
    public function getPartInfo($winNum,$isNew)
    {       
		$is_multiple_wos = true;

		if (!is_array ($winNum)) {
			$winNum = array ($winNum);
			$is_multiple_wos = false;
		}

        $db = Core_Database::getInstance();
        $select = $db->select();
        $select->from(array('pe'=>'part_entries'), array());
        $select->join(array('p'=>'parts'),
                'p.PartEntryID = pe.id',
                array('*')
        );
        $select->where("pe.WinNum IN (?)", $winNum);
        $select->where("p.IsNew = '$isNew'");

        $records = Core_Database::fetchAll($select);

		if ($is_multiple_wos) {
			$wos = array ();

			foreach ($records as $rec) {
				$win = $rec["WinNum"];
				if (!isset ($wos[$win])) $wos[$win] = $rec;
			}

			return $wos;
		}
		else {
			if(empty($records)) return null;
			else return $records[0];        
		}
    }
    //--- 13190
    public function hasTechUpdateParts($winNum)
    {
        $db = Core_Database::getInstance();
        $select = $db->select();
        $select->from('part_entries');
        $select->where("WinNum = '$winNum'");
        $select->where("TechIdLastUpd >0");
        $records = Core_Database::fetchAll($select);
        if(empty($records)) 0;
        else return 1;        
    }
    //--- 13525
    public function getNumOfParts($winNum)
    {       
        $db = Core_Database::getInstance();
        $select = $db->select();
        $select->from('part_entries', array('numofparts'=>'(Count(*))'));
        $select->where("WinNum = '$winNum'");

        $records = Core_Database::fetchAll($select);
        if(empty($records)) return 0;
        else return $records[0]['numofparts'];        
    }
    //--- 13525
    public function getPartEntries($winNum)
    {
		$is_multiple_wos = true;

		if (!is_array ($winNum)) {
			$winNum = array ($winNum);
			$is_multiple_wos = false;
		}

        $db = Core_Database::getInstance();
        $select = $db->select();
        $select->from('part_entries');
        $select->where("WinNum IN (?)", $winNum);
        $select->order("id ASC");
        $records = Core_Database::fetchAll($select);

		if ($is_multiple_wos) {
			$wos = array ();

			foreach ($winNum as $win) {
				$wos[$win] = array ();
			}

			foreach ($records as $rec) {
				$win = $rec["WinNum"];
				$wos[$win][] = $rec;
			}

			return $wos;
		}
		else {
			if(empty($records)) return null;
			else return $records;        
		}
    }
    
    //--- 13525
    public function getPartsInfo($winNum,$isNew)
    {
		$is_multiple_wos = true;

		if (!is_array ($winNum)) {
			$winNum = array ($winNum);
			$is_multiple_wos = false;
		}

        $db = Core_Database::getInstance();
        $select = $db->select();
        $select->from(array('pe'=>'part_entries'), array("WinNum"));
        $select->join(array('p'=>'parts'),
                'p.PartEntryID = pe.id',
                array('*')
        );
        $select->where("pe.WinNum IN (?)", $winNum);
        $select->where("p.IsNew = '$isNew'");
        $select->order("p.PartEntryID ASC");

        $records = Core_Database::fetchAll($select);

		if ($is_multiple_wos) {
			$wos = array ();

			foreach ($records as $rec) {
				$win = $rec["WinNum"];
				if (!isset ($wos[$win])) $wos[$win] = array ();
				$wos[$win][] = $rec;
			}

			return $wos;
		}
		else {
			if(empty($records)) return null;
			else return $records;        
		}
    }


    public function getBundleForWorkOrders($wins)
    {
	// returns the bundle name common amount array of wins, false if none found
	if (!is_array($wins)) return false;
         $db = Core_Database::getInstance();
         $select = $db->select();
         $select->from("work_orders", array("Route"));
	$select->distinct();
         $select->where("WIN_NUM IN (?)", $wins);
         $records = Core_Database::fetchAll($select);  
	if (count($records) == 1)
		$bundle = $records[0]["Route"];
	return $bundle;
    }

    public function setBundleForWorkOrders($name, $wins)
    {
	// returns the bundle name common amount array of wins, false if none found
	if (!is_array($wins)) return false;
	$db = Core_Database::getInstance();
	$fieldvals = array(
		"Route" => $name
	);
	$criteria = $db->quoteInto("WIN_NUM IN (?)", $wins);
        $result = Core_Database::update(Core_Database::TABLE_WORK_ORDERS,$fieldvals,$criteria);
    }
    
    //--- 13555
    public function updateBaseVisit_CheckInForTech($winNum,$techCheckInDate,$techCheckInTime)
    {
        //--- update work_order_visits
        $fieldvals=array('TechCheckInDate'=>$techCheckInDate,
                            'TechCheckInTime'=>$techCheckInTime
        );
        $criteria = "WIN_NUM='$winNum' AND Type=1 ";  
        $result = Core_Database::update("work_order_visits",$fieldvals,$criteria);     
        //--- upadte work_orders
        $fieldvals_1=array('Date_In'=>$techCheckInDate,
                            'Time_In'=>$techCheckInTime
        );
        $criteria_1 = "WIN_NUM='$winNum'";  
        $result_1 = Core_Database::update("work_orders",$fieldvals_1,$criteria_1); 
        
            
    }
    
    public function refreshQtyVisits($winNum)
    {
        $numOfVisits = $this->getNumOfVisits($winNum);
        $fieldvals=array('Qty_Visits'=>$numOfVisits);
        $criteria = "WIN_NUM='$winNum'";  
        $result = Core_Database::update("work_orders",$fieldvals,$criteria); 
        return $result;        
    }

    //--- 13592
    public function getWinIDs_ForToday($companyID)
    {
        $today = date("Y-m-d");
        $db = Core_Database::getInstance();
        $query = $db->select();
        $query->from(array('wi'=>'work_order_visits'),array('WIN_NUM'));
        $query->join(array('wo'=>Core_Database::TABLE_WORK_ORDERS),
                'wo.WIN_NUM = wi.WIN_NUM',
                array()
        );
        $query->where("wo.Company_ID = '$companyID'");
        $query->where("wo.Status != 'created'");
        $query->where("wo.Status != 'deactivated'");
        $query->where("wi.StartDate = '$today'");
        $records = Core_Database::fetchAll($query);
        
        if(empty($records)) return '';
        else {
            $winIDs = '';
            foreach($records as $rec)
            {
                $winIDs .= empty($winIDs)? $rec['WIN_NUM']:','.$rec['WIN_NUM'];
            }
            return $winIDs;
        }        
    }
    
    public function countWinIDs_ForToday($companyID)
    {
        $today = date("Y-m-d");
        $db = Core_Database::getInstance();
        $query = $db->select();
        $query->from(array('wi'=>'work_order_visits'),
                array('numWins'=>'COUNT(*)')
            );
        $query->join(array('wo'=>Core_Database::TABLE_WORK_ORDERS),
                'wo.WIN_NUM = wi.WIN_NUM',
                array()
        );
        $query->where("wo.Company_ID = '$companyID'");
        $query->where("wo.Status != 'created'");
        $query->where("wo.Status != 'deactivated'");
        $query->where("wi.StartDate = '$today'");
        $records = Core_Database::fetchAll($query);
        
        if(empty($records)) return 0;
        else return $records['0']['numWins'];

    }
    //--- 13620
    public function addTechQuestion($winNum,$techUsername,$techName,$techID,$message)
    {
		$db = Core_Database::getInstance();
    	$created_date = date("Y-m-d H:i:s");

    	$fieldvals = array("Workorder_ID"=>$winNum,
                        "Created_Date"=>$created_date,
                        "Created_By"=>$techUsername,
                        "Type"=>self::WOQA_TYPE_QUESTION,
                        "Tech_ID"=>$techID,
                        "Tech_Name"=>$techName,
                        "Message"=>$message);   
        $result = Core_Database::insert("wo_qa",$fieldvals);    

        if (!empty ($result)) {
        	$type = Core_Api_NoticeClass::WOStatusID_FOR_UNASSIGNED_QA;

			$query = $db->select();
        	$query->from(array(Core_Database::TABLE_WORK_ORDERS), array("Status"));
			$query->where("WIN_NUM = '$winNum'");
			$records = Core_Database::fetchAll($query);

			if (!empty ($records) && $records[0]["Status"] != "published") {
				$type = Core_Api_NoticeClass::WOStatusID_FOR_ASSIGNED_QA;
			}

			$notices = new Core_Api_NoticeClass ();
        	$notices->createWOChangeStatusLog($winNum, $type, $created_date, $techID, $techName);
        }
    }
    //--- 13620
    public function addClientAnswer($winNum,$clientUsername,$clientName,$clientID,$companyID,$questionID,$message)
    {
        $fieldvals = array("Workorder_ID"=>$winNum,
                        "Created_Date"=>date("Y-m-d H:i:s"),
                        "Created_By"=>$clientUsername,
                        "Type"=>self::WOQA_TYPE_ANSWER,
                        "Client_ID"=>$clientID,
                        "Client_Name"=>$clientName,
                        "Company_ID"=>$companyID,
                        "Question_ID"=>$questionID,
                        "Message"=>$message);   
        $result = Core_Database::insert("wo_qa",$fieldvals);
		$wm = new Core_WorkMarket($winNum);
		$wm->pushQuestion($message);
    }
    //--- 13620
    public function getQAList_ForTech($winNum,$techID)
    {
        $db = Core_Database::getInstance();
        $query = $db->select();
        $query->from('wo_qa');
        $query->where("Workorder_ID = '$winNum'");
        $query->where("Tech_ID = '$techID'");
        $query->order("Created_Date ASC");                
        $records = Core_Database::fetchAll($query);        
        return $records;        
    }

    public function getAnswerList_ForQuestion($questionID,$winNum)
    {
        $db = Core_Database::getInstance();
        $query = $db->select();
        $query->from('wo_qa');
        $query->where("Workorder_ID = '$winNum'");
        $query->where("Question_ID = '$questionID'");
        $query->where("Type = ".self::WOQA_TYPE_ANSWER);    
        $query->order("Created_Date ASC");                
        $records = Core_Database::fetchAll($query);        
        return $records;        
    }
    
    //--- 13620, 13646
    public function getQuestionContactLog($winNum,
            $forClientBox=0,
            $clientID=0,
            $sortDateDescending=0,
            $forTechID=0)
    {
        $resultArray = array();
        
        //--- published
        if($forClientBox!=1)
        {
        $Notes = $this->getNotices_WOS_Published($winNum);
        if(!empty($Notes) && count($Notes)>0){
            $resultArray = array_merge($resultArray,$Notes);
        }
        }    
        //--- assigned
        if($forClientBox!=1)
        {
        $Notes = $this->getNotices_Assigned($winNum);
        if(!empty($Notes) && count($Notes)>0){
            $resultArray = array_merge($resultArray,$Notes);
        }
        }
        //--- marked complete by tech
        if($forClientBox!=1)
        {

        $Notes = $this->getNotices_WOS_MarkedCompleteByTech($winNum);
        if(!empty($Notes) && count($Notes)>0){
            $resultArray = array_merge($resultArray,$Notes);
        }
        }     

        //--- marked complete
        if($forClientBox!=1)
        {            
        $Notes = $this->getNotices_WOS_MarkedComplete($winNum);
        if(!empty($Notes) && count($Notes)>0){
            $resultArray = array_merge($resultArray,$Notes);
        }
        }
        //--- techCheckInOutVisit
        if($forClientBox!=1)
        {            
            $Notes = $this->getNotices_TechCheckInOutVisit($winNum);
        if(!empty($Notes) && count($Notes)>0){
            $resultArray = array_merge($resultArray,$Notes);
        }
        }
        //--- marked incomplete
        if($forClientBox!=1)
        {                    
        $Notes = $this->getNotices_WOS_MarkedIncomplete($winNum);
        if(!empty($Notes) && count($Notes)>0){
            $resultArray = array_merge($resultArray,$Notes);
        }
        }
        //--- approved
        if($forClientBox!=1)
        {        
        $Notes = $this->getNotices_WOS_Approved($winNum);
        if(!empty($Notes) && count($Notes)>0){
            $resultArray = array_merge($resultArray,$Notes);
        }
        }
        //--- pay
        if($forClientBox!=1)
        {
        $Notes = $this->getNotices_WOS_Pay($winNum);
        if(!empty($Notes) && count($Notes)>0){
            $resultArray = array_merge($resultArray,$Notes);
        }
        }
        //--- blast
        if($forClientBox!=1)
        {        
            $Notes = $this->getNotices_WOS_AvailWorkEmailBlast($winNum);
            if(!empty($Notes) && count($Notes)>0){
                $resultArray = array_merge($resultArray,$Notes);
            }
        }
        //--- contactlog phone
        if($forClientBox!=1)
        {            
        $Notes = $this->getContactLog_Phone($winNum);
        if(!empty($Notes) && count($Notes)>0){
            $resultArray = array_merge($resultArray,$Notes);
        }
        }
        //--- contactlog email
        if($forClientBox!=1)
        {        
        $Notes = $this->getContactLog_Email($winNum);
        if(!empty($Notes) && count($Notes)>0){
            $resultArray = array_merge($resultArray,$Notes);
        }
        }
        //--- bid
        $Notes = $this->getNotices_Bids($winNum,$forTechID);
        if(!empty($Notes) && count($Notes)>0){
            $resultArray = array_merge($resultArray,$Notes);
        }
        //--- QAMessageNotificationSentToClient
        if($forClientBox!=1)
        {        
        $Notes = $this->getNotices_WOS_QAMessageNotificationSentToClient($winNum);
        if(!empty($Notes) && count($Notes)>0){
            $resultArray = array_merge($resultArray,$Notes);
        }
        }
        //--- QAMessageNotificationSentToTech
        if($forClientBox!=1)
        {        
        $Notes = $this->getNotices_WOS_QAMessageNotificationSentToTech($winNum);
        if(!empty($Notes) && count($Notes)>0){
            $resultArray = array_merge($resultArray,$Notes);
        }
        }
        //--- Questions               
        $Notes = $this->getQuestions($winNum,$forTechID);
        if(!empty($Notes) && count($Notes)>0){
            $resultArray = array_merge($resultArray,$Notes);
        }
        //--- ClientMessages
        $Notes = $this->getClientMessages($winNum,$clientID,$forTechID);
        if(!empty($Notes) && count($Notes)>0){
            $resultArray = array_merge($resultArray,$Notes);
        }
        //--- PreStart_1HR_ReminderCall                
        $Notes = $this->getNotices_PreStart_1HR_ReminderCall($winNum);
        if(!empty($Notes) && count($Notes)>0){
            $resultArray = array_merge($resultArray,$Notes);
        }
        //--- PreStart_24HR_ReminderCall
        $Notes = $this->getNotices_PreStart_24HR_ReminderCall($winNum);
        if(!empty($Notes) && count($Notes)>0){
            $resultArray = array_merge($resultArray,$Notes);
        }

        //--- Email-WorkOrder Assign
        if($forClientBox!=1)
        {   
        $Notes = $this->getEmailNotices_WorkOrderAssignedEmail($winNum);
        if(!empty($Notes) && count($Notes)>0){
            $resultArray = array_merge($resultArray,$Notes);
        }
        }        
        //--- Email-WorkOrderIncompleteReminderEmailToTech
        if($forClientBox!=1)
        {   
            $Notes = $this->getEmailNotices_WorkOrderIncompleteReminderEmail($winNum);
            if(!empty($Notes) && count($Notes)>0){
                $resultArray = array_merge($resultArray,$Notes);
            }
        }
        //--- sort
        foreach ($resultArray as $key => $row) {
            $timestamp[$key]  = $row['timestamp'];
        }        
        if($sortDateDescending==0)
        {
        array_multisort($timestamp, SORT_ASC, $resultArray);
        }
        else
        {
            array_multisort($timestamp, SORT_DESC, $resultArray);            
        }
                           
        return $resultArray;
        
    }
    
    //--- 13646
    public function getQuestionContactLog_forTech($winNum,$techID)
    {
        $resultArray = array();
        
        $Notes = $this->getNotices_WOS_Published($winNum);
        if(!empty($Notes) && count($Notes)>0){
            $resultArray = array_merge($resultArray,$Notes);
        }
        $Notes = $this->getNotices_Assigned($winNum);
        if(!empty($Notes) && count($Notes)>0){
            $resultArray = array_merge($resultArray,$Notes);
        }
        
        $Notes = $this->getNotices_WOS_MarkedCompleteByTech($winNum);
        if(!empty($Notes) && count($Notes)>0){
            $resultArray = array_merge($resultArray,$Notes);
        }
        
        $Notes = $this->getNotices_WOS_MarkedComplete($winNum);
        if(!empty($Notes) && count($Notes)>0){
            $resultArray = array_merge($resultArray,$Notes);
        }
                
        $Notes = $this->getNotices_TechCheckInOutVisit($winNum);
        if(!empty($Notes) && count($Notes)>0){
            $resultArray = array_merge($resultArray,$Notes);
        }
                
        $Notes = $this->getNotices_WOS_MarkedIncomplete($winNum);
        if(!empty($Notes) && count($Notes)>0){
            $resultArray = array_merge($resultArray,$Notes);
        }

        $Notes = $this->getNotices_WOS_Approved($winNum);
        if(!empty($Notes) && count($Notes)>0){
            $resultArray = array_merge($resultArray,$Notes);
        }
        
        $Notes = $this->getNotices_WOS_Pay($winNum);
        if(!empty($Notes) && count($Notes)>0){
            $resultArray = array_merge($resultArray,$Notes);
        }
        
        $Notes = $this->getNotices_WOS_AvailWorkEmailBlast($winNum);
        if(!empty($Notes) && count($Notes)>0){
            $resultArray = array_merge($resultArray,$Notes);
        }
        
        $Notes = $this->getContactLog_Phone($winNum);
        if(!empty($Notes) && count($Notes)>0){
            $resultArray = array_merge($resultArray,$Notes);
        }

        $Notes = $this->getContactLog_Email($winNum);
        if(!empty($Notes) && count($Notes)>0){
            $resultArray = array_merge($resultArray,$Notes);
        }
    
        $Notes = $this->getNotices_Bids($winNum);
        if(!empty($Notes) && count($Notes)>0){
            $resultArray = array_merge($resultArray,$Notes);
        }
                
        
        $Notes = $this->getQuestions($winNum);
        if(!empty($Notes) && count($Notes)>0){
            $resultArray = array_merge($resultArray,$Notes);
        }
        
        $Notes = $this->getClientMessages_forTech($winNum,$techID);
        if(!empty($Notes) && count($Notes)>0){
            $resultArray = array_merge($resultArray,$Notes);
        }
                
        $Notes = $this->getNotices_PreStart_1HR_ReminderCall($winNum);
        if(!empty($Notes) && count($Notes)>0){
            $resultArray = array_merge($resultArray,$Notes);
        }

        $Notes = $this->getNotices_PreStart_24HR_ReminderCall($winNum);
        if(!empty($Notes) && count($Notes)>0){
            $resultArray = array_merge($resultArray,$Notes);
        }

        $Notes = $this->getNotices_PreStart_Custom_ReminderCall($winNum);
        if(!empty($Notes) && count($Notes)>0){
            $resultArray = array_merge($resultArray,$Notes);
        }
        
        $Notes = $this->getNotices_WOS_QAMessageNotificationSentToTech($winNum);
        if(!empty($Notes) && count($Notes)>0){
            $resultArray = array_merge($resultArray,$Notes);
        }
        
        $Notes = $this->getEmailNotices_WorkOrderAssignedEmail($winNum);
        if(!empty($Notes) && count($Notes)>0){
            $resultArray = array_merge($resultArray,$Notes);
        }

        $Notes = $this->getEmailNotices_WorkOrderIncompleteReminderEmail($winNum);
        if(!empty($Notes) && count($Notes)>0){
            $resultArray = array_merge($resultArray,$Notes);
        }
        
        
        //--- sort
        foreach ($resultArray as $key => $row) {
            $timestamp[$key]  = $row['timestamp'];
        }        
        array_multisort($timestamp, SORT_ASC, $resultArray);
                           
        return $resultArray;
        
    }
    
    //--- 13620
    public function getQuestions($winNum,$forTechID=0)
    {
        $db = Core_Database::getInstance();
        $query = $db->select();
        $query->from('wo_qa');
        $query->where("Workorder_ID = '$winNum'");
        if(!empty($forTechID))
        {
            $query->where("Tech_ID = '$forTechID'");
        }
        $query->where("Type = ".self::WOQA_TYPE_QUESTION);
        $query->order("Created_Date ASC");                
        $records = Core_Database::fetchAll($query);     
        
        if(empty($records) || count($records)==0) return null;
        
        $resultArray = array();
        foreach($records as $rec)
        {
             $techID = $rec['Tech_ID'];
             $techName = $rec['Tech_Name'];
             $Created_Date = $rec['Created_Date'];
             $Message = $rec['Message'];     
             $QuestionID = $rec['id'];             
                          
             $resultArray[]=array('message'=>"$techName - $Message",
                    'message_type'=>"TECH_QUESTION",
                    'timestamp'=>$Created_Date, 
                    'data'=>array('QuestionID'=>$QuestionID,
                            'Message'=>$Message,
                            'TechID'=>$techID,
                            'TechName'=>$techName
                    )
             );
         }         
         return $resultArray;
    }
           
    public function getNum_ofUnseen_QA_fromTech($clientID,$winNum)
    {
        $db = Core_Database::getInstance();

		if (is_array ($winNum)) {
			$results = array();

			if (!empty ($winNum)) {
				$win_nums = implode (",", $winNum);
				$type = self::WOQA_TYPE_QUESTION;
				$query = "SELECT DISTINCT qa2.WorkOrder_ID, (" .
					"	SELECT COUNT(*) FROM wo_qa qa " .
					"	LEFT OUTER JOIN wo_qa_lastview lv ON qa.WorkOrder_ID = lv.WorkOrder_ID " .
					"	WHERE qa.Type = 1 AND qa.WorkOrder_ID = qa2.WorkOrder_ID " .
					"		AND (ISNULL (lv.last_view_date) OR qa2.Created_Date > lv.last_view_date)" .
					") AS num " .
					"FROM wo_qa qa2 " .
					"WHERE qa2.Type = $type AND qa2.WorkOrder_ID IN ($win_nums)";
				$records = $db->query ($query);

				foreach ($winNum as $win) {
					$results[$win] = 0;
				}
				foreach ($records as $record) {
					$results[$record->Workorder_ID] = 1;
				}
			}

			return $results;
		}
		else {
			$lastView = $this->getClient_LastViewQAWinDatetime($clientID,$winNum);
                
			$query = $db->select();
			$query->from('wo_qa',array('num'=>'Count(*)'));
			$query->where("Workorder_ID = '$winNum'");
			$query->where("Type = ".self::WOQA_TYPE_QUESTION);
			if(!empty($lastView))        
			{
				$query->where("Created_Date > '$lastView'");            
			}
			$records = Core_Database::fetchAll($query);     
        
			if(empty($records) || count($records)==0) return 0;
			else return $records[0]['num'];
		}
    }

    //--- 13620
    public function getContactLog_Phone($winNum)
    {
         $db = Core_Database::getInstance();
         $query = $db->select();
         $query->from('WO_Contact_Log');
         $query->where("WorkOrder_ID = '$winNum'");
         $query->where("LOWER(Type) = 'phone'");         
         $query->order("Created_Date ASC");                
         $records = Core_Database::fetchAll($query);                
         if(empty($records) || count($records)==0) return null;
         
         $resultArray = array();
         foreach($records as $rec)
         {
             $Contact = $rec['Contact'];
             $Created_Date = $rec['Created_Date'];             
             $resultArray[]=array('message'=>"Call Made to $Contact",
                    'message_type'=>"CALL_MADE_TO_TECH",
                    'timestamp'=>$Created_Date, 
                    'data'=>array('Contact'=>$Contact)
             );
         }         
         return $resultArray;
    }
    
    public function getContactLog_Email($winNum)
    {
         $db = Core_Database::getInstance();
         $query = $db->select();
         $query->from('WO_Contact_Log');
         $query->where("WorkOrder_ID = '$winNum'");
         $query->where("LOWER(Type) = 'email'");         
         $query->order("Created_Date ASC");                
         $records = Core_Database::fetchAll($query);                
         if(empty($records) || count($records)==0) return null;
         
         $resultArray = array();
         $core_tech = new Core_Api_TechClass();
         
         foreach($records as $rec)
         {
             $Contact = $rec['Contact'];
             if($Contact == 'Bid Notification Email')
             {
                 continue;
             }

             $Created_Date = $rec['Created_Date'];             
             $Subject = $rec['Subject'];  
             $Message = $rec['Message'];             
             $techInfo = $core_tech->getTechInfoByEmail($Contact);
             $techID=""; $techName="";
             if(!empty($techInfo))
             {
                 $techID = $techInfo['TechID'];
                 $techName = $techInfo['FirstName'].' '.$techInfo['LastName'];
             }
             
             $resultArray[]=array('message'=>"Email Sent to $Contact",
                    'message_type'=>"EMAIL_SENT_TO_TECH",
                    'timestamp'=>$Created_Date, 
                    'data'=>array('Subject'=>$Subject,
                            'Message'=>$Message,
                            'TechID'=>$techID,
                            'TechName'=>$techName
                    )
             );
         }         
         return $resultArray;
    }
    
    public function getNotices_Assigned($winNum)
    {
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from(array('ts'=>Core_Database::TABLE_TIMESTAMPS),
                array('WIN_NUM','DateTime_Stamp')
        );
        $select->join(array('w'=>Core_Database::TABLE_WORK_ORDERS),
                    'w.WIN_NUM=ts.WIN_NUM',
                    array('WorkOrderOwner','Project_ID','WO_ID','Tech_ID','Tech_FName','Tech_LName','Company_ID')
        );          
        $select->where("ts.Description LIKE 'Work Order Assigned%'");
        $select->where("w.WIN_NUM = '$winNum'");
        
        $select->order("ts.DateTime_Stamp DESC");
        
        $records = Core_Database::fetchAll($select); 
        if(empty($records) || count($records)==0) return null;

        foreach($records as $rec)
        {
             $Created_Date = $rec['DateTime_Stamp'];             
             $techID = $rec['Tech_ID'];      
             $techName = $rec['Tech_FName'].' '.$rec['Tech_LName'];
             $winNum = $rec['WIN_NUM']; 
             $woID = $rec['WO_ID']; 
             $company_ID = $rec['Company_ID']; 
                                 
             $resultArray[]=array('message'=>"Work Order Assigned to {TECH_NAME}",
                    'message_type'=>"WO_ASSIGNED_TO",
                    'timestamp'=>$Created_Date, 
                    'data'=>array('TechID'=>$techID,
                            'TechName'=>$techName,
                            'WinNum'=>$winNum,
                            'Company_ID'=>$company_ID,
                            'WoID'=>$woID
                    )
             );
        }         
        return $resultArray;
    }
    
    public function getNotices_WOS_Approved($winNum)
    {
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from(array('ts'=>Core_Database::TABLE_TIMESTAMPS),
                array('WIN_NUM','DateTime_Stamp')
        );
        $select->join(array('w'=>Core_Database::TABLE_WORK_ORDERS),
                    'w.WIN_NUM=ts.WIN_NUM',
                    array('WorkOrderOwner','Project_ID','WO_ID','Tech_ID','Tech_FName','Tech_LName','Company_ID')
        );          
        $select->where("ts.Description LIKE 'Work Order Approved%'");
        $select->where("w.WIN_NUM = '$winNum'");
        
        $select->order("ts.DateTime_Stamp ASC");
        
        $records = Core_Database::fetchAll($select); 
        if(empty($records) || count($records)==0) return null;

        foreach($records as $rec)
        {
             $Created_Date = $rec['DateTime_Stamp'];             
             $techID = $rec['Tech_ID'];      
             $techName = $rec['Tech_FName'].' '.$rec['Tech_LName'];
             $winNum = $rec['WIN_NUM']; 
             $woID = $rec['WO_ID']; 
             $company_ID = $rec['Company_ID']; 
                                 
             $resultArray[]=array('message'=>"Work Order Approved",
                    'message_type'=>"WO_APPROVED",
                    'timestamp'=>$Created_Date, 
                    'data'=>array('TechID'=>$techID,
                            'TechName'=>$techName,
                            'WinNum'=>$winNum,
                            'Company_ID'=>$company_ID,
                            'WoID'=>$woID
                    )
             );
        }         
        return $resultArray;
    }

    public function getNotices_WOS_Published($winNum)
    {
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from(array('ts'=>Core_Database::TABLE_TIMESTAMPS),
                array('WIN_NUM','DateTime_Stamp')
        );
        $select->join(array('w'=>Core_Database::TABLE_WORK_ORDERS),
                    'w.WIN_NUM=ts.WIN_NUM',
                    array('WorkOrderOwner','Project_ID','WO_ID','Tech_ID','Tech_FName','Tech_LName','Company_ID')
        );          
        $select->where("ts.Description LIKE 'Work Order Published%'");
        $select->where("w.WIN_NUM = '$winNum'");
        
        $select->order("ts.DateTime_Stamp ASC");
        
        $records = Core_Database::fetchAll($select); 
        if(empty($records) || count($records)==0) return null;

        foreach($records as $rec)
        {
             $Created_Date = $rec['DateTime_Stamp'];             
             $techID = $rec['Tech_ID'];      
             $techName = $rec['Tech_FName'].' '.$rec['Tech_LName'];
             $winNum = $rec['WIN_NUM']; 
             $woID = $rec['WO_ID']; 
             $company_ID = $rec['Company_ID']; 
                                 
             $resultArray[]=array('message'=>"Work Order Published",
                    'message_type'=>"WO_PUBLISHED",
                    'timestamp'=>$Created_Date, 
                    'data'=>array('TechID'=>$techID,
                            'TechName'=>$techName,
                            'WinNum'=>$winNum,
                            'Company_ID'=>$company_ID,
                            'WoID'=>$woID
                    )
             );
        }         
        return $resultArray;
    }
    //--- 13620
    public function getNotices_Bids($winNum,$forTechID=0)
    {        
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from(array('b'=>Core_Database::TABLE_WORK_ORDER_BIDS),
                array('WorkOrderID','TechID','BidAmount','Comments','Bid_Date')
        );
        $select->join(array('w'=>Core_Database::TABLE_WORK_ORDERS),
                    'w.WIN_NUM=b.WorkOrderID',
                    array('WorkOrderOwner','Project_ID','WO_ID','Company_ID','Amount_Per')
        );          
        $select->join(array('t'=>Core_Database::TABLE_TECH_BANK_INFO),
                    't.TechID=b.TechID',
                    array('FirstName','LastName')
        );
         
        $select->where("w.WIN_NUM = '$winNum'");
        if(!empty($forTechID))
        {
            $select->where("b.TechID = '$forTechID'");            
        }
        
        $records = Core_Database::fetchAll($select);  
        if(empty($records) || count($records)==0) return null;
        
        $resultArray = array();
        
        foreach($records as $rec)
        {
             $Created_Date = $rec['Bid_Date'];             
             $techName = $rec['FirstName'].' '.$rec['LastName'];
             
             $message = '{TECH_NAME} (ID:{TECH_ID}) bid ${BID_AMOUNT} per {AMOUNT_PER} {COMMENT}';                    
             
             $resultArray[]=array('message'=>$message,
                    'message_type'=>"TECH_BID",
                    'timestamp'=>$Created_Date, 
                    'data'=>array('TechID'=>$rec['TechID'],
                            'TechName'=>$techName,
                            'BidAmount'=>$rec['BidAmount'],
                            'Amount_Per'=>$rec['Amount_Per'],
                            'Comments'=>$rec['Comments']
                    )
             );
        }         
        return $resultArray;
    }
    
    //--- 13620
    public function getNotices_WOS_MarkedIncomplete($winNum)
    {
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from(array('ts'=>Core_Database::TABLE_TIMESTAMPS),
                array('WIN_NUM','DateTime_Stamp')
        );
        $select->join(array('w'=>Core_Database::TABLE_WORK_ORDERS),
                    'w.WIN_NUM=ts.WIN_NUM',
                    array('WorkOrderOwner','Project_ID','WO_ID','Tech_ID','Tech_FName','Tech_LName','Company_ID')
        );          
        $select->where("ts.Description LIKE 'Work Order Marked Incomplete%'");
        $select->where("w.WIN_NUM = '$winNum'");
        
        $select->order("ts.DateTime_Stamp ASC");
        
        $records = Core_Database::fetchAll($select); 
        if(empty($records) || count($records)==0) return null;

        foreach($records as $rec)
        {
             $Created_Date = $rec['DateTime_Stamp'];             
             $techID = $rec['Tech_ID'];      
             $techName = $rec['Tech_FName'].' '.$rec['Tech_LName'];
             $winNum = $rec['WIN_NUM']; 
             $woID = $rec['WO_ID']; 
             $company_ID = $rec['Company_ID']; 
                                 
             $resultArray[]=array('message'=>"Work Order Marked Incomplete",
                    'message_type'=>"WO_MARKED_INCOMPLETE",
                    'timestamp'=>$Created_Date, 
                    'data'=>array('TechID'=>$techID,
                            'TechName'=>$techName,
                            'WinNum'=>$winNum,
                            'Company_ID'=>$company_ID,
                            'WoID'=>$woID
                    )
             );
        }         
        return $resultArray;
    }
    //--- 13620
    public function getNotices_WOS_MarkedCompleteByTech($winNum)
    {
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from(array('ts'=>Core_Database::TABLE_TIMESTAMPS),
                array('WIN_NUM','DateTime_Stamp','Tech_Name')
        );
        $select->join(array('w'=>Core_Database::TABLE_WORK_ORDERS),
                    'w.WIN_NUM=ts.WIN_NUM',
                    array('WorkOrderOwner','Project_ID','WO_ID','Tech_ID','Tech_FName','Tech_LName','Company_ID')
        );          
        $select->where("ts.Description = 'Work Order Marked: Completed'");
        $select->where("w.WIN_NUM = '$winNum'");
        $select->where("Tech_Name IS NOT NULL AND TRIM(Tech_Name)<>''");
        
        $select->order("ts.DateTime_Stamp ASC");
        
        $records = Core_Database::fetchAll($select); 
        if(empty($records) || count($records)==0) return null;

        foreach($records as $rec)
        {
             $Created_Date = $rec['DateTime_Stamp'];             

             $techID = $rec['Tech_ID'];      
             $techName = $rec['Tech_FName'].' '.$rec['Tech_LName'];
             if(empty($rec['Tech_Name']))
             {
                 $techID = 0;
                 $techName = '';
             }
             
             $winNum = $rec['WIN_NUM']; 
             $woID = $rec['WO_ID']; 
             $company_ID = $rec['Company_ID']; 
                                 
             $resultArray[]=array('message'=>"Work Order Marked Complete by {TECH_NAME}",
                    'message_type'=>"WO_MARKED_COMPLETE_BY_TECH",
                    'timestamp'=>$Created_Date, 
                    'data'=>array('TechID'=>$techID,
                            'TechName'=>$techName,
                            'WinNum'=>$winNum,
                            'Company_ID'=>$company_ID,
                            'WoID'=>$woID
                    )
             );
        }         
        return $resultArray;
    }
    
    //--- 13620
    public function getNotices_WOS_MarkedComplete($winNum)
    {
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from(array('ts'=>Core_Database::TABLE_TIMESTAMPS),
                array('WIN_NUM','DateTime_Stamp','Tech_Name')
        );
        $select->join(array('w'=>Core_Database::TABLE_WORK_ORDERS),
                    'w.WIN_NUM=ts.WIN_NUM',
                    array('WorkOrderOwner','Project_ID','WO_ID','Tech_ID','Tech_FName','Tech_LName','Company_ID')
        );          
        $select->where("ts.Description = 'Work Order Marked: Completed'");
        $select->where("w.WIN_NUM = '$winNum'");
        $select->where("Tech_Name IS NULL OR TRIM(Tech_Name)=''");
        
        $select->order("ts.DateTime_Stamp ASC");
        
        $records = Core_Database::fetchAll($select); 
        if(empty($records) || count($records)==0) return null;

        foreach($records as $rec)
        {
             $Created_Date = $rec['DateTime_Stamp'];   
             $woID = $rec['WO_ID']; 
             $company_ID = $rec['Company_ID']; 
                                 
             $resultArray[]=array('message'=>"Work Order Marked Complete",
                    'message_type'=>"WO_MARKED_COMPLETE",
                    'timestamp'=>$Created_Date, 
                    'data'=>array('WinNum'=>$winNum,
                            'Company_ID'=>$company_ID,
                            'WoID'=>$woID
                    )
             );
        }         
        return $resultArray;
    }
    
    //--- 13620
    public function getNotices_WOS_AvailWorkEmailBlast($winNum)
    {
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from(array('ts'=>Core_Database::TABLE_TIMESTAMPS),
                array('WIN_NUM','DateTime_Stamp','Description')
        );
        $select->join(array('w'=>Core_Database::TABLE_WORK_ORDERS),
                    'w.WIN_NUM=ts.WIN_NUM',
                    array('WorkOrderOwner','Project_ID','WO_ID','Tech_ID','Tech_FName','Tech_LName','Company_ID')
        );          
        $select->where("ts.Description LIKE 'Blast:%'");
        $select->where("w.WIN_NUM = '$winNum'");        
        $select->order("ts.DateTime_Stamp ASC");
        
        $records = Core_Database::fetchAll($select); 
        if(empty($records) || count($records)==0) return null;

        foreach($records as $rec)
        {
            //Blast: WIN#: 412272 NEW Gen - 86435 - Hline (8 sent)
             $Created_Date = $rec['DateTime_Stamp'];             
             $description = $rec['Description'];             
             $pos_1 = strrpos($description, "(");
             $emailedNum = 0;
             $message = "";
             if ($pos_1 === false) {
                $emailedNum = 0;     
             }
             else
             {
                 $pos_2 = strpos($description, " sent",$pos_1);
                 if($pos_2!==false)
                 {
                     $emailedNum = substr($description,$pos_1+1,$pos_2-$pos_1);
                     $emailedNum = trim($emailedNum);
                 }
             }
             if(empty($emailedNum))
             {
                 $message = "Available Work Email Blast";
             }
             else
             {
                 $message = "Available Work Email Blast ({EMAILED_NUM} techs emailed)";
             }
                                 
             $resultArray[]=array('message'=>$message,
                    'message_type'=>"AVAIL_WO_EMAIL_BLAST",
                    'timestamp'=>$Created_Date, 
                    'data'=>array('EmailedNum'=>$emailedNum)
             );
        }         
        return $resultArray;
    }

    //--- 13646
    public function getNotices_WOS_QAMessageNotificationSentToClient($winNum)
    {
        $db = Core_Database::getInstance();  
        $select = $db->select();

        $select->from(array('ts'=>Core_Database::TABLE_TIMESTAMPS),
                array('WIN_NUM','DateTime_Stamp','Description')
        );
        $select->where("ts.Description LIKE 'Q&A Message Notification sent to Client:%'");
        $select->where("ts.WIN_NUM = '$winNum'");        
        $select->order("ts.DateTime_Stamp ASC");
        
        $records = Core_Database::fetchAll($select); 
        if(empty($records) || count($records)==0) return null;

        foreach($records as $rec)
        {
             $Created_Date = $rec['DateTime_Stamp'];                                 
             $resultArray[]=array('message'=>$rec['Description'],
                    'message_type'=>"QAMessageNotificationSentToClient",
                    'timestamp'=>$Created_Date
             );
        }         
        return $resultArray;
    }
    
    //--- 13646
    public function getNotices_WOS_QAMessageNotificationSentToTech($winNum)
    {
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from(array('ts'=>Core_Database::TABLE_TIMESTAMPS),
                array('WIN_NUM','DateTime_Stamp','Description','Tech_ID','Tech_Name')
        );
        $select->where("ts.Description LIKE 'Q&A Message Notification sent to Tech:%'");
        $select->where("ts.WIN_NUM = '$winNum'");        
        $select->order("ts.DateTime_Stamp ASC");
        
        $records = Core_Database::fetchAll($select); 
        if(empty($records) || count($records)==0) return null;

        foreach($records as $rec)
        {
             $Created_Date = $rec['DateTime_Stamp'];                                 
             $resultArray[]=array('message'=>$rec['Description'],
                    'message_type'=>"QAMessageNotificationSentToTech",
                    'timestamp'=>$Created_Date,
                    'data'=>array('TechID'=>$rec['Tech_ID'],'TechName'=>$rec['Tech_Name'])
             );
        }         
        return $resultArray;
    }
    
    //--- 13620
    public function getNotices_WOS_Pay($winNum)
    {
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from(Core_Database::TABLE_WORK_ORDERS,
                    array('Status','TechPaid','DatePaid')
        );
        $select->where("WIN_NUM = '$winNum'");
        $select->where("TechPaid = 1");
        $select->where("DatePaid is not null");        
        $select->where("Status = 'completed'");        
        $records = Core_Database::fetchAll($select); 
        if(empty($records) || count($records)==0) return null;
        
        foreach($records as $rec)
        {
             $Created_Date = $rec['DatePaid'];             
                                 
             $resultArray[]=array('message'=>"Work Order Paid",
                    'message_type'=>"WO_PAID",
                    'timestamp'=>$Created_Date, 
                    'data'=>array()
             );
        }         
        return $resultArray;
    }
    
    //--- 13620, 13646
    public function getClientMessages($winNum,$clientID=0,$forTechID=0)
    {
        $apiClass = new Core_Api_Class();
        $cliCompanyID='';
        if(!empty($clientID))
        {
            $clientInfo = $apiClass->getClientByID($clientID);
            $cliCompanyID = $clientInfo['Company_ID'];
        }        
        
        $db = Core_Database::getInstance();
        $query = $db->select();
        $query->from(array('qa'=>'wo_qa'),
            array('Tech_ID','Tech_Name','Client_Name','Client_ID','Created_Date','Message','Group_Name')
        );
        $query->join(array('cl'=>'clients'),
            'cl.ClientID=qa.Client_ID',
            array('Company_ID')
        );
        $query->where("qa.Workorder_ID = '$winNum'");
        $query->where("qa.Type = ".self::WOQA_TYPE_NORMAL_CLIENT_MESSAGE);
        if(!empty($forTechID))
        {
            $query->where("(qa.Tech_ID = '$forTechID') OR (qa.Tech_ID is null AND (qa.Group_Name = 'GPM' OR qa.Group_Name = 'InternalNote'))");        
        }
        $query->order("qa.Created_Date ASC");                
        $records = Core_Database::fetchAll($query);     
        
        if(empty($records) || count($records)==0) return null;
        
        $resultArray = array();
        foreach($records as $rec)
        {
             $techID = $rec['Tech_ID'];
             $techName = $rec['Tech_Name'];
             $clientName = $rec['Client_Name'];
             $recCompany_ID = $rec['Company_ID'];
             $Created_Date = $rec['Created_Date'];
             $Message = $rec['Message'];     
             $groupName = $rec['Group_Name'];
             
             $allowViewThisRecord = 1;             
             if($groupName==self::WOQA_GROUP_GPM)
             {
                if(empty($clientID) || $apiClass->isGPM($clientID)===false) {
                    $allowViewThisRecord=0;   
                }
             }
             else if($groupName==self::WOQA_GROUP_INTERNAL_NOTE)
             {
                 if(empty($clientID) || empty($cliCompanyID))
                 {
                     $allowViewThisRecord=0;   
                 }                    
                 else if($apiClass->isGPM($clientID)===false && $cliCompanyID != $recCompany_ID)
                 {
                     //$allowViewThisRecord=0;                        
                 }                  
             }

             if($allowViewThisRecord==0) continue;
             
             $MessageType = "";$firstMessage = "";
             if(empty($techID)) {
                 if($groupName==self::WOQA_GROUP_GPM){
                     $MessageType = "CLIENT_MESSAGE_TO_GPM";
                     $firstMessage = "{CLIENT_NAME} sent a message to GPM:";
                 }
                 else if($groupName==self::WOQA_GROUP_INTERNAL_NOTE){
                     $MessageType = "CLIENT_MESSAGE_TO_INTERNAL_NOTE";
                     $firstMessage = "{CLIENT_NAME} sent a message to INTERNAL_NOTE:";                     
                 }
                 else {                 
                 $MessageType = "CLIENT_MESSAGE_TO_ALL_TECH";
                 $firstMessage = "{CLIENT_NAME} sent a message to ALL Techs:";
             }
             }
             else {
                $MessageType="CLIENT_MESSAGE_TO_TECH";   
                $firstMessage = "{CLIENT_NAME} sent a message to {TECH_NAME} (ID {TECH_ID}):
";
             }
                                                               
             $resultArray[]=array('message'=>$firstMessage,
                    'message_type'=>$MessageType,
                    'timestamp'=>$Created_Date, 
                    'data'=>array('ClientName'=>$clientName,
                            'Message'=>$Message,
                            'TechID'=>$techID,
                            'TechName'=>$techName,
                            'GroupName'=>$groupName
                    )
             );
         }         
         return $resultArray;

           
    }

    //--- 13646
    public function getClientMessages_forTech($winNum,$techID)
    {
        $db = Core_Database::getInstance();
        $query = $db->select();
        $query->from('wo_qa',
            array('Tech_ID','Tech_Name','Client_Name','Client_ID','Created_Date','Message','Group_Name')
        );
        $query->where("Workorder_ID = '$winNum'");
        $query->where("Type = ".self::WOQA_TYPE_NORMAL_CLIENT_MESSAGE);
        $query->order("Created_Date ASC");   
        
        $records = Core_Database::fetchAll($query);     
        
        if(empty($records) || count($records)==0) return null;
        
        $resultArray = array();
        foreach($records as $rec)
        {
             $recTechID = $rec['Tech_ID'];
             $techName = $rec['Tech_Name'];
             $clientName = $rec['Client_Name'];
             $recCompany_ID = $rec['Company_ID'];
             $Created_Date = $rec['Created_Date'];
             $Message = $rec['Message'];     
             $groupName = $rec['Group_Name'];

             $allowViewThisRecord = 1;             
             if($groupName==self::WOQA_GROUP_GPM)
             {
                $allowViewThisRecord=0;   
             }
             else if($groupName==self::WOQA_GROUP_INTERNAL_NOTE)
             {
                $allowViewThisRecord=0;   
             }

             if(!empty($recTechID) && $recTechID != $techID)
             {
                 $allowViewThisRecord=0;   
             }
             
             if($allowViewThisRecord==1)
             {              
             $MessageType = "";$firstMessage = "";
                    if(empty($recTechID)) 
                    {
                 $MessageType = "CLIENT_MESSAGE_TO_ALL_TECH";
                 $firstMessage = "{CLIENT_NAME} sent a message to ALL Techs:";
             }
                    else 
                    {
                $MessageType="CLIENT_MESSAGE_TO_TECH";   
                $firstMessage = "{CLIENT_NAME} sent a message to {TECH_NAME} (ID {TECH_ID}):
";
             }
             $resultArray[]=array('message'=>$firstMessage,
                    'message_type'=>$MessageType,
                    'timestamp'=>$Created_Date, 
                    'data'=>array('ClientName'=>$clientName,
                            'Message'=>$Message,
                                'TechID'=>$recTechID,
                            'TechName'=>$techName
                    )
             );
         }         
         }//end of for         
         return $resultArray;           
    }
    //--- 13620
    public function getNotices_TechCheckInOutVisit($winNum)
    {
        //--- work-order info
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from(Core_Database::TABLE_WORK_ORDERS,
                    array('Tech_ID','Tech_FName','Tech_LName')
        );                  
        $select->where("WIN_NUM = '$winNum'");
        $records = Core_Database::fetchAll($select); 
        if(empty($records) || count($records)==0) return null;
        $wo = $records[0];
                
        
        //--- visit
        $visits = $this->GetAllWOVisitList($winNum);
        if(!empty($visits) && count($visits)>0)
        {
            $resultArray = array();
            for($i=0;$i<count($visits);$i++)
            {
                $visit = $visits[$i];
                $techCheckInDate = $visit['TechCheckInDate'];
                $techCheckInTime = $visit['TechCheckInTime'];
                $techCheckOutDate = $visit['TechCheckOutDate'];
                $techCheckOutTime = $visit['TechCheckOutTime'];  
                $visitNo = $i+1;      
                if(!empty($techCheckInDate))
                {
                    $message="{TECH_NAME} (ID: {TECH_ID}) Checked In - Visit #{VISIT_NO}";   
                    
                    $datetimestamp = strtotime($techCheckInDate.' '.$techCheckInTime);
                    $datetime = date("Y-m-d H:i:s",$datetimestamp);
                    
                    $resultArray[]=array('message'=>$message,
                    'message_type'=>'TECH_CHECK_IN_VISIT',
                    'timestamp'=>$datetime, 
                    'data'=>array('TechID'=>$wo['Tech_ID'],
                                'TechName'=>$wo['Tech_FName'].' '.$wo['Tech_LName'],
                                'VisitNo'=>$visitNo
                            )
                    );
                }
                if(!empty($techCheckOutDate))
                {
                    $message="{TECH_NAME} (ID: {TECH_ID}) Checked Out - Visit #{VISIT_NO}";   
                    
                    $datetimestamp = strtotime($techCheckOutDate.' '.$techCheckOutTime);
                    $datetime = date("Y-m-d H:i:s",$datetimestamp);

                    $resultArray[]=array('message'=>$message,
                    'message_type'=>'TECH_CHECK_OUT_VISIT',
                    'timestamp'=>$datetime, 
                    'data'=>array('TechID'=>$wo['Tech_ID'],
                                'TechName'=>$wo['Tech_FName'].' '.$wo['Tech_LName'],
                                'VisitNo'=>$visitNo
                            )
                    );
                }
            }//end of for
            return $resultArray;
        }
        else
        {
            return null;
        }
    }

    //--- 13620
    public function getNotices_PreStart_1HR_ReminderCall($winNum)
    {
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from('ivr_log');        
        $select->where("WOUNID = '$winNum'");
        $select->where("CallType LIKE '1-HR Reminder%'");
        $select->where("CallLengthSeconds >0");
        
        $records = Core_Database::fetchAll($select); 
        if(empty($records) || count($records)==0) return null;
    
        $resultArray = array();
        foreach($records as $rec)
        {
            $message = "Pre-Start 1hr-Reminder Call";
            $resultArray[]=array('message'=>$message,
                    'message_type'=>'PRE_START_1HR_REMINDER_CALL',
                    'timestamp'=>$rec['CalledTime'], 
                    'data'=>array()
                    );
        }
        return $resultArray;
    }
    
    //--- 13620
    public function getNotices_PreStart_24HR_ReminderCall($winNum)
    {
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from('ivr_log');        
        $select->where("WOUNID = '$winNum'");
        $select->where("CallType LIKE '24-HR Reminder%'");
        $select->where("CallLengthSeconds >0");
        
        $records = Core_Database::fetchAll($select); 
        if(empty($records) || count($records)==0) return null;
    
        $resultArray = array();
        foreach($records as $rec)
        {
            $message = "Pre-Start 24hr-Reminder Call";
            $resultArray[]=array('message'=>$message,
                    'message_type'=>'PRE_START_24HR_REMINDER_CALL',
                    'timestamp'=>$rec['CalledTime'], 
                    'data'=>array()
                    );
        }
        return $resultArray;
    }    
    

    //--- 13620
    public function getNotices_PreStart_Custom_ReminderCall($winNum)
    {
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from('ivr_log');        
        $select->where("WOUNID = '$winNum'");
        $select->where("CallType LIKE 'Custom HR Reminder%'");
        $select->where("CallLengthSeconds >0");
        
        $records = Core_Database::fetchAll($select); 
        if(empty($records) || count($records)==0) return null;
    
        $resultArray = array();
        foreach($records as $rec)
        {
            $message = "Pre-Start ".$rec['CallType'];
            $resultArray[]=array('message'=>$message,
                    'message_type'=>'PRE_START_CUSTOM_REMINDER_CALL',
                    'timestamp'=>$rec['CalledTime'], 
                    'data'=>array()
                    );
        }
        return $resultArray;
    }    

    //--- 13620
    public function getNotices_PreStart_Custom_ReminderCall_OLD($winNum)
    {
        //--- wo info      
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from(Core_Database::TABLE_WORK_ORDERS,
                    array('Company_ID','Project_ID')
        );                  
        $select->where("WIN_NUM = '$winNum'");
        $records = Core_Database::fetchAll($select); 
        if(empty($records) || count($records)==0) return null;
        $wo = $records[0];
        $projectID = $wo['Project_ID'];
        
        //----
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from('ProjectACSChangeLog');
        $select->where("Project_ID = '$projectID'");
        $select->where("ReminderCustomHrChecked = 1 OR ReminderCustomHrChecked_2=1 OR  ReminderCustomHrChecked_3=1");                
        $records = Core_Database::fetchAll($select); 
        if(empty($records) || count($records)==0) return null;
    
        $resultArray = array();
        foreach($records as $rec)
        {
            $customCheck = $rec['ReminderCustomHrChecked'];
            $customCheck_2 = $rec['ReminderCustomHrChecked_2'];
            $customCheck_3 = $rec['ReminderCustomHrChecked_3'];
            
            if($customCheck==1)
            {                            
                $message = "Pre-Start ".$rec['ReminderCustomHr']."hr-Reminder Call";
                $resultArray[]=array('message'=>$message,
                        'message_type'=>'PRE_START_CUSTOM_REMINDER_CALL',
                        'timestamp'=>$rec['DateChanged'], 
                        'data'=>array()
                        );
            }
            if($customCheck_2==1)
            {                            
                $message = "Pre-Start "+$rec['ReminderCustomHr_2']+"hr-Reminder Call";
                $resultArray[]=array('message'=>$message,
                        'message_type'=>'PRE_START_CUSTOM_REMINDER_CALL',
                        'timestamp'=>$rec['DateChanged'], 
                        'data'=>array()
                        );
            }
            if($customCheck_3==1)
            {                            
                $message = "Pre-Start "+$rec['ReminderCustomHr_3']+"hr-Reminder Call";
                $resultArray[]=array('message'=>$message,
                        'message_type'=>'PRE_START_CUSTOM_REMINDER_CALL',
                        'timestamp'=>$rec['DateChanged'], 
                        'data'=>array()
                        );
            }            
        }
        return $resultArray;
    }    
    
    //--- 13646
    public function getEmailNotices_WorkOrderAssignedEmail($winNum)
    {
        $imapConfig = $this->getImapConfig();
        $mail = new Zend_Mail_Storage_Imap($imapConfig);
                
        $keySearch = "Work Order Assigned:";        
        $searchArray = array('SUBJECT "'.$winNum.'"','SUBJECT "'.$keySearch.'"');
        
        $searchResult= $mail->_protocol->search($searchArray);
        $resultArray = array();
        if(!empty($searchResult) && count($searchResult) > 0)
        {
            foreach($searchResult as $messageid)
            {
                $message = $mail->getMessage($messageid);
                $subject = $message->getHeader('subject'); 
                $date = $message->getHeader('date');
                $mailDate = date("Y-m-d H:i:s",strtotime($date));        
                $text = $message->getContent();
                //--- get TechName
                $techName = "";
                $key_1 = 'Dear'; $key_2 = ',';
                $pos_1 = strpos($text, $key_1);
                if($pos_1!==false)
                {
                    $pos_2 = strpos($text,$key_2,$pos_1);
                    if($pos_2!==false)
                    {
                        $value = substr($text,$pos_1+strlen($key_1),$pos_2-$pos_1-strlen($key_1));
                    }
                }          
                $techName = trim($value);
                //--- get TechID
                $techID = "";
                $value = "";
                $key_1 = "Click this link to CONFIRM YOUR ASSIGNMENT";
                $key_2 = "&v="; $key_3="'";
                $pos_1 = strpos($text, $key_1);
                if($pos_1!==false)
                {
                    $pos_2 = strpos($text,$key_2,$pos_1);
                    if($pos_2!==false)
                    {
                        $pos_3 = strpos($text,$key_3,$pos_2);
                        if($pos_3!==false)
                        {
                            $value = substr($text,$pos_2+strlen($key_2),$pos_3-$pos_2-strlen($key_2));
                        }
                    }
                }          
                $techID = trim($value);                
                //---
                $message="Work Order Assigned Email sent to {TECH_NAME} (ID# {TECH_ID})";   
                $resultArray[]=array('message'=>$message,
                    'message_type'=>'WO_ASSIGNED_EMAIL',
                    'timestamp'=>$mailDate, 
                    'data'=>array('TechID'=>$techID,
                                'TechName'=>$techName
                            )
                    );
            }//end of for
        }//end of if
        return $resultArray;
    }
        
    //--- 13646
    public function getEmailNotices_WorkOrderIncompleteReminderEmail($winNum)
    {
        $imapConfig = $this->getImapConfig();
        $mail = new Zend_Mail_Storage_Imap($imapConfig);
                
        $keySearch = "Payment Delayed";        
        $searchArray = array('SUBJECT "'.$winNum.'"','SUBJECT "'.$keySearch.'"');
        
        $searchResult= $mail->_protocol->search($searchArray);
        $resultArray = array();
        if(!empty($searchResult) && count($searchResult) > 0)
        {
            foreach($searchResult as $messageid)
            {
                $message = $mail->getMessage($messageid);
                $subject = $message->getHeader('subject'); 
                $date = $message->getHeader('date');
                $mailDate = date("Y-m-d H:i:s",strtotime($date));  
                $deliveredTo = $message->getHeader('delivered-to');      
                $text = $message->getContent();
                //---
                $message="Work Order Incomplete Reminder Email sent to Technician ($deliveredTo)";   
                $resultArray[]=array('message'=>$message,
                    'message_type'=>'WO_INCOMPLETE_REMINDER_EMAIL',
                    'timestamp'=>$mailDate, 
                    'data'=>array()
                    );
            }//end of for
        }//end of if
        return $resultArray;
    }
    
    //--- 13646
    public function getEmailNotices_AssignNotifiSentToClient($winNum)
    {
        $imapConfig = $this->getImapConfig();
        $mail = new Zend_Mail_Storage_Imap($imapConfig);
                
        $keySearch = ") has been Assigned";        
        $searchArray = array('SUBJECT "'.$winNum.'"','SUBJECT "'.$keySearch.'"');
        
        $searchResult= $mail->_protocol->search($searchArray);
        $resultArray = array();
        if(!empty($searchResult) && count($searchResult) > 0)
        {
            foreach($searchResult as $messageid)
            {
                $message = $mail->getMessage($messageid);
                $subject = $message->getHeader('subject'); 
                $date = $message->getHeader('date');
                $mailDate = date("Y-m-d H:i:s",strtotime($date));  
                $deliveredTo = $message->getHeader('delivered-to');      
                $text = $message->getContent();
                //---
                $message="Work Order Incomplete Reminder Email sent to Technician ($deliveredTo)";   
                $resultArray[]=array('message'=>$message,
                    'message_type'=>'WO_INCOMPLETE_REMINDER_EMAIL',
                    'timestamp'=>$mailDate, 
                    'data'=>array()
                    );
            }//end of for
        }//end of if
        return $resultArray;
    }
    
    //--- 13646
    public function getImapConfig()
    {
        return array('host' =>"fieldsolutions.us",
                'user' =>"systemtest",
                'password' =>"testEmail!84");
    }
        
    

    //-- 13620
    public function saveClient_LastViewQAWinDatetime($clientID,$winNum)
    {
        if($this->ClientQALastView_Exists($clientID,$winNum)==0)
        {
            //--- insert   
            $fieldvals = array("Client_ID"=>$clientID,
                        "Workorder_ID"=>$winNum,
                        "Type"=>1,
                        "last_view_date"=>date('Y-m-d H:i:s'));
            $result = Core_Database::insert("wo_qa_lastview",$fieldvals);
        }
        else
        {
            //--- update
            $fieldvals = array("last_view_date"=>date('Y-m-d H:i:s'));   
            $criteria = "Client_ID = '$clientID' AND Workorder_ID = '$winNum' AND Type = 1";  
            $result = Core_Database::update("wo_qa_lastview",$fieldvals,$criteria);
        }         
    }
    //-- 13620
    public function getClient_LastViewQAWinDatetime($clientID,$winNum)
    {
        $db = Core_Database::getInstance();  
        $select = $db->select();
		$select->from("wo_qa_lastview");
		$select->where("Client_ID = '$clientID'");
		$select->where("Type = 1");

		if (is_array ($winNum)) {
			$select->where("Workorder_ID IN (" . implode (",", $winNum) . ")");
			$records = Core_Database::fetchAll($select);  

			$results = array();
			foreach ($winNum as $win) {
				$results[$win] = null;
			}
			foreach ($records as $record) {
				$lastViewDate = $record["last_view_date"];
				if ($lastViewDate > "1980-01-01") {
					$results[$record->Workorder_ID] = $lastViewDate;
				}
			}

			return $results;
		}
		else {
			$select->where("Workorder_ID = '$winNum'");
			$records = Core_Database::fetchAll($select);  
        
			if(empty($records) || count($records)==0) return null;
			else
			{
				$lastViewDate = $records[0]['last_view_date'];  
				if($lastViewDate < '1970-01-01') return null;      
				else return $lastViewDate;
			}
		}
    }
    //-- 13620
    public function getTech_LastViewQAWinDatetime($techID,$winNum)
    {
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from("wo_qa_lastview");
        $select->where("Tech_ID = '$techID'");
        $select->where("Workorder_ID = '$winNum'");
        $select->where("Type = 2");
        $records = Core_Database::fetchAll($select);  
        
        if(empty($records) || count($records)==0) return null;
        else
        {
            $lastViewDate = $records[0]['last_view_date'];  
            if($lastViewDate < '1970-01-01') return null;      
            else return $lastViewDate;
        }
    }
    //-- 13620
    public function saveTech_LastViewQAWinDatetime($techID,$winNum)
    {
        if($this->TechQALastView_Exists($techID,$winNum)==0)
        {
            //--- insert   
            $fieldvals = array("Tech_ID"=>$techID,
                        "Workorder_ID"=>$winNum,
                        "Type"=>2,
                        "last_view_date"=>date('Y-m-d H:i:s'));
            $result = Core_Database::insert("wo_qa_lastview",$fieldvals);
        }
        else
        {
            //--- update
            $fieldvals = array("last_view_date"=>date('Y-m-d H:i:s'));   
            $criteria = "Tech_ID = '$techID' AND Workorder_ID = '$winNum' AND Type = 2";  
            $result = Core_Database::update("wo_qa_lastview",$fieldvals,$criteria);
        }         
    }
    
    //-- 13620
    public function ClientQALastView_Exists($clientID,$winNum)
    {
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from("wo_qa_lastview");
        $select->where("Client_ID = '$clientID'");
        $select->where("Workorder_ID = '$winNum'");
        $select->where("Type = 1");
        $records = Core_Database::fetchAll($select);
        if(empty($records) || count($records)==0) return 0;
        else return 1;
    }
    //-- 13620
    public function TechQALastView_Exists($techID,$winNum)
    {
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from("wo_qa_lastview");
        $select->where("Tech_ID = '$techID'");
        $select->where("Workorder_ID = '$winNum'");
        $select->where("Type = 2");
        $records = Core_Database::fetchAll($select);
        if(empty($records) || count($records)==0) return 0;
        else return 1;
    }
    //--- 13620
    public function HasAnswer_TechNotViewYet($techID,$winNum)
    {
        $hasClientMessage_TechNotViewYet = $this->HasClientMessage_TechNotViewYet($techID,$winNum);
        
        $questionIDs = $this->getQuestionIDs_ofTech_forWin($techID,$winNum);
        if(empty($questionIDs) || count($questionIDs)==0) return $hasClientMessage_TechNotViewYet;
                
        $lastView = $this->getTech_LastViewQAWinDatetime($techID,$winNum);
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from("wo_qa");
        $select->where("Workorder_ID = '$winNum'");
        $select->where("Type = 2");
        $select->where("Question_ID IN (?)",$questionIDs);
        if(!empty($lastView))        
        {
            $select->where("Created_Date > '$lastView'");            
        }
        
        $records = Core_Database::fetchAll($select);
        
        $result = false;
        if(empty($records) || count($records)==0) $result = false;
        else $result = true;
        
        return $result || $hasClientMessage_TechNotViewYet;        
    }
    //--- 13646
    public function HasClientMessage_TechNotViewYet($techID,$winNum)
    {                
        $lastView = $this->getTech_LastViewQAWinDatetime($techID,$winNum);
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from("wo_qa");
        $select->where("Workorder_ID = '$winNum'");
        $select->where("Type = 3");
        $select->where("Tech_ID = '$techID'");
        if(!empty($lastView))        
        {
            $select->where("Created_Date > '$lastView'");            
        }
        //print_r($select->__toString());
        $records = Core_Database::fetchAll($select);
        
        if(empty($records) || count($records)==0) return false;
        else return true;
        
    }
    
    //--- 13620
    public function HasQuestion_ClientNotViewYet ($clientID, $winNum)
    {
        $db = Core_Database::getInstance();

		if (is_array ($winNum)) {
			$results = array();

			if (!empty ($winNum)) {
				$win_nums = implode (",", $winNum);
				$query = "SELECT * FROM (" .
					"	SELECT qa.*, ( " .
					"		SELECT lv.last_view_date FROM wo_qa_lastview lv " .
					"		WHERE lv.Type = 1 AND lv.WorkOrder_ID = qa.WorkOrder_ID " .
					"	) AS last_view_date FROM wo_qa qa " .
					"	WHERE qa.Type = 1 AND qa.WorkOrder_ID IN ($win_nums) " .
					") as qa2 " .
					"WHERE ISNULL (qa2.last_view_date) OR qa2.Created_Date > qa2.last_view_date";
				$records = $db->query ($query);

				foreach ($winNum as $win) {
					$results[$win] = 0;
				}
				foreach ($records as $record) {
					$results[$record->Workorder_ID] = 1;
				}
			}

			return $results;
		}
		else {
			$lastView = $this->getClient_LastViewQAWinDatetime($clientID,$winNum);

			$select = $db->select();
			$select->from("wo_qa");
			$select->where("Workorder_ID = ?", $winNum);
			$select->where("Type = 1");
			if(!empty($lastView))        
			{
				$select->where("Created_Date > '$lastView'");            
			}
			$records = Core_Database::fetchAll($select);
        
			if(empty($records) || count($records)==0) return 0;
			else return 1;
		}
    }

    //--- 13620
    public function getQuestionIDs_ofTech_forWin($techID,$winNum)
    {
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from("wo_qa");
        $select->where("Tech_ID = '$techID'");
        $select->where("Workorder_ID = '$winNum'");
        $select->where("Type = 1");
        
        $records = Core_Database::fetchAll($select);
        
        if(empty($records) || count($records)==0) return null;        
        
        $questionIDs = array();
        foreach($records as $rec)
        {
            $questionIDs[]=$rec['id'];
        }
        return $questionIDs;
    }    
    //--- 13646
    public function getClientMessageIDs_ofTech_forWin($techID,$winNum)
    {
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from("wo_qa");
        $select->where("Tech_ID = '$techID'");
        $select->where("Workorder_ID = '$winNum'");
        $select->where("Type = 3");
        
        $records = Core_Database::fetchAll($select);
        
        if(empty($records) || count($records)==0) return null;        
        
        $questionIDs = array();
        foreach($records as $rec)
        {
            $questionIDs[]=$rec['id'];
        }
        return $questionIDs;
    }    
    //--- 13620
    /*
    * $toTechID = 0: means send to all-techs
    */
    public function addClientQAMessage($winNum,$clientUsername,$clientName,$clientID,$companyID,$message,$toTechID,$toTechName,$groupName='')
    {
        $fieldvals = array("Workorder_ID"=>$winNum,
                        "Created_Date"=>date("Y-m-d H:i:s"),
                        "Created_By"=>$clientUsername,
                        "Type"=>self::WOQA_TYPE_NORMAL_CLIENT_MESSAGE,
                        "Client_ID"=>$clientID,
                        "Client_Name"=>$clientName,
                        "Company_ID"=>$companyID,
                        "Tech_ID"=>$toTechID,
                        "Tech_Name"=>$toTechName,
                        "Message"=>$message);   
        if(!empty($groupName))                
        {
            $fieldvals['Group_Name']=$groupName;
        }
        $result = Core_Database::insert("wo_qa",$fieldvals);    
    }

    //--- 13620
    public function getTechIDList_HaveBidOrQuestion($winNum)
    {
        //Format: ID# - Fname Lname
        $techIDs = array();
        //--- have question
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from("wo_qa",array('Tech_ID'));
        $select->where("Workorder_ID = '$winNum'");
        $select->where("Type = 1");        
        $select->group("Tech_ID");
        $records = Core_Database::fetchAll($select);
        if(!empty($records) && count($records)>0)
        {
            foreach($records as $rec)
            {
                $techIDs[]=$rec['Tech_ID'];
            }            
        }
        //--- have bid
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from(Core_Database::TABLE_WORK_ORDER_BIDS,array('TechID'));
        $select->where("WorkOrderID = '$winNum'");
        $select->group("TechID");
        $records = Core_Database::fetchAll($select);
        if(!empty($records) && count($records)>0)
        {
            foreach($records as $rec)
            {
                $techIDs[]=$rec['TechID'];
            }            
        }
        if(count($techIDs)>0)
        {
            $db = Core_Database::getInstance();  
            $select = $db->select();
            $select->from(Core_Database::TABLE_TECH_BANK_INFO,array('TechID','FirstName','LastName','PrimaryEmail'));
            $select->where("TechID IN (?)",$techIDs);
            $records = Core_Database::fetchAll($select);  
            return $records;          
        }
        else
        {
            return null;
        }
    }
    
    //---- 13620
    public function getLastBidInfo($winNum,$techID)
    {
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from(Core_Database::TABLE_WORK_ORDER_BIDS);
        $select->where("WorkOrderID = '$winNum'");
        $select->where("TechID = '$techID'");
        
        $select->order("Bid_Date DESC");
        $records = Core_Database::fetchAll($select);
        if(!empty($records) && count($records)>0) return $records[0];
        else return null;
    }

 	public function getRegionBundleByCriteria($filters){
    	if(empty($filters))return false;
    	$db = Core_Database::getInstance();
    	
    	$select = $db->select();
    	$select->from(Core_Database::TABLE_WORK_ORDERS, array("Route as Bundle","Region"));
    	$select->where("Company_ID = ?", $filters->Company_ID);
    	
    	if(!empty($filters->Project_ID))
    		$select->where("Project_ID IN (?)",$filters->Project_ID);
    	
    	if(!empty($filters->StartDateFrom))
    		$select->where("StartDate >= ?", $filters->StartDateFrom);
    		
    	if(!empty($filters->StartDateTo))
    		$select->where("StartDate <= ?", $filters->StartDateTo);
    	
    	if(!empty($filters->WO_State))
    		$select->where("Status = ?",$filters->WO_State);
    	
    	$result = Core_Database::fetchAll($select);
    	
    	return $result;
    }
    //--- 13676
    public function getNumOfWOs_InSiteOfProject($siteNumber,$projectID)
    {
        $db = Core_Database::getInstance();
        $select = $db->select();
        $select->from(Core_Database::TABLE_WORK_ORDERS,
            array("NUM"=>"(Count(*))")
        );
        $select->where("SiteNumber='$siteNumber'");
        $select->where("Project_ID='$projectID'");
        $records = Core_Database::fetchAll($select);  
        
        if(empty($records) || count($records)==0) return 0;
        return $records[0]['NUM'];                                      
    }
    //--- 13676, 13857
    public function getWinNumArray_InSiteOfProject($siteNumber,$projectID)
    {
        //13857
        $projectIDs = "";
        $prjID0 = "";
        if(is_array($projectID) && count($projectID) > 0){
            $prjID0 = $projectID[0];
            foreach($projectID as $prjID){
                $projectIDs.= $projectIDs == ""? $prjID : ','.$prjID;
            }
        }else{
            $prjID0 = $projectID;
            $projectIDs = $projectID;
        }
        if(empty($projectIDs)) return null;
        $projectClass = new Core_Api_ProjectClass();
        $prjID0_info = $projectClass->getProject($prjID0);
        $companyID = $prjID0_info['Project_Company_ID'];        
        
        //end 13857        

        
        $db = Core_Database::getInstance();
        $select = $db->select();
        $select->from(Core_Database::TABLE_WORK_ORDERS,
            array('WIN_NUM')
        );
        $select->where("SiteNumber='$siteNumber'");
        //13857
        $select->where("Company_ID = '$companyID'");
        $select->where("Project_ID IN ($projectIDs)");
        
        $select->order("WIN_NUM DESC");
        $records = Core_Database::fetchAll($select);  
        if(empty($records) || count($records)==0)  {
            return null;
        }  else {
            $winArray = array();
            foreach($records as $rec) $winArray[]=$rec['WIN_NUM'];
            
            return $winArray;
        }
    }

    
   //--- 13676, 13754, 13852, 13857
    public function getWinNumArray_OfProject($projectID,
                $startDateGreater='',$startDateLess='',
                $techMarkedCompleteDateGreater='',$techMarkedCompleteDateLess='',
                $approvedDateGreater='',$approvedDateLess='',
                $invoicedDateGreater='',$invoicedDateLess=''
         )
    {
        //13857
        $projectIDs = "";
        $prjID0 = "";
        if(is_array($projectID) && count($projectID) > 0){
            $prjID0 = $projectID[0];
            foreach($projectID as $prjID){
                $projectIDs.= $projectIDs == ""? $prjID : ','.$prjID;
            }
        }else{
            $prjID0 = $projectID;
            $projectIDs = $projectID;
        }
        if(empty($projectIDs)) return null;
        $projectClass = new Core_Api_ProjectClass();
        $prjID0_info = $projectClass->getProject($prjID0);
        $companyID = $prjID0_info['Project_Company_ID'];        
        //end 13857        

        $db = Core_Database::getInstance();

        $select = $db->select();
        $select->from(array('w'=>Core_Database::TABLE_WORK_ORDERS),array('WIN_NUM'));
        if(!empty($techMarkedCompleteDateGreater) || !empty($techMarkedCompleteDateLess))
        {
            $select->joinLeft(array('ts'=>Core_Database::TABLE_TIMESTAMPS),
                "w.WIN_NUM = ts.WIN_NUM",
                array('DateTime_Stamp','Description')
            );
        }
        $select->where("w.Company_ID = '$companyID'");
        $select->where("w.Project_ID IN ($projectIDs)");
        if(!empty($startDateGreater))
        {
            $select->where("w.StartDate  >= '$startDateGreater'");
        }
        if(!empty($startDateLess))
        {
            $select->where("w.StartDate  <= '$startDateLess'");
        }        
        if(!empty($approvedDateGreater))
        {
            $select->where("w.DateApproved >= '$approvedDateGreater'");
        }
        if(!empty($approvedDateLess))
        {
            $select->where("w.DateApproved <= '$approvedDateLess'");
        }
        if(!empty($invoicedDateGreater))
        {
            $select->where("w.DateInvoiced  >= '$invoicedDateGreater'");
        }
        if(!empty($invoicedDateLess))
        {
            $select->where("w.DateInvoiced  <= '$invoicedDateLess'");
        }
        
        if(!empty($techMarkedCompleteDateGreater) || !empty($techMarkedCompleteDateLess))
        {
            $select->where("ts.Description = 'Work Order Marked: Completed'");
        }

        if(!empty($techMarkedCompleteDateGreater))
        {
            $select->where("ts.DateTime_Stamp  >= '$techMarkedCompleteDateGreater'");
        }
        if(!empty($techMarkedCompleteDateLess))
        {
            $select->where("ts.DateTime_Stamp  <= '$techMarkedCompleteDateLess'");
        }
        
        $select->order("w.WIN_NUM DESC");
        $select->distinct();
                
        $records = Core_Database::fetchAll($select);  

        if(empty($records) || count($records)==0)  {
            return null;   
        }
        else
        {
            $winArray = array();
            foreach($records as $rec) {
                $winArray[]=$rec['WIN_NUM'];   
            }
            
            return $winArray;
        }
    }
    
    //---13676
/*Work Order is under a project with Site Status Reporting enabled
Work Start Date/Time are edited AFTER WO is created
Work Start Date/Time was not blank*/    
    public function checkforPopupAddWinTags($winNum)
    {
        //TABLE_WORK_ORDERS Project_ID StartDate StartTime
        // projects Project_ID   EnableSiteStatusReporting
        $db = Core_Database::getInstance();
        $select = $db->select();
        $select->from(array('w'=>Core_Database::TABLE_WORK_ORDERS),
                array('WIN_NUM','StartDate','StartTime','Project_ID')
        );
        $select->join(array('p'=>Core_Database::TABLE_PROJECTS),
                'p.Project_ID = w.Project_ID',
                array('EnableSiteStatusReporting')
        );
        $select->where("w.WIN_NUM = '$winNum'");
        $select->where("p.EnableSiteStatusReporting = 1");        
        $records = Core_Database::fetchAll($select);      
        if(empty($records) || count($records)==0){
            return 0;
        }    
        else{
            return 1;
        }        
    }
    
    //--- 13676
    /* 
    *   $Paperwork_Received: 0; 1
    *   $Incomplete_Paperwork: 0; 1
    */
    public function updatePaperWork($winNum,$Paperwork_Received,$Incomplete_Paperwork)
    {
        $criteria = "WIN_NUM = '$winNum'";
        $updateFields = array('Paperwork_Received'=>$Paperwork_Received,
                            'Incomplete_Paperwork'=>$Incomplete_Paperwork
                            );
        $result = Core_Database::update(Core_Database::TABLE_WORK_ORDERS,$updateFields,$criteria);
        return $result;        
    }
    
    //--- 13676
    public function createSiteIfNotExist_withDatafromWO($winNum)
    {
        if(empty($winNum)) return;
        //--- get site data from WO
        $fields = array('Address','City','Country','Project_ID','SiteEmail','SiteFax','SiteName','SiteNumber','SitePhone','Site_Contact_Name','State','Zipcode');

        $db = Core_Database::getInstance();
        $select = $db->select();
        $select->from(Core_Database::TABLE_WORK_ORDERS,$fields);
        $select->where("WIN_NUM = '$winNum'");
        $records = Core_Database::fetchAll($select);      
        if(empty($records) || count($records)==0) return;
        
        $rec = $records[0];
        $siteNumber = $rec['SiteNumber'];
        $project_ID = $rec['Project_ID'];
        if(empty($siteNumber) || empty($project_ID)) return;
        //--- does site exist?
        $db = Core_Database::getInstance();
        $select = $db->select();
        $select->from(Core_Database::TABLE_SITES,$fields);
        $select->where("SiteNumber = '$siteNumber'");
        $select->where("Project_ID = '$project_ID'");
        $records = Core_Database::fetchAll($select);  
        //--- insert site
        if(empty($records) || count($records)==0)    
        {
            $updateFields = array();
            foreach($fields as $field)
            {
                $updateFields[$field] = $rec[$field];
            }            
            $result = Core_Database::insert(Core_Database::TABLE_SITES,$updateFields);
            return $result;
        }
        else
        {
            return 0;
        }
    }
    //--- 13690
    //--- 13713, 13734, 13806
    /*
    $option = 1: update all WOs of project, 2: update all unassigned WOs of project
    */
    public function updateDeVryObserveAndPerform_ofWOs($projectID,$AllowDeVryInternstoObserveWork,$AllowDeVryInternstoPerformField,$option)
    {
        if(empty($option) || $option != 1 && $option != 2) return;
        if(empty($projectID)) return;
        if(empty($AllowDeVryInternstoObserveWork)) $AllowDeVryInternstoObserveWork=0;
        if(empty($AllowDeVryInternstoPerformField)) $AllowDeVryInternstoPerformField=0;

		$db = Core_Database::getInstance();
        
        if($option == 1) //update all WOs of project
        {
			$query = $db->query("SELECT WIN_NUM FROM work_orders WHERE Project_ID = '$projectID'");
        }
        else if($option == 2) //update all unassigned WOs of project
        {
			$query = $db->query("SELECT WIN_NUM FROM work_orders WHERE (Tech_ID is null OR Tech_ID='' OR Tech_ID=0) AND Project_ID ='$projectID'");
        }


		$result = $query->fetchAll();
		$winList = array();
		foreach ($result as $row) {
			$winList[] = $row["WIN_NUM"];
		}
		if (empty($winList)) return;
		$criteria = $db->quoteInto("WINNUM IN (?)", $winList);

        if($option==1 || $option==2)
        {
            $updateFields = array(
                'AllowDeVryInternstoObserveWork'=>$AllowDeVryInternstoObserveWork,
                'AllowDeVryInternstoPerformField'=>$AllowDeVryInternstoPerformField
            );
            $result = Core_Database::update("work_orders_additional_fields",
                        $updateFields,$criteria);        
            
        }

    }            
    //--- 13719,13806
    /*
    $startTypeID = 1: Firm Start Time, 2: Start Time Window    
    $option = 1: update all WOs of the project, 2: update all unassigned WOs of the project
    $startTypeID = empty: donot update this field
    $startTime = empty: donot update this field
    $endTime = empty: donot update this field
    $estimateDuration = empty: donot update this field
    $techArrivalInstruction = empty: donot update this field
    */
    public function updateServiceSchedule_ofWOs($projectID,
            $startTime,$endTime,$startTypeID,
            $estimateDuration,$techArrivalInstruction,
            $option)
    {
        if(empty($option) || ($option != 1 && $option != 2)) return;

        if(empty($projectID)) return;
        //--- update work_orders table
        if($option == 1) //update all WOs of project
        {            
            $criteria = "Project_ID ='$projectID'";            
        }
        else if($option == 2) //update all unassigned WOs of project
        {
            $criteria = "(Tech_ID is null OR Tech_ID='' OR Tech_ID=0) AND (Project_ID ='$projectID')";            
        }
        $updateFields = array();
        if(!empty($startTime)){
            $updateFields['StartTime'] = $startTime;
        }
        if(!empty($endTime)){
            $updateFields['EndTime'] = $endTime;
        }
        if(!empty($estimateDuration)){
            $updateFields['Duration'] = $estimateDuration;
        }
        if(count($updateFields) > 0){
            $result = Core_Database::update(Core_Database::TABLE_WORK_ORDERS,
                        $updateFields,$criteria);            
        }
        //--- update work_order_visits
        $db = Core_Database::getInstance();
        if($option == 1) //update all WOs of project
        {
            $query = $db->query("SELECT WIN_NUM FROM work_orders WHERE Project_ID = '$projectID'");
        }
        else if($option == 2) //update all unassigned WOs of project
        {
            $query = $db->query("SELECT WIN_NUM FROM work_orders WHERE (Tech_ID is null OR Tech_ID='' OR Tech_ID=0) AND Project_ID ='$projectID'");
        }        
        $result = $query->fetchAll();
        $winList = array();
        foreach ($result as $row) {
            $winList[] = $row["WIN_NUM"];
        }
        if (empty($winList)) return;
        
        $criteria_1 =  $db->quoteInto("WIN_NUM IN (?)", $winList);
        $criteria_1 .= " AND Type = 1";

        $updateFields_1 = array();
        if(!empty($startTime)){
            $updateFields_1['StartTime'] = $startTime;
        }
        if(!empty($endTime)){
            $updateFields_1['EndTime'] = $endTime;
        }
        if(!empty($startTypeID)){
            $updateFields_1['StartTypeID'] = $startTypeID;
        }
        if(!empty($estimateDuration)){
            $updateFields_1['EstimatedDuration'] = $estimateDuration;
        }
        if(!empty($techArrivalInstruction)){
            $updateFields_1['TechArrivalInstructions'] = $techArrivalInstruction;
        }
        
        if(count($updateFields_1) > 0){
            $result = Core_Database::update("work_order_visits",
                        $updateFields_1,$criteria_1);            
        }        
    }

    //13691 

    public function updateWoAdditionalFields_ofWOs($projectID,$WoAdditionalFieldValues,$option)
    {
        if(empty($option) || ($option != 1 && $option != 2)) return;
        if(empty($projectID)) return;
        if(empty($WoAdditionalFieldValues) && count($WoAdditionalFieldValues)==0) return;     
        
        $db = Core_Database::getInstance();
		$query = $db->query("SELECT WIN_NUM FROM work_orders WHERE Project_ID = '$projectID'");
		$result = $query->fetchAll();
		$winList = array();
		foreach ($result as $row) {
			$winList[] = $row["WIN_NUM"];
		}
		if (empty($winList)) return;       
        $criteria = $db->quoteInto("WINNUM IN (?)", $winList);
        if($option==1 || $option==2)
        {
            $result = Core_Database::update("work_orders_additional_fields",
                        $WoAdditionalFieldValues,$criteria);            
        }

    }            
    
    public function getNum_TechUnseen_QAMessage_ofWO($techID,$winNum)
    {
        $numMessageUnseen = 0;
        //--- count the answers that are unseen by tech
            $numAnswersUnseen = 0;
        $lastView = $this->getTech_LastViewQAWinDatetime($techID,$winNum);
        $db = Core_Database::getInstance();
        $query = $db->select();
        $query->from(array('ans'=>'wo_qa'),array('num'=>'Count(*)'));
        $query->join(array('que'=>'wo_qa'),
                'que.id = ans.Question_ID',
                array('')
        );
        $query->where("ans.Workorder_ID = '$winNum'");
        $query->where("ans.Type = ".self::WOQA_TYPE_ANSWER);
        $query->where("que.Workorder_ID = '$winNum'");
        $query->where("que.Type = ".self::WOQA_TYPE_QUESTION);
        $query->where("que.Tech_ID = '$techID'");
        if(!empty($lastView))        
        {
            $query->where("ans.Created_Date > '$lastView'");            
        }
        $records = Core_Database::fetchAll($query);     
        if(empty($records) || count($records)==0){
            $numAnswersUnseen = 0;
    }
        else {
            $numAnswersUnseen = $records[0]['num'];                      
        }
        $numMessageUnseen += $numAnswersUnseen;
        //--- count the client's message that are unseen by tech      Tech_Name
        $db = Core_Database::getInstance();
        $query = $db->select();
        $query->from('wo_qa',array('num'=>'Count(*)'));
        $query->where("Type = ".self::WOQA_TYPE_NORMAL_CLIENT_MESSAGE);
        $query->where("Workorder_ID = '$winNum'");
        $query->where("Tech_ID = '$techID'");
        if(!empty($lastView))        
        {
            $query->where("Created_Date > '$lastView'");            
        }
        $records = Core_Database::fetchAll($query);     
        $numClientMessageUnseen = 0;
        if(!empty($records) && count($records)>0)
        {
            $numClientMessageUnseen = $records[0]['num'];                      
        }
        $numMessageUnseen += $numClientMessageUnseen; 
        
        //13714
        //--- count the client's message sent to all techs that are unseen by tech 
        $db = Core_Database::getInstance();
        $query = $db->select();
        $query->from('wo_qa',array('num'=>'Count(*)'));
        $query->where("Type = ".self::WOQA_TYPE_NORMAL_CLIENT_MESSAGE);
        $query->where("Workorder_ID = '$winNum'");
        $query->where("Tech_Name = 'All Techs'");
        if(!empty($lastView))        
        {
            $query->where("Created_Date > '$lastView'");            
        }
        //print_r($query->__toString());
        $records = Core_Database::fetchAll($query);     
        $numClientMessageToAllTechsUnseen = 0;
        if(!empty($records) && count($records)>0)
        {
            $numClientMessageToAllTechsUnseen = $records[0]['num'];         
        }
        $numMessageUnseen += $numClientMessageToAllTechsUnseen; 
        //end 13714       
        //--- return 
        return $numMessageUnseen;
    }
   
   
    public function getFutureWOInfo($WorkOrderOwner)
    {
        $db = Core_Database::getInstance();
        $select = $db->select();
        $select->from("future_wo_info");
        $select->where("ClientUserName = '$WorkOrderOwner'");
        $records = Core_Database::fetchAll($select);   
                
        if(empty($records) || count($records)==0)
        {
            return null;
        }
        else
        {
            return $records[0];
        }
    }

 
    public function getFutureWOInfo_ByWinNum($winNum)
    {
        $db = Core_Database::getInstance();
        $select = $db->select();
        $select->from(Core_Database::TABLE_WORK_ORDERS,array('WorkOrderOwner'));
        $select->where("WIN_NUM = '$winNum'");
        $records = Core_Database::fetchAll($select);   
        if(empty($records) || count($records)==0)
        {
            return null;
        }
        else
        {
            return $this->getFutureWOInfo($records[0]['WorkOrderOwner']);
        }
                
        
    }
	
	public function getSystemGeneratedEmailAddress_ByWinNum($winNum) {
		// pulls the correct system generated email addresses to use with consideration for all wo / project settings, based on the win num
		$ApiWosClass = new Core_Api_WosClass();
		$FutureWOInfo = $ApiWosClass->getFutureWOInfo_ByWinNum($winNum);
		$SystemGeneratedEmailFrom = $FutureWOInfo['SystemGeneratedEmailFrom'];
		$SystemGeneratedEmailTo = $FutureWOInfo['SystemGeneratedEmailTo'];
		
		$wo = new Core_WorkOrder($winNum);
		$woEmailFrom = $wo->getSystemGeneratedEmailFrom();
		$woEmailTo = $wo->getSystemGeneratedEmailTo();
		$project_id = $wo->getProject_ID();
		
		$db = Core_Database::getInstance();
		$select = $db->select();
		$select->from(Core_Database::TABLE_PROJECTS, array("AllowCustomCommSettings", "To_Email", "From_Email"))
			->where('Project_ID = ?', $project_id);
		$projectInfo = $db->fetchRow($select);
		$result = array();
				
		if (!empty($woEmailFrom))
		{
			$result["SystemGeneratedEmailFrom"] = $woEmailFrom;
		}
		else if ($projectInfo && $projectInfo['AllowCustomCommSettings'] == 1 && !empty($SystemGeneratedEmailFrom))
		{
			$result["SystemGeneratedEmailFrom"] = $SystemGeneratedEmailFrom;
		}
		else
		{
			$result["SystemGeneratedEmailFrom"] = $projectInfo['From_Email'];
		}
		
		if (!empty($woEmailTo))
		{
			$result["SystemGeneratedEmailTo"] = $woEmailTo;
		}
		else if ($projectInfo && $projectInfo['AllowCustomCommSettings'] == 1 && !empty($SystemGeneratedEmailTo))
		{
			$result["SystemGeneratedEmailTo"] = $SystemGeneratedEmailTo;
		}
		else
		{
			$result["SystemGeneratedEmailTo"] = $projectInfo['To_Email'];
		}
		return $result;
	}            
            
    public function updateAM_AllWOs_ofCompany($companyID,$FSAccountManagerId)
    {
        if(empty($companyID) || empty($FSAccountManagerId)) return;

        $criteria = "WINNUM IN (SELECT WIN_NUM FROM work_orders WHERE Company_ID ='$companyID')";
        $updateFields = array('FSAccountManagerId'=>$FSAccountManagerId);
        $result = Core_Database::update("work_orders_additional_fields",$updateFields,$criteria);
        return $result;        
    }
    //--- 13622
    public function updateAM_AllNonInvoiceWOs_ofCompany($companyID,$FSAccountManagerId)
    {
        if(empty($companyID) || empty($FSAccountManagerId)) return;
        
        $criteria = "WINNUM IN (SELECT WIN_NUM FROM work_orders WHERE Invoiced=0 AND Company_ID ='$companyID' )";
        $updateFields = array('FSAccountManagerId'=>$FSAccountManagerId);
        $result = Core_Database::update("work_orders_additional_fields",$updateFields,$criteria);
        return $result;        
    }
    
    
    //--- 13622
    public function updateServiceTypeID_AllNonInvoiceWOs_ofProject($projectID,$ServiceTypeId)
    {
        if(empty($companyID) || empty($ServiceTypeId)) return;
        
        $criteria = "WINNUM IN (SELECT WIN_NUM FROM work_orders WHERE Invoiced=0 AND Project_ID ='$projectID')";
        $updateFields = array('ServiceTypeId'=>$ServiceTypeId);
        $result = Core_Database::update("work_orders_additional_fields",$updateFields,$criteria);
        return $result;        
    }
  
    public function updateCSDID_AllNonInvoiceWOs_ofProject($projectID,$FSClientServiceDirectorId)
    {
        if(empty($projectID) || empty($FSClientServiceDirectorId)) return;
        
        $criteria = "WINNUM IN (SELECT WIN_NUM FROM work_orders WHERE Invoiced=0 AND Project_ID ='$projectID')";
        $updateFields = array('FSClientServiceDirectorId'=>$FSClientServiceDirectorId);
        $result = Core_Database::update("work_orders_additional_fields",$updateFields,$criteria);
        return $result;        
    }

    public function getAPIWOS($fromDate,$toDate,$distance,$zipCode)
    {
        //--- latitude, longitude
        $latitude = $longitude = 0.0;
        if ( NULL !== $zipCode ) {
            $gserarch = new Core_Geocoding_Google;
            $gserarch->setZip($zipCode);
            $latitude   = $gserarch->getLat();
            $longitude  = $gserarch->getLon();
            unset($gserarch);
        } 
        //--- realDist
        $realDist = preg_replace('/[^0-9\.]/', '', $distance);
                
        //----
        $fields = array('WIN_NUM','Status','Project_ID','Project_Name','Headline','WO_Category','StartDate','StartTime','EndDate','EndTime','Address','City','State','Zipcode','Tech_ID','Tech_FName','Tech_LName','TechEmail','TechPhone','Description','Requirements','SpecialInstructions');
        $db = Core_Database::getInstance();
        $select = $db->select();
        $select->from(array('w'=>Core_Database::TABLE_WORK_ORDERS),
            $fields
        );
        $select->joinLeft(array('wv'=>'work_order_visits'),
            'wv.WIN_NUM = w.WIN_NUM',
            array('StartTypeID')
        );
        $select->joinLeft(array('woaf'=>'work_orders_additional_fields'),
            'woaf.WINNUM = w.WIN_NUM',
            array('AllowDeVryInternstoObserveWork')
        );
        $select->where("w.Tech_ID is not null OR w.Tech_ID <> ''");
        $select->where("wv.Type is null OR wv.Type=1");
        
        if(!empty($fromDate))
        {
            $select->where("w.StartDate >= '$fromDate'");
        }
        if(!empty($toDate))
        {
            $select->where("w.StartDate <= '$toDate'");
        }
        
//        echo("<br/>realDist: $realDist");
        $select = Core_Database::whereProximity($select, $latitude, $longitude, "Latitude", "Longitude", $realDist, "Distance");

        //echo("<br/>");print_r($select->__toString());
        
        $records = Core_Database::fetchAll($select);   
        
        
        if(empty($records) || count($records)==0)
        {
            return null;
        }
        else
        {
            for($i=0;$i<count($records);$i++)            
            {
                if(empty($records[$i]['AllowDeVryInternstoObserveWork'])) {
                    $records[$i]['AllowDeVryInternstoObserveWork']=0;   
                }
                if(empty($records[$i]['StartTypeID'])) {
                    $records[$i]['StartTypeID']=1;   
                }                
                $winNum = $records[$i]['WIN_NUM'];
                $records[$i]['EstimatedDurationOnsite'] = $this->getTotatDurationOnSite($winNum);
            }
        }
        return $records;  
        
    }
    //13784
    public function isWoCheckedOut($winNum)
    {
        $baseWOVisitInfo = $this->getBaseWOVisitInfo_ByWinnum($winNum);
        $optionCheckOut = $baseWOVisitInfo['OptionCheckInOut'];
        $techCheckOutDate = $baseWOVisitInfo['TechCheckOutDate'];
        $techCheckOutTime = $baseWOVisitInfo['TechCheckOutTime'];
        $CORCheckOutDate = $baseWOVisitInfo['CORCheckOutDate'];
        $CORCheckOutTime = $baseWOVisitInfo['CORCheckOutTime'];
        if( ($optionCheckOut==1 && !empty($techCheckOutDate)) || ($optionCheckOut==2 && !empty($CORCheckOutDate)) )  {
            return true;
        } else {
            return false;
        }
    }
    
    //13881
    public function blankCheckInOutByClient($winNum,$forBaseVisit=1)
    {
        if($forBaseVisit==self::VISIT_TYPE_BASE) {
            $baseWOVisitInfo = $this->getBaseWOVisitInfo_ByWinnum($winNum);
            $optionCheckOut = $baseWOVisitInfo['OptionCheckInOut'];
            $CORCheckInDate = $baseWOVisitInfo['CORCheckInDate'];
            $CORCheckInTime = $baseWOVisitInfo['CORCheckInTime'];
            $CORCheckOutDate = $baseWOVisitInfo['CORCheckOutDate'];
            $CORCheckOutTime = $baseWOVisitInfo['CORCheckOutTime'];
            if( $optionCheckOut==2 && empty($CORCheckInDate) && empty($CORCheckInTime) && empty($CORCheckOutDate) && empty($CORCheckOutTime) )  {
                return true;
            } else {
                return false;
            }
        } else {
            $db = Core_Database::getInstance();
            $query = $db->select();
            $query->from('work_order_visits');                 
            $query->where("WIN_NUM = '$winNum'");        
            $records = Core_Database::fetchAll($query);
            if(empty($records) || count($records)==0){
                return false;
            } else {
                foreach($records as $rec)
                {
                    $optionCheckOut = $rec['OptionCheckInOut'];
                    $CORCheckInDate = $rec['CORCheckInDate'];
                    $CORCheckInTime = $rec['CORCheckInTime'];
                    $CORCheckOutDate = $rec['CORCheckOutDate'];
                    $CORCheckOutTime = $rec['CORCheckOutTime']; 
                    if( $optionCheckOut==2 && empty($CORCheckInDate) && empty($CORCheckInTime) && empty($CORCheckOutDate) && empty($CORCheckOutTime) )  {
                        return true;
                    }
                }
                return false;
            }
        }
    }
    
        
     
    //13881
    public function updateCalculatedTechHrs_WhenBlankInfo($winNum)
    {
        $blankInfo = $this->blankCheckInOutByClient($winNum,0);
        if($blankInfo){
            //--- work order info
            $db = Core_Database::getInstance();
            $query = $db->select();
            $query->from(Core_Database::TABLE_WORK_ORDERS,
                array('calculatedTechHrs','Amount_Per','baseTechPay'));                 
            $query->where("WIN_NUM = '$winNum'");        
            $wos = Core_Database::fetchAll($query);
            if(empty($wos)){
                return;
            }
            $wo = $wos[0];
            //--- calculatedTechHrs
            $visits = $this->GetAllWOVisitList($winNum);            
            $calculatedTechHrs = 0;
            foreach($visits as $visit)
            {
                $OptionCheckInOut = $visit['OptionCheckInOut'];
                $techTotalHours = $visit['TechTotalHours'];
                $CORTotalHours = $visit['CORTotalHours'];
                if($OptionCheckInOut==1) $visitHours = $techTotalHours;
                else if($OptionCheckInOut==2) $visitHours = $CORTotalHours;
                else $visitHours=0;                            
                $calculatedTechHrs += $visitHours;
            }
            //--- update
            $criteria = "WIN_NUM = '$winNum'";
            $fields = array();
            $fields['calculatedTechHrs'] = $calculatedTechHrs;
            if($wo['Amount_Per']=='Hour'){
                $fields['baseTechPay'] = $calculatedTechHrs * $wo['Tech_Bid_Amount'];
            }
            $result = Core_Database::update(Core_Database::TABLE_WORK_ORDERS,$fields,$criteria);
            return $result;
        }
    }
    
    //13889
    public function exists_ClientWorkOrderID($clientWorkOrderID)
    {
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from(Core_Database::TABLE_WORK_ORDERS,array('WO_ID'));
        $select->where("WO_ID = ?", $clientWorkOrderID);
        $records = Core_Database::fetchAll($select); 
        
        if(!empty($records) && count($records) > 0){
            return true;
        } else {
            return false;
        }
    }
    
    //13940 
    public function insertBCatsFromProjectIfnotExist($winNum,$projectId,$expenseReportingOfProject)
    {
        if(empty($projectId)) {
            return;
        }
        $current = $this->getWorkOrderBCats($winNum);
        if(empty($current) || count($current)==0)
        {
            if(empty($expenseReportingOfProject) || count($expenseReportingOfProject)==0){
                $projectApi = new Core_Api_ProjectClass();
                $expenseReportingOfProject = $projectApi->getProjectBCats_insertFromClientIfNotExists($projectId);                
            }
            
            foreach($expenseReportingOfProject as $rec)            
            {
                $fieldvals = array("winid"=>$winNum,
                    "bcatname"=>$rec["bcatname"],
                    "bcatdesc"=>$rec["bcatdesc"],
                    "ischecked"=>$rec["ischecked"],
                    "reimbursable_thru_fs"=>$rec["reimbursable_thru_fs"],
                    "require_doc_upl"=>$rec["require_doc_upl"],
                    "bcattype"=>$rec["bcattype"]);   
                
                $bcatname = $rec["bcatname"];
                if($this->exists_WinBCatName($winNum,$bcatname)==0);    
                    $result = Core_Database::insert("work_orders_bcats",$fieldvals);        
            }            
        }       
        return;        
    }
    
    //13943
    /*
    $filters-keys:
    - Tech_ID:
    - Company_ID:
    - Status: InAccounting_OR_Completed
    */
    public function countWOs($filters)
    {
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from(Core_Database::TABLE_WORK_ORDERS,
                array('NUM'=>'Count(WIN_NUM)')
        );
        if(!empty($filters['Tech_ID'])){
            $select->where("Tech_ID = '".$filters['Tech_ID']."'");            
        }
        if(!empty($filters['Company_ID'])){
            $select->where("Company_ID = '".$filters['Company_ID']."'");            
        }
        if(!empty($filters['Status'])){
            if($filters['Status']=='InAccounting_OR_Completed'){
                $select->where("Approved=1 AND (Type_ID = 1 OR Type_ID = 2)");    
            } else {
                $status = strtolower($filters['Status']);
                $select->where("Status = '$status'");
            }
        }
        $records = Core_Database::fetchAll($select); 
        
        if(!empty($records) && count($records) > 0){
            return $records[0]['NUM'];
        } else {
            return 0;
        }
    }
    
    //13943
    public function TotalWos_byTechandCompany($techID,$companyID)
    {
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from('tech_company_wo_count', array('TotalWosCompany'));
        $select->where("Tech_ID='$techID'");
        $select->where("Company_ID='$companyID'");
        $records = Core_Database::fetchAll($select); 
        if(!empty($records) && count($records) > 0){
            return $records[0]['TotalWosCompany'];
        } else {
            return 0;
        }
    }
    
    //--- 13555
    public function updateBaseVisit_TechArrivalInstructions($winNumFrom,$winNumTo)
    {
        $visits = $this->getBaseWOVisitInfo($winNumFrom);
        if(empty($visits) || count($visits)==0)        {
            return;
        }
        
        $visit = $visits["$winNumFrom"];
        $techArrivalInstructions = $visit['TechArrivalInstructions'];
        //--- update work_order_visits
        $fieldvals=array('TechArrivalInstructions'=>$techArrivalInstructions
        );
        $criteria = "WIN_NUM='$winNumTo' AND Type=1 ";  
        $result = Core_Database::update("work_order_visits",$fieldvals,$criteria);     
    }
    
    //14078
    public function updateWorkOrder_CSDID($winNum,$CSDID)
    { 
        $criteria = "WINNUM = '$winNum'";
        $updateFields = array("FSClientServiceDirectorId"=>$CSDID);
        $result = Core_Database::update("work_orders_additional_fields",$updateFields,$criteria);
        return $result;
    }
    
    public function updateWorkOrder_ServiceTypeId($winNum,$ServiceTypeId)
    { 
        $criteria = "WINNUM = '$winNum'";
        $updateFields = array("ServiceTypeId"=>$ServiceTypeId);
        $result = Core_Database::update("work_orders_additional_fields",$updateFields,$criteria);
        return $result;
    }
    
    public function updateWorkOrder_EntityTypeId($winNum,$EntityTypeId)
    { 
        $criteria = "WINNUM = '$winNum'";
        $updateFields = array("EntityTypeId"=>$EntityTypeId);
        $result = Core_Database::update("work_orders_additional_fields",$updateFields,$criteria);
        return $result;
    }

    //14109
    public function getSiteSurveyReport($companyID)
    {
         $db = Core_Database::getInstance();  
         $select = $db->select();
         $select->from(array('s'=>"SiteSurvey"),array('*'));
         $select->join(array('w'=>Core_Database::TABLE_WORK_ORDERS),
                        'w.WIN_NUM = s.WIN_NUM',
                        array('SiteNumber','Address','City','State','Zipcode','Tech_ID','Tech_FName','Tech_LName','StartDate','StartTime')
         );
         $records = Core_Database::fetchAll($select);  
         
         if(!empty($records) && count($records) >0 ){
             
             $rows = array();             
             $CoreFile = new Core_File();
             foreach($records as $rec){

                $key = 'Sec1Q13_FrontofStore'; 
                $rec[$key.'_Num'] = !empty($rec[$key])? 1 : 0; 
                $rec[$key.'_Link'] = $this->getLink($rec[$key]);

                $key = 'S3Q7a_BE_Bline_Pic'; 
                $rec[$key.'_Num'] = !empty($rec[$key])? 1 : 0; 
                $rec[$key.'_Link'] = $this->getLink($rec[$key]);

                $key = 'S3Q7b_BE_Fline_Pic'; 
                $rec[$key.'_Num'] = !empty($rec[$key])? 1 : 0; 
                $rec[$key.'_Link'] = $this->getLink($rec[$key]);
                
                $key = 'S3Q7c_BE_DThru_Pic'; 
                $rec[$key.'_Num'] = !empty($rec[$key])? 1 : 0; 
                $rec[$key.'_Link'] = $this->getLink($rec[$key]);

                $key = 'S3Q7d_BE_Others_Zip'; 
                $rec[$key.'_Link'] = $this->getLink($rec[$key]);

                $key = 'S3Q8b_GR_Bline_Zip'; 
                $rec[$key.'_Link'] = $this->getLink($rec[$key]);

                $key = 'S3Q8c_GR_Fline_Zip'; 
                $rec[$key.'_Link'] = $this->getLink($rec[$key]);

                $key = 'S3Q8d_GR_DThru_Zip'; 
                $rec[$key.'_Link'] = $this->getLink($rec[$key]);

                $key = 'S3Q8e_GR_Others_Zip'; 
                $rec[$key.'_Link'] = $this->getLink($rec[$key]);

                $key = 'S4Q6_Pic'; 
                $rec[$key.'_Num'] = !empty($rec[$key])? 1 : 0; 
                $rec[$key.'_Link'] = $this->getLink($rec[$key]);
                
                $key = 'S4Q12_Zip'; 
                $rec[$key.'_Link'] = $this->getLink($rec[$key]);
                
                $key = 'S5Q2b_Zip'; 
                $rec[$key.'_Link'] = $this->getLink($rec[$key]);

                $key = 'S5Q3b_Zip'; 
                $rec[$key.'_Link'] = $this->getLink($rec[$key]);

                $key = 'S5Q4b_Zip'; 
                $rec[$key.'_Link'] = $this->getLink($rec[$key]);
                
                $key = 'S6Q3_Pic'; 
                $rec[$key.'_Num'] = !empty($rec[$key])? 1 : 0; 
                $rec[$key.'_Link'] = $this->getLink($rec[$key]);

                $key = 'S6Q4_Pic'; 
                $rec[$key.'_Num'] = !empty($rec[$key])? 1 : 0; 
                $rec[$key.'_Link'] = $this->getLink($rec[$key]);

                $key = 'S6Q5_Pic'; 
                $rec[$key.'_Num'] = !empty($rec[$key])? 1 : 0; 
                $rec[$key.'_Link'] = $this->getLink($rec[$key]);

                $key = 'S7Q5_WaterFilter_Pic'; 
                $rec[$key.'_Num'] = !empty($rec[$key])? 1 : 0; 
                $rec[$key.'_Link'] = $this->getLink($rec[$key]);

                $key = 'S7Q5_WaterFilter_Pic'; 
                $rec[$key.'_Num'] = !empty($rec[$key])? 1 : 0; 
                $rec[$key.'_Link'] = $this->getLink($rec[$key]);
                
                $key = 'S8Q1_ElecPanel_Pic'; 
                $rec[$key.'_Num'] = !empty($rec[$key])? 1 : 0; 
                $rec[$key.'_Link'] = $this->getLink($rec[$key]);
                
                $key = 'S8Q2_Circuits_Zip';                 
                $rec[$key.'_Link'] = $this->getLink($rec[$key]);
                
                $key = 'S8Q3_PanelDiagram_Pic'; 
                $rec[$key.'_Num'] = !empty($rec[$key])? 1 : 0; 
                $rec[$key.'_Link'] = $this->getLink($rec[$key]);                

                $key = 'S0_AllPhotos_Zip';                 
                $rec[$key.'_Link'] = $this->getLink($rec[$key]);
                 
                $rows[]=$rec;
             }
             
             return $rows;
         } else {
             return null;
         }

    }
    
    public function getLink($filename)
    {
        $CoreFile = new Core_File();
        $link = '';
        if(!empty($filename)){
            $link = $CoreFile->getS3Url($filename, S3_TECHS_DOCS_DIR);
        }
        return $link;
    }
    
    
    //14109 
    public function getWOInfoforSurvey($winNum)
    {
        $fields = array('WIN_NUM','SiteNumber','Address','City','State','Zipcode','Tech_ID','Tech_FName','Tech_LName','StartDate','StartTime');
        $result = $this->getWoInfo($winNum,$fields);
        return $result;
    }
    
    //14130
    public function OverwriteWOPropFromProjectProp($winNum,$projectID,$props)
    {
        $availProps = array('Qty_Days','Qty_Weeks','Qty_Years');
        if(empty($projectID) || empty($winNum) || empty($props) || count($props)== 0) return;
        
        $coreProject = new Core_Api_ProjectClass();
        $projectInfo = $coreProject->getProjectById($projectID);
        if(empty($projectInfo)) return;

        $updateFields = array();
        foreach($props as $k){
            if(in_array($k,$availProps)){
                $updateFields[$k] = $projectInfo[$k];
            }
        }
        
        if(count($updateFields) > 0){
            $criteria = "WINNUM='$winNum'";
            $result = Core_Database::update("work_orders_additional_fields",
                                    $updateFields,$criteria);
        }
    }
    //14163
    public function trim_characters( $text, $length) {

        $length = (int) $length;
        $text = trim( strip_tags( $text ) );

        if ( strlen( $text ) > $length ) {
           // $text = substr( $text, 0, $length + 1 );
            $offset = ($length - 3) - strlen($text);//
            $text = substr($text, 0, strrpos($text, ' ', $offset));//
            $words = preg_split( "/[\s]|&nbsp;/", $text, -1, PREG_SPLIT_NO_EMPTY );
            preg_match( "/[\s]|&nbsp;/", $text, $lastchar, 0, $length );
            if ( empty( $lastchar ) )
                array_pop( $words );

            $text = implode( ' ', $words ); 
            $text = $text.'...';
        }

        return  $text;
}
    //14163

}

