<?php
/**
 * Core_Api_AdminProjectClass
 *
 * @author Alex Scherba
 */
class Core_Api_AdminProjectClass
{
	public function __construct() {
		Core_Caspio::init();
	}

	/**
	 * Function for getting Projects for current usre
	 *
	 * @param string $user
	 * @param string $pass
	 * @param array $project_ids
	 * @return API_Response
	 */
	public function getProjects($user, $pass, $project_ids, $companyId = null)
	{
       	$logger = Core_Api_Logger::getInstance();
		$logger->setUrl('getProjects');
        $logger->setLogin($user);
		$logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');
        $errors = Core_Api_Error::getInstance();
		
		if (!is_array($project_ids)) $project_ids = array($project_ids);

		$db = Core_Database::getInstance();
		$select = $db->select()->from(Core_Database::TABLE_PROJECTS);

		if (!empty($companyId)) {
    		$select->where('Project_Company_ID=?', $companyId);
		}
		if (count($project_ids)) $select->where('Project_ID IN (?)', $project_ids);
		$result = Core_Database::fetchAll($select);
		$err = $errors->getErrors();
		if (!empty($err))
			return API_Response::fail();

		return API_Response::success($this->makeProjectsArray($result));
	}

    public function updateProject($user, $pass, $prjId, $project, $ignoreWarnings = false)
    {
        $errors = Core_Api_Error::getInstance();

        if (is_string($project)) {
            $prjXML = $project;
        } else {
            
			$dom = new DOMDocument("1.0");
			$dom->appendChild($project->toXMLElementTect($dom));
			$prjXML = $dom->saveXML();
		}

       	/* Log request */
       	$logger = Core_Api_Logger::getInstance();
		$logger->setUrl('Update Project '.$prjId);
        $logger->setLogin($user);



	$authData = array('login'=>$user, 'password'=>$pass);
        $userClass = new Core_Api_AdminUser();
	$userClass->checkAuthentication($authData);
        if (!$userClass->isAuthenticate())
		return API_Response::fail();

        //$logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');

        $params = array();
		$params['Project_ID'] = $prjId;
		$params['xml'] = $prjXML;

		if (empty($params['Project_ID']))
			$errors->addError(2, 'Project_ID parameter is empty but required');

		if (empty($params['xml']))
			$errors->addError(2, 'XML parameter is empty but required');

		$err = $errors->getErrors();
		if (!empty($err))
			return API_Response::fail();

		$prjData = $this->getDataFromProjectXML($params['xml']);


		$db = Core_Database::getInstance();
		$fieldListMap = Core_Database::getFieldList(Core_Database::TABLE_PROJECTS);
		$fieldListMap = array_combine($fieldListMap, $fieldListMap);
		$select = $db->select();
		$select->from(Core_Database::TABLE_PROJECTS, $fieldListMap)
			->where("Project_ID = ?", $prjId);

		$curPrj = Core_Database::fetchAll($select);

        if (empty($curPrj[0])) {
            $err = $errors->getErrors();
    		if (!empty($err))
				return API_Response::fail();

        	$errors->addError(7, 'Project '.$prjId.' does not exist');
			return API_Response::fail();
        }
        $curPrj = $curPrj[0];

		$fieldListType = Core_Database::getFieldListType(Core_Database::TABLE_PROJECTS);

		$prjData = Core_Database::convertQueryResult($prjData, $fieldListType);
		$curPrj = Core_Database::convertQueryResult($curPrj, $fieldListType);

        $updatedFields = array();
        foreach ($curPrj as $key=>$val) {
            if (isset($prjData[$key]) && $prjData[$key]!==null) {
                $updatedFields[$key] = $prjData[$key];
            } else {
                $updatedFields[$key] = $val;
            }
        }

/*	if (!$userClass->isPM()) {
         	$updatedFields["PcntDeduct"] = $curPrj["PcntDeduct"];
	}*/

		$factory = new Core_Validation_Factory(Core_Validation_Factory::VALIDATION_TYPE_PROJECT_UPDATE);
	    $validation = $factory->getValidationObject($updatedFields);

	    if ($validation->validate() === false) {
            $errors->addErrors('6', $validation->getErrors());
			return API_Response::fail();
        }
		$updatedChangedFields = array();
		$updatedFieldsConverted = Core_Database::convertQueryResult($updatedFields, $fieldListType);
		foreach ($updatedFieldsConverted as $k => $v) {
			if (array_key_exists($k, $curPrj ) && $curPrj[$k] === $v) continue;
			$updatedChangedFields[$k] = $updatedFields[$k];
		}
        $result = false;
		$result = Core_Database::update(Core_Database::TABLE_PROJECTS, $updatedChangedFields, $db->quoteInto("Project_ID = ?", $prjId));

        Core_TimeStamp::createProjectTimeStamp($prjId, $user, $pass);

        return API_Response::success(array(new API_Project($prjId)));
    }

    private function makeProjectsArray($result)
	{
		$wo = array();
	    if (!empty($result[0])) {
			foreach ($result as $key=>$value)
				$wo[] = API_Project::projectWithResult($value);
	    }
		return $wo;
	}


   	private function makeProjectsXML($result)
	{
		$dom = new DOMDocument("1.0");
		$root = $dom->createElement('Projects');
	    if (!empty($result[0])) {
		    foreach ($result as $key=>$value) {
                $pj = $dom->createElement('Project');

                $id = $dom->createAttribute('ID');
                $name = $dom->createAttribute('NAME');

                $id->appendChild($dom->createTextNode($value['Project_ID']));
                $name->appendChild($dom->createTextNode($value[' Project_Name']));

                $pj->appendChild($id);
                $pj->appendChild($name);

                $root->appendChild($pj);
            }
	    }
	    $dom->appendChild($root);
		return $dom->saveXML();
	}

    private function getDataFromProjectXML($xml)
	{
	    $result = array();
		$dom = new DOMDocument("1.0");
		$dom->loadXML($xml);
		$nodes = $dom->getElementsByTagName('Project')->item(0)->childNodes;

		foreach ($nodes as $node)
		{
			$result[$node->nodeName] = $node->nodeValue;
		}

		return $result;
	}

    

}
