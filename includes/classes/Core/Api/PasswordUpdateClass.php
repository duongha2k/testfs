<?php
class Core_Api_PasswordUpdateClass
{
    public function MustChangePasswordorNot_Client($clientID)
    {
        //$clientID, ClientID, clients
         $db = Core_Database::getInstance();  
         $select = $db->select();
         $select->from("clients");
         $select->where("ClientID = '$clientID'");
         $records = Core_Database::fetchAll($select);  
         if(empty($records) || count($records)==0) return 0;
         else
         {
             $mustChangePassword = $records[0]['MustChangePassword'];
             if(empty($mustChangePassword) || $mustChangePassword==0) return 0;
             else return 1;
         }
    }

    public function MustChangePasswordorNot_Tech($techID)
    {
         $db = Core_Database::getInstance();  
         $select = $db->select();
         $select->from("TechBankInfo");
         $select->where("TechID = '$techID'");
         $records = Core_Database::fetchAll($select);  
         if(empty($records) || count($records)==0) return 0;
         else
         {
             $mustChangePassword = $records[0]['MustChangePassword'];
             if(empty($mustChangePassword) || $mustChangePassword==0) return 0;
             else return 1;
         }
    }

    public function MustChangePasswordorNot_Admin($adminID)
    {
         $db = Core_Database::getInstance();  
         $select = $db->select();
         $select->from("admins");
         $select->where("AdminID = '$adminID'");
         $records = Core_Database::fetchAll($select);  
         if(empty($records) || count($records)==0) return 0;
         else
         {
             $mustChangePassword = $records[0]['MustChangePassword'];
             if(empty($mustChangePassword) || $mustChangePassword==0) return 0;
             else return 1;
         }
    }
    public function MustChangePasswordorNot_AdminUserName($adminUsername)
    {
         $db = Core_Database::getInstance();  
         $select = $db->select();
         $select->from("admins");
         $select->where("UserName = '$adminUsername'");
         $records = Core_Database::fetchAll($select);  
         if(empty($records) || count($records)==0) return 0;
         else
         {
             $mustChangePassword = $records[0]['MustChangePassword'];
             if(empty($mustChangePassword) || $mustChangePassword==0) return 0;
             else return 1;
         }
    }
    
    public function UpdatePassword_Client($clientID,$newPassword)
    {
		$username = false;

		$db = Core_Database::getInstance();
		$select = $db->select();
		$select->from ("clients");
		$select->where ("ClientID = ?", $clientID);
		$records = Core_Database::fetchAll ($select);

		if (!empty ($records)) {
			$username = $records[0]["UserName"];
		}

		$fields = array('Password'=>$newPassword,
            'MustChangePassword'=>0,'DateOfLastPasswordChanged'=>date("Y-m-d H:i:s")
        );
        
        $criteria = "ClientID = '$clientID'";
        $result = Core_Database::update("clients",$fields,$criteria);

		if ($username) {
			$authData = array('login'=>$username, 'password'=>$newPassword);
			$user = new Core_Api_User();
			$user->checkAuthentication($authData);
		}

        return $result;
    }

    public function UpdatePassword_Tech($techID,$newPassword)
    {
        $fields = array('Password'=>$newPassword,
            'MustChangePassword'=>0,'DateOfLastPasswordChanged'=>date("Y-m-d H:i:s")
        );
        $criteria = "TechID = '$techID'";
        $result = Core_Database::update("TechBankInfo",$fields,$criteria);        
        return $result;
    }
    
    public function UpdatePassword_AdminUsername($adminUsername,$newPassword)
    {
        $fields = array('Password'=>$newPassword,
            'MustChangePassword'=>0,'DateOfLastPasswordChanged'=>date("Y-m-d H:i:s")
        );
        $criteria = "UserName = '$adminUsername'";
        $result = Core_Database::update("admins",$fields,$criteria);                
        return $result;
    }
    
    public function getClientList_NotUpdatedPassword($limit=0,$offet=0)
    {
         $db = Core_Database::getInstance();  
         $select = $db->select();
         $select->from(array('c'=>"clients"),array('ClientID','ContactName','Email1','ContactPhone1','Company_ID','UserName'));
         $select->joinLeft(array('ext'=>'clients_ext'),
            'ext.ClientId=c.ClientID',
            array('FSClientServiceDirectorId','FSAccountManagerId')
         );
         $select->joinLeft(array('os1'=>'operations_staff'),
            'os1.id=ext.FSClientServiceDirectorId',
            array('CSDName'=>'name','CSDEmail'=>'email')
         );
         $select->joinLeft(array('os2'=>'operations_staff'),
            'os2.id=ext.FSAccountManagerId',
            array('AMName'=>'name','AMEmail'=>'email')
         );         
         $select->where("c.MustChangePassword=1");
         $select->where("c.Company_ID is not null");
         $select->where("c.Company_ID<>'' ");
         $select->order("c.Company_ID desc");
         if(!empty($limit))
         {
             $select->limit($limit,$offet);
         }
         $records = Core_Database::fetchAll($select);  
         return $records;
    }

    public function Count_ClientList_NotUpdatedPassword()
    {
         $db = Core_Database::getInstance();  
         $select = $db->select();
         $select->from(array('c'=>"clients"),array('Num'=>'(Count(*))'));
         $select->joinLeft(array('ext'=>'clients_ext'),
            'ext.ClientId=c.ClientID',
            array('FSClientServiceDirectorId','FSAccountManagerId')
         );
         $select->joinLeft(array('os1'=>'operations_staff'),
            'os1.id=ext.FSClientServiceDirectorId',
            array('CSDName'=>'name','CSDEmail'=>'email')
         );
         $select->joinLeft(array('os2'=>'operations_staff'),
            'os2.id=ext.FSAccountManagerId',
            array('AMName'=>'name','AMEmail'=>'email')
         );         
         $select->where("c.MustChangePassword=1");
         $select->where("c.Company_ID is not null");
         $select->where("c.Company_ID<>'' ");
         $select->order("c.Company_ID desc");
         $records = Core_Database::fetchAll($select);  
         if(empty($records) || count($records)==0) return 0;
         else return $records[0]['Num'];
    }
    
    public function getTechList_NotUpdatedPassword($limit=0,$offet=0)
    {
         $db = Core_Database::getInstance();  
         $select = $db->select();
         $select->from("TechBankInfo",array('TechID','FirstName','LastName','PrimaryPhone','PrimaryEmail','UserName'));
         $select->where("MustChangePassword = 1");
         $select->where("Deactivated = 0");
         $select->order("FirstName");
         $select->order("LastName");
         if(!empty($limit))
         {
             $select->limit($limit,$offet);
         }
         //print_r($select->__toString());
         $records = Core_Database::fetchAll($select);  
         return $records;
    }
    
    public function Count_TechList_NotUpdatedPassword()
    {
         $db = Core_Database::getInstance();  
         $select = $db->select();
         $select->from("TechBankInfo",array('Num'=>'(Count(*))'));
         $select->where("MustChangePassword = 1");
         $select->where("Deactivated = 0");
         $select->order("FirstName");
         $select->order("LastName");
         $records = Core_Database::fetchAll($select);  
         if(empty($records) || count($records)==0) return 0;
         else return $records[0]['Num'];
    }
    
        
}
