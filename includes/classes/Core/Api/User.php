<?php

class Core_Api_User extends API_Abstract_User
{
    protected $_name = 'clients';

    private $CompanyName = '';
    private $ContactName = '';
    private $ContactPhone1 = '';
    private $ContactPhone2 = '';
    private $Email1 = '';
    private $Email2 = '';
    private $WebsiteURL = '';
    private $UserName = '';
    private $Password = '';
    private $ClientID = null;
    private $CCType = '';
    private $CCNumber = '';
    private $CCExp = '';
    private $CCCode = '';
    private $CCHolderName = '';
    private $BusinessAddress = '';
    private $BusinessCity = '';
    private $BusinessState = '';
    private $BusinessZip = '';
    private $CCBillingPhone = '';
    private $ContractTerms = '';
    private $AcceptTerms = 0;
    private $StaffComments = '';
    private $EIN = '';
    private $CompanyDescription = '';
    private $RegDate = null;
    private $BillingType = '';
    private $AccountEnabled = 0;
    private $MarkDelete = 0;
    private $ClientExpiration = null;
    private $AmountPaid = '';
    private $SearchZip = '';
    private $Company_Logo = '';
    private $CustomSignOff = null;
    private $CustomSignOff_Enabled = 1;
    private $SignOff_Disabled = 0;
    private $Admin = 0;
    private $Company_ID = '';
    private $ShowOnReports = 0;
    private $PrimaryContact = 0;
    private $Country = '';
    private $ProjectManager = 0;
    private $CIB = '';
    private $PricingRuleID = null;
    private $UserType = '';
    private $CreatedBy = '';

    private $contextId = '';
	private $contextName = '';
    
    private $FSAccountManagerId = 0;
    private $FSAccountManagerName = "";
    private $FSAccountManagerPhone = "";
    private $FSAccountManagerEmail = "";
    private $FSClientServiceDirectorId = 0;
    private $FSClientServiceDirectorName = "";
    private $FSClientServiceDirectorPhone = "";
    private $FSClientServiceDirectorEmail = "";
    
    private $File1URL="";
    private $File2URL="";
    private $File3URL="";
    
    private $LastLoginDate="";//13901
    //13970
    private $DeniedTech_NoShowGreater = 0;
    private $DeniedTech_NoShowPeriod = "";
    private $DeniedTech_BackOutGreater = 0;
    private $DeniedTech_BackOutPeriod = "";
    private $DeniedTech_PreferenceRatingSmaller = 0;
    private $DeniedTech_PreferenceRatingPeriod = "";
    private $DeniedTech_PerformanceRatingSmaller = 0;
    private $DeniedTech_PerformanceRatingPeriod = "";
   //end 13970
	
	private $contextPushWMEnabled=0;

    public $_restrictedValue=2;
    public $_openedValue=1;
    //622
    const SYSTEM_STATUS_ID_ACTIVE = 1;
    const SYSTEM_STATUS_ID_PARTIAL_RESTRICTION = 2;
    const SYSTEM_STATUS_ID_RESTRICT = 3;
    const SYSTEM_STATUS_ID_DEACTIVED = 4;//576
    //end 622
    public function  __construct($id = null) {
        parent::__construct();
        $this->ClientID = $id;
    }
    /**
	 * DEPRECATED - use loadWithExt
     * @param array $authData login/password or just login
     * @return Core_Api_User $this
     * @author Pavel Shutin
     */
    //13970
    public function load($authData,$useUserSession=true) {
		// DEPRECATED - use loadWithExt
		return $this->loadWithExt($authData, $useUserSession);
	}

    /**
     * @param int $id
     * @return Core_Api_User $this
     * @author Alex Scherba
     */
    public function loadById($id = null) {
        $errors = Core_Api_Error::getInstance();

        if (empty($id) && empty($this->ClientID)) {
			$errors->addError(2, 'ID is empty but required');
            return false;
		}
        $id = empty($id) ? $this->ClientID : $id;


        /*$this->UserName = Core_Caspio::caspioEscape($authData['login']);
        if (isset($authData['password'])) {
          $this->Password = Core_Caspio::caspioEscape($authData['password']);
        }*/

        $select = $this->select()
            ->where("ClientId = ?", $id);
      
        $user = $this->fetchRow($select);

	if (!empty($user)) 
        {
            foreach( $user as $col=>$val ) 
            {
                if(property_exists($this, $col) || $col == "ClientID"){ $this->$col = $val;};                
            }

            $this->ClientID = $id;
			$companyAdditionalFields = $this->getClientCompanyAdditionalFields($this->Company_ID);
			$this->contextPushWMEnabled = $companyAdditionalFields['PushWMEnabled'];

        return $this;
	} 
        else 
        {
			$er = $errors->getErrors();
			if (empty($er)) $errors->addError(3, 'There is no client with ID '.$id);
            return false;
		}
    }

	/**
	 * Pull any files associated with client account from clients_files table.
	 *
	 * @param array $authData
	 * @return array
	 */
	public function getFileList ($id = null, $type = false) {
		$errors = Core_Api_Error::getInstance();
    	$files = false;

		if (empty($id) && empty($this->ClientID)) {
			$errors->addError(2, 'ID is empty but required');
			return false;
		}

		if (!empty ($id)) {
			$select = $this->select ()->where ("Company_ID = ?", $id);
			$user = $this->fetchRow ($select);
		}
		else {
			$user = $this;
		}

		if (!empty ($user)) {
			$id = $user->ClientID;

			$db = Core_Database::getInstance ();
			$select = $db->select ();
			$select->from (Core_Database::TABLE_CLIENTS_FILES)
				->where ("company_id = ?", $user->Company_ID);
			if ($type !== false)
				$select->where ("type = ?", $type);
			$files = Core_Database::fetchAll ($select);
		}

		return $files;
	}

	/**
	 * Check up user information
	 *
	 * @param array $authData
	 * @return bool
	 */
	public function checkAuthentication($authData)
	{
		$errors = Core_Api_Error::getInstance();
        $this->authenticate = false;

		if (empty($authData['login']) || empty($authData['password'])) {
			$errors->addError(2, 'Login or password is empty but required');	
			return false;
		}
        
        if (!$this->checkProtocol()) {
			throw new SoapFault("jeff", "failing in check protocol");
			return false;
		}
        
        $this->load($authData);

        if ($this->ClientID) {
            $this->authenticate = true;
        }

		return $this->authenticate;
	}


    /**
     * Load Core_Api_User By Username
     * @param string $username
     * @return boolean
     * @author Artem Sukharev
     */
	public function lookupUsername( $username ) {
		if ( empty($username) ) return false;

        $fieldList = array("ClientID","Company_ID","CompanyName","ProjectManager","ContactName","Email1","Email2");
        $fieldList = array_combine($fieldList, $fieldList);

        $db = Core_Database::getInstance();
        $select = $db->select();
        $select->from(Core_Database::TABLE_CLIENTS, $fieldList)
            ->where("UserName = ?", Core_Caspio::caspioEscape($username));
        $user = Core_Database::fetchAll($select);

		if ( !empty($user) ) {
			$this->authenticate = false;
			$this->Company_ID  = Core_Caspio::caspioEscape($user[0]['Company_ID']);
            $this->ClientID    = Core_Caspio::caspioEscape($user[0]['ClientID']);
			$this->CompanyName = Core_Caspio::caspioEscape($user[0]['CompanyName']);
			$this->ContactName = Core_Caspio::caspioEscape($user[0]['ContactName']);
			$this->Email1      = Core_Caspio::caspioEscape($user[0]['Email1']);
			$this->Email2      = Core_Caspio::caspioEscape($user[0]['Email2']);
            return true;
		} else {
            if ( !$errors->hasErrors() ) $errors->addError(3, 'Cannot find user');
			$this->authenticate     = false;
			$this->CompanyName  = NULL;
           	$this->Email1       = NULL;
			$this->Email2       = NULL;
            return false;
		}
	}
	
	public function loginHistory($userName)
	{
		
		$class = new Core_Api_Class;
		$class->loginHistory($userName, "Client");
	}
	

    public function isPM()
    {
        return $this->ProjectManager;
    }

    public function isAdmin()
    {
        return $this->getAdmin();
    }

    public function getPassword()
    {
        return $this->Password;
    }

	public function getClientId()
	{
		return $this->ClientID;
	}

	public function getCompanyId()
	{
		return $this->Company_ID;
	}

	public function getCompanyName()
	{
		return $this->CompanyName;
	}

	public function getContextCompanyId() {
		return $this->contextId;
	}

	public function getContextCompanyName() {
		return $this->contextName;
	}

	public function getLogin()
	{
	    return $this->UserName;
    }
    public function getContactName()
    {
        return $this->ContactName;
    }
    public function getEmailPrimary()
    {
        return $this->Email1;
    }
    public function getEmailSecondary()
    {
        return $this->Email2;
    }

	public function getUserType() {
		return $this->UserType;
	}
	
	public function getAdmin() {
		return strtolower($this->UserType) == 'admin' ? 1 : 0;
	}
	
	static function getLoginByAuth($authLogin)
	{
	    list($login) = explode("|pmContext=", $authLogin);
	    return $login;
	}
    
    public function save($data = null){
        $errors = Core_Api_Error::getInstance();
        if( empty($data) ) {
            $info = $this->info();
            $data = array();
            foreach( $info['cols'] as $col ) $data[$col] = $this->$col;
        }
		if (isset($data['contextPushWMEnabled'])) {
			$this->saveClientCompanyAdditionalFields($this->getCompanyId(), array('PushWMEnabled' => $data['contextPushWMEnabled']));
			unset($data['contextPushWMEnabled']);
		}
		if (empty($data)) return $this;
        if( empty($this->ClientID) ){
            $objDateNow = new Zend_Date();
            $data['RegDate'] = $objDateNow->toString('yyyy-MM-dd hh:mm:ss');
            //13622
            unset($data['ClientID']);
            if(empty($data['SignOff_Disabled'])) unset($data['SignOff_Disabled']);
            //end: 13622
            $res = $this->insert($data);
            if( $res ) return new $this($res);
        } else {
		try {
            $res = $this->update($data, 'ClientID = '.$this->ClientID);
		} catch (Exception $e) {
		        $errors->addError('7', 'Incorrect SQL request');
		}
            if( $res ) {
                foreach( $data as $k=>$v ) $this->$k = $v;
                return $this;
            }
        }
        return null;
    }

    function __set($name,$value) {
        if (property_exists($this, $name)){
            $this->$name = $value;
        } else {
//            throw new Exception('Property not exists');
        }
    }

    function __get($name) {
        if (property_exists($this, $name)){
            return $this->$name;
        } else {
//            throw new Exception('Property not exists');
        }
    }

    public function toXMLElement($dom) {
		$cl = $dom->createElement('Client');
		$id = $dom->createAttribute('ClientID');
		$name = $dom->createAttribute('UserName');

		$id->appendChild($dom->createTextNode($this->ClientID));
		$name->appendChild($dom->createTextNode($this->UserName));

		$cl->appendChild($id);
		$cl->appendChild($name);

		return $cl;
	}

    public function toXMLElementTect($dom) {
		$fieldList = $this;
		$root = $dom->createElement('Client');
		foreach ($this as $key => $value) {
			if ($value === NULL) continue;
			if (is_bool($value))
				$value = $value === true ? "True" : "False";
			$parameter = $dom->createElement($key, htmlspecialchars($value, ENT_QUOTES));
			$root->appendChild($parameter);
		}
		$dom->appendChild($root);
		return $root;
	}

	public static function toXMLWithArray($array, $notUsed) {
		$dom = new DOMDocument("1.0");
		$root = $dom->createElement('Clients');
		foreach ($array as $data) {
			$root->appendChild($data->toXMLElement($dom));
		}
		$dom->appendChild($root);
		return $dom->saveXML();
	}

	public static function clientWithResult($result) {
		$client = new Core_Api_User();
		if (!empty($result)) {
			foreach ($result as $key=>$value) {
		    	$client->$key = $value;
		    }
		}
		return $client;
	}
	
	
    /**
     * uploadFile - upload project file to S3
     *
     * @param string $user
     * @param string $pass
     * @param string $prjId
     * @param string $fName
     * @param string $fData
     * @access public
     * @return API_Response
     */
    public function uploadFile( $user, $pass, $fName, $fData, $saveToField = '' )
    {
        $error = Core_Api_Error::getInstance();
        $error->resetErrors();
        $fName = ltrim(trim($fName), '/');
        $result = false;

        $file = new Core_File();

        if ($saveToField == "CustomSignOff") {
	        $uploadStatus = $file->uploadFile($fName, $fData, S3_CLIENTS_DOCS_DIR, true);
	        if ( !$uploadStatus) {
	            $error->addError(6, 'Upload file '.$fName.' failed');
	            return API_Response::fail();
	        }

	        $authData = array('login'=>$user, 'password'=>$pass);
	        $client = $this->load($authData); 
	        if ( $error->getErrors() ) return API_Response::fail();

	        $db = Core_Database::getInstance ();
	        $select = $db->select ();
	        $select->from (Core_Database::TABLE_CLIENTS_FILES)
	        	->where ("company_id = ?", $client->Company_ID)
	        	->where ("type = ?", Core_Files::SIGNOFF_FILE);
	        $files = Core_Database::fetchAll ($select);

	        if (!empty ($files)) {
	        	$file_id = $files[0]->id;
	        	$file_path = $files[0]->path;
				$file->removeFile($file_path, S3_CLIENTS_DOCS_DIR);	// Remove old file
	        	$result = $db->update (Core_Database::TABLE_CLIENTS_FILES, 
	        		array ("path" => "clients-docs/" . $fName), "id = " . $file_id
	        	);
	        }
	        else {
	        	$result = $db->insert (Core_Database::TABLE_CLIENTS_FILES, 
	        		array (
	        			"company_id" => $client->Company_ID,
	        			"path" => "clients-docs/" . $fName,
	        			"type" => Core_Files::SIGNOFF_FILE
	        		)
	        	);
	        }

        	$result = $result ? API_Response::success ($result) : API_Response::fail();
        }
        else {
	        $uploadStatus = $file->uploadFile($fName, $fData, S3_CLIENTS_LOGO_DIR, true);
	        if ( !$uploadStatus) {
	            $error->addError(6, 'Upload file '.$fName.' failed');
	            return API_Response::fail();
	        }

	        $authData = array('login'=>$user, 'password'=>$pass);
	        $client = $this->load($authData); 
	        if ( $error->getErrors() ) return API_Response::fail();
	        if ( !empty($client->Company_Logo) ) { // if it was uploaded file
	                $file->removeFile($client->Company_Logo, S3_CLIENTS_LOGO_DIR);// Remove old file
	        }

	        $client = $this->load($authData);
	        $result = $client->save(array('Company_Logo' => $fName));
        }

        return $result;
    }

	/**
	 * download project file from S3
	 *
	 * @param string $user
	 * @param string $pass
	 * @param string $fileName
	 * @return API_Response
     * @author Alex Scherba
	 */
    public function getFile($fileName)
    {
        $errors = Core_Api_Error::getInstance();
        // get file from S3
        $file = new Core_File();
        $file = $file->downloadFile($fileName, S3_CLIENTS_LOGO_DIR);
        if($file === false) $errors->addError(6, 'File not exists');

		$err = $errors->getErrors();
		if (!empty($err))
			return API_Response::fail();

        return $file;
    }

    public function getFilesFields(){
        return array('Company_Logo', 'CIB');
    }

    public function remove($id = null)
    {
        $id = empty($this->ClientID) ? $id : $this->ClientID;
        if (empty($id))
        {
            $errors = Core_Api_Error::getInstance();
            $errors->addError(7, 'Client ID empty but required.');
            return false;
        }
        //504
        //return $this->delete('ClientID = '.$id);
        $data = array();
        $data['MarkDelete'] = 1;
        $data['AccountEnabled'] = 0;
        try
        {
            $res = $this->update($data, 'ClientID = ' . $id);
        } catch (Exception $e)
        {
            $errors->addError('7', 'Incorrect SQL request');            
        }
        return true;
    }
	
	//13457
	public function editClientAccessValue($clientId,$attributeId,$accessValue)
    {
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from("cliaccessctl");
        $select->where("clientid = ?",$clientId);
        $select->where("cliaccess_atrib_id = ?",$attributeId);
        $accessCtr = Core_Database::fetchAll($select); 
        if(empty($accessCtr))
        {
            $result = Core_Database::insert("cliaccessctl",
                                        array("clientid"=>$clientId,
                                            "cliaccess_atrib_id"=>$attributeId,
                                            "cliaccess_atrib_value"=>$accessValue)
                                    );    
        }   
        else
        {
            $criteria = "clientid = ".$clientId." AND "."cliaccess_atrib_id = ".$attributeId;
            $result = Core_Database::update("cliaccessctl",
                                    array("cliaccess_atrib_value"=>$accessValue),
                                    $criteria                                           
                                );            
        }  
        return  $result;             
    }
        
    public function editExt($dataExt,$needSendEmail=0)
    {
        $companyId = $this->Company_ID;
        $clientId = $this->ClientID;
        $datetime = date("m/d/Y h:i A");
        $username = $_SESSION['Auth_User']['login'];
        //echo("companyId: $companyId, clientId: $clientId");die();//test
        //echo("pre");print_r($_SESSION);die();//test
        //print_r($dataExt);die();//test
        $FSClientServiceDirectorId = $dataExt['FSClientServiceDirectorId'];
        $FSAccountManagerId = $dataExt['FSAccountManagerId'];
        
        $File1URL = empty($dataExt['File1URL'])?"":$dataExt['File1URL'];
        $File2URL = empty($dataExt['File2URL'])?"":$dataExt['File2URL'];
        $File3URL = empty($dataExt['File3URL'])?"":$dataExt['File3URL'];
        $Upload1Date = empty($dataExt['Upload1Date'])?"":$dataExt['Upload1Date'];
        $Upload2Date = empty($dataExt['Upload2Date'])?"":$dataExt['Upload2Date'];
        $Upload3Date = empty($dataExt['Upload3Date'])?"":$dataExt['Upload3Date'];
                                      
        $db = Core_Database::getInstance();  
        $fieldList = array("ClientId","FSClientServiceDirectorId"
                    ,"FSAccountManagerId","File1URL","File2URL","File3URL"
                    ,"Upload1Date","Upload2Date","Upload3Date");
        $select = $db->select();
        $select->from("clients_ext", $fieldList)
            ->where("ClientId = ?", $this->ClientID);
        $userExt = Core_Database::fetchAll($select);  
        $actionMsg="";   
        $emailBody="";
        $commonObj = new Core_Api_CommonClass(); 
        $CSDName = $commonObj->getOpeStaffNameById($FSClientServiceDirectorId);
        $AMName = $commonObj->getOpeStaffNameById($FSAccountManagerId);
        if(empty($userExt))
        {            
            $insertFields = array("ClientId"=>$this->ClientID,
                                "FSClientServiceDirectorId"=>$FSClientServiceDirectorId,
                                "FSAccountManagerId"=>$FSAccountManagerId,
                                "File1URL"=>$File1URL,
                                "File2URL"=>$File2URL,
                                "File3URL"=>$File3URL,
                                "Upload1Date"=>$Upload1Date,
                                "Upload2Date"=>$Upload2Date,
                                "Upload3Date"=>$Upload3Date                                
                                );
                                
            $result = Core_Database::insert(Core_Database::TABLE_CLIENTS_EXT,$insertFields);
                              
            //--- write to log                
            $actionMsg.="<br/>Changed FS Client Service Director";
            $actionMsg.="<br/>To: ".$commonObj->getOpeStaffNameById($FSClientServiceDirectorId);
            $actionMsg.="<br/>Changed FS Account Manager";
            $actionMsg.="<br/>To: ".$commonObj->getOpeStaffNameById($FSAccountManagerId);
            //--- emailBody
            if(!empty($CSDName))
            {
                $emailBody .= "\nPlease be advised that on $datetime, $username changed the CSD for client $companyId to $CSDName.";
            }
            if(!empty($AMName))
            {
                $emailBody .= "\nPlease be advised that on $datetime, $username changed the AM for client $companyId to $AMName.";
            }
            //--- other                    
            if(!empty($File1URL))
            {
                $actionMsg.="<br/>Changed Non-Disclosure Agreement Document";
            }            
            if(!empty($File2URL))
            {
                $actionMsg.="<br/>Changed New Client Information Form Document";
            }            
            if(!empty($File3URL))
            {
                $actionMsg.="<br/>Changed Terms of Service Document";
            }                        
        }
        else
        {
            //--- old data 
           $OldFSClientServiceDirectorId = $userExt[0]["FSClientServiceDirectorId"];
           $OldFSAccountManagerId = $userExt[0]["FSAccountManagerId"]; 
           $OldCSDName = $commonObj->getOpeStaffNameById($OldFSClientServiceDirectorId);
           $OldAMName = $commonObj->getOpeStaffNameById($OldFSAccountManagerId);
           
           $OldFile1URL = empty($userExt[0]["File1URL"])?"":$userExt[0]["File1URL"]; 
           $OldFile2URL = empty($userExt[0]["File2URL"])?"":$userExt[0]["File2URL"];  
           $OldFile3URL = empty($userExt[0]["File3URL"])?"":$userExt[0]["File3URL"];
            
            //echo("ashagshahgs");die();//test
            $updateFields =  array("FSClientServiceDirectorId"=>$FSClientServiceDirectorId,
                                        "FSAccountManagerId"=>$FSAccountManagerId,
                                        "File1URL"=>$File1URL,
                                        "File2URL"=>$File2URL,
                                        "File3URL"=>$File3URL,
                                        "Upload1Date"=>$Upload1Date,
                                        "Upload2Date"=>$Upload2Date,
                                        "Upload3Date"=>$Upload3Date                                
                                );
            $criteria = "ClientId = " . $this->ClientID;
            $result = Core_Database::update(Core_Database::TABLE_CLIENTS_EXT,
                                $updateFields,
                                $criteria                                           
                );
           //--- write to log                
           if($OldFSClientServiceDirectorId != $FSClientServiceDirectorId)
           {
                if(!empty($OldFSClientServiceDirectorId) || !empty($FSClientServiceDirectorId))                      
                    $actionMsg.="<br/>Changed FS Client Service Director ";
                if(!empty($OldFSClientServiceDirectorId))
                    $actionMsg.="from: ".$commonObj->getOpeStaffNameById($OldFSClientServiceDirectorId);
                if(!empty($FSClientServiceDirectorId))    
                $actionMsg.="<br/>To: ".$commonObj->getOpeStaffNameById($FSClientServiceDirectorId);               
                else
                    $actionMsg.="<br/>To: empty";  
                    
                //--- emailBody             
                if(!empty($OldCSDName) && !empty($CSDName))
                {
                    $emailBody .= "\nPlease be advised that on $datetime, $username changed the CSD for client $companyId from $OldCSDName to $CSDName.";
           }  
                else if(!empty($OldCSDName) && empty($CSDName))
                {
                    $emailBody .= "\nPlease be advised that on $datetime, $username changed the CSD for client $companyId from $OldCSDName to empty.";                    
                }
                else if(empty($OldCSDName) && !empty($CSDName))
                {
                    $emailBody .= "\nPlease be advised that on $datetime, $username changed the CSD for client $companyId to $CSDName.";                    
                }                
           }  
           if($OldFSAccountManagerId != $FSAccountManagerId)
           {
                if(!empty($OldFSAccountManagerId) || !empty($FSAccountManagerId))       
                    $actionMsg.="<br/>Changed FS Account Manager ";
                if(!empty($OldFSAccountManagerId))       
                    $actionMsg.="from: ".$commonObj->getOpeStaffNameById($OldFSAccountManagerId);
                if(!empty($FSAccountManagerId))           
                $actionMsg.="<br/>To: ".$commonObj->getOpeStaffNameById($FSAccountManagerId);               
                    
                //--- emailBody            
                if(!empty($OldAMName) && !empty($AMName))
                {
                    $emailBody .= "\nPlease be advised that on $datetime, $username changed the CSD for client $companyId from $OldAMName to $AMName.";
           }  
                else if(!empty($OldAMName) && empty($AMName))
                {
                    $emailBody .= "\nPlease be advised that on $datetime, $username changed the CSD for client $companyId from $OldAMName to empty.";                    
                }
                else if(empty($OldAMName) && !empty($AMName))
                {
                    $emailBody .= "\nPlease be advised that on $datetime, $username changed the CSD for client $companyId to $AMName.";                    
                }                
           }  
           if($OldFile1URL!=$File1URL)
           {
                $actionMsg.="<br/>Changed Non-Disclosure Agreement Document";
           }
           if($OldFile2URL!=$File2URL)
           {
                $actionMsg.="<br/>Changed New Client Information Form Document";
           }            
           if($OldFile3URL!=$File3URL)
           {
                $actionMsg.="<br/>Changed Terms of Service Document";
           }                        
        }        
        //--- send email
        if(!empty($emailBody) && $needSendEmail==1)
        {
            $toEmail = "accounting@fieldsolutions.com";
            $fromName = "no-replies@fieldsolutions.com";
            $fromEmail = "no-replies@fieldsolutions.com";
            $subject = "Client Settings - FS Contact Info changed for $companyId";
            
            $this->sendemail($fromName,$fromEmail,$toEmail,$subject,$emailBody);            
        }
        //--- return
        return  $actionMsg;
        
    }
    public function sendemail($fromName,$fromEmail,$toEmail,$subject,$emailBody)
    {
        $mail = new Core_Mail();
        $mail->setBodyText($emailBody);
        $mail->setFromName($fromName);
        $mail->setFromEmail($fromEmail);
        $mail->setToEmail($toEmail);
        $mail->setSubject($subject);
        $mail->send();        
    }
    
    //--- 13999
    public function getClientExt($clientId)
    {                                        
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from(array('cliext'=>"clients_ext"),array('*'));
        $select->join(array('cli'=>'clients'),
                'cli.ClientID = cliext.ClientId',
                array('CompanyName')
        );
        $select->joinLeft(array('csd'=>'operations_staff'),
            'csd.id = cliext.FSClientServiceDirectorId',
            array('FSClientServiceDirectorName'=>'name',
                   'FSClientServiceDirectorEmail'=>'email',
                   'FSClientServiceDirectorPhone'=>'phone')
        );
        $select->joinLeft(array('am'=>'operations_staff'),
            'am.id = cliext.FSAccountManagerId',
            array('FSAccountManagerEmail'=>'name',
                   'FSClientServiceDirectorEmail'=>'email',
                   'FSAccountManagerPhone'=>'phone')
        );        
        $select->where("cliext.ClientId = ?", $clientId) ;
        $result = Core_Database::fetchAll($select);  
        return  $result;            
    }

    public function loadWithExt($authData, $useUserSession=true) 
    {

        $errors = Core_Api_Error::getInstance();

        if (!is_array($authData) || empty($authData['login'])) {
            $errors->addError(2, 'Login is empty but required');
            return false;
        }

        if (strpos($authData['login'], "|pmContext=") !== FALSE) {
            $parts = explode("|pmContext=", $authData['login'], 2);
            $authData['login'] = $parts[0];
            $this->contextId = Core_Caspio::caspioEscape($parts[1]);
        }

        $this->UserName = Core_Caspio::caspioEscape($authData['login']);
        if (isset($authData['password'])) {
          $this->Password = Core_Caspio::caspioEscape($authData['password']);
        }

        $fieldList = array("Password","ClientID","Company_ID","CompanyName",
            "ProjectManager",
            "ContactName",
            "Email1","Email2","UserType", "Admin",
            'EIN',
            'CompanyDescription',
            'BusinessAddress',
            'BusinessCity',
            'BusinessState',
            'BusinessZip',
            'BusinessZip',
            'Country',    
            'ContactPhone1',    
            'ContactPhone2',    
            'WebsiteURL',    
            'Password',
            'Company_Logo',
        	'CustomSignOff',
			'CustomSignOff_Enabled',
			'SignOff_Disabled',
            'DeniedTech_NoShowGreater',
            'DeniedTech_NoShowPeriod',
            'DeniedTech_BackOutGreater',
            'DeniedTech_BackOutPeriod',
            'DeniedTech_PreferenceRatingSmaller',
            'DeniedTech_PreferenceRatingPeriod',
            'DeniedTech_PerformanceRatingSmaller',
            'DeniedTech_PerformanceRatingPeriod'
            );
        $fieldList["ContextCompany"] = "(SELECT Company_ID FROM " . Core_Database::TABLE_CLIENTS . " WHERE Company_ID = '$this->contextId' LIMIT 0, 1)";
        $fieldList["ContextCompanyName"] = "(SELECT CompanyName FROM " . Core_Database::TABLE_CLIENTS . " WHERE Company_ID = '$this->contextId' LIMIT 0, 1)";

        $db = Core_Database::getInstance();
        $select = $db->select();
        $select->from(Core_Database::TABLE_CLIENTS, $fieldList)
            ->where("AccountEnabled = ?", 1)
            ->where("UserName = ?", $this->UserName);

        if (isset($authData['password'])) {
            $select->where("Password = ?", $this->Password);
        }

        if (defined('SHORT_AUTH') && $useUserSession) {
            if (!empty($_SESSION['UserObj']) && !empty($_SESSION['UserObj'][0]['ClientID']) && $_SESSION['UserObj'][0]['ContextCompany'] == $this->contextId) {
                $user = $_SESSION['UserObj'];
            }else{
                $user = Core_Database::fetchAll($select);
                $_SESSION['UserObj'] = $user;
            }
        }else{
            $user = Core_Database::fetchAll($select);
        }

        if (!empty($user)) 
        {
            foreach ($user[0] as $key => $value) {
                if (property_exists('Core_Api_User', $key)) {
                    $this->{$key} = $value;
                }
            }
            
            if ($user[0]["ProjectManager"] != 1 || empty($this->contextId)) {
                $this->contextId      = $this->Company_ID;
                $this->contextName    = $this->CompanyName;
                $this->ProjectManager = false;
            }
            else {
                $this->contextId      = empty($user[0]["ContextCompany"]) ? $this->Company_ID   : $this->contextId;
                $this->contextName    = empty($user[0]["ContextCompany"]) ? $this->CompanyName : $user[0]["ContextCompanyName"];
                $this->ProjectManager = true;
            }
			$companyAdditionalFields = $this->getClientCompanyAdditionalFields($this->contextId);
			if (!isset($_SESSION['UserObj']['contextPushWMEnabled'])) {
				if (defined('SHORT_AUTH'))
					$_SESSION['UserObj']['contextPushWMEnabled'] = $companyAdditionalFields['PushWMEnabled'];
			}
			$this->contextPushWMEnabled = $companyAdditionalFields['PushWMEnabled'];
            //---  $FSClientServiceDirectorId,$FSAccountManagerId
            $fieldList = array("ClientId","FSClientServiceDirectorId"
                            ,"FSAccountManagerId","File1URL","File2URL","File3URL");
            $select = $db->select();
            $select->from("clients_ext", $fieldList)
                ->where("ClientId = ?", $this->ClientID) ;
            $userExt = Core_Database::fetchAll($select);      
            if (!empty($userExt)) 
            {
                $this->FSClientServiceDirectorId = $userExt[0]['FSClientServiceDirectorId'];   
                $this->FSAccountManagerId = $userExt[0]['FSAccountManagerId'];  
                $this->File1URL = $userExt[0]['File1URL'];  
                $this->File2URL = $userExt[0]['File2URL'];  
                $this->File3URL = $userExt[0]['File3URL'];  
                  
                $os_fieldList = array("id","groupid","name","email","phone");
                
                //--- get FSClientServiceDirector info.
                $os_select = $db->select();            
                $os_select->from("operations_staff")
                            ->where("id = ?", $this->FSClientServiceDirectorId);
                $ostaffs = Core_Database::fetchAll($os_select);
                if(!empty($ostaffs))  
                {
                    $this->FSClientServiceDirectorName = $ostaffs[0]['name'];
                    $this->FSClientServiceDirectorPhone = $ostaffs[0]['phone'];
                    $this->FSClientServiceDirectorEmail = $ostaffs[0]['email'];                     
                }    
                else
                {
                    $this->FSClientServiceDirectorName = "";
                    $this->FSClientServiceDirectorPhone = "";
                    $this->FSClientServiceDirectorEmail = "";
                }
                //echo("FSClientServiceDirectorId: ".$this->FSClientServiceDirectorId);//test
                //echo("ostaffs: ");print_r($ostaffs);//.$this->FSClientServiceDirectorId);//test
                
                //--- get FSAccountManager info.
                $os_select = $db->select();            
                $os_select->from("operations_staff")
                            ->where("id = ?", $this->FSAccountManagerId);
                $ostaffs = Core_Database::fetchAll($os_select);
                if(!empty($ostaffs))  
                {
                    $this->FSAccountManagerName = $ostaffs[0]['name'];
                    $this->FSAccountManagerPhone = $ostaffs[0]['phone'];
                    $this->FSAccountManagerEmail = $ostaffs[0]['email']; 
                }    
                else
                {
                    $this->FSAccountManagerName = "";
                    $this->FSAccountManagerPhone = "";
                    $this->FSAccountManagerEmail = "";
                }
            }
            else
            {
                $this->FSAccountManagerId = 0;
                $this->FSAccountManagerName = "";
                $this->FSAccountManagerPhone = "";
                $this->FSAccountManagerEmail = "";
                $this->FSClientServiceDirectorId = 0;
                $this->FSClientServiceDirectorName = "";
                $this->FSClientServiceDirectorPhone = "";
                $this->FSClientServiceDirectorEmail = "";
                $this->File1URL = "";
                $this->File2URL = "";
                $this->File3URL = "";                
            }
                        
            return $this;
        } else {
            $er = $errors->getErrors();
            if (empty($er)) $errors->addError(3, 'Login or password is not correct');

            $this->ContactName = NULL;
               $this->Email1      = NULL;
            $this->Email2      = NULL;
            return $this;
        }
    }
    
    public function getFieldDescription($field)
    {
        $result = $field;
        if($field=="CompanyName") $result="Company Name";
        else if($field=="EIN") $result="EIN";          
        else if($field=="FSClientServiceDirectorId") $result="FS Client Service Director Id";          
        else if($field=="FSAccountManagerId") $result="FS Account Manager Id";          
        else if($field=="CompanyDescription") $result="Company Description";          
        else if($field=="ContactName") $result="Contact Name";          
        else if($field=="BusinessAddress") $result="Business Address";          
        else if($field=="BusinessCity") $result="Business City";          
        else if($field=="BusinessState") $result="Business State";          
        else if($field=="BusinessZip") $result="Business Zip";          
        else if($field=="Country") $result="Country";          
        else if($field=="ContactPhone1") $result="Primary Phone";          
        else if($field=="ContactPhone2") $result="Secondary Phone";          
        else if($field=="Email1") $result="Primary Email";          
        else if($field=="Email2") $result="Secondary Email";          
        else if($field=="WebsiteURL") $result="Website";          
        else if($field=="Company_Logo") $result="Company Logo";          
        else if($field=="Password") $result="Password";          
        else $result = $field; 
        
        return $result;                
    }
    
    public function WriteAccessLogForClientFromAdmin($clientId,$adminUserName,$oldData,$writeLog=true)
    {
        //echo("<pre>");print_r($oldData);die();
        $this->WriteRestrictedAccessLogForClientFromAdmin($clientId,$adminUserName,$oldData,$writeLog);
        $this->WriteOpenedAccessLogForClientFromAdmin($clientId,$adminUserName,$oldData,$writeLog);        
    }

    public function WriteAccessForRepClientFromAdmin($clientId,$adminUserName,$oldData)
    {
        //--- Write Access Log for represent client
		//13990
        $this->WriteAccessLogForClientFromAdmin($clientId,$adminUserName,$oldData,true);         
		//End 13990
        //--- get rep. client record.
        $db = Core_Database::getInstance();    
        $select = $db->select();
        $select->from(Core_Database::TABLE_CLIENTS)
                ->where("ClientID = ?", $clientId);
        $repClient = Core_Database::fetchAll($select);  
        //--- 
        if(!empty($repClient)) 
        {
            $companyID = $repClient[0]['Company_ID'];
            //--- get access values of rep-client
            $select = $db->select();   
            $select->from("cliaccessctl")
                ->where("clientid = ?", $clientId);
            $repAccessValues = Core_Database::fetchAll($select);    
            //--- set access values and write to log for other clients in this company   
            $select = $db->select();
            $select->from(Core_Database::TABLE_CLIENTS)
                ->where("Company_ID = '$companyID'")
                ->where("ClientID != ?", $clientId);
            $otherClients = Core_Database::fetchAll($select);
            if(!empty($otherClients))
            {
                foreach($otherClients as $oClient)
                {
                    $oClientId = $oClient['ClientID'];    
                    //--- get old data of oClients
                    $select = $db->select();   
                    $select->from("cliaccessctl")
                        ->where("clientid = ?", $oClientId);
                    $oClientOldAccessValues = Core_Database::fetchAll($select);    
                    //--- update 
                    foreach($repAccessValues as $repAccessValue)
                    {
                         $attributeId = $repAccessValue['cliaccess_atrib_id'];
                         $accessValue = $repAccessValue['cliaccess_atrib_value'];
                         $this->editClientAccessValue($oClientId,$attributeId,$accessValue);                         
                    }
                    //--- Write to log
					//13990
                    $this->WriteAccessLogForClientFromAdmin($oClientId,$adminUserName,$oClientOldAccessValues,false);                             
					//End 13990
                }// end for
            }//end if
        }       
        
    }
    
	//13990
    public function WriteRestrictedAccessLogForClientFromAdmin($clientId,$adminUserName,$oldData,$writeLog=true)
    {
        //WriteAccessLogForClientFromAdmin_RestrictedAccess($clientId,$adminUsername);
        $objDateNow = new Zend_Date();
        $date = $objDateNow->toString('yyyy-MM-dd HH:mm:ss');
        //--- parents
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from("cliaccess_atribs");
        $select->where("parent_id=0");
        $select->order("id ASC");
        $parents = Core_Database::fetchAll($select);

        //***************** Restricted Access ************************/
        if($this->AreAllAreasRestricted($clientId,$oldData))
        {
			//13990
		if($writeLog){
                   $actionMsg = "Restricted Access to: All Areas";
                   $result = Core_Database::insert("cliprofile_action_log",
                                    array("clientid"=>$clientId,
                                    "username"=>$adminUserName,
                                    "logdate"=>$date,
                                    "action"=>"$actionMsg")
                            );             
                }
                return $result;     
			//End 13990
        }

        $actionMsg = "";
        for($i=0;$i<count($parents);$i++)
        {
            $parentLabel = $parents[$i]['label'];
            $parentId = $parents[$i]['id'];
            
            $haveChild = $this->DoesParentHaveChildren($parentId);
            //$actionMsg.= $i.','.$parentId.','.$parentLabel.'---';
            if($haveChild)
            {
                if( $this->AreAllChildrenRestricted($clientId,$parentId,$oldData))
                {
                    $actionMsg.= "All $parentLabel, ";
                }
                else
                {
                     $restrictedChildren = $this->GetAllRestrictedChildren($clientId,$parentId);
                     if(!empty($restrictedChildren))
                     {
                         for($ch=0;$ch<count($restrictedChildren);$ch++)
                         {
                             foreach($oldData as $oldrow)
                             {
                                    if( $restrictedChildren[$ch]['cliaccess_atrib_id']==$oldrow['cliaccess_atrib_id'] &&
                                        $restrictedChildren[$ch]['cliaccess_atrib_value']!=$oldrow['cliaccess_atrib_value'])                                            
                                    {
                                        $childLabel = $restrictedChildren[$ch]['label'];
                                        $actionMsg.= "$childLabel, "; 
                                        break;                                               
                                    }
                             }                             
                         }
                     }
                }
            }
            else // not have any child
            {
                $parentAccessValue=$this->GetAccessValueByClientAndAttribute($clientId,$parentId);                
                if($parentAccessValue==$this->_restrictedValue)
                {
                     foreach($oldData as $oldrow)
                     {
                         if($parentId==$oldrow['cliaccess_atrib_id'] &&
                            $parentAccessValue !=$oldrow['cliaccess_atrib_value'] 
                         )
                         {
                            $actionMsg.= "$parentLabel, ";
                            break;    
                         }
                     }                    
                }
            }
        } 
        $actionMsg=trim($actionMsg);
        if(!empty($actionMsg))
        {
            $actionMsg = substr($actionMsg,0,strlen($actionMsg)-1);
            $actionMsg = "Restricted Access to:<br/>".$actionMsg;   
            //13990
            if($writeLog){
                 $result = Core_Database::insert("cliprofile_action_log",
                                    array("clientid"=>$clientId,
                                    "username"=>$adminUserName,
                                    "logdate"=>$date,
                                    "action"=>"$actionMsg")
                 );             
            }        
            //end 13990
        }
    }
    
	//13990
    public function WriteOpenedAccessLogForClientFromAdmin($clientId,$adminUserName,$oldData,$writeLog=true)
    {
        //WriteAccessLogForClientFromAdmin_RestrictedAccess($clientId,$adminUsername);
        $objDateNow = new Zend_Date();
        $date = $objDateNow->toString('yyyy-MM-dd HH:mm:ss');
        //--- parents
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from("cliaccess_atribs");
        $select->where("parent_id=0");
        $select->order("id ASC");
        $parents = Core_Database::fetchAll($select);

        //***************** Opened Access ************************/
        if($this->AreAllAreasOpened($clientId))
        {
			//13990
		if($writeLog){
                         $actionMsg = "Opened Access to: All Areas";
                         $result = Core_Database::insert("cliprofile_action_log",
                                    array("clientid"=>$clientId,
                                    "username"=>$adminUserName,
                                    "logdate"=>$date,
                                    "action"=>"$actionMsg")
                        );             
		}
                return $result;     
			//End 13990
        }

        $actionMsg = "";
        for($i=0;$i<count($parents);$i++)
        {
            $parentLabel = $parents[$i]['label'];
            $parentId = $parents[$i]['id'];
            $haveChild = $this->DoesParentHaveChildren($parentId);
            //$actionMsg.= $i.','.$parentId.','.$parentLabel.'---';
            if($haveChild)
            {
                if( $this->AreAllChildrenOpened($clientId,$parentId,$oldData))
                {
                    $actionMsg.= "All $parentLabel, ";
                }
                else
                {
                     $openedChildren = $this->GetAllOpenedChildren($clientId,$parentId);
                     if(!empty($openedChildren))
                     {
                         for($ch=0;$ch<count($openedChildren);$ch++)
                         {
                             foreach($oldData as $oldrow)
                             {
                                    if( $openedChildren[$ch]['cliaccess_atrib_id']==$oldrow['cliaccess_atrib_id'] &&
                                        $openedChildren[$ch]['cliaccess_atrib_value']!=$oldrow['cliaccess_atrib_value'])                                            
                                    {
                                        $childLabel = $openedChildren[$ch]['label'];
                                        $actionMsg.= "$childLabel, "; 
                                        break;                                               
                                    }
                             }                             
                         }
                     }
                }
            }
            else // not have any child
            {
                $parentAccessValue=$this->GetAccessValueByClientAndAttribute($clientId,$parentId);
                if($parentAccessValue==$this->_openedValue)
                {
                     foreach($oldData as $oldrow)
                     {
                         if($parentId==$oldrow['cliaccess_atrib_id'] &&
                            $parentAccessValue !=$oldrow['cliaccess_atrib_value'] 
                         )
                         {
                            $actionMsg.= "$parentLabel, ";
                            break;    
                         }
                     }                                        
                }
            }
        } 
        $actionMsg=trim($actionMsg);
        if(!empty($actionMsg))
        {
            $actionMsg = substr($actionMsg,0,strlen($actionMsg)-1);
            $actionMsg = "Opened Access to:<br/>".$actionMsg;   
            //13900
            if($writeLog){
                 $result = Core_Database::insert("cliprofile_action_log",
                                    array("clientid"=>$clientId,
                                    "username"=>$adminUserName,
                                    "logdate"=>$date,
                                    "action"=>"$actionMsg")
                 );             
            }        
            //end 13900
        }
    }
    
    public function GetTotalOfAccessLogForClient($clientId)
    {
        $db = Core_Database::getInstance();
        $subquery = $db->select();
        $subquery->from("clients",array('Company_ID'));
        $subquery->where('ClientID = ?', $clientId);
        
        $query = $db->select();
        $query->from("cliprofile_action_log", array('COUNT(*)'));
        $query->where('clientid = ?', $clientId);
        $query->orWhere('company_id = ?', $subquery);
        $result = Core_Database::fetchAll($query);
        
        if(empty($result))
            $count = 0;
        else
            $count = $result[0]["COUNT(*)"];           
        
        return $count;    
    }
    
    public function GetListOfAccessLogForClient($clientId, $limit=0,$offset=0) 
    {
        
        $db = Core_Database::getInstance();
        $subquery = $db->select();
        $subquery->from("clients",array('Company_ID'));
        $subquery->where('ClientID = ?', $clientId);
        
        $query = $db->select();
        $query->from("cliprofile_action_log");
        $query->where('clientid = ?', $clientId);
        $query->orWhere('company_id = ?', $subquery);
        //die("avavaa");
        $query->order('logdate DESC');
        if ($limit > 0 && $offset >= 0)
        {
            $query->limit($limit, $offset);
        }
        $result = Core_Database::fetchAll($query);
        //die("avavaa-1");
            
        return $result;
                    
    }
    
    public function SetAccessDefaultValuesForClient($clientId)
    {        
        //--- represent 
        $client = $this->loadById($clientId);
        $companyID = $client->Company_ID;
        $repClientID = $this->getRepClientIDforCompany($companyID);
        
        $repAccessValues = null;
        if(!empty($repClientID))
        {
            $db = Core_Database::getInstance();  
            $select = $db->select();
            $select->from("cliaccessctl");
            $select->where("clientid=?",$repClientID);
            $repAccessValues = Core_Database::fetchAll($select);
        }
        //--- this client 
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from("cliaccessctl");
        $select->where("clientid=?",$clientId);
        $cliAccessCtl = Core_Database::fetchAll($select);
        
        if(empty($cliAccessCtl))
        {
            $db = Core_Database::getInstance();  
            $select = $db->select();
            $select->from("cliaccess_atribs");
            $select->order("presentation_order ASC");
            $access_attribs = Core_Database::fetchAll($select);

            if(!empty($repAccessValues))
            {
                foreach($repAccessValues as $repAccessValue)                
                {
                    $attID = $repAccessValue['cliaccess_atrib_id'];
                    $attValue = $repAccessValue['cliaccess_atrib_value'];
                    $result = Core_Database::insert("cliaccessctl",
                            array("clientid"=>$clientId,
                            "cliaccess_atrib_id"=>$attID,
                            "cliaccess_atrib_value"=>$attValue)
                    );    

                }
            }
            else if(!empty($access_attribs))
            {
                for($i=0;$i<count($access_attribs);$i++)
                {
                    $attID = $access_attribs[$i]['id'];
                    $attValue = $this->_openedValue;
                    $result = Core_Database::insert("cliaccessctl",
                            array("clientid"=>$clientId,
                            "cliaccess_atrib_id"=>$attID,
                            "cliaccess_atrib_value"=>$attValue)
                    ); 
                }
            }
        }
    }
    //--- 13457
    public function GetAccessValueByClientAndAttribute($clientId,$attributeId)
    {
        $client = $this->loadById($clientId);
        $companyID = $client->Company_ID;
        //echo("<br/>companyID: $companyID");
        return $this->GetAccessValueByCompanyAndAttribute($companyID,$attributeId);        
    }    
    
    public function uploadFileForClient($cleint_id, $fName, $fData) {
        $error = Core_Api_Error::getInstance();
        $error->resetErrors();
        $fName = ltrim(trim($fName), '/');

        $file = new Core_File();

        $uploadStatus = $file->uploadFile($fName, $fData, S3_CLIENTS_LOGO_DIR, true);
        if (!$uploadStatus) {
            $error->addError(6, 'Upload file ' . $fName . ' failed');
            return API_Response::fail();
        }


        $client = $this->loadById($cleint_id);
        if ($error->getErrors())
            return API_Response::fail();
        if (!empty($client->Company_Logo)) { // if it was uploaded file
            $file->removeFile($client->Company_Logo, S3_CLIENTS_LOGO_DIR); // Remove old file
        }

        $client = $this->loadById($cleint_id);
        //$client->Company_Logo = $fName;
        return $client->save(array('Company_Logo' => $fName));
    }
    //--- 13457
    public function getRepClientforCompany($companyId,$onlyOne=0)
    {
        $db = Core_Database::getInstance();
        
        $select = $db->select();
        $select->from(Core_Database::TABLE_CLIENTS);
        $select->where("AccountEnabled = ?", 1);
        $select->where("Company_ID = '$companyId'");    
        $result = Core_Database::fetchAll($select);  
        if($onlyOne)
        {
            if(empty($result)) return null;
            else return $result[0];
        }
        else
        {
            return $result;     
        }    
    }
    
    
    public function getdbVersion()
    {
        $dbVersion = 'full';
        if(isset($_SESSION))
        {
			try {
			$authData = array('login'=>$_SESSION['Auth_User']['login'], 'password'=>$_SESSION['Auth_User']['password']);
			$user = new self();
			$user->load($authData);
			$client_id = $user->getClientId();
			$settings = Core_DashboardSettings::getSettings($client_id);
            $dbVersion = $settings['view'] == Core_DashboardSettings::VIEW_LITE ? 'lite' : 'full';			
			} catch (Exception $e) {}
        }
        return $dbVersion;
    }
    
    /********************** Private Functions ************************************/
    private function AreAllAreasRestricted($clientId,$oldData)
    {
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from("cliaccessctl");
        $select->where("clientid=?",$clientId);
        $result = Core_Database::fetchAll($select);
        
        if(empty($result)) 
        {
            return false;
        }
        else 
        {
            foreach($result as $aarow)
            {
                if($aarow['cliaccess_atrib_value']==$this->_openedValue){return false;}
                foreach($oldData as $oldrow)
                {
                    if( $aarow['cliaccess_atrib_id']==$oldrow['cliaccess_atrib_id'] &&
                        $aarow['cliaccess_atrib_value']==$oldrow['cliaccess_atrib_value'])
                    {return false;}
                }                
            }
            return true;        
        }
    }
    
    private function AreAllAreasOpened($clientId,$oldData)
    {
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from("cliaccessctl");
        $select->where("clientid=?",$clientId);
        $result = Core_Database::fetchAll($select);
        
        if(empty($result))
        {
            return false;
        } 
        else
        { 
            foreach($result as $aarow)
            {
                if($aarow['cliaccess_atrib_value']==$this->_restrictedValue){return false;}
                foreach($oldData as $oldrow)
                {
                    if( $aarow['cliaccess_atrib_id']==$oldrow['cliaccess_atrib_id'] &&
                        $aarow['cliaccess_atrib_value']==$oldrow['cliaccess_atrib_value'])
                    {return false;}
                }                
            }
            return true;        
        }
    }
    
    private function AreAllChildrenRestricted($clientId,$parentId,$oldData)
    {
        $ret = false;
        $db = Core_Database::getInstance();  
        $select = $db->select()
             ->from(array('ac' => 'cliaccessctl'),
                    array('clientid', 'cliaccess_atrib_id','cliaccess_atrib_value'))
             ->join(array('atr' => 'cliaccess_atribs'),
                    'atr.id = ac.cliaccess_atrib_id')
             ->where("ac.clientid = ?",$clientId)
             ->where("atr.parent_id = ?",$parentId);
                    
        $result = Core_Database::fetchAll($select);
        if(empty($result))
        {            
            return false;
        }        
        else
        {
            foreach($result as $aarow)
            {
                if($aarow['cliaccess_atrib_value']==$this->_openedValue){return false;}
                foreach($oldData as $oldrow)
                {
                    if( $aarow['cliaccess_atrib_id']==$oldrow['cliaccess_atrib_id'] &&
                        $aarow['cliaccess_atrib_value']==$oldrow['cliaccess_atrib_value'])
                    {return false;}
                }                
            }
            return true;        
        }
    }
    
    private function AreAllChildrenOpened($clientId,$parentId,$oldData)
    {
        $db = Core_Database::getInstance();  
        $select = $db->select()
             ->from(array('ac' => 'cliaccessctl'),
                    array('clientid', 'cliaccess_atrib_id','cliaccess_atrib_value'))
             ->join(array('atr' => 'cliaccess_atribs'),
                    'atr.id = ac.cliaccess_atrib_id')
             ->where("ac.clientid = ?",$clientId)
             ->where("atr.parent_id = ?",$parentId);
                    
        $result = Core_Database::fetchAll($select);
        
        if(empty($result)) 
        {
            return false;
        }
        else 
        {
            foreach($result as $aarow)
            {
                if($aarow['cliaccess_atrib_value']==$this->_restrictedValue){return false;}
                foreach($oldData as $oldrow)
                {
                    if( $aarow['cliaccess_atrib_id']==$oldrow['cliaccess_atrib_id'] &&
                        $aarow['cliaccess_atrib_value']==$oldrow['cliaccess_atrib_value'])
                    {return false;}
                }                
            }
            return true;        
        }       
    }
    
    private function GetAllRestrictedChildren($clientId,$parentId) 
    {
        $db = Core_Database::getInstance();  
        $select = $db->select()
             ->from(array('ac' => 'cliaccessctl'),
                    array('clientid', 'cliaccess_atrib_id','cliaccess_atrib_value'))
             ->join(array('atr' => 'cliaccess_atribs'),
                    'atr.id = ac.cliaccess_atrib_id')
             ->where("ac.clientid = ?",$clientId)
             ->where("atr.parent_id = ?",$parentId)
             ->where("ac.cliaccess_atrib_value=?",$this->_restrictedValue);                           
                    
        $result = Core_Database::fetchAll($select);
        return $result;        
    }

    private function GetAllOpenedChildren($clientId,$parentId) 
    {
        $db = Core_Database::getInstance();  
        $select = $db->select()
             ->from(array('ac' => 'cliaccessctl'),
                    array('clientid', 'cliaccess_atrib_id','cliaccess_atrib_value'))
             ->join(array('atr' => 'cliaccess_atribs'),
                    'atr.id = ac.cliaccess_atrib_id')
             ->where("ac.clientid = ?",$clientId)
             ->where("atr.parent_id = ?",$parentId)
             ->where("ac.cliaccess_atrib_value=?",$this->_openedValue);                           
                    
        $result = Core_Database::fetchAll($select);
        return $result;        
    }
    
    
    private function DoesParentHaveChildren($parentId)
    {        
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from("cliaccess_atribs");
        $select->where("parent_id=?",$parentId);
        $result = Core_Database::fetchAll($select);
        
        if(empty($result)) return false;
        else return true;        
    }
    
    //2012-03-29
    public function sendEmailToFS($clientId,$techId)
    {
        $db = Core_Database::getInstance();  
        //--- technician info.
        $query = $db->select();
        $query->from(Core_Database::TABLE_TECH_BANK_INFO); 
        $query->where("TechID = '$techId'");
        $techInfo = Core_Database::fetchAll($query);
        $techFName="";$techLName="";
        if(!empty($techInfo))
        {
            $techFName=$techInfo[0]['FirstName'];
            $techLName=$techInfo[0]['LastName'];            
        }            
        //---  client info
        $query = $db->select();
        $query->from("clients"); 
        $query->where("ClientID = ?", $clientId);
        $clientInfo = Core_Database::fetchAll($query);  
        $cname="";$companyId="";$companyName="";$cemail="";$cphone="";$address="";$citystateZip="";
        //14110
        $client = new Core_Api_Class();
        $auth = new Zend_Session_Namespace('Auth_User');
        $isPM = $client->isGPMByUserName($auth->login);
        if(!empty($clientInfo))
        {
            if($isPM)
            {
                $companyId = $_SESSION["UserObj"][0]["ContextCompany"];
                $companyName = $_SESSION["UserObj"][0]["ContextCompanyName"];
            }
            else 
            {
                $companyId=$clientInfo[0]['Company_ID'];
                $companyName=$clientInfo[0]['CompanyName'];
            }
            $cname=$clientInfo[0]['ContactName'];
            $cemail=$clientInfo[0]['Email1'];
            $cphone=$clientInfo[0]['ContactPhone1'];
            $address=$clientInfo[0]['BusinessAddress'];
            $city=$clientInfo[0]['BusinessCity'];
            $state=$clientInfo[0]['BusinessState'];
            $zip=$clientInfo[0]['BusinessZip'];
            $citystateZip="";
            if(!empty($city)&&!empty($state)&&!empty($zip))
                $citystateZip= $city.", ".$state." ".$zip;                          
        }     
        //--- clientExt
        $clientExt = $this->getClientExt($clientId);
        $csdId = "";
        $amId="";
        $toEmail = ""; 
        $ccEmail = "";
         
        if(!empty($clientExt))
        {
           $csdId=  $clientExt[0]['FSClientServiceDirectorId'];
           $amId=  $clientExt[0]['FSAccountManagerId'];
           $common = new Core_Api_CommonClass;
           $CSD = $common->getOpeStaffById($csdId);
           $AM = $common->getOpeStaffById($amId);
           
            $toEmail = 'Accounting@fieldsolutions.com';
            
            if(!empty($CSD))
            {
                $ccEmail = $CSD['email'];
            }
            if(!empty($AM))
            {
                $ccEmail = $ccEmail .', '.$AM['email'];
            }
        }
        
        //--- email
        $fromName = "$cname";
        $fromEmail = "$cemail";
        $subject = "\n $companyName COI Request";
        $body = "\n $cname is requesting a Certificate of Insurance for $companyName.";
        $body.= "\nClient Name: $cname";
        $body.= "\nEmail: $cemail";
        $body.= "\nPhone: $cphone";
        $body.="\nCompany: $companyId - $companyName ";
        $mail = new Core_Mail();
        $mail->setBodyText($body);
        $mail->setFromName($fromName);
        $mail->setFromEmail($fromEmail);
        $mail->setToEmail($toEmail);
        $mail->setCcEmail($ccEmail);
        $mail->setSubject($subject);
        $mail->send();
        //end 14110  
    }
    //*** 13334
    public function GetDeductPercent_FSSFeeFrTech($companyId)
    {
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from("clients_tech_deduct");
        $select->where("Company_ID=?",$companyId);
        $result = Core_Database::fetchAll($select);
        
        if(empty($result)) return 0;
        else return $result[0]['PcntDeductPercent'];        
    }
    
    public function SaveDeductPercent_FSSFeeFrTech($companyId,$deductPercent,$updateBy='',$client_id='')
    {
         $db = Core_Database::getInstance();  
         $select = $db->select();
         $select->from("clients_tech_deduct");
         $select->where("Company_ID = ?",$companyId);         
         $records = Core_Database::fetchAll($select); 
        
         $actionMsg = "";
         if(empty($records))
         {
             $result = Core_Database::insert("clients_tech_deduct",
                array("Company_ID"=>$companyId,"PcntDeductPercent"=>$deductPercent)
             ); 
             $actionMsg = "Edited Client Profile:";
             $actionMsg.="<br/>Changed Tech Deduction ";
             $actionMsg.="<br/>To: ".$deductPercent;
             
         } else {                 
             $criteria = "Company_ID = '$companyId'";
             $oldDeductPercent = $records[0]['PcntDeductPercent'];
             if($deductPercent != $oldDeductPercent)
             {
                 $result = Core_Database::update("clients_tech_deduct",
                    array("PcntDeductPercent"=>$deductPercent), $criteria 
                 ); 
             $actionMsg = "Edited Client Profile:";
             $actionMsg.="<br/>Changed Tech Deduction from: ".$oldDeductPercent;
             $actionMsg.="<br/>To: ".$deductPercent;
         }
         }
		 
         if($actionMsg!="")
         {
             $result = Core_Database::insert("cliprofile_action_log",
                        array("company_id"=>$companyId,
                        "username"=>$updateBy,
                        "logdate"=>date("Y-m-d H:i:s"),
                        "action"=>"$actionMsg",
                        "clientid"=>$client_id
                        )
                     
            );     
         }
		 
		$db->update(Core_Database::TABLE_PROJECTS, array("PcntDeduct" => ($deductPercent > 0 ? 1 : 0), "PcntDeductPercent"=>$deductPercent), $db->quoteInto("Project_Name = 'Default Project' AND Project_Company_ID = ?", $companyId));
    }
    //*** 13334 end
    //--- 13376
    public function getClientCompanyAdditionalFields($companyId)
    {
         $db = Core_Database::getInstance();  
         $select = $db->select();
         $select->from("clicompanies_additional_fields");
         $select->where("companyid = '$companyId'");         
         $records = Core_Database::fetchAll($select); 
         if(!empty($records)) return $records[0];
         else return null;         
}
    
    public function saveClientCompanyAdditionalFields($companyId,$fieldvals)
    {
        $current = $this->getClientCompanyAdditionalFields($companyId);
        if(!empty($current))
        {
            $criteria = "companyid='$companyId'";
            $result = Core_Database::update("clicompanies_additional_fields",$fieldvals,$criteria);
        }   
        else
        {
            $fieldvals['companyid'] = $companyId;
            $result = Core_Database::insert("clicompanies_additional_fields",$fieldvals);
        }     
        return $result;
    }
    public function getClientCompanyBCats($companyId)
    {
         $db = Core_Database::getInstance();  
         $select = $db->select();
         $select->from("clicompanies_bcats");
         $select->where("companyid = '$companyId'"); 
         $select->order("bcattype desc");
         $select->order("bcatname asc");
         $records = Core_Database::fetchAll($select);  
         return $records;              
    }
    
    public function getClientCompanyBCats_insertIfNotExists($companyId)
    {
        if(empty($companyId))  return null;
        
        if($this->existsCompanyBCats($companyId)==0)
        {
            $this->insertCompanyDefaultFixedBCats($companyId);            
        }
        
        $records = $this->getClientCompanyBCats($companyId);
        return $records;
    }    
    
    private function existsCompanyBCats($companyId)
    {
         $db = Core_Database::getInstance();  
         $select = $db->select();
         $select->from("clicompanies_bcats");
         $select->where("companyid = '$companyId'"); 
         $records = Core_Database::fetchAll($select);  
         if(!empty($records)) return 1;
         else return 0;              
    }
    
    private function insertCompanyDefaultFixedBCats($companyId)
    {
        if(empty($companyId)) return;
        
        $fieldvals = array("companyid"=>$companyId,"bcatdesc"=>"","ischecked"=>0,"reimbursable_thru_fs"=>0,"require_doc_upl"=>0,"bcattype"=>"fixed");   
            
        //$fieldvals["bcatname"] = "Airfare";
        $fieldvals["bcatname"] = "Airfare / Train Tickets";
        $result = Core_Database::insert("clicompanies_bcats",$fieldvals);           
        //$fieldvals["bcatname"] = "Train/Taxi/Shuttle/Tolls";
        $fieldvals["bcatname"] = "Bus / Taxi / Subway";
        $result = Core_Database::insert("clicompanies_bcats",$fieldvals);  

        $fieldvals["bcatname"] = "Entertainment";
        $result = Core_Database::insert("clicompanies_bcats",$fieldvals);     
        
        //$fieldvals["bcatname"] = "Hotel";
        $fieldvals["bcatname"] = "Hotel / Lodging";
        $result = Core_Database::insert("clicompanies_bcats",$fieldvals);     
        
        //$fieldvals["bcatname"] = "Miscellaneous Supplies";
        $fieldvals["bcatname"] = "Materials / Supplies";
        $result = Core_Database::insert("clicompanies_bcats",$fieldvals);     
        
        $fieldvals["bcatname"] = "Meals";
        $result = Core_Database::insert("clicompanies_bcats",$fieldvals);     

        $fieldvals["bcatname"] = "Miscellaneous";
        $result = Core_Database::insert("clicompanies_bcats",$fieldvals);     

        //$fieldvals["bcatname"] = "Parking";
        $fieldvals["bcatname"] = "Parking / Tolls";
        $result = Core_Database::insert("clicompanies_bcats",$fieldvals);     
        
        //$fieldvals["bcatname"] = "Gasoline";
        //$result = Core_Database::insert("clicompanies_bcats",$fieldvals);   

        $fieldvals["bcatname"] = "Per Diem";
        $result = Core_Database::insert("clicompanies_bcats",$fieldvals);     
           
        $fieldvals["bcatname"] = "Personal Car Mileage";
        $result = Core_Database::insert("clicompanies_bcats",$fieldvals);        
        //$fieldvals["bcatname"] = "Postage / Freight";
        $fieldvals["bcatname"] = "Postage / Shipping / Freight";
        $result = Core_Database::insert("clicompanies_bcats",$fieldvals);        
        //$fieldvals["bcatname"] = "Car Rental";
        $fieldvals["bcatname"] = "Rental Vehicle / Gas";
        $result = Core_Database::insert("clicompanies_bcats",$fieldvals);     
        
        return $result;     
    }
    
    public function saveClientCompanyBCat($bcatid,$fieldvals)
    {
        if(!empty($bcatid))
        {
            $criteria = "bcatid='$bcatid'";
            unset($fieldvals['bcatid']);
            $result = Core_Database::update("clicompanies_bcats",$fieldvals,$criteria);
        }
        else if($fieldvals['bcattype'] == 'custom')
        {
            unset($fieldvals['bcatid']);
            $result = Core_Database::insert("clicompanies_bcats",$fieldvals);
        }              
        return $result;  
    }
    
    public function enableExpenseReporting($companyId)
    {
        $current = $this->getClientCompanyAdditionalFields($companyId);
        if(empty($current)) return 0;
        else return $current['enable_expense_reporting'];
    }     

    public function getAccountManagerInfo($clientId)
    {
        if(empty($clientId)) return null;
        $clientExtInfo = $this->getClientExt($clientId);
        if(empty($clientExtInfo)) return null;
        $accountManagerId = $clientExtInfo[0]['FSAccountManagerId'];
        $commonApi = new Core_Api_CommonClass();
        $accountManagerInfo = $commonApi->getOpeStaffById($accountManagerId);
        return $accountManagerInfo;
    }
    //--- end 376
    //--- 13411
    /*$onlyEnableBcat = 0; 1 */
    public function getFixedBCatNames_ByCompany($companyID,$onlyEnableBcat=0)
    {
        
        if(empty($companyID))  return null;        
        if($this->existsCompanyBCats($companyID)==0)
        {
            $this->insertCompanyDefaultFixedBCats($companyID);            
        }
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from("clicompanies_bcats");
        $select->where("companyid = '$companyID'"); 
        $select->where("bcattype = 'fixed'"); 
        if($onlyEnableBcat==1) $select->where("ischecked = 1"); 
        $select->order("bcatname asc");
        $records = Core_Database::fetchAll($select); 
        if(empty($records)) return null;
        else 
        {
            $bcatnames = array();
            foreach($records as $rec)
            {
                $bcatnames[]= $rec['bcatname'];
            }
            return $bcatnames;
        }
        
    }
    
    //--- 13411
    public function getCustomBCatNames_ByCompany($companyID)
    {
        if(empty($companyID))  return null;                
        if($this->existsCompanyBCats($companyID)==0)        
        {
            $this->insertCompanyDefaultFixedBCats($companyID);            
        }
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from(array('pbcat'=>'projects_bcats'),array('bcatname'));
        $select->join(array('prj'=>'projects'),
                    'prj.Project_ID = pbcat.projectid',
                    array()
        );        
        $select->where("prj.Project_Company_ID = '$companyID'"); 
        $select->where("pbcat.bcattype = 'custom'"); 
        $select->where("pbcat.ischecked =1");         
        $select->order("pbcat.bcatname asc");
        $select->distinct();
        $records = Core_Database::fetchAll($select); 
        if(empty($records)) return null;
        else 
        {
            $bcatnames = array();
            foreach($records as $rec)
            {
                $bcatnames[]= $rec['bcatname'];
            }
            return $bcatnames;
        }        
    }


    public function add_profile_action_log($clientId,$adminUserName,$actionMsg)
    {
        $result = Core_Database::insert("cliprofile_action_log",
                                    array("clientid"=>$clientId,
                                    "username"=>"$adminUserName",
                                    "logdate"=>date("Y-m-d H:i:s"),
                                    "action"=>"$actionMsg")
                 );             
         return $result;     
    }
    
    //--- 13457
    public function setAllAccessRestricted_InNDaysNotAssignedRule($companyID)
    {
        if(empty($companyID)) return;
        $apiUser = new Core_Api_User();
        $additionValues= $apiUser->getClientCompanyAdditionalFields($companyID);
        if(!empty($additionValues))
        {
            $enableAuto21DayRestricTool = $additionValues['EnableAuto21DayRestricTool'];
            if(empty($enableAuto21DayRestricTool)) return;
        }        
        //---
        $Ndays = 21;
        $restricted = 2;
        $opened = 1;
        $NDays_dateTime = new DateTime();
        $NDays_dateTime->modify("-$Ndays days");
        
        
        //--- get before date for counting '21-Days Rule'
        $openedDate = $this->getLastOpenedDate($companyID);
		if (!empty($openedDate)) {
			// skip if access opened in last 21 days
			$openedData_dateTime = new DateTime($openedDate);
			if ($openedData_dateTime > $NDays_dateTime) return;
		}
      
      $today = new DateTime();
        $companyAdditionalInfo = $apiUser->getClientCompanyAdditionalFields($companyID);
        $beginDate = $companyAdditionalInfo['BeginDateForCountingNDaysRule'];        
        $beginDateBeforeDays = -1;
        if(!empty($beginDate))
        {
            $tmpDate_1 = new DateTime($beginDate);
            $diff_1 = $tmpDate_1->diff($today);
            $beginDateBeforeDays = $diff_1->days;            
        }
        $remainDays_1 = 0;
        if($beginDateBeforeDays!=-1 && $beginDateBeforeDays<21)
        {            
            $remainDays_1 = 21-$beginDateBeforeDays;
        }
        
        $lastAssignedDate = API_Timestamp::getLastWinAssignedTimestamp_ByCompany($companyID);
        $lastAssignedBeforeDays = -1;                
        if(!empty($lastAssignedDate)) 
        {
            $tmpDate = new DateTime($lastAssignedDate);
            $diff = $tmpDate->diff($today);
            $lastAssignedBeforeDays = $diff->days;
        }
        $remainDays_2 = 0;
        if($lastAssignedBeforeDays !=-1 && $lastAssignedBeforeDays < 21)
        {
            $remainDays_2 = 21-$lastAssignedBeforeDays;
        }

        $remainDays = $remainDays_1 <= $remainDays_2? $remainDays_2: $remainDays_1;
         
        
        //--- make restriction
        if($remainDays==0)
        {
            //--- clientID_list
            $db = Core_Database::getInstance();  
            $select = $db->select();
            $select->from(Core_Database::TABLE_CLIENTS,array('ClientID'));
            $select->where("Company_ID='$companyID'");
            $records = Core_Database::fetchAll($select); 
            if(empty($records)) return;

            $clientIDs = "";
            foreach($records as $rec)
            {
                if(!empty($clientIDs)) $clientIDs.=",";
                
                $clientIDs .= $rec['ClientID'];
            }

            //--- are all accesses restricted ?
            $areAllAccessRestrict = "";
            $db = Core_Database::getInstance();  
            $select = $db->select();
            $select->from("cliaccessctl",array('cliaccess_atrib_id'));
            $select->where("clientid IN ($clientIDs)");
            $select->where("cliaccess_atrib_value = '$opened'");
            $records = Core_Database::fetchAll($select); 
            
          
           if(empty($records)&& $openedData_dateTime > $NDays_dateTime) $areAllAccessRestrict = 1;//13995   
            else $areAllAccessRestrict = 0;
            
            //--- update
            $criteria = "clientid IN ($clientIDs)"; 
            $result = Core_Database::update("cliaccessctl",
                    array("cliaccess_atrib_value"=>$restricted),
                    $criteria
            );            
            //--- write to log
            $clientID = $this->getRepClientIDforCompany($companyID);
			$restrictedDate = $this->getLastRestrictedDate($companyID);
            $openedDate = $this->getLastOpenedDate($companyID);
           
           if(empty($restrictedDate) && ($openedDate > $restrictedDate))//13995
              
            {
                $this->add_profile_action_log($clientID,"fs_system","Restricted Access to: All Areas");
            }
        }
        
    }
    
    public function getRepClientIDforCompany($companyId)
    {
        $db = Core_Database::getInstance();
        
        $select = $db->select();
        $select->from(Core_Database::TABLE_CLIENTS);
        $select->where("AccountEnabled = ?", 1);
        $select->where("Company_ID = '$companyId'");
        $select->order("ClientID ASC");
        
        $result = Core_Database::fetchAll($select);  
        if(empty($result)) return 0;
        else return $result[0]['ClientID'];         
    }
    
    //--- 13457
    public function GetAccessValueByCompanyAndAttribute($companyID,$attributeId)
    {        
        $clientId = $this->getRepClientIDforCompany($companyID);
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from("cliaccessctl");
        $select->where("clientid=?",$clientId);
        $select->where("cliaccess_atrib_id=?",$attributeId);        
        $result = Core_Database::fetchAll($select);
        
        if(!empty($result))
        {
            return  $result[0]['cliaccess_atrib_value'];
        }
        else
        {
            return '';
        }        
    }
    //--- 13457
    public function ResetAccessAndMoreWhenPublishing($companyID,$winNum,$userLogin)
    {
        //--- update 'BeginDateForCountingNDaysRule' field
        $fieldvals=array('BeginDateForCountingNDaysRule'=>date("Y-m-d"));
        $this->saveClientCompanyAdditionalFields($companyID,$fieldvals);
        //--- Remove all restriction
        $strClientIDs = $this->getClientIDs_InCompany($companyID);
        if(empty($strClientIDs)) return;
        
        $openedValue=1;            
        $criteria = "clientid IN ($strClientIDs)"; 
        $result = Core_Database::update("cliaccessctl",
                array("cliaccess_atrib_value"=>$openedValue),
                $criteria
        );            
                
        $clientID = $this->getRepClientIDforCompany($companyID);
        
        //--- write log        
        $this->add_profile_action_log($clientID,$userLogin,"Opened Access to: All Areas");
    }
    //--- 13457
    public function getClientIDs_InCompany($companyID)
    {        
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from(Core_Database::TABLE_CLIENTS,array('ClientID'));
        $select->where("Company_ID='$companyID'");
        $records = Core_Database::fetchAll($select); 
        if(empty($records)) return "";
        $clientIDs = "";
        foreach($records as $rec)
        {
            if(!empty($clientIDs)) $clientIDs.=",";
            
            $clientIDs .= $rec['ClientID'];
        }
        return $clientIDs;
    }
    //--- 13622
    public function getClientIDArray_InCompany($companyID)
    {        
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from(Core_Database::TABLE_CLIENTS,array('ClientID'));
        $select->where("Company_ID='$companyID'");
        $records = Core_Database::fetchAll($select); 
        
        $IDarray = array();
        if(empty($records)) return array();
        
        $IDarray = array();
        foreach($records as $rec)
        {            
            $IDarray[] = $rec['ClientID'];
        }
        return $IDarray;
    }
    //--- 13457
    public function isClientCompanyRestricted($companyID)
    {
        if(empty($companyID)) return false;
        $clientIDs = $this->getClientIDs_InCompany($companyID);
        if(empty($clientIDs)) return false;

        //---
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from("cliaccessctl",array('cliaccess_atrib_id'));
        $select->where("clientid IN ($clientIDs)");
        $records = Core_Database::fetchAll($select); 
        if(empty($records)) return false;
        
        //--- 
        $opened = $this->_openedValue;
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from("cliaccessctl",array('cliaccess_atrib_id'));
        $select->where("clientid IN ($clientIDs)");
        $select->where("cliaccess_atrib_value = '$opened'");
        $records = Core_Database::fetchAll($select); 
        if(empty($records)) return true;
        else return false;        
    }
    //--- 13457
    
    //622
    public function getSystemStatus($companyID)
    {
        $opened = $this->_openedValue;
        $restricted = $this->_restrictedValue;
        //----
        if (empty($companyID)) return false;
        $repClientID = $this->getRepClientIDforCompany($companyID);    
                    
        $hasOneOpened = 0;
        $hasOneRestrictived = 0;
        
        $db = Core_Database::getInstance();
        $select = $db->select();
        $select->from("cliaccessctl", array('cliaccess_atrib_id'));
        $select->where("clientid = '$repClientID'");
        $select->where("cliaccess_atrib_value = '$opened'");
        $records = Core_Database::fetchAll($select);
        if (!empty($records))
        {
            $hasOneOpened = 1;
        }
        
        $select = $db->select();
        $select->from("cliaccessctl", array('cliaccess_atrib_id'));
        $select->where("clientid = '$repClientID' ");
        $select->where("cliaccess_atrib_value = '$restricted'");
        $records = Core_Database::fetchAll($select);
        if (!empty($records))
        {
            $hasOneRestrictived = 1;
        }     
       
        if ($hasOneOpened > 0 && $hasOneRestrictived > 0)
        {
            return self::SYSTEM_STATUS_ID_PARTIAL_RESTRICTION;
        } else if ($hasOneOpened > 0 && $hasOneRestrictived == 0)
        {
            return self::SYSTEM_STATUS_ID_ACTIVE;
        } else if ($hasOneOpened == 0 && $hasOneRestrictived > 0)
        {
            return self::SYSTEM_STATUS_ID_RESTRICT;
        }else if ($hasOneOpened == 0 && $hasOneRestrictived == 0)
        {
            return self::SYSTEM_STATUS_ID_ACTIVE;
        }else//576
        {
            return self::SYSTEM_STATUS_ID_DEACTIVED;
        }
        
    }
    //end 622
    public function getLastRestrictedDate($companyID)
    {
        if(empty($companyID)) return '';
        $clientIDs = $this->getClientIDs_InCompany($companyID);
        if(empty($clientIDs)) return '';

        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from("cliprofile_action_log");
        $select->where("clientid IN ($clientIDs)");
        $select->where("action = 'Restricted Access to: All Areas'");
        $select->order("logdate DESC");
        $records = Core_Database::fetchAll($select); 
        if(empty($records)) return '';
        else return $records[0]['logdate'];
    }
    
    //--- 13457 Opened Access
    public function getLastOpenedDate($companyID)
    {
        if(empty($companyID)) return '';
        $clientIDs = $this->getClientIDs_InCompany($companyID);
        if(empty($clientIDs)) return '';

        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from("cliprofile_action_log");
        $select->where("clientid IN ($clientIDs)");
        $select->where("action LIKE 'Opened Access%'");
        $select->order("logdate DESC");
        $records = Core_Database::fetchAll($select); 
        if(empty($records)) return '';
        else return $records[0]['logdate'];
    }
    //--- 13571
    public function setAllAccessOpened($companyID)
    {
        if(empty($companyID)) return;        
        //--- clientID_list
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from(Core_Database::TABLE_CLIENTS,array('ClientID'));
        $select->where("Company_ID='$companyID'");
        $records = Core_Database::fetchAll($select); 
        if(empty($records)) return;
        $clientIDs = "";
        foreach($records as $rec)
        {
            if(!empty($clientIDs)) $clientIDs.=",";
            
            $clientIDs .= $rec['ClientID'];
        }
        //--- update
        $restricted = 2;
        $opened = 1;
        $criteria = "clientid IN ($clientIDs)"; 
        $result = Core_Database::update("cliaccessctl",
                array("cliaccess_atrib_value"=>$opened),
                $criteria
        );            
        //--- write to log
        $clientID = $this->getRepClientIDforCompany($companyID);
        $restrictedDate = $this->getLastRestrictedDate($companyID);
        $openedDate = $this->getLastOpenedDate($companyID);
        if(empty($openedDate) || ($restrictedDate > $openedDate))
        {
            $this->add_profile_action_log($clientID,"fs_system","Opened Access to: All Areas");
        }
    }
    
    //--- 13777
    public function isDuplicated_Username($userName,$companyID='')
    {
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from(Core_Database::TABLE_CLIENTS,array('ClientID'));
        $select->where("UserName='$userName'");
        if(!empty($companyID)){
            $select->where("Company_ID = ? ",$companyID);
        }
        $records = Core_Database::fetchAll($select); 
        
        if(!empty($records) && count($records)>0) return 1;
        else return 0;
    }

    //--- 13713
    /*
    *   $saveToWOsOption:   1: save all projects and all WOs in the company
    */
    public function saveClientTechnicianWorkControls($companyID,
        $AllowDeVryInternstoObserveWork,
        $AllowDeVryInternstoPerformField,
        $saveToWOsOption)
    {
            //--- save data to client_ext
            $clientID = $this->getRepClientIDforCompany($companyID);
            $clientExt = $this->get_ClientExt($clientID); 
            $oldAllowDeVryInternstoObserveWork = $clientExt['AllowDeVryInternstoObserveWork'];
            $updateFields = array();
            if(empty($clientExt))
            {
                $updateFields['ClientId']=$clientID;
                $updateFields['FSClientServiceDirectorId']=0;
                $updateFields['FSAccountManagerId']=0;
                $updateFields['AllowDeVryInternstoObserveWork']=$AllowDeVryInternstoObserveWork;
                $updateFields['AllowDeVryInternstoPerformField']=$AllowDeVryInternstoPerformField;
                $result = Core_Database::insert("clients_ext",$updateFields);
            }
            else
            {
                $criteria = "ClientId = '$clientID'";
                $updateFields['AllowDeVryInternstoObserveWork']=$AllowDeVryInternstoObserveWork;
                $updateFields['AllowDeVryInternstoPerformField']=$AllowDeVryInternstoPerformField;
                $result = Core_Database::update("clients_ext",$updateFields,$criteria);
            }        
            
	        if($saveToWOsOption == 1)
	        {
            //--- save values to WOs
            $criteria = "WINNUM IN (SELECT WIN_NUM FROM work_orders WHERE Company_ID = '$companyID' )"; 
            $updateFields = array();
            $updateFields['AllowDeVryInternstoObserveWork']=$AllowDeVryInternstoObserveWork;
            $updateFields['AllowDeVryInternstoPerformField']=$AllowDeVryInternstoPerformField;
            $result = Core_Database::update("work_orders_additional_fields",$updateFields,$criteria);
            
            //--- save values to projects
            // projects  Project_Company_ID,AllowDeVryInternstoObserveWork,AllowDeVryInternstoPerformField
            $criteria = "Project_Company_ID = '$companyID'"; 
            $updateFields = array();
            $updateFields['AllowDeVryInternstoObserveWork']=$AllowDeVryInternstoObserveWork;
            $updateFields['AllowDeVryInternstoPerformField']=$AllowDeVryInternstoPerformField;
            $result = Core_Database::update("projects",$updateFields,$criteria);
        	}
        
        
    }
    //--- 13713
    public function Exists_ClientExt($clientID)
    {
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from(Core_Database::TABLE_CLIENTS_EXT,array('ClientId'));
        $select->where("ClientId='$clientID'");
        $records = Core_Database::fetchAll($select); 
        
        if(!empty($records) && count($records)>0) return 1;
        else return 0;        
    }
    
    //--- 13713
    public function get_ClientExt($clientID)
    {
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from(Core_Database::TABLE_CLIENTS_EXT,array('ClientId'));
        $select->where("ClientId='$clientID'");
        $records = Core_Database::fetchAll($select); 
        
        if(!empty($records) && count($records)>0) return $records[0];
        else return null;
    }

    //13901
    public function updateLastLoginDate($userName,$lastLoginDate='')
    {
        if(empty($lastLoginDate)){
            $lastLoginDate = date("Y-m-d H:i:s");
        }
        
        $criteria = "UserName = '$userName'";
        $fields = array('LastLoginDate'=>$lastLoginDate);
        $result = Core_Database::update(Core_Database::TABLE_CLIENTS,$fields,$criteria);
        return $result;
    }
    
    //13743
    public function AreFullorPartialRestriction($companyID)
    {
        $clientID = $this->getRepClientIDforCompany($companyID);
        if(empty($clientID)){
            return false;
        }        
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from("cliaccessctl");
        $select->where("clientid=?",$clientID);
        $result = Core_Database::fetchAll($select);        
        if(empty($result))  {
            return false;
        }  else  {
            foreach($result as $aarow) {
                if($aarow['cliaccess_atrib_value']==$this->_restrictedValue) {
                    return true;
                }
            }
            return false;        
        }
    }

    //13743
    public function AreFullRestriction($companyID)
    {
        $clientID = $this->getRepClientIDforCompany($companyID);
        if(empty($clientID)){
            return false;
        }        
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from("cliaccessctl");
        $select->where("clientid=?",$clientID);
        $result = Core_Database::fetchAll($select);        
        if(empty($result)) {
            return false;
        } else {
            foreach($result as $aarow) {
                if($aarow['cliaccess_atrib_value']==$this->_openedValue) {
                    return false;
                }
            }
            return true;        
        }
    }

    //13743
    public function ArePartialRestriction($companyID)
    {
        $clientID = $this->getRepClientIDforCompany($companyID);
        if(empty($clientID)){
            return false;
        }        
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from("cliaccessctl");
        $select->where("clientid=?",$clientID);
        $result = Core_Database::fetchAll($select);        
        if(empty($result))  {
            return false;
        }  else  {
            foreach($result as $aarow)  {
                if($aarow['cliaccess_atrib_value']==$this->_restrictedValue) {
                    return true;
                }
            }
            return false;        
        }
    }
    
    //14046
    public function getRepClientforCompanyOnyOne($companyId)
    {
        $db = Core_Database::getInstance();
        //--- get first enabled account
        $select = $db->select();
        $select->from(Core_Database::TABLE_CLIENTS);
        $select->where("AccountEnabled = ?", 1);
        $select->where("Company_ID = '$companyId'");    
        $select->order("ClientID ASC");
        $result = Core_Database::fetchAll($select);  
        if(!empty($result) && count($result)>0){
            return $result[0];
        }
        
        //---- if not, get first client
        $select = $db->select();
        $select->from(Core_Database::TABLE_CLIENTS);
        $select->where("Company_ID = '$companyId'");    
        $select->order("ClientID ASC");
        $result = Core_Database::fetchAll($select);  
        if(!empty($result) && count($result)>0){
            return $result[0];
        } else {
            return null;
        }        
    }//end 14046
    
    //14144
    public function setAllAccessAttribToRestricted($companyID)
    {
         
        $clientIDArray = $this->getClientIDArray_InCompany($companyID);
        if(empty($clientIDArray) || count($clientIDArray)==0) return null;        
        $clientIDs = implode(',',$clientIDArray);
        
        $criteria = "clientid IN ($clientIDs)";
        $updateFields = array("cliaccess_atrib_value"=>$this->_restrictedValue);
        $result = Core_Database::update("cliaccessctl",$updateFields,$criteria);
        
        return $result;
    }

}
