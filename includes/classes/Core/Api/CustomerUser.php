<?php
class Core_Api_CustomerUser extends API_Abstract_User
{
	protected $_name = 'customers';
	
	protected $login = '';  //Username
	protected $password = ''; //Password
    protected $customerId = ''; //UNID
    protected $companyId = '';  //Company Name
	
	protected $customerName = ''; //Customer Name
	protected $contactName = ''; //Contact Name
	protected $contactEmail = ''; //Contact Email
	protected $contactPhone = ''; //Contact Phone
	
    protected $Active = ''; //Active
    protected $sendWelcomeEmail = ''; //SendWelcomeEmail
    
    
    public function  __construct($id = null) {
        parent::__construct();
        $this->ClientID = $id;
    }

    /**
     *
     * @param array $authData login/password or just login
     * @return Core_Api_TechUser $this
     * @author Alex Scherba
     */
    public function load($authData) {
        $errors = Core_Api_Error::getInstance();

        if (!is_array($authData) || empty($authData['login'])) {
			$errors->addError(2, 'Login is empty but required');
            return false;
        }
		if (strpos($authData['login'], "|pmContext=") !== FALSE) {
			$parts = explode("|pmContext=", $authData['login'], 2);
			$authData['login'] = $parts[0];
			$this->contextId = Core_Caspio::caspioEscape($parts[1]);
		}
		
		$this->login = $authData['login'];
        if (isset($authData['password'])) {
          $this->password = Core_Caspio::caspioEscape($authData['password']);
        }
		if (isset($this->contextId)){
			$db = Core_Database::getInstance();
			$select = $db->select();
			$select->from(Core_Database::TABLE_CUSTOMERS, array("UNID","Username","Password","Company_ID", "Customer_Name", "Contact_Name", "Contact_Email", "Contact_Phone"));
        
			$select->where("Company_ID = ?", $this->contextId);
			$customer = Core_Database::fetchAll($select);
			if (!empty($customer)) {
			$this->customerId       = $customer[0]['UNID'];
            $this->password       = $customer[0]['Password'];
            $this->companyId = $customer[0]['Company_ID'];
            $this->customerName = $customer[0]['Customer_Name'];
            $this->contactName    = $customer[0]['Contact_Name'];
            $this->contactEmail     = $customer[0]['Contact_Email'];
            $this->contactPhone      = $customer[0]['Contact_Phone'];
            $this->sendWelcomeEmail = $customer[0]['SendWelcomeEmail'];         
		} else {
			$er = $errors->getErrors();
			if (empty($er)) $errors->addError(3, 'Login or password is not correct');
            $this->customerId = '';
			$this->companyId = '';
			$this->customerName = NULL;
			$this->contactName = NULL;
			$this->contactEmail = NULL;
			$this->contactPhone = NULL;
                $this->sendWelcomeEmail = 0;
			return false;
		}
		}else{
        $db = Core_Database::getInstance();
       	$select = $db->select();
       	$select->from(Core_Database::TABLE_CUSTOMERS, array("UNID","Username","Password","Company_ID", "Customer_Name", "Contact_Name", "Contact_Email", "Contact_Phone"));
        
        $select->where("Username = ?", $this->login);
		//$criteria = 'Username = \''.$this->login.'\'';
        if (!empty($this->password)) {
           // $criteria . ' AND Password = \''.$this->password.'\'';
           $select->where("Password = ?", $this->password);
        }

//	echo $select;
        if (defined('SHORT_AUTH') && isset($_SESSION)) {
		$userCache = new Zend_Session_Namespace('User_Cache');
            if (!empty($userCache->UserObj) && !empty($userCache->UserObj['UNID'])) {
                $user = $userCache->UserObj;
            } else {
               	$user = Core_Database::fetchAll($select);
		$userCache->UserObj = $user;
            }
        }else{
        	$user = Core_Database::fetchAll($select);
        }

//	print_r($user);
	

//array("UNID","Username","Password","Company_ID", "Customer_Name", "Contact_Name", "Contact_Email", "Contact_Phone"));
		if (!empty($user)) {
			$this->customerId       = $user[0]['UNID'];
            $this->password       = $user[0]['Password'];
            $this->companyId = $user[0]['Company_ID'];
            $this->customerName = $user[0]['Customer_Name'];
            $this->contactName    = $user[0]['Contact_Name'];
            $this->contactEmail     = $user[0]['Contact_Email'];
            $this->contactPhone      = $user[0]['Contact_Phone'];
                $this->sendWelcomeEmail      = $user[0]['SendWelcomeEmail'];
		} else {
			$er = $errors->getErrors();
			if (empty($er)) $errors->addError(3, 'Login or password is not correct');
            $this->customerId = '';
			$this->companyId = '';
			$this->customerName = NULL;
			$this->contactName = NULL;
			$this->contactEmail = NULL;
			$this->contactPhone = NULL;
                $this->sendWelcomeEmail = NULL;
			return false;
		}
	}
        return $this;
    }

    public function save($data = null){
        $errors = Core_Api_Error::getInstance();
        if( empty($data) ) {
            $info = $this->info();
            $data = array();
            foreach( $info['cols'] as $col ) $data[$col] = $this->$col;
        }
        
		if (!isset($data['active'])) $data['active'] = 1;		
		
		$db = Core_Database::getInstance();
        if( empty($this->customerId) ){
			$res = $db->insert(Core_Database::TABLE_CUSTOMERS, $data);
            if( $res ) return new $this($res);
        } else {
                try {
            $res = $db->update(Core_Database::TABLE_CUSTOMERS, $data, $db->quoteInto('UNID = ?',$this->customerId));
                } catch (Exception $e) {
                        $errors->addError('7', 'Incorrect SQL request');
                }
		
			$userCache = new Zend_Session_Namespace('User_Cache');
			unset($userCache->UserObj);
				if( $res ) {
					return $this;
				}
        }
        return null;
    }

	/**
     * @param int $id
     * @return Core_Api_CustomerUser $this
     * @author Alex Scherba
     */
    public function loadById($cid) {
        $errors = Core_Api_Error::getInstance();

        if (empty($cid) && empty($this->customerId)) {
			$errors->addError(2, 'ID is empty but required');
            return false;
		}
        $cid = empty($cid) ? $this->customerId : $cid;


        $db = Core_Database::getInstance();
       	$select = $db->select();

        $select->from(Core_Database::TABLE_CUSTOMERS)
            ->where("UNID = ?", $cid);
        $user = $db->fetchRow($select);

		if (!empty($user)) {
			$this->login = $user['Username'];
			$this->customerId       = $user['UNID'];
            $this->password       = $user['Password'];
            $this->companyId = $user['Company_ID'];
            $this->customerName = $user['Customer_Name'];
            $this->contactName    = $user['Contact_Name'];
            $this->contactEmail     = $user['Contact_Email'];
            $this->contactPhone      = $user['Contact_Phone'];
            $this->active      = $user['active']; 
            $this->SendWelcomeEmail       = $user['SendWelcomeEmail'];  
        return $this;
		} else {
			$er = $errors->getErrors();
			if (empty($er)) $errors->addError(3, 'There is no customer with ID '.$cid);
            return false;
		}
    }
	/**
	 * Check up user information
	 *
	 * @param array $authData
	 * @return bool
	 */
	public function checkAuthentication($authData) {
		$errors = Core_Api_Error::getInstance();

		$this->authenticate = false;

		if (empty($authData['login']) || empty($authData['password'])) {
			$errors->addError(2, 'Login or password is empty but required');	
			return false;
		}
        
        if (!$this->checkProtocol()) {
			throw new SoapFault("jeff", "failing in check protocol");
			return false;
		}
        
        $this->load($authData);

        if ($this->customerId || $this->companyId) {
            $this->authenticate = true;
        }

		return $this->authenticate;
	}

    function __get($name) {
        if (property_exists($this, $name)){
            return $this->$name;
        } else {
//            throw new Exception('Property not exists');
        }
    }


	/**
	 * Check up user authentication
	 *
	 * @return bool
	 */
	public function isAuthenticate() {
		return $this->authenticate;
	}

	/**
	 *
	 *
	 * @return bool
	 */
	protected function checkProtocol() {
		return true;

		if (HTTPS_PROTOCOL_REQUIRED) {
			$cur_protocol = $_SERVER['SERVER_PROTOCOL'];
			$cur_protocol = explode('/',$cur_protocol);
			if ($cur_protocol[0] == 'HTTPS') {
				$errors = Core_Api_Error::getInstance();
				$errors->addError(4, 'Not HTTPS protocol used');
				return false;
			} else {
				return true;
			}
		}
		return false;
	}
	
	public function loginHistory($userName){
		$class = new Core_Api_Class;
		$class->loginHistory($userName, "Customer");
	}

	public function getCustomerId()	{
		return $this->customerId;
	}
    public function getCustomerName()    {
        return $this->customerName;
    }
    
    public function getSendWelcomeEmail()    {
        return $this->SendWelcomeEmail;
    }
    /**
     * getLogin
     *
     * @access public
     * @return string
     */
	public function getLogin() {
	    return $this->login;
    }
    /**
     * getPassword
     *
     * @access public
     * @return string
     */
    public function getPassword() {
        return $this->password;
    }
    /**
     * getCompanyId
     *
     * @access public
     * @return string
     */
    public function getCompanyId() {
        return $this->companyId;
    }
	public function getContextCompanyId($company) {
		return $this->getCompanyId();
	}
	/**
     * getContactName
     *
     * @access public
     * @return string
     */
    public function getContactName() {
        return $this->contactName;
    }
	/**
     * getContactEmail
     *
     * @access public
     * @return string
     */
    public function getContactEmail() {
        return $this->contactEmail;
    }
	/**
     * getContactPhone
     *
     * @access public
     * @return string
     */
    public function getContactPhone() {
        return $this->contactPhone;
    }
	public function getContextCompanyName($company) {
		$errors = Core_Api_Error::getInstance();

        if (empty($company) && empty($this->company)) {
			$errors->addError(2, 'Company ID is empty but required');
            return false;
		}
        $cid = empty($company) ? $this->company : $company;


        $db = Core_Database::getInstance();
       	$select = $db->select();

        $select->from(Core_Database::TABLE_CLIENT, "CompanyName")
            ->where("Company_ID = ?", $cid);
        $user = $db->fetchRow($select);

		if (!empty($user)) {
            $this->companyName = $user['CompanyName'];
			return $this;
		} else {
			$er = $errors->getErrors();
			if (empty($er)) $errors->addError(3, 'There is no company associated with this username '.$cid);
            return false;
		}
	}
    public static function customerWithResult($result) {
                $client = new Core_Api_CustomerUser();
                if (!empty($result)) {
                    foreach ($result as $key=>$value) {
				switch ($key) {
					case 'Contact_Name':
					$client->contactName;
					break;
					case 'UserName':
					$client->login;
					break;
					default:
	                                $client->$key = $value;
				}
                    }
                }
                return $client;
        }
	public function remove($id = null){
        $id = empty($this->ClientID)?$id:$this->ClientID;
        if(empty($id)){
            $errors = Core_Api_Error::getInstance();
            $errors->addError(7, 'Client ID empty but reqired.');
            return false;
        }
        return $this->delete('ClientID = '.$id);
    }
}
