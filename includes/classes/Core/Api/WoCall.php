<?php
define("ADAPTER_LOCATION", "https://www.fieldsolutions.com/api/wosAdapter.php"); 

class Core_WoCall
{
    const WO_CREATE  = 'create';
    const WO_UPDATE  = 'update';
    const WO_APPROVE = 'approve';
    const WO_TECH_COMPLETE = 'tech_complete';

    private $params = array();
    private $techID;
    private $tbUNID;
    private $projectID;
    private $callType = self::WO_UPDATE;
    private $curlURL;
    
    public function setParams($value) {
        $this->params = $value;
    }
    
    public function setTechId($value) {
        $this->techID = (int)$value;
    }
    
    public function setWorkOrderId($value) {
        $this->tbUNID = (int)$value;
    }
    
    public function setProjectId($value) {
        $this->projectID = $value;
    }
    
    public function setCallType($value) {
        $this->callType = $value;
    }
    
    public function doCall()
    {
        $paramsArray[] = array();
        
        $paramsArray[] = 'TB_UNID='.urlencode($this->tbUNID);
        $paramsArray[] = 'techID='.urlencode($this->techID);
        if ($this->callType == self::WO_UPDATE || $this->callType == self::WO_CREATE) {
        	$paramsArray[] = 'ProjectID='.urlencode($this->projectID);
        } else {
            $paramsArray[] = 'ProjectNo='.urlencode($this->projectID);
        }

        foreach ($this->params as $key=>$val) {
        	$paramsArray[] = $key.'='.urlencode($val);
        }

        $paramsArray[] = 'callType='.urlencode($this->callType);
        
        $this->curlURL = ADAPTER_LOCATION.'?'.implode('&'.$paramsArray);
        $this->executeCurlRequest();
    }
    
    private function executeCurlRequest()
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->curlURL); // set url to post to
        curl_setopt($ch, CURLOPT_FAILONERROR, 0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);// allow redirects
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1); // return into a variable
        curl_setopt($ch, CURLOPT_TIMEOUT, 20); // times out after 4s
        curl_setopt($ch, CURLOPT_POST, 0); // set POST method
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        //$result = curl_exec($ch); // run the whole process
        curl_close($ch);
    }
}