<?php
class Core_Api_Logger
{
     static private $instance = NULL;
     private $logId     = null;
     private $logUrl    = null;
    private $companyId = null;
     private $login     = null;
    
     private function __construct(){}
    
     private function __clone(){}
    
     public static function getInstance()
     {
          if (self::$instance == NULL)
               self::$instance = new Core_Api_Logger();
              
          return self::$instance;
     }
    
     public function addLog($error, $errorId=null, $description=null, $resultCode=null)
     {
//          $db = Zend_Registry::get('DB');
         
          $ip  = $_SERVER['REMOTE_ADDR'];
//          $date = date('Y-m-d h:i:s', time());

		  date_default_timezone_set('US/Central');
          $date = date('m-d-Y H:i:s', time());
         
          $row = array (
              'url'             => $this->logUrl,
              'result_code'     => $resultCode,
              'ip'              => $ip,
              'time_requested'  => $date,
              'client_username' => $this->login,
              'error'           => $error,
              'error_id'        => $errorId,
              'error_description'     => $description
          );
       
        if ($this->companyId !== null) {
            $row['company_id'] = $this->companyId;  
        }


          $fieldsList = implode(",",array_keys($row));
          
     }
    
     public function setLogin($login)
     {
          $this->login = $login;
     }
    
     public function setAuthData($authData)
     {
          $this->login = !empty($authData['login']) ? $authData['login'] : null;
     }
    
     public function setUrl($url)
     {
          $this->logUrl = !empty($url) ? $url : null;
     }
   
    public function setCompanyId($companyId)
    {
        $this->companyId = $companyId;
    }
    
}
