<?php
class Core_Api_CusReportClass extends Core_Api_CusReportClass2
{
    const DATATYPE_DATETIME = "datetime";
    const DATATYPE_STRING = "string";
    const DATATYPE_BOOLEAN = "boolean";
    const DATATYPE_NUMERIC = "numeric";

    const CONTROL_TYPE_DROPDOWN = "D";
    const CONTROL_TYPE_DROPDOWN_YES_NO = "DYN";
    const CONTROL_TYPE_TEXT = "T";
    const CONTROL_TYPE_DATETIME = "DT";
    const CONTROL_TYPE_NUMBER_FROM_TO = "N2";
    const CONTROL_TYPE_TIME_FROM_TO = "T2";
    const CONTROL_TYPE_SELECT_IN_POPUP = "S";  //Select in popup
    
    const FieldFilter_Location_WO_Select_Projects = "Select Projects";
    const FieldFilter_Location_WO_Work_Order_Status = "Work Order Status";    
    const FieldFilter_Location_WO_Additional_Information = "Additional Information";
    const FieldFilter_Location_WO_Basic_Information = "Basic Work Order Information";
    const FieldFilter_Location_WO_Bid_Information = "Bid Information";
    const FieldFilter_Location_WO_Communications = "Communications";
    const FieldFilter_Location_WO_Confirmations_Close_Out = "Confirmations & Close Out";
    const FieldFilter_Location_WO_Dates_Times = "Dates & Times";
    const FieldFilter_Location_WO_Pricing_Pay_Details = "Pricing & Pay Details";
    const FieldFilter_Location_WO_Site_Information = "Site Information";
    const FieldFilter_Location_WO_Technicians = "Technicians";
    const FieldFilter_Location_WO_PartsManager = "Parts Manager";
    const FieldFilter_Location_WO_WinTags = "Win Tags";

    const Filter_Location_WO_Dates_Times = "Dates & Times";
    const Filter_Location_WO_Site_Information = "Site Information";
    const Filter_Location_WO_Bid_Information = "Bid Information";
    const Filter_Location_WO_Payment_Information = "Payment Information";
    const Filter_Location_WO_Additional_Filters = "Additional Filters";
        
    const DT_Ref_Key_FixedDateRange = "FixedDateRange";
    const DT_Ref_Key_Today = "Today";
    const DT_Ref_Key_TodayAndTomorrow = "TodayAndTomorrow";
    const DT_Ref_Key_TodayAndNextBusinessDay = "TodayAndNextBusinessDay";
    const DT_Ref_Key_Tomorrow = "Tomorrow";
    const DT_Ref_Key_NextBusinessDay = "NextBusinessDay";
    const DT_Ref_Key_Next2days = "Next2days";
    const DT_Ref_Key_Next2Businessdays = "Next2Businessdays";
    const DT_Ref_Key_Next3days = "Next3days";
    const DT_Ref_Key_Next3Businessdays = "Next3Businessdays";
    const DT_Ref_Key_Next4days = "Next4days";
    const DT_Ref_Key_Next4BusinessDays = "Next4BusinessDays";
    const DT_Ref_Key_Next5Days = "Next5Days";
    const DT_Ref_Key_Next5BusinessDays = "Next5BusinessDays";
    const DT_Ref_Key_ThisWeek = "ThisWeek";
    const DT_Ref_Key_ThisMonth = "ThisMonth";
    const DT_Ref_Key_NextMonth = "NextMonth";
    const DT_Ref_Key_Yesterday = "Yesterday";
    const DT_Ref_Key_LastBusinessDay = "LastBusinessDay";
    const DT_Ref_Key_Last2days = "Last2days";
    const DT_Ref_Key_Last2Businessdays = "Last2Businessdays";
    const DT_Ref_Key_Last3days = "Last3days";
    const DT_Ref_Key_Last3Businessdays = "Last3Businessdays";
    const DT_Ref_Key_Last4days = "Last4days";
    const DT_Ref_Key_Last4Businessdays = "Last4Businessdays";
    const DT_Ref_Key_Last5days = "Last5days";
    const DT_Ref_Key_Last5Businessdays = "Last5Businessdays";
    const DT_Ref_Key_LastWeek = "LastWeek";
    const DT_Ref_Key_LastMonth = "LastMonth";
    const DT_Ref_Key_Q1 = "Q1";
    const DT_Ref_Key_Q2 = "Q2";
    const DT_Ref_Key_Q3 = "Q3";
    const DT_Ref_Key_Q4 = "Q4";
    const DT_Ref_Key_H1 = "H1";
    const DT_Ref_Key_H2 = "H2";
    const DT_Ref_Key_YTD = "YTD";
    
    const DT_Ref_Text_FixedDateRange = "Fixed Date Range";
    const DT_Ref_Text_Today = "Today";
    const DT_Ref_Text_TodayAndTomorrow = "Today and Tomorrow";
    const DT_Ref_Text_TodayAndNextBusinessDay = "Today and Next Business Day";
    const DT_Ref_Text_Tomorrow = "Tomorrow";    
    const DT_Ref_Text_NextBusinessDay = "Next Business Day";
    const DT_Ref_Text_Next2days = "Next 2 days";
    const DT_Ref_Text_Next2Businessdays = "Next 2 Business days";
    const DT_Ref_Text_Next3days = "Next 3 days";
    const DT_Ref_Text_Next3Businessdays = "Next 3 Business days";
    const DT_Ref_Text_Next4days = "Next 4 days";
    const DT_Ref_Text_Next4BusinessDays = "Next 4 Business Days";
    const DT_Ref_Text_Next5Days = "Next 5 Days";
    const DT_Ref_Text_Next5BusinessDays = "Next 5 Business Days";
    const DT_Ref_Text_ThisWeek = "This Week";
    const DT_Ref_Text_ThisMonth = "This Month";
    const DT_Ref_Text_NextMonth = "Next Month";
    const DT_Ref_Text_Yesterday = "Yesterday";
    const DT_Ref_Text_LastBusinessDay = "Last Business Day";
    const DT_Ref_Text_Last2days = "Last 2 days";
    const DT_Ref_Text_Last2Businessdays = "Last 2 Business days";
    const DT_Ref_Text_Last3days = "Last 3 days";
    const DT_Ref_Text_Last3Businessdays = "Last 3 Business days";
    const DT_Ref_Text_Last4days = "Last 4 days";
    const DT_Ref_Text_Last4Businessdays = "Last 4 Businessdays";
    const DT_Ref_Text_Last5days = "Last 5 days";
    const DT_Ref_Text_Last5Businessdays = "Last 5 Business days";
    const DT_Ref_Text_LastWeek = "Last Week";
    const DT_Ref_Text_LastMonth = "Last Month";
    const DT_Ref_Text_Q1 = "Q1";
    const DT_Ref_Text_Q2 = "Q2";
    const DT_Ref_Text_Q3 = "Q3";
    const DT_Ref_Text_Q4 = "Q4";
    const DT_Ref_Text_H1 = "H1";
    const DT_Ref_Text_H2 = "H2";
    const DT_Ref_Text_YTD = "YTD";

    const String_Matching_ExactMatch = "ExactMatch";
    const String_Matching_BeginsWith = "BeginsWith";
    const String_Matching_EndsWith = "EndsWith";
    const String_Matching_Contains = "Contains";
    const String_Matching_IsNot = "IsNot";
    const String_Matching_SelectMultiple = "SelectMultiple"; //SelectMultiple

    const Bool_Value_True = "t";
    const Bool_Value_False = "f";
    const Bool_Value_Any = "a";
    
    const GetList_Filter_CreateBy = "created_by";
    
    const Report_Type_WorkOrder = 1;
    const Report_Type_Bids = 2;
    const Report_Type_WinTags = 3;
    const Report_Type_Parts = 4;
    const Report_Type_Visits = 5;
    
    const Criteria_Type_FromDate_ToDate = "FromDate_ToDate";
    const Criteria_Type_FromTime_ToTime = "FromTime_ToTime";
    const Criteria_Type_FromNumber_ToNumber = "FromNumber_ToNumber";
    const Criteria_Type_Yes_No = "Yes_No";
    const Criteria_Type_ExactMatch = "ExactMatch";
    const Criteria_Type_SelectMulti = "SelectMulti";
    
    public function getCriteriaYesNo($filter,$yesCri,$noCri)
    {
        $retCriteria = "";
        $SelectMultiple = $filter['SelectMultiple'];
        if(count($SelectMultiple) > 0)
        {
             $yesno = $SelectMultiple[0];
             if($yesno=='Y' || $yesno=='t'){                 
                 $retCriteria= $yesCri;    
             } else if ($yesno=='N' || $yesno=='f'){
                 $retCriteria = $noCri;
             }
        }             
        return $retCriteria;                                   
    }
    
    public function getCriteria($filter,$fieldCode,$type)
    {
        $retCriteria = "";
        if($type == self::Criteria_Type_FromDate_ToDate)
        {            
            $from = $filter['datetime_range_from'];
            $to = $filter['datetime_range_to'];            
            if(!empty($from)){
                $from = date('Y-m-d',strtotime($from));
                $retCriteria = "$fieldCode >='$from'";    
            }
            if(!empty($to)){
                $to = date('Y-m-d',strtotime($to));
                $retCriteria .= empty($retCriteria) ? "" : " AND ";
                $retCriteria .= "$fieldCode <='$to'";    
            }
        } 
        else if($type==self::Criteria_Type_FromTime_ToTime)
        {
             $cri = "";
             $timeFrom = $filter['time_from'];
             $timeTo = $filter['time_to'];
             $standardOffset = $filter['time_tz'];
             
             $stdOffsetArray = array('-10','-9','-8','-7','-6','-5');
             $timeFromArray = array();
             $timeToArray = array();
             if(!empty($timeFrom)){
                 $timeFromArray = $this->getTimeForTimezoneStandardOffset($timeFrom, $standardOffset); 
             }
             if(!empty($timeTo)){
                 $timeToArray = $this->getTimeForTimezoneStandardOffset($timeTo, $standardOffset); 
             }
             
             foreach($stdOffsetArray as $stdOffset){
                 $subCri = "";
                 if(!empty($timeFrom)){
                     $subtimefr = $timeFromArray[$stdOffset];
                     $subCri = "(STR_TO_DATE($fieldCode,'%l:%i %p') >= STR_TO_DATE('$subtimefr','%l:%i %p') AND z.StandardOffset = '$stdOffset')";
                 }
                 if(!empty($timeTo)){
                     $subtimeto = $timeToArray[$stdOffset];
                     $subCri .= empty($subCri) ? "" : " AND ";
                     $subCri .= "(STR_TO_DATE($fieldCode,'%l:%i %p') <= STR_TO_DATE('$subtimeto','%l:%i %p') AND z.StandardOffset = '$stdOffset')";
                 }
                 if(!empty($subCri)){
                     $cri .= empty($cri) ? "" : " OR ";
                     $cri .= "($subCri)";
                 }
             }
             $retCriteria = $cri;
        } 
        else if($type==self::Criteria_Type_FromNumber_ToNumber)
        {
            $from = $filter['numeric_range_from'];
            $to = $filter['numeric_range_to'];            
            if(isset($from) && is_numeric($from) ){
                $retCriteria = "$fieldCode >='$from'";    
            }
            if(isset($to) && is_numeric($to)){
                $retCriteria .= empty($retCriteria) ? "" : " AND ";
                $retCriteria .= "$fieldCode <='$to'";    
            }
        }       
        else if($type==self::Criteria_Type_ExactMatch)
        {
            $value = $filter['string_value'];
            if(!empty($value)) {
               $retCriteria = "$fieldCode = '$value'";     
            }
        }
        else if($type==self::Criteria_Type_SelectMulti)
        {
            $SelectMultiple = $filter['SelectMultiple'];
            $filterFieldCode = $filter['fieldcode'];
            if(!empty($SelectMultiple) && count($SelectMultiple)>0){
                $cri = "";
                foreach($SelectMultiple as $k=>$v){
                    if($filterFieldCode=='Status'){
                        if($v=='work_done') $v='work done';
                        if($v=='in_accounting') $v='in accounting';
                    }
                    $cri .= empty($cri) ? "'$v'" : ",'$v'";
                }
                if(!empty($cri)){                    
                    $retCriteria = "$fieldCode IN ($cri)";
                }
            }
        }
        
        return $retCriteria;
    }
    

    
    /*
    * Filter keys:
    *   - created_by
    */    
    public function getList($companyID,$limit=0,$offset=0,$filters=null,$sort=null)
    {
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from("custom_reports");
        $select->where("companyid = '$companyID'");
        
        if(!empty($filters) && is_array($filters) && count($filters)>0){
            foreach($filters as $key=>$val){
                if($key==self::GetList_Filter_CreateBy){
                    $select->where("created_by = '$val'");                    
                }
            }                
        }
        
        if(!empty($sort)){
            $select->order($sort);
        } else {
            $select->order("rpt_name asc");            
        }
                
        if(!empty($limit) || !empty($offset)){
            $select->limit($limit,$offset);
        }
        
        //print_r($select->__toString());
        
        $records = Core_Database::fetchAll($select);  
        if(empty($records) || count($records)==0) {
            return null;   
        }
        else
        {
            return $records;              
        }

    }
    
    public function ReportName_Exists($reportName,$excludeReportID=0)
    {
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from("custom_reports");
        $select->where("rpt_name = '$reportName'");
        if(!empty($excludeReportID))
        {
            $select->where("id <> '$excludeReportID'");
        }
        $records = Core_Database::fetchAll($select);  
        if(empty($records) || count($records)==0) return 0;
        else return 1;
    }
    
    public function getReportByName($reportName,$companyID='')
    {
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from("custom_reports");
        $select->where("rpt_name = '$reportName'");
        if(!empty($companyID)){
            $select->where("companyid = '$companyID'");            
        }
        $records = Core_Database::fetchAll($select);  
        if(empty($records) || count($records)==0) return null;
        else return $records[0];
    }

        
    public function add($companyID,$repName,$repTable,$fieldArray,$filterArray=null,$reportType=1)
    {
        if($this->ReportName_Exists($repName)) return -1;

        $authUser = $_SESSION['Auth_User'];
        $loginName = $authUser['login'];
        $nameOfCurrentUser = $authUser['name'];
        
        //--- insert into custom_reports
        $fieldvals = array("rpt_name"=>$repName,
                        "table"=>$repTable,
                        "companyid"=>$companyID,
                        "creation_date"=>date("Y-m-d H:i:s"),
                        'lastupdated_date'=>date("Y-m-d H:i:s"),
                        "last_modified_by"=>$loginName,
                        "last_modified_by_name"=>$nameOfCurrentUser,
                        "created_by"=>$loginName,
                        "created_by_name"=>$nameOfCurrentUser,
                        'report_type'=>$reportType
                        );   
        $result = Core_Database::insert("custom_reports",$fieldvals);
        //--- insert into custom_reports_fields
        $report = $this->getReportByName($repName);
        if(empty($report)) return;
        
        $rep_id = $report['id'];
        if(empty($rep_id)) return;
        
        foreach($fieldArray as $k=>$v)
        { 
            $fieldvals = array("rep_id"=>$rep_id,
                        "fieldname"=>$k,
                        "fieldcaption"=>$v
            );
            $result = Core_Database::insert("custom_reports_fields",$fieldvals);
        }        
        if(!empty($filterArray) && count($filterArray)>0){
            foreach($filterArray as $filterArray){
                $this->addFilter($rep_id,$filterArray);
            }
        }
        
        //---
        return $rep_id;
    }
    
    
    public function getReportInfo($repID)
    {
        /*select cr.id,cr.rpt_name,cr.rpt_name,cr.companyid,crtbl.table_title 
from custom_reports cr
inner join custom_reports_tables crtbl on crtbl.table_code=cr.table*/

//sort_1     sort_dir_1     sort_2     sort_dir_2     sort_3     sort_dir_3 
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from(array('cr'=>"custom_reports"),
            array('id','rpt_name','companyid','table','sort_1','sort_dir_1','sort_2','sort_dir_2','sort_3','sort_dir_3','report_type')
        );
        $select->join(array('crtbl'=>'custom_reports_tables'),
            'crtbl.table_code=cr.table',
            array('table_title','special_where')
        );
        $select->where("id = '$repID'");
        $records = Core_Database::fetchAll($select); 
        if(empty($records) || count($records)==0){
            return null;  
        } else {
            return $records[0];   
        }
    }
    
    /*
    $paras-keys:
    groupid:
    isdisplayed
    */    
    public function getReportFieldList($repID,$paras=null,$sort='')
    {
        $reportinfo = $this->getReportInfo2($repID);
        if(empty($reportinfo)) return null;        
        $companyID = $reportinfo['companyid'];

        $db = Core_Database::getInstance();  
        $select = $db->select();
        //isdisplayed     issum     iscount     issortby     isgroupby
        $select->from(array('crf'=>'custom_reports_fields'),
            array('crf_id'=>'id','rep_id','fieldname','display_order','groupid',
            'isdisplayed','issum',
            'iscount','issortby','isgroupby')
        );
        $select->join(array('cr'=>'custom_reports'),
            'cr.id = crf.rep_id',
            array('table','companyid','report_type')
        );        
        $select->join(array('crtf'=>'custom_reports_tablefields'),
            'crtf.field_code = crf.fieldname',
            array('table_code','field_code','field_title','datatype','dbtable')
        );
        $select->where("crf.rep_id = '$repID'");
        $select->where("crtf.table_code = cr.table");
        if(!empty($paras) && count($paras)>0){
            if(!empty($paras['groupid'])){
                $select->where("crf.groupid = '".$paras['groupid']."'");
            }
            if(!empty($paras['isdisplayed'])){
                $select->where("crf.isdisplayed = '".$paras['isdisplayed']."'");
            }
        }

        if($companyID != 'FLS'){
            $select->where("crtf.FLSOnly = 0");
        }
        
        if(empty($sort)){
            $select->order("crf.display_order asc");
            $select->order("crtf.field_title asc");            
        } else {
            $select->order($sort);
        }
        
        $select->distinct();
        $records = Core_Database::fetchAll($select);   
               
        $reportFields = array();
        if(empty($records) || count($records)==0) {
            return null;   
        } else {
            foreach($records as $rec)
            {
                $dbDataType = $rec['datatype'];
                $rec['datatype'] = $this->getDataType($dbDataType);
                $reportFields[]=$rec;                    
            }
            return $reportFields;
        }
    }

    public function getReportFieldList2($repID,$sort='')
    {
        $reportinfo = $this->getReportInfo2($repID);
        if(empty($reportinfo)) return null;        
        $companyID = $reportinfo['companyid'];
        
        $db = Core_Database::getInstance();  
        $select = $db->select();
        //isdisplayed     issum     iscount     issortby     isgroupby
        $select->from(array('crf'=>'custom_reports_fields'),
            array('crf_id'=>'id','rep_id','fieldname','display_order',
            'isdisplayed','issum','iscount','issortby','isgroupby','isavg')
        );
        $select->join(array('cr'=>'custom_reports'),
            'cr.id = crf.rep_id',
            array('table','companyid','report_type')
        );        
        $select->joinLeft(array('crtf'=>'custom_reports_tablefields'),
            'crtf.field_code = crf.fieldname',
            array('field_title','datatype','dbtable','Location','AllowedToSum','AllowedToAverage','AllowedToCount','AllowedToGroupBy','AverageIfOtherGrouped','BlankIfOtherGrouped','CountIfOtherGrouped','SumIfOtherGrouped','FLSOnly','GPMOnly','FilterLocation','FilterType')
        );
        $select->where("crf.rep_id = '$repID'");
        $select->where("crtf.table_code = cr.table");
        if($companyID != 'FLS'){
            $select->where("crtf.FLSOnly = 0");
        }
        
        if(empty($sort)){
            $select->order("crf.display_order asc");
            $select->order("crtf.Location asc");            
            $select->order("crtf.field_title asc");            
        } else {
            $select->order($sort);
        }
        
        $select->distinct();
        $records = Core_Database::fetchAll($select);   
               
        $reportFields = array();
        if(empty($records) || count($records)==0) {
            return null;   
        } else {
            foreach($records as $rec)
            {
                $key = $rec['fieldname'];
                if(empty($rec['field_title'])){
                    $rec['field_title'] = $rec['fieldname'];
                }
                $reportFields[$key]=$rec;  
                                  
            }
            return $reportFields;
        }
    }

    public function getReportHeader2($repID,$counts=null,$sums=null,$avgs=null)
    {
        $reportinfo = $this->getReportInfo2($repID);
        if(empty($reportinfo)) return null;        
        $companyID = $reportinfo['companyid'];

        
        $db = Core_Database::getInstance();  
        $select = $db->select();
        //isdisplayed     issum     iscount     issortby     isgroupby
        $select->from(array('crf'=>'custom_reports_fields'),
            array('fieldname')
        );
        $select->join(array('cr'=>'custom_reports'),
            'cr.id = crf.rep_id',
            array('report_type')
        );        
        $select->joinLeft(array('crtf'=>'custom_reports_tablefields'),
            'crtf.field_code = crf.fieldname',
            array('field_title','AllowedToSum','AllowedToAverage','AllowedToCount','AllowedToGroupBy')
        );
        $select->where("crf.rep_id = '$repID'");
        $select->where("crtf.table_code = cr.table");

        if($companyID != 'FLS'){
            $select->where("crtf.FLSOnly = 0");
        }
        
        $select->order("crf.display_order asc");
        $select->order("crtf.Location asc");            
        $select->order("crtf.field_title asc");            
        
        $select->distinct();
        $records = Core_Database::fetchAll($select);   
               
        $reportFields = array();
        if(empty($records) || count($records)==0) {
            return null;   
        } else {
            foreach($records as $rec)
            {
                $key = $rec['fieldname'];
                $allowedToCount = $rec['AllowedToCount'];
                $allowedToSum = $rec['AllowedToSum'];
                $allowedToAverage = $rec['AllowedToAverage'];
                
                $title = empty($rec['field_title'])?$key:$rec['field_title'];
                
                $reportFields[$key]=$title;                  
                if(!empty($allowedToCount) && !empty($counts) && count($counts)>0 && in_array($key,$counts)){
                    $reportFields["Count$key"]="Count of $title";
                }
                if(!empty($allowedToSum) && !empty($sums) && count($sums)>0 && in_array($key,$sums)){
                    $reportFields["Sum$key"]="Sum of $title";
                }
                if(!empty($allowedToAverage) && !empty($avgs) && count($avgs)>0 && in_array($key,$avgs)){
                    $reportFields["Avg$key"]="Avg. of $title";
                }                                  
            }
            return $reportFields;
        }
    }
    
    public function getReportFieldList_forGroup_1($repID)
    {
        $paras = array('groupid'=>1);
        return $this->getReportFieldList($repID,$paras);
    }
    
    public function getReportFieldList_forDisplay($repID)
    {
        $reportinfo = $this->getReportInfo2($repID);
        if(empty($reportinfo)) return null;        
        $companyID = $reportinfo['companyid'];

        $db = Core_Database::getInstance();  
        $select = $db->select();
        //isdisplayed     issum     iscount     issortby     isgroupby
        $select->from(array('crf'=>'custom_reports_fields'),
            array('crf_id'=>'id','rep_id','fieldname','display_order','groupid',
            'isdisplayed','issum',
            'iscount','issortby','isgroupby')
        );
        $select->join(array('cr'=>'custom_reports'),
            'cr.id = crf.rep_id',
            array('table','companyid','report_type')
        );        
        $select->join(array('crtf'=>'custom_reports_tablefields'),
            'crtf.field_code = crf.fieldname',
            array('table_code','field_code','field_title','datatype','dbtable')
        );
        $select->where("crf.rep_id = '$repID'");
        $select->where("crtf.table_code = cr.table");
        //$select->where("crf.isdisplayed = 1");
        
        if($companyID != 'FLS'){
            $select->where("crtf.FLSOnly = 0");
        }
        
        if(empty($sort)){
            $select->order("crf.display_order asc");
            $select->order("crtf.field_title asc");            
        } else {
            $select->order($sort);
        }
        
        $select->distinct();
        $records = Core_Database::fetchAll($select);   
               
        $reportFields = array();
        if(empty($records) || count($records)==0) {
            return null;   
        } else {
            foreach($records as $rec)
            {
                $dbDataType = $rec['datatype'];
                $rec['datatype'] = $this->getDataType($dbDataType);
                $fieldName = $rec['fieldname'];
                $reportFields[]=$rec;    
                if($rec['iscount']==1){
                    $rec1= $rec;
                    $rec1['fieldname'] = "Count".$fieldName;
                    $reportFields[]=$rec1;      
                }                
                if($rec['issum']==1){
                    $rec2= $rec;
                    $rec2['fieldname'] = "Sum".$fieldName;
                    $reportFields[]=$rec2;      
                }                
            }
            return $reportFields;
        }
    }

    
    public function getReportFieldNameArray($repID)
    {
        $reportinfo = $this->getReportInfo2($repID);
        if(empty($reportinfo)) return null;        
        $companyID = $reportinfo['companyid'];


        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from('custom_reports_fields',array('fieldname'));
        $select->where("rep_id = '$repID'");
        $records = Core_Database::fetchAll($select);  
        $reportFieldNameArray = array();
        if(!empty($records) && count($records)>0) {
            foreach($records as $rec){
                $reportFieldNameArray[]=$rec['fieldname'];
            }
        }
        return $reportFieldNameArray;
    }
    
    public function getReportFieldList_WithFilters($repID)
    {
        $reportinfo = $this->getReportInfo2($repID);
        if(empty($reportinfo)) return null;        
        $companyID = $reportinfo['companyid'];
        
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from(array('crf'=>'custom_reports_fields'),
            array('crf_id'=>'id','rep_id','fieldname')
        );
        $select->join(array('cr'=>'custom_reports'),
            'cr.id = crf.rep_id',
            array('table','companyid','report_type')
        );        
        $select->join(array('crtf'=>'custom_reports_tablefields'),
            'crtf.field_code = crf.fieldname',
            array('table_code','field_code','field_title','datatype','dbtable')
        );
        $select->joinLeft(array('crfi'=>'custom_reports_filters'),
            'crfi.rep_id = crf.id AND crfi.fieldcode = crtf.field_code',
            array('fieldcode','datetime_relative_data_ref','datetime_range_from',
'datetime_range_to','string_matching','string_value','bool_value','numeric_base_value','numeric_range_from','numeric_range_to')
        );
        
        $select->where("crf.rep_id = '$repID'");
        $select->where("crtf.table_code = cr.table");
        
        if($companyID != 'FLS'){
            $select->where("FLSOnly = 0");
        }
                
        $select->order("crf.id asc");
        $select->distinct();
        $records = Core_Database::fetchAll($select);  
        if(empty($records) || count($records)==0) {
            return null;   
        } else {
            for($i=0;$i<count($records);$i++)
            {
                $dbDataType = $records[$i]['datatype'];
                $records[$i]['datatype'] = $this->getDataType($dbDataType);
            }
            return $records;   
        }
    }

    public function edit($repID,$companyID,$repName,$repTable,$fieldArray,$reportType=1)
    {
        if(empty($repID)) return -2;        
        if($this->ReportName_Exists($repName,$repID)) return -1;
        
        //--- update custom_reports
        $fieldvals = array("rpt_name"=>$repName,
                        "table"=>$repTable,
                        "companyid"=>$companyID,
                        "lastupdated_date"=>date("Y-m-d H:i:s"),
                        'report_type'=>$reportType
                        );  
        $criteria = "id='$repID'";                 
        $result = Core_Database::update("custom_reports",$fieldvals,$criteria);
        //--- delete old data from custom_reports_fields
        $criteria = "rep_id='$repID'";                 
        $db = Core_Database::getInstance();
        $result = $db->delete('custom_reports_fields',$criteria);        
        //--- insert into custom_reports_fields        
        foreach($fieldArray as $k=>$v)
        { 
            $fieldvals = array("rep_id"=>$repID,
                        "fieldname"=>$k,
                        "fieldcaption"=>$v
            );
            $result = Core_Database::insert("custom_reports_fields",$fieldvals);
        }        
        //---
        return 0;
    }
    
    public function delete($repID)
    {
        //--- delete data from custom_reports_fields
        $criteria = "rep_id='$repID'";                 
        $db = Core_Database::getInstance();
        $result = $db->delete('custom_reports_fields',$criteria);        
        //--- delete data from custom_reports_fields
        $criteria = "id='$repID'";                 
        $db = Core_Database::getInstance();
        $result = $db->delete('custom_reports',$criteria);        
        
    }
    
    public function getAvailFields_ByTable($table)
    {
        //$table = "work_orders"; "TechBankInfo"
        if ($table == "work_orders") {
            $fields = array(
                  "Basic Work Order Information"=>""
                , "WIN#" => "WIN_NUM"
                , "Client Work Order ID #" => "WO_ID"
                , "Project" => "Project_Name"
                , "City" => "City"
                , "State" => "State"
                , "Site" => "SiteName"
                , "Zipcode" => "Zipcode"
                , "Route" => "Route"
                , "Skill" => "WO_Category_ID"
                , "Headline" => "Headline"
                , "Tech Assignment & Pay"=>""
                , "Client $ Offer" => "PayMax"
                , "Amount Per" => "Amount_Per"
                , "Bid/Agreed Amount" => "Tech_Bid_Amount"
                , "Site Completion Information"=>""
                , "Return Parts" => "ReturnPartCount"
                , "Paperwork Received" => "Paperwork_Received"
                , "Paperwork Incomplete" => "Incomplete_Paperwork"
                , "Site Marked Complete by Tech" => "TechMarkedComplete"
                , "Site Marked Complete in Client System" => "CallClosed"
                , "Financials & Approval"=>""
                , "Agreed Pay" => "Tech_Bid_Amount"
                , "Base Tech Pay" => "baseTechPay"
                , "Final Total Tech Pay" => "PayAmount"
                , "Invoice Amount" => "calculatedInvoice"
                );
              return $fields;  
        } else if ($table == "TechBankInfo") {
            $fields = array("Tech ID" => "TechID"
                , "First Name" => "FirstName"
                , "Last Name" => "LastName"
                , "Birth Date" => "Birthdate"
                , "Primary Email" => "PrimaryEmail"
                , "Address 1" => "Address1"
                , "State" => "State"
                , "City" => "City"
                , "Username" => "UserName"
            );
            return $fields;
        }
        else
        {
            return array();
        }
    }
    
    public function getListOfTables()
    {
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from("custom_reports_tables");
        $records = Core_Database::fetchAll($select);  
        if(empty($records) || count($records)==0) {
            return null;  
        } 
        else {
            return $records;  
        } 
    }
    
    public function getListofFields_InTable($tableCode)
    {
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from("custom_reports_tablefields");
        $select->where("table_code = '$tableCode'");
        $select->order("dbtable");
        $select->order("field_title");
        $records = Core_Database::fetchAll($select);  
        if(empty($records) || count($records)==0) {
            return null;  
        } 
        else {
            return $records;  
        } 
        
    }
    
    public function getReportRecords($repID,$limit=0,$offset=0,$forCount=0)
    {
        $reportInfo = $this->getReportInfo($repID);
        if(empty($reportInfo)){
            return null;
        }
        $tableCode = $reportInfo['table'];
        $companyID = $reportInfo['companyid'];
        $specialWhere = $reportInfo['special_where'];
        if(empty($companyID)){
            return null;
        }
                
        $clauseWhere = '';
        if(!empty($specialWhere)){
            $clauseWhere = str_replace("{Company_ID}",$companyID,$specialWhere);
        }
        
        $db = Core_Database::getInstance();  
        $select = $db->select();
        if($forCount){
            $fields = array('NUM'=>'Count(*)');
            $select->from($tableCode,array('NUM'=>'Count(*)'));
        } else {
            $fields = $this->getReportFieldNameArray($repID);
            $select->from($tableCode,$fields);            
        }
        
        
        if(!empty($clauseWhere)){
            $select->where($clauseWhere);            
        }
        //--- filters
        $filters = $this->getFilterList_FullData($repID);
        if(!empty($filters) && count($filters) > 0){
            foreach($filters as $filter){
                $DBDefaultValue = $filter['DBDefaultValue'];
                $filter_at = $filter['filter_at'];
                if($filter_at==1 || $filter_at==3){
                    $select->where($DBDefaultValue);            
                }
            }
        }
        //--- limit, offset
        if(!empty($limit) || !empty($offset)){
            $select->limit($limit,$offset);            
        }
        //print_r($select->__toString());return;
        $records = Core_Database::fetchAll($select); 

        if($forCount){
            $count = 0;
            if(!empty($records) && count($records)>0){
                $count = $records[0]['NUM'];
            } 
            return $count;            
        } else {
            return $records;            
        }
    }
    
    public function getDataType($dbDatatype){
        if($dbDatatype=='tinyint') {
            $datatype = self::DATATYPE_BOOLEAN;
        } else if(strpos($dbDatatype, 'decimal')!==false || $dbDatatype=='float' || $dbDatatype=='int' || $dbDatatype=='money') {
            $datatype = self::DATATYPE_NUMERIC;
        } else if($dbDatatype==self::DATATYPE_DATETIME || $dbDatatype==self::DATATYPE_STRING){
            $datatype = $dbDatatype;
        }
        return $datatype;
    }
    
    
    public function addFilter($reportID,$key_value_array)
    {
        if(empty($reportID) || empty($key_value_array) || !is_array($key_value_array) || count($key_value_array)==0){
            return -1;
        }
        
        if(empty($key_value_array['fieldcode']) || empty($key_value_array['filter_at'])){
            return -2;
        }

        $fieldCode = $key_value_array['fieldcode'];
        $filterAt = $key_value_array['filter_at'];
        
        //--- insert to custom_reports_filters table 
        $avaiKeyArray = array('fieldcode','filter_at','datetime_relative_data_ref','datetime_range_from','datetime_range_to','string_matching','string_value','bool_value','numeric_base_value','numeric_range_from','numeric_range_to','datetime_range_int_from','datetime_range_int_to');
        
        $addFields = array();
        $addFields['rep_id'] = $reportID;
        
        foreach($key_value_array as $field=>$value)
        {
            if(in_array($field,$avaiKeyArray)){
                if($field == 'datetime_range_from' ||  $field == 'datetime_range_to'){
                    if(!empty($value)){
                        $value = date("Y-m-d",strtotime($value));
                    }
                }
                $addFields[$field] = $value;                
            }            
        }
        
        if(count($addFields) > 0){
            $result = Core_Database::insert("custom_reports_filters",$addFields);
            //$errors = Core_Api_Error::getInstance();
            //echo("error: ");print_r($errors);
        }
        
        //--- get filterID
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from('custom_reports_filters',array('id'));            
        $select->where("rep_id = '$reportID'");
        $select->where("fieldcode = '$fieldCode'");
        $select->where("filter_at = '$filterAt'");
        $select->order("id DESC");
        $records = Core_Database::fetchAll($select);  
        if(empty($records) && count($records)>0){
            return -1;
        } 
        
        $filterID = $records[0]['id'];
        
        //--- insert to  custom_reports_filters_selmulti
        if(isset($key_value_array['SelectMultiple'])){
            $selectArray = $key_value_array['SelectMultiple'];
            if(!empty($selectArray) && count($selectArray)>0)
            {
                foreach($selectArray as $selectValue){
                    try {                
                        $selectValue = trim($selectValue);
                        Core_Database::insert("custom_reports_filters_selmulti",
                                array('filter_id'=>$filterID,
                                    'filter_value'=>$selectValue)
                        );                        
                    } catch(Exception $ex) {
                                
                    }                                    
                }
            }
        }

        return 0;
    }
    
    public function updateFilter($reportID,$filterID,$key_value_array)
    {
        if(empty($reportID) || empty($filterID) || empty($key_value_array) || !is_array($key_value_array) || count($key_value_array)==0){
            return -1;
        }
        
        //--- update custom_reports_filters table 
        $avaiKeyArray = array('fieldcode','filter_at','datetime_relative_data_ref','datetime_range_from','datetime_range_to','string_matching','string_value','bool_value','numeric_base_value','numeric_range_from','numeric_range_to','datetime_range_int_from','datetime_range_int_to');
        
        $updateFields = array();
        
        foreach($key_value_array as $field=>$value)
        {
            if(in_array($field,$avaiKeyArray)){
                if($field == 'datetime_range_from' ||  $field == 'datetime_range_to'){
                    if(!empty($value)){
                        $value = date("Y-m-d",strtotime($value));
                    } 
                }
                $updateFields[$field] = $value;                
            }            
        }
        
        //echo("updateFilter");
        if(count($updateFields) > 0){
            $criteria = "rep_id = '$reportID' AND id = '$filterID'";
            $result = Core_Database::update("custom_reports_filters",
                                $updateFields,$criteria
                            );
            //$errors = Core_Api_Error::getInstance();
            //echo("error: ");print_r($errors);
        }
        
        $db = Core_Database::getInstance();  
        //--- insert to  custom_reports_filters_selmulti
        if(isset($key_value_array['SelectMultiple'])){            
            //echo("SelectMultiple: ");print_r($key_value_array['SelectMultiple']);die();
            //delete old value
            $criteria = "filter_id='$filterID'";                 
            $result = $db->delete('custom_reports_filters_selmulti',$criteria);                    
            //insert new value
            $selectArray = $key_value_array['SelectMultiple'];
            
            if(!empty($selectArray) && is_array($selectArray) && count($selectArray)>0)
            {                
                foreach($selectArray as $selectValue){
                    try {                
                        //$selectValue = trim($selectValue);
                        Core_Database::insert("custom_reports_filters_selmulti",
                                array('filter_id'=>$filterID,
                                    'filter_value'=>$selectValue)
                        );                        
                    } catch(Exception $ex) {
                                
                    }                                    
                }
            }
        } 
        
        return 0;
    }
    
    public function getFilterDefaultValue($filterRecord)
    {
        //echo("<br/>getFilterDefaultValue->filterRecord: ");print_r($filterRecord);
        /*
Array
        (
            [string_matching] => SelectMultiple
            [string_value] => 
            [datatype] => string
            [fieldcode] => Project_Name
            [SelectMultiple] => Telesource
        ) 
        */       
        //die();
         $dbDataType = $filterRecord['datatype'];
         $fieldcode = $filterRecord['fieldcode'];
         $filterID = $filterRecord['id'];
         $SelectMultiList = $filterRecord['SelectMultiple'];

         $dataType = $this->getDataType($dbDataType);   
         $specialDateFilters = array('RunTimeDate', 'CurrentYear', 'LastYear', 'CurrentQuater','LastQuarter');   
         
         $dbDefaultValue = "";
         $defaultValue = "";
         //echo("<br/>dataType: '$dataType'");
         if($dataType==self::DATATYPE_DATETIME) {
            $relativeDataRef = "";
            if(isset($filterRecord['datetime_relative_data_ref'])){
                $relativeDataRef = $filterRecord['datetime_relative_data_ref'];
            }
            $from = $filterRecord['datetime_range_from'];    
            $to = $filterRecord['datetime_range_to'];    
            
            if($relativeDataRef==self::DT_Ref_Key_AsEntered){
                if(!empty($from)){
                    $dbDefaultValue =  "$fieldcode  >= '$from'";
                    $defaultValue = "$fieldcode  >= '$from'";
                }
                if(!empty($to)){
                    if(!empty($dbDefaultValue)){
                        $dbDefaultValue .= " AND ";
                        $defaultValue .= " AND ";
                    }
                    $dbDefaultValue .=  "$fieldcode <= '$to'";
                    $defaultValue .= "$fieldcode <= '$to'";
                }                                
            } else if($relativeDataRef==self::DT_Ref_Key_CurrentQuater){
                $range = $this->getCurrentQuarter();
                $rangeBegin = $range['RangeBegin'];
                $rangeEnd = $range['RangeEnd'];
                $dbDefaultValue =  "$fieldcode  >= '$rangeBegin' AND $fieldcode  <= '$rangeEnd'";
                $defaultValue = self::DT_Ref_Text_CurrentQuater;
                
            } else if($relativeDataRef==self::DT_Ref_Key_CurrentYear){
                $range = $this->getCurrentYear();
                $rangeBegin = $range['RangeBegin'];
                $rangeEnd = $range['RangeEnd'];
                $dbDefaultValue =  "$fieldcode  >= '$rangeBegin' AND $fieldcode  <= '$rangeEnd'";
                $defaultValue = self::DT_Ref_Text_CurrentYear;
                
            } else if($relativeDataRef==self::DT_Ref_Key_LastQuarter){
                $range = $this->getLastQuarter();
                $rangeBegin = $range['RangeBegin'];
                $rangeEnd = $range['RangeEnd'];
                $dbDefaultValue =  "$fieldcode  >= '$rangeBegin' AND $fieldcode  <= '$rangeEnd'"; 
                $defaultValue = self::DT_Ref_Text_LastQuarter; 
                
            } else if($relativeDataRef==self::DT_Ref_Key_LastYear){
                $range = $this->getLastYear();
                $rangeBegin = $range['RangeBegin'];
                $rangeEnd = $range['RangeEnd'];
                $dbDefaultValue =  "$fieldcode  >= '$rangeBegin' AND $fieldcode  <= '$rangeEnd'";
                $defaultValue = self::DT_Ref_Text_LastQuarter;  
                
            } else if($relativeDataRef==self::DT_Ref_Key_RunTimeDate){
                $intFrom = $filterRecord['datetime_range_int_from'];    
                $intTo = $filterRecord['datetime_range_int_to'];    
                $range = $this->getRunTimeDate($intFrom,$intTo);
                $rangeBegin = $range['RangeBegin'];
                $rangeEnd = $range['RangeEnd'];
                $dbDefaultValue =  "$fieldcode  >= '$rangeBegin' AND $fieldcode  <= '$rangeEnd'";                       
                if($intFrom==0){
                    $displayIntFrom = '';
                } else {
                    $displayIntFrom = $intFrom < 0? $intFrom : "+ ".$intFrom;    
                }
                if($intTo==0){
                    $displayIntTo = '';
                } else {
                    $displayIntTo = $intTo < 0? $intTo : "+ ".$intTo;    
                }                
                $defaultValue = ">= Today $displayIntFrom and <= Today $displayIntTo";   
                        
            } else {
                if(!empty($from)){
                    $dbDefaultValue =  "$fieldcode  >= '$from'";
                    $defaultValue = "$fieldcode  >= '$from'";
                }
                if(!empty($to)){
                    if(!empty($dbDefaultValue)){
                        $dbDefaultValue .= " AND ";
                        $defaultValue .= " AND ";
                    }
                    $dbDefaultValue .=  "$fieldcode <= '$to'";
                    $defaultValue .=  "$fieldcode <= '$to'";
                } 
            }
            
         } else if($dataType==self::DATATYPE_STRING){
             //string_matching, string_value
             $string_matching = $filterRecord['string_matching'];
             $string_value = $filterRecord['string_value'];
             
             if(!empty($string_value)){
                 if($string_matching==self::String_Matching_ExactMatch){
                     $dbDefaultValue = "$fieldcode = '$string_value'";
                     $defaultValue = "= '$string_value'";                     
                 } else if($string_matching==self::String_Matching_BeginsWith){
                     $dbDefaultValue = "$fieldcode LIKE '$string_value%'";
                     $defaultValue = "BEGINS_WITH '$string_value'";
                 } else if($string_matching==self::String_Matching_Contains){
                     $dbDefaultValue = "$fieldcode LIKE '%$string_value%'";
                     $defaultValue = "CONTAINS '$string_value'";
                 } else if($string_matching==self::String_Matching_EndsWith){
                     $dbDefaultValue = "$fieldcode LIKE '%$string_value'";
                     $defaultValue = "ENDS_WITH '$string_value'";
                 } else if($string_matching==self::String_Matching_IsNot){
                     $dbDefaultValue = "$fieldcode <> '$string_value'";
                     $defaultValue = "IS_NOT '$string_value'";
                 }                  
             }
             
             if($string_matching==self::String_Matching_SelectMultiple){
                 $defaultValue = "SELECT_MULTIPLE";
                 
                 $valueList = array();
                 if(!empty($SelectMultiList) && is_array($SelectMultiList)) {
                     $valueList = $SelectMultiList;
                 } else  if(!empty($filterID)){
                     $valueList = $this->getValueList_ForSelMultiFilter($filterID);
                 }
                 
                 //echo("<br/>SELECT_MULTIPLE-valueList: ");print_r($valueList);
                 $values = ""; 
                 if(!empty($valueList) && count($valueList)>0){
                    foreach($valueList as $value){
                        if(empty($values)){
                            $values = "\"".$value."\"";        
                        } else {
                            $values .= ",\"".$value."\"";
                        }
                    } 
                 }
                 
                 //echo("<br/>SELECT_MULTIPLE-values: ");print_r($values);

                 if(!empty($values)){
                    $dbDefaultValue = "$fieldcode IN ($values)";
                 } else {
                    $dbDefaultValue = "";                         
                 }
                 //echo("<br/>SELECT_MULTIPLE-dbDefaultValue: ");print_r($dbDefaultValue);
             }        

             
         } else if($dataType==self::DATATYPE_BOOLEAN){
             //bool_value = 
             $bool_value = $filterRecord['bool_value'];
             if($bool_value==self::Bool_Value_True){
                 $dbDefaultValue = "$fieldcode = 1";
                 $defaultValue = "True";
             } else if($bool_value==self::Bool_Value_False){
                 $dbDefaultValue = "$fieldcode = 0";
                 $defaultValue = "False";
             } else if($bool_value==self::Bool_Value_Any){
                 $dbDefaultValue = "";
                 $defaultValue = "Any";
             }
         } else if($dataType==self::DATATYPE_NUMERIC){
             //$numeric_base_value = $filterRecord['numeric_base_value'];
             $numeric_range_from = $filterRecord['numeric_range_from'];
             $numeric_range_to = $filterRecord['numeric_range_to'];
             if($dbDataType=='int'){
                 //$numeric_base_value = round($numeric_base_value,0);
                 $numeric_range_from = round($numeric_range_from,0);
                 $numeric_range_to = round($numeric_range_to,0);
             }
             
             if(!empty($numeric_range_from)) {                 
                 if(!empty($numeric_range_to)){
                     $dbDefaultValue = "$fieldcode >= '$numeric_range_from' AND $fieldcode <= '$numeric_range_to'";
                     $defaultValue = ">= $numeric_range_from AND <= $numeric_range_to";
                 } else {
                     $dbDefaultValue = "$fieldcode >= '$numeric_range_from'";
                     $defaultValue = ">= $numeric_range_from";
                 }
             } else if(!empty($numeric_range_to)){
                 if(!empty($numeric_base_value)){
                     //$numeric_range_to += $numeric_base_value;
                 }                     
                 
                 $dbDefaultValue = "$fieldcode <= '$numeric_range_to'";
                 $defaultValue = "<= $numeric_range_to";
             } else {
                 $dbDefaultValue = "";
                 $defaultValue = "";
             }
         }
         $ret = array('DBDefaultValue'=>$dbDefaultValue,'DefaultValue'=>$defaultValue);
         //echo("<br/>ret: ");print_r($ret);
         //die();
         return $ret;
    }

    public function getFilterList($reportID)
    {
        //datetime_range_int_from     datetime_range_int_to
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $fields = array('id','rep_id','fieldcode','filter_at','datetime_range_from','datetime_range_to','string_matching','string_value','bool_value','numeric_range_from','numeric_range_to','datetime_range_int_from','datetime_range_int_to','datetime_relative_data_ref');

        $select->from(array('crf'=>'custom_reports_filters'),$fields);
        $select->join(array('cr'=>'custom_reports'),
                'cr.id = crf.rep_id',
                array('report_type')
        );
        $select->join(array('crtf'=>'custom_reports_tablefields'),
                'crtf.table_code = cr.table AND crtf.field_code = crf.fieldcode',
                array('datatype','field_title')
        );        
        $select->where("crf.rep_id = '$reportID'");
        //print_r($select->__toString());
        $records = Core_Database::fetchAll($select);  
        if(empty($records) || count($records)==0) {
            return null;   
        } else {
           $list = array();
           foreach($records as $rec){
               $filterID = $rec['id'];
               $filterDefaultValue = $this->getFilterDefaultValue($rec);
               $rec['DefaultValue']=$filterDefaultValue['DefaultValue'];
               $dbDataType = $rec['datatype'];
               $dataType = $this->getDataType($dbDataType);   
               if($dataType==self::DATATYPE_DATETIME) {
                    $relativeDataRef = $rec['datetime_relative_data_ref'];
                    if($relativeDataRef==self::DT_Ref_Key_CurrentQuater){
                        $range = $this->getCurrentQuarter();
                        $rec['datetime_range_from'] = $range['RangeBegin'];
                        $rec['datetime_range_to'] = $range['RangeEnd'];
                    } else if($relativeDataRef==self::DT_Ref_Key_CurrentYear){
                        $range = $this->getCurrentYear();
                    } else if($relativeDataRef==self::DT_Ref_Key_LastQuarter){
                        $range = $this->getLastQuarter();
                    } else if($relativeDataRef==self::DT_Ref_Key_LastYear){
                        $range = $this->getLastYear();
                    } else if($relativeDataRef==self::DT_Ref_Key_RunTimeDate){
                        $intFrom = $rec['datetime_range_int_from'];    
                        $intTo = $rec['datetime_range_int_to'];    
                        $range = $this->getRunTimeDate($intFrom,$intTo);
                    } 
                    if(!empty($range) && count($range) > 0){
                        $rec['datetime_range_from'] = $range['RangeBegin'];
                        $rec['datetime_range_to'] = $range['RangeEnd'];                        
                    }
               }               
               
              if($rec['string_matching']==self::String_Matching_SelectMultiple){
                 $valueList = $this->getValueList_ForSelMultiFilter($filterID);
                 $rec['SelectMultiple']=$valueList;
              }        

               
               $list[] = $rec;
           }  
           return $list;
        }        
    }

    public function getFilter($reportID,$filterID)
    {
        //datetime_range_int_from     datetime_range_int_to
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $fields = array('id','rep_id','fieldcode','filter_at','datetime_range_from','datetime_range_to','string_matching','string_value','bool_value','numeric_range_from','numeric_range_to','datetime_range_int_from','datetime_range_int_to','datetime_relative_data_ref');

        $select->from(array('crf'=>'custom_reports_filters'),$fields);
        $select->join(array('cr'=>'custom_reports'),
                'cr.id = crf.rep_id',
                array('report_type')
        );
        $select->join(array('crtf'=>'custom_reports_tablefields'),
                'crtf.table_code = cr.table AND crtf.field_code = crf.fieldcode',
                array('datatype','field_title')
        );        
        $select->where("crf.rep_id = '$reportID'");
        $select->where("crf.id = '$filterID'");
        //print_r($select->__toString());
        $records = Core_Database::fetchAll($select);  
        if(empty($records) || count($records)==0) {
            return null;   
        } else {
           $list = array();
           $rec = $records[0];
           $filterDefaultValue = $this->getFilterDefaultValue($rec);
           $rec['DefaultValue']=$filterDefaultValue['DefaultValue'];
           $dbDataType = $rec['datatype'];
           $dataType = $this->getDataType($dbDataType);   
           if($dataType==self::DATATYPE_DATETIME) {
                $relativeDataRef = $rec['datetime_relative_data_ref'];
                if($relativeDataRef==self::DT_Ref_Key_CurrentQuater){
                    $range = $this->getCurrentQuarter();
                    $rec['datetime_range_from'] = $range['RangeBegin'];
                    $rec['datetime_range_to'] = $range['RangeEnd'];
                } else if($relativeDataRef==self::DT_Ref_Key_CurrentYear){
                    $range = $this->getCurrentYear();
                } else if($relativeDataRef==self::DT_Ref_Key_LastQuarter){
                    $range = $this->getLastQuarter();
                } else if($relativeDataRef==self::DT_Ref_Key_LastYear){
                    $range = $this->getLastYear();
                } else if($relativeDataRef==self::DT_Ref_Key_RunTimeDate){
                    $intFrom = $rec['datetime_range_int_from'];    
                    $intTo = $rec['datetime_range_int_to'];    
                    $range = $this->getRunTimeDate($intFrom,$intTo);
                } 
                if(!empty($range) && count($range) > 0){
                    $rec['datetime_range_from'] = $range['RangeBegin'];
                    $rec['datetime_range_to'] = $range['RangeEnd'];                        
                }
           }
           
           if($rec['string_matching']==self::String_Matching_SelectMultiple){
                 $valueList = $this->getValueList_ForSelMultiFilter($filterID);
                 $rec['SelectMultiple']=$valueList;
           }        
               
           return $rec;
        }        
    }
    
    public function getFilterList_FullData($reportID)
    {
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from(array('crf'=>'custom_reports_filters'),
            array('*')
        );            
        $select->join(array('cr'=>'custom_reports'),
                'cr.id = crf.rep_id',
                array('table','companyid','report_type')
        );
        $select->join(array('crtf'=>'custom_reports_tablefields'),
                'crtf.table_code = cr.table AND crtf.field_code = crf.fieldcode',
                array('datatype')
        );        
        $select->where("crf.rep_id = '$reportID'");
        //print_r($select->__toString());
        $records = Core_Database::fetchAll($select);  
        if(empty($records) || count($records)==0) {
            return null;   
        } else {
           $list = array();
           foreach($records as $rec){
               $filterDefaultValue = $this->getFilterDefaultValue($rec);
               $rec['DefaultValue']=$filterDefaultValue['DefaultValue'];
               $rec['DBDefaultValue']=$filterDefaultValue['DBDefaultValue'];
               $list[] = $rec;
           }  
           return $list;
        }        
    }
    
    public function deleteReportColumn($repID,$fieldCode)
    {        
        //--- delete data from custom_reports_fields
        $criteria = "rep_id = '$repID' AND fieldname = '$fieldCode'";
        $db = Core_Database::getInstance();
        $result = $db->delete('custom_reports_fields',$criteria);        
    }

    public function deleteReportFilter($repID,$filterID)
    {
        $db = Core_Database::getInstance();

        //--- delete data from custom_reports_filters_selmulti
        $criteria = "filter_id = '$filterID'";
        $result = $db->delete('custom_reports_filters_selmulti',$criteria);        

        //--- delete data from custom_reports_filters
        $criteria = "rep_id = '$repID' AND id = '$filterID'";
        $result = $db->delete('custom_reports_filters',$criteria);        
                
        return $result;
    }
    
    /* --------------------------------------------------
    * return: 
    *   - error message if there is a error.
    *   - empty string if adding sucessfully.
    *       
    * ---------------------------------------------------*/
    public function addReportColumn($repID,$fieldCode)
    {
        
        //--- repID is existing in the system?
        $filters = array('rep_id'=>$repID);
        $repID_exists = $this->exists_Report($filters);
        if($repID_exists===false){
            return 'Report ID is not existing.';
        }        
        
        //--- report info, tablefieldInfo
        $reportInfo = $this->getReportInfo($repID);
        $baseTable = $reportInfo['table'];
        
        //--- $fieldCode is in the base table?
        $exists_FieldInTable = $this->exists_FieldInTable($baseTable,$fieldCode);
        if($exists_FieldInTable===false){
            return 'Field is not in the table of the report.';
        }
                
                
        //--- $fieldCode is existing in the report fields?
        $exists_FieldInReport = $this->exists_FieldInReport($repID,$fieldCode);
        if($exists_FieldInReport===true){
            return 'Field is alredy in the report.';
        }
        
        //--- tablefieldInfo
        $tableFieldInfo = $this->getTableFieldInfo($baseTable,$fieldCode);
        $fieldTitle = $tableFieldInfo['field_title'];        
        
        //--- add column rep_id     fieldname     fieldcaption
        $updFields = array();
        $updFields['rep_id'] = $repID;
        $updFields['fieldname'] = $fieldCode;
        $updFields['fieldcaption'] = $fieldTitle;
        $result = Core_Database::insert("custom_reports_fields",$updFields);
        
        return "";
        
    }
    
    /*---------------
    * $filters - keys:
    *   - rep_id
    -----------------*/
    public function exists_Report($filters)
    {
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from('custom_reports');            
        
        if(!empty($filters['rep_id'])){
            $select->where("id = ? ",$filters['rep_id']);
        }
        $records = Core_Database::fetchAll($select);          
        if(empty($records) || count($records)==0){
            return false;
        } else {
            return true;
        }
    }
    
    public function exists_FieldInTable($table,$fieldCode)
    {
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from('custom_reports_tablefields');
        $select->where("table_code = '$table'");
        $select->where("field_code = '$fieldCode'");
        $records = Core_Database::fetchAll($select);          
        if(empty($records) || count($records)==0){
            return false;
        } else {
            return true;
        }         
    }
    //custom_reports_fields rep_id     fieldname
    public function exists_FieldInReport($repID,$fieldCode)
    {
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from('custom_reports_fields');
        $select->where("rep_id = '$repID'");
        $select->where("fieldname = '$fieldCode'");
        $records = Core_Database::fetchAll($select);          
        if(empty($records) || count($records)==0){
            return false;
        } else {
            return true;
        }         
    }
    
    public function getTableFieldInfo($table,$fieldCode)
    {
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from('custom_reports_tablefields');
        $select->where("table_code = '$table'");
        $select->where("field_code = '$fieldCode'");
        $records = Core_Database::fetchAll($select);          
        if(empty($records) || count($records)==0){
            return null;
        } else {
            return $records[0];
        }                 
    }
    
    public function runReport($repID,$filters,$limit=0,$offset=0,$forCount=false,$sorts=null,$groups=null,$counts=null,$sums=null)
    {
        $reportInfo = $this->getReportInfo($repID);
        if(empty($reportInfo)){
            return null;
        }
        $tableCode = $reportInfo['table'];
        $companyID = $reportInfo['companyid'];
        $specialWhere = $reportInfo['special_where'];
        if(empty($companyID)){
            return null;
        }
                
        $clauseWhere = '';
        if(!empty($specialWhere)){
            $clauseWhere = str_replace("{Company_ID}",$companyID,$specialWhere);
        }
        
        $ReportFieldList = $this->getReportFieldList($repID);
        $reportFields = array();

        foreach($ReportFieldList as $field){
            $reportFields[]=$field['fieldname'];       
            if(!empty($counts) && count($counts) > 0)
            {
                foreach($counts as $countFieldName){
                    if($countFieldName==$field['fieldname']){
                        $key = "Count".$field['fieldname'];
                        $reportFields[$key]="Count(".$field['fieldname'].")";
                        break;
                    }
                }
            }
            
            /*             
            if($field['iscount']==1){
                $key = "Count".$field['fieldname'];
                $reportFields[$key]="Count(".$field['fieldname'].")";
            } 
            */
            if(!empty($sums) && count($sums) > 0)
            {
                foreach($sums as $sumFieldName){
                    if($sumFieldName==$field['fieldname']){
                        $key = "Sum".$field['fieldname'];
                        $reportFields[$key]="Sum(".$field['fieldname'].")";
                        break;
                    }
                }
            }
            /*
            if($field['issum']==1){
                $key = "Sum".$field['fieldname'];
                $reportFields[$key]="Sum(".$field['fieldname'].")";
            } 
            */            
        }
        
        $db = Core_Database::getInstance();  
        $select = $db->select();
        //echo("<br/>limit: $limit");
        //echo("<br/>offset: $offset");
        //echo("<br/>forCount: $forCount");
        //echo("<br/>reportFields: ");print_r($reportFields);die();
//$repID,$filters,$limit=0,$offset=0,$forCount=false)
                    
        if($forCount===true){
            $select->from($tableCode,array('NUM'=>'Count(*)'));
        } else if($forCount===false) {
            if(empty($reportFields) || count($reportFields)==0){
                return null;
            }
            $select->from($tableCode,$reportFields);            
        }
        //echo("<br/>sql:");print_r($select->__toString());die();        
        
        if(!empty($clauseWhere)){
            $select->where($clauseWhere);            
        }
        //--- filters
        if(!empty($filters) && count($filters) > 0){
            foreach($filters as $filter){
                $filterDefaultValue = $this->getFilterDefaultValue($filter);
                $DBDefaultValue = $filterDefaultValue['DBDefaultValue'];
                //print_r($filter);print_r($filterDefaultValue);die();
                if(!empty($DBDefaultValue)){
                    $select->where($DBDefaultValue);            
                }
            }
        }
        //--- group by
        if(!empty($groups) && count($groups)>0){
            foreach($groups as $fieldname){
                $select->group($fieldname);
            }
        }        
        //--- sort By
        
        if(!empty($sorts) && count($sorts)>0){
            if(!empty($sorts['sort_1'])){
                $select->order($sorts['sort_1'].' '.$sorts['sort_dir_1']);
            }
            if(!empty($sorts['sort_2'])){
                $select->order($sorts['sort_2'].' '.$sorts['sort_dir_2']);
            }
            if(!empty($sorts['sort_3'])){
                $select->order($sorts['sort_3'].' '.$sorts['sort_dir_3']);
            }
        }
        
        //--- limit, offset
        if(!empty($limit) || !empty($offset)){
            $select->limit($limit,$offset);            
        }
        //echo("<br/><br/>select: ");print_r($select->__toString());
        
        $records = Core_Database::fetchAll($select); 
        
       // echo("<br/><br/>forCount:$forCount");
        //echo("<br/>groupBys:");print_r($groupBys);
        //echo("<br/>records:");print_r($records);
        if($forCount===true){
            $count = 0;
            if(!empty($records) && count($records)>0){
                if(!empty($groupBys) && count($groupBys)>0){
                   $count = count($records);                     
                } else {
                    $count = $records[0]['NUM'];
                }
            } 
            //echo("<br/>count:$count");
            return $count;            
        } else {
            //echo("<br/>sql:");print_r($select->__toString());
            //echo("<br/>reportFields:");print_r($reportFields);
            
            return $records;            
        }
    }
    /*
    $columnArray : example: array('AccountName','UserName','id','FirstName','LastName')
    */
    public function reArrangeColumns($reportID,$columnArray)
    {
        if(empty($columnArray) || count($columnArray)==0){
            return;
        }
        
        for($i=0; $i<count($columnArray);$i++){
            $displayOrder = $i + 1;
            $fieldName = $columnArray[$i];
            $criteria = "rep_id = $reportID AND fieldname = '$fieldName'";
            $fields = array('display_order'=>$displayOrder);
            $result = Core_Database::update("custom_reports_fields",$fields,$criteria);
        }
        return;
    }
    //custom_reports_fields
    public function saveReportFields($repID,$groupID,$fields,$columnArrange=null)
    {
        if(!empty($columnArrange) && count($columnArrange)>0){
            $result = $this->reArrangeColumns($repID,$columnArrange);            
        }
        
        if(empty($fields) || count($fields)==0){
            return;
        }
        
        foreach($fields as $field){
            $fieldName = $field['fieldname']; 
            
            $criteria = "rep_id = '$repID' AND fieldname = '$fieldName'";  
            $fieldsUpdate = array();
            if(isset($field['issum'])){
                $fieldsUpdate['issum'] = $field['issum'];
            }          
            if(isset($field['iscount'])){
                $fieldsUpdate['iscount'] = $field['iscount'];
            }          
            if(isset($field['issortby'])){
                $fieldsUpdate['issortby'] = $field['issortby'];
            }          
            if(isset($field['isgroupby'])){
                $fieldsUpdate['isgroupby'] = $field['isgroupby'];
            }          
            if(isset($field['isdisplayed'])){
                $fieldsUpdate['isdisplayed'] = $field['isdisplayed'];
            }          
                        
            $result = Core_Database::update("custom_reports_fields",$fieldsUpdate,$criteria);
        }        
        return;
    }
    
    //custom_reports
    public function saveReport($repID,$updateFields)
    {
        if(!empty($updateFields) && count($updateFields) > 0){
            $criteria = "id = '$repID'";                        
            $result = Core_Database::update("custom_reports",$updateFields,$criteria);
            return $result;
        }
        return;        
    }
    
    public function exists_ReportName($companyID,$reportName,$excludeName='')
    {
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from(array('cr'=>"custom_reports"),
            array('id','rpt_name','companyid','table')
        );
        $select->where("companyid = '$companyID'");
        $select->where("rpt_name = '$reportName'");
        $select->where("rpt_name <> '$excludeName'");
        
        $records = Core_Database::fetchAll($select);  
        if(!empty($records) && count($records) > 0) {
            return true;
        } else {
            return false;   
        }
    }
    
    public function getValueList_ForSelMultiFilter($filterID)
    {
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from("custom_reports_filters_selmulti",
            array('filter_id','filter_value')
        );
        $select->where("filter_id = '$filterID'");
        
        $records = Core_Database::fetchAll($select);  
        
        $valueList = array();
        if(!empty($records) && count($records) > 0) {
            foreach($records as $rec){
                $valueList[] = $rec['filter_value'];
            }
        } 
        
        return $valueList;
        
    }
    
    public function getValueListOfField($tableCode,$fieldCode,$companyID='')
    {
        //echo("getValueListOfField->companyID: $companyID");die();
        $db = Core_Database::getInstance();
        //--- special where
        $specialWhere = "";
        if(!empty($companyID))
        {
            $select = $db->select();
            $select->from("custom_reports_tables",array('special_where'));
            $select->where("table_code = '$tableCode'");
            $records = Core_Database::fetchAll($select); 
            if(empty($records) || count($records)==0){
                return array();
            }
            
            $specialWhere = $records[0]['special_where'];
            if(!empty($specialWhere)){
                $specialWhere = str_replace("{Company_ID}",$companyID,$specialWhere);
            }
        }
        //--- main
        $fields = array();
        $fields[] = $fieldCode;
        //table_code,field_code   
        $select = $db->select();
        $select->from($tableCode,$fields);
        $select->where("$fieldCode IS NOT NULL OR $fieldCode <>''");
        if(!empty($specialWhere)){
            $select->where($specialWhere);    
        }
        
        $select->group($fieldCode);
       // echo("getValueList: ");print_r($select->__toString());
        $records = Core_Database::fetchAll($select); 
        
        $valueList = array();
        if(!empty($records) && count($records) > 0){
            foreach($records as $rec){
                $valueList[] = $rec[$fieldCode];
            }
        }
        
        return $valueList;
    }
    
    public function getProjects($fields,$filters,$sort='Project_Name ASC')
    {
        
        if(empty($fields)) {
            $fields = array("*");
        }
        
        $db = Core_Database::getInstance();
        $select = $db->select();
        $select->from("projects",$fields);
        if(!empty($filters) && count($filters) > 0){
            foreach($filters as $key=>$value) {
                $select->where("$key = '$value'");
            }
        }
        
        if(!empty($sort)){
            $select->order($sort);
        }
        
        $records = $db->fetchAll($select);
        if(empty($records) || count($records)==0){
            return null;
        } else {
            return $records;
        }
    }
    
    public function getPrjectList_ByCompanyID($companyID)
    {
        $fields = array('Project_ID','Project_Name');
        $filters = array('Project_Company_ID'=>$companyID);
        return $this->getProjects($fields,$filters);
    }
    
    public function getWOStatusList()
    {
        $list = array();
        $list['created']='Created';
        $list['published']='Published';
        $list['assigned']='Assigned';
        $list['work_done']='Work Done';
        $list['incomplete']='Incomplete';
        $list['approved']='Approved';
        $list['in_accounting']='Invoiced Not Yet Paid';
        $list['completed']='Complete (Paid)';
        $list['deactivated']='Deactivated';
        return $list;
    }
    
    public function getFilterDropdownOptions($filterKey,$companyID,$isGPM)
    {
        if($filterKey=='WO_Category_ID'){
            return $this->getOptionsforWOCategory();
        } else if($filterKey=='WorkOrderOwner') {
            return $this->getOptionsforWorkOrderOwner($companyID,$isGPM);
        } else if($filterKey=='Type_ID') {
            return $this->getOptionsforTypeID();
        } else if($filterKey=='Country') {
            return $this->getOptionsforCountry();
        } else if($filterKey=='State') {
            return $this->getOptionsforState();            
        } else if($filterKey=='Lead') {
            return $this->getOptionsYesNo('Select Lead');            
        } else if($filterKey=='Assist') {
            return $this->getOptionsYesNo('Select Assist');            
        } else if($filterKey=='LeadAssist') {
            return $this->getOptionsforLeadAssist();            
        } else if($filterKey=='WorkOrderReviewed') {
            return $this->getOptionsYesNo('Select Tech Accepted');
        } else if($filterKey=='TechCheckedIn_24hrs') {
            return $this->getOptionsYesNo('Select Tech Confirmed');
        } else if($filterKey=='CheckedIn') {
            return $this->getOptionsYesNo('Select Tech Checked In');
        } else if($filterKey=='TechMarkedComplete') {
            return $this->getOptionsYesNo('Select Tech Marked Complete');
        } else if($filterKey=='Audit') {
            return $this->getOptionsforAudit();            
        } else if($filterKey=='Amount_Per') {
            return $this->getOptionsforAmountPer(); 
        } else if($filterKey=='_BiddingTechIsPreferred') {
            return $this->getOptionsYesNo('Select Bidding Tech Preferred'); 
        } else if($filterKey=='_BidHide') {
            return $this->getOptionsYesNo('Select Bid Hidden'); 
        } else if($filterKey=='AbortFee') {
            return $this->getOptionsYesNo('Any'); 
        } else if($filterKey=='Approved') {
            return $this->getOptionsYesNo('Any'); 
        } else if($filterKey=='Invoiced') {
            return $this->getOptionsYesNo('Any'); 
        } else if($filterKey=='PricingRan') {
            return $this->getOptionsYesNo('Any'); 
        } else if($filterKey=='PricingRuleApplied') {
            return $this->getOptionsYesNo('Any'); 
        } else if($filterKey=='PcntDeduct') {
            return $this->getOptionsYesNo('Any'); 
        } else if($filterKey=='TechPaid') {
            return $this->getOptionsYesNo('Any'); 
        } else if($filterKey=='_BiddingTechPreferred') {
            return $this->getOptionsYesNo('Any'); 
        } else if($filterKey=='_Hide') {
            return $this->getOptionsYesNo('Any'); 
        } else if($filterKey=='SignOff_Disabled') {
            return $this->getOptionsYesNo('Any'); 
        } else if($filterKey=='CustomSignOff_Enabled') {
            return $this->getOptionsYesNo('Any'); 
        } else if($filterKey=='Update_Requested') {
            return $this->getOptionsYesNo('Any'); 
        } else if($filterKey=='PreCall_1') {
            return $this->getOptionsYesNo('Any'); 
        } else if($filterKey=='PreCall_2') {
            return $this->getOptionsYesNo('Any'); 
        } else if($filterKey=='CheckedIn') {
            return $this->getOptionsYesNo('Any'); 
        } else if($filterKey=='FLS_OOS') {
            return $this->getOptionsYesNo('Any'); 
        } else if($filterKey=='ContactHD') {
            return $this->getOptionsYesNo('Any'); 
        } else if($filterKey=='Incomplete_Paperwork') {
            return $this->getOptionsYesNo('Any'); 
        } else if($filterKey=='Paperwork_Received') {
            return $this->getOptionsYesNo('Any'); 
        } else if($filterKey=='SignOff_Disabled') {
            return $this->getOptionsYesNo('Any'); 
        } else if($filterKey=='CustomSignOff_Enabled') {
            return $this->getOptionsYesNo('Any'); 
        } else if($filterKey=='TechMarkedComplete') {
            return $this->getOptionsYesNo('Any'); 
        } else if($filterKey=='CallClosed') {
            return $this->getOptionsYesNo('Any'); 
        } else if($filterKey=='ShortNotice') {
            return $this->getOptionsYesNo('Any'); 
        } else if($filterKey=='StoreNotified') {
            return $this->getOptionsYesNo('Any'); 
        } else if($filterKey=='WorkOrderReviewed') {
            return $this->getOptionsYesNo('Any'); 
        } else if($filterKey=='TechCheckedIn_24hrs') {
            return $this->getOptionsYesNo('Any'); 
        } else if($filterKey=='Unexpected_Steps') {
            return $this->getOptionsYesNo('Any'); 
        } else if($filterKey=='Work_Out_of_Scope') {
            return $this->getOptionsYesNo('Any'); 
        } else if($filterKey=='_TechBackedOut') {
            return $this->getOptionsYesNo('Any'); 
        } else if($filterKey=='_TechNoShowed') {
            return $this->getOptionsYesNo('Any'); 
        } else if($filterKey=='DeactivationCode') {
            return $this->getOptionsforDeactiveReason(); 
        } else if($filterKey=='PreCall_1_Status') {
            return $this->getOptionsforPreCall_1_Status(); 
        } else if($filterKey=='PreCall_2_Status') {
            return $this->getOptionsforPreCall_2_Status(); 
        } else if($filterKey=='Penalty_Reason') {
            return $this->getOptionsforPenaltyReason(); 
        } else if($filterKey=='AskedBy') {
            return $this->getOptionsforAskedBy(); 
        } else if($filterKey=='_NewPartCarrier') {
            return $this->getOptionsforCarrier(); 
        } else if($filterKey=='_NewPartShipsTo') {
            return $this->getOptionsforShipTo(); 
        } else if($filterKey=='_ReturnPartCarrier') {
            return $this->getOptionsforCarrier(); 
        } else if($filterKey=='_VisitStartTypeID') {
            return $this->getOptionsforVisitStartType(); 
        } else if($filterKey=='_VisitDayPart') {
            return $this->getOptionsforDayPart(); 
        } else if($filterKey=='_VisitLeadTime') {
            return $this->getOptionsforLeadTime(); 
        } else if($filterKey=='_VisitActualCheckInResult') {
            return $this->getOptionsforCheckInResult(); 
        } else if($filterKey=='_VisitActualCheckOutResult') {
            return $this->getOptionsforCheckOutResult(); 
        } else if($filterKey=='_VisitActualDurationResult') {
            return $this->getOptionsforDurationResult(); 
        } else if($filterKey=='_Geography') {
            return $this->getOptionsforGeography(); 
        } else if($filterKey=='_BidAmount_Per') {
            return $this->getOptionsforAmountPer(); 
        } else if($filterKey=='_TechAmount_Per') {
            return $this->getOptionsforAmountPer(); 
        } else if($filterKey=='_WINTagOwner') {
            return $this->getOptionsforWinTagOwner(); 
        } else if($filterKey=='_WINTagCheckInResult') {
            return $this->getOptionsforCheckInResultWINTag(); 
        } else if($filterKey=='_WINTagCheckOutResult') {
            return $this->getOptionsforCheckOutResultWINTag(); 
        } else if($filterKey=='_WINTagDurationResult') {
            return $this->getOptionsforDurationResultWINTag(); 
        } else if($filterKey=='_WINTag') {
            return $this->getOptionsforWinTag(); 
        } else if($filterKey=='_VisitSiteTimeZone') {
            return $this->getOptionsforSiteTimeZone(); 
        }
        //_WINTagCheckInResult, _WINTagCheckOutResult, _WINTagDurationResult
        
    }
    
    public function getOptionsforSiteTimeZone()
    {
        $options = array();
        $options[]=array('Value'=>'','Caption'=>'Any');
        $options['-10']=array('Value'=>'-10','Caption'=>'Hawaii (GMT-10:00)');
        $options['-9']=array('Value'=>'-9','Caption'=>'Alaska (GMT-8:00)');
        $options['-8']=array('Value'=>'-8','Caption'=>'Pacific (GMT-7:00)');
        $options['-7']=array('Value'=>'-7','Caption'=>'Mountain (GMT-6:00)');
        $options['-6']=array('Value'=>'-6','Caption'=>'Central (GMT-5:00)');
        $options['-5']=array('Value'=>'-5','Caption'=>'Eastern (GMT-4:00)');
        return $options;        
    }
    
    public function getOptionsforWinTag()
    {
        $options = array();
        $options[]=array('Value'=>'','Caption'=>'Any');
        $options['Complete']=array('Value'=>'Complete','Caption'=>'Complete');
        $options['Rescheduled']=array('Value'=>'Rescheduled','Caption'=>'Rescheduled');
        $options['Incomplete']=array('Value'=>'Incomplete','Caption'=>'Incomplete');
        $options['Cancelled']=array('Value'=>'Cancelled','Caption'=>'Cancelled');
        $options['Scheduled']=array('Value'=>'Scheduled','Caption'=>'No');
        return $options;        
    }

    public function getOptionsforWinTagOwner()
    {
        $options = array();
        $options[]=array('Value'=>'','Caption'=>'Select Win-Tag Owner');
        $options['Site / Client']=array('Value'=>'Site / Client','Caption'=>'Site/Client Caused');
        $options['Tech / Provider']=array('Value'=>'Tech / Provider','Caption'=>'Tech/Provider Caused');        
        $options['Natural Causes']=array('Value'=>'Natural Causes','Caption'=>'Natural Causes');        
        return $options;
    }
    
    public function getOptionsforCheckInResultWINTag()
    {
        $options = array();
        $options[]=array('Value'=>'','Caption'=>'Select Check In Result WIN-Tag');
        $options['Early']=array('Value'=>'Early','Caption'=>'Early');
        $options['On Time']=array('Value'=>'On Time','Caption'=>'On Time');
        $options['Late']=array('Value'=>'Late','Caption'=>'Late');
        return $options; 
    }

    public function getOptionsforCheckOutResultWINTag()
    {
        $options = array();
        $options[]=array('Value'=>'','Caption'=>'Select Check Out Result WIN-Tag');
        $options['Early']=array('Value'=>'Early','Caption'=>'Early');
        $options['On Time']=array('Value'=>'On Time','Caption'=>'On Time');
        $options['Late']=array('Value'=>'Late','Caption'=>'Late');
        return $options; 
    }

    public function getOptionsforDurationResultWINTag()
    {
        $options = array();
        $options[]=array('Value'=>'','Caption'=>'Select Duration Result WIN-Tag');
        $options['Under Duration']=array('Value'=>'Under Duration','Caption'=>'Under Duration');
        $options['Correct Duration']=array('Value'=>'Correct Duration','Caption'=>'Correct Duration');
        $options['Over Duration']=array('Value'=>'Over Duration','Caption'=>'Over Duration');
        return $options; 
    }

    public function getOptionsforAskedBy()
    {
        $data = array("Command Center","Customer On-Site","Level 1 Helpdesk","Level 2 Helpdesk","Level 3 Helpdesk");
        
        $options = array();
        $options[]=array('Value'=>'','Caption'=>'Select Asked By');
        foreach($data as $value){
            $key = str_replace(' ','_',$value);
            $options[$key] = array('Value'=>$value,'Caption'=>$value);
        }
        return $options;        
    }

    public function getOptionsforPenaltyReason()
    {
        $data = array("Admin Process","Early Arrival","Late Arrival");
        
        $options = array();
        $options[]=array('Value'=>'','Caption'=>'Select Penalty Reason');
        foreach($data as $value){
            $key = str_replace(' ','_',$value);
            $options[$key] = array('Value'=>$value,'Caption'=>$value);
        }
        return $options;        
    }
    
    public function getOptionsforPreCall_1_Status()
    {
        $data = array("Backed Out","Confirmed","Unconfirmed");
        
        $options = array();
        $options[]=array('Value'=>'','Caption'=>'Select PreCall 1 Status');
        foreach($data as $value){
            $key = str_replace(' ','_',$value);
            $options[$key] = array('Value'=>$value,'Caption'=>$value);
        }
        return $options;        
    }

    public function getOptionsforPreCall_2_Status()
    {
        $data = array("Backed Out","Confirmed","Unconfirmed");
        
        $options = array();
        $options[]=array('Value'=>'','Caption'=>'Select PreCall 2 Status');
        foreach($data as $value){
            $key = str_replace(' ','_',$value);
            $options[$key] = array('Value'=>$value,'Caption'=>$value);
        }
        return $options;        
    }

    
    //WO_Category_ID,Skill Category,D 
    public function getOptionsforWOCategory()
    {
        $db = Core_Database::getInstance();
        $select = $db->select();
        $select->from("wo_categories",array('Category_ID','Category'));
        $select->order("Category");
        $records = $db->fetchAll($select);

        $options = array();
        $options[]=array('Value'=>'','Caption'=>'Select Skill Category');
        if(!empty($records) && count($records)>0){
            foreach($records as $rec){
                $key = $rec['Category_ID'];
                $options[$key]=array('Value'=>$rec['Category_ID'],
                            'Caption'=>$rec['Category']);            
            }
        }
        return $options;
    }
    //WorkOrderOwner,Work Order Owner,D (D, for current company only, if viewing as GPM, include GPMs, multiple selections allowed),
    public function getOptionsforWorkOrderOwner($companyID,$isGPM)
    {
        //--- Client users UserName,ContactName,Admin,ProjectManager
        $db = Core_Database::getInstance();
        $select = $db->select();
        $select->from("clients",
            array('UserName','ContactName','Admin','ProjectManager','Company_ID')
        );
        $select->where("Company_ID = '$companyID'");
        $select->order("ContactName");        
        $records = $db->fetchAll($select);

        $options = array();
        $options['']=array('Value'=>'','Caption'=>'Select Work Order Owner');
        if(!empty($records) && count($records)>0){
            foreach($records as $rec){
                $key = $rec['UserName'];
                $options["$key"]=array('Value'=>$rec['UserName'],
                            'Caption'=>$rec['ContactName'].' - '.$rec['UserName']);       
            }
        }
        //--- for GPM
        if($isGPM){
            $options['.']=array('Value'=>'','Caption'=>'----------------');
            $db = Core_Database::getInstance();
            $select = $db->select();
            $select->from("clients",
                array('UserName','ContactName','Admin','ProjectManager','Company_ID')
            );
            $select->where("Company_ID <> '$companyID'");
            $select->where("Admin = 1");
            $select->where("ProjectManager = 1");
            $select->order("ContactName");        
            $records = $db->fetchAll($select);     
            if(!empty($records) && count($records)>0){
                foreach($records as $rec){
                    $key = $rec['UserName'];
                    $options["$key"]=array('Value'=>$rec['UserName'],
                                'Caption'=>$rec['ContactName'].' - '.$rec['UserName']);
                }
            }
        }
        //--- return                
        return $options;
    }
    //Type_ID,Call Type,D 
    public function getOptionsforTypeID()
    {
        $options = array();
        $options[]=array('Value'=>'','Caption'=>'Call Type');
        $options['1']=array('Value'=>'1','Caption'=>'Install');
        $options['2']=array('Value'=>'2','Caption'=>'Service Call');
        $options['3']=array('Value'=>'3','Caption'=>'Recruiting');
        return $options;
    }
    //Country,Country,D
    public function getOptionsforCountry()
    {
        $CommonClass = new Core_Api_CommonClass();
        $countriesObj = $CommonClass->getCountries();
        $countries = $countriesObj->data;
        $options = array();
        $options[]=array('Value'=>'','Caption'=>'Select Country','Disable'=>'');
        foreach ($countries as $ckey => $country) {
            $disabled ="";
            $code = $country->Code;
            $name = $country->Name;
            
            if($name =="---South America" || $name=="---EMEA" || $name=="---APAC") {
                $disabled ="disabled";
                $code = $name;
            }
            $options[$code]=array('Value'=>$code,'Caption'=>$country->Name,'Disable'=>$disabled);           }
        
        return $options;
    }
    //State,State,D 
    public function getOptionsforState($country='')
    {
        $common = new Core_Api_CommonClass;
        $USStatesObj = $common->getStatesArray('US');
        $USStates = $USStatesObj->data;
        $CAStatesObj = $common->getStatesArray('CA');
        $CAStates = $CAStatesObj->data;
        $MXStatesObj = $common->getStatesArray('MX');
        $MXStates = $MXStatesObj->data;

        $options = array();
        $options[]=array('Value'=>'','Caption'=>'Select State','Disable'=>'');
        
        foreach($USStates as $k=>$v){
            $options['US_'.$k]=array('Value'=>$k,'Caption'=>$v,'Disable'=>'');            
        }
        $options[]=array('Value'=>'---CAStates','Caption'=>'------------','Disable'=>'disabled');
        foreach($CAStates as $k=>$v){
            $options['CA_'.$k]=array('Value'=>$k,'Caption'=>$v,'Disable'=>'');            
        }
        $options[]=array('Value'=>'---MXStates','Caption'=>'------------','Disable'=>'disabled');
        foreach($MXStates as $k=>$v){
            $options['MX_'.$k]=array('Value'=>$k,'Caption'=>$v,'Disable'=>'');            
        }
        
        return $options;
    }
    //LeadAssist,Lead/Assist,D 
    public function getOptionsforLeadAssist()
    {
        $options = array();
        $options[]=array('Value'=>'','Caption'=>'Select Lead/Assist');
        $options['Lead']=array('Value'=>'Lead','Caption'=>'Lead');
        $options['Assist']=array('Value'=>'Assist','Caption'=>'Assist');        
        return $options;
    }
    
    public function getOptionsYesNo($title)
    {
        $options = array();
        $options[]=array('Value'=>'','Caption'=>"$title");
        $options['Y']=array('Value'=>'Y','Caption'=>'Yes');
        $options['N']=array('Value'=>'N','Caption'=>'No');        
        return $options;
    }

    
    //ClientCredential,Client Credential, D (D, displaying all credentials that are visible to the current user, returns WO’s from techs with the selected filter)
    public function getOptionsforClientCredential($companyID,$isGPM)
    {
        $options = array();
        $options[]=array('Value'=>'','Caption'=>'Select Client Credential');
        return $options;
    }
    
    //Audit,Audit,D (D, Needed, Complete),
    public function getOptionsforAudit()
    {
        $options = array();
        $options[]=array('Value'=>'','Caption'=>'Select Audit');
        $options['AuditNeeded']=array('Value'=>'AuditNeeded','Caption'=>'Needed');
        $options['AuditComplete']=array('Value'=>'AuditComplete','Caption'=>'Complete');
        return $options;
    }
    //Amount_Per,Pay Structure,D  (D, Amount Per)
    public function getOptionsforAmountPer()
    {
        $options = array();
        $options[]=array('Value'=>'','Caption'=>'Select Pay Structure');
        $options['Site']=array('Value'=>'Site','Caption'=>'Site');
        $options['Hour']=array('Value'=>'Hour','Caption'=>'Hour');
        $options['Device']=array('Value'=>'Device','Caption'=>'Device');
        $options['Visit']=array('Value'=>'Visit','Caption'=>'Visit');
        return $options;
    }

    public function getOptionsforCarrier()
    {
        $options = array();
        $options[]=array('Value'=>'','Caption'=>'Select Carrier');
        $options['Fedex']=array('Value'=>'Fedex','Caption'=>'Fedex');
        $options['USPS']=array('Value'=>'USPS','Caption'=>'USPS');
        $options['DHL']=array('Value'=>'DHL','Caption'=>'DHL');
        $options['Pilot']=array('Value'=>'Pilot','Caption'=>'Pilot');
        $options['Ceva']=array('Value'=>'Ceva','Caption'=>'Ceva');
        $options['AIT']=array('Value'=>'AIT','Caption'=>'AIT');
        return $options;        
    }

    public function getOptionsforShipTo()
    {
        $options = array();
        $options[]=array('Value'=>'','Caption'=>'Select Ship To');
        $options['Site']=array('Value'=>'Site','Caption'=>'Site');
        $options['Tech']=array('Value'=>'Tech','Caption'=>'Tech');
        $options['Fedex Hold']=array('Value'=>'Fedex Hold','Caption'=>'Fedex Hold');
        $options['UPS Store Hold']=array('Value'=>'UPS Store Hold','Caption'=>'UPS Store Hold');
        $options['USPS Hold']=array('Value'=>'USPS Hold','Caption'=>'USPS Hold');        
        $options['Other']=array('Value'=>'Other','Caption'=>'Other');
        return $options;        
    }
    
    public function getOptionsforDeactiveReason()
    {
        $data = array("Work order kicked back","Work order declined","Not Sourced","No bidding techs (within time needed)","Not approved","Reassigned with a new work order","Recruiting work order","Rescheduled work","Sourced elsewhere","Tech no showed","Work order cancelled","Duplicate Work Order","EU cxl - maximum call attempts");
        
        $options = array();
        $options[]=array('Value'=>'','Caption'=>'Select Deactivation Code');
        foreach($data as $value){
            $key = str_replace(' ','_',$value);
            $options[$key] = array('Value'=>$value,'Caption'=>$value);
        }
        return $options;
    }
    
    public function getOptionsforVisitStartType()
    {
        $options = array();
        $options[]=array('Value'=>'','Caption'=>'Select Start Type');
        $options['1']=array('Value'=>'1','Caption'=>'Firm Start Time');
        $options['2']=array('Value'=>'2','Caption'=>'Start Time Window');
        return $options;        
    } 
    
    public function getOptionsforDayPart()
    {
        $options = array();
        $options[]=array('Value'=>'','Caption'=>'Select Day Part');
        $options['Day']=array('Value'=>'Day','Caption'=>'Day');
        $options['Evening']=array('Value'=>'Evening','Caption'=>'Evening');
        $options['Night']=array('Value'=>'Night','Caption'=>'Night');
        $options['Weekend']=array('Value'=>'Weekend','Caption'=>'Weekend');
        return $options;        
    } 

    public function getOptionsforLeadTime()
    {
        $options = array();
        $options[]=array('Value'=>'','Caption'=>'Select Lead Time');
        $options['Scheduled']=array('Value'=>'Scheduled','Caption'=>'Scheduled');
        $options['Next Business Day']=array('Value'=>'Next Business Day','Caption'=>'Next Business Day');
        $options['Same Business Day']=array('Value'=>'Same Business Day','Caption'=>'Same Business Day');
        return $options;        
    } 
    
    public function getOptionsforCheckInResult()
    {
        $options = array();
        $options[]=array('Value'=>'','Caption'=>'Select Check In Result');
        $options['Early']=array('Value'=>'Early','Caption'=>'Early');
        $options['OnTime']=array('Value'=>'OnTime','Caption'=>'On Time');
        $options['Late']=array('Value'=>'Late','Caption'=>'Late');
        return $options;        
    } 
    
    public function getOptionsforCheckOutResult()
    {
        $options = array();
        $options[]=array('Value'=>'','Caption'=>'Select Check Out Result');
        $options['Early']=array('Value'=>'Early','Caption'=>'Early');
        $options['OnTime']=array('Value'=>'OnTime','Caption'=>'On Time');
        $options['Late']=array('Value'=>'Late','Caption'=>'Late');
        return $options;        
    } 

    public function getOptionsforDurationResult()
    {
        $options = array();
        $options[]=array('Value'=>'','Caption'=>'Select Duration Result');
        $options['UnderDuration']=array('Value'=>'UnderDuration','Caption'=>'Under Duration');
        $options['CorrectDuration']=array('Value'=>'CorrectDuration','Caption'=>'Correct Duration');
        $options['OverDuration']=array('Value'=>'OverDuration','Caption'=>'Over Duration');
        return $options;        
    } 
    
    public function getOptionsforGeography()
    {
        $options = array();
        $options[]=array('Value'=>'','Caption'=>'Select Geography');
        $options['Urban/Metro']=array('Value'=>'Urban/Metro','Caption'=>'Urban/Metro');
        $options['Suburban']=array('Value'=>'Suburban','Caption'=>'Suburban');
        $options['Rural']=array('Value'=>'Rural','Caption'=>'Rural');
        $options['Remote']=array('Value'=>'Remote','Caption'=>'Remote');        
        return $options;        
        
    }
    
    public function getBasicWOInformationFields($companyID,$isGPM)
    {
        $list = array();
        $list['WIN_NUM']='WIN#';
        $list['WO_ID']='Client Work Order ID#';
        $list['Status']='Current Status';
        $list['Project_Name']='Project Name';
        $list['Project_ID']='Project ID';
        if($isGPM){
            $list['ServiceTypeName']='Service Type';                   
            $list['EntityTypeName']='Legal Entity';            
            $list['CSDName']='Client Service Director';            
            $list['AMName']='Account Manager';            
        }
        $list['PO']='Purchase Order #';
        $list['WO_Category_ID']='Skill Category';
        $list['Type_ID']='Call Type';
        $list['Headline']='Headline';
        
        $list['StartTypeName']='Start Type';
        $list['EstimatedDuration']='Estimated Duration';
        $list['TechArrivalInstructions']='Arrival Instructions';
        $list['Qty_Visits']='# Visits';
        
        if($companyID=='FLS'){
            $list['Update_Requested']='Update Requested';
            $list['Date_Update_Requested']='Date Update Requested ';
            $list['Update_Reason']='Reason for Update';
            $list['ShortNotice']='Short Notice';
        }
        
        $list['Description']='Work Description';        
        $list['Requirements']='Tech Requirements & Tools';
        $list['SpecialInstructions']='Special Instructions – All';
        $list['Ship_Contact_Info']='Special Instructions – Assigned Only';
        $list['LeadAssist']='Lead / Assist';
        return $list;
    }
    
    public function getBasicWOInformationDefaultFields()
    {
        $list = array();
        $list['WIN_NUM']='WIN#';
        $list['WO_ID']='Client Work Order ID#';
        $list['Status']='Current Status';
        $list['Project_Name']='Project Name';
        return $list;
    }
    
    public function add2($companyID,$repName,$repTable,$fieldArray,$filterArray=null,$reportType=1,$RRDParams=null)
    {
        //echo("filterArray: <pre>");print_r($filterArray);echo("</pre>");die();
        //work_orders
        if($this->exists_ReportName($companyID,$repName)) return -1;

        $authUser = $_SESSION['Auth_User'];
        $loginName = $authUser['login'];
        $nameOfCurrentUser = $authUser['name'];
        
        //--- insert into custom_reports
        $fieldvals = array("rpt_name"=>$repName,
                        "table"=>$repTable,
                        "companyid"=>$companyID,
                        "creation_date"=>date("Y-m-d H:i:s"),
                        'lastupdated_date'=>date("Y-m-d H:i:s"),
                        "last_modified_by"=>$loginName,
                        "last_modified_by_name"=>$nameOfCurrentUser,
                        "created_by"=>$loginName,
                        "created_by_name"=>$nameOfCurrentUser,
                        'report_type'=>$reportType
                        );  
                         
        $RRDAvailableFields = array('RRDEnable','RRDEmailTo'
                    ,'RRDEmailFromName','RRDEmailFrom'
                    ,'RRDEmailSubject','RRDEmailMessage'
                    ,'RRDEmailSchedule','RRDTimeOfDay'
                    ,'RRDTimeZone','RRDWeeklyDays'
                    ,'RRDMonthlyScheduleOptions','RRDMonthlyScheduleDayNum'
                    ,'RRDMonthlyScheduleDayOrder','RRDMonthlyScheduleDayOrderDay'
                    ,'RRDDailyOptions','RRDDailyEveryDaySpaceOutNum'
                    ,'RRDDailyEveryWeekday','RRDWeeklySpaceOutNum'
                    ,'RRDMonthlyDayNumSpaceOutNum','RRDMonthlyDayOrderSpaceOutNum'
        );
        
        if(!empty($RRDParams) && count($RRDParams)>0){
            foreach($RRDParams as $key=>$val){
                if(in_array($key,$RRDAvailableFields)){
                    $fieldvals[$key] = $val;
                }
            }
        }
        //echo('<br/>$RRDParams: ');print_r($RRDParams);
        //echo('<br/>$fieldvals: ');print_r($fieldvals);
        $result = Core_Database::insert("custom_reports",$fieldvals);
        //die();

        //--- insert into custom_reports_fields
        $report = $this->getReportByName($repName,$companyID);
        if(empty($report)) return;
        
        $rep_id = $report['id'];
        if(empty($rep_id)) return;
        
        foreach($fieldArray as $field)
        { 
            $fieldvals = array("rep_id"=>$rep_id,
                        "fieldname"=>$field,
                        "fieldcaption"=>$field
            );
            $result = Core_Database::insert("custom_reports_fields",$fieldvals);
        }   

        if(!empty($filterArray) && count($filterArray)>0){
            foreach($filterArray as $filter){
                $this->addFilter2($rep_id,$filter);
            }
        }
        
        //---
        return $rep_id;
    }
    
    public function addFilter2($reportID,$key_value_array)
    {
        if(empty($reportID) || empty($key_value_array) || !is_array($key_value_array) || count($key_value_array)==0){
            return -2;
        }
        
        if(empty($key_value_array['fieldcode'])){
            return -3;
        }

        $fieldCode = $key_value_array['fieldcode'];
        $filterAt = $key_value_array['filter_at'];
        
        //--- insert to custom_reports_filters table 
        $avaiKeyArray = array('fieldcode','filter_at','datetime_relative_data_ref','datetime_range_from','datetime_range_to','string_matching','string_value','bool_value','numeric_base_value','numeric_range_from','numeric_range_to','datetime_range_int_from','datetime_range_int_to','time_from','time_to','time_tz');
        
        $addFields = array();
        $addFields['rep_id'] = $reportID;
        
        foreach($key_value_array as $field=>$value)
        {
            if(in_array($field,$avaiKeyArray)){
                if($field == 'datetime_range_from' ||  $field == 'datetime_range_to'){
                    if(!empty($value)){
                        $value = date("Y-m-d",strtotime($value));
                    }
                }
                $addFields[$field] = $value;                
            }            
        }
        
        if(isset($key_value_array['SelectMultiple'])) {
            $addFields['string_matching']='SelectMultiple';
        }
                
        if(count($addFields) > 0){
            $result = Core_Database::insert("custom_reports_filters",$addFields);
            //$errors = Core_Api_Error::getInstance();
            //echo("error: ");print_r($errors);
        }
        
        //--- get filterID
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from('custom_reports_filters',array('id'));            
        $select->where("rep_id = '$reportID'");
        $select->where("fieldcode = '$fieldCode'");
        $select->where("filter_at = '$filterAt'");
        $select->order("id DESC");
        $records = Core_Database::fetchAll($select);  
        if(empty($records) && count($records)>0){
            return -1;
        } 
        
        $filterID = $records[0]['id'];
        
        //--- insert to  custom_reports_filters_selmulti
        if(isset($key_value_array['SelectMultiple'])){
            $selectArray = $key_value_array['SelectMultiple'];
            if(!empty($selectArray) && count($selectArray)>0)
            {
                foreach($selectArray as $selectValue){
                    try {                
                        $selectValue = trim($selectValue);
                        Core_Database::insert("custom_reports_filters_selmulti",
                                array('filter_id'=>$filterID,
                                    'filter_value'=>$selectValue)
                        );                        
                    } catch(Exception $ex) {
                                
                    }                                    
                }
            }
        }

        return 0;
    }
    
    public function getFilterList2($reportID)
    {
        //GPMOnly,FLSOnly     
        //datetime_range_int_from     datetime_range_int_to
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $fields = array('id','rep_id','fieldcode','filter_at','datetime_range_from','datetime_range_to','string_matching','string_value','datetime_relative_data_ref','numeric_range_from','numeric_range_to','time_from','time_to','time_tz');

        $select->from(array('crf'=>'custom_reports_filters'),$fields);
        $select->join(array('cr'=>'custom_reports'),
                'cr.id = crf.rep_id',
                array('report_type')
        );
        $select->joinLeft(array('crtf'=>'custom_reports_tablefields'),
                'crtf.table_code = cr.table AND crtf.field_code = crf.fieldcode',
                array('datatype','field_title','FilterLocation','FilterType','FLSOnly','GPMOnly')
        );        
        $select->where("crf.rep_id = '$reportID'");
        $select->where("crtf.FilterType IS NOT NULL AND crtf.FilterType <> ''");
        //echo("<br/>");print_r($select->__toString());
        $records = Core_Database::fetchAll($select);  
        //echo("<br/>records: ");print_r($records);
        if(empty($records) || count($records)==0) {
            return null;   
        } else {
$Today = date("m/d/Y");
$nforToday = date("N");//1 (for Monday) through 5 (for Friday),  6(Saturday) 7 (for Sunday)
$Tomorrow = date("m/d/Y", mktime(0, 0, 0, date("m"), date("d") + 1, date("Y")));
//Next Business Day, Next 2 Business days, Next 3 Business days, Next 4 Business days, Next 5 Business days
if($nforToday<=4){
    $NextBusinessDay = date("m/d/Y", mktime(0, 0, 0, date("m"), date("d") + 1, date("Y")));    
} else {
    $NextBusinessDay = date("m/d/Y", mktime(0, 0, 0, date("m"), date("d") - $this->getDay(date("N")) + 9, date("Y")));    
}

$nbd = strtotime($NextBusinessDay);
$nforNBD = date("N",strtotime($NextBusinessDay)); // 1->5

if($nforNBD <= 4){
    $EONext2Businessdays = date("m/d/Y", mktime(0, 0, 0, date("m",$nbd), date("d",$nbd) + 1, date("Y",$nbd)));    
} else if($nforNBD == 5){
    $EONext2Businessdays = date("m/d/Y", mktime(0, 0, 0, date("m",$nbd), date("d",$nbd) + 3, date("Y",$nbd)));    
}

if($nforNBD <= 3){
    $EONext3Businessdays = date("m/d/Y", mktime(0, 0, 0, date("m",$nbd), date("d",$nbd) + 2, date("Y",$nbd)));    
} else if($nforNBD >= 4){
    $EONext3Businessdays = date("m/d/Y", mktime(0, 0, 0, date("m",$nbd), date("d",$nbd) + 4, date("Y",$nbd)));    
}

if($nforNBD <= 2){
    $EONext4Businessdays = date("m/d/Y", mktime(0, 0, 0, date("m",$nbd), date("d",$nbd) + 3, date("Y",$nbd)));    
} else if($nforNBD >= 3){
    $EONext4Businessdays = date("m/d/Y", mktime(0, 0, 0, date("m",$nbd), date("d",$nbd) + 5, date("Y",$nbd)));    
}

if($nforNBD == 1){
    $EONext5Businessdays = date("m/d/Y", mktime(0, 0, 0, date("m",$nbd), date("d",$nbd) + 4, date("Y",$nbd)));    
} else if($nforNBD >= 2){
    $EONext5Businessdays = date("m/d/Y", mktime(0, 0, 0, date("m",$nbd), date("d",$nbd) + 6, date("Y",$nbd)));    
}
//Next2days, Next3days, Next4days, Next5Days
$EONext2days = date("m/d/Y", mktime(0, 0, 0, date("m"), date("d") + 2, date("Y")));
$EONext3days = date("m/d/Y", mktime(0, 0, 0, date("m"), date("d") + 3, date("Y")));
$EONext4days = date("m/d/Y", mktime(0, 0, 0, date("m"), date("d") + 4, date("Y")));
$EONext5days = date("m/d/Y", mktime(0, 0, 0, date("m"), date("d") + 5, date("Y")));
// This Week (Mon = 1 - Sun = 7)
$BOW = date("m/d/Y", mktime(0, 0, 0, date("m"), date("d") - $this->getDay(date("N")) + 1, date("Y")));
$EOW = date("m/d/Y", mktime(0, 0, 0, date("m"), date("d") - $this->getDay(date("N")) + 7, date("Y")));
// This Month
$BOM = date("m/d/Y", mktime(0, 0, 0, date("m"), date("d") - date("d") + 1, date("Y")));
$EOM = date("m/d/Y", mktime(0, 0, 0, date("m") + 1, date("d") - date("d"), date("Y")));
// Next Month
$BONextMonth = date("m/d/Y", mktime(0, 0, 0, date("m") + 1, date("d") - date("d") + 1, date("Y")));
$EONextMonth = date("m/d/Y", mktime(0, 0, 0, date("m") + 2, date("d") - date("d"), date("Y")));
// Yesterday
$Yesterday = date("m/d/Y", mktime(0, 0, 0, date("m"), date("d") - 1, date("Y")));

//Last Business Day, Last 2 Business days, Last 3 Business days, Last 4 Business days, Last 5 Business days
if($nforToday == 1){
    $LastBusinessDay = date("m/d/Y", mktime(0, 0, 0, date("m"), date("d") - 3, date("Y")));
} else if($nforToday >=2 && $nforToday <=6 ) {
    $LastBusinessDay = date("m/d/Y", mktime(0, 0, 0, date("m"), date("d") - 1, date("Y")));    
} else if($nforToday==7){
    $LastBusinessDay = date("m/d/Y", mktime(0, 0, 0, date("m"), date("d") - 2, date("Y")));    
}
$lbd = strtotime($LastBusinessDay);
$nforLBD = date("N",strtotime($LastBusinessDay)); // 1->5

if($nforLBD == 1){
    $BOLast2Businessdays = date("m/d/Y", mktime(0, 0, 0, date("m",$lbd), date("d",$lbd) - 3, date("Y",$lbd)));    
} else {
    $BOLast2Businessdays = date("m/d/Y", mktime(0, 0, 0, date("m",$lbd), date("d",$lbd) - 1, date("Y",$lbd)));    
}

if($nforLBD <= 2){
    $BOLast3Businessdays = date("m/d/Y", mktime(0, 0, 0, date("m",$lbd), date("d",$lbd) - 4, date("Y",$lbd)));    
} else {
    $BOLast3Businessdays = date("m/d/Y", mktime(0, 0, 0, date("m",$lbd), date("d",$lbd) - 2, date("Y",$lbd)));    
}

if($nforLBD <= 3){
    $BOLast4Businessdays = date("m/d/Y", mktime(0, 0, 0, date("m",$lbd), date("d",$lbd) - 5, date("Y",$lbd)));    
} else {
    $BOLast4Businessdays = date("m/d/Y", mktime(0, 0, 0, date("m",$lbd), date("d",$lbd) - 3, date("Y",$lbd)));    
}

if($nforLBD <= 4){
    $BOLast5Businessdays = date("m/d/Y", mktime(0, 0, 0, date("m",$lbd), date("d",$lbd) - 6, date("Y",$lbd)));    
} else {
    $BOLast5Businessdays = date("m/d/Y", mktime(0, 0, 0, date("m",$lbd), date("d",$lbd) - 4, date("Y",$lbd)));    
}

// Last2days, Last3days, Last4days, Last5days
$BOLast2days = date("m/d/Y", mktime(0, 0, 0, date("m"), date("d") - 2, date("Y")));
$BOLast3days = date("m/d/Y", mktime(0, 0, 0, date("m"), date("d") - 3, date("Y")));
$BOLast4days = date("m/d/Y", mktime(0, 0, 0, date("m"), date("d") - 4, date("Y")));
$BOLast4days = date("m/d/Y", mktime(0, 0, 0, date("m"), date("d") - 5, date("Y")));


// last Week (Mon = 1 - Sun = 7)
$BOLW = date("m/d/Y", mktime(0, 0, 0, date("m"), date("d") - $this->getDay(date("N")) - 6, date("Y")));
$EOLW = date("m/d/Y", mktime(0, 0, 0, date("m"), date("d") - $this->getDay(date("N")), date("Y")));
// Last Month
$PrevBOM = date("m/d/Y", mktime(0, 0, 0, date("m") - 1, date("d") - date("d") + 1, date("Y")));
$PrevEOM = date("m/d/Y", mktime(0, 0, 0, date("m"), date("d") - date("d"), date("Y")));
// Q1
$Q1Start = date("m/d/Y", mktime(0, 0, 0, 1, 1, date("Y")));
$Q1End = date("m/d/Y", mktime(0, 0, 0, 4, date("d") - date("d"), date("Y")));
// Q2
$Q2Start = date("m/d/Y", mktime(0, 0, 0, 4, 1, date("Y")));
$Q2End = date("m/d/Y", mktime(0, 0, 0, 7, date("d") - date("d"), date("Y")));
// Q3
$Q3Start = date("m/d/Y", mktime(0, 0, 0, 7, 1, date("Y")));
$Q3End = date("m/d/Y", mktime(0, 0, 0, 10, date("d") - date("d"), date("Y")));
// Q4
$Q4Start = date("m/d/Y", mktime(0, 0, 0, 10, 1, date("Y")));
$Q4End = date("m/d/Y", mktime(0, 0, 0, 13, date("d") - date("d"), date("Y")));
// H1
$H1Start = date("m/d/Y", mktime(0, 0, 0, 1, 1, date("Y")));
$H1End = date("m/d/Y", mktime(0, 0, 0, 7, date("d") - date("d"), date("Y")));
// H2
$H2Start = date("m/d/Y", mktime(0, 0, 0, 7, 1, date("Y")));
$H2End = date("m/d/Y", mktime(0, 0, 0, 13, date("d") - date("d"), date("Y")));
// YTD
$YTDStart = date("m/d/Y", mktime(0, 0, 0, 1, 1, date("Y")));
$YTDEnd = date("m/d/Y", mktime(0, 0, 0, date("m"), date("d"), date("Y")));
            
           $list = array();
           //print_r($records);

           foreach($records as $rec){
               $filterID = $rec['id'];
               $relativeDataRef = $rec['datetime_relative_data_ref'];
                if($relativeDataRef==self::DT_Ref_Key_Today){
                    $rec['datetime_range_from'] = $Today;
                    $rec['datetime_range_to'] = $Today;
                } else if($relativeDataRef==self::DT_Ref_Key_Tomorrow){
                    $rec['datetime_range_from'] = $Tomorrow;
                    $rec['datetime_range_to'] = $Tomorrow;
                } else if($relativeDataRef==self::DT_Ref_Key_TodayAndTomorrow){
                    $rec['datetime_range_from'] = $Today;
                    $rec['datetime_range_to'] = $Tomorrow;
                } else if($relativeDataRef==self::DT_Ref_Key_TodayAndNextBusinessDay){
                    $rec['datetime_range_from'] = $Today;
                    $rec['datetime_range_to'] = $NextBusinessDay;
                } else if($relativeDataRef==self::DT_Ref_Key_NextBusinessDay){
                    $rec['datetime_range_from'] = $NextBusinessDay;
                    $rec['datetime_range_to'] = $NextBusinessDay;
                    
                } else if($relativeDataRef==self::DT_Ref_Key_Next2days){
                    $rec['datetime_range_from'] = $Tomorrow;
                    $rec['datetime_range_to'] = $EONext2days;
                } else if($relativeDataRef==self::DT_Ref_Key_Next2Businessdays){
                    $rec['datetime_range_from'] = $NextBusinessDay;
                    $rec['datetime_range_to'] = $EONext2Businessdays;
                    
                } else if($relativeDataRef==self::DT_Ref_Key_Next3days){
                    $rec['datetime_range_from'] = $Tomorrow;
                    $rec['datetime_range_to'] = $EONext3days;
                } else if($relativeDataRef==self::DT_Ref_Key_Next3Businessdays){
                    $rec['datetime_range_from'] = $NextBusinessDay;
                    $rec['datetime_range_to'] = $EONext3Businessdays;
                    
                } else if($relativeDataRef==self::DT_Ref_Key_Next4days){
                    $rec['datetime_range_from'] = $Tomorrow;
                    $rec['datetime_range_to'] = $EONext4days;
                } else if($relativeDataRef==self::DT_Ref_Key_Next4BusinessDays){
                    $rec['datetime_range_from'] = $NextBusinessDay;
                    $rec['datetime_range_to'] = $EONext4Businessdays;
                    
                } else if($relativeDataRef==self::DT_Ref_Key_Next5Days){
                    $rec['datetime_range_from'] = $Tomorrow;
                    $rec['datetime_range_to'] = $EONext5days;
                } else if($relativeDataRef==self::DT_Ref_Key_Next5BusinessDays){
                    $rec['datetime_range_from'] = $NextBusinessDay;
                    $rec['datetime_range_to'] = $EONext5Businessdays;
                    
                } else if($relativeDataRef==self::DT_Ref_Key_ThisWeek){
                    $rec['datetime_range_from'] = $BOW;
                    $rec['datetime_range_to'] = $EOW;
                } else if($relativeDataRef==self::DT_Ref_Key_ThisMonth){
                    $rec['datetime_range_from'] = $BOM;
                    $rec['datetime_range_to'] = $EOM;
                } else if($relativeDataRef==self::DT_Ref_Key_NextMonth){
                    $rec['datetime_range_from'] = $BONextMonth;
                    $rec['datetime_range_to'] = $EONextMonth;
                } else if($relativeDataRef==self::DT_Ref_Key_Yesterday){
                    $rec['datetime_range_from'] = $Yesterday;
                    $rec['datetime_range_to'] = $Yesterday;
                } else if($relativeDataRef==self::DT_Ref_Key_LastBusinessDay){
                    $rec['datetime_range_from'] = $LastBusinessDay;
                    $rec['datetime_range_to'] = $LastBusinessDay;
                } else if($relativeDataRef==self::DT_Ref_Key_Last2days){
                    $rec['datetime_range_from'] = $BOLast2days;
                    $rec['datetime_range_to'] = $Yesterday;
                } else if($relativeDataRef==self::DT_Ref_Key_Last2Businessdays){
                    $rec['datetime_range_from'] = $BOLast2Businessdays;
                    $rec['datetime_range_to'] = $Yesterday;
                } else if($relativeDataRef==self::DT_Ref_Key_Last3days){
                    $rec['datetime_range_from'] = $BOLast3days;
                    $rec['datetime_range_to'] = $Yesterday;
                } else if($relativeDataRef==self::DT_Ref_Key_Last3Businessdays){
                    $rec['datetime_range_from'] = $BOLast3Businessdays;
                    $rec['datetime_range_to'] = $Yesterday;
                } else if($relativeDataRef==self::DT_Ref_Key_Last4days){
                    $rec['datetime_range_from'] = $BOLast4days;
                    $rec['datetime_range_to'] = $Yesterday;
                } else if($relativeDataRef==self::DT_Ref_Key_Last4Businessdays){
                    $rec['datetime_range_from'] = $BOLast4Businessdays;
                    $rec['datetime_range_to'] = $Yesterday;
                } else if($relativeDataRef==self::DT_Ref_Key_Last5days){
                    $rec['datetime_range_from'] = $BOLast5days;
                    $rec['datetime_range_to'] = $Yesterday;
                } else if($relativeDataRef==self::DT_Ref_Key_Last5Businessdays){
                    $rec['datetime_range_from'] = $BOLast5Businessdays;
                    $rec['datetime_range_to'] = $Yesterday;
                } else if($relativeDataRef==self::DT_Ref_Key_LastWeek){
                    $rec['datetime_range_from'] = $BOLW;
                    $rec['datetime_range_to'] = $EOLW;
                } else if($relativeDataRef==self::DT_Ref_Key_LastMonth){
                    $rec['datetime_range_from'] = $PrevBOM;
                    $rec['datetime_range_to'] = $PrevEOM;
                } else if($relativeDataRef==self::DT_Ref_Key_Q1){
                    $rec['datetime_range_from'] = $Q1Start;
                    $rec['datetime_range_to'] = $Q1End;
                } else if($relativeDataRef==self::DT_Ref_Key_Q2){
                    $rec['datetime_range_from'] = $Q2Start;
                    $rec['datetime_range_to'] = $Q2End;
                } else if($relativeDataRef==self::DT_Ref_Key_Q3){
                    $rec['datetime_range_from'] = $Q3Start;
                    $rec['datetime_range_to'] = $Q3End;
                } else if($relativeDataRef==self::DT_Ref_Key_Q4){
                    $rec['datetime_range_from'] = $Q4Start;
                    $rec['datetime_range_to'] = $Q4End;
                } else if($relativeDataRef==self::DT_Ref_Key_H1){
                    $rec['datetime_range_from'] = $H1Start;
                    $rec['datetime_range_to'] = $H1End;
                } else if($relativeDataRef==self::DT_Ref_Key_H2){
                    $rec['datetime_range_from'] = $H2Start;
                    $rec['datetime_range_to'] = $H2End;
                } else if($relativeDataRef==self::DT_Ref_Key_YTD){
                    $rec['datetime_range_from'] = $YTDStart;
                    $rec['datetime_range_to'] = $YTDEnd;
                } 
               
              if($rec['string_matching']==self::String_Matching_SelectMultiple){
                 $valueList = $this->getValueList_ForSelMultiFilter($filterID);
                 $rec['SelectMultiple']=$valueList;
              }        
               
               if(empty($rec['field_title'])){
                   $rec['field_title'] = $rec['fieldcode'];
               }
               $key = $rec['fieldcode'];
               $list[$key] = $rec;
           }  
           return $list;
        }        
    }

    
    public function getReportInfo2($repID)
    {

//sort_1     sort_dir_1     sort_2     sort_dir_2     sort_3     sort_dir_3 
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from(array('cr'=>"custom_reports"),
            array('id','rpt_name','companyid','table','sort_1','sort_dir_1','sort_2','sort_dir_2','sort_3','sort_dir_3','CreateBidReport','report_type'
                    ,'RRDEnable','RRDEmailTo'
                    ,'RRDEmailFromName','RRDEmailFrom'
                    ,'RRDEmailSubject','RRDEmailMessage'
                    ,'RRDEmailSchedule','RRDTimeOfDay'
                    ,'RRDTimeZone','RRDWeeklyDays'
                    ,'RRDMonthlyScheduleOptions','RRDMonthlyScheduleDayNum'
                    ,'RRDMonthlyScheduleDayOrder','RRDMonthlyScheduleDayOrderDay'
                    ,'RRDDailyOptions','RRDDailyEveryDaySpaceOutNum'
                    ,'RRDDailyEveryWeekday','RRDWeeklySpaceOutNum'
                    ,'RRDMonthlyDayNumSpaceOutNum','RRDMonthlyDayOrderSpaceOutNum'
                    )
        );
        $select->joinLeft(array('crtbl'=>'custom_reports_tables'),
            'crtbl.table_code=cr.table',
            array('table_title','special_where')
        );
        $select->where("id = '$repID'");
        $records = Core_Database::fetchAll($select); 
        if(empty($records) || count($records)==0){
            return null;  
        } else {
            return $records[0];   
        }
    }

    public function saveReport_RRD($reportID,$RRDParams)
    {
        $criteria = "id = '$reportID'";
        $fieldvals = array();
                         
        $RRDAvailableFields = array('RRDEnable','RRDEmailTo'
                    ,'RRDEmailFromName','RRDEmailFrom'
                    ,'RRDEmailSubject','RRDEmailMessage'
                    ,'RRDEmailSchedule','RRDTimeOfDay'
                    ,'RRDTimeZone','RRDWeeklyDays'
                    ,'RRDMonthlyScheduleOptions','RRDMonthlyScheduleDayNum'
                    ,'RRDMonthlyScheduleDayOrder','RRDMonthlyScheduleDayOrderDay'
                    ,'RRDDailyOptions','RRDDailyEveryDaySpaceOutNum'
                    ,'RRDDailyEveryWeekday','RRDWeeklySpaceOutNum'
                    ,'RRDMonthlyDayNumSpaceOutNum','RRDMonthlyDayOrderSpaceOutNum'
        );
        
        if(!empty($RRDParams) && count($RRDParams)>0){
            foreach($RRDParams as $key=>$val){
                if(in_array($key,$RRDAvailableFields)){
                    $fieldvals[$key] = $val;
                }
            }
        }
        
        if(count($fieldvals)>0){
            $result = Core_Database::update("custom_reports",$fieldvals,$criteria);
            
        }
        
        return $result;
    }
    
    public function saveReport2($reportID,$repName,$fieldArray,$filterArray=null,$createBidReport=null,$reportType=1)
    {
        //echo("<br/>filterArray: ");print_r($filterArray); die();       

        $repName = trim($repName);
        $report = $this->getReportInfo2($reportID);
        //rpt_name
        if($repName != $report['rpt_name']){
            if($this->ReportName_Exists($repName)) return -1;
        }

        $authUser = $_SESSION['Auth_User'];
        $loginName = $authUser['login'];
        $nameOfCurrentUser = $authUser['name'];
        
        //echo("<br/>nameOfCurrentUser: $nameOfCurrentUser");
        //echo("<br/>loginName: $loginName");
       // echo("<br/>repTable: $repTable");
        //--- update custom_reports
        $criteria = "id = '$reportID'";
        $fieldvals = array("rpt_name"=>$repName,
                        'lastupdated_date'=>date("Y-m-d H:i:s"),
                        "last_modified_by"=>$loginName,
                        "last_modified_by_name"=>$nameOfCurrentUser
                        );   
        if(isset($createBidReport)) {
            $fieldvals['CreateBidReport'] = $createBidReport;
        }
        if(!empty($reportType)){
            $fieldvals['report_type'] = $reportType;
        }
        $result = Core_Database::update("custom_reports",$fieldvals,$criteria);
        //echo("<br/>update->result: $result");
        //die();
        
        //---  update custom_reports_fields
        $oldReportFieldList = $this->getReportFieldList2($reportID);
        
        if(!empty($fieldArray) && count($fieldArray)>0){
            //insert new fields
            foreach($fieldArray as $field)
            { 
                if(!isset($oldReportFieldList[$field])){
                    $fieldvals = array("rep_id"=>$reportID,
                                "fieldname"=>$field,
                                "fieldcaption"=>$field
                    );
                    $result = Core_Database::insert("custom_reports_fields",$fieldvals);                    
                }
            }    
            //delete old fields
            $db = Core_Database::getInstance();
            foreach($oldReportFieldList as $key=>$reportField){
                $oldField = $reportField['fieldname'];
                if(!in_array($oldField,$fieldArray)){
                    $criteria = "rep_id='$reportID'  AND fieldname='$oldField'"; 
                    $result = $db->delete('custom_reports_fields',$criteria);
                }
            }
        }
        //---  update filters
        $oldFilters = $this->getFilterList2($reportID);
        
        //insert new filters/update existing filters
        if(!empty($filterArray) && count($filterArray)>0){
            foreach($filterArray as $filter){
                $fieldcode = $filter['fieldcode'];
                $filterID = $filter['id'];
                if(!isset($oldFilters[$fieldcode]) || empty($filterID)){
                    $this->addFilter2($reportID,$filter);                    
                } else {
                    $this->updateFilter2($reportID,$filterID,$filter);
                }
            }
        }
        //delete old filters
        if(!empty($oldFilters) && count($oldFilters)>0) {
            foreach($oldFilters as $key=>$oldFilter){
                $filterID = $oldFilter['id'];
                $delete = true;
                foreach($filterArray as $filter){
                    if($filter['id']==$filterID){
                        $delete = false;
                        break;
                    }
                }
                if($delete && $filterID>0){
                    $this->deleteReportFilter($reportID,$filterID);
                }
            }
        }
        //echo("<br/>filterArray: ");print_r($filterArray);        
        //echo("<br/>oldFilters: ");print_r($oldFilters);        
        //die();        
        
        //---
        return 0;
    }
    
    public function updateFilter2($reportID,$filterID,$key_value_array)
    {
        if(empty($reportID) || empty($filterID) || empty($key_value_array) || !is_array($key_value_array) || count($key_value_array)==0){
            return -1;
        }
        
        //--- update custom_reports_filters table 
        $avaiKeyArray = array('fieldcode','filter_at','datetime_relative_data_ref','datetime_range_from','datetime_range_to','string_matching','string_value','bool_value','numeric_base_value','numeric_range_from','numeric_range_to','datetime_range_int_from','datetime_range_int_to','time_tz','time_from','time_to');
        
        $updateFields = array();
        
        foreach($key_value_array as $field=>$value)
        {
            if(in_array($field,$avaiKeyArray)){
                if($field == 'datetime_range_from' ||  $field == 'datetime_range_to'){
                    if(!empty($value)){
                        $value = date("Y-m-d",strtotime($value));
                    } 
                }
                $updateFields[$field] = $value;                
            }            
        }
        
        //echo("updateFilter");
        if(count($updateFields) > 0){
            $criteria = "rep_id = '$reportID' AND id = '$filterID'";
            $result = Core_Database::update("custom_reports_filters",
                                $updateFields,$criteria
                            );
            //$errors = Core_Api_Error::getInstance();
            //echo("error: ");print_r($errors);
        }
        
        $db = Core_Database::getInstance();  
        
        //--- insert to  custom_reports_filters_selmulti
        if(isset($key_value_array['SelectMultiple'])){            
            //echo("SelectMultiple: ");print_r($key_value_array['SelectMultiple']);die();
            //delete old value
            $criteria = "filter_id='$filterID'";                 
            $result = $db->delete('custom_reports_filters_selmulti',$criteria);                    
            //insert new value
            $selectArray = $key_value_array['SelectMultiple'];
            
            if(!empty($selectArray) && is_array($selectArray) && count($selectArray)>0)
            {                
                foreach($selectArray as $selectValue){
                    try {                
                        $selectValue = trim($selectValue);
                        Core_Database::insert("custom_reports_filters_selmulti",
                                array('filter_id'=>$filterID,
                                    'filter_value'=>$selectValue)
                        );                        
                    } catch(Exception $ex) {
                                
                    }                                    
                }
            }
        } 
        
        return 0;
    }

    public function getFieldList_ByLocation($fieldLocation,$companyID,$isGPM)
    {
        if($fieldLocation==self::FieldFilter_Location_WO_Work_Order_Status){
            return $this->getWOStatusList();            
        }
            
        //if($fieldLocation==self::f)
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from("custom_reports_tablefields");        
        $select->where("active = 1");                
        $select->where("table_code='work_orders'");
        $select->where("Location = '$fieldLocation'");
        if($companyID != 'FLS' && !$isGPM){
            //FLSOnly
            $select->where("FLSOnly = 0");            
        }
        if(!$isGPM){
            //GPMOnly
            $select->where("GPMOnly = 0");
        }
        $select->order('field_title');
        $records = Core_Database::fetchAll($select); 
        
        if(!empty($records) && count($records)>0) {
            return $records;
        } else {
            return null;
        }

    }
    
    public function AvaiFieldListbyLocation($fieldLocation,$companyID,$isGPM)
    {
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from("custom_reports_tablefields",array('field_code'));        
        $select->where("active = 1");                
        $select->where("table_code='work_orders'");
        $select->where("Location = '$fieldLocation'");            
        if($companyID != 'FLS'){
            //FLSOnly
            $select->where("FLSOnly = 0");            
        }
        if(!$isGPM){
            //GPMOnly
            $select->where("GPMOnly = 0");
        }
        $select->order('field_code');
        $records = Core_Database::fetchAll($select); 
        
        $fieldList = array();
        if(!empty($records) && count($records)>0) {
            foreach($records as $rec)
            {
                $fieldList[] = $rec['field_code'];
            }
        }         
        return $fieldList;       
    }

    public function AvaiFilterListbyLocation($fieldLocation,$companyID,$isGPM)
    {
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from("custom_reports_tablefields",array('field_code'));        
        $select->where("active = 1");                
        $select->where("table_code='work_orders'");
        $select->where("Location = '$fieldLocation'");
        $select->where("FilterType IS NOT NULL AND FilterType <> ''");
        if($companyID != 'FLS'){
            //FLSOnly
            $select->where("FLSOnly = 0");            
        }
        if(!$isGPM){
            //GPMOnly
            $select->where("GPMOnly = 0");
        }
        $select->order('field_code');
        $records = Core_Database::fetchAll($select); 
        
        $fieldList = array();
        if(!empty($records) && count($records)>0) {
            foreach($records as $rec)
            {
                $fieldList[] = $rec['field_code'];
            }
        }         
        return $fieldList;       
    }

    
    public function getFilterList_ByLocation__($filterLocation,$companyID,$isGPM)
    {
        //echo("<br/>companyID: $companyID");
        if($filterLocation==self::FieldFilter_Location_WO_Select_Projects){
            return $this->getPrjectList_ByCompanyID($companyID);            
        } else if($filterLocation==self::FieldFilter_Location_WO_Work_Order_Status){
            return $this->getWOStatusList();            
        } else {
            
            $db = Core_Database::getInstance();  
            $select = $db->select();
            $select->from("custom_reports_tablefields",
                 array('field_code','field_title','FilterType','FilterLocation')
            );        
            $select->where("table_code='work_orders'");
            $select->where("FilterLocation = '$filterLocation'");
            if($companyID != 'FLS'){
                //FLSOnly
                $select->where("FLSOnly = 0");            
            }
            if(!$isGPM){
                //GPMOnly
                $select->where("GPMOnly = 0");
            }
            $select->order('field_title');
            //echo("select: ");print_r($select->__toString());
            $records = Core_Database::fetchAll($select); 
            $list = array();
            if(!empty($records) && count($records)>0) {
                foreach($records as $rec){
                    $fieldCode = $rec['field_code'];
                    $list[$fieldCode]=array('Field'=>$rec['field_code'],
                                    'Caption'=>$rec['field_title'],
                                    'ControlType'=>$rec['FilterType']);
                }
            }
            return $list;            
        }
        
    }
    
    public function runReport2($repID,$limit=0,$offset=0,$forCount=false,$sorts=null,$filters=null,$isGPM=1)
    {
        //echo("<pre>repID: ");print_r($repID);echo("<pre>");die();

        //--- last_run_date
        $fieldvals = array("last_run_date"=>date("Y-m-d H:i:s"));  
        $criteria = "id='$repID'";                 
        $result = Core_Database::update("custom_reports",$fieldvals,$criteria);

        //--- report info.
        $reportInfo = $this->getReportInfo2($repID);
        //echo("runReport2->reportInfo: <pre>");print_r($reportInfo);echo("</pre>");return;//test
        if(empty($reportInfo)){
            return null;
        }
        $tableCode = $reportInfo['table'];
        $companyID = $reportInfo['companyid'];
        $specialWhere = $reportInfo['special_where'];
        $reportType = $reportInfo['report_type'];
        
        $AvailableBasicWOFields = $this->AvaiFieldListbyLocation(self::FieldFilter_Location_WO_Basic_Information,$companyID,$isGPM);
        
        $AvailableOnSiteScheduleFields = $this->AvaiFieldListbyLocation(self::FieldFilter_Location_WO_Dates_Times,$companyID,$isGPM);

        $AvailableSiteInformationFields = $this->AvaiFieldListbyLocation(self::FieldFilter_Location_WO_Site_Information,$companyID,$isGPM);
        
        $AvailableBidFields = $this->AvaiFieldListbyLocation(self::FieldFilter_Location_WO_Bid_Information,$companyID,$isGPM);

        $AvailableTechnicianFields = $this->AvaiFieldListbyLocation(self::FieldFilter_Location_WO_Technicians,$companyID,$isGPM);

        $AvailableCommunicationFields = $this->AvaiFieldListbyLocation(self::FieldFilter_Location_WO_Communications,$companyID,$isGPM);

        $AvailableConfirmationFields = $this->AvaiFieldListbyLocation(self::FieldFilter_Location_WO_Confirmations_Close_Out,$companyID,$isGPM);

        $AvailablePartManagerFields = $this->AvaiFieldListbyLocation(self::FieldFilter_Location_WO_PartsManager,$companyID,$isGPM);

        $AvailableWinTagFields = $this->AvaiFieldListbyLocation(self::FieldFilter_Location_WO_WinTags,$companyID,$isGPM);

        $AvailablePricingFields = $this->AvaiFieldListbyLocation(self::FieldFilter_Location_WO_Pricing_Pay_Details,$companyID,$isGPM);
                
        $AvailableAdditionFields = $this->AvaiFieldListbyLocation(self::FieldFilter_Location_WO_Additional_Information,$companyID,$isGPM);
                        
        if($tableCode!='work_orders'){
            return null;
        }

        if(empty($companyID)){
            return null;
        }        

        //--- fields for sql                
        $ReportFieldList = $this->getReportFieldList2($repID);   
//echo("<pre>ReportFieldList: ");print_r($ReportFieldList);echo("</pre>");die();
        
        $allReportFields = array();        
        
        $woFields = array();
        $woTimeZoneFields = array();
        $woZipSiteTypesFields = array();
        $woTechFields = array();
        $woAssignedTechBidFields = array();
        $woProjectFields = array();
        $woEntryPartFields = array();
        $woNewPartFields = array();
        $woReturnPartFields = array();
        $woWintagItemValueFields = array();
        $woWintagItemFields = array();
        $woCategoryFields = array();
        
        $woAdditionalFields = array();
        $woAdditionalAMFields = array();
        $woAdditionalCSDFields = array();
        
        $clientExtFields = array();
        $clientExtAMFields = array();
        $clientExtCSDFields = array();
        $clientExtLEFields = array();
        $clientExtSTFields = array();
        
        $visitFields = array();
        
        $bidFields = array();
        $bidTechFields = array();
        
        $hasT2Filter = false;
        
        foreach($ReportFieldList as $field){
            $fieldname = $field['fieldname'];
            $filterType = $field['FilterType'];
            $allReportFields[]=$fieldname;  
            if($filterType=='T2') $hasT2Filter=true;
            
            $pos = strpos($fieldname, '_');    
            if($fieldname=='Qty_Applicants') 
            {
                $woFields[$fieldname]="(SELECT Count(*) FROM `work_orders__bids` wb INNER JOIN  TechBankInfo tb ON tb.TechID = wb.TechID WHERE (tb.Deactivated = 0 AND tb.HideBids != '1') AND (tb.AcceptTerms = 'Yes' OR tb.AcceptTerms = 'WM') AND (wb.WorkOrderID = w.WIN_NUM))";
                
            }  
            else if($fieldname=='Date_Assigned') 
            {
                $woFields[$fieldname]="(SELECT MAX(DateTime_Stamp) FROM  timestamps WHERE Description like 'Work Order Assigned%' AND WIN_NUM = w.WIN_NUM)";      
                          
            }
            else if($fieldname=='DateIncomplete') 
            {
                $woFields[$fieldname]="(SELECT MAX(DateTime_Stamp) FROM timestamps WHERE WIN_NUM=w.WIN_NUM AND Description = 'Work Order Marked Incomplete')";
            }
            else if($pos===false || $pos > 0)
            {
                $woFields[]=$fieldname;
                if(!empty($allowedToCount) && !empty($counts) && count($counts)>0 && in_array($fieldname,$counts)){
                    $woFields["Count$fieldname"]="Count($fieldname)";
                }
                if(!empty($allowedToSum) && !empty($sums) && count($sums)>0 && in_array($fieldname,$sums)){
                    $woFields["Sum$fieldname"]="SUM($fieldname)";
                }
                if(!empty($allowedToAverage) && !empty($avgs) && count($avgs)>0 && in_array($fieldname,$avgs)){
                    $woFields["Avg$fieldname"]="AVG($fieldname)";
                }
            } 
            else if($fieldname=='_PublishedDate')
            {
                $woFields[$fieldname] = "(CASE WHEN w.ShowTechs=1 THEN  w.DateEntered WHEN (w.ShowTechs=0 AND w.Status='assigned') THEN (SELECT MAX(DateTime_Stamp) FROM  timestamps WHERE Description like 'Work Order Assigned%' AND WIN_NUM = w.WIN_NUM) ELSE '' END)";
                
            } 
            else if(in_array($fieldname,$AvailableBasicWOFields))
            {
                //_AccountManager,_ClientServicesDirector,_LegalEntity,_ServiceType
                //_AccountManager, clients_ext                
                if($fieldname=='_AccountManager'){
                    $clientExtAMFields[$fieldname] = 'name';
                    $clientExtFields['FSAccountManagerId']='FSAccountManagerId';
                } else if($fieldname=='_ClientServicesDirector'){
                    $clientExtCSDFields[$fieldname] = 'name';
                    $clientExtFields['FSClientServiceDirectorId']='FSClientServiceDirectorId'; 
                } else if($fieldname=='_LegalEntity'){
                    $clientExtLEFields[$fieldname] = 'wo_tag_item_name';
                    $clientExtFields['EntityTypeId']='EntityTypeId'; 
                } else if($fieldname=='_ServiceType'){
                    $clientExtSTFields[$fieldname] = 'wo_tag_item_name';
                    $clientExtFields['ServiceTypeId']='ServiceTypeId'; 
                }
            } 
            else if(in_array($fieldname,$AvailableOnSiteScheduleFields)) 
            {
                if($fieldname=='_VisitStartTypeID'){
                    $visitFields[$fieldname] = "(CASE WHEN StartTypeID=1 THEN 'Firm' WHEN StartTypeID=2 THEN 'Window' ELSE '' END)";  
                } else if($fieldname=='_VisitStartDate'){
                    $visitFields[$fieldname] = 'StartDate';                      
                } else if($fieldname=='_VisitStartTime'){
                    $visitFields[$fieldname] = 'StartTime';                      
                } else if($fieldname=='_VisitEndDate'){
                    $visitFields[$fieldname] = 'EndDate';                      
                } else if($fieldname=='_VisitEndTime'){
                    $visitFields[$fieldname] = 'EndTime';                      
                } else if($fieldname=='_VisitSiteTimeZone'){
                    $woTimeZoneFields[$fieldname] = "(CASE WHEN z.StandardOffset =-10 THEN 'Hawaii' WHEN z.StandardOffset =-9 THEN 'Alaska' WHEN z.StandardOffset =-8 THEN 'Pacific' WHEN z.StandardOffset =-7 THEN 'Mountain' WHEN z.StandardOffset =-6 THEN 'Central' WHEN z.StandardOffset =-5 THEN 'Eastern' ELSE '' END)";   
                           
                } else if($fieldname=='_VisitDayPart'){//Night
                    $visitFields[$fieldname] = "(CASE WHEN (DAYOFWEEK(vis.StartDate)=7 or DAYOFWEEK(vis.StartDate)=0) THEN 'Weekend' WHEN HOUR(STR_TO_DATE(vis.StartTime,'%l:%i %p')) >=20 THEN 'Night' WHEN (HOUR(STR_TO_DATE(vis.StartTime,'%l:%i %p')) >=17 AND HOUR(STR_TO_DATE(vis.StartTime,'%l:%i %p')) < 20) THEN 'Evening' WHEN (HOUR(STR_TO_DATE(vis.StartTime,'%l:%i %p')) < 17) THEN 'Day' ELSE '' END)";
                    
                } else if($fieldname=='_VisitLeadTime'){
        $visitFields[$fieldname] = "(CASE WHEN (w.ShowTechs=0 AND w.Status!='assigned') THEN '' WHEN (DATE_FORMAT(vis.StartDate,'%m-%d-%Y') = DATE_FORMAT(w.DateEntered,'%m-%d-%Y')) THEN 'Same Business Day' WHEN (DATE_FORMAT(vis.StartDate,'%m-%d-%Y') =  DATE_FORMAT(DATE_ADD(w.DateEntered,INTERVAL 1 DAY),'%m-%d-%Y') ) THEN 'Next Business Day' ELSE 'Scheduled' END)";
                     
                } else if($fieldname=='_VisitEstimatedDuration'){
                    $visitFields[$fieldname] = "(TIMEDIFF(STR_TO_DATE(vis.EndTime,'%l:%i %p'),STR_TO_DATE(vis.StartTime,'%l:%i %p')))";
                } else if($fieldname=='_VisitActualCheckInDate'){
                    $visitFields[$fieldname] = "TechCheckInDate";
                } else if($fieldname=='_VisitActualCheckInTime'){
                    $visitFields[$fieldname] = "TechCheckInTime";
                } else if($fieldname=='_VisitActualCheckInResult'){
                    $visitFields[$fieldname] = "(CASE WHEN STR_TO_DATE(vis.TechCheckInTime,'%l:%i %p') < STR_TO_DATE(vis.StartTime,'%l:%i %p') THEN 'Early' WHEN  STR_TO_DATE(vis.TechCheckInTime,'%l:%i %p') = STR_TO_DATE(vis.StartTime,'%l:%i %p') THEN 'On Time' WHEN  STR_TO_DATE(vis.TechCheckInTime,'%l:%i %p') > STR_TO_DATE(vis.StartTime,'%l:%i %p') THEN 'Late' ELSE '' END )";
                } else if($fieldname=='_VisitActualCheckOutDate'){
                    $visitFields[$fieldname] = "TechCheckOutDate";                    
                } else if($fieldname=='_VisitActualCheckOutTime'){
                    $visitFields[$fieldname] = "TechCheckOutTime";                    
                } else if($fieldname=='_VisitActualCheckOutResult'){
                    $visitFields[$fieldname] = "(CASE WHEN STR_TO_DATE(vis.TechCheckOutTime,'%l:%i %p') < STR_TO_DATE(vis.EndTime,'%l:%i %p') THEN 'Early' WHEN  STR_TO_DATE(vis.TechCheckOutTime,'%l:%i %p') = STR_TO_DATE(vis.EndTime,'%l:%i %p') THEN 'On Time' WHEN  STR_TO_DATE(vis.TechCheckOutTime,'%l:%i %p') > STR_TO_DATE(vis.EndTime,'%l:%i %p') THEN 'Late' ELSE '' END )";
                } else if($fieldname=='_VisitActualCalculatedDuration'){
                    $visitFields[$fieldname] = "(TIMEDIFF(STR_TO_DATE(vis.TechCheckOutTime,'%l:%i %p'),STR_TO_DATE(vis.TechCheckInTime,'%l:%i %p')))";                    
                } else if($fieldname=='_VisitActualDurationResult'){
                    $visitFields[$fieldname] = "(CASE WHEN (TIMEDIFF(STR_TO_DATE(vis.TechCheckOutTime,'%l:%i %p'),STR_TO_DATE(vis.TechCheckInTime,'%l:%i %p'))) < (TIMEDIFF(STR_TO_DATE(vis.EndTime,'%l:%i %p'),STR_TO_DATE(vis.StartTime,'%l:%i %p'))) THEN 'Under Duration' WHEN (TIMEDIFF(STR_TO_DATE(vis.TechCheckOutTime,'%l:%i %p'),STR_TO_DATE(vis.TechCheckInTime,'%l:%i %p'))) > (TIMEDIFF(STR_TO_DATE(vis.EndTime,'%l:%i %p'),STR_TO_DATE(vis.StartTime,'%l:%i %p'))) THEN 'Over Duration' WHEN (TIMEDIFF(STR_TO_DATE(vis.TechCheckOutTime,'%l:%i %p'),STR_TO_DATE(vis.TechCheckInTime,'%l:%i %p'))) = (TIMEDIFF(STR_TO_DATE(vis.EndTime,'%l:%i %p'),STR_TO_DATE(vis.StartTime,'%l:%i %p'))) THEN 'Correct Duration' ELSE '' END )";
                }
                // _VisitActualCalculatedDuration
            } 
            else if(in_array($fieldname,$AvailableSiteInformationFields))
            {
                if($fieldname=='_Geography') {
                    $woZipSiteTypesFields[$fieldname] = "SiteType";
                }
            } 
            else if(in_array($fieldname,$AvailableBidFields))
            {                
                if($fieldname=='_BidPayMax'){
                    $woFields[$fieldname] ="PayMax";
                } else if($fieldname=='_BidAmount_Per'){
                    $woFields[$fieldname] ="Amount_Per";
                }
                //'_BidPayMax, work_orders , '_BidAmount_Per'
                if($reportType==self::Report_Type_Bids){
                    if($fieldname=='_BiddingTechEmail'){
                        $bidTechFields[$fieldname] ="PrimaryEmail";
                        if(!empty($allowedToCount) && !empty($counts) && count($counts)>0 && in_array($fieldname,$counts)){
                            $bidTechFields["Count$fieldname"]="Count(PrimaryEmail)";
                        }                        
                    } else if($fieldname=='_BiddingTechFLSID'){
                        $bidTechFields[$fieldname] ="FLSID"; 
                        if(!empty($allowedToCount) && !empty($counts) && count($counts)>0 && in_array($fieldname,$counts)){
                            $bidTechFields["Count$fieldname"]="Count(FLSID)";
                        }                                                
                    } else if($fieldname=='_BiddingTechPhone'){
                        $bidTechFields[$fieldname] ="PrimaryPhone";   
                        if(!empty($allowedToCount) && !empty($counts) && count($counts)>0 && in_array($fieldname,$counts)){
                            $bidTechFields["Count$fieldname"]="Count(PrimaryPhone)";
                        }                                                                            
                    } else if($fieldname=='_BiddingTechPreferred'){
                        $bidFields[$fieldname] = "(CASE WHEN EXISTS(SELECT Tech_ID FROM `client_preferred_techs` where `Tech_ID` = wbid.TechID and `CompanyID` = '$companyID') THEN 1 ELSE 0 END)";
                        
                    } else if($fieldname=='_BidAmount'){
                        $bidFields[$fieldname] ="BidAmount";       
                        if(!empty($allowedToCount) && !empty($counts) && count($counts)>0 && in_array($fieldname,$counts)){
                            $bidFields["Count$fieldname"]="Count(BidAmount)";
                        }                                           
                        if(!empty($sums) && count($sums)>0 && in_array($fieldname,$sums)){
                            $bidFields["Sum_BidAmount"]="SUM(BidAmount)";
                        }                                           
                    } else if($fieldname=='_Comments'){
                        $bidFields[$fieldname] ="Comments";   
                        if(!empty($allowedToCount) && !empty($counts) && count($counts)>0 && in_array($fieldname,$counts)){
                            $bidFields["Count$fieldname"]="Count(Comments)";
                        }                                                                       
                    } else if($fieldname=='_Bid_Date'){
                        $bidFields[$fieldname] ="Bid_Date";     
                        if(!empty($allowedToCount) && !empty($counts) && count($counts)>0 && in_array($fieldname,$counts)){
                            $bidFields["Count$fieldname"]="Count(Bid_Date)";
                        }                                           
                    } else if($fieldname=='_Hide'){
                        $bidFields[$fieldname] ="Hide";       
                        if(!empty($allowedToCount) && !empty($counts) && count($counts)>0 && in_array($fieldname,$counts)){
                            $bidFields["Count$fieldname"]="Count(Hide)";
                        }                                                                   
                    } else if($fieldname=='_Tech_Fname'){
                        $bidFields[$fieldname] ="Tech_FName";     
                        if(!empty($allowedToCount) && !empty($counts) && count($counts)>0 && in_array($fieldname,$counts)){
                            $bidFields["Count$fieldname"]="Count(Tech_FName)";
                        }                                  
                    } else if($fieldname=='_TechID'){
                        $bidFields[$fieldname] ="TechID";       
                        if(!empty($allowedToCount) && !empty($counts) && count($counts)>0 && in_array($fieldname,$counts)){
                            $bidFields["Count$fieldname"]="Count(TechID)";
                        }                                                          
                    } else if($fieldname=='_Tech_Lname'){
                        $bidFields[$fieldname] ="Tech_LName";       
                        if(!empty($allowedToCount) && !empty($counts) && count($counts)>0 && in_array($fieldname,$counts)){
                            $bidFields["Count$fieldname"]="Count(Tech_LName)";
                        }                         
                    }
                }
            } 
            else if(in_array($fieldname,$AvailableTechnicianFields))
            {
                if($fieldname=='_PrimaryEmail'){
                    $woTechFields[$fieldname] = 'PrimaryEmail';
                } else if($fieldname=='_PrimaryEmail'){
                    $woTechFields[$fieldname] = 'SecondaryEmail';                    
                } else if($fieldname=='_PrimaryPhone'){
                    $woTechFields[$fieldname] = 'PrimaryPhone';                    
                } else if($fieldname=='_SecondaryPhone'){
                    $woTechFields[$fieldname] = 'SecondaryPhone';                    
                } else if($fieldname=='_AssignedTechDenied'){
                    $woFields[$fieldname] = "(CASE WHEN (Tech_ID > 0 AND EXISTS(SELECT TechID FROM `client_denied_techs` where `TechID` = w.Tech_ID and `Company_ID` = '$companyID') ) THEN 1 ELSE 0 END)";
                }  else if($fieldname=='_AssignedTechPreferred') {
                    $woFields[$fieldname] = "(CASE WHEN (Tech_ID > 0 AND EXISTS(SELECT Tech_ID FROM `client_preferred_techs` where `Tech_ID` = w.Tech_ID and `CompanyID` = '$companyID') ) THEN 1 ELSE 0 END)";
                }  else if($fieldname=='_TechBackedOut') {
                    $woFields[$fieldname] = "(CASE WHEN (BackOut_Tech > 0)  THEN 1 ELSE 0 END)";
                }  else if($fieldname=='_BackOutFLSTechID') {
                    $woFields[$fieldname] = "(CASE WHEN (BackOut_Tech > 0) AND EXISTS(SELECT FLSID FROM `TechBankInfo` where `TechID` = w.BackOut_Tech)) THEN (SELECT FLSID FROM `TechBankInfo` where `TechID` = w.BackOut_Tech) ELSE '' END)";                        
                }  else if($fieldname=='_TechNoShowed') {
                    $woFields[$fieldname] = "(CASE WHEN (NoShow_Tech > 0)  THEN 1 ELSE 0 END)";
                }  else if($fieldname=='_NoShowFLSTechID') {
                    $woFields[$fieldname] = "(CASE WHEN (NoShow_Tech > 0) AND EXISTS(SELECT FLSID FROM `TechBankInfo` where `TechID` = w.NoShow_Tech)) THEN (SELECT FLSID FROM `TechBankInfo` where `TechID` = w.NoShow_Tech) ELSE '' END)";
                } else if($fieldname=='_TechAmount_Per'){
                    $woFields[$fieldname] = "Amount_Per";
                } else if($fieldname=='_TechBidAmount'){
                    $woAssignedTechBidFields[$fieldname] = "BidAmount";
                } else if($fieldname=='_TechBid_Date'){
                    $woAssignedTechBidFields[$fieldname] = "Bid_Date";                    
                }
            } 
            else if(in_array($fieldname,$AvailableCommunicationFields))
            {
                if($fieldname=='_ACSNotifiPOC_Email') {
                    $woAdditionalFields[$fieldname] = "ACSNotifiPOC_Email";
                }  else if($fieldname=='_ACSNotifiPOC') {
                    $woAdditionalFields[$fieldname] = "ACSNotifiPOC";
                }  else if($fieldname=='_ACSNotifiPOC_Phone') {
                    $woAdditionalFields[$fieldname] = "ACSNotifiPOC_Phone";
                }  else if($fieldname=='_EmailBlastRadius') {
                    $woProjectFields[$fieldname]='EmailBlastRadius';
                }
            } 
            else if(in_array($fieldname,$AvailablePartManagerFields))
            {
                if($fieldname=='_NewPartCarrier') {
                    $woNewPartFields[$fieldname]='Carrier';
                }  else if($fieldname=='_NewPartEstArrival') {
                    $woNewPartFields[$fieldname]='ETA';
                }  else if($fieldname=='_NewPartInfo') {
                    $woNewPartFields[$fieldname]='Description';
                }  else if($fieldname=='_NewPartisConsumable') {
                    $woEntryPartFields[$fieldname]='Consumable';//entry 
                }  else if($fieldname=='_NewPartModelNum') {
                    $woNewPartFields[$fieldname]='ModelNum';
                }  else if($fieldname=='_NewPartPartNum') {
                    $woNewPartFields[$fieldname]='PartNum';
                }  else if($fieldname=='_NewPartSerialNum') {
                    $woNewPartFields[$fieldname]='SerialNum';
                }  else if($fieldname=='_NewPartShippingNotes') {
                    $woNewPartFields[$fieldname]='Instructions';
                }  else if($fieldname=='_NewPartShipsTo') {
                    $woNewPartFields[$fieldname]='ShipTo';
                }  else if($fieldname=='_NewPartTrackingNum') {
                    $woNewPartFields[$fieldname]='TrackingNum';
                }  else if($fieldname=='_NumofNewParts') {
                    $woFields[$fieldname]="(SELECT Count(*) FROM part_entries paen INNER JOIN parts pa ON pa.PartEntryID = paen.id WHERE pa.IsNew=1 AND paen.WinNum=w.WIN_NUM)";
                }  else if($fieldname=='_NumofReturnParts') {
                    $woFields[$fieldname]="(SELECT Count(*) FROM part_entries paen INNER JOIN parts pa ON pa.PartEntryID = paen.id  WHERE pa.IsNew=0 AND paen.WinNum=w.WIN_NUM)";                    
                }  else if($fieldname=='_PartManagerUpdatedByTech') {
                    $woFields[$fieldname]="(CASE WHEN EXISTS(SELECT TechIdLastUpd FROM part_entries WHERE TechIdLastUpd > 0 AND WinNum=w.WIN_NUM) THEN 1 ELSE 0 END)";
                    //SELECT TechIdLastUpd FROM part_entries WHERE TechIdLastUpd > 0 AND paen.WinNum='561072'

                }  else if($fieldname=='_ReturnPartCarrier') {
                    $woReturnPartFields[$fieldname]='Carrier';
                }  else if($fieldname=='_ReturnPartEstArrial') {
                    $woReturnPartFields[$fieldname]='ETA';
                }  else if($fieldname=='_ReturnPartInfo') {
                    $woReturnPartFields[$fieldname]='Description';
                }  else if($fieldname=='_ReturnPartInstructions') {
                    $woReturnPartFields[$fieldname]='Instructions';
                }  else if($fieldname=='_ReturnPartModelNum') {
                    $woReturnPartFields[$fieldname]='ModelNum';
                }  else if($fieldname=='_ReturnPartPartNum') {
                    $woReturnPartFields[$fieldname]='PartNum';
                }  else if($fieldname=='_ReturnPartRMA') {
                    $woReturnPartFields[$fieldname]='RMA';
                }  else if($fieldname=='_ReturnPartSerialNum') {
                    $woReturnPartFields[$fieldname]='SerialNum';
                }  else if($fieldname=='_ReturnPartTrackingNum') {
                    $woReturnPartFields[$fieldname]='TrackingNum';
                }
            }
            else if(in_array($fieldname,$AvailableWinTagFields))
            {
                 if($fieldname=='_WINTagAdded') {
                    $woFields[$fieldname] = "(CASE WHEN EXISTS(SELECT wtiv.WIN_NUM, wtiv.wo_tag_item_id, wti.wo_tag_code FROM work_order_tagitem_values wtiv INNER JOIN  wo_tag_items wti ON  wti.id = wtiv.wo_tag_item_id WHERE wti.wo_tag_code ='CompletionStatus' AND wtiv.WIN_NUM = w.WIN_NUM) THEN 'Yes' ELSE 'No' END)";

                 } else if($fieldname=='_TotalNumOfWINTags'){
                    $woFields[$fieldname] = "(SELECT Count(*) FROM work_order_tagitem_values wtiv INNER JOIN  wo_tag_items wti ON  wti.id = wtiv.wo_tag_item_id WHERE wti.wo_tag_code ='CompletionStatus' AND wtiv.WIN_NUM = w.WIN_NUM)";
                 } else if($fieldname=='_WINTagAddedDate'){
                    $woWintagItemValueFields[$fieldname] = 'DateAdded';
                    
                 } else if($fieldname=='_WINTagComplete'){
                    $woFields[$fieldname] = "(CASE WHEN EXISTS(SELECT wtiv.WIN_NUM, wtiv.wo_tag_item_id, wti.wo_tag_code FROM work_order_tagitem_values wtiv INNER JOIN  wo_tag_items wti ON  wti.id = wtiv.wo_tag_item_id WHERE wti.wo_tag_code ='CompletionStatus' AND wti.item_sortdesc = 'Complete' AND wtiv.WIN_NUM = w.WIN_NUM) THEN 'Yes' ELSE 'No' END)";
                     
                 } else if($fieldname=='_WINTagIncomplete'){
                    $woFields[$fieldname] = "(CASE WHEN EXISTS(SELECT wtiv.WIN_NUM, wtiv.wo_tag_item_id, wti.wo_tag_code FROM work_order_tagitem_values wtiv INNER JOIN  wo_tag_items wti ON  wti.id = wtiv.wo_tag_item_id WHERE wti.wo_tag_code ='CompletionStatus' AND wti.item_sortdesc = 'Incomplete' AND wtiv.WIN_NUM = w.WIN_NUM) THEN 'Yes' ELSE 'No' END)";
                                          
                 } else if($fieldname=='_WINTagRescheduled'){
                    $woFields[$fieldname] = "(CASE WHEN EXISTS(SELECT wtiv.WIN_NUM, wtiv.wo_tag_item_id, wti.wo_tag_code FROM work_order_tagitem_values wtiv INNER JOIN  wo_tag_items wti ON  wti.id = wtiv.wo_tag_item_id WHERE wti.wo_tag_code ='CompletionStatus' AND wti.item_sortdesc = 'Rescheduled' AND wtiv.WIN_NUM = w.WIN_NUM) THEN 'Yes' ELSE 'No' END)";
                                          
                 } else if($fieldname=='_WINTagSiteCancelled'){
                    $woFields[$fieldname] = "(CASE WHEN EXISTS(SELECT wtiv.WIN_NUM, wtiv.wo_tag_item_id, wti.wo_tag_code FROM work_order_tagitem_values wtiv INNER JOIN  wo_tag_items wti ON  wti.id = wtiv.wo_tag_item_id WHERE wti.wo_tag_code ='CompletionStatus' AND wti.item_sortdesc = 'Cancelled' AND wtiv.WIN_NUM = w.WIN_NUM) THEN 'Yes' ELSE 'No' END)";
                                          
                 } else if($fieldname=='_WINTag'){
                     $woWintagItemFields[$fieldname] = "(CASE WHEN item_sortdesc='Scheduled' THEN '' ELSE item_sortdesc END)";
                     
                 } else if($fieldname=='_WINTagOwner'){
                     $woWintagItemFields[$fieldname] = "(CASE WHEN item_sortdesc='Scheduled' THEN '' ELSE item_owner END )";
                     
                 } else if($fieldname=='_WINTagDetails'){
                     $woWintagItemFields[$fieldname] = 'item_details';

                 } else if($fieldname=='_WINTagNotes'){
                     $woWintagItemValueFields[$fieldname] = 'notes';    
                     
                 } else if($fieldname=='_WINTagBillable'){
                     $woWintagItemValueFields[$fieldname] = "(CASE WHEN wtiv.value_id=2 THEN 'Yes' WHEN wtiv.value_id=1 THEN 'No' ELSE '' END)";    
                     
                 } else if($fieldname=='_WINTagCheckInResult'){
                    $visitFields[$fieldname] = "(CASE WHEN STR_TO_DATE(vis.TechCheckInTime,'%l:%i %p') < STR_TO_DATE(vis.StartTime,'%l:%i %p') THEN 'Early' WHEN  STR_TO_DATE(vis.TechCheckInTime,'%l:%i %p') = STR_TO_DATE(vis.StartTime,'%l:%i %p') THEN 'On Time' WHEN  STR_TO_DATE(vis.TechCheckInTime,'%l:%i %p') > STR_TO_DATE(vis.StartTime,'%l:%i %p') THEN 'Late' ELSE '' END )";
                    
                 } else if($fieldname=='_WINTagCheckOutResult'){
                    $visitFields[$fieldname] = "(CASE WHEN STR_TO_DATE(vis.TechCheckOutTime,'%l:%i %p') < STR_TO_DATE(vis.EndTime,'%l:%i %p') THEN 'Early' WHEN  STR_TO_DATE(vis.TechCheckOutTime,'%l:%i %p') = STR_TO_DATE(vis.EndTime,'%l:%i %p') THEN 'On Time' WHEN  STR_TO_DATE(vis.TechCheckOutTime,'%l:%i %p') > STR_TO_DATE(vis.EndTime,'%l:%i %p') THEN 'Late' ELSE '' END )";
                     
                     
                 } else if($fieldname=='_WINTagDurationResult'){
                    $visitFields[$fieldname] = "(CASE WHEN (TIMEDIFF(STR_TO_DATE(vis.TechCheckOutTime,'%l:%i %p'),STR_TO_DATE(vis.TechCheckInTime,'%l:%i %p'))) < (TIMEDIFF(STR_TO_DATE(vis.EndTime,'%l:%i %p'),STR_TO_DATE(vis.StartTime,'%l:%i %p'))) THEN 'Under Duration' WHEN (TIMEDIFF(STR_TO_DATE(vis.TechCheckOutTime,'%l:%i %p'),STR_TO_DATE(vis.TechCheckInTime,'%l:%i %p'))) > (TIMEDIFF(STR_TO_DATE(vis.EndTime,'%l:%i %p'),STR_TO_DATE(vis.StartTime,'%l:%i %p'))) THEN 'Over Duration' WHEN (TIMEDIFF(STR_TO_DATE(vis.TechCheckOutTime,'%l:%i %p'),STR_TO_DATE(vis.TechCheckInTime,'%l:%i %p'))) = (TIMEDIFF(STR_TO_DATE(vis.EndTime,'%l:%i %p'),STR_TO_DATE(vis.StartTime,'%l:%i %p'))) THEN 'Correct Duration' ELSE '' END )";                     
                     
                 } else if($fieldname=='_WINTagTechPerformanceRating'){
                     $woFields[$fieldname] = 'SATPerformance';
                     
                 } else if($fieldname=='_WINTagTechPreferenceRating'){
                     $woFields[$fieldname] = 'SATRecommended';
                                          
                 }
                 //_WINTagAddedBy  DateAdded
            }
            else if(in_array($fieldname,$AvailableAdditionFields))
            {
                if($fieldname=='_approvedBy') {
                    $woFields[$fieldname]='approvedBy';
                    
                }  else if($fieldname=='_AssignedBy') {
                    $woFields[$fieldname] = "(SELECT Username FROM timestamps WHERE WIN_NUM=w.WIN_NUM AND (Description LIKE 'Work Order Assigned') LIMIT 1)";
                    
                }  else if($fieldname=='_MarkedIncompleteBy') {
                    $woFields[$fieldname] = "(SELECT Username FROM timestamps WHERE WIN_NUM=w.WIN_NUM AND (Description = 'Work Order Marked Incomplete') LIMIT 1)";
                    
                }  else if($fieldname=='_PublishedBy') {
                    $woFields[$fieldname]="(SELECT Username FROM timestamps WHERE WIN_NUM=w.WIN_NUM AND (Description LIKE '%Published%' OR Description LIKE '%Work Order Assigned%') LIMIT 1)";
                }
            }
        }
            
//echo("<pre>woFields: ");print_r($woFields);echo("</pre>");die();
        
        //--- filters for sql
        if(empty($filters) || count($filters)==0){
            $filters = $this->getFilterList2($repID);            
        }
//echo("<pre>getFilterList2: ");print_r($filters);echo("</pre>");die();
        $criteriaArray=array(); 
        $hasBidFilter = false;
        
        $AvailableBasicInfoFilters = $this->AvaiFilterListbyLocation(self::FieldFilter_Location_WO_Basic_Information,$companyID,$isGPM);

        $AvailableOnsiteScheduleFilters = $this->AvaiFilterListbyLocation(self::FieldFilter_Location_WO_Dates_Times,$companyID,$isGPM);
        
        $AvailableSiteInfoFilters = $this->AvaiFilterListbyLocation(self::FieldFilter_Location_WO_Site_Information,$companyID,$isGPM);

        $AvailableBidFilters = $this->AvaiFilterListbyLocation(self::FieldFilter_Location_WO_Bid_Information,$companyID,$isGPM);

        $AvailableTechFilters = $this->AvaiFilterListbyLocation(self::FieldFilter_Location_WO_Technicians,$companyID,$isGPM);

        $AvailableCommuniFilters = $this->AvaiFilterListbyLocation(self::FieldFilter_Location_WO_Communications,$companyID,$isGPM);
        
        $AvailableConfirmFilters = $this->AvaiFilterListbyLocation(self::FieldFilter_Location_WO_Confirmations_Close_Out,$companyID,$isGPM);

        $AvailablePartManaFilters = $this->AvaiFilterListbyLocation(self::FieldFilter_Location_WO_PartsManager,$companyID,$isGPM);

        $AvailableWinTagFilters = $this->AvaiFilterListbyLocation(self::FieldFilter_Location_WO_WinTags,$companyID,$isGPM);
        
        $AvailablePricingFilters = $this->AvaiFilterListbyLocation(self::FieldFilter_Location_WO_Pricing_Pay_Details,$companyID,$isGPM);
        
        $AvailableAdditionFilters = $this->AvaiFilterListbyLocation(self::FieldFilter_Location_WO_Additional_Information,$companyID,$isGPM);
        
        
        $db = Core_Database::getInstance();

        if(!empty($filters) && count($filters) > 0){
            foreach($filters as $filter){                
                $fieldcode = $filter['fieldcode'];
                $pos = strpos($fieldcode, '_');
                if($fieldcode=='Tech_ID')
                {
                    $cri = $this->getCriteria($filter,'w.Tech_ID',self::Criteria_Type_ExactMatch);
                    if(!empty($cri))  $criteriaArray[] = $cri;
                }
                else if($fieldcode=='Qty_Applicants') 
                {
                    $from = $filter['numeric_range_from'];
                    $to = $filter['numeric_range_to'];   
                    $having = "";
                    if(isset($from) && is_numeric($from) ){
                        
                        $having = "Count(*) >= '$from'";    
                    }

                    if(isset($to) && is_numeric($to)){
                        $having .= empty($having) ? "" : " AND ";
                        $having .= "Count(*) <='$to'";    
                    }
                    if(!empty($having)){
                        $criteriaArray[] = "(EXISTS(SELECT Count(*) FROM `work_orders__bids` wb INNER JOIN  TechBankInfo tb ON tb.TechID = wb.TechID 
    WHERE (tb.Deactivated = 0 AND tb.HideBids != '1') AND (tb.AcceptTerms = 'Yes' OR tb.AcceptTerms = 'WM') AND (wb.WorkOrderID = w.WIN_NUM) HAVING $having))";
                    }
                }
                else if($fieldcode=='Date_Assigned')
                {
                    $from = $filter['datetime_range_from'];
                    $to = $filter['datetime_range_to'];            
                    $having = "";                    
                    if(!empty($from)){
                        $from = date('Y-m-d',strtotime($from));                        
                        $having = "MAX(DateTime_Stamp) >='$from'";    
                    }
                    if(!empty($to)){
                        $to = date('Y-m-d',strtotime($to));
                        $having .= empty($having) ? "" : " AND ";
                        $having .= "MAX(DateTime_Stamp) <='$to'";    
                    }
                    if(!empty($having)){
                        $criteriaArray[] = "(EXISTS(SELECT MAX(DateTime_Stamp) FROM  timestamps WHERE Description like 'Work Order Assigned%' AND WIN_NUM = w.WIN_NUM HAVING $having))";         
                    }
                }
                else if($fieldcode=='DateIncomplete')
                {
                    $from = $filter['datetime_range_from'];
                    $to = $filter['datetime_range_to'];            
                    $having = "";                    
                    if(!empty($from)){
                        $from = date('Y-m-d',strtotime($from));                        
                        $having = "MAX(DateTime_Stamp) >='$from'";    
                    }
                    if(!empty($to)){
                        $to = date('Y-m-d',strtotime($to));
                        $having .= empty($having) ? "" : " AND ";
                        $having .= "MAX(DateTime_Stamp) <='$to'";    
                    }
                    if(!empty($having)){
                        $criteriaArray[] = "(EXISTS(SELECT MAX(DateTime_Stamp) FROM  timestamps WHERE  Description = 'Work Order Marked Incomplete' AND WIN_NUM = w.WIN_NUM HAVING $having))";         
                        
                    }                    
                }
                else if($fieldcode=='Status')
                {
                    $cri = $this->getCriteria($filter,'w.Status',self::Criteria_Type_SelectMulti);
                    if(!empty($cri))  $criteriaArray[] = $cri;
                }
                else if($pos===false || $pos > 0)
                {
                    $filterDefaultValue = $this->getFilterDefaultValue2($filter);
                    $DBDefaultValue = $filterDefaultValue['DBDefaultValue'];
                    if(!empty($DBDefaultValue)){
                        $criteriaArray[] = $DBDefaultValue;
                    }
                } 
                else if(in_array($fieldcode,$AvailableBasicInfoFilters)) 
                {
                    if($fieldcode=='_AccountManager'){
                        $value = $filter['string_value'];
                        if(!empty($value)){
                            $criteriaArray[] = "cextAM.name = '$value'";
                        }
                    } else if($fieldcode=='_ClientServicesDirector'){
                        $value = $filter['string_value'];
                        if(!empty($value)){
                            $criteriaArray[] = "cextCSD.name = '$value'";
                        }                        
                    } else if($fieldcode=='_LegalEntity'){
                        $value = $filter['string_value'];
                        if(!empty($value)){
                            $criteriaArray[] = "cextLE.wo_tag_item_name = '$value'";
                        }                                                
                    } else if($fieldcode=='_ServiceType'){
                        $value = $filter['string_value'];
                        if(!empty($value)){
                            $criteriaArray[] = "cextST.wo_tag_item_name = '$value'";
                        }                        
                    }
                } 
                else if(in_array($fieldcode,$AvailableOnsiteScheduleFilters)) 
                {                                        
                    if($fieldcode=='_VisitStartTypeID')
                    {
                        $SelectMultiple = $filter['SelectMultiple'];
                        if(!empty($SelectMultiple) && count($SelectMultiple)>0){
                            $filterValues = implode(',',$SelectMultiple);
                            $criteriaArray[] = "vis.StartTypeID IN ($filterValues)";
                        }                        
                    } 
                    else if($fieldcode=='_VisitStartDate')
                    {
                        $crit = $this->getCriteria($filter,'vis.StartDate',self::Criteria_Type_FromDate_ToDate);
                        if(!empty($crit)) $criteriaArray[] = $crit;
                        
                    } 
                    else if($fieldcode=='_VisitStartTime')
                    {
                         $cri = $this->getCriteria($filter,'vis.StartTime',self::Criteria_Type_FromTime_ToTime);
                         if(!empty($cri)) $criteriaArray[] = $cri;                        
                    } 
                    else if($fieldcode=='_VisitEndDate')
                    {
                        $crit = $this->getCriteria($filter,'vis.EndDate',self::Criteria_Type_FromDate_ToDate);
                        if(!empty($crit)) $criteriaArray[] = $crit;
                        
                    } 
                    else if($fieldcode=='_VisitEndTime')
                    {
                        $crit = $this->getCriteria($filter,'vis.EndTime',self::Criteria_Type_FromTime_ToTime);
                        if(!empty($crit)) $criteriaArray[] = $crit;

                    } 
                    else if($fieldcode=='_VisitDayPart')
                    {
                        $SelectMultiple = $filter['SelectMultiple'];
                        if(!empty($SelectMultiple) && count($SelectMultiple)>0){
                            $cri = "";
                            foreach($SelectMultiple as $k=>$v){
                                if($v=='Night'){
                                    $cri .= empty($cri) ? "" : " OR ";
                                    $cri .= "(DAYOFWEEK(vis.StartDate)=7 or DAYOFWEEK(vis.StartDate)=0)";
                                } else if($v=='Evening'){
                                    $cri .= empty($cri) ? "" : " OR ";
                                    $cri .= "(DAYOFWEEK(vis.StartDate) <> 7 AND DAYOFWEEK(vis.StartDate) <> 0 AND HOUR(STR_TO_DATE(vis.StartTime,'%l:%i %p')) >=20)";
                                } else if($v=='Evening'){
                                    $cri .= empty($cri) ? "" : " OR ";
                                    $cri .= "(DAYOFWEEK(vis.StartDate) <> 7 AND DAYOFWEEK(vis.StartDate) <> 0 AND HOUR(STR_TO_DATE(vis.StartTime,'%l:%i %p')) >=17 AND HOUR(STR_TO_DATE(vis.StartTime,'%l:%i %p')) < 20)";                                    } else if($v=='Day'){
                                    $cri .= empty($cri) ? "" : " OR ";
                                    $cri .= "( (vis.StartDate IS NULL OR (DAYOFWEEK(vis.StartDate) <> 7 AND DAYOFWEEK(vis.StartDate) <> 0)) AND HOUR(STR_TO_DATE(vis.StartTime,'%l:%i %p')) < 17)";                                            
                                }
                            }
                            if(!empty($cri)) $criteriaArray[] = $cri;
                        }
                        
                    } 
                    else if($fieldcode=='_VisitLeadTime')
                    {
                        $SelectMultiple = $filter['SelectMultiple'];
                        if(!empty($SelectMultiple) && count($SelectMultiple)>0){
                            $cri = "";
                            foreach($SelectMultiple as $v){
                                if($v=='Scheduled'){
                                    $cri .= empty($cri) ? "" : " OR ";
                                    $cri .= "( w.ShowTechs=1 AND DATE_FORMAT(vis.StartDate,'%m-%d-%Y') != DATE_FORMAT(w.DateEntered,'%m-%d-%Y') AND (DATE_FORMAT(vis.StartDate,'%m-%d-%Y') !=  DATE_FORMAT(DATE_ADD(w.DateEntered,INTERVAL 1 DAY),'%m-%d-%Y')) )";
                                } else if($v=='Same Business Day'){
                                    $cri .= empty($cri) ? "" : " OR ";
                                    $cri .= "(w.ShowTechs=1 AND DATE_FORMAT(vis.StartDate,'%m-%d-%Y') = DATE_FORMAT(w.DateEntered,'%m-%d-%Y'))";                                    
                                } else if($v=='Next Business Day'){
                                    $cri .= empty($cri) ? "" : " OR ";
                                    $cri .= "(w.ShowTechs=1 AND DATE_FORMAT(vis.StartDate,'%m-%d-%Y') =  DATE_FORMAT(DATE_ADD(w.DateEntered,INTERVAL 1 DAY),'%m-%d-%Y'))";                                                                     }
                            }
                            
                            if(!empty($cri)) $criteriaArray[] = $cri;
                        }                        
                        
                    } 
                    else if($fieldcode=='_VisitEstimatedDuration')
                    {
                        
                    } 
                    else if($fieldcode=='_VisitActualCheckInDate')
                    {
                        $crit = $this->getCriteria($filter,'vis.TechCheckInDate',
                                            self::Criteria_Type_FromDate_ToDate);
                        if(!empty($crit)) $criteriaArray[] = $crit;
                        
                    } 
                    else if($fieldcode=='_VisitActualCheckInTime')
                    {
                        $crit = $this->getCriteria($filter,'vis.TechCheckInTime',
                                            self::Criteria_Type_FromTime_ToTime);
                        if(!empty($crit)) $criteriaArray[] = $crit;

                    } 
                    else if($fieldcode=='_VisitActualCheckInResult')
                    {
                        $SelectMultiple = $filter['SelectMultiple'];
                        if(!empty($SelectMultiple) && count($SelectMultiple)>0){
                            $cri = "";
                            foreach($SelectMultiple as $v){
                                if($v=='Early'){
                                    $cri .= empty($cri) ? "" : " OR ";
                                    $cri .= "(STR_TO_DATE(vis.TechCheckInTime,'%l:%i %p') < STR_TO_DATE(vis.StartTime,'%l:%i %p'))";                                    
                                } else if($v=='Late'){
                                    $cri .= empty($cri) ? "" : " OR ";
                                    $cri .= "(STR_TO_DATE(vis.TechCheckInTime,'%l:%i %p') > STR_TO_DATE(vis.StartTime,'%l:%i %p'))";                                     
                                } else if($v=='OnTime'){
                                    $cri .= empty($cri) ? "" : " OR ";
                                    $cri .= "(STR_TO_DATE(vis.TechCheckInTime,'%l:%i %p') = STR_TO_DATE(vis.StartTime,'%l:%i %p'))";                                    
                                }                                
                            } 
                            if(!empty($cri)) $criteriaArray[] = $cri;
                        }
                    } 
                    else if($fieldcode=='_VisitActualCheckOutDate')
                    {
                        $crit = $this->getCriteria($filter,'vis.TechCheckOutDate',
                                            self::Criteria_Type_FromDate_ToDate);
                        if(!empty($crit)) $criteriaArray[] = $crit;
                        
                    } 
                    else if($fieldcode=='_VisitActualCheckOutTime')
                    {
                        $crit = $this->getCriteria($filter,'vis.TechCheckOutTime',
                                            self::Criteria_Type_FromTime_ToTime);
                        if(!empty($crit)) $criteriaArray[] = $crit;

                    } 
                    else if($fieldcode=='_VisitActualCheckOutResult')
                    {
                        $SelectMultiple = $filter['SelectMultiple'];
                        if(!empty($SelectMultiple) && count($SelectMultiple)>0){
                            $cri = "";
                            foreach($SelectMultiple as $v){
                                if($v=='Early'){
                                    $cri .= empty($cri) ? "" : " OR ";
                                    $cri .= "(STR_TO_DATE(vis.TechCheckOutTime,'%l:%i %p') < STR_TO_DATE(vis.EndTime,'%l:%i %p'))";                                    
                                } else if($v=='Late'){
                                    $cri .= empty($cri) ? "" : " OR ";
                                    $cri .= "(STR_TO_DATE(vis.TechCheckOutTime,'%l:%i %p') > STR_TO_DATE(vis.EndTime,'%l:%i %p'))";                                                                        
                                } else if($v=='OnTime'){
                                    $cri .= empty($cri) ? "" : " OR ";
                                    $cri .= "(STR_TO_DATE(vis.TechCheckOutTime,'%l:%i %p') = STR_TO_DATE(vis.EndTime,'%l:%i %p'))";                                                                        
                                }                                
                            } 
                            if(!empty($cri)) $criteriaArray[] = $cri;
                        }
                    } 
                    else if($fieldcode=='_VisitActualDurationResult')
                    {
                        $SelectMultiple = $filter['SelectMultiple'];
                        if(!empty($SelectMultiple) && count($SelectMultiple)>0){
                            $cri = "";
                            //UnderDuration, CorrectDuration, OverDuration
                            foreach($SelectMultiple as $v){
                                if($v=='UnderDuration'){
                                    $cri .= empty($cri) ? "" : " OR ";
                                    $cri .= "((TIMEDIFF(STR_TO_DATE(vis.TechCheckOutTime,'%l:%i %p'),STR_TO_DATE(vis.TechCheckInTime,'%l:%i %p'))) < (TIMEDIFF(STR_TO_DATE(vis.EndTime,'%l:%i %p'),STR_TO_DATE(vis.StartTime,'%l:%i %p'))))";                                    
                                } else if($v=='CorrectDuration'){
                                    $cri .= empty($cri) ? "" : " OR ";
                                    $cri .= "((TIMEDIFF(STR_TO_DATE(vis.TechCheckOutTime,'%l:%i %p'),STR_TO_DATE(vis.TechCheckInTime,'%l:%i %p'))) = (TIMEDIFF(STR_TO_DATE(vis.EndTime,'%l:%i %p'),STR_TO_DATE(vis.StartTime,'%l:%i %p'))))"; 
                                } else if($v=='OverDuration'){
                                    $cri .= empty($cri) ? "" : " OR ";
                                    $cri .= "((TIMEDIFF(STR_TO_DATE(vis.TechCheckOutTime,'%l:%i %p'),STR_TO_DATE(vis.TechCheckInTime,'%l:%i %p'))) > (TIMEDIFF(STR_TO_DATE(vis.EndTime,'%l:%i %p'),STR_TO_DATE(vis.StartTime,'%l:%i %p'))))";  
                                }                                
                            } 
                            if(!empty($cri)) $criteriaArray[] = $cri;
                        }
                    }                     
                    else if($fieldcode=='_VisitSiteTimeZone')
                    {
                        $cri = $this->getCriteria($filter,'z.StandardOffset',Core_Api_CusReportClass::Criteria_Type_SelectMulti);
                        if(!empty($cri)) $criteriaArray[] = $cri;
                        //z.StandardOffset
                    }
                } 
                else if(in_array($fieldcode,$AvailableSiteInfoFilters)) 
                {
                    if($fieldcode=='_Geography') {
                        $SelectMultiple = $filter['SelectMultiple'];
                        if(!empty($SelectMultiple) && count($SelectMultiple)>0){
                            $crit = "";
                            foreach($SelectMultiple as $k=>$v){
                                $crit .=  empty($crit) ?  "" : " OR ";
                                $crit .= "(zst.SiteType LIKE '$v%')";
                            }
                            $criteriaArray[] = $crit;
                        }                        
                    }
                } 
                else if(in_array($fieldcode,$AvailableBidFilters)) 
                {
                    if($fieldcode=='_BidAmount_Per')
                    {
                        $SelectMultiple = $filter['SelectMultiple'];
                        if(!empty($SelectMultiple) && count($SelectMultiple)>0){
                            $criteriaArray[] = $db->quoteInto("w.Amount_Per IN (?)", $SelectMultiple);
                        }
                    } 
                    else if($fieldcode=='_BiddingTechFLSID')
                    {
                        $value = $filter['string_value'];//string_value;
                        $criteriaArray[] = "btech.FLSID = '$value'";
                        //$bidTechFields[$fieldname] ="FLSID";    
                    } 
                    else if($fieldcode=='_BiddingTechPreferred')
                    {
                        $cri = $this->getCriteriaYesNo($filter,
                            "(EXISTS(SELECT Tech_ID FROM `client_preferred_techs` where `Tech_ID` = wbid.TechID and `CompanyID` = '$companyID'))",
                            "(NOT EXISTS(SELECT Tech_ID FROM `client_preferred_techs` where `Tech_ID` = wbid.TechID and `CompanyID` = '$companyID'))"
                        );
                        if(!empty($cri)) $criteriaArray[] = $cri;
                        
                    } 
                    else if($fieldcode=='_BidPayMax')
                    {
                        $crit = $this->getCriteria($filter,'w.PayMax',self::Criteria_Type_FromNumber_ToNumber);
                        if(!empty($crit)) $criteriaArray[] = $crit;
                    } 
                    else if($fieldcode=='_Bid_Date')
                    {
                        //$bidFields[$fieldname] ="Bid_Date";     
                        $crit = $this->getCriteria($filter,'wbid.Bid_Date',self::Criteria_Type_FromDate_ToDate);
                        if(!empty($crit)) $criteriaArray[] = $crit;
                    } 
                    else if($fieldcode=='_Hide')
                    {
                        $SelectMultiple = $filter['SelectMultiple'];
                        if(count($SelectMultiple) > 0)
                        {
                             $yesno = $SelectMultiple[0];
                             // $bidFields[$fieldname] ="Hide";       
                             if($yesno=='t' || $yesno=='Y'){
                                 $criteriaArray[] = "wbid.Hide = 1";    
                             } else if($yesno=='N') {
                                 $criteriaArray[] = "wbid.Hide = 0";
                             }
                        }                                                                        
                    } 
                    else if($fieldcode=='_TechID')
                    {
                        $value = $filter['string_value'];
                        if(!empty($value)){
                            $criteriaArray[] = "wbid.TechID = '$value'";                            
                        }
                    }
                } 
                else if(in_array($fieldcode,$AvailableTechFilters)) 
                {
                    if($fieldcode=='_TechBackedOut')
                    {
                        $crit = $this->getCriteriaYesNo($filter,
                                "w.BackOut_Tech > 0",
                                "w.BackOut_Tech IS NULL OR w.BackOut_Tech=0 OR w.BackOut_Tech=''"
                        );
                        if(!empty($crit)) $criteriaArray[] = $crit;
                    }     
                    else if($fieldcode=='_TechNoShowed')
                    {                    
                        $crit = $this->getCriteriaYesNo($filter,
                                "w.NoShow_Tech > 0",
                                "w.NoShow_Tech IS NULL OR w.NoShow_Tech=0 OR w.NoShow_Tech=''"
                        );
                        if(!empty($crit)) $criteriaArray[] = $crit;
                    }     
                    else if($fieldcode=='_TechAmount_Per')
                    {                    
                        $SelectMultiple = $filter['SelectMultiple'];
                        if(!empty($SelectMultiple) && count($SelectMultiple)>0){
                            $criteriaArray[] = $db->quoteInto("w.Amount_Per IN (?)", $SelectMultiple);
                        }
                    } 
                } 
                else if(in_array($fieldcode,$AvailableCommuniFilters)) 
                {
                    if($fieldcode=='_EmailBlastRadius')
                    {
                        $crit = $this->getCriteria($filter,'p.EmailBlastRadius',self::Criteria_Type_FromNumber_ToNumber);
                        if(!empty($crit)) $criteriaArray[] = $crit;
                    }
                } 
                else if(in_array($fieldcode,$AvailableConfirmFilters)) 
                {
                    
                }
                else if(in_array($fieldcode,$AvailablePartManaFilters)) 
                {
                    if($fieldcode=='_NumofNewParts')
                    {
                        $crit = $this->getCriteria($filter,'Count(*)',self::Criteria_Type_FromNumber_ToNumber);
                        if(!empty($crit)){
                            $criteriaArray[] = "Exists(SELECT Count(*) FROM part_entries paen 
 INNER JOIN parts pa ON pa.PartEntryID = paen.id WHERE pa.IsNew=1 AND paen.WinNum=w.WIN_NUM HAVING $crit)";
                        }
                    } 
                    else if($fieldcode=='_NumofReturnParts')
                    {
                        $crit = $this->getCriteria($filter,'Count(*)',self::Criteria_Type_FromNumber_ToNumber);
                        if(!empty($crit)){
                            $criteriaArray[] = "Exists(SELECT Count(*) FROM part_entries paen 
 INNER JOIN parts pa ON pa.PartEntryID = paen.id WHERE pa.IsNew=0 AND paen.WinNum=w.WIN_NUM HAVING $crit)";
                        }
                    } 
                    else if($fieldcode=='_PartManagerUpdatedByTech')
                    {
                        $crit = $this->getCriteriaYesNo($filter,
                    "EXISTS(SELECT TechIdLastUpd FROM part_entries WHERE TechIdLastUpd > 0 AND WinNum=w.WIN_NUM)",
                    "NOT EXISTS(SELECT TechIdLastUpd FROM part_entries WHERE TechIdLastUpd > 0 AND WinNum=w.WIN_NUM)"
                        );
                        if(!empty($crit)) $criteriaArray[] = $crit;
                    } 
                    else if($fieldcode=='_NewPartPartNum')
                    {
                        $crit = $this->getCriteria($filter,"newpart.PartNum",self::Criteria_Type_ExactMatch);
                        if(!empty($crit)) $criteriaArray[] = $crit;
                    }
                    else if($fieldcode=='_NewPartModelNum')
                    {
                        $crit = $this->getCriteria($filter,"newpart.ModelNum",self::Criteria_Type_ExactMatch);
                        if(!empty($crit)) $criteriaArray[] = $crit;
                    }
                    else if($fieldcode=='_NewPartEstArrival')
                    {
                        $crit = $this->getCriteria($filter,"newpart.ETA",self::Criteria_Type_FromDate_ToDate);
                        if(!empty($crit)) $criteriaArray[] = $crit;
                    }
                    else if($fieldcode=='_NewPartCarrier')
                    {
                        $crit = $this->getCriteria($filter,"newpart.Carrier",self::Criteria_Type_SelectMulti);
                        if(!empty($crit)) $criteriaArray[] = $crit;
                    }
                    else if($fieldcode=='_NewPartTrackingNum')
                    {
                        $crit = $this->getCriteria($filter,"newpart.TrackingNum",self::Criteria_Type_ExactMatch);
                        if(!empty($crit)) $criteriaArray[] = $crit;
                    }
                    else if($fieldcode=='_NewPartShipsTo')
                    {
                        $crit = $this->getCriteria($filter,"newpart.ShipTo",self::Criteria_Type_SelectMulti);
                        if(!empty($crit)) $criteriaArray[] = $crit;
                    }
                    else if($fieldcode=='_NewPartisConsumable')
                    {
                        $crit = $this->getCriteriaYesNo($filter,
                            "partentry.Consumable=1",
                            "partentry.Consumable IS NULL OR partentry.Consumable=0");
                        if(!empty($crit)) $criteriaArray[] = $crit;
                    }                    
                    else if($fieldcode=='_ReturnPartPartNum')
                    {
                        $crit = $this->getCriteria($filter,"retunpart.PartNum",self::Criteria_Type_ExactMatch);
                        if(!empty($crit)) $criteriaArray[] = $crit;
                    }
                    else if($fieldcode=='_ReturnPartModelNum')
                    {
                        $crit = $this->getCriteria($filter,"retunpart.ModelNum",self::Criteria_Type_ExactMatch);
                        if(!empty($crit)) $criteriaArray[] = $crit;
                    }
                    else if($fieldcode=='_ReturnPartEstArrial')
                    {
                        $crit = $this->getCriteria($filter,"retunpart.ETA",self::Criteria_Type_FromDate_ToDate);
                        if(!empty($crit)) $criteriaArray[] = $crit;
                    }
                    else if($fieldcode=='_ReturnPartCarrier')
                    {
                        $crit = $this->getCriteria($filter,"retunpart.Carrier",self::Criteria_Type_SelectMulti);
                        if(!empty($crit)) $criteriaArray[] = $crit;
                    }
                    else if($fieldcode=='_ReturnPartTrackingNum')
                    {
                        $crit = $this->getCriteria($filter,"retunpart.TrackingNum",self::Criteria_Type_ExactMatch);
                        if(!empty($crit)) $criteriaArray[] = $crit;
                    }
                    //_NewPartisConsumable, retunpart.PartNum
                    
                }
                else if(in_array($fieldcode,$AvailableAdditionFilters))
                {
                    if($fieldcode=='_PublishedDate')
                    {
                        $crit = $this->getCriteria($filter,"(CASE WHEN w.ShowTechs=1 THEN  w.DateEntered WHEN (w.ShowTechs=0 AND w.Status='assigned') THEN (SELECT MAX(DateTime_Stamp) FROM  timestamps WHERE Description like 'Work Order Assigned%' AND WIN_NUM = w.WIN_NUM) ELSE '' END)",self::Criteria_Type_FromDate_ToDate);
                        
                        if(!empty($crit)) $criteriaArray[] = $crit;
                    }                    
                }
                else if(in_array($fieldcode,$AvailableWinTagFilters))
                {
                    if($fieldcode=='_WINTagAdded') 
                    {
                        $cri = $this->getCriteriaYesNo($filter,
                            "EXISTS(SELECT wtiv.WIN_NUM, wtiv.wo_tag_item_id, wti.wo_tag_code FROM work_order_tagitem_values wtiv INNER JOIN  wo_tag_items wti ON  wti.id = wtiv.wo_tag_item_id WHERE wti.wo_tag_code ='CompletionStatus' AND wtiv.WIN_NUM = w.WIN_NUM)",
                            "NOT EXISTS(SELECT wtiv.WIN_NUM, wtiv.wo_tag_item_id, wti.wo_tag_code FROM work_order_tagitem_values wtiv INNER JOIN  wo_tag_items wti ON  wti.id = wtiv.wo_tag_item_id WHERE wti.wo_tag_code ='CompletionStatus' AND wtiv.WIN_NUM = w.WIN_NUM)"
                        );
                        if(!empty($cri)) $criteriaArray[]=$cri;

                    } 
                    else if($fieldcode=='_WINTagAddedDate') 
                    {
                        $cri = $this->getCriteria($filter,'wtiv.DateAdded',self::Criteria_Type_FromDate_ToDate);
                        if(!empty($cri)) $criteriaArray[]=$cri;
                        
                    } 
                    else if($fieldcode=='_WINTagComplete') 
                    {
                        $cri = $this->getCriteriaYesNo($filter,
                            "EXISTS(SELECT wtiv.WIN_NUM, wtiv.wo_tag_item_id, wti.wo_tag_code FROM work_order_tagitem_values wtiv INNER JOIN  wo_tag_items wti ON  wti.id = wtiv.wo_tag_item_id WHERE wti.wo_tag_code ='CompletionStatus' AND wti.item_sortdesc = 'Complete' AND wtiv.WIN_NUM = w.WIN_NUM)",
                            "NOT EXISTS(SELECT wtiv.WIN_NUM, wtiv.wo_tag_item_id, wti.wo_tag_code FROM work_order_tagitem_values wtiv INNER JOIN  wo_tag_items wti ON  wti.id = wtiv.wo_tag_item_id WHERE wti.wo_tag_code ='CompletionStatus' AND wti.item_sortdesc = 'Complete' AND wtiv.WIN_NUM = w.WIN_NUM)"
                        );
                        if(!empty($cri)) $criteriaArray[]=$cri;
                        
                    } 
                    else if($fieldcode=='_WINTagIncomplete') 
                    {
                        $cri = $this->getCriteriaYesNo($filter,
                            "EXISTS(SELECT wtiv.WIN_NUM, wtiv.wo_tag_item_id, wti.wo_tag_code FROM work_order_tagitem_values wtiv INNER JOIN  wo_tag_items wti ON  wti.id = wtiv.wo_tag_item_id WHERE wti.wo_tag_code ='CompletionStatus' AND wti.item_sortdesc = 'Incomplete' AND wtiv.WIN_NUM = w.WIN_NUM)",
                            "NOT EXISTS(SELECT wtiv.WIN_NUM, wtiv.wo_tag_item_id, wti.wo_tag_code FROM work_order_tagitem_values wtiv INNER JOIN  wo_tag_items wti ON  wti.id = wtiv.wo_tag_item_id WHERE wti.wo_tag_code ='CompletionStatus' AND wti.item_sortdesc = 'Incomplete' AND wtiv.WIN_NUM = w.WIN_NUM)"
                        );
                        if(!empty($cri)) $criteriaArray[]=$cri;
                        
                        //Incomplete
                    } 
                    else if($fieldcode=='_WINTagRescheduled') 
                    {
                        $cri = $this->getCriteriaYesNo($filter,
                            "EXISTS(SELECT wtiv.WIN_NUM, wtiv.wo_tag_item_id, wti.wo_tag_code FROM work_order_tagitem_values wtiv INNER JOIN  wo_tag_items wti ON  wti.id = wtiv.wo_tag_item_id WHERE wti.wo_tag_code ='CompletionStatus' AND wti.item_sortdesc = 'Rescheduled' AND wtiv.WIN_NUM = w.WIN_NUM)",
                            "NOT EXISTS(SELECT wtiv.WIN_NUM, wtiv.wo_tag_item_id, wti.wo_tag_code FROM work_order_tagitem_values wtiv INNER JOIN  wo_tag_items wti ON  wti.id = wtiv.wo_tag_item_id WHERE wti.wo_tag_code ='CompletionStatus' AND wti.item_sortdesc = 'Rescheduled' AND wtiv.WIN_NUM = w.WIN_NUM)"
                        );
                        if(!empty($cri)) $criteriaArray[]=$cri;
                        
                    } 
                    else if($fieldcode=='_WINTagSiteCancelled') 
                    {
                        $cri = $this->getCriteriaYesNo($filter,
                            "EXISTS(SELECT wtiv.WIN_NUM, wtiv.wo_tag_item_id, wti.wo_tag_code FROM work_order_tagitem_values wtiv INNER JOIN  wo_tag_items wti ON  wti.id = wtiv.wo_tag_item_id WHERE wti.wo_tag_code ='CompletionStatus' AND wti.item_sortdesc = 'Cancelled' AND wtiv.WIN_NUM = w.WIN_NUM)",
                            "NOT EXISTS(SELECT wtiv.WIN_NUM, wtiv.wo_tag_item_id, wti.wo_tag_code FROM work_order_tagitem_values wtiv INNER JOIN  wo_tag_items wti ON  wti.id = wtiv.wo_tag_item_id WHERE wti.wo_tag_code ='CompletionStatus' AND wti.item_sortdesc = 'Cancelled' AND wtiv.WIN_NUM = w.WIN_NUM)"
                        );
                        if(!empty($cri)) $criteriaArray[]=$cri;
                        
                        
                    } 
                    else if($fieldcode=='_WINTagOwner') 
                    {
                        $cri = $this->getCriteria($filter,'wti.item_owner',self::Criteria_Type_SelectMulti);
                        if(!empty($cri)) $criteriaArray[]=$cri;
                        
                    } 
                    else if($fieldcode=='_WINTagBillable') 
                    {
                        $cri = $this->getCriteriaYesNo($filter,
                            "wtiv.value_id=2",
                            "wtiv.value_id=1"
                        );
                        if(!empty($cri)) $criteriaArray[]=$cri;
                        
                    } 
                    else if($fieldcode=='_WINTagCheckInResult') 
                    {
                        $SelectMultiple = $filter['SelectMultiple'];
                        if(!empty($SelectMultiple) && count($SelectMultiple)>0){
                            $cri = "";
                            foreach($SelectMultiple as $v){
                               if($v=='Early'){
                                   $cri .= empty($cri) ? "" : " OR ";
                                   $cri .= "(STR_TO_DATE(vis.TechCheckInTime,'%l:%i %p') < STR_TO_DATE(vis.StartTime,'%l:%i %p'))";                                    
                               } else if($v='Late') {
                                   $cri .= empty($cri) ? "" : " OR ";
                                   $cri .= "(STR_TO_DATE(vis.TechCheckInTime,'%l:%i %p') > STR_TO_DATE(vis.StartTime,'%l:%i %p'))";                                    
                               } else if($v='On Time') {
                                   $cri .= empty($cri) ? "" : " OR ";
                                   $cri .= "(STR_TO_DATE(vis.TechCheckInTime,'%l:%i %p') = STR_TO_DATE(vis.StartTime,'%l:%i %p'))";                                     
                               }
                            }
                            if(!empty($cri)) $criteriaArray[]=$cri;
                        }
                        
                    } 
                    else if($fieldcode=='_WINTagCheckOutResult') 
                    {
                        $SelectMultiple = $filter['SelectMultiple'];
                        if(!empty($SelectMultiple) && count($SelectMultiple)>0){
                            $cri = "";
                            foreach($SelectMultiple as $v){
                                if($v=='Early'){
                                    $cri .= empty($cri) ? "" : " OR ";
                                    $cri .= "(STR_TO_DATE(vis.TechCheckOutTime,'%l:%i %p') < STR_TO_DATE(vis.EndTime,'%l:%i %p'))";                                    
                                } else if($v=='Late'){
                                    $cri .= empty($cri) ? "" : " OR ";
                                    $cri .= "(STR_TO_DATE(vis.TechCheckOutTime,'%l:%i %p') > STR_TO_DATE(vis.EndTime,'%l:%i %p'))";                                                                        
                                } else if($v=='On Time'){
                                    $cri .= empty($cri) ? "" : " OR ";
                                    $cri .= "(STR_TO_DATE(vis.TechCheckOutTime,'%l:%i %p') = STR_TO_DATE(vis.EndTime,'%l:%i %p'))";                                                                        
                                }                                
                            } 
                            if(!empty($cri)) $criteriaArray[] = $cri;
                        }
                    } 
                    else if($fieldcode=='_WINTagDurationResult') 
                    {
                        $SelectMultiple = $filter['SelectMultiple'];
                        if(!empty($SelectMultiple) && count($SelectMultiple)>0){
                            $cri = "";
                            //UnderDuration, CorrectDuration, OverDuration
                            foreach($SelectMultiple as $v){
                                if($v=='Under Duration'){
                                    $cri .= empty($cri) ? "" : " OR ";
                                    $cri .= "((TIMEDIFF(STR_TO_DATE(vis.TechCheckOutTime,'%l:%i %p'),STR_TO_DATE(vis.TechCheckInTime,'%l:%i %p'))) < (TIMEDIFF(STR_TO_DATE(vis.EndTime,'%l:%i %p'),STR_TO_DATE(vis.StartTime,'%l:%i %p'))))";                                    
                                } else if($v=='Correct Duration'){
                                    $cri .= empty($cri) ? "" : " OR ";
                                    $cri .= "((TIMEDIFF(STR_TO_DATE(vis.TechCheckOutTime,'%l:%i %p'),STR_TO_DATE(vis.TechCheckInTime,'%l:%i %p'))) = (TIMEDIFF(STR_TO_DATE(vis.EndTime,'%l:%i %p'),STR_TO_DATE(vis.StartTime,'%l:%i %p'))))"; 
                                } else if($v=='Over Duration'){
                                    $cri .= empty($cri) ? "" : " OR ";
                                    $cri .= "((TIMEDIFF(STR_TO_DATE(vis.TechCheckOutTime,'%l:%i %p'),STR_TO_DATE(vis.TechCheckInTime,'%l:%i %p'))) > (TIMEDIFF(STR_TO_DATE(vis.EndTime,'%l:%i %p'),STR_TO_DATE(vis.StartTime,'%l:%i %p'))))";  
                                }                                
                            } 
                            if(!empty($cri)) $criteriaArray[] = $cri;
                        }

                    } 
                    else if($fieldcode=='_WINTagTechPerformanceRating') 
                    {
                        $cri = $this->getCriteria($filter,'w.SATPerformance',self::Criteria_Type_FromNumber_ToNumber);
                        if(!empty($cri)) $criteriaArray[] = $cri;
                    } 
                    else if($fieldcode=='_WINTagTechPreferenceRating') 
                    {
                        $cri = $this->getCriteria($filter,'w.SATRecommended',self::Criteria_Type_FromNumber_ToNumber);
                        if(!empty($cri)) $criteriaArray[] = $cri;                        
                    }
                    else if($fieldcode=='_WINTag') 
                    {
                        $cri= $this->getCriteria($filter,'wti.item_sortdesc',self::Criteria_Type_SelectMulti);
                        if(!empty($cri)) $criteriaArray[] = $cri;                        
                        //wti.item_sortdesc
                    }
                }
                                 
            }//end of foreach
        }
        $db = Core_Database::getInstance();  
        
        //--- categoryList
        $categoryList = array();
        if(in_array('WO_Category_ID',$allReportFields)) {
            $selectCat =  $db->select();
            $selectCat->from("wo_categories");
            $catRecords = Core_Database::fetchAll($selectCat);
            if(!empty($catRecords) && count($catRecords) > 0) {
                foreach($catRecords as $rec){
                    $catID = $rec['Category_ID'];
                    $categoryList[$catID] = $rec['Category'];
                }
            }                  
        }       

        //--- main query
        $select = $db->select();
        
                
        if($forCount===true){
            $select->from(array('w'=>Core_Database::TABLE_WORK_ORDERS),
                    array('NUM'=>'Count(*)'));
        } else if($forCount===false) {
            if(empty($allReportFields) || count($allReportFields)==0){
                return null;
            }
            $select->from(array('w'=>Core_Database::TABLE_WORK_ORDERS),$woFields);   
        }
        
        //$woTimeZoneFields
        if(count($woTimeZoneFields)>0 || $hasT2Filter)
        {
            if($forCount===true){
                $select->joinLeft(array('z'=>'zipcodes'),
                    'z.ZIPCode = w.Zipcode',
                    array()
                );                            
            } else {
                $select->joinLeft(array('z'=>'zipcodes'),
                    'z.ZIPCode = w.Zipcode',
                    $woTimeZoneFields
                );                                            
            }
        }

        //$woZipSiteTypesFields
        if(count($woZipSiteTypesFields)>0)
        {
            if($forCount===true){
                $select->joinLeft(array('zst'=>'zipcode_sitetypes'),
                    'zst.ZipCode = w.Zipcode',
                    array()
                );                            
            } else {
                $select->joinLeft(array('zst'=>'zipcode_sitetypes'),
                    'zst.ZipCode = w.Zipcode',
                    $woZipSiteTypesFields
                );                            
            }
        }

        //$woTechFields
        if(count($woTechFields))
        {
            if($forCount===true){
                $select->joinLeft(array('wt'=>Core_Database::TABLE_TECH_BANK_INFO),
                    'wt.TechID = w.Tech_ID',
                    $woTechFields
                );                                        
            } else {
                $select->joinLeft(array('wt'=>Core_Database::TABLE_TECH_BANK_INFO),
                    'wt.TechID = w.Tech_ID',
                    $woTechFields
                );                                        
            }            
        }        
            
        //$woAssignedTechBidFields
        if(count($woAssignedTechBidFields)>0) 
        {
            $assignedTechBidQuery = $db->select();
            $assignedTechBidQuery->from('work_orders__bids',
                 array('WorkOrderID','TechID','BidAmount','BidDate'=>'(MAX(Bid_Date))')
            );
            $assignedTechBidQuery->group('WorkOrderID');
            $assignedTechBidQuery->group('TechID');
            
            if($forCount===true){
                $select->joinLeft(array('atb'=>$assignedTechBidQuery),
                    'atb.TechID = w.Tech_ID AND atb.WorkOrderID = w.WIN_NUM',
                     array()
                );                
            } else {
                $select->joinLeft(array('atb'=>$assignedTechBidQuery),
                    'atb.TechID = w.Tech_ID AND atb.WorkOrderID = w.WIN_NUM',
                     $woAssignedTechBidFields
                );                
            }            
        }
        
        if(count($woProjectFields)>0)
        {
            if($forCount===true){
                $select->joinLeft(array('p'=>'projects'),
                    'p.Project_ID = w.Project_ID',
                    array()
                );                            
            } else {
                $select->joinLeft(array('p'=>'projects'),
                    'p.Project_ID = w.Project_ID',
                    $woProjectFields
                );                            
            }

        }
        
        if(count($woEntryPartFields)>0 || count($woNewPartFields)>0 || count($woReturnPartFields)>0)
        {
            if($forCount===true){
                $select->joinLeft(array('partentry'=>'part_entries'),
                    'partentry.WinNum = w.WIN_NUM',
                    array()
                );
                if(count($woNewPartFields)>0) {
                    $select->joinLeft(array('newpart'=>'parts'),
                        'newpart.PartEntryID = partentry.id AND newpart.IsNew=1',
                        array()
                    );                
                }
                if(count($woReturnPartFields)>0) {
                    $select->joinLeft(array('retunpart'=>'parts'),
                        'retunpart.PartEntryID = partentry.id AND retunpart.IsNew=0',
                        array()
                    );                
                }                                              
            } else {
                $select->joinLeft(array('partentry'=>'part_entries'),
                    'partentry.WinNum = w.WIN_NUM',
                    $woEntryPartFields
                );
                if(count($woNewPartFields)>0)      {
                    $select->joinLeft(array('newpart'=>'parts'),
                        'newpart.PartEntryID = partentry.id AND newpart.IsNew=1',
                        $woNewPartFields
                    );                
                }
                if(count($woReturnPartFields)>0)      {
                    $select->joinLeft(array('retunpart'=>'parts'),
                        'retunpart.PartEntryID = partentry.id AND retunpart.IsNew=0',
                        $woReturnPartFields
                    );                
                }                  
            }
        }
        //--- $clientExtFields
        if(count($clientExtFields) > 0)        
        {
            $select->joinLeft(array('cli'=>'clients'),
                'cli.UserName = w.WorkOrderOwner',
                array('ClientID')
            );
            if($forCount===true){
                $select->joinLeft(array('cext'=>'clients_ext'),
                    'cext.ClientId = cli.ClientID',
                    array()
                );
                if(count($clientExtAMFields) > 0){
                    $select->joinLeft(array('cextAM'=>'operations_staff'),
                        'cextAM.id = cext.FSAccountManagerId',
                         array()
                    );
                }
                if(count($clientExtCSDFields) > 0){
                    $select->joinLeft(array('cextCSD'=>'operations_staff'),
                        'cextCSD.id = cext.FSClientServiceDirectorId',
                         array()
                    );                
                }            
                if(count($clientExtLEFields) > 0){
                    $select->joinLeft(array('cextLE'=>'wo_tag_items'),
                        'cextLE.id = cext.EntityTypeId',
                         array()
                    );                                
                }
                if(count($clientExtSTFields) > 0){
                    $select->joinLeft(array('cextST'=>'wo_tag_items'),
                        'cextST.id = cext.ServiceTypeId',
                         array()
                    );                                
                }
            } else {
                $select->joinLeft(array('cext'=>'clients_ext'),
                    'cext.ClientId = cli.ClientID',
                    $clientExtFields
                );
                if(count($clientExtAMFields) > 0){
                    $select->joinLeft(array('cextAM'=>'operations_staff'),
                        'cextAM.id = cext.FSAccountManagerId',
                        $clientExtAMFields
                    );
                }
                if(count($clientExtCSDFields) > 0){
                    $select->joinLeft(array('cextCSD'=>'operations_staff'),
                        'cextCSD.id = cext.FSClientServiceDirectorId',
                        $clientExtCSDFields
                    );                
                }            
                if(count($clientExtLEFields) > 0){
                    $select->joinLeft(array('cextLE'=>'wo_tag_items'),
                        'cextLE.id = cext.EntityTypeId',
                        $clientExtLEFields
                    );                                
                }
                if(count($clientExtSTFields) > 0){
                    $select->joinLeft(array('cextST'=>'wo_tag_items'),
                        'cextST.id = cext.ServiceTypeId',
                        $clientExtSTFields
                    );                                
                }

            }

        }
        
        //--- visit fields
        if(count($visitFields)>0) 
        {
            $visitQuery = $db->select();
            $visitQuery->from(array('vis'=>'work_order_visits'),array('*'));
            if($reportType != self::Report_Type_Visits){
                $visitQuery->where('Type = 1');
                $visitQuery->group('WIN_NUM');
            }
            $visitQuery->order('Type asc');
            
            if($reportType==self::Report_Type_Visits){
                if($forCount===true){
                    $select->join(array('vis'=>'work_order_visits'),
                        'vis.WIN_NUM = w.WIN_NUM',
                         array()
                    );                
                } else {
                    $select->join(array('vis'=>'work_order_visits'),
                        'vis.WIN_NUM = w.WIN_NUM',
                         $visitFields
                    );                
                }                                        
            } else {
                if($forCount===true){
                    $select->join(array('vis'=>'work_order_visits'),
                        'vis.WIN_NUM = w.WIN_NUM AND vis.Type=1',
                         array()
                    );                
                } else {
                    $select->join(array('vis'=>'work_order_visits'),
                        'vis.WIN_NUM = w.WIN_NUM AND vis.Type=1',
                         $visitFields
                    );                
                }                                                        
            }
        }
        
        
        //--- $woAdditionalFields
        if(count($woAdditionalFields)>0)
        {
            if($forCount===true){
                $select->joinLeft(array('woadd'=>'work_orders_additional_fields'),
                    'woadd.WINNUM = w.WIN_NUM',
                    $woAdditionalFields
                );            
                if(count($woAdditionalAMFields) > 0){
                    $select->joinLeft(array('woaddAM'=>'operations_staff'),
                        'woaddAM.id = woadd.FSAccountManagerId',
                        $woAdditionalAMFields
                    );
                }
                if(count($woAdditionalCSDFields) > 0){
                    $select->joinLeft(array('woaddCSD'=>'operations_staff'),
                        'woaddCSD.id = woadd.FSClientServiceDirectorId',
                        $woAdditionalCSDFields
                    );                
                }                
            } else {
                $select->joinLeft(array('woadd'=>'work_orders_additional_fields'),
                    'woadd.WINNUM = w.WIN_NUM',
                    $woAdditionalFields
                );            
                if(count($woAdditionalAMFields) > 0){
                    $select->joinLeft(array('woaddAM'=>'operations_staff'),
                        'woaddAM.id = woadd.FSAccountManagerId',
                        $woAdditionalAMFields
                    );
                }
                if(count($woAdditionalCSDFields) > 0){
                    $select->joinLeft(array('woaddCSD'=>'operations_staff'),
                        'woaddCSD.id = woadd.FSClientServiceDirectorId',
                        $woAdditionalCSDFields
                    );                
                }                
            }

        }
        
        if(count($bidFields) > 0 || count($bidTechFields) > 0) 
        {
            if($forCount===true){
                $select->joinLeft(array('wbid'=>Core_Database::TABLE_WORK_ORDER_BIDS),
                    'wbid.WorkOrderID = w.WIN_NUM',
                    array()
                );
                if(count($bidTechFields)>0 || $hasBidTechFilter){
                    $select->joinLeft(array('btech'=>Core_Database::TABLE_TECH_BANK_INFO),
                        'btech.TechID = wbid.TechID',
                        array()
                    );
                }                
            } else {
                $select->joinLeft(array('wbid'=>Core_Database::TABLE_WORK_ORDER_BIDS),
                    'wbid.WorkOrderID = w.WIN_NUM',
                    $bidFields
                );
                if(count($bidTechFields)>0 || $hasBidTechFilter){
                    $select->joinLeft(array('btech'=>Core_Database::TABLE_TECH_BANK_INFO),
                        'btech.TechID = wbid.TechID',
                        $bidTechFields
                    );
                }                
            }            
        }

        //$woWintagFields
        if(count($woWintagItemValueFields)>0 || count($woWintagItemFields)>0)
        {
             if($forCount===true){
                $select->joinLeft(array('wtiv'=>'work_order_tagitem_values'),
                    'wtiv.WIN_NUM = w.WIN_NUM',
                    array()
                );
                $select->joinLeft(array('wti'=>'wo_tag_items'),
                    'wti.id = wtiv.wo_tag_item_id',
                    array()
                );
            } else {
                $select->joinLeft(array('wtiv'=>'work_order_tagitem_values'),
                    'wtiv.WIN_NUM = w.WIN_NUM',
                    $woWintagItemValueFields
                );
                $select->joinLeft(array('wti'=>'wo_tag_items'),
                    'wti.id = wtiv.wo_tag_item_id',
                    $woWintagItemFields
                );
            }            
        }
        
        if(!empty($companyID)){
            $select->where("w.Company_ID = '$companyID'");            
        }
        
        //print_r($select->__toString());die();        
        //--- $criteria
        if(count($criteriaArray)>0){
            foreach($criteriaArray as $criteria){
                $select->where("$criteria");            
            }
        }
        
        //--- sort By        
        if(!empty($sorts) && count($sorts)>0){
            if(!empty($sorts['sort_1'])){
                if(in_array($sorts['sort_1'],$allReportFields)){
                    $select->order($sorts['sort_1'].' '.$sorts['sort_dir_1']);
                }
            }
            if(!empty($sorts['sort_2'])){
                if(in_array($sorts['sort_1'],$allReportFields)){                
                    $select->order($sorts['sort_2'].' '.$sorts['sort_dir_2']);
                }
            }
            if(!empty($sorts['sort_3'])){
                if(in_array($sorts['sort_1'],$allReportFields)){                
                    $select->order($sorts['sort_3'].' '.$sorts['sort_dir_3']);
                }
            }
        }
        
        //--- limit, offset
        if($forCount!==true){
            if(!empty($limit) || !empty($offset)){
                $select->limit($limit,$offset);            
            }            
        }
        
        //print_r($select->__toString());echo("<br/><br/><br/>");//die();

        if($forCount===true){
            $count = 0;
            //echo("<br/>groups: ");print_r($groups);echo("<br/><br/><br/>");//die();
           //echo("<br/>count->select: ");print_r($select->__toString());echo("<br/><br/><br/>");//die();
            $records = Core_Database::fetchAll($select);                  
            if(!empty($records) && count($records)>0){
                if((!empty($groups) && count($groups)>0) || (!empty($counts) && count($counts)>0) || (!empty($sums) && count($sums)>0) || (!empty($avgs) && count($avgs)>0)){
                   $count = count($records);                     
                } else {
                    $count = $records[0]['NUM'];
                }
            } 
            //echo("<br/>count: $count");
            return $count;            
        } else {
            //echo("<br/>select: ");print_r($select->__toString());echo("<br/>");//die();
            $resultArray = array();
            
            $records = Core_Database::fetchAll($select);      
            if(!empty($records) && count($records)>0)
            {
                foreach($records as $rec){
                    $result = array();                    
                    foreach($ReportFieldList as $key=>$val){
                        $fieldname = $key;   
                        if(!empty($rec[$key])){
                            if($key=='WO_Category_ID'){
                                $result[$key]=$categoryList[$rec[$key]];
                            } else {
                                $result[$key]=$rec[$key];
                            }
                        } else {
                            $result[$key]='';
                        }                        
                    }
                    $resultArray[]=$result;
                }   
            }
            return $resultArray;
        }
    }

    
    public function getFilterDefaultValue2($filterRecord)
    {
        //datatype,fieldcode,id,SelectMultiple,datetime_relative_data_ref,datetime_range_from,datetime_range_to
         $dbDataType = $filterRecord['datatype'];
         $fieldcode = $filterRecord['fieldcode'];
         $pos = strpos($fieldcode, '_');
         if($pos===false || $pos > 0){
             $fieldcode="w.".$fieldcode;
         }

         $filterID = $filterRecord['id'];
         $SelectMultiList = $filterRecord['SelectMultiple'];

         $dataType = $this->getDataType($dbDataType);            
         $dbDefaultValue = "";
         $defaultValue = "";
         //echo("<br/>dataType: '$dataType'");
         $string_matching = $filterRecord['string_matching'];

         if($string_matching==self::String_Matching_SelectMultiple)
         {
             $defaultValue = "SELECT_MULTIPLE";
             
             $valueList = array();
             if(!empty($SelectMultiList) && is_array($SelectMultiList)) {
                 $valueList = $SelectMultiList;
             } else  if(!empty($filterID)){
                 $valueList = $this->getValueList_ForSelMultiFilter($filterID);
             }
             
             //echo("<br/>SELECT_MULTIPLE-valueList: ");print_r($valueList);
             $values = ""; 
             if(!empty($valueList) && count($valueList)>0){
                foreach($valueList as $value){
                    if($fieldcode=='Status'){
                        $value = str_replace('_',' ',$value);
                    }
                    if(empty($values)){
                        $values = "\"".$value."\"";        
                    } else {
                        $values .= ",\"".$value."\"";
                    }
                } 
             }
             //echo("<br/>SELECT_MULTIPLE-values: ");print_r($values);

             if(!empty($values)){
                $dbDefaultValue = "$fieldcode IN ($values)";
             } else {
                $dbDefaultValue = "";                         
             }
             //echo("<br/>SELECT_MULTIPLE-dbDefaultValue: ");print_r($dbDefaultValue);
         }      
         else if($dataType==self::DATATYPE_DATETIME) 
         {
             
            $Today = date("m/d/Y");
            $nforToday = date("N");//1 (for Monday) through 5 (for Friday),  6(Saturday) 7 (for Sunday)
            // Today 
            $BOToday = date("Y-m-d H:i:s", mktime(0, 0, 0, date("m"), date("d"), date("Y")) );
            $EOToday = date("Y-m-d H:i:s", mktime(23, 59, 59, date("m"), date("d"), date("Y")));
            // Tomorrow
            $BOTomorrow = date("Y-m-d H:i:s", mktime(0, 0, 0, date("m"), date("d") + 1, date("Y")) );
            $EOTomorrow = date("Y-m-d H:i:s", mktime(23, 59, 59, date("m"), date("d") + 1, date("Y")));

            //Next Business Day, Next 2 Business days, Next 3 Business days, Next 4 Business days, Next 5 Business days
            if($nforToday<=4){
                $BONextBuDay = date("Y-m-d H:i:s", mktime(0, 0, 0, date("m"), date("d") + 1, date("Y")));    
                $EONextBuDay = date("Y-m-d H:i:s", mktime(23, 59, 59, date("m"), date("d") + 1, date("Y")));    
            } else {
                $BONextBuDay = date("Y-m-d H:i:s", mktime(0, 0, 0, date("m"), date("d") - $this->getDay(date("N")) + 9, date("Y")));    
                $EONextBuDay = date("Y-m-d H:i:s", mktime(23, 59, 59, date("m"), date("d") - $this->getDay(date("N")) + 9, date("Y")));    
            }
            $nbd = strtotime($BONextBuDay);
            $nforNBD = date("N",strtotime($BONextBuDay)); // 1->5
            $EO_Fri = date("Y-m-d H:i:s", mktime(23, 59, 59, date("m",$nbd), date("d",$nbd) - $nforNBD + 5 , date("Y",$nbd)));            
            $BO_NextMonday = date("Y-m-d H:i:s", mktime(0, 0, 0, date("m",$nbd), date("d",$nbd) - $nforNBD + 8 , date("Y",$nbd)));

            if($nforNBD <= 4){
                $EONext2Budays = date("Y-m-d H:i:s", mktime(23, 59, 59, date("m",$nbd), date("d",$nbd) + 1, date("Y",$nbd)));
            } else {
                $Next2Budays_hasSaSu = true;
                $EONext2Budays = date("Y-m-d H:i:s", mktime(23, 59, 59, date("m",$nbd), date("d",$nbd) + 3, date("Y",$nbd)));
            }

            if($nforNBD <= 3){
                $EONext3Budays = date("Y-m-d H:i:s", mktime(23, 59, 59, date("m",$nbd), date("d",$nbd) + 2, date("Y",$nbd)));
            } else {
                $Next3Budays_hasSaSu = true;
                $EONext3Budays = date("Y-m-d H:i:s", mktime(23, 59, 59, date("m",$nbd), date("d",$nbd) + 4, date("Y",$nbd)));
            }

            if($nforNBD <= 2){
                $EONext4Budays = date("Y-m-d H:i:s", mktime(23, 59, 59, date("m",$nbd), date("d",$nbd) + 3, date("Y",$nbd)));
            } else {
                $Next4Budays_hasSaSu = true;
                $EONext4Budays = date("Y-m-d H:i:s", mktime(23, 59, 59, date("m",$nbd), date("d",$nbd) + 5, date("Y",$nbd)));
            }

            if($nforNBD == 1){
                $EONext5Budays = date("Y-m-d H:i:s", mktime(23, 59, 59, date("m",$nbd), date("d",$nbd) + 4, date("Y",$nbd)));
            } else {
                $Next4Budays_hasSaSu = true;                
                $EONext5Budays = date("Y-m-d H:i:s", mktime(23, 59, 59, date("m",$nbd), date("d",$nbd) + 6, date("Y",$nbd)));
            }
            
            // Next2days, Next3days, Next4days, Next5Days
            $EONext2days = date("Y-m-d H:i:s", mktime(23, 59, 59, date("m"), date("d") + 2, date("Y")));
            $EONext3days = date("Y-m-d H:i:s", mktime(23, 59, 59, date("m"), date("d") + 3, date("Y")));
            $EONext4days = date("Y-m-d H:i:s", mktime(23, 59, 59, date("m"), date("d") + 4, date("Y")));
            $EONext5days = date("Y-m-d H:i:s", mktime(23, 59, 59, date("m"), date("d") + 5, date("Y")));            
            // This Week (Mon = 1 - Sun = 7)
            $BOW = date("Y-m-d", mktime(0, 0, 0, date("m"), date("d") - $this->getDay(date("N")) + 1, date("Y")));
            $EOW = date("Y-m-d", mktime(0, 0, 0, date("m"), date("d") - $this->getDay(date("N")) + 7, date("Y")));
            // This Month
            $BOM = date("Y-m-d", mktime(0, 0, 0, date("m"), date("d") - date("d") + 1, date("Y")));
            $EOM = date("Y-m-d", mktime(0, 0, 0, date("m") + 1, date("d") - date("d"), date("Y")));
            // Next Month
            $BONextMonth = date("Y-m-d", mktime(0, 0, 0, date("m") + 1, date("d") - date("d") + 1, date("Y")));
            $EONextMonth = date("Y-m-d", mktime(0, 0, 0, date("m") + 1, date("d") - date("d"), date("Y")));
            // Yesterday
            $BOYesterday = date("Y-m-d H:i:s", mktime(0, 0, 0, date("m"), date("d") - 1, date("Y")) );
            $EOYesterday = date("Y-m-d H:i:s", mktime(23, 59, 59, date("m"), date("d") - 1, date("Y")));
            
            //Last Business Day, Last 2 Business days, Last 3 Business days, Last 4 Business days, Last 5 Business days
            $minus = 0;
            if($nforToday == 1){
                $minus = 3;
            } else if($nforToday >=2 && $nforToday <=6 ) {
                $minus = 1;
            } else if($nforToday==7){
                $minus = 2;
            }
            $BOLastBuDay = date("Y-m-d H:i:s", mktime(0, 0, 0, date("m"), date("d") - $minus, date("Y")));    
            $EOLastBuDay = date("Y-m-d H:i:s", mktime(23, 59, 59, date("m"), date("d") - $minus, date("Y")));    
            
            $lbd = strtotime($BOLastBuDay);
            $nforLBD = date("N",strtotime($BOLastBuDay)); // 1->5
            $EO_LastFri = date("Y-m-d H:i:s", mktime(23, 59, 59, date("m",$lbd), date("d",$lbd) - $nforLBD - 2 , date("Y",$lbd)));            
            $BO_ThisMonday = date("Y-m-d H:i:s", mktime(0, 0, 0, date("m",$lbd), date("d",$lbd) - $nforLBD + 1 , date("Y",$lbd)));


            $minus = 0;
            if($nforLBD == 1){
                $Last2Budays_hasSaSu = true;
                $BOLast2Budays = date("Y-m-d H:i:s", mktime(0, 0, 0, date("m",$lbd), date("d",$lbd) - 3, date("Y",$lbd)));
            } else {
                $BOLast2Budays = date("Y-m-d H:i:s", mktime(0, 0, 0, date("m",$lbd), date("d",$lbd) - 1, date("Y",$lbd)));
            }

            if($nforLBD <= 2){
                $Last3Budays_hasSaSu = true;
                $BOLast3Budays = date("Y-m-d H:i:s", mktime(0, 0, 0, date("m",$lbd), date("d",$lbd) - 4, date("Y",$lbd)));
            } else {
                $BOLast3Budays = date("Y-m-d H:i:s", mktime(0, 0, 0, date("m",$lbd), date("d",$lbd) - 2, date("Y",$lbd)));
            }

            if($nforLBD <= 3){
                $Last4Budays_hasSaSu = true;
                $BOLast4Budays = date("Y-m-d H:i:s", mktime(0, 0, 0, date("m",$lbd), date("d",$lbd) - 5, date("Y",$lbd)));
            } else {
                $BOLast4Budays = date("Y-m-d H:i:s", mktime(0, 0, 0, date("m",$lbd), date("d",$lbd) - 3, date("Y",$lbd)));
            }

            if($nforLBD <= 4){
                $Last5Budays_hasSaSu = true;
                $BOLast5Budays = date("Y-m-d H:i:s", mktime(0, 0, 0, date("m",$lbd), date("d",$lbd) - 6, date("Y",$lbd)));
            } else {
                $BOLast5Budays = date("Y-m-d H:i:s", mktime(0, 0, 0, date("m",$lbd), date("d",$lbd) - 4, date("Y",$lbd)));
            }
            
            // Last2days, Last3days, Last4days, Last5days
            $BOLast2days = date("Y-m-d H:i:s", mktime(0, 0, 0, date("m"), date("d") - 2, date("Y")));
            $BOLast3days = date("Y-m-d H:i:s", mktime(0, 0, 0, date("m"), date("d") - 3, date("Y")));
            $BOLast4days = date("Y-m-d H:i:s", mktime(0, 0, 0, date("m"), date("d") - 4, date("Y")));
            $BOLast5days = date("Y-m-d H:i:s", mktime(0, 0, 0, date("m"), date("d") - 5, date("Y")));            
            // last Week (Mon = 1 - Sun = 7)
            $BOLW = date("Y-m-d", mktime(0, 0, 0, date("m"), date("d") - $this->getDay(date("N")) - 6, date("Y")));
            $EOLW = date("Y-m-d", mktime(0, 0, 0, date("m"), date("d") - $this->getDay(date("N")), date("Y")));
            // Last Month
            $PrevBOM = date("Y-m-d", mktime(0, 0, 0, date("m") - 1, date("d") - date("d") + 1, date("Y")));
            $PrevEOM = date("Y-m-d", mktime(0, 0, 0, date("m"), date("d") - date("d"), date("Y")));
            // Q1
            $Q1Start = date("Y-m-d", mktime(0, 0, 0, 1, 1, date("Y")));
            $Q1End = date("Y-m-d", mktime(0, 0, 0, 4, date("d") - date("d"), date("Y")));
            // Q2
            $Q2Start = date("Y-m-d", mktime(0, 0, 0, 4, 1, date("Y")));
            $Q2End = date("Y-m-d", mktime(0, 0, 0, 7, date("d") - date("d"), date("Y")));
            // Q3
            $Q3Start = date("Y-m-d", mktime(0, 0, 0, 7, 1, date("Y")));
            $Q3End = date("Y-m-d", mktime(0, 0, 0, 10, date("d") - date("d"), date("Y")));
            // Q4
            $Q4Start = date("Y-m-d", mktime(0, 0, 0, 10, 1, date("Y")));
            $Q4End = date("Y-m-d", mktime(0, 0, 0, 13, date("d") - date("d"), date("Y")));
            // H1
            $H1Start = date("Y-m-d", mktime(0, 0, 0, 1, 1, date("Y")));
            $H1End = date("Y-m-d", mktime(0, 0, 0, 7, date("d") - date("d"), date("Y")));
            // H2
            $H2Start = date("Y-m-d", mktime(0, 0, 0, 7, 1, date("Y")));
            $H2End = date("Y-m-d", mktime(0, 0, 0, 13, date("d") - date("d"), date("Y")));

            // YTD
            $YTDStart = date("Y-m-d", mktime(0, 0, 0, 1, 1, date("Y")));
            $YTDEnd = date("Y-m-d", mktime(0, 0, 0, date("m"), date("d"), date("Y")));
             
            $relativeDataRef = "";
            if(isset($filterRecord['datetime_relative_data_ref'])){
                $relativeDataRef = $filterRecord['datetime_relative_data_ref'];
            }
            $from = $filterRecord['datetime_range_from'];    
            $to = $filterRecord['datetime_range_to'];    
            
            if($relativeDataRef==self::DT_Ref_Key_FixedDateRange){
                if(!empty($from)){
                    $dbDefaultValue =  "$fieldcode  >= '$from'";
                    $defaultValue = "$fieldcode  >= '$from'";
                }
                if(!empty($to)){
                    if(!empty($dbDefaultValue)){
                        $dbDefaultValue .= " AND ";
                        $defaultValue .= " AND ";
                    }
                    $dbDefaultValue .=  "$fieldcode <= '$to'";
                    $defaultValue .= "$fieldcode <= '$to'";
                }                                
            } else if($relativeDataRef==self::DT_Ref_Key_Today){
                $dbDefaultValue =  "$fieldcode  >= '$BOToday' AND $fieldcode  <= '$EOToday'";
                $defaultValue = self::DT_Ref_Text_Today;                
            } else if($relativeDataRef==self::DT_Ref_Key_Tomorrow){
                $dbDefaultValue =  "$fieldcode  >= '$BOTomorrow' AND $fieldcode  <= '$EOTomorrow'";
                $defaultValue = self::DT_Ref_Text_Tomorrow;                
            } else if($relativeDataRef==self::DT_Ref_Key_TodayAndTomorrow){
                $dbDefaultValue =  "$fieldcode  >= '$BOToday' AND $fieldcode  <= '$EOTomorrow'";
                $defaultValue = self::DT_Ref_Text_TodayAndTomorrow;   
            } else if($relativeDataRef==self::DT_Ref_Key_TodayAndNextBusinessDay){
                $dbDefaultValue =  "$fieldcode  >= '$BOToday' AND $fieldcode  <= '$EONextBuDay'";
                $defaultValue = self::DT_Ref_Text_TodayAndNextBusinessDay;     
            } else if($relativeDataRef==self::DT_Ref_Key_NextBusinessDay){
                $dbDefaultValue =  "$fieldcode  >= '$BONextBuDay' AND $fieldcode  <= '$EONextBuDay'";
                $defaultValue = self::DT_Ref_Text_NextBusinessDay;                
            } else if($relativeDataRef==self::DT_Ref_Key_Next2days){
                $dbDefaultValue =  "$fieldcode  >= '$BOTomorrow' AND $fieldcode  <= '$EONext2days'";
                $defaultValue = self::DT_Ref_Text_Next2days;
            } else if($relativeDataRef==self::DT_Ref_Key_Next2Businessdays){
                if($Next2Budays_hasSaSu){
                    $dbDefaultValue =  "($fieldcode  >= '$BONextBuDay' AND $fieldcode  <= '$EO_Fri') OR ($fieldcode  >= '$BO_NextMonday' AND $fieldcode  <= '$EONext2Budays') ";
                } else {
                    $dbDefaultValue =  "$fieldcode  >= '$BONextBuDay' AND $fieldcode  <= '$EONext2Budays'";
                }
                $defaultValue = self::DT_Ref_Text_Next2Businessdays;
                
            } else if($relativeDataRef==self::DT_Ref_Key_Next3days){
                $dbDefaultValue =  "$fieldcode  >= '$BOTomorrow' AND $fieldcode  <= '$EONext3days'";
                $defaultValue = self::DT_Ref_Text_Next3days;
            } else if($relativeDataRef==self::DT_Ref_Key_Next3Businessdays){
                if($Next3Budays_hasSaSu){
                    $dbDefaultValue =  "($fieldcode  >= '$BONextBuDay' AND $fieldcode  <= '$EO_Fri') OR ($fieldcode  >= '$BO_NextMonday' AND $fieldcode  <= '$EONext3Budays') ";
                } else {
                    $dbDefaultValue =  "$fieldcode  >= '$BONextBuDay' AND $fieldcode  <= '$EONext3Budays'";
                }
                $defaultValue = self::DT_Ref_Text_Next3Businessdays;
                
            } else if($relativeDataRef==self::DT_Ref_Key_Next4days){
                $dbDefaultValue =  "$fieldcode  >= '$BOTomorrow' AND $fieldcode  <= '$EONext4days'";
                $defaultValue = self::DT_Ref_Text_Next4days;
            } else if($relativeDataRef==self::DT_Ref_Key_Next4BusinessDays){
                if($Next4Budays_hasSaSu){
                    $dbDefaultValue =  "($fieldcode  >= '$BONextBuDay' AND $fieldcode  <= '$EO_Fri') OR ($fieldcode  >= '$BO_NextMonday' AND $fieldcode  <= '$EONext4Budays') ";
                } else {
                    $dbDefaultValue =  "$fieldcode  >= '$BONextBuDay' AND $fieldcode  <= '$EONext4Budays'";
                }
                $defaultValue = self::DT_Ref_Text_Next4BusinessDays;
                
            } else if($relativeDataRef==self::DT_Ref_Key_Next5Days){
                $dbDefaultValue =  "$fieldcode  >= '$BOTomorrow' AND $fieldcode  <= '$EONext5days'";
                $defaultValue = self::DT_Ref_Text_Next5Days;
            } else if($relativeDataRef==self::DT_Ref_Key_Next5BusinessDays){
                if($Next5Budays_hasSaSu){
                    $dbDefaultValue =  "($fieldcode  >= '$BONextBuDay' AND $fieldcode  <= '$EO_Fri') OR ($fieldcode  >= '$BO_NextMonday' AND $fieldcode  <= '$EONext5Budays') ";
                } else {
                    $dbDefaultValue =  "$fieldcode  >= '$BONextBuDay' AND $fieldcode  <= '$EONext5Budays'";
                }
                $defaultValue = self::DT_Ref_Text_Next5BusinessDays;
                
            } else if($relativeDataRef==self::DT_Ref_Key_ThisWeek){
                $dbDefaultValue =  "$fieldcode  >= '$BOW' AND $fieldcode  <= '$EOW'";
                $defaultValue = self::DT_Ref_Text_ThisWeek;  
            } else if($relativeDataRef==self::DT_Ref_Key_ThisMonth){
                $dbDefaultValue =  "$fieldcode  >= '$BOM' AND $fieldcode  <= '$EOM'";
                $defaultValue = self::DT_Ref_Text_ThisMonth;                
            } else if($relativeDataRef==self::DT_Ref_Key_NextMonth){
                $dbDefaultValue =  "$fieldcode  >= '$BONextMonth' AND $fieldcode  <= '$EONextMonth'";
                $defaultValue = self::DT_Ref_Text_NextMonth;                
            } else if($relativeDataRef==self::DT_Ref_Key_Yesterday){
                $dbDefaultValue =  "$fieldcode  >= '$BOYesterday' AND $fieldcode  <= '$EOYesterday'";
                $defaultValue = self::DT_Ref_Text_Yesterday;                
            } else if($relativeDataRef==self::DT_Ref_Key_LastBusinessDay){
                $dbDefaultValue =  "$fieldcode  >= '$BOLastBuDay' AND $fieldcode  <= '$EOLastBuDay'";
                $defaultValue = self::DT_Ref_Text_LastBusinessDay;                
            } else if($relativeDataRef==self::DT_Ref_Key_Last2days){
                $dbDefaultValue =  "$fieldcode  >= '$BOLast2days' AND $fieldcode  <= '$EOYesterday'";
                $defaultValue = self::DT_Ref_Text_Last2days;                
            } else if($relativeDataRef==self::DT_Ref_Key_Last2Businessdays){
                if($Last2Budays_hasSaSu){
                    $dbDefaultValue =  "($fieldcode  >= '$BOLast2Budays' AND $fieldcode  <= '$EO_LastFri') OR ($fieldcode  >= '$BO_ThisMonday' AND $fieldcode  <= '$EOLastBuDay') ";
                } else {
                    $dbDefaultValue =  "$fieldcode  >= '$BOLast2Budays' AND $fieldcode  <= '$EOLastBuDay'";
                }
                $defaultValue = self::DT_Ref_Text_Last2Businessdays;
            } else if($relativeDataRef==self::DT_Ref_Key_Last3Businessdays){
                if($Last3Budays_hasSaSu){
                    $dbDefaultValue =  "($fieldcode  >= '$BOLast3Budays' AND $fieldcode  <= '$EO_LastFri') OR ($fieldcode  >= '$BO_ThisMonday' AND $fieldcode  <= '$EOLastBuDay') ";
                } else {
                    $dbDefaultValue =  "$fieldcode  >= '$BOLast3Budays' AND $fieldcode  <= '$EOLastBuDay'";
                }
                $defaultValue = self::DT_Ref_Text_Last3Businessdays;
            } else if($relativeDataRef==self::DT_Ref_Key_Last4Businessdays){
                if($Last4Budays_hasSaSu){
                    $dbDefaultValue =  "($fieldcode  >= '$BOLast4Budays' AND $fieldcode  <= '$EO_LastFri') OR ($fieldcode  >= '$BO_ThisMonday' AND $fieldcode  <= '$EOLastBuDay') ";
                } else {
                    $dbDefaultValue =  "$fieldcode  >= '$BOLast4Budays' AND $fieldcode  <= '$EOLastBuDay'";
                }
                $defaultValue = self::DT_Ref_Text_Last4Businessdays;
            } else if($relativeDataRef==self::DT_Ref_Key_Last5Businessdays){
                if($Last5Budays_hasSaSu){
                    $dbDefaultValue =  "($fieldcode  >= '$BOLast5Budays' AND $fieldcode  <= '$EO_LastFri') OR ($fieldcode  >= '$BO_ThisMonday' AND $fieldcode  <= '$EOLastBuDay') ";
                } else {
                    $dbDefaultValue =  "$fieldcode  >= '$BOLast5Budays' AND $fieldcode  <= '$EOLastBuDay'";
                }
                $defaultValue = self::DT_Ref_Text_Last5Businessdays;

            } else if($relativeDataRef==self::DT_Ref_Key_Last3days){
                $dbDefaultValue =  "$fieldcode  >= '$BOLast3days' AND $fieldcode  <= '$EOYesterday'";
                $defaultValue = self::DT_Ref_Text_Last3days;                
            } else if($relativeDataRef==self::DT_Ref_Key_Last4days){
                $dbDefaultValue =  "$fieldcode  >= '$BOLast4days' AND $fieldcode  <= '$EOYesterday'";
                $defaultValue = self::DT_Ref_Text_Last4days;                
            } else if($relativeDataRef==self::DT_Ref_Key_Last5days){
                $dbDefaultValue =  "$fieldcode  >= '$BOLast5days' AND $fieldcode  <= '$EOYesterday'";
                $defaultValue = self::DT_Ref_Text_Last5days;                
            } else if($relativeDataRef==self::DT_Ref_Key_LastWeek){
                $dbDefaultValue =  "$fieldcode  >= '$BOLW' AND $fieldcode  <= '$EOLW'";
                $defaultValue = self::DT_Ref_Text_LastWeek;                 
            } else if($relativeDataRef==self::DT_Ref_Key_LastMonth){
                $dbDefaultValue =  "$fieldcode  >= '$PrevBOM' AND $fieldcode  <= '$PrevEOM'";
                $defaultValue = self::DT_Ref_Text_LastMonth;                
            } else if($relativeDataRef==self::DT_Ref_Key_Q1){
                $dbDefaultValue =  "$fieldcode  >= '$Q1Start' AND $fieldcode  <= '$Q1End'";
                $defaultValue = self::DT_Ref_Text_Q1;                
            } else if($relativeDataRef==self::DT_Ref_Key_Q2){
                $dbDefaultValue =  "$fieldcode  >= '$Q2Start' AND $fieldcode  <= '$Q2End'";
                $defaultValue = self::DT_Ref_Text_Q2;                
            } else if($relativeDataRef==self::DT_Ref_Key_Q3){
                $dbDefaultValue =  "$fieldcode  >= '$Q3Start' AND $fieldcode  <= '$Q3End'";
                $defaultValue = self::DT_Ref_Text_Q3;                
            } else if($relativeDataRef==self::DT_Ref_Key_Q4){
                $dbDefaultValue =  "$fieldcode  >= '$Q4Start' AND $fieldcode  <= '$Q4End'";
                $defaultValue = self::DT_Ref_Text_Q4;                
            } else if($relativeDataRef==self::DT_Ref_Key_H1){
                $dbDefaultValue =  "$fieldcode  >= '$H1Start' AND $fieldcode  <= '$H1End'";
                $defaultValue = self::DT_Ref_Text_H1;                
            } else if($relativeDataRef==self::DT_Ref_Key_H2){
                $dbDefaultValue =  "$fieldcode  >= '$H2Start' AND $fieldcode  <= '$H2End'";
                $defaultValue = self::DT_Ref_Text_H2;                
            } else if($relativeDataRef==self::DT_Ref_Key_YTD){
                $dbDefaultValue =  "$fieldcode  >= '$YTDStart' AND $fieldcode  <= '$YTDEnd'";
                $defaultValue = self::DT_Ref_Text_YTD;                
            } else {
                if(!empty($from)){
                    $dbDefaultValue =  "$fieldcode  >= '$from'";
                    $defaultValue = "$fieldcode  >= '$from'";
                }
                if(!empty($to)){
                    if(!empty($dbDefaultValue)){
                        $dbDefaultValue .= " AND ";
                        $defaultValue .= " AND ";
                    }
                    $dbDefaultValue .=  "$fieldcode <= '$to'";
                    $defaultValue .=  "$fieldcode <= '$to'";
                } 
            }            
         } 
         else if($dataType==self::DATATYPE_STRING)
         {
             //string_matching, string_value
             $string_value = $filterRecord['string_value'];             
             if(!empty($string_value)){
                 if($string_matching==self::String_Matching_ExactMatch){
                     $dbDefaultValue = "$fieldcode = '$string_value'";
                     $defaultValue = "= '$string_value'";                     
                 } else if($string_matching==self::String_Matching_BeginsWith){
                     $dbDefaultValue = "$fieldcode LIKE '$string_value%'";
                     $defaultValue = "BEGINS_WITH '$string_value'";
                 } else if($string_matching==self::String_Matching_Contains){
                     $dbDefaultValue = "$fieldcode LIKE '%$string_value%'";
                     $defaultValue = "CONTAINS '$string_value'";
                 } else if($string_matching==self::String_Matching_EndsWith){
                     $dbDefaultValue = "$fieldcode LIKE '%$string_value'";
                     $defaultValue = "ENDS_WITH '$string_value'";
                 } else if($string_matching==self::String_Matching_IsNot){
                     $dbDefaultValue = "$fieldcode <> '$string_value'";
                     $defaultValue = "IS_NOT '$string_value'";
                 }                  
             }
             
  
             
             
         } 
         else if($dataType==self::DATATYPE_BOOLEAN)
         {
             //bool_value = 
             $bool_value = $filterRecord['bool_value'];
             if($bool_value==self::Bool_Value_True){
                 $dbDefaultValue = "$fieldcode = 1";
                 $defaultValue = "True";
             } else if($bool_value==self::Bool_Value_False){
                 $dbDefaultValue = "$fieldcode = 0";
                 $defaultValue = "False";
             } else if($bool_value==self::Bool_Value_Any){
                 $dbDefaultValue = "";
                 $defaultValue = "Any";
             }
         } 
         else if($dataType==self::DATATYPE_NUMERIC)
         {
             //$numeric_base_value = $filterRecord['numeric_base_value'];
             $numeric_range_from = $filterRecord['numeric_range_from'];
             $numeric_range_to = $filterRecord['numeric_range_to'];
             if($dbDataType=='int'){
                 //$numeric_base_value = round($numeric_base_value,0);
                 $numeric_range_from = round($numeric_range_from,0);
                 $numeric_range_to = round($numeric_range_to,0);
             }
             
             if(!empty($numeric_range_from)) {                 
                 if(!empty($numeric_range_to)){
                     $dbDefaultValue = "$fieldcode >= '$numeric_range_from' AND $fieldcode <= '$numeric_range_to'";
                     $defaultValue = ">= $numeric_range_from AND <= $numeric_range_to";
                 } else {
                     $dbDefaultValue = "$fieldcode >= '$numeric_range_from'";
                     $defaultValue = ">= $numeric_range_from";
                 }
             } else if(!empty($numeric_range_to)){
                 if(!empty($numeric_base_value)){
                     //$numeric_range_to += $numeric_base_value;
                 }                     
                 
                 $dbDefaultValue = "$fieldcode <= '$numeric_range_to'";
                 $defaultValue = "<= $numeric_range_to";
             } else {
                 $dbDefaultValue = "";
                 $defaultValue = "";
             }
         }
         
         $ret = array('DBDefaultValue'=>$dbDefaultValue,'DefaultValue'=>$defaultValue);
         //echo("<br/>ret: ");print_r($ret);
         //die();
         return $ret;
    }
     
        
    public function getDay($Day) 
    {
        if($Day >=1 && $Day < 7){
            return ($Day + 1);
        } else if($Day==7){
            return 0;
        } else {
            return $Day;
        }
    }
    /*
    *   $sortFields - keys: sort_1, sort_dir_1, sort_2, sort_dir_2, sort_3, sort_dir_3     
    */
    public function saveSortInfo2($reportID,$sortFields)
    {
        if(empty($sortFields) && !is_array($sortFields) && count($sortFields)==0){
            return;
        }
        
        $availSortKeys = array('sort_1','sort_dir_1','sort_2','sort_dir_2','sort_3', 'sort_dir_3');
        $updFields = array();
        foreach($sortFields as $k=>$v){
            if(in_array($k,$availSortKeys)){
                $updFields[$k]=$v;
            }
        }
        
        if(count($updFields)>0){
            $criteria = "id = '$reportID'";
            $result = Core_Database::update("custom_reports",$updFields,$criteria);
        }
        
        return $result;
    }
    
    public function getSortInfo2($reportID)
    {
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from(array('cr'=>"custom_reports"),
            array('id','sort_1','sort_dir_1','sort_2','sort_dir_2','sort_3','sort_dir_3')
        );
        $select->where("id = '$reportID'");
        $records = Core_Database::fetchAll($select); 
        if(empty($records) || count($records)==0){
            return null;  
        } else {
            return $records[0];   
        }
    }

    public function getArrangeColumns2($reportID)
    {
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from(array('crf'=>"custom_reports_fields"),
            array('rep_id','fieldname','display_order')
        );
        $select->where("rep_id = '$reportID'");
        $select->order('display_order');
        $records = Core_Database::fetchAll($select); 
        if(empty($records) || count($records)==0){
            return null;  
        } else {
            return $records[0];   
        }
    }
    
    
    public function addFilter2ForCreatedReport($reportID,$fieldCode)
    {
        if(empty($reportID)){
            return -2;
        }
                      
        $filterAt = '3';
        
        //--- insert to custom_reports_filters table 
        $avaiKeyArray = array('fieldcode','filter_at','datetime_relative_data_ref','datetime_range_from','datetime_range_to','string_matching','string_value','bool_value','numeric_base_value','numeric_range_from','numeric_range_to','datetime_range_int_from','datetime_range_int_to','time_from','time_to');
               
        $addFields = array();
        $addFields['rep_id'] = $reportID;
        $addFields['filter_at'] = $filterAt;  
        $table ="work_orders";
        $fieldInfos = $this->getFieldInfoHasFilter($table,$fieldCode);
        //echo("<pre>cReport->$fieldInfos: ");print_r($fieldInfos);echo("</pre>");//test
        if(!empty($fieldInfos)&& count($fieldInfos)>0)
        {    
           $addFields['fieldcode'] = $fieldCode;
           $addFields['string_matching'] = "SelectMultiple";
           $filterType = $fieldInfos["FilterType"];
                if ($filterType=='S')
                {
                    $addFields['string_matching']='SelectMultiple';
                }else if($filterType=='T')
                {
                    $addFields['string_matching']='ExactMatch';
                }else if($filterType=='DT')
                {
                    $addFields['string_matching']='';
                    $addFields['datetime_range_from'] = date("Y-m-d H:i:s");
                    $addFields['datetime_range_to'] = date("Y-m-d H:i:s");
                }else
                {
                    $addFields['string_matching']='';
                }  
           $result = Core_Database::insert("custom_reports_filters",$addFields); 
          
        }        
       
        return 0;
    }
    public function getFieldInfoHasFilter($table,$fieldCode)
    {

        $field_Code = strtolower($fieldCode); 
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from('custom_reports_tablefields');
        $select->where('table_code = ?',$table);
        $select->where('LOWER(field_code) = ?',$field_Code);
	$select->where('FilterLocation IS NOT NULL');
        $records = Core_Database::fetchAll($select);          
        if(empty($records) || count($records)==0){
            return null;
        } else {
            return $records[0];
        }                 
    }
    
            
    public function getCurrentQuarter()
    {
        $year = date("Y");
        $month = date("m");
        $fromDate = "";
        $toDate = "";
        if($month >=1 && $month <=3){
            $fromDate = $year.'-01-01';
            $toDate = $year.'-03-31';
        } else if($month >=4 && $month <=6){
            $fromDate = $year.'-04-01';
            $toDate = $year.'-06-30';
        } else if($month >=7 && $month <=9){
            $fromDate = $year.'-07-01';
            $toDate = $year.'-09-30';
        } else { //$month >=10 && $month <=9
            $fromDate = $year.'-10-01';
            $toDate = $year.'-12-31';            
        }
        $ret = array('RangeBegin'=>$fromDate,'RangeEnd'=>$toDate);
        return $ret;        
    }
    
    public function getCurrentYear()
    {
        $year = date("Y");
        $fromDate = $year.'-01-01';;
        $toDate = $year.'-12-31';        
        
        $ret = array('RangeBegin'=>$fromDate,'RangeEnd'=>$toDate);
        return $ret;        
    }

    public function getLastQuarter()
    {
        $year = date("Y");
        $month = date("m");
        $fromDate = "";
        $toDate = "";
        if($month >=1 && $month <=3){
            $year_1 = $year - 1;
            $fromDate = $year_1.'-10-01';
            $toDate = $year_1.'-12-31';
        } else if($month >=4 && $month <=6){
            $fromDate = $year.'-01-01';
            $toDate = $year.'-03-31';
        } else if($month >=7 && $month <=9){
            $fromDate = $year.'-04-01';
            $toDate = $year.'-06-30';
        } else { //$month >=10 && $month <=9
            $fromDate = $year.'-07-01';
            $toDate = $year.'-09-30';            
        }
        $ret = array('RangeBegin'=>$fromDate,'RangeEnd'=>$toDate);
        return $ret;        
    }

    public function getLastYear()
    {
        $year = date("Y");
        $year_1 = $year-1;
        $fromDate = $year_1.'-01-01';
        $toDate = $year_1.'-12-31';        
        
        $ret = array('RangeBegin'=>$fromDate,'RangeEnd'=>$toDate);
        return $ret;        
    }
    //RunTimeDate
    public function getRunTimeDate($intFrom,$intTo)
    {
        $frTimestamp = mktime(0,0,1,date('m'),date('d')+$intFrom,date('Y'));
        $toTimestamp = mktime(23,59,59,date('m'),date('d')+$intTo,date('Y'));
        
        $from = date("Y-m-d H:i:s",$frTimestamp);
        $to = date("Y-m-d H:i:s",$toTimestamp);
               
        $ret = array('RangeBegin'=>$from,'RangeEnd'=>$to);
        return $ret;        
    }
        

 
}
