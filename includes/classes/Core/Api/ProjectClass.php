<?php
/**
 * Core_Api_ProjectClass
 *
 * @author Alex Scherba
 */
class Core_Api_ProjectClass
{
    const UPLOAD_FILE_ACTION_CREATE = 'create';
    const UPLOAD_FILE_ACTION_UPDATE = 'update';
    const SITE_STATUS_ID_SCHEDULED = 1;
    const SITE_STATUS_ID_COMPLETE = 2;
    const SITE_STATUS_ID_CANCELED = 3;
    const SITE_STATUS_ID_RESCHEDULED = 4;


	public function __construct() {
		Core_Caspio::init();
	}

    /**
     * uploadFileAWS - upload project file to S3
     *
     * @param string $user
     * @param string $pass
     * @param string $prjId
     * @param string $fName
     * @param string $fData
     * @param string $descr
     * @param string $action
     * @param string $saveToField
     * @access public
     * @return API_Response
     * @author Alex Scherba
     */
    public function uploadFile( $user, $pass, $prjId, $fName, $fData, $descr, $action, $saveToField = '' )
    {
        $error = Core_Api_Error::getInstance();
        $fName = ltrim(trim($fName), '/');
        
        /* Log request */
       	$logger = Core_Api_Logger::getInstance();
		$logger->setUrl('Upload file '.$fName.' for Project '.$prjId);
        $logger->setLogin($user);
        $logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');
        $file = new Core_File();
        if ($saveToField == 'Project_Logo') {
            $uploadStatus = $file->uploadFile($fName, $fData, S3_PROJECTS_LOGO_DIR, true);
            if ( !$uploadStatus) {
                $error->addError(6, 'Upload file '.$fName.' failed');
                return API_Response::fail();
            }
            if ($action === self::UPLOAD_FILE_ACTION_UPDATE){ // if upload field when update Prj
                $prj = $this->getProjects($user, $pass, $prjId); //current Prj
                if ( $error->getErrors() ) return API_Response::fail();
                $prj = $prj->data[0];
                if ( !empty($prj->$saveToField) ) { // if it was uploaded file
                        $file->removeFile($prj->$saveToField, S3_PROJECTS_LOGO_DIR);// Remove old file
                }
            }
            $prjUp = new API_Project();
            $prjUp->$saveToField = $fName;

            return $this->updateProject($user, $pass, $prjId, $prjUp);
        }
        elseif ($saveToField == "CustomSignOff") {
        	if (stripos ($fName, ".pdf") !== false) {
	        	$uploadStatus = $file->uploadFile($fName, $fData, S3_PROJECTS_DOCS_DIR, false);
	            if(!$uploadStatus){
	                $error->addError(6, 'Upload signoff sheet '.$fName.' failed');
	                return API_Response::fail();
	            }

	            $prjUp = new API_Project();
	            $prjUp->$saveToField = "projects-docs/" . $fName;

	            return $this->updateProject($user, $pass, $prjId, $prjUp);
        	}
        	else {
        		return API_Response::fail();
        	}
        }else{
            $uploadStatus = $file->uploadFile($fName, $fData, S3_PROJECTS_DOCS_DIR, false);
            if(!$uploadStatus){
                $error->addError(6, 'Upload file '.$fName.' failed');
                return API_Response::fail();
            }

            $files = new Core_ProjectsFiles();
            $lastId = $files->insert(array(
                'Project_ID' => $prjId,
                'path'       => $fName,
                'description'=> mysql_escape_string($descr)
            ));

            if($lastId) $res = new API_Response (1, 0, $lastId);
            else $res = API_Response::fail();
            return $res;
        }
    }

	/**
	 * download project file from S3
	 *
	 * @param string $user
	 * @param string $pass
	 * @param string $fileName
	 * @return API_Response
     * @author Alex Scherba
	 */
    public function getFile($fileName)
    {
        $errors = Core_Api_Error::getInstance();
        // get file from S3
        $file = new Core_File();
        $file = $file->downloadFile($fileName, S3_PROJECTS_DOCS_DIR);
        if($file === false) $errors->addError(6, 'File not exists');

		$err = $errors->getErrors();
		if (!empty($err))
			return API_Response::fail();

        return $file;
    }

    /**
	 * create Project
	 *
	 * @param string $user
	 * @param string $pass
	 * @param API_Project $project
	 * @return API_Response
	 */
	public function createProject($user, $pass, $project)
	{
        $errors = Core_Api_Error::getInstance();
        
        $logger = Core_Api_Logger::getInstance();
		$logger->setUrl('createProject');
        $logger->setLogin($user);
		$logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');

		$authData = array('login'=>$user, 'password'=>$pass);
        $userClass = new Core_Api_User();
		$userClass->checkAuthentication($authData);
        if (!$userClass->isAuthenticate())
			return API_Response::fail();

		if (is_string($project)) $woXML = $project;
		else {
			$dom = new DOMDocument("1.0");
			$dom->appendChild($project->toXMLElementTect($dom));
			$woXML = $dom->saveXML();
		}
        if (empty($woXML))
			$errors->addError(2, 'XML parameter is empty but required');

		$err = $errors->getErrors();
		if (!empty($err))
			return API_Response::fail();

        $prjData = $this->getDataFromProjectXML($woXML);

        $fieldListType = Core_Database::getFieldListType(Core_Database::TABLE_PROJECTS);
        $prjData = Core_Database::convertQueryResult($prjData, $fieldListType);

		if (!$userClass->isPM()) {
			unset($prjData["PcntDeduct"]);
			if (Core_TechDeduct::isEnabledForCompanyID($userClass->getContextCompanyId())) {
				$currentDeduct = $userClass->GetDeductPercent_FSSFeeFrTech($userClass->getContextCompanyId());
				$prjData["PcntDeduct"] = 1;
				$prjData["PcntDeductPercent"] = $currentDeduct;
			}
		}

		if (empty($prjData["PcntDeductPercent"])) {
			// clear PcntDeduct if percentage is zero
			$prjData["PcntDeduct"] = 0;
			$prjData["PcntDeductPercent"] = 0;
		}

		$prjData["AutoBlastOnPublish"] = 1;

		$factory = new Core_Validation_Factory(Core_Validation_Factory::VALIDATION_TYPE_PROJECT_CREATE);
	    $validation = $factory->getValidationObject($prjData);

        if ($validation->validate() === false) {
            $errors->addErrors('6', $validation->getErrors());
			return API_Response::fail();
        }

        $fieldsList = implode(',',array_keys($prjData));

        $db = Core_Database::getInstance();
		
		$clientCredential = "";
		if (isset($prjData['ClientCredential'])) {
			$clientCredential = $prjData['ClientCredential'];
			unset($prjData['ClientCredential']);
		}
		$result = false;
		$result = Core_Database::insert(Core_Database::TABLE_PROJECTS, $prjData);
		$prj_id = $db->lastInsertId(Core_Database::TABLE_PROJECTS);
        if (!$result) {
    		$err = $errors->getErrors();
    		if (!empty($err))
				return API_Response::fail();

		    $errors->addError(7, 'Incorrect SQL request');
			return API_Response::fail();
		}
		
		if (!empty($prjData['ClientCredential'])) {
			// save client credential
			foreach ($prjData['ClientCredential'] as $cid) {
				$pc = new Core_ProjectCertification($prj_id, $cid);
				$pc->save();
			}
		}

        Core_TimeStamp::createProjectTimeStamp($prj_id, $user, $pass);

		$mail = new Core_EmailForNewProject();
		$mail->setProjectID($prj_id);
		$mail->setProjectName($prjData["Project_Name"]);
		$mail->setUser($userClass);
		$mail->sendMail();

		return API_Response::success(array(new API_Project($prj_id)));
	}
 

	/**
	 * Function for getting Projects for current usre
	 *
	 * @param string $user
	 * @param string $pass
	 * @param array $project_ids
	 * @return API_Response
	 */
	public function getProjects($user, $pass, $project_ids, $companyId = null, $owner = null)
	{
       	$logger = Core_Api_Logger::getInstance();
		$logger->setUrl('getProjects');
        $logger->setLogin($user);
		$logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');
        $errors = Core_Api_Error::getInstance();
		
		if (!is_array($project_ids)) $project_ids = array($project_ids);

		$db = Core_Database::getInstance();
		$select = $db->select()->from(Core_Database::TABLE_PROJECTS);

		if (!empty($companyId)) {
    		$select->where('Project_Company_ID=?', $companyId);
		}
		if (count($project_ids)) $select->where('Project_ID IN (?)', $project_ids);
		$result = Core_Database::fetchAll($select);
		$err = $errors->getErrors();
		if (!empty($err))
			return API_Response::fail();
			
		$pcs = new Core_ProjectCertifications();
		$certs = $pcs->getProjectCertification($project_ids);
		if (!empty($owner)) {
			$future = new Core_FutureWorkOrderInfo;
			$row = $future->find($owner)->toArray();
		}
		else
			$row = null;
		if (!empty($row[0]) || !empty($certs)) {
			foreach ($result as $k=>$p) {
				if (!empty($owner) && !empty($result[$k]["AllowCustomCommSettings"])) {
					// combine owner info if project allows custom comm settings //14052
					$futureToProjectMap = array(
						'ProjectManagerName' => 'Project_Manager',
						'ProjectManagerEmail' => 'Project_Manager_Email',
						'ProjectManagerPhone' => 'Project_Manager_Phone',
						'ResourceCoordinatorName' => 'Resource_Coordinator',
						'ResourceCoordinatorEmail' => 'Resource_Coord_Email',
						'ResourceCoordinatorPhone' => 'Resource_Coord_Phone',
						'SystemGeneratedEmailTo' => 'To_Email',
						'WorkAvailableEmailFrom' => 'RecruitmentFromEmail',
						'BidNotificationEmailTo' => 'BidNotificationEmails',
						'P2TNotificationEmailTo' => 'P2TNotificationEmail',
						'WorkAssignedEmailTo' => 'AssignmentNotificationEmail',
						'WorkDoneEmailTo' => 'WorkDoneNotificationEmails',
                                                'ACSNotifiPOC'  =>  'ACSNotifiPOC',
                                                'ACSNotifiPOC_Phone' => 'ACSNotifiPOC_Phone',
                                                'ACSNotifiPOC_Email' => 'ACSNotifiPOC_Email',
                                                'EmergencyName' => 'EmergencyName',
                                                'EmergencyPhone'=> 'EmergencyPhone',
                                                'EmergencyEmail' => 'EmergencyEmail',
                                                'TechnicalSupportName' =>'TechnicalSupportName',
                                                'TechnicalSupportPhone' => 'TechnicalSupportPhone',
                                                'TechnicalSupportEmail' => 'TechnicalSupportEmail',
                                                'CheckInOutName' => 'CheckInOutName',
                                                'CheckInOutNumber' => 'CheckInOutNumber',
                                                'CheckInOutEmail' => 'CheckInOutEmail'
					);
					foreach ($row[0] as $field => $value) {
						if (array_key_exists($field, $futureToProjectMap)) $field = $futureToProjectMap[$field];
						if (!array_key_exists($field, $result[$k]) || empty($value)) continue;
							$result[$k][$field] = $value;
						}
				}
				if (!array_key_exists($p['Project_ID'], $certs)) continue;
				$result[$k]['ClientCredential'] = $certs[$p['Project_ID']];
			}
		}

		return API_Response::success($this->makeProjectsArray($result));
	}

	/**
	 * Function for getting all Projects for current usre
	 *
	 * @param string $user
	 * @param string $pass
	 * @param string $createdByUser - filter by created by user (optional)
	 * @return API_Response
	 */
	public function getAllProjects($user, $pass, $createdByUser = NULL, $filter = NULL)
	{
       	$logger = Core_Api_Logger::getInstance();
		$logger->setUrl('getAllProjects');
        $logger->setLogin($user);
		$logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');

		$authData = array('login'=>$user, 'password'=>$pass);
		$errors = Core_Api_Error::getInstance();

		$user = new Core_Api_User();
		$user->checkAuthentication($authData);

		if (!$user->isAuthenticate())
			return API_Response::fail();

		$fieldsList = array('Project_ID','Project_Name','Active','CreatedBy','Project_Company_ID','Owner');
		$companyId  = $user->getContextCompanyId();
        if(!empty($filter['Company_ID'])) $companyId = $filter['Company_ID'];

		$db = Core_Database::getInstance();
		$select = $db->select();

		$select->from(Core_Database::TABLE_PROJECTS, $fieldsList);
			
		if (!$user->isPM() && isset($filter['ProjectUserAllowedCreateWO']) && $filter['ProjectUserAllowedCreateWO'] == true) {
			$fieldsList[0] = 'p.Project_ID';
			$allowProjects = NULL;
			if (isset($filter['ProjectUserAllowedCreateWOAllowList']) && is_array($filter['ProjectUserAllowedCreateWOAllowList'])) {
				// pass a list of projects to always allow. Used to default current selected project to visible
				$allowProjects = $filter['ProjectUserAllowedCreateWOAllowList'];
			}
			$select = Core_ProjectUsersAllowedCreateWO::projectSelectAllowedForUser($user->getLogin(), $companyId, $fieldsList, $allowProjects);
		}
		
		if (!$user->isPM() || !isset($filter['ignoreCompany']) || $filter['ignoreCompany'] != true) {
			$select->where("Project_Company_ID = ?", $companyId);
		}
		else {
			$select->order("Project_Company_ID");
			$fieldsList[] = "Project_Company_ID";
		}
		
		$select->order("Project_Name");

		if (!empty($createdByUser)) $select->where("CreatedBy = ?", $createdByUser);

		$result = Core_Database::fetchAll($select);

		$err = $errors->getErrors();
		if (!empty($err))
			return API_Response::fail();

		return API_Response::success($this->makeProjectsArray($result));
	}
	

    public function updateProject($user, $pass, $prjId, $project, $ignoreWarnings = false)
    {
        $errors = Core_Api_Error::getInstance();

        if (is_string($project)) {
            $prjXML = $project;
        } else {
            
			$dom = new DOMDocument("1.0");
			$dom->appendChild($project->toXMLElementTect($dom));
			$prjXML = $dom->saveXML();
		}

       	/* Log request */
       	$logger = Core_Api_Logger::getInstance();
		$logger->setUrl('Update Project '.$prjId);
        $logger->setLogin($user);



	$authData = array('login'=>$user, 'password'=>$pass);
        $userClass = new Core_Api_User();
	$userClass->checkAuthentication($authData);
        if (!$userClass->isAuthenticate())
		return API_Response::fail();

        //$logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');

        $params = array();
		$params['Project_ID'] = $prjId;
		$params['xml'] = $prjXML;

		if (empty($params['Project_ID']))
			$errors->addError(2, 'Project_ID parameter is empty but required');

		if (empty($params['xml']))
			$errors->addError(2, 'XML parameter is empty but required');

		$err = $errors->getErrors();
		if (!empty($err))
			return API_Response::fail();

		$prjData = $this->getDataFromProjectXML($params['xml']);


		$db = Core_Database::getInstance();
		$fieldListMap = Core_Database::getFieldList(Core_Database::TABLE_PROJECTS);
		$fieldListMap = array_combine($fieldListMap, $fieldListMap);
		$select = $db->select();
		$select->from(Core_Database::TABLE_PROJECTS, $fieldListMap)
			->where("Project_ID = ?", $prjId);

		$curPrj = Core_Database::fetchAll($select);

        if (empty($curPrj[0])) {
            $err = $errors->getErrors();
    		if (!empty($err))
				return API_Response::fail();

        	$errors->addError(7, 'Project '.$prjId.' does not exist');
			return API_Response::fail();
        }
        $curPrj = $curPrj[0];
		
		$fieldListType = Core_Database::getFieldListType(Core_Database::TABLE_PROJECTS);
		$prjData = Core_Database::convertQueryResult($prjData, $fieldListType);
		$curPrj = Core_Database::convertQueryResult($curPrj, $fieldListType);

        $updatedFields = array();
        foreach ($curPrj as $key=>$val) {
            if (isset($prjData[$key]) && $prjData[$key]!==null) {
                $updatedFields[$key] = $prjData[$key];
            } else {
                $updatedFields[$key] = $val;
            }
        }

		if (!$userClass->isPM()) {
         	$updatedFields["PcntDeduct"] = $curPrj["PcntDeduct"];
         	$updatedFields["PcntDeductPercent"] = $curPrj["PcntDeductPercent"];
			if ($updatedFields["Owner"] != $userClass->getLogin())
				// ignore custom comm if not owner and not GPM
				unset($updatedFields["AllowCustomCommSettings"]);
		}

		$updatedFields["AutoBlastOnPublish"] = $curPrj["AutoBlastOnPublish"];

		$factory = new Core_Validation_Factory(Core_Validation_Factory::VALIDATION_TYPE_PROJECT_UPDATE);
	    $validation = $factory->getValidationObject($updatedFields);

	    if ($validation->validate() === false) {
            $errors->addErrors('6', $validation->getErrors());
			return API_Response::fail();
        }
		
		$clientCredential = "";
		if (isset($updatedFields['ClientCredential'])) {
			$clientCredential = $updatedFields['ClientCredential'];
			unset($updatedFields['ClientCredential']);
		}	

		$updatedChangedFields = array();
		$updatedFieldsConverted = Core_Database::convertQueryResult($updatedFields, $fieldListType);
		foreach ($updatedFieldsConverted as $k => $v) {
			if (array_key_exists($k, $curPrj ) && $curPrj[$k] === $v) continue;
			$updatedChangedFields[$k] = $updatedFields[$k];
		}
        $result = false;
		$result = Core_Database::update(Core_Database::TABLE_PROJECTS, $updatedChangedFields, $db->quoteInto("Project_ID = ?", $prjId));
		
		if (!empty($prjData['ClientCredential'])) {
			// get certifications for project
			$pcs = new Core_ProjectCertifications();
			$certs = $pcs->getProjectCertification($prjId);
			
			if (!empty($certs[$prjId])) {
				// delete certifications removed by update
				$certs[$prjId] = array_values($certs[$prjId]);
				$diff = array_diff($certs[$prjId], $prjData['ClientCredential']);
				foreach ($diff as $cid) {
					$pc = new Core_ProjectCertification($prjId, $cid);
					$pc->delete();
				}
			}
			// save client credential
			foreach ($prjData['ClientCredential'] as $cid) {
				$pc = new Core_ProjectCertification($prjId, $cid);
				$pc->save();
			}
		}
		
        Core_TimeStamp::createProjectTimeStamp($prjId, $user, $pass);

        return API_Response::success(array(new API_Project($prjId)));
    }

    private function makeProjectsArray($result)
	{
		$wo = array();
	    if (!empty($result[0])) {
			foreach ($result as $key=>$value)
				$wo[] = API_Project::projectWithResult($value);
	    }
		return $wo;
	}


   	private function makeProjectsXML($result)
	{
		$dom = new DOMDocument("1.0");
		$root = $dom->createElement('Projects');
	    if (!empty($result[0])) {
		    foreach ($result as $key=>$value) {
                $pj = $dom->createElement('Project');

                $id = $dom->createAttribute('ID');
                $name = $dom->createAttribute('NAME');

                $id->appendChild($dom->createTextNode($value['Project_ID']));
                $name->appendChild($dom->createTextNode($value[' Project_Name']));

                $pj->appendChild($id);
                $pj->appendChild($name);

                $root->appendChild($pj);
            }
	    }
	    $dom->appendChild($root);
		return $dom->saveXML();
	}

    private function getDataFromProjectXML($xml)
	{
	    $result = array();
		$dom = new DOMDocument("1.0");
		$dom->loadXML($xml);
		$nodes = $dom->getElementsByTagName('Project')->item(0)->childNodes;

		foreach ($nodes as $node)
		{
			$result[$node->nodeName] = $node->nodeValue;
		}

		if (isset($result['ClientCredential']) && is_string($result['ClientCredential'])) $result['ClientCredential'] = explode(",", $result['ClientCredential']);

		return $result;
	}
    public function getProjectById($projectId)
    {
        $db = Core_Database::getInstance();
        $select = $db->select();
        $select->from(Core_Database::TABLE_PROJECTS);
        $select->where("Project_ID = '$projectId'");
        $records = Core_Database::fetchAll($select);
        if(!empty($records)) return $records[0];
        else return null;
    }
    

    public function getProjectByWin($winNum)
    {
        $db = Core_Database::getInstance();
        $select = $db->select();
        $select->from(Core_Database::TABLE_WORK_ORDERS, array("Project_ID"));
        $select->where("WIN_NUM = ?", $winNum);

        $records = Core_Database::fetchAll($select);

        if (empty($records)) return null;
        else return $this->getProject($records[0]['Project_ID']);
    }
    public function getProject($projectId)
    {
        $db = Core_Database::getInstance();
        $select = $db->select();
        $select->from(Core_Database::TABLE_PROJECTS);
        $select->where("Project_ID = ?", $projectId);

        $records = Core_Database::fetchAll($select);

        if (empty($records)) return null;
        else return $records[0];
    }
    
    public function isRequiredTechDoc($win)
    {
        $db = Core_Database::getInstance();
        $select = $db->select();
        $select->from(array('w'=>Core_Database::TABLE_WORK_ORDERS),
                       array('WIN_NUM','Project_Id') 
        );
        $select->join(array('p'=>Core_Database::TABLE_PROJECTS),
                        'p.Project_ID = w.Project_ID',
                        array('SignOffSheet_Required')
        );
        $select->where("w.WIN_NUM = '$win'");
        $select->where("p.SignOffSheet_Required = 1");
        $result = Core_Database::fetchAll($select);
        
        if(!empty($result)) return true;
        else return false;
    }    

    public function enableExpenseReporting($projectId,$companyId='')
    {        
        if(empty($projectId))
        {
            //if(empty($companyId))
            //{
                return 0;
            //}
            //else
            //{
            //    $userApi= new Core_Api_User();
            //    return $userApi->enableExpenseReporting($companyId);
            //}
        }
        else
        {
            $current = $this->getProjectAdditionalFields($projectId);
            if(empty($current))
            {
            	return 0; 
                //$projectInfo = $this->getProject($projectId);
                //$companyId = $projectInfo['Project_Company_ID'];
                //--- get data from client
                //$userApi = new Core_Api_User();
                //$enable = $userApi->enableExpenseReporting($companyId);
                //return $enable;
            }
            else return $current['enable_expense_reporting'];
        }
    }

    
    // end 13376

    public function updateUnassignedWOs_BillableBcats_ByProject($projectID)
    {
        //--- get WO Ids
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from(Core_Database::TABLE_WORK_ORDERS,array('WIN_NUM'));
        $select->where("Project_ID = '$projectID'"); 
        $select->where("Deactivated = 0"); 
        $select->where("TechMarkedComplete = 0"); 
        $select->where("Approved = 0");     
        $select->where("(Tech_ID IS NULL OR Tech_ID = '')");      
        $select->where("(Status = 'created' OR Status = 'published')");      
                   
        $records = Core_Database::fetchAll($select);  
        if(empty($records)) return;
        
        
        
        $woIDs = '';
        foreach($records as $rec)
        {
            if(!empty($woIDs)) $woIDs.=',';
            $woIDs .= $rec['WIN_NUM'];            
        }
        
        //echo("<br/>woIDs: $woIDs");die();
		$apiWos = new Core_Api_WosClass();
        $result = $apiWos->updateBCatDataForWOs($woIDs,$projectID);   
        
    }
    
    public function getProjectAdditionalFields($projectId)
    {
         $db = Core_Database::getInstance();  
         $select = $db->select();
         $select->from("projects_additional_fields");
         $select->where("projectid = '$projectId'");         
         $records = Core_Database::fetchAll($select); 
         if(!empty($records)) return $records[0];
         else return null;         
    }
    
    public function saveProjectAdditionalFields($projectId,$fieldvals)
    {
        $current = $this->getProjectAdditionalFields($projectId);
        if(!empty($current))
        {
            $criteria = "projectid='$projectId'";
            $result = Core_Database::update("projects_additional_fields",$fieldvals,$criteria);
        }   
        else
        {
            $fieldvals['projectid'] = $projectId;
            $result = Core_Database::insert("projects_additional_fields",$fieldvals);
        }     
        return $result;
    }
    
    public function getProjectBCats($projectId)
    {
         $db = Core_Database::getInstance();  
         $select = $db->select();
         $select->from("projects_bcats");
         $select->where("projectid = '$projectId'"); 
         $select->order("bcattype desc");
         $select->order("bcatname asc");
         $records = Core_Database::fetchAll($select);  
         return $records;              
    }
    
    private function exists_projectBCatName($projectId,$bcatName)
    {
         $db = Core_Database::getInstance();  
         $select = $db->select();
         $select->from("projects_bcats");
         $select->where("projectid = '$projectId'"); 
         $select->where("bcatname = '$bcatName'"); 
         $records = Core_Database::fetchAll($select);  
         if(empty($records)) return 0;
         else return 1;
    }
    
    public function getProjectTurnedOnBCats($projectId)
    {
         $db = Core_Database::getInstance();  
         $select = $db->select();
         $select->from("projects_bcats");
         $select->where("projectid = '$projectId'"); 
         $select->where("ischecked = 1"); 
         $select->order("bcattype desc");
         $select->order("bcatname asc");
         $records = Core_Database::fetchAll($select);  
         return $records;                      
    }

    
    public function getProjectBCats_insertFromClientIfNotExists($projectId,$companyId='')
    {
        if(empty($projectId))
        {
            if(empty($companyId)) return null;
            else
            {
                //--- get data from client
                $userApi = new Core_Api_User();
                $records = $userApi->getClientCompanyBCats_insertIfNotExists($companyId);
                if(!empty($records))
                {
                    for($i=0;$i<count($records);$i++)
                    {
                        unset($records['companyid']);
                        $records[$i]['projectid']=0;
                        $records[$i]['bcatid']=0;
                    }
                }
                return $records;
            }
        }
        else
        {
            $projectInfo = $this->getProject($projectId);
            $companyId = $projectInfo['Project_Company_ID'];
            $current = $this->getProjectBCats($projectId);
            if(empty($current))
            {
                //--- get data from client
                $userApi = new Core_Api_User();
                $records = $userApi->getClientCompanyBCats_insertIfNotExists($companyId);
                
                //--- 
                foreach($records as $rec)            
                {
                    $fieldvals = array("projectid"=>$projectId,
                        "bcatname"=>$rec["bcatname"],
                        "bcatdesc"=>$rec["bcatdesc"],
                        "ischecked"=>$rec["ischecked"],
                        "reimbursable_thru_fs"=>$rec["reimbursable_thru_fs"],
                        "require_doc_upl"=>$rec["require_doc_upl"],
                        "bcattype"=>$rec["bcattype"]); 
                         
                    if($this->exists_projectBCatName($projectId,$rec["bcatname"]) == 0)  
                        $result = Core_Database::insert("projects_bcats",$fieldvals);    
                }            
                $current = $this->getProjectBCats($projectId);            
            }
            return $current;
        }
    }    
    
    public function getProjectTurnedOnBCats_insertFromClientIfNotExists($projectId)
    {
        $projectInfo = $this->getProject($projectId);
        $companyId = $projectInfo['Project_Company_ID'];
        $current = $this->getProjectBCats($projectId);
        if(empty($current))
        {
            //--- get data from client
            $userApi = new Core_Api_User();
            $records = $userApi->getClientCompanyBCats_insertIfNotExists($companyId);
            
            //--- 
            foreach($records as $rec)            
            {
                $fieldvals = array("projectid"=>$projectId,
                    "bcatname"=>$rec["bcatname"],
                    "bcatdesc"=>$rec["bcatdesc"],
                    "ischecked"=>$rec["ischecked"],
                    "reimbursable_thru_fs"=>$rec["reimbursable_thru_fs"],
                    "require_doc_upl"=>$rec["require_doc_upl"],
                    "bcattype"=>$rec["bcattype"]); 
                    
                $bcatname = $rec["bcatname"];
                if($this->exists_projectBCatName($projectId,$bcatname) == 0)  
                    $result = Core_Database::insert("projects_bcats",$fieldvals);
            }            
        }
        $ret = $this->getProjectTurnedOnBCats($projectId);
        return $ret;
    }    
    
    public function saveProjectBCat($bcatid,$fieldvals)
    {
        $projectId = $fieldvals['projectid'];
        if(!empty($bcatid))
        {
            $criteria = "bcatid='$bcatid'";
            unset($fieldvals['bcatid']);
            $result = Core_Database::update("projects_bcats",$fieldvals,$criteria);
        }
        else 
        {
            unset($fieldvals['bcatid']);
            $bcatname = $fieldvals["bcatname"];
            if($this->exists_projectBCatName($projectId,$bcatname) == 0)            
                $result = Core_Database::insert("projects_bcats",$fieldvals);
        }              
        return $result;  
    }

    //14091
    public function updateAllWOs_BillableBcats_ByProject($projectID)
    {

        //--- get WO Ids
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from(Core_Database::TABLE_WORK_ORDERS,array('WIN_NUM'));
        $select->where("Project_ID = '$projectID'"); 
        $select->where("Deactivated = 0"); 
        $select->where("TechMarkedComplete = 0"); 
        $select->where("Approved = 0");         
        $select->where("Status <> 'completed'");       
        $select->where("Status <> 'in accounting'");       
        $select->where("Status <> 'approved'");       
        $records = Core_Database::fetchAll($select);  

        if(empty($records)) return;
        
        $woIDs = '';
        foreach($records as $rec)
        {
            if(!empty($woIDs)) $woIDs.=',';
            $woIDs .= $rec['WIN_NUM'];            
        }
        
        //--- update 

		$apiWos = new Core_Api_WosClass();
        $apiWos->updateBCatDataForWOs($woIDs,$projectID);
        
    }

    /*
     * Applies signoff sheet changes of project to work orders.
     * 
	 * @param integer $id Project ID
	 * @param string $fname File name to add to work orders.  Removes files
	 * 		if left blank.
	 * @param integer $option Pass 1 for all active work orders or 2 for
	 * 		unassigned work orders.
	 * @param string $saveToField Adds ability to save other doc types.
	 * @return boolean Success true|false
     */
    public function updateAllWOs_CustomSignOff ($id, $fname, $option, $saveToField = 'CustomSignOff') {
		//Apply uploads to active work orders under the given project ID? 
		if ($option == 1 || $option == 2) {
			$db = Zend_Registry::get ("DB");
			$select = $db->select()->from ("work_orders", array ("WIN_NUM"));
			$select->where ("Project_ID = ?", $id);
			$select->where ("Deactivated = 0");
			$select->where ("TechMarkedComplete = 0");
			$select->where ("Approved = 0");

			//Limit to unassigned work orders only?
            if ($option == 2) {
            	$select->where ("Tech_ID IS NULL OR Tech_ID = ''");
            	$select->where ("Status = 'created' OR Status = 'published'");
            }

            $wo_list = $db->fetchAll ($select);

            if (!empty ($wo_list)) {
	            //Clear out any file entries to be overwritten
				$remove_files = array ();
				foreach ($wo_list as $wo) {
					$remove_files[] = $wo["WIN_NUM"];            
				}

				$files = new Core_Files ();
				if ($saveToField == "CustomSignOff") {
					$filesToRemove = $files->getFileIDsByWinsAndType($remove_files, Core_Files::SIGNOFF_FILE);
					if (!empty($filesToRemove))
						$db->delete ("files", $db->quoteInto ("id IN (?)", $filesToRemove));

					$fname_prepend = "projects-docs/";
					$ftype = Core_Files::SIGNOFF_FILE;
				}
				else {
					$fname_prepend = "";
					$ftype = Core_Files::WOS_PROJECT_FILE;
				}

				//If no file name is provided, previous operation clears all
				//signoffs and no new files are provided (delete)
				if (!empty ($fname)) {
					//Add file entries for the given work orders
					foreach ($wo_list as $wo){
						$files->insert (array (
							"WIN_NUM" => $wo["WIN_NUM"],
							"path"    => $fname_prepend . $fname,
							"type"    => $ftype,
							"description" => "Updated in project"
						));
		            }
				}

				return true;
            }
		}
		elseif ($option == 3) {	
			return true;	//Option 3 acceptable but has no effect on existing WOs
		}

		return false;
    }
    //--- 13622, 13992
    public function getProjectList_ForCompany($companyID)
    {
        $db = Core_Database::getInstance();
        $companyQuery = $db->select();
        $companyQuery->from(array('cl'=>Core_Database::TABLE_CLIENTS),
                array('Company_ID','CompanyName')
        );
        
        $companyQuery->joinLeft(array('clex'=>"clients_ext"),
                'clex.ClientId = cl.ClientID',
                array('ClientFSCSDId'=>'FSClientServiceDirectorId')
        );   
                
        $companyQuery->where("cl.Company_ID IS NOT NULL");
        $companyQuery->where("TRIM(cl.Company_ID) <> ''");
        $companyQuery->group("cl.Company_ID");
        
        
        $select = $db->select();
        $select->from(array('p'=>Core_Database::TABLE_PROJECTS),
                                    array('*'));
        $select->join(array('cl'=>$companyQuery),
                'cl.Company_ID = p.Project_Company_ID',
                    array('CompanyName','ClientFSCSDId')
        );
        
        $select->where("p.Project_Company_ID='$companyID'");
        //print_r($select->__toString());//test
        $records = Core_Database::fetchAll($select);  
        return $records;                      
    }
    //--- 13676
    public function getSiteStatusList()
    {
        return array(1=>'Scheduled',4=>'Rescheduled',2=>'Completed',3=>'Cancelled');
    }
    //--- 13676
    public function getSiteStatus_BySiteOfProject($siteNumber,$projectID)
    {
        $db = Core_Database::getInstance();
        $select = $db->select()->from(Core_Database::TABLE_SITES);
        $select->where("SiteNumber='$siteNumber'");
        $select->where("Project_ID='$projectID'");
        $records = Core_Database::fetchAll($select);         
        if(empty($records) || count($records)==0) 
        {
            return array('Status_ID'=>1, 'Status'=>'Scheduled');
        }
        else
        {
            $statusID = $records[0]['Status_ID'];
            if(empty($statusID))
            {
                return array('Status_ID'=>1, 'Status'=>'Scheduled');
            }
            else
            {
                $status = '';
                $statusArray = $this->getSiteStatusList();
                $status = $statusArray[$statusID];
                return array('Status_ID'=>$statusID, 'Status'=>$status);                
            }
        }
    }
    //--- 13676
    public function saveSiteStatus_BySiteOfProject($siteNumber,$projectID,$statusID)
    {
        if(empty($projectID) || empty($siteNumber)) return;

        $criteria = "Project_ID = '$projectID' AND SiteNumber = '$siteNumber'";
        $updateFields = array();
        $updateFields['Status_ID']=$statusID;
        if($statusID==self::SITE_STATUS_ID_SCHEDULED)
        {
            $updateFields['StartDate']=date("Y-m-d H:i:s");            
        }
        else if($statusID==self::SITE_STATUS_ID_COMPLETE)
        {
            $updateFields['CompleteDate']=date("Y-m-d H:i:s");            
        }
        else if($statusID==self::SITE_STATUS_ID_CANCELED)
        {
            $updateFields['CanceledDate']=date("Y-m-d H:i:s");            
        }
        else if($statusID==self::SITE_STATUS_ID_RESCHEDULED)
        {
            $updateFields['RescheduledDate']=date("Y-m-d H:i:s");            
        }
        $result = Core_Database::update("sites",$updateFields,$criteria);
    }

    //--- 13622
    public function updateServiceTypeWO_FromServiceTypeProject($winNum,$projectID)
    {
        //--- get ServiceType from project
        $db = Core_Database::getInstance();
        $select = $db->select()->from(Core_Database::TABLE_PROJECTS);
        $select->where("Project_ID='$projectID'");
        $records = Core_Database::fetchAll($select);         
        if(empty($records) || count($records)==0) {
            return;   
        } 
        
        $serviceType = $records[0]['ServiceTypeId'];
        //---- update ServiceTypeId of WO
        $criteria = "WINNUM = '$winNum'";
        $updateFields = array("ServiceTypeId"=>$serviceType);
        $result = Core_Database::update("work_orders_additional_fields",$updateFields,$criteria);
        return $result;
    }

    public function updateServiceTypeWO_FromItsProject($winNum)
    {
        //--- get ProjectID of WO
        $db = Core_Database::getInstance();
        $select = $db->select()->from(Core_Database::TABLE_WORK_ORDERS);
        $select->where("WIN_NUM='$winNum'");
        $records = Core_Database::fetchAll($select);         
        if(empty($records) || count($records)==0) {
            return;   
        } 
        $projectID=$records[0]['Project_ID'];
        
        //--- get ServiceType from project
        $db = Core_Database::getInstance();
        $select = $db->select()->from(Core_Database::TABLE_PROJECTS);
        $select->where("Project_ID='$projectID'");
        $records = Core_Database::fetchAll($select);         
        if(empty($records) || count($records)==0) {
            return;   
        }         
        $serviceType = $records[0]['ServiceTypeId'];
        //---- update ServiceTypeId of WO
        $criteria = "WINNUM = '$winNum'";
        $updateFields = array("ServiceTypeId"=>$serviceType);
        $result = Core_Database::update("work_orders_additional_fields",$updateFields,$criteria);
        return $result;
    }
    
    //14078
    public function updateProject_CSDID($projectID,$CSDID)
    { 
        $criteria = "Project_ID = '$projectID'";
        $updateFields = array("FSClientServiceDirectorId"=>$CSDID);
        $result = Core_Database::update("projects",$updateFields,$criteria);
        return $result;
    }
    
    public function updateProject_EntityTypeId($projectID,$EntityTypeId)
    { 
        $criteria = "Project_ID = '$projectID'";
        $updateFields = array("EntityTypeId"=>$EntityTypeId);
        $result = Core_Database::update("projects",$updateFields,$criteria);
        return $result;        
    }
    public function updateProject_ServiceTypeId($projectID,$ServiceTypeId)
    { 
        $criteria = "Project_ID = '$projectID'";
        $updateFields = array("ServiceTypeId"=>$ServiceTypeId);
        $result = Core_Database::update("projects",$updateFields,$criteria);
        return $result;        
    }

	public function saveP2TCertRequirements ($id, $insert, $is_update = true, $is_project = true) {
		//May write to projects or work_orders tables
		$table_name = $is_project ? "projects_rush_options" : "work_orders_rush_options";
		$id_field = $is_project ? "project_id" : "WIN_NUM";

		//Anonymous utility function to return number of credentials in data
		$get_count = function ($data) {
			$count = 0;
			foreach ($data as $type => $subdata) {
				foreach ($subdata as $value => $level) {
					if (!empty ($level)) $count++;
				}
			}
			return $count;
		};

		if ($get_count ($insert) > 0) {
			$db = Core_Database::getInstance();

			if ($is_update) {
				//Get current certification requirements
				$query = $db->query (
					$db->quoteInto (
						"SELECT * FROM $table_name WHERE $id_field = ?", 
						$id
					)
				);
				$result = $query->fetchAll ();

				//If exists and is modified, remove from insert list and update
				//but delete if marked as removed (level = -1)
				foreach ($result as $row) {
					$type = $row["type"];
					$option = $row["option_id"];

					if (isset ($insert[$type]) && isset ($insert[$type][$option])) {
						$temp = $insert[$type];

						if (isset ($temp[$option]["level"]) && $temp[$option]["level"] != -1) {
							if ($temp[$option]["level"] != $row["level"]) {
								$qry_update  = "UPDATE $table_name ";
								$qry_update .= "SET level = ? ";
								$qry_update .= "WHERE id = " . $row["id"];
								$qry_update = $db->quoteInto ($qry_update, $temp[$option]["level"]);

								$db->query ($qry_update);
							}
						}
						else {
							$qry_delete  = "DELETE FROM $table_name ";
							$qry_delete .= "WHERE id = " . $row["id"];

							$db->query ($qry_delete);
						}

						unset ($temp[$option]);
					}
				}
			}

			//Build single query to insert all new cert requirement records
			if ($get_count ($insert) > 0) {
				$qry_insert  = "INSERT INTO $table_name ";
				$qry_insert .= "($id_field, type, option_id, level) VALUES ";
				$is_first = true;
				$count = 0;

				foreach ($insert as $type => $subdata) {
					foreach ($subdata as $value => $level) {
						if ($level != -1) {
							if (!$is_first)
								$qry_insert .= ", ";
							else
								$is_first = false;

							$qry_insert .= "($id, '$type', '$value', $level)";
							$count++;
						}
					}
				}

				if ($count > 0) $db->query ($qry_insert);
			}
		}

		return false;
	}

	public function loadP2TCertRequirements ($id, $is_project = true) {
		$table_name = $is_project ? "projects_rush_options" : "work_orders_rush_options";
		$id_field = $is_project ? "project_id" : "WIN_NUM";
		$result = array ();

		$db = Core_Database::getInstance();
		$query = $db->query (
			$db->quoteInto (
				"SELECT * FROM $table_name WHERE $id_field = ?", 
				$id
			)
		);
		$records = $query->fetchAll ();

		foreach ($records as $row) {
			$type = $row["type"];
			$option = $row["option_id"];

			if (!isset ($result[$type])) $result[$type] = array ();

			$result[$type][$option] = intval ($row["level"]);
		}

		return $result;
	}

	public static function countP2TCertRequirements ($data) {
		$count = 0;

		foreach ($data as $type => $subdata) {
			foreach ($subdata as $value => $level) {
				if (!empty ($level)) $count++;
			}
		}

		return $count;
	}

    //14125
    public function createCopyProject($user, $pass, $project)
	{
        $errors = Core_Api_Error::getInstance();

        $logger = Core_Api_Logger::getInstance();
		$logger->setUrl('createProject');
        $logger->setLogin($user);
		$logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');

		$authData = array('login'=>$user, 'password'=>$pass);
        $userClass = new Core_Api_User();
		$userClass->checkAuthentication($authData);
        if (!$userClass->isAuthenticate())
			return API_Response::fail();

		if (is_string($project)) $woXML = $project;
		else {
			$dom = new DOMDocument("1.0");
			$dom->appendChild($project->toXMLElementTect($dom));
			$woXML = $dom->saveXML();
		}
        if (empty($woXML))
			$errors->addError(2, 'XML parameter is empty but required');

		$err = $errors->getErrors();
		if (!empty($err))
			return API_Response::fail();

        $prjData = $this->getDataFromProjectXML($woXML);
        
        $fieldListType = Core_Database::getFieldListType(Core_Database::TABLE_PROJECTS);
        $prjData = Core_Database::convertQueryResult($prjData, $fieldListType);
        
        $result = $this->exists_Project($prjData['Project_Name']);       
        if ($result) {    		
		$errors->addError(6, 'Project_Name already exist.');
                $err = $errors->getErrors();
		if (!empty($err))
			return API_Response::fail();
        }       
        if (isset($prjData['Project_ID'])) {
                $prjIdOld = $prjData['Project_ID'];
		unset($prjData['Project_ID']);
		}
        $db = Core_Database::getInstance();
                $clientCredential = "";
		if (isset($prjData['ClientCredential'])) {
			$clientCredential = $prjData['ClientCredential'];
			unset($prjData['ClientCredential']);
		}             
		$result = false;
                try {
                    $result = Core_Database::insert(Core_Database::TABLE_PROJECTS, $prjData);
		} catch (Exception $e) {			
                     error_log($e);               			
		}
	
		$prj_id = $db->lastInsertId(Core_Database::TABLE_PROJECTS);
                
        if (!$result) {
    		$err = $errors->getErrors();
    		if (!empty($err))
				return API_Response::fail();

		    $errors->addError(7, 'Incorrect SQL request');
			return API_Response::fail();
		}
                $files = new Core_ProjectsFiles();
                $select = $files->select();
                $select->where('Project_ID = ?', $prjIdOld);
                $filesList = $files->fetchAll($select);
		if (!empty($filesList)) {
                        $files = new Core_ProjectsFiles();
			foreach ($filesList as $file) {				                           
                            $fileId = $files->insert(array(
                                'Project_ID' => $prj_id,
                                'path'       => $file['path'],
                                'description'=> mysql_escape_string($file['description'])
                            ));
			}
		}
                $extProject = $this->getProjectAdditionalFields($prjIdOld);
                if (!empty($extProject)) {
                    $fieldvals = array();
                    $fieldvals["enable_expense_reporting"] = $extProject['enable_expense_reporting'];   
                    $fieldvals['projectid'] = $prj_id;
                    $result = Core_Database::insert("projects_additional_fields",$fieldvals);                  
                }
                
                $apiQT = new Core_Api_CustomerClass();
                $quickTicket_ID = $apiQT->getQTVId_ForProject($prjIdOld);
		
                if($quickTicket_ID >0)
                {
                    $apiQT->addProjectToQTVOnlyOne($quickTicket_ID,$prj_id);
                }
                $records = $this->getProjectBCats($prjIdOld);
                if(!empty($records))
                {
                    foreach($records as $rec)            
                    {
                        $fieldvals = array("projectid"=>$prj_id,
                            "bcatname"=>$rec["bcatname"],
                            "bcatdesc"=>$rec["bcatdesc"],
                            "ischecked"=>$rec["ischecked"],
                            "reimbursable_thru_fs"=>$rec["reimbursable_thru_fs"],
                            "require_doc_upl"=>$rec["require_doc_upl"],
                            "bcattype"=>$rec["bcattype"]); 

                            $result = Core_Database::insert("projects_bcats",$fieldvals);
                    }            
                }
                //
		if (!empty($clientCredential)) {
			// save client credential
			foreach ($clientCredential as $cid) {
				$pc = new Core_ProjectCertification($prj_id, $cid);
				$pc->save();
			}
		}

		return API_Response::success(array(new API_Project($prj_id)));
	}
    public function copyProject($user, $pass, $project,$prjName)
    {        
        $project->Project_Name = $prjName;
         
        try{            
            $return = $this->createCopyProject($user, $pass, $project);         
            
        }catch(Exception $e){
           
            error_log($e);
        }       
	return $return;
    }
  
    public function exists_Project($ProjectName)
    {
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from(Core_Database::TABLE_PROJECTS,array('Project_Name'));
        $select->where("Project_Name = ?", $ProjectName);
        $records = Core_Database::fetchAll($select); 
        
        if(!empty($records) && count($records) > 0){
            return true;
        } else {
            return false;
        }
    }//14125

  
}


