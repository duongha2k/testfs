<?php

class Core_Api_AdminUser
{
	protected $login = '';
	protected $password = '';
    protected $adminId = null;
	protected $authenticate = false;
	protected $accounting = '';
	protected $fsplusone = '';

    /**
     *
     * @param array $authData login/password or just login
     * @return Core_Api_AdminUser $this
     * @author Pavel Shutin
     */
    public function load($authData) {

        $errors = Core_Api_Error::getInstance();

        if (!is_array($authData) || empty($authData['login'])) {
			$errors->addError(2, 'Login is empty but required');
            return false;
        }

        $this->login = $authData['login'];
        if (isset($authData['password'])) {
          $this->password = $authData['password'];
        }

        if (defined('SHORT_AUTH')) {
            if (!empty($_SESSION['UserObj']) && !empty($user[0]['AdminID'])) {
                $user = $_SESSION['UserObj'];
            }else{
                $fieldList = array("AdminID");
                $fieldList = array_combine($fieldList, $fieldList);

                $db = Core_Database::getInstance();
                $select = $db->select();
                $select->from(Core_Database::TABLE_ADMINS, array("AdminID","Password", "accounting", "fsplusone"))
                    ->where("UserName = ?", $this->login);
                if (!empty($this->password)) {
                    $select->where("MD5(Password) = MD5(?)", $this->password);
                }
                $user = Core_Database::fetchAll($select);
                $_SESSION['UserObj'] = $user;
            }

        }else{
                $db = Core_Database::getInstance();
                $select = $db->select();
                $select->from(Core_Database::TABLE_ADMINS, array("AdminID","Password", "accounting"))
                    ->where("UserName = ?", $this->login);
                if (!empty($this->password)) {
                    $select->where("MD5(Password) = MD5(?)", $this->password);
                }
        }

		if (!empty($user)) {
            $this->adminId = $user[0]['AdminID'];
            $this->password = $user[0]['Password'];
            $this->accounting = $user[0]['accounting'];
            $this->fsplusone = $user[0]['fsplusone'];
		} else {
			$er = $errors->getErrors();
			if (empty($er)) {
				$errors->addError(3, 'Login or password is not correct');
			}
            $this->adminId = null;
		}
        return $this;
    }

	/**
	 * Check up user information
	 *
	 * @param array $authData
	 * @return bool
	 */
	public function checkAuthentication($authData)
	{
		$cache = Core_Cache_Manager::factory();

		$errors = Core_Api_Error::getInstance();

		$cacheKey = $authData['login'] . "|$|" . $authData['password'];

		$this->login = $authData['login'];
		$this->password = $authData['password'];

		if (empty($this->login) || empty($this->password)) {
			$errors->addError(2, 'Login or password is empty but required');
			$this->authenticate = false;
			return false;
		}

		if (!$this->checkProtocol()) {
			throw new SoapFault("jeff", "failing in check protocol");
			$this->authenticate = false;
			return false;
		}
        $user = $this->load($authData);

        if ($user->adminId) {
            $this->authenticate = true;
        }else{
            $this->authenticate = false;
        }

		return $this->authenticate;
	}
	
	/**
	 * Add record to login history
	 *
	 * @return bool
	 */
	public function loginHistory($userName)
	{
		
		$class = new Core_Api_Class;
		$class->loginHistory($userName, "Admin");
	}

	/**
	 * Check up user authentication
	 *
	 * @return bool
	 */
	public function isAuthenticate()
	{
		return $this->authenticate;
	}

	/**
	 *
	 *
	 * @return bool
	 */
	protected function checkProtocol()
	{
		return true;

		if (HTTPS_PROTOCOL_REQUIRED) {
			$cur_protocol = $_SERVER['SERVER_PROTOCOL'];
			$cur_protocol = explode('/',$cur_protocol);
			if ($cur_protocol[0] == 'HTTPS') {
				$errors = Core_Api_Error::getInstance();
				$errors->addError(4, 'Not HTTPS protocol used');
				return false;
			} else {
				return true;
			}
		}
		return false;
	}

	public function getAdminId()
	{
		return $this->adminId;
	}

	public function getLogin()
	{
	    return $this->login;
    }

	public function getPassword()
	{
	    return $this->password;
    }
    
	public function isAccounting()
	{
	    return $this->accounting;
    }
    
	public function isFsPlusOne()
	{
	    return $this->fsplusone;
    }
    
    //--- 13233
    public function getAdminInfo($adminUserName)
    {
        $db = Core_Database::getInstance();
        $select = $db->select();
        $select->from(Core_Database::TABLE_ADMINS, array("AdminID","Fname", "Lname", "Email","Password"));
        $select->where("UserName = '$adminUserName'");                
        $records = Core_Database::fetchAll($select);
        if(empty($records) || count($records)==0) return null;
        else return $records[0];
    }
    
    
    public function getAdminUserList()
    {
        /*admins     AdminID     Fname     Lname     UserName*/
        $db = Core_Database::getInstance();
        $select = $db->select();
        $select->from(Core_Database::TABLE_ADMINS, array("AdminID","Fname", "Lname", "Email",'UserName'));
        $records = Core_Database::fetchAll($select);
        if(empty($records) || count($records)==0) return null;
        else return $records;
    }
    
    
    
    
    
}