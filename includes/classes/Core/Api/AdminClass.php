<?php
class Core_Api_AdminClass extends Core_Api_CommonClass
{
    public $TagCodeIdArray = array('ClientStatusId','ClientRelationshipId','SegmentId','OriginalSellerId','ExecutiveId','EntityTypeId','ServiceTypeId','SystemStatusId','FSClientServiceDirectorId','FSAccountManagerId');
    
    public static $CompanyInfoArray = null;
        
	public function __construct() {
		parent::__construct();
	}

	/**
	 * pull the list of timestamps for a work order
	 *
	 * @param string $user
	 * @param string $pass
	 * @param int $winNum
	 * @param int $offset
	 * @param int $limit
	 * @param string $sort
	 * @return API_Response
	 */
	public function getTimestampsWithWinNum($user, $pass, $winNum, $offset = 0, $limit = 0, $sort = "")
	{
       	$logger = Core_Api_Logger::getInstance();
		$logger->setUrl('getTimestampsForWinNum' . $winNum);
		$logger->setLogin($user);
		$logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');

		$authData = array('login'=>$user, 'password'=>$pass);

		$user = new Core_Api_AdminUser();
		$user->checkAuthentication($authData);

		if (!$user->isAuthenticate())
			return API_Response::fail();

		return API_Response::success(API_Timestamp::getTimestamps($winNum, null, $offset, $limit, $sort));
	}

	/**
	 * pull basic tech info
	 *
	 * @param string $user
	 * @param string $pass
	 * @param int $id
	 * @return API_Response
	 */
	public function techLookup($user, $pass, $id)
	{
		$tech = new API_Tech();
		$tech->lookupID($id);
		return API_Response::success(array($tech));
	}

	/**
	 * pull basic tech info
	 *
	 * @param string $user
	 * @param string $pass
	 * @param string $fileName
	 * @return API_Response
	 */

    public function getFile( $user, $pass, $fileName )
    {
       	$logger = Core_Api_Logger::getInstance();
		$logger->setUrl('getWorkOrders');
        $logger->setLogin($user);
		$logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');

		$authData = array('login'=>$user, 'password'=>$pass);
		$errors = Core_Api_Error::getInstance();
		$errors->resetErrors();

		$user = new Core_Api_AdminUser();
        $user->checkAuthentication($authData);

		if (!$user->isAuthenticate())
			return API_Response::fail();

        $file = Core_Caspio::caspioDownloadFile( $fileName );

		$err = $errors->getErrors();
		if (!empty($err))
			return API_Response::fail();

        return $file;
    }

	/**
	 * get list of work orders
	 *
	 * @param string $user
	 * @param string $pass
	 * @param string $fields
	 * @param string $timeStampFrom
	 * @param string $timeStampTo
	 * @param string $timeStampType
	 * @param API_AdminWorkOrderFilter $filters
	 * @param int $offset
	 * @param int $limit
	 * @param int $countRows
	 * @param string $sort
	 * @return API_Response
	 */
	public function getWorkOrders($user, $pass, $fields, $timeStampFrom, $timeStampTo, $timeStampType, $filters, $offset = 0, $limit = 0, &$countRows = NULL, $sort = "")
	{

       	$logger = Core_Api_Logger::getInstance();
		$logger->setUrl('getWorkOrders');
        $logger->setLogin($user);
		$logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');

		$authData = array('login'=>$user, 'password'=>$pass);
		$errors = Core_Api_Error::getInstance();

		$user = new Core_Api_AdminUser();
        $user->checkAuthentication($authData);

		if (!$user->isAuthenticate())
			return API_Response::fail();

		$countRows = 0;

		try {
			$result = API_AdminWorkOrderFilter::filter(null,
													$fields,
													$timeStampFrom,
													$timeStampTo,
													$timeStampType,
													$filters, $sort, $limit, $offset, $countRows);

			$woArray = API_AdminWorkOrder::makeWorkOrderArray($result->toArray(), new API_AdminWorkOrder(NULL));
		} catch (Exception $e) {
			$errors->addError(7, 'SQL Error: '. $e);
			error_log($e);
		}
		$err = $errors->getErrors();
		if (!empty($err))
			return API_Response::fail();

		return API_Response::success($woArray);
	}


	/**
	 * publish work order
	 *
	 * @param string $user
	 * @param string $pass
	 * @param int $winNum
	 * @return API_Response
	 */
	public function publishWorkOrder($user, $pass, $winNum, $additionalFields = array())
	{
       	$logger = Core_Api_Logger::getInstance();
		$logger->setUrl('publishWorkOrder ' . $winNum);

        $logger->setLogin($user);
		$logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');

		$authData = array('login'=>$user, 'password'=>$pass);

		$user = new Core_Api_AdminUser();
		$user->checkAuthentication($authData);

		if (!$user->isAuthenticate())
			return API_Response::fail();

		$params = array('TB_UNID'=>$winNum);
		$queryFieldList = array();
		$queryFieldList["ShowTechs"] = "True";
		$queryFieldList["Deactivated"] = "False";
		$queryFieldList["Tech_ID"] = "";
		$queryFieldList["TechMarkedComplete"] = "False";

		foreach ($additionalFields as $key => $value) {
			$queryFieldList[$key] = $value;
		}

		return $this->updateWorkOrderPostXMLParse($user, $params, $queryFieldList);
	}

	/**
	 * assign work order
	 *
	 * @param string $user
	 * @param string $pass
	 * @param int $winNum
	 * @param int $techID
	 * @param float $bidAmount
     * @param string $bidComments
     * @param string $amountPer
     * @param int $qtyDevices
	 * @return API_Response
	 */
 	public function assignWorkOrder($user, $pass, $winNum, $techID, $bidAmount,$bidComments=null,$amountPer=null,$qtyDevices=null)
	{
       	$logger = Core_Api_Logger::getInstance();
		$logger->setUrl('assignWorkOrder ' . $winNum);
        $logger->setLogin($user);
		$logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');

		$authData = array('login'=>$user, 'password'=>$pass);

		$user = new Core_Api_AdminUser();
		$user->checkAuthentication($authData);

		if (!$user->isAuthenticate())
			return API_Response::fail();

		$params = array('TB_UNID'=>$winNum);
		$idField = defined("FLSVersion") && FLSVersion ? "FLS_ID" : "Tech_ID";
		$queryFieldList = array($idField=>$techID, 'Tech_Bid_Amount'=>$bidAmount, 'bid_comments'=>trim($bidComments));
        if(!empty($amountPer)) {
            $queryFieldList['Amount_Per'] = $amountPer;
            $queryFieldList['Qty_Devices'] = $qtyDevices;
        }
		return $this->updateWorkOrderPostXMLParse($user, $params, $queryFieldList);
	}

	/**
	 * approve work order
	 *
	 * @param string $user
	 * @param string $pass
	 * @param int $winNum
	 * @return API_Response
	 */
 	public function approveWorkOrder($user, $pass, $winNum, $payAmount = null)
	{
       	$logger = Core_Api_Logger::getInstance();
		$logger->setUrl('approveWorkOrder ' . $winNum);
        $logger->setLogin($user);
		$logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');

		$authData = array('login'=>$user, 'password'=>$pass);

		$user = new Core_Api_AdminUser();
		$user->checkAuthentication($authData);

		if (!$user->isAuthenticate())
			return API_Response::fail();

		$params = array('TB_UNID'=>$winNum);
		$queryFieldList = array();
		$queryFieldList["Approved"] = "True";
		$queryFieldList["Invoiced"] = "False";
		$queryFieldList["TechMarkedComplete"] = "True";
		if ($payAmount) {
			$queryFieldList['PayAmount'] = $payAmount;
		}
		return $this->updateWorkOrderPostXMLParse($user, $params, $queryFieldList);
	}

	/**
	 * deactivate work order
	 *
	 * @param string $user
	 * @param string $pass
	 * @param int $winNum
	 * @param string $code
	 * @param string $reason
	 * @return API_Response
	 */
 	public function deactivateWorkOrder($user, $pass, $winNum, $code, $reason = "")
	{
       	$logger = Core_Api_Logger::getInstance();
		$logger->setUrl('deactivateWorkOrder ' . $winNum);
        $logger->setLogin($user);
		$logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');

		$authData = array('login'=>$user, 'password'=>$pass);

		$user = new Core_Api_AdminUser();
		$user->checkAuthentication($authData);

		if (!$user->isAuthenticate())
			return API_Response::fail();

		$params = array('TB_UNID'=>$winNum);
		$queryFieldList = array();
		$queryFieldList["Deactivated"] = "True";
		$queryFieldList["DeactivationCode"] = $code;
		$queryFieldList["Deactivated_Reason"] = $reason;
		return $this->updateWorkOrderPostXMLParse($user, $params, $queryFieldList);
	}

	/**
	 * mark work order incomplete
	 *
	 * @param string $user
	 * @param string $pass
	 * @param int $winNum
	 * @param string $comments
	 * @return API_Response
	 */
 	public function markWorkOrderIncomplete($user, $pass, $winNum, $comments)
	{
       	$logger = Core_Api_Logger::getInstance();
		$logger->setUrl('markWorkOrderIncomplete ' . $winNum);
        $logger->setLogin($user);
		$logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');

		$authData = array('login'=>$user, 'password'=>$pass);

		$user = new Core_Api_AdminUser();
		$user->checkAuthentication($authData);

		if (!$user->isAuthenticate())
			return API_Response::fail();

		$params = array('TB_UNID'=>$winNum);
		$queryFieldList = array();
		$queryFieldList["TechMarkedComplete"] = "False";
		$queryFieldList["MissingComments"] = $comments;
		$queryFieldList["Approved"] = "False";
		return $this->updateWorkOrderPostXMLParse($user, $params, $queryFieldList);
	}

	/**
	 * get work orders using win num
	 *
	 * @param int $winNum
	 * @param string $fields
	 * @param string $companyId
	 * @param string $sort
	 * @access protected
	 * @return API_Response
	 */
	protected function getWorkOrdersWithWinNum($winNum, $fields, $sort, $companyId = "")
	{
		if (is_integer($winNum)) $winNum = array($winNum);
		if (!is_array($winNum)) return false;
		if (!empty($fields)) {
			$fieldList = str_replace(' ', '', $fields);
			$fieldListParts = explode(",", $fieldList);
		}
		else
			$fieldListParts = Core_Database::getFieldList(Core_Database::TABLE_WORK_ORDERS);

		$fieldListMap = array_combine($fieldListParts, $fieldListParts);

/*		if (!empty($fieldListMap["Status"]) || empty($fields))
			$fieldListMap["Status"] = Core_WorkOrder_Status::getStatusQueryAsName();*/

		$db = Core_Database::getInstance();
		$select = $db->select();

		$select->from(Core_Database::TABLE_WORK_ORDERS, $fieldListMap )
			->where("WIN_NUM IN (?)", $winNum)
			->order($sort);

		if (!empty($companyId)) {
			$select->where("Company_ID = ?", $companyId);
		}

		$result = Core_Database::fetchAll($select);

		return $result;
	}

	/**
	 * Get Work Order
	 *
	 * @param string $user
	 * @param string $pass
	 * @param int $winNum
	 * @return API_Response
	 */
	public function getWorkOrder($user, $pass, $winNum)
	{
       	$logger = Core_Api_Logger::getInstance();
		$logger->setUrl('getWorkOrder ' . $winNum);
        $logger->setLogin($user);
		$logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');

		$authData = array('login'=>$user, 'password'=>$pass);
		$errors = Core_Api_Error::getInstance();

		if (empty($winNum))
			$errors->addError(2, 'Work Order ID parameter is empty but required');

		$err = $errors->getErrors();
		if (!empty($err))
			return API_Response::fail();

		$wo_id = (int)$winNum;

		$user = new Core_Api_AdminUser();
		$user->checkAuthentication($authData);

		if (!$user->isAuthenticate())
			return API_Response::fail();

		$fieldList = Core_Database::getFieldList(Core_Database::TABLE_WORK_ORDERS);
		$fieldList = array_combine($fieldList, $fieldList);
        $wofFields =  array('AuditNeeded','AuditNeededBy','AuditNeededDate','AuditComplete','AuditCompleteBy','AuditCompleteDate','EntityTypeId','ServiceTypeId','FSClientServiceDirectorId','FSAccountManagerId','WMAssignmentID', 'PushWM');
        
		$db = Core_Database::getInstance();
		$select = $db->select();
		$select->from(array('w'=>Core_Database::TABLE_WORK_ORDERS), $fieldList)
					->where("w.WIN_NUM = ?", $wo_id);
                    
        $select->joinLeft(array('tbi'=>Core_Database::TABLE_TECH_BANK_INFO),
                'tbi.TechID = w.Tech_ID',
				array('WMID')
        );        
		
        $select->joinLeft(array('waf'=>'work_orders_additional_fields'),
                'waf.WINNUM = w.WIN_NUM',
                $wofFields
        );        
        
		$result = Core_Database::fetchAll($select);
    	$err = $errors->getErrors();
		if (!empty($err))
			return API_Response::fail();

		return API_Response::success(API_AdminWorkOrder::makeWorkOrderArray($result, new API_AdminWorkOrder(NULL)));
	}

	/**
	 * get win num
	 *
	 * @param string $user
	 * @param string $pass
	 * @param string $woID
	 * @return API_Response
	 */
	public function getWinNum($user, $pass, $woID)
	{
       	$logger = Core_Api_Logger::getInstance();
		$logger->setUrl('getWinNum ' . $woID);
        $logger->setLogin($user);
		$logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');

		$authData = array('login'=>$user, 'password'=>$pass);

		$user = new Core_Api_AdminUser();
		$user->checkAuthentication($authData);

		if (!$user->isAuthenticate())
			return API_Response::fail();

		$winNum = $this->retrieveWinNum($woID);

		if (empty($winNum))
		{
			$errors = Core_Api_Error::getInstance();
			$errors->addError(6, 'No such client work order number');
			return API_Response::fail();
		}

		return API_Response::success(array(new API_AdminWorkOrder($winNum)));
	}

	private function retrieveWinNum($woID, $companyID = null)
	{
		$db = Core_Database::getInstance();
		$select = $db->select();

		$select->from(Core_Database::TABLE_WORK_ORDERS, "WIN_NUM")
			->where("WO_ID = ?", $woID);

		if (!empty($companyID))
			$select->where("Company_ID = ?", $companyID);

		$result = Core_Database::fetchAll($select);

		return empty($result[0]['WIN_NUM']) ? NULL : $result[0]['WIN_NUM'];
	}

	private function blankIfInvalidDate($date) {
		if (empty($date) || strtotime($date) === FALSE) return null;
		return $date;
	}

	private function pullAssignTechInfo($companyId, $id, $isFLS = FALSE) {
		$fieldListMap = array(
					"TechID" => "TechID",
					"AcceptTerms" => "AcceptTerms",
					"FirstName" => "FirstName",
					"LastName" => "LastName",
					"PrimaryEmail" => "PrimaryEmail",
					"PrimaryPhone" => "PrimaryPhone",
					"PrimaryPhoneExt" => "PrimaryPhoneExt",
					"PrimaryPhoneExt" => "PrimaryPhoneExt",
					"FLS_ID" => "FLSID",
					"Deactivated" => "Deactivated",
					"WMID" => "WMID"
				);

		$id = Core_Caspio::caspioEscape($id);
		$criteria = $isFLS ? "FLSID = '$id'" : "TechID = '$id'";
		$criteria .= " AND Deactivated = '0'";

		$result = Core_Caspio::caspioSelectWithFieldListMap(TABLE_MASTER_LIST, $fieldListMap, $criteria, "");
		if (!$result) return $result;
		$result[0]['IsAdminDenied'] = Core_Tech::isClientDenied($id, $companyId) ? $id : '';
		return $result;
	}

	/**
	 * create Work Order
	 *
	 * @param string $user
	 * @param string $pass
	 * @param boolean $publish
	 * @param API_AdminWorkOrder $workOrder
	 * @return API_Response
	 */
	public function createWorkOrder($user, $pass, $publish, $workOrder)
	{
		$benchmark  = Core_Benchmark::getInstance();

		$benchmark->point('Method: "createWorkOrder"; Start log method call');
		if (is_string($workOrder)) $woXML = $workOrder;
		else {
			$dom = new DOMDocument("1.0");
			$dom->appendChild($workOrder->toXMLElement($dom));
			$woXML = $dom->saveXML();
		}

       	$logger = Core_Api_Logger::getInstance();
		$logger->setUrl('createWorkOrder');
        $logger->setLogin($user);
		$logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');

		$authData = array('login'=>$user, 'password'=>$pass);
		$params = array('xml'=>$woXML);
		$errors = Core_Api_Error::getInstance();

		if (empty($params['xml']))
			$errors->addError(2, 'XML parameter is empty but required');

		$err = $errors->getErrors();
		if (!empty($err))
			return API_Response::fail();

		$benchmark->point('Auth');
		$user = new Core_Api_AdminUser();
		$user->checkAuthentication($authData);
		$benchmark->point('Auth Done');

		if (!$user->isAuthenticate())
			return API_Response::fail();

        $woData = $this->getDataFromXML($params['xml']);

        $this->excludeFields($woData);

	$fieldListType = Core_Database::getFieldListType(Core_Database::TABLE_WORK_ORDERS);

	$woData = Core_Database::convertQueryResult($woData, $fieldListType);

        $woData['ShowTechs'] = ($publish === TRUE ? "True" : "False");
		$woData['Company_ID']   = $user->getContextCompanyId();
		$company = $user->getContextCompanyId();
		$woData['Company_Name'] = $user->getContextCompanyName();

		$payFieldUpdated = false;
		$payFields = Core_TechPay::recalcFields();
		$payFields = array_combine($payFields, $payFields);
		foreach ($woData as $key=>$val) {
			if (array_key_exists($key, $payFields))
				$payFieldUpdated = true;
		}

        if ( ((!empty($woData['TechMarkedComplete']) && $woData['TechMarkedComplete'] == "True") || (!empty($woData['Approved']) && $woData['Approved'] == "True")) && (empty($woData['PayAmount']) || $woData['PayAmount'] == 0.00 || $payFieldUpdated)) {
			$techPayCalculator = new Core_TechPay();
			$woData = $techPayCalculator->updateTechPay($woData);
		}

	$benchmark->point('Pull assign if needed');
		if (!empty($woData["FLS_ID"]))
			$techData = $this->pullAssignTechInfo($user, $woData["FLS_ID"], true);
		else if (!empty($woData["Tech_ID"]))
			$techData = $this->pullAssignTechInfo($user, $woData["Tech_ID"], false);

	$benchmark->point('Pull assign done');

		if (!empty($techData[0])) {
			$techData = $techData[0];
			$woData["Tech_ID"] = $techData["TechID"];
			$woData["Tech_FName"] = $techData["FirstName"];
			$woData["Tech_LName"] = $techData["LastName"];
			$woData["TechEmail"] = $techData["PrimaryEmail"];
			$woData["TechPhone"] = $techData["PrimaryPhone"];
			if (!empty($techData["PrimaryPhoneExt"]))
				$woData["TechPhoneExt"] = $techData["PrimaryPhoneExt"];
			else
				$woData["TechPhoneExt"] = NULL;
			if (!empty($techData["IsAdminDenied"])) {
		        	$errors->addError(2, "You are about to assign work to a technician that has been \"denied access\" by your company. Please re-confirm your choice of technician. Tech ID: " . $woData["Tech_ID"] . " (" . $woData["Tech_FName"] . " " . $woData["Tech_LName"] . ")");
				return API_Response::fail();
			}
			if (!empty($techData["FLS_ID"]) && $techData["FLS_ID"] != 'NULL')
				$woData["FLS_ID"] = trim($techData["FLS_ID"] );
			else {
				$woData["FLS_ID"] = NULL;
				if ($woData['Company_ID'] == "FLS") {
					$errors->addError(2, "You are about to assign work to a technician that does not have a FLS Badge. Please re-confirm your choice of technician. Tech ID: " . $woData["Tech_ID"] . " (" . $woData["Tech_FName"] . " " . $woData["Tech_LName"] . ")");
					return API_Response::fail();
				}
			}
			if (strtolower($techData["AcceptTerms"]) != "yes" && $techData["AcceptTerms"] != 'WM') {
	        		$errors->addError(2, "The technician you're attempting to assign has not accepted terms of usage.  Sorry for any issues this may cause you. Tech ID: " . $woData["Tech_ID"] . " (" . $woData["Tech_FName"] . " " . $woData["Tech_LName"] . ")");
				return API_Response::fail();
			}
		}
		else {
			$woData['Tech_ID'] = NULL;
            $woData['Date_Assigned'] = "";
			$woData["Tech_FName"] = "";
			$woData["Tech_FName"] = "";
			$woData["Tech_LName"] = "";
			$woData["TechEmail"] = "";
			$woData["TechPhone"] = "";
			$woData["Tech_Bid_Amount"] = NULL;
			$woData["FLS_ID"] = NULL;
		}

		if (!empty($woData["FLS_ID"]))
			$techData = $this->pullAssignTechInfo($user, $woData["FLS_ID"], true);
		else if (!empty($woData["Tech_ID"]))
			$techData = $this->pullAssignTechInfo($user, $woData["Tech_ID"], false);

		if (!empty($techData[0])) {
			$techData = $techData[0];
			$woData["Tech_ID"] = $techData["TechID"];
			$woData["Tech_FName"] = $techData["FirstName"];
			$woData["Tech_LName"] = $techData["LastName"];
			$woData["TechEmail"] = $techData["PrimaryEmail"];
			$woData["TechPhone"] = $techData["PrimaryPhone"];
			if (!empty($techData["PrimaryPhoneExt"]))
				$woData["TechPhoneExt"] = $techData["PrimaryPhoneExt"];
			else
				$woData["TechPhoneExt"] = NULL;
			if (!empty($techData["IsAdminDenied"])) {
		        	$errors->addError(2, "You are about to assign work to a technician that has been \"denied access\" by your company. Please re-confirm your choice of technician. Tech ID: " . $woData["Tech_ID"] . " (" . $woData["Tech_FName"] . " " . $woData["Tech_LName"] . ")");
				return API_Response::fail();
			}
			if (!empty($techData["FLS_ID"]) && $techData["FLS_ID"] != 'NULL')
				$woData["FLS_ID"] = trim($techData["FLS_ID"] );
			else {
				$woData["FLS_ID"] = NULL;
				if ($woData['Company_ID'] == "FLS") {
					$errors->addError(2, "You are about to assign work to a technician that does not have a FLS Badge. Please re-confirm your choice of technician. Tech ID: " . $woData["Tech_ID"] . " (" . $woData["Tech_FName"] . " " . $woData["Tech_LName"] . ")");
					return API_Response::fail();
				}
			}
			if (strtolower($techData["AcceptTerms"]) != "yes" && $techData["AcceptTerms"] != 'WM') {
	        		$errors->addError(2, "The technician you're attempting to assign has not accepted terms of usage.  Sorry for any issues this may cause you. Tech ID: " . $woData["Tech_ID"] . " (" . $woData["Tech_FName"] . " " . $woData["Tech_LName"] . ")");
				return API_Response::fail();
			}
		}
		else {
			$woData['Tech_ID'] = NULL;
        		$woData['Date_Assigned'] = "";
			$woData["Tech_FName"] = "";
			$woData["Tech_FName"] = "";
			$woData["Tech_LName"] = "";
			$woData["TechEmail"] = "";
			$woData["TechPhone"] = "";
			$woData["Tech_Bid_Amount"] = NULL;
			$woData["FLS_ID"] = NULL;
		}

		if (empty($woData["Project_ID"]) && empty($woData["Project_Name"]))
			$woData["Project_Name"] = "Default Project";

		$benchmark->point('Combine Project Info');
		$combiner = new Core_Combiner_WorkOrder($woData);
		$combiner->setCompanyId($woData['Company_ID']);
		$combiner->setCompanyName($woData['Company_Name']);
		$combiner->setUserName($user->getLogin());
		if ($combiner->combine()) {
			$woData = $combiner->getData();
		} else {
		    $errors->addErrors('6', $combiner->getErrors());
			return API_Response::fail();
		}
		$benchmark->point('Combiner Project Info Done');

		if (!empty($woData['Tech_ID'])) {
			$projectInfo = new Core_Project((int)$woData["Project_ID"]);
			$projectInfo->setData();
			if (strtoupper($projectInfo->getFixedBid()) == "TRUE")
				$woData["Tech_Bid_Amount"] = $woData["PayMax"];
		}

		$woData["Username"] = $user->getLogin();


		if (!empty($woData['Approved']) && $woData['Approved'] != "True" && $woData['Approved'] != "False")
			$woData["Approved"] = "False";
		if (!empty($woData['Invoiced']) && $woData['Invoiced'] != "True" && $woData['Invoiced'] != "False")
			$woData["Invoiced"] = "False";
		if (!empty($woData['TechPaid']) && $woData['TechPaid'] != "True" && $woData['TechPaid'] != "False")
			$woData["TechPaid"] = "False";

		if (array_key_exists("StartDate", $woData))
			$woData["StartDate"] = $this->blankIfInvalidDate($woData["StartDate"]);
		if (array_key_exists("EndDate", $woData))
			$woData["EndDate"] = $this->blankIfInvalidDate($woData["EndDate"]);

	$benchmark->point('Validation');
		$factory = new Core_Validation_Factory(Core_Validation_Factory::VALIDATION_TYPE_CREATE);
	    $validation = $factory->getValidationObject($woData);

        if ($validation->validate() === false) {
            $errors->addErrors('6', $validation->getErrors());
			return API_Response::fail();
        }

	$benchmark->point('Validation Done');

        date_default_timezone_set('US/Central');
        $woData['DateEntered'] = date('Y-m-d H:i:s');

	if (!empty($woData["StartDate"])) {
		// auto calc sourced by date and auto mark short notice
			$numWeekDays = $this->numberWeekDays(date("Y-m-d"), $woData["StartDate"]);

		if ($numWeekDays <= 5)
			$woData["ShortNotice"] = "True";

		if ($woData["Type_ID"] == "1") {

				$startDateTS = strtotime(date("Y-m-d"));
			$endDateTS = strtotime($woData["StartDate"]);

			$sourceByDateTS = $this->calculateSourceByDate($startDateTS, $endDateTS, $woData['DateEntered'], $numWeekDays);

				$woData["SourceByDate"] = date("Y-m-d", $sourceByDateTS);
		}
	}


        $benchmark->point('Geocode');
        if (empty($woData['Latitude']) || empty($woData['Longitude'])) {
			$geocode = new Core_Geocoding_Google();
			if (!empty($woData['Address']))
				$geocode->setAddress($woData['Address']);
			if (!empty($woData['City']))
				$geocode->setCity($woData['City']);
			if (!empty($woData['Zipcode']))
				$geocode->setZip($woData['Zipcode']);
			$woData['Latitude'] = $geocode->getLat();
			$woData['Longitude'] = $geocode->getLon();
	    }

        $benchmark->point('Geocode done');

	    if (!empty($woData['Approved']) && $woData['Appoved']=="True" && $woData['Invoiced']=="False") {
    		$woData['DateApproved'] = $woData['DateEntered'];
    		$woData['approvedBy'] = $user->getLogin();
	    }
	    if (!empty($woData['Deactivated']) && $woData['Deactivated']=='True') {
            $woData['DeactivatedDate'] = date('Y-m-d H:i:s');
            $woData['DeactivatedBy'] = $user->getLogin();
        }

		if (!empty($woData['TechMarkedComplete']) && $woData['TechMarkedComplete']=="True")
            $woData['Date_Completed'] = $curTime;

		$woData['lastModDate'] = date('Y-m-d H:i:s');
		$woData['lastModBy'] = $user->getLogin();

        $fieldsList = implode(',',array_keys($woData));

        $benchmark->point('Inserting WO');
        $db = Core_Database::getInstance();

		// Update Status Field
		$woData["Status"] = Core_WorkOrder_Status::getStatusWithWorkOrder($woData);

		$result = false;
		$result = Core_Database::insert(Core_Database::TABLE_WORK_ORDERS, $woData);

        $benchmark->point('Inserting WO done');
	if (empty($woData["Project_Name"]))
		$woData["Project_Name"] = "";

	if (!$result) {
    		$err = $errors->getErrors();
    		if (!empty($err))
				return API_Response::fail();

		    $errors->addError(7, 'Incorrect SQL request');
			return API_Response::fail();
		}

		$benchmark->point('Time stamps');
       	Core_TimeStamp::createTimeStamp($wo_id, $user->getLogin(), "Work Order Created", $woData['Company_ID'], $woData['Company_Name'], $woData['Project_Name'], "", "", "");

       	if (!empty($woData['Tech_ID'])) {
			$mail  = new Core_EmailForAssignedTech();
			$mail->setWOData($woData);
			$mail->setTech($woData['Tech_ID']);
			$mail->setWorkOrder($wo_id);
			$mail->setProject($woData['Project_ID']);
			$mail->setCompanyId($woData['Company_ID']);
			$mail->setCompanyName($woData['Company_Name']);
			$mail->sendMail();
			Core_TimeStamp::createTimeStamp($wo_id, $user->getLogin(), "Work Order Assigned", $woData['Company_ID'], $woData['Company_Name'], $woData['Project_Name'], "", "", "");
                        $NoticeClass = new Core_Api_NoticeClass();
                        $NoticeClass->createWOChangeStatusLog($wo_id,Core_Api_NoticeClass::WOStatusID_FOR_PUBLISHED);
		}

       	if (!empty($woData['ShowTechs']) && $woData['ShowTechs'] == "True"){
			Core_TimeStamp::createTimeStamp($wo_id, $user->getLogin(), "Work Order Published", $woData['Company_ID'], $woData['Company_Name'], $woData['Project_Name'], "", "", "");
            $NoticeClass = new Core_Api_NoticeClass();
            $ret = $NoticeClass->createWOChangeStatusLog($wo_id,Core_Api_NoticeClass::WOStatusID_FOR_ASSIGNED);
        }


        if (!empty($woData['ShowTechs']) && $woData['ShowTechs'] == "True" && (empty($woData['Deactivated']) || $woData['Deactivated'] == "False") && empty($woData['Tech_ID'])) {
    		    $mail = new Core_BlastRecruitmentEmail();
    		    $mail->setWorkOrder($wo_id);
    		    $mail->setCompanyId($woData['Company_ID']);
    		    $mail->sendMail();
		    if ($woData['isProjectAutoAssign']=='True') {
		    	$this->createAtJob($wo_id);
		    }
        }
        if (!empty($woData['WorkOrderReviewed']) && $woData['WorkOrderReviewed']=="True") {
          	Core_TimeStamp::createTimeStamp($wo_id, $user->getLogin(), "Work Order Reviewed", $woData['Company_ID'], $woData['Company_Name'], $woData['Project_Name'], "", "", "");
        }
        if (!empty($woData['TechCheckedIn_24hrs']) && $woData['TechCheckedIn_24hrs']=="True") {
          	Core_TimeStamp::createTimeStamp($wo_id, $user->getLogin(), "Work Order Confirmed", $woData['Company_ID'], $woData['Company_Name'], $woData['Project_Name'], "", "", "");
        }
        if (!empty($woData['TechMarkedComplete']) && $woData['TechMarkedComplete']=="True") {
	        Core_TimeStamp::createTimeStamp($wo_id, $user->getLogin(), "Work Order Marked: Completed", $woData['Company_ID'], $woData['Company_Name'], $woData['Project_Name'], "", "", "");
                $NoticeClass = new Core_Api_NoticeClass();
                $NoticeClass->createWOChangeStatusLog($wo_id,Core_Api_NoticeClass::WOStatusID_FOR_WORKDONE);
			if ($updatedFields["Approved"] == "False") {
//				$techPayCalculator = new Core_TechPay();
//				$techPayCalculator->setWinNum($wo_id);
//				$techPayCalculator->updateTechPay();
			}
        }
        if (!empty($woData['Approved']) && $woData['Approved']=="True" && $woData['Invoiced']=="False") {
	        Core_TimeStamp::createTimeStamp($wo_id, $user->getLogin(), "Work Order Approved", $woData['Company_ID'], $woData['Company_Name'], $woData['Project_Name'], "", "", "");
                $NoticeClass = new Core_Api_NoticeClass();
                $NoticeClass->createWOChangeStatusLog($wo_id,Core_Api_NoticeClass::WOStatusID_FOR_APPROVED);
	        $mail = new Core_EmailForApprovedTech();
			$mail->setCompanyId($woData['Company_ID']);
            $mail->setCompanyName($woData['Company_Name']);
            $mail->setTech($woData['Tech_ID']);
            $mail->setWorkOrder($wo_id);
            $mail->setProject($woData['Project_ID']);
            $mail->sendMail();
        }

        if (!empty($woData['Deactivated']) && $woData['Deactivated']=="True") {
	        Core_TimeStamp::createTimeStamp($wo_id, $user->getLogin(), "Work Order Deactivated", $woData['Company_ID'], $woData['Company_Name'], $woData['Project_Name'], "", "", "");
        }
        if (!empty($woData['AbortFee']) && $woData['AbortFee']=="True") {
	        Core_TimeStamp::createTimeStamp($wo_id, $user->getLogin(), "Abort Fee Checked", $woData['Company_ID'], $woData['Company_Name'], $woData['Project_Name'], "", "", "");
        }

	$benchmark->point('Time stamps done');
		$devicesText = "";
		if (!empty($woData["Amount_Per"]) && $woData["Amount_Per"] == "Device") {
			$devicesText = "
			Devices: {$woData["Qty_Devices"]}";
		}

	$benchmark->point('Emails');

		//$startDate = $woData["StartDate"];
		if (!empty($woData["StartDate"]))
			$startDate = date("m/d/Y", strtotime($woData["StartDate"]));
		else
			$startDate = NULL;
		$newWOMail = new Core_Mail();
		$newWOMail->setFromName("Work Order Created");
		$newWOMail->setFromEmail("nobody@fieldsolutions.com");
		$newWOMail->setToName("NewWOsNote");
		$newWOMail->setToEmail("NewWOsNote@fieldsolutions.com");
		$newWOMail->setSubject("New $company WO - {$woData["Project_Name"]}");
		@$newWOMail->setBodyText("
			WorkOrderID: {$woData["WO_ID"]}
			Username: {$woData["Username"]}
			Max Pay: {$woData["PayMax"]}
			Amount Per: {$woData["Amount_Per"]} $devicesText
			Start Date: $startDate
			Start Time: {$woData["StartTime"]}
			Project Name: {$woData["Project_Name"]}
			Address: {$woData["Address"]}
			City: {$woData["City"]}
			State: {$woData["State"]}
			Zip: {$woData["Zipcode"]}

			Description:

			{$woData["Description"]}

			Special Instructions:

			{$woData["SpecialInstructions"]}");

		$newWOMail->send();
		$benchmark->point('Emails done');

		return API_Response::success(array(new API_AdminWorkOrder($wo_id)));
	}

    /**
	 * download clients file from S3
	 *
	 * @param string $fileName
	 * @return API_Response
     * @author Alex Che
	 */
    static public function getClientFile($fileName)
    {
        $errors = Core_Api_Error::getInstance();
        // get file from S3
        $file = new Core_File();
        $file = $file->downloadFile($fileName, S3_CLIENTS_LOGO_DIR);

        if ($file === false) $errors->addError(6, 'File not exists');

		$err = $errors->getErrors();
		if (!empty($err))
			return API_Response::fail();

        return $file;
    }

	/**
	 * update Work Order
	 *
	 * @param string $user
	 * @param string $pass
	 * @param int $winNum
	 * @param API_AdminWorkOrder $workOrder
	 * @return API_Response
	 */
	public function updateWorkOrder($user, $pass, $winNum, $workOrder, $calledFromWMPoll = false)
	{

	//error_log("**** PRE: " . print_r($workOrder,true));
		if (is_string($workOrder)) $woXML = $workOrder;
		else {
			$dom = new DOMDocument("1.0");
			$dom->appendChild($workOrder->toXMLElement($dom));
			$woXML = $dom->saveXML();
		}



       	$logger = Core_Api_Logger::getInstance();
		$logger->setUrl('updateWorkOrder ' . $winNum);
        $logger->setLogin($user);
		$logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');

		$authData = array('login'=>$user, 'password'=>$pass);

		$user = new Core_Api_AdminUser();
		$user->checkAuthentication($authData);

		if (!$user->isAuthenticate())
			return API_Response::fail();

		$params = array();
		$params['TB_UNID'] = $winNum;
		$params['xml'] = $woXML;

		$errors = Core_Api_Error::getInstance();
		if (empty($params['TB_UNID']))
			$errors->addError(2, 'TB_UNID parameter is empty but required');

		if (empty($params['xml']))
			$errors->addError(2, 'XML parameter is empty but required');

		$err = $errors->getErrors();
		if (!empty($err))
			return API_Response::fail();

		$woData = $this->getDataFromXML($params['xml']);

		return $this->updateWorkOrderPostXMLParse($user, $params, $woData, $calledFromWMPoll);
	}

	/**
	 * update Work Orders
	 *
	 * @param string $user
	 * @param string $pass
	 * @param string $timeStampFrom
	 * @param string $timeStampTo
	 * @param string $timeStampType
	 * @param array $criteria
	 * @param API_AdminWorkOrder $workOrder
	 * @return API_Response
	 */
	private function updateWorkOrders($user, $pass, $timeStampFrom, $timeStampTo, $timeStampType, $criteria, $workOrder)
	{
		if (is_string($workOrder)) $woXML = $workOrder;
		else {
			$dom = new DOMDocument("1.0");
			$dom->appendChild($workOrder->toXMLElement($dom));
			$woXML = $dom->saveXML();
		}

		$authData = array('login'=>$user, 'password'=>$pass);
		$errors = Core_Api_Error::getInstance();

	    $logger = Core_Api_Logger::getInstance();
        $logger->setLogin($companyID);
	    $logger->setUrl('updateWorkOrders');

		$userObj = new Core_Api_AdminUser();
		$userObj->checkAuthentication($authData);

		if (!$userObj->isAuthenticate())
			return API_Response::fail();
        $companyID = $userObj->getContextCompanyId();

		$workOrders = API_AdminWorkOrderFilter::filter($companyID,
														'WIN_NUM',
														$timeStampFrom,
														$timeStampTo,
														$timeStampType,
														$criteria);
		$woData = $this->getDataFromXML($woXML);

		$updates = '<WOS>';

		try { //API_AdminWorkOrderFilter::filter return Core_Filter_Result that implements Traversable. So it fetch data in foreach
			foreach ($workOrders as $workOrder)
			{
				$this->updateWorkOrderPostXMLParse($authData, $workOrder, $woData);
				$updates .= '<WO><WIN_NUM>' . $workOrder['TB_UNID'] . '</WIN_NUM></WO>';

				$err = $errors->getErrors();
				if (!empty($err))
				{
					return API_Response::fail();
				}
			}
		} catch (Exception $e) {
			$errors->addError(7, 'SQL Error: '. $e);
			error_log($e);
			return API_Response::fail();
		}

		$updates .= '</WOS>';
		return $updates;
	}

	private function updateWorkOrderPostXMLParse($user, $params, $woData, $calledFromWMPoll = false) {
	    $errors = Core_Api_Error::getInstance();

		$wo_id = trim($params['TB_UNID'],"`'");

		$db = Core_Database::getInstance();
		$fieldListMap = Core_Database::getFieldList(Core_Database::TABLE_WORK_ORDERS);
		$fieldListMap = array_combine($fieldListMap, $fieldListMap);
		$select = $db->select();
		$select->from(Core_Database::TABLE_WORK_ORDERS, $fieldListMap)
			->where("WIN_NUM = ?", $wo_id);

		$curWO = Core_Database::fetchAll($select);
		
		$select = $db->select();
		$select->from('work_orders_additional_fields', array('PushWM'))
			->where("WINNUM = ?", $wo_id);
		$workOrderAdditionalFieldsTemp = $db->fetchRow($select);

        if (empty($curWO[0])) {
            $err = $errors->getErrors();
    		if (!empty($err))
				return API_Response::fail();

        	$errors->addError(7, 'Work Order id '.$wo_id.' does not exist');
			return API_Response::fail();
        }
        $curWO = $curWO[0];

		$fieldListType = Core_Database::getFieldListType(Core_Database::TABLE_WORK_ORDERS);

		// TODO Disables By Alex Che. Broke Status fields saving
//ob_start();
//var_dump($woData["TechMarkedComplete"]);
//$a=ob_get_contents();
//ob_end_clean();
		//error_log("**** VD: $a");
		$woData = Core_Database::convertQueryResult($woData, $fieldListType);

		//error_log("**** WODATA: " . print_r($woData,true));

		$curWO = Core_Database::convertQueryResult($curWO, $fieldListType);


		if (array_key_exists("StartDate", $woData))
			$woData["StartDate"] = $this->blankIfInvalidDate($woData["StartDate"]);
		if (array_key_exists("EndDate", $woData))
			$woData["EndDate"] = $this->blankIfInvalidDate($woData["EndDate"]);
		$techData = NULL;
		if (!empty($woData["Tech_ID"]))
			$techData = $this->pullAssignTechInfo($user, $woData["Tech_ID"]);
		else if (!empty($woData["FLS_ID"]))
			$techData = $this->pullAssignTechInfo($user, $woData["FLS_ID"], true);
		
		unset($woData["PushWM"]);
    	// exclude readonly fields
        $this->excludeFields($woData);

        $updatedFields = array();
        foreach ($curWO as $key=>$val) {
            if (isset($woData[$key]) && $woData[$key]!==null) {
                $updatedFields[$key] = $woData[$key];
            } else {
                $updatedFields[$key] = $val;
            }
        }

		//error_log("**** UPDATED: " . print_r($updatedFields,true));

		//13454 - admin interface must update net pay when changing percent deduct
		if (isset ($updatedFields["PcntDeductPercent"])) {
			$payFieldUpdated = false;
			$payFields = Core_TechPay::recalcFields();
			$payFields = array_combine($payFields, $payFields);
			foreach ($woData as $key=>$val) {
				if (array_key_exists($key, $payFields) && $curWO[$key] != $updatedFields[$key])
					$payFieldUpdated = true;
			}
			if (!empty($woData['Total_Reimbursable_Expense'])) {
				$payFieldUpdated = true;
			}

			$payData = NULL;

            // must be done before exclude of read only field otherwise we loose access to these fields
            $techPayCalculator = new Core_TechPay();
            //376
            $updatedFields['Total_Reimbursable_Expense'] = $woData['Total_Reimbursable_Expense'];
            
            $payData = $techPayCalculator->updateTechPay($updatedFields);
            unset($updatedFields['Expensethrough']);
            //376
	        unset($woData['Expensethrough']);

	        $isProjectAutoAssign = $updatedFields["isProjectAutoAssign"];
	        $this->excludeFields($updatedFields);
	        $updatedFields["isProjectAutoAssign"] = $isProjectAutoAssign;

	        if ($payData != NULL) {
	            // restore tech pay info excluded
	            $updatedFields["PayAmount"] = $payData ["PayAmount"];
	            $updatedFields["calculatedTechHrs"] = $payData ["calculatedTechHrs"];
	            $updatedFields["baseTechPay"] = $payData ["baseTechPay"];
	            $updatedFields["Net_Pay_Amount"] = $payData ["Net_Pay_Amount"];
				$updatedFields["Z2T_Pay_Amount"] = $payData ["Z2T_Pay_Amount"];
	            $updatedFields["PricingRan"] = "False";
	        }
		}

		/*$payFieldUpdated = false;

		$payFields = Core_TechPay::recalcFields();
		$payFields = array_combine($payFields, $payFields);
		foreach ($woData as $key=>$val) {
			if (array_key_exists($key, $payFields) && $curWO[$key] != $updatedFields[$key])
				$payFieldUpdated = true;
		}

		$payData = NULL;
        if ( (($updatedFields['TechMarkedComplete'] == "True") || ($updatedFields['Approved'] == "True" && $curWO['Approved'] == "False")) && (empty($updatedFields['PayAmount']) || $updatedFields['PayAmount'] == 0.00 || $payFieldUpdated)) {
			// must be done before exclude of read only field otherwise we loose access to these fields
			//
		 	//* REMOVED FROM MYSQL VERSION. REWRITE TO SQL
			// * $techPayCalculator = new Core_TechPay();
			//$payData = $techPayCalculator->updateTechPay($updatedFields);

		}

        $this->excludeFields($updatedFields);

		if ($payData != NULL) {
			// restore tech pay info excluded
			$updatedFields["PayAmount"] = $payData ["PayAmount"];
			$updatedFields["calculatedTechHrs"] = $payData ["calculatedTechHrs"];
			$updatedFields["baseTechPay"] = $payData ["baseTechPay"];
			$updatedFields["Net_Pay_Amount"] = $payData ["Net_Pay_Amount"];
		}
*/
		if (!empty($woData["Project_Name"]) && $curWO["Project_Name"] != $updatedFields["Project_Name"]) {
			// clear project id if project name is set
			$updatedFields["Project_ID"] = "";
		}

/*
 * // disabled not working validation
        // If invalid field is passed
        $exceptionConverter = new Core_Caspio_ExceptionConverter();
		if (!$exceptionConverter->columnValidate(Core_Database::TABLE_WORK_ORDERS, array_keys($woData))){
            $errors->addError(6, 'Invalid field "'.$exceptionConverter->getNotValidColumn().'"');
			return API_Response::fail();
        }
*/

		if ($techData != NULL) {
			// use already pulled in tech data
			$techData = $techData[0];
			$updatedFields["Tech_FName"] = $techData["FirstName"];
			$updatedFields["Tech_LName"] = $techData["LastName"];
			$updatedFields["TechEmail"] = $techData["PrimaryEmail"];
			$updatedFields["TechPhone"] = $techData["PrimaryPhone"];
			if (!empty($techData["PrimaryPhoneExt"]))
				$updatedFields["TechPhoneExt"] = $techData["PrimaryPhoneExt"];
			else
				$updatedFields["TechPhoneExt"] = NULL;
			if (!empty($techData["IsAdminDenied"])) {
		        	$errors->addError(2, "You are about to assign work to a technician that has been \"denied access\" by your company. Please re-confirm your choice of technician. Tech ID: " . $updatedFields["Tech_ID"] . " (" . $updatedFields["Tech_FName"] . " " . $updatedFields["Tech_LName"] . ")");
				return API_Response::fail();
			}
			if (!empty($techData["FLS_ID"]) && $techData["FLS_ID"] != 'NULL')
				$updatedFields["FLS_ID"] = trim($techData["FLS_ID"]);
			else {
				$updatedFields["FLS_ID"] = NULL;
				if ($curWO['Company_ID'] == "FLS") {
					$errors->addError(2, "You are about to assign work to a technician that does not have a FLS Badge. Please re-confirm your choice of technician. Tech ID: " . $updatedFields["Tech_ID"] . " (" . $updatedFields["Tech_FName"] . " " . $updatedFields["Tech_LName"] . ")");
					return API_Response::fail();
				}
			}
			if (strtolower($techData["AcceptTerms"]) != "yes" && ($techData["AcceptTerms"] != 'WM' || $workOrderAdditionalFieldsTemp['PushWM'] != 1)) {
	        		$errors->addError(2, "The technician you're attempting to assign has not accepted terms of usage.  Sorry for any issues this may cause you. Tech ID: " . $updatedFields["Tech_ID"] . " (" . $updatedFields["Tech_FName"] . " " . $updatedFields["Tech_LName"] . ")");
				return API_Response::fail();
			}
						
			$WMClass = new Core_WorkMarket($wo_id);
			if (!$calledFromWMPoll && $workOrderAdditionalFieldsTemp['PushWM'] == 1 && $techData["AcceptTerms"] != 'WM' && !$WMClass->prepareToAssignInFS()) {
        		$errors->addError(2, "This work order was already assigned in Work Market");
				return API_Response::fail();
			}
		}

        $combiner = new Core_Combiner_WorkOrder($updatedFields);
		$combiner->setCompanyId($curWO["Company_ID"]);
		$combiner->setCompanyName($curWO["Company_Name"]);
		$combiner->setUserName($user->getLogin());
        if ($combiner->combine($curWO)) {
			$updatedFields = $combiner->getData();
		} else {
		    $errors->addErrors('6', $combiner->getErrors());
			return API_Response::fail();
		}

		unset($updatedFields['PushWM']);
		//error_log("**** Combiner: " . print_r($updatedFields,true));
		/*$factory = new Core_Validation_Factory(Core_Validation_Factory::VALIDATION_TYPE_UPDATE);
	    $validation = $factory->getValidationObject($updatedFields);

	    if ($validation->validate() === false) {
            $errors->addErrors('6', $validation->getErrors());
			return API_Response::fail();
        }*/

	    /*if (!empty($updatedFields["Tech_ID"]) && $updatedFields["TechMarkedComplete"] == "False" && $curWO["TechMarkedComplete"] == "True" && empty($updatedFields["MissingComments"])) {
			$errors->addError(2, 'MissingComments parameter is empty but required');
			return API_Response::fail();
		}*/

		date_default_timezone_set('US/Central');
		$curTime = date('Y-m-d H:i:s');

		if ($updatedFields['TechMarkedComplete']=="True" && $curWO['TechMarkedComplete']=="False")
            $updatedFields['Date_Completed'] = $curTime;

		$updatedFields['lastModDate'] = date('Y-m-d H:i:s');
		$updatedFields['lastModBy'] = $user->getLogin();

		// End field validation. Now only clean up fields before updating

		$projectInfo = NULL;
		if (!empty($updatedFields['Tech_ID'])) {
			$projectInfo = new Core_Project((int)$updatedFields["Project_ID"]);
			$projectInfo->setData();
			if (strtoupper($projectInfo->getFixedBid()) == "TRUE")
				$updatedFields["Tech_Bid_Amount"] = $updatedFields["PayMax"];
		}

        $changedFields = false;
        if (!empty($updatedFields['Tech_ID'])){
            $bufArr = array('StartDate','StartTime','SiteAddress','SiteCity','SiteState','SiteZip','PayAmount',
                            'SpecialInstructions','BaseTechPay','OutOfScope','TripCharge','MileageCharge',
                            'MaterialsCharge','AbortFeeAmount','Additional_Pay_Amount','Add_Pay_Reason','lastModBy');
            $woChangedMail = new Core_Mail_WoStatus_Change();
			$woChangedMail->assign($key, $updatedFields["Approved"]);

            foreach ($curWO as $key=>$val) {
                if (!empty($updatedFields[$key]) && $val != $updatedFields[$key]) {
                    if (in_array($key,$bufArr)) {
                        $woChangedMail->assign($key, $updatedFields[$key]);
                    	$changedFields = true;
                    }
                }
            }
        }

        if (empty($curWO['Latitude']) || empty($curWO['Longitude']) || $updatedFields['Zipcode']!=$curWO['Zipcode'] ||
            $updatedFields['Address']!=$curWO['Address'] || $updatedFields['City']!=$curWO['City']) {
			$geocode = new Core_Geocoding_Google();
			$geocode->setAddress($updatedFields['Address']);
			$geocode->setCity($updatedFields['City']);
			$geocode->setZip($updatedFields['Zipcode']);
			$updatedFields['Latitude'] = $geocode->getLat();
			$updatedFields['Longitude'] = $geocode->getLon();
		}

		if (!empty($updatedFields["StartDate"]) && $updatedFields['StartDate'] != $curWO['StartDate']) {
			// auto calc sourced by date and auto mark short notice
			$numWeekDays = $this->numberWeekDays(date("Y-m-d"), $updatedFields["StartDate"]);

			if ($numWeekDays <= 5)
				$updatedFields["ShortNotice"] = "True";

			if ($updatedFields["Type_ID"] == "1") {
				$startDateTS = strtotime(date("Y-m-d"));
				$endDateTS = strtotime($updatedFields["StartDate"]);

				$sourceByDateTS = $this->calculateSourceByDate($startDateTS, $endDateTS, $curWO['DateEntered'], $numWeekDays);

				$updatedFields["SourceByDate"] = date("Y-m-d", $sourceByDateTS);
			}
		}

		if ($updatedFields['Approved']=="True") {
        	$updatedFields['TechMarkedComplete'] = 'True';
			if ($curWO['Approved'] == "False") {
				$updatedFields['DateApproved'] = date('Y-m-d H:i:s');
				$updatedFields['approvedBy'] = $user->getLogin();
			}
		}
		else {
			// clear approve fields
			$updatedFields['DateApproved'] = NULL;
			$updatedFields['approvedBy'] = "";
		}

		$requireACSReset = $updatedFields['Tech_ID'] != $curWO['Tech_ID'] || $updatedFields['StartDate'] != $curWO['StartDate'] || $updatedFields['StartTime'] != $curWO['StartTime'];
		$woChangedToPublished = $updatedFields["ShowTechs"] == "True" && $updatedFields["ShowTechs"] != $curWO["ShowTechs"];
		$techAssignedOrReassigned = $updatedFields['Tech_ID'] != $curWO['Tech_ID'] && !empty($updatedFields['Tech_ID']);
		$woChangedToApproved = $updatedFields["Approved"] == "True" && $updatedFields["Approved"] != $curWO["Approved"];

		if ($updatedFields['Deactivated']=='True' && $curWO['Deactivated']=='False') {
			$updatedFields['DeactivatedDate'] = date('Y-m-d H:i:s');
            $updatedFields['DeactivatedBy'] = $user->getLogin();
        }

		if (empty($updatedFields['Tech_ID'])) {
			$updatedFields['Tech_ID'] = NULL;
        	$updatedFields['Date_Assigned'] = "";
			$updatedFields["Tech_FName"] = "";
			$updatedFields["Tech_FName"] = "";
			$updatedFields["Tech_LName"] = "";
			$updatedFields["TechEmail"] = "";
			$updatedFields["TechPhone"] = "";
			$updatedFields["Tech_Bid_Amount"] = NULL;
			$updatedFields["FLS_ID"] = NULL;
		}

		// End field clean up, now updating

		// Update Status Field
		$updatedFields["Status"] = Core_WorkOrder_Status::getStatusWithWorkOrder($updatedFields);

		$result = false;
		$result = Core_Database::update(Core_Database::TABLE_WORK_ORDERS, $updatedFields, $db->quoteInto("WIN_NUM = ?", $wo_id));

		$changed = array();
		$updatedFieldsConverted = Core_Database::convertQueryResult($updatedFields, $fieldListType);
		foreach ($updatedFieldsConverted as $key=>$value) {
			if ($value == $curWO[$key]) continue;
			$changed[] = $key;
		}
		$changed = implode(",", $changed);
		Core_TimeStamp::createTimeStamp($wo_id, $user->getLogin(), "Work Order Updated: $changed", $updatedFields["Company_ID"], $updatedFields["Company_Name"], $updatedFields['Project_Name'], "", "", "");

		if (!$result) {
    		$err = $errors->getErrors();
    		if (!empty($err))
				return API_Response::fail();

		    $errors->addError(7, 'Incorrect SQL request');
			return API_Response::fail();
		}
		
		if ($obsync = Core_WorkOrder_OutboundSync_Factory::build($curWO["Company_ID"])) {
			$obsync->syncWithWorkOrder($wo_id, $curWO, $updatedFields, Core_WorkOrder_OutboundSync_Abstract::CONTEXT_ADMIN, $user->getLogin());
		}

		// End Updating, now send emails and do timestamps

		$techInfo = NULL;
		if (!empty($updatedFields['Tech_ID']))

		if (!empty($updatedFields['Tech_ID']) && $changedFields) {
			$techInfo = new Core_Tech((int)$updatedFields["Tech_ID"]);
			if ($projectInfo === NULL) {
				$projectInfo = new Core_Project((int)$updatedFields["Project_ID"]);
				$projectInfo->setData();
			}
			$woChangedMail->setTech($techInfo);
			$woChangedMail->setProject($projectInfo);
//			$woChangedMail->loadProject($updatedFields['Project_ID']);
//            $woChangedMail->loadTech($updatedFields['Tech_ID']);
            $woChangedMail->assign('WorkOrderNo', $updatedFields['WO_ID']);
            $woChangedMail->assign('TB_UNID', $wo_id);
            $woChangedMail->assign('Approved', $updatedFields['Approved']);

            $woChangedMail->send();
		}

        if ($woChangedToPublished && $updatedFields['Deactivated'] == "False" && empty($updatedFields['Tech_ID'])) {
//            if ($updatedFields['AutoBlastOnPublish'] == "False" && !empty($updatedFields['Zipcode'])) {
    		    $mail = new Core_BlastRecruitmentEmail();
    		    $mail->setWorkOrder($wo_id);
    		    $mail->setCompanyId($updatedFields["Company_ID"]);
    		    $mail->sendMail();
//		    }
		    if ($updatedFields['isProjectAutoAssign']=='True') {
		    	$this->createAtJob($wo_id);
		    }
         	Core_TimeStamp::createTimeStamp($wo_id, $user->getLogin(), "Work Order Published", $updatedFields["Company_ID"], $updatedFields["Company_Name"], $updatedFields['Project_Name'], "", "", "");
                $NoticeClass = new Core_Api_NoticeClass();
                $NoticeClass->createWOChangeStatusLog($wo_id,Core_Api_NoticeClass::WOStatusID_FOR_PUBLISHED);
        }

		// Missing wosEmailTechWhenCompleteUnchecked?

        if ($techAssignedOrReassigned) {
            $mail  = new Core_EmailForAssignedTech();
		$mail->setWOData($updatedFields);
			$mail->setTech($updatedFields['Tech_ID']);
            $mail->setWorkOrder($wo_id);
            $mail->setProject($updatedFields['Project_ID']);
            $mail->setCompanyId($updatedFields["Company_ID"]);
            $mail->setCompanyName($updatedFields["Company_Name"]);
            $mail->sendMail();
          	Core_TimeStamp::createTimeStamp($wo_id, $user->getLogin(), "Work Order Assigned", $updatedFields["Company_ID"], $updatedFields["Company_Name"], $updatedFields['Project_Name'], "", "", "");
                $NoticeClass = new Core_Api_NoticeClass();
            $ret = $NoticeClass->createWOChangeStatusLog($wo_id,Core_Api_NoticeClass::WOStatusID_FOR_ASSIGNED);
        }

		if ($requireACSReset) {
			Core_IVR_Redial::resetCalls($wo_id);
			Core_IVR_Log::resetCalls($wo_id);
            Core_TimeStamp::createTimeStamp($wo_id, $user->getLogin(), "Reset ACS Calls", $updatedFields["Company_ID"], $updatedFields["Company_Name"], $updatedFields['Project_Name'], "", "", "");
            Core_TimeStamp::createWOACSTimeStamp($wo_id,$updatedFields["Company_ID"]);
        }

        if (!empty($updatedFields['WorkOrderReviewed']) && $updatedFields['WorkOrderReviewed']=='True' && $curWO['WorkOrderReviewed']=='False') {
          	Core_TimeStamp::createTimeStamp($wo_id, $user->getLogin(), "Work Order Reviewed", $updatedFields["Company_ID"], $updatedFields["Company_Name"], $updatedFields['Project_Name'], "", "", "");
        }

        if (!empty($updatedFields['TechCheckedIn_24hrs']) && $updatedFields['TechCheckedIn_24hrs']=='True' && $curWO['TechCheckedIn_24hrs']=='False') {
          	Core_TimeStamp::createTimeStamp($wo_id, $user->getLogin(), "Work Order Confirmed", $updatedFields["Company_ID"], $updatedFields["Company_Name"], $updatedFields['Project_Name'], "", "", "");
        }

        if (!empty($updatedFields["Tech_ID"]) && $updatedFields['TechMarkedComplete'] != $curWO['TechMarkedComplete']) {
			$val = $updatedFields['TechMarkedComplete']=='True' ? "Completed" : "Not Completed";
	        Core_TimeStamp::createTimeStamp($wo_id, $user->getLogin(), "Work Order Marked: $val", $updatedFields["Company_ID"], $updatedFields["Company_Name"], $updatedFields['Project_Name'], "", "", "");
                if($val == 'Completed'){
                    $NoticeClass = new Core_Api_NoticeClass();
                    $NoticeClass->createWOChangeStatusLog($wo_id,Core_Api_NoticeClass::WOStatusID_FOR_WORKDONE);
                }
			if ($updatedFields['TechMarkedComplete'] == "False") {
				$updatedFields['DateIncomplete'] = date('Y-m-d H:i:s');
		        Core_TimeStamp::createTimeStamp($wo_id, $user->getLogin(), "Work Order Marked Incomplete", $updatedFields["Company_ID"], $updatedFields["Company_Name"], $updatedFields['Project_Name'], "", "", "");
	            $woIncompleteMail = new Core_Mail_WoStatus_Incomplete();
				if ($techInfo == NULL)
					$techInfo = new Core_Tech((int)$updatedFields["Tech_ID"]);
				if ($projectInfo == NULL) {
					$projectInfo = new Core_Project((int)$updatedFields["Project_ID"]);
					$projectInfo->setData();
				}
		        $woIncompleteMail->assign('TB_UNID', $wo_id);
		        $woIncompleteMail->assign('WorkOrderNo', $updatedFields["WO_ID"]);
		        $woIncompleteMail->assign('missingInfo', $updatedFields["MissingComments"]);

				$woIncompleteMail->setTech($techInfo);
				$woIncompleteMail->setProject($projectInfo);

	            $woIncompleteMail->send();

			}
			else if (!empty($updatedFields['Approved']) && $updatedFields["Approved"] == "False") {
//				$techPayCalculator = new Core_TechPay();
//				$techPayCalculator->setWinNum($wo_id);
//				$techPayCalculator->updateTechPay();
			}
        }

        if ($woChangedToApproved) {
	        Core_TimeStamp::createTimeStamp($wo_id, $user->getLogin(), "Work Order Approved", $updatedFields["Company_ID"], $updatedFields["Company_Name"], $updatedFields['Project_Name'], "", "", "");
                $NoticeClass = new Core_Api_NoticeClass();
                $NoticeClass->createWOChangeStatusLog($wo_id,Core_Api_NoticeClass::WOStatusID_FOR_APPROVED);
            $mail = new Core_EmailForApprovedTech();
			$mail->setCompanyId($updatedFields["Company_ID"]);
            $mail->setCompanyName($updatedFields["Company_Name"]);
            $mail->setTech($updatedFields['Tech_ID']);
            $mail->setWorkOrder($wo_id);
            $mail->setProject($updatedFields['Project_ID']);
            $mail->sendMail();
        }

        if (!empty($updatedFields['Deactivated']) && $updatedFields['Deactivated'] == "True" && $curWO['Deactivated'] == "False") {
	        Core_TimeStamp::createTimeStamp($wo_id, $user->getLogin(), "Work Order Deactivated", $updatedFields["Company_ID"], $updatedFields["Company_Name"], $updatedFields['Project_Name'], "", "", "");
        }

        if (!empty($updatedFields['AbortFee']) && $updatedFields['AbortFee'] != $curWO['AbortFee']) {
	        Core_TimeStamp::createTimeStamp($wo_id, $user->getLogin(), "Abort Fee ". ($updatedFields['AbortFee']=='True'?'Checked':'Un-checked'), $updatedFields["Company_ID"], $updatedFields["Company_Name"], $updatedFields['Project_Name'], "", "", "");
        }

		return API_Response::success(array(new API_AdminWorkOrder($wo_id)));
	}


	/**
	 * Function get list of WO Categories
	 *
	 * @param string $user
	 * @param string $pass
	 * @return API_Response
	 */
	public function getWOCategories($user, $pass)
	{
            $logger = Core_Api_Logger::getInstance();
            $logger->setUrl('getWOCategories');
            $logger->setLogin($user);
            $logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');

            $errors = Core_Api_Error::getInstance();

            $authData = array('login'=>$user, 'password'=>$pass);
            $user = new Core_Api_AdminUser();
            $user->checkAuthentication($authData);
            if (!$user->isAuthenticate()) return API_Response::fail();

            $fieldsList = 'Category_ID,Category';

            $db = Core_Database::getInstance();
            $select = $db->select();

            $select->from(Core_Database::TABLE_WO_CATEGORIES, explode(',', $fieldsList));
            $select->where("Category_ID !='11'");//13102	
            $result = Core_Database::fetchAll($select);

            $err = $errors->getErrors();
            if (!empty($err))
                    return API_Response::fail();

            return API_Response::success($this->makeWOCategoryArray($result));
	}
        /**
	 * Function get list of WO Categories
	 *
	 * @param string $user
	 * @param string $pass
	 * @return API_Response
	 */
	public function getCompanies($user, $pass, $fields = null, $sort = null, $group = null)
	{
            $logger = Core_Api_Logger::getInstance();
            $logger->setUrl('getProjects');
            $logger->setLogin($user);
            $logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');

            $errors = Core_Api_Error::getInstance();

            $authData = array('login'=>$user, 'password'=>$pass);
            $user = new Core_Api_AdminUser();
            $user->checkAuthentication($authData);
            if (!$user->isAuthenticate()) return API_Response::fail();

            $fieldsList = (!empty($fields)) ? $fields : Core_Database::getFieldList(Core_Database::TABLE_CLIENTS);

            $db = Core_Database::getInstance();
            $select = $db->select();

            $select->from(Core_Database::TABLE_CLIENTS, $fieldsList);

            if(!empty($sort)) $select->order(trim($sort));
            if(!empty($group)) $select->group(trim($group));

            $result = Core_Database::fetchAll($select);
            $err = $errors->getErrors();
            if (!empty($err)) return API_Response::fail();
            return API_Response::success($this->makeCompaniesArray($result));
	}
	/**
	 * Function for getting all Projects for company
	 *
	 * @param string $user
	 * @param string $pass
	 * @return API_Response
	 */
	public function getAllProjects($user, $pass, $companyId)
	{
       	$logger = Core_Api_Logger::getInstance();
		$logger->setUrl('getAllProjects');
        $logger->setLogin($user);
		$logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');

		$authData = array('login'=>$user, 'password'=>$pass);
		$errors = Core_Api_Error::getInstance();

		$user = new Core_Api_AdminUser();
		$user->checkAuthentication($authData);

		if (!$user->isAuthenticate())
			return API_Response::fail();

		$fieldsList = 'Project_ID,Project_Name,Active';

		$db = Core_Database::getInstance();
		$select = $db->select();

		$select->from(Core_Database::TABLE_PROJECTS, explode(',', $fieldsList) )
		       ->where("Project_Company_ID = ?", $companyId);

		$result = Core_Database::fetchAll($select);

		$err = $errors->getErrors();
		if (!empty($err))
			return API_Response::fail();

//	    $xml = $this->makeProjectsXML($result);
//		return $xml;

		return API_Response::success($this->makeProjectsArray($result));
	}
    /**
     * getDeactivationReasons
     *
     * @access public
     * @return API_Response
     */
    public function getDeactivationCodes()
    {
        return API_Response::success(array(
            1 => 'Work order kicked back',
            2 => 'Work order declined',
            3 => 'Not Sourced',
            4 => 'No bidding techs (within time needed)"',
            5 => 'Not approved',
            6 => 'Reassigned with a new work order',
            7 => 'Recruiting work order',
            8 => 'Rescheduled work',
            9 => 'Sourced elsewhere',
            10 => 'Tech no showed',
            11 => 'Work order cancelled',
            12 => 'Duplicate Work Order',
            13 => 'EU cxl - maximum call attempts'
        ));
    }
    /**
     * getPenaltyReasons
     *
     * @access public
     * @return API_Response
     */
    public function getPenaltyReasons()
    {
        return API_Response::success(array(
            1 => 'Early Arrival',
            2 => 'Late Arrival',
            3 => 'Admin Process'
        ));
    }
    /**
     * getCallTypes
     *
     * @access public
     * @return API_Response
     */
    public function getCallTypes()
    {
        return API_Response::success(array(
            1 => 'Install',
            2 => 'Service Call',
            3 => 'Recruiting'
        ));
    }


	/**
	 * get parts for a work order
	 *
	 * @param string $user
	 * @param string $pass
	 * @param string $winNum
	 *
	 * @return API_Response
	 */
	public function getParts($user, $pass, $winNum) {
       	$logger = Core_Api_Logger::getInstance();
		$logger->setUrl('getParts');
        $logger->setLogin($user);
		$logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');

		$authData = array('login'=>$user, 'password'=>$pass);
		$errors = Core_Api_Error::getInstance();

        $user = new Core_Api_AdminUser();
        $user->checkAuthentication($authData);

		if (!$user->isAuthenticate())
			return API_Response::fail();
        $companyID = $user->getContextCompanyId();

		$parts = new Core_PartEntry(null, $winNum, $companyID);
		$objects = $parts->toAPI_PartEntry();

		$err = $errors->getErrors();
		if (!empty($err))
			return API_Response::fail();

		return API_Response::success($objects);
	}


	/**
	 * get parts for a work order
	 *
	 * @param string $user
	 * @param string $pass
	 * @param string $winNum
	 *
	 * @return API_Response
	 */
	public function getPricingRules($user, $pass) {
		return $this->getPricingRulesWithOrder($user, $pass);
	}

	/**
	 * get parts for a work order
	 *
	 * @param string $user
	 * @param string $pass
	 * @param string $winNum
	 *
	 * @return API_Response
	 */
	public function getPricingRulesDescriptionOrder($user, $pass) {
		return $this->getPricingRulesWithOrder($user, $pass, 'Description ASC');
	}

	private function getPricingRulesWithOrder($user, $pass, $order) {
       	$logger = Core_Api_Logger::getInstance();
		$logger->setUrl('getParts');
        $logger->setLogin($user);
		$logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');

		$authData = array('login'=>$user, 'password'=>$pass);
		$errors = Core_Api_Error::getInstance();

        $user = new Core_Api_AdminUser();
        $user->checkAuthentication($authData);

		if (!$user->isAuthenticate())
			return API_Response::fail();

        $result = Core_Pricing::getRules($order);

		$err = $errors->getErrors();
		if (!empty($err))
			return API_Response::fail();

		$objects = $this->makePricingRulesArray($result);

		return API_Response::success($objects);
	}
	/**
	 * get part entry
	 *
	 * @param string $user
	 * @param string $pass
	 * @param string $winNum
	 * @param string $partID
	 *
	 * @return API_Response
	 */
	public function getPartEntry($user, $pass, $winNum, $partID)
	{
       	$logger = Core_Api_Logger::getInstance();
		$logger->setUrl('getPartEntry');
        $logger->setLogin($user);
		$logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');

		$authData = array('login'=>$user, 'password'=>$pass);
		$errors = Core_Api_Error::getInstance();

		$user = new Core_Api_AdminUser();
		$user->checkAuthentication($authData);

		if (!$user->isAuthenticate())
			return API_Response::fail();
        $companyID = $user->getContextCompanyId();

		$parts = new Core_PartEntry(array($partID), $winNum, $companyID);

		$err = $errors->getErrors();
		if (!empty($err))
			return API_Response::fail();

		$objects = $parts->toAPI_PartEntry();

		return API_Response::success($objects);
	}

	private function updatePartCount($winNum) {
		$db = Core_Database::getInstance();
		$countQuery = ", ";
		$fieldMap = array("NewCount" => "TOP 1 (SELECT COUNT(id) FROM " . TABLE_PARTS . " WHERE IsNew = '1' AND PartEntryID IN ((SELECT id FROM " . TABLE_PART_ENTRIES. " WHERE WinNum = '$winNum' AND Deleted = '0')))", "ReturnCount"=> "(SELECT COUNT(id) FROM " . TABLE_PARTS . " WHERE IsNew = '0' AND PartEntryID IN ((SELECT id FROM " . TABLE_PART_ENTRIES . " WHERE WinNum = '$winNum' AND Deleted = '0' AND Consumable = '0')))");
		$result = Core_Caspio::caspioSelectWithFieldListMap(TABLE_PART_ENTRIES, $fieldMap, '', '');
		if ($result && $result[0]) {
			$newCount = $result[0]["NewCount"];
			$returnCount = $result[0]["ReturnCount"];
			try {
				$result = $db->update(Core_Database::TABLE_WORK_ORDERS, array("NewPartCount" => $newCount,"ReturnPartCount" => $returnCount), "WIN_NUM = '$winNum'");
			} catch (Exception $e) {
				$result = false;
			}
		}

		return $result;
	}

	/**
	 * create part entry
	 *
	 * @param string $user
	 * @param string $pass
	 * @param string $winNum
	 * @param API_PartEntry $partEntry
	 *
	 * @return API_Response
	 */
	public function createPartEntry($user, $pass, $winNum, $partEntry) {
		if (is_string($partEntry)) $data = $partEntry;
		else {
			$dom = new DOMDocument("1.0");
			$dom->appendChild($partEntry->toXMLElement($dom));
			$data = $dom->saveXML();
		}

       	$logger = Core_Api_Logger::getInstance();
		$logger->setUrl('createPartEntry');
        $logger->setLogin($user);
		$logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');

		$authData = array('login'=>$user, 'password'=>$pass);
		$errors = Core_Api_Error::getInstance();

		$user = new Core_Api_AdminUser();
		$user->checkAuthentication($authData);

		if (!$user->isAuthenticate())
			return API_Response::fail();
        $companyID = $user->getContextCompanyId();

		if ($partEntry instanceof API_PartEntry)
			$partData = $partEntry;
		else {
			$partData = $this->getDataFromXMLGeneric($data);
			$entry = array();
			$entry[0] = $partData["NewPart"];
			$entry[0]["IsNew"] = "True";
			$entry[0]["Consumable"] = $partData["Consumable"];
			$entry[0]["PartEntryID"] = $partID;
			$entry[1] = $partData["ReturnPart"];
			$entry[1]["IsNew"] = "False";
			$partData = API_PartEntry::partEntryWithResult($entry);
		}

		$result = Core_PartEntry::create($partData, $winNum);

		$err = $errors->getErrors();
		if (!empty($err))
			return API_Response::fail();

		$objects = array(new API_PartEntry($result, NULL));

		return API_Response::success($objects);
	}

	/**
	 * update part entry
	 *
	 * @param string $user
	 * @param string $pass
	 * @param string $winNum
	 * @param string $partID
	 * @param API_PartEntry $partEntry
	 *
	 * @return API_Response
	 */
	public function updatePartEntry($user, $pass, $winNum, $partID, $partEntry) {
		if (is_string($partEntry)) $data = $partEntry;
		else {
			$dom = new DOMDocument("1.0");
			$dom->appendChild($partEntry->toXMLElement($dom));
			$data = $dom->saveXML();
		}

       	$logger = Core_Api_Logger::getInstance();
		$logger->setUrl('updatePartEntry');
        $logger->setLogin($user);
		$logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');

		$authData = array('login'=>$user, 'password'=>$pass);
		$errors = Core_Api_Error::getInstance();

		$user = new Core_Api_AdminUser();
		$user->checkAuthentication($authData);

		if (!$user->isAuthenticate())
			return API_Response::fail();
        $companyID = $user->getContextCompanyId();

		if ($partEntry instanceof API_PartEntry)
			$partData = $partEntry;
		else {
			$partData = $this->getDataFromXMLGeneric($data);
			$entry = array();
			$entry[0] = $partData["NewPart"];
			$entry[0]["IsNew"] = "True";
			$entry[0]["Consumable"] = $partData["Consumable"];
			$entry[0]["PartEntryID"] = $partID;
			$entry[1] = $partData["ReturnPart"];
			$entry[1]["IsNew"] = "False";
			$partData = API_PartEntry::partEntryWithResult($entry);
		}

		Core_PartEntry::update($partData, $winNum);

		$err = $errors->getErrors();
		if (!empty($err))
			return API_Response::fail();

		$objects = array(new API_PartEntry($partID, NULL));

		return API_Response::success($objects);
	}

	/**
	 * update part entry
	 *
	 * @param string $user
	 * @param string $pass
	 * @param string $winNum
	 * @param string $partID
	 *
	 * @return API_Response
	 */
	public function deletePart($user, $pass, $winNum, $partID) {
       	$logger = Core_Api_Logger::getInstance();
		$logger->setUrl('deletePart');
        $logger->setLogin($user);
		$logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');

		$authData = array('login'=>$user, 'password'=>$pass);
		$errors = Core_Api_Error::getInstance();

		$user = new Core_Api_AdminUser();
		$user->checkAuthentication($authData);

		if (!$user->isAuthenticate())
			return API_Response::fail();
        $companyID = $user->getContextCompanyId();

		$partData = new API_PartEntry($partID, false);

		Core_PartEntry::delete($partData, $winNum);

		$err = $errors->getErrors();
		if (!empty($err))
			return API_Response::fail();

		$objects = array(new API_PartEntry($partID, NULL));

		return API_Response::success($objects);
	}

        /**
	 * Function for getting Projects for current admin user
	 *
	 * @param string $user
	 * @param string $pass
         * @param array $fields
         * @param string $company_id
	 * @param array $project_ids
         * @param string $projectsState
	 * @param string $sort
	 * @return API_Response
	 */
	public function getProjects($user, $pass, $fields = null, $company_id = null, $project_ids = null,$projectsState= 'all',$sort = null)
	{
            $logger = Core_Api_Logger::getInstance();
            $logger->setUrl('getProjects');
            $logger->setLogin($user);
            $logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');

            $errors = Core_Api_Error::getInstance();

            $authData = array('login'=>$user, 'password'=>$pass);
            $user = new Core_Api_AdminUser();
            $user->checkAuthentication($authData);
            if (!$user->isAuthenticate()) return API_Response::fail();

            $fieldsList = (!empty($fields)) ? $fields : Core_Database::getFieldList(Core_Database::TABLE_PROJECTS);

            $db = Core_Database::getInstance();
            $select = $db->select();

            $select->from(Core_Database::TABLE_PROJECTS, $fieldsList);

            if(!empty($company_id)) $select->where("Project_Company_ID = ?", $companyId);
            if(!empty($project_ids)) $select->where("Project_ID IN (?)", $project_ids);
            switch ( strtolower(trim($projectsState)) ) {
                case 'active':
                    $select->where("Active = 1");
                    break;
                case 'notactive':
                    $select->where("Active = 0");
                    break;
            }
            if(!empty($sort)) $select->order(trim($sort));
            $result = Core_Database::fetchAll($select);

            $err = $errors->getErrors();
            if (!empty($err)) return API_Response::fail();

            return API_Response::success($this->makeProjectsArray($result));
	}

    public function createClient($login, $hash, $data){
        $errors = Core_Api_Error::getInstance();

        $authData = array('login'=>$login, 'password'=>$hash);
        $user = new Core_Api_AdminUser();
        $user->checkAuthentication($authData);
        if (!$user->isAuthenticate()) return API_Response::fail();

        $logger = Core_Api_Logger::getInstance();
        $logger->setUrl('createClient');
        $logger->setLogin($login);
        $logger->addLog(LOGGER_ERROR_NO, null, 'db request');

        $factory = new Core_Validation_Factory(Core_Validation_Factory::VALIDATION_TYPE_CLIENT_CREATE);
	    $validation = $factory->getValidationObject($data);

        if ($validation->validate() === false) {
            $errors->addErrors('6', $validation->getErrors());
			return API_Response::fail();
        }

        unset($data['PasswordConf']);
		$deduct = $data['PcntDeductPercent'];
		unset($data['PcntDeductPercent']);

        $client = new Core_Api_User();
        foreach( $data as $f=>$v ) {
            $client->$f = $v;
        }

        $result = $client->save();
		
		$db = Core_Database::getInstance();
		$companyList = $db->fetchCol("SELECT DISTINCT Company_ID FROM " . Core_Database::TABLE_CLIENTS . " WHERE Company_ID = ". $db->quoteInto("?", $data['Company_ID']) . " AND NOT EXISTS (SELECT Project_ID FROM " . Core_Database::TABLE_PROJECTS . " WHERE Project_Company_ID = Company_ID AND Project_Name = 'Default Project')");
		 
                if ($companyList && sizeof($companyList) > 0) {
                    	//13870		
			// only set deduct for new company
			$client->SaveDeductPercent_FSSFeeFrTech($data['Company_ID'], $deduct);

			$client->saveClientCompanyAdditionalFields(
				$data['Company_ID'],
				array('BeginDateForCountingNDaysRule' => date("Y-m-d"))
			);
		}

        $err = $errors->getErrors();
  		if (!empty($err))
			return API_Response::fail();

        return API_Response::success(array($result));
    }
    public function createClient_NoCheck($data){
        $errors = Core_Api_Error::getInstance();

        $factory = new Core_Validation_Factory(Core_Validation_Factory::VALIDATION_TYPE_CLIENT_CREATE);
	$validation = $factory->getValidationObject($data);

        if ($validation->validate() === false) {
            $errors->addErrors('6', $validation->getErrors());
			return API_Response::fail();
        }

        unset($data['PasswordConf']);
		$deduct = $data['PcntDeductPercent'];
		unset($data['PcntDeductPercent']);

        $client = new Core_Api_User();
        foreach( $data as $f=>$v ) {
            $client->$f = $v;
        }

        $result = $client->save();
		
		$db = Core_Database::getInstance();
		$companyList = $db->fetchCol("SELECT DISTINCT Company_ID FROM " . Core_Database::TABLE_CLIENTS . " WHERE Company_ID = ". $db->quoteInto("?", $data['Company_ID']) . " AND NOT EXISTS (SELECT Project_ID FROM " . Core_Database::TABLE_PROJECTS . " WHERE Project_Company_ID = Company_ID AND Project_Name = 'Default Project')");
		if ($companyList && sizeof($companyList) > 0) {
			$db->insert(Core_Database::TABLE_PROJECTS, array("Project_Name" => "Default Project", "Active" => 1, "CreatedBy" => "fs_system", "From_Email" => "no-replies@fieldsolutions.us", "To_Email" => "no-replies@fieldsolutions.us", "WO_Category_ID" => 26, "Headline" => "General Assignment", "Project_Company_ID" => $data['Company_ID'], "AutoBlastOnPublish" => 1));
			// only set deduct for new company
			$client->SaveDeductPercent_FSSFeeFrTech($data['Company_ID'], $deduct);
		}

        $err = $errors->getErrors();
  		if (!empty($err))
			return API_Response::fail();

        return API_Response::success(array($result));
    }
    public function updateClient($login, $hash, $id, $data){        
        $errors = Core_Api_Error::getInstance();

        $authData = array('login'=>$login, 'password'=>$hash);
        $user = new Core_Api_AdminUser();
        $user->checkAuthentication($authData);
        
        if (!$user->isAuthenticate()) return API_Response::fail();

        $logger = Core_Api_Logger::getInstance();
        $logger->setUrl('updateClient');
        $logger->setLogin($login);
        $logger->addLog(LOGGER_ERROR_NO, null, 'db request');

        $user = new Core_Api_User($id);
        $user->loadById();

        $datatoupdate = array();
        foreach($data as $field=>$val){
            if($val != $user->$field) $datatoupdate[$field] = $val;
        }

        if (!empty($datatoupdate['ClientExpiration'])){
            $data['ClientExpiration']=$datatoupdate['ClientExpiration']=date("Y-m-d", strtotime($datatoupdate['ClientExpiration']));
        }
        $factory = new Core_Validation_Factory(Core_Validation_Factory::VALIDATION_TYPE_CLIENT_UPDATE);

        $user = new Core_Api_User($id);
        $user->loadById();

		if ($data['UserName'] == $user->UserName) {
			// remove UserName to avoid validation
			unset($data['UserName']);
		}

		if (!empty($data['Password']) && $data['Password'] == $user->Password) $data['Password'] = 'bypass78';

	    $validation = $factory->getValidationObject($data);

        if ($validation->validate() === false) {
            $errors->addErrors('6', $validation->getErrors());
			return API_Response::fail();
        }
        if(empty($datatoupdate)) return API_Response::success(array($user));

        $result = $user->save($datatoupdate);
        $err = $errors->getErrors();
  		if (!empty($err))
			return API_Response::fail();

        return API_Response::success(array($result));
    }

    /**
     * uploadFile
     *
     * @param string $user
     * @param string $pass
     * @param string $client_id
     * @param string $fName
     * @param string $fData
     * @param string $field
     * @access public
     * @return API_Response
     */
    public function uploadFileAWS( $login, $hash, $client_id, $fName, $fData, $field)
    {
        $errors = Core_Api_Error::getInstance();

        $authData = array('login'=>$login, 'password'=>$hash);
        $user = new Core_Api_AdminUser();
        $user->checkAuthentication($authData);

        if (!$user->isAuthenticate()) return API_Response::fail();

        $logger = Core_Api_Logger::getInstance();
        $logger->setUrl('updateClient');
        $logger->setLogin($login);
        $logger->addLog(LOGGER_ERROR_NO, null, 'db request');

        $fName = ltrim(trim($fName), '/');
        $file = new Core_File();
        $uploadResult = $file->uploadFile($fName, $fData, S3_CLIENTS_LOGO_DIR, true);
        if(!$uploadResult){
            $errors->addError(6, 'Upload file '.$fName.' failed');
            $errorsDetails = $errors->getErrors();
            error_log('FileUploading: (admin:client:uploadFileAWS) ' . implode('|', $errorsDetails));
            return API_Response::fail();
        }

        if ( $errors->getErrors() ) {
            return API_Response::fail();
        }

        $client = new Core_Api_User($client_id);
        $res = $client->save(array($field=>$fName));

        //Core_TimeStamp::createTimeStamp($client_id, Core_Api_TechUser::getLoginByAuth($user), "Client Updated: File {$fName} was uploaded.", '', '', '', "", "", "");

        $err = $errors->getErrors();
  		if (!empty($err))
			return API_Response::fail();

        return API_Response::success(array());
    }

    /**
     *  remove Client by ID
     * @param string $login
     * @param string $hash
     * @param int $id
     * @return API_Response object
     * @author Alex Scherba
     */
    public function removeClient($login, $hash, $id) {
        $errors = Core_Api_Error::getInstance();

        $authData = array('login'=>$login, 'password'=>$hash);
        $user = new Core_Api_AdminUser();
        $user->checkAuthentication($authData);

        if (!$user->isAuthenticate()) return API_Response::fail();

        $logger = Core_Api_Logger::getInstance();
        $logger->setUrl('updateClient');
        $logger->setLogin($login);
        $logger->addLog(LOGGER_ERROR_NO, null, 'db request');

        $user = new Core_Api_User($id);
        $user->loadById();
        $file_fields = $user->getFilesFields();
        $docs_to_delete = array();
        foreach ($file_fields as $f) $docs_to_delete[$f] = $user->$f;
        $res = $user->remove();

        if(!$res) $errors->addError(7, 'SQL error accured.');
        $err = $errors->getErrors();
  		if (!empty($err))
			return API_Response::fail();

        $file = new Core_File();
        foreach ($docs_to_delete as $f) {
            $file->removeFile(trim($f, '/') , S3_CLIENTS_LOGO_DIR);
        }
        $err = $errors->getErrors();
        if (!empty($err))
			return API_Response::fail();

        return API_Response::success(array());
    }

	/* PRIVATE SECTION */

	private function doesPartExist($companyID, $winNum, $partEntryID) {
		$errors = Core_Api_Error::getInstance();
		// check work order exists
		$result = $this->getWorkOrdersWithWinNum($winNum, "WIN_NUM", "", $companyID);

        if (empty($result[0])) {
        	$errors->addError(2, 'WinNum '.$winNum.' does not exist');
        	return FALSE;
        }

		$resultPart = Core_Caspio::caspioSelectAdv(TABLE_PART_ENTRIES, "id", "id = '$partEntryID' AND WinNum = '$winNum' AND Deleted = '0'", '');

		if (@$resultPart[0]["id"] != $partEntryID) {
		    $errors->addError(2, "PartEntryID $partEntryID does not exist");
		    return FALSE;
		}
		return TRUE;
	}

	private function insertPart($data) {
		$errors = Core_Api_Error::getInstance();
		$data["ETA"] = empty($data["ETA"]) ? NULL : $data["ETA"];

		$values = array_values($data);
/*		foreach ($values as $k => $val) {
			if ($val == NULL) continue;
			$values[$k] = Core_Caspio::caspioEscape($val);
		}*/

		// create New Part
		$result = Core_Caspio::caspioInsert(TABLE_PARTS, array_keys($data), $values);

		if (!$result) {
			$err = $errors->getErrors();
			if (!empty($err))
				return FALSE;

			$errors->addError(7, 'Incorrect SQL request');
			return FALSE;
		}

		return TRUE;
	}

	private function updatePart($data) {
		$errors = Core_Api_Error::getInstance();
		$data["ETA"] = empty($data["ETA"]) ? NULL : $data["ETA"];
		$isNew = isset($data["IsNew"]) && $data["IsNew"] == "True" ? 1 : 0;
		$partEntryID = $data["PartEntryID"];

		$values = array_values($data);
/*		foreach ($values as $k => $val) {
			if ($val == NULL) continue;
			$values[$k] = Core_Caspio::caspioEscape($val);
		}*/

		$result = Core_Caspio::caspioUpdate(TABLE_PARTS, array_keys($data), $values, "PartEntryID = '$partEntryID' AND IsNew = '$isNew'");

		if ($result == 0) {
			$result = Core_Caspio::caspioInsert(TABLE_PARTS, array_keys($data), array_values($data));
		}

		if (!$result) {
			$err = $errors->getErrors();
			if (!empty($err))
				return FALSE;

			$errors->addError(7, 'Incorrect SQL request');
			return FALSE;
		}

		return TRUE;
	}

	private static function errorsToAPIErrors($errors) {
		$ret = array();
		foreach ($errors as $key => $val) {
			$ret[] = new API_Error($val["error_id"], $val["id"], $val["description"]);
		}
		return $ret;
	}

	private function makePartsArray($result) {
		$parts = array();
		$entryList = array();

	    if (!empty($result[0])) {
			foreach ($result as $key=>$value) {
				if (!array_key_exists($value['PartEntryID'], $entryList)) {
					// New Part Entry
					$entryList[$value['PartEntryID']] = array();
				}
				$entryList[$value['PartEntryID']][] = $value;
			}
			foreach ($entryList as $key=>$value) {
				$part = API_PartEntry::partEntryWithResult($value);
				$parts[] = $part;
			}
	    }
		return $parts;
	}


	private function makePricingRulesArray($result)
	{
		$parts = array();

	    if (is_array($result) && sizeof($result) > 0) {
			foreach ($result as $key => $value) {
				$part = API_PricingRuleEntry::partEntryWithResult($value);
				$parts[] = $part;
			}
	    }
		return $parts;
	}

/*	private function makeWorkOrderArray($result) {
		$wo = array();
	    if (!empty($result[0])) {
			foreach ($result as $key=>$value) {
				$wo[] = API_WorkOrder::workOrderWithResult($value);
			}
	    }
		return $wo;
	}*/
        private function makeCompaniesArray($result)
	{
            $companies = array();
	    if (!empty($result[0])) {
                foreach ($result as $key=>$value)
                    $companies[] = API_Company::companyWithResult($value);
	    }
            return $companies;
	}
	private function makeWOCategoryArray($result)
	{
		$wo = array();
	    if (!empty($result[0])) {
			foreach ($result as $key=>$value) {
				$wo[] = API_WOCategory::woCategoryWithResult($value);
			}
	    }
		return $wo;
	}

	private function makeProjectsArray($result)
	{
		$wo = array();
	    if (!empty($result[0])) {
			foreach ($result as $key=>$value) {
				$prj = API_Project::projectWithResult($value);
                if(isset($value['ServiceTypeName'])){
                    $prj->ServiceTypeName = $value['ServiceTypeName'];                    
                }else{
                    $prj->ServiceTypeName = '';                    
                }
                
                if(isset($value['EntityTypeName'])){
                    $prj->EntityTypeName = $value['EntityTypeName'];                    
                } else {
                    $prj->EntityTypeName = '';
                }
                
                if(isset($value['AMName'])){
                    $prj->AMName = $value['AMName'];                                
                } else {
                    $prj->AMName = '';
                }

                //13992
                if(isset($value['CSDName'])){
                    $prj->CSDName = $value['CSDName'];                                
                } else {
                    if(isset($value['CliCSDName'])){
                        $prj->CSDName = $value['CliCSDName'];                       
                    } else {
                        $prj->CSDName = '';
                    }
                }
                //end 13992
                
                $prj->CompanyCode = $value['Project_Company_ID'];
                
                if(isset($value['CompanyName'])){
                    $prj->CompanyName = $value['CompanyName'];                                
                } else {
                    $prj->CompanyName = '';
                }
                
                
                $wo[]=$prj;
			}
	    }
		return $wo;
	}
    private function makeClientsArray($result)
	{
		$wo = array();
	    if (!empty($result[0])) {
			foreach ($result as $key=>$value) {
				$wo[] = Core_Api_User::clientWithResult($value);
			}
	    }
		return $wo;
	}

	private function makePartsXML($result)
	{
		$dom = new DOMDocument("1.0");
		$root = $dom->createElement('Parts');
		$entryList = array();
		$entry = NULL;

	    if (!empty($result[0])) {
		    foreach ($result as $key=>$value) {

				if (!array_key_exists($value['PartEntryID'], $entryList)) {
					// New Part Entry
	                $entry = $dom->createElement('PartEntry');
					$entryList[$value['PartEntryID']] = "1";
	                $id = $dom->createAttribute('id');
	                $id->appendChild($dom->createTextNode($value['PartEntryID']));
	                $entry->appendChild($id);

			if (array_key_exists("Consumable", $value)) {
				$consumable = $value["Consumable"];
				unset($value["Consumable"]);
				$element = $dom->createElement("Consumable");
				$element->appendChild($dom->createTextNode($consumable));
				$entry->appendChild($element);
			}


	                $root->appendChild($entry);
				}

				$this->makePartDOM($dom, $entry, $value);
            }
	    }
	    $dom->appendChild($root);
		return $dom->saveXML();
	}

	private function makePartXML($result)
	{
		$dom = new DOMDocument("1.0");
        $entry = $dom->createElement('PartEntry');
		if (!empty($result[0])) {
			$value = $result[0];
	        $id = $dom->createAttribute('id');
	        $id->appendChild($dom->createTextNode($value['PartEntryID']));
	        $entry->appendChild($id);

		    foreach ($result as $k=>$value) {
				if (array_key_exists("Consumable", $value)) {
					$consumable = $value["Consumable"];
					unset($value["Consumable"]);
					$element = $dom->createElement("Consumable");
					$element->appendChild($dom->createTextNode($consumable));
					$entry->appendChild($element);
				}

				$this->makePartDOM($dom, $entry, $value);
			}
		}
		$dom->appendChild($entry);
		return $dom->saveXML();
	}

	private function makePartDOM($dom, &$entry, $value)
	{
		$ignorePartsField = array("id" => 1, "IsNew" => 1, "PartEntryID" => 1);
		$ignoreNewPartsField = array("ShippingAddress" => 1, "ShipTo" => 1);
		$ignoreReturnPartsField = array("RMA" => 1, "Instructions" => 1);
		$dateFields = array("ETA" => 1);

		$isNew = $value["IsNew"] == "True";
		$part =  $isNew ? $dom->createElement("NewPart") : $dom->createElement("ReturnPart");
		$ignoreList = $isNew ? $ignoreReturnPartsField : $ignoreNewPartsField;

		foreach ($value as $k=>$v) {
			if (array_key_exists($k, $ignorePartsField) || array_key_exists($k, $ignoreList)) continue;
			if ($v == "NULL")
				$val = "";
			else if (!empty($v) && array_key_exists($k, $dateFields )) {
				if ($ts = strtotime($v))
					$val = date("m/d/Y", $ts);
				else
					$val = NULL;
			}
			else
				$val = $v;

			$element = $dom->createElement($k);
			$element->appendChild($dom->createTextNode( htmlspecialchars($val, ENT_QUOTES) ));
			$part->appendChild($element);
			$entry->appendChild($part);
		}
	}

	private function makeProjectsXML($result)
	{
		$dom = new DOMDocument("1.0");
		$root = $dom->createElement('Projects');
	    if (!empty($result[0])) {
		    foreach ($result as $key=>$value) {
                $pj = $dom->createElement('Project');

                $id = $dom->createAttribute('ID');
                $name = $dom->createAttribute('NAME');

                $id->appendChild($dom->createTextNode($value['Project_ID']));
                $name->appendChild($dom->createTextNode($value[' Project_Name']));

                $pj->appendChild($id);
                $pj->appendChild($name);

                $root->appendChild($pj);
            }
	    } else {
//			$root->appendChild($dom->createTextNode('No results'));
	    }
	    $dom->appendChild($root);
		return $dom->saveXML();
	}

	private function makeWorkOrdersXML($result)
	{
		$dom = new DOMDocument("1.0");
		$root = $dom->createElement('WOS');
	    if (!empty($result[0])) {
		    foreach ($result as $key=>$value) {
			    $wo = $dom->createElement('WO');
			    foreach ($value as $k=>$val) {
			        if (!in_array( strtoupper($k), $this->internalFields)) {
    			    	$field = $k == 'TB_UNID' ? $dom->createElement('WIN_NUM') :$dom->createElement($k);
    			    	$field->appendChild($dom->createTextNode($val));
    			    	$wo->appendChild($field);
			        }
			    }
			    $root->appendChild($wo);
		    }
	    }
	    $dom->appendChild($root);
		return $dom->saveXML();
	}

	private function makeWorkOrderXML($result)
	{
		$dom = new DOMDocument("1.0");
		$wo = $dom->createElement('WO');
   		if (!empty($result)) {
		    foreach ($result as $k=>$val) {
		        if (!in_array(strtoupper($k), $this->internalFields)) {
    			    $field = $k == 'TB_UNID' ? $dom->createElement('WIN_NUM') :$dom->createElement($k);

			    	$field->appendChild($dom->createTextNode( htmlspecialchars($val, ENT_QUOTES) ));
			    	$wo->appendChild($field);
		        }
		    }
   		}
   		$dom->appendChild($wo);
		return $dom->saveXML();
	}

	private function getTableDesign($tableName)
	{
		$columns = Core_Caspio::caspioGetTableDesign($tableName);
        $fieldList = array();

        foreach ($columns as $info) {
	        $fieldInfo = explode(",",$info);
	        $fieldList[] = trim($fieldInfo[0]);
        }

        $fieldList = implode(',', $fieldList);

        return $fieldList;
	}

	private function getWorkOrderrequiredFields()
	{
		$requiredFields = array('WO_ID','Headline');
		return $requiredFields;
	}

	private function sendEmailToFirstTechs($lat, $long)
	{
	    return $techs = Core_Caspio::caspioProximitySearchByCoordinatesRaw(TABLE_MASTER_LIST, false, $lat, $long, "Latitude", "Longitude", "<= 50", "3", '8', 'TechID', "calculated_distance ASC", true, "|", "`", true);
	    if (!empty($techs)) {
		    $params = array();
		   	$woCall = new Core_WoCall();
		   	$woCall->setCallType('create');
		  	foreach ($techs as $tech) {
		   		$woCall->setParams($params);
		  		$woCall->doCall();
		 	}
        }
	}

	private function createAtJob($woId)
	{
        $cfgSite = Zend_Registry::get('CFG_SITE');

	    $secureStr = md5($cfgSite->autoassign->secure_string . $woId);
	    $url = $cfgSite->autoassign->url . "?TB_UNID=".urlencode($woId)."&secure_str=$secureStr";

	    $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url); // set url to post to
        curl_setopt($ch, CURLOPT_FAILONERROR, 0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);// allow redirects
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1); // return into a variable
        curl_setopt($ch, CURLOPT_TIMEOUT, 20); // times out after 4s
        curl_setopt($ch, CURLOPT_POST, 0); // set POST method
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        $result = curl_exec($ch); // run the whole process
        curl_close($ch);
	}

	private function excludeFields(&$fieldsList)
	{
		$ro = API_AdminWorkOrder::readOnlyFields();
	    foreach ($fieldsList as $key=>$field) {
	    	if ( in_array( strtoupper($key), $ro) ) {
	    		unset($fieldsList[$key]);
	    	}
	    }
	}

	private function resetFieldsForCopyWO(&$data)
	{
	    //$data[''] = trim($arr[0], "'") . " - " . date("mdYHis"); // This is a unique field, adding dateTime in order to ensure uniqueness of the newly created work order.
	    date_default_timezone_set('US/Central');
        $data['DateEntered'] = date('m/d/Y h:i:s A');
		$data['StartDate'] = NULL; // Reset StartDate
		$data['EndDate'] = NULL; // Reset EndDate
		$data['Tech_FName'] = NULL; // Reset Tech_FName
		$data['Approved'] = '0'; // Reset Approved
		$data['ShowTechs'] = '0'; // Reset ShowTechs
		$data['Tech_ID'] = NULL; // Reset Tech_ID
		$data['FLS_ID'] = NULL; // Reset FLS_ID
		$data['TechMarkedComplete'] = '0'; // Reset TechMarkedComplete
		$data['TechComments'] = ''; // Reset TechComments
		$data['MissingComments'] = ''; // Reset MissingComments
		$data['Tech_LName'] = ''; // Reset Tech_LName
		$data['TechPaid'] = '0'; // Reset TechPaid
		$data['DatePaid'] = NULL; // Reset DatePaid
		$data['PayAmount'] = ''; // Reset PayAmount
		$data['CheckedIn'] = '0'; // Reset CheckedIn
		$data['Checkedby'] = ''; // Reset Checkedby
		$data['CheckInNotes'] = ''; // Reset CheckInNotes
		$data['StartTime'] = ''; // Reset StartTime
		$data['StoreNotified'] = '0'; // Reset StoreNotified
		$data['Tech_Bid_Amount'] = NULL; // Reset Tech_Bid_Amount
		$data['Qty_Applicants'] = '0'; // Reset Qty_Applicants
		$data['Invoiced'] = '0'; // Reset Invoiced
		$data['DateInvoiced'] = ''; // Reset DateInvoiced
		$data['DateApproved'] = NULL; // Reset DateApproved
		$data['PayApprove'] = ''; // Reset PayApprove
		$data['WorkOrderReviewed'] = ''; // Reset WorkOrderReviewed
		$data['Deactivated'] = '0'; // Reset Deactivated70
		$data['TechCheckedIn_24hrs'] = '0'; // Reset TechCheckedIn_24hrs
		$data['TechCalled'] = ''; // Reset TechCalled72
		$data['CalledBy'] = ''; // Reset CalledBy73
		$data['CallDate'] = ''; // Reset CallDate74
		$data['NoShow_Tech'] = NULL; // Reset NoShow_Tech
		$data['Time_In'] = ''; // Reset Time_In
		$data['Time_Out'] = ''; // Reset Time_Out
		$data['TechEmail'] = ''; // Reset TechEmail
		$data['TechPhone'] = ''; // Reset TechPhone
		$data['Date_Assigned'] = NULL; // Reset Date_Assigned
		$data['Update_Requested'] = '0'; // Reset Update_Requested
		$data['Update_Reason'] = ''; // Reset Update_Reason
		$data['Additional_Pay_Amount'] = ''; // Reset Additional_Pay_Amount
		$data['Add_Pay_AuthBy'] = ''; // Reset Add_Pay_AuthBy
		$data['Work_Out_of_Scope'] = '0'; // Reset Work_Out_of_Scope
		$data['OutOfScope_Reason'] = ''; // Reset OutOfScope_Reason
		$data['Site_Complete'] = '0'; // Reset Site_Complete
		$data['Add_Pay_Reason'] = ''; // Reset Add_Pay_Reason
		$data['BackOut_Tech'] = NULL; // Reset BackOut_Tech
		$data['EndTime'] = ''; // Reset EndTime
		$data['lastModBy'] = ''; // Reset lastModBy
		$data['lastModDate'] = ''; // Reset lastModDate
		$data['approvedBy'] = ''; // Reset approvedBy
		$data['Date_Completed'] = ''; // Reset Date_Completed
		$data['Deactivated_Reason'] = ''; // Reset Deactivated_Reason
		$data['OutofScope_Amount'] = ''; // Reset OutofScope_Amount101
		$data['PayAmount_Final'] = ''; // Reset PayAmount_Final102
		$data['Site_Contact_Name'] = ''; // Reset Site_Contact_Name
		$data['Check_Number'] = ''; // Reset Check_Number
		$data['MovedToMysql'] = '0'; // Reset MovedToMysql106
		$data['StartRange'] = ''; // Reset StartRange108
		$data['Duration'] = ''; // Reset Duration
		$data['calculatedInvoice'] = ''; // Reset calculatedInvoice
		$data['calculatedTotalTechPay'] = ''; // Reset calculatedTotalTechPay
		$data['calculatedTechHrs'] = ''; // Reset calculatedTechHrs
		$data['baseTechPay'] = ''; // Reset baseTechPay
		$data['PricingCalculationText'] = ''; // Reset PricingCalculationText
		$data['TripCharge'] = NULL; // Reset TripCharge
		$data['MileageReimbursement'] = NULL; // Reset MileageReimbursement
		$data['MaterialsReimbursement'] = NULL; // Reset MaterialsReimbursement
		$data['TechnicianFee'] = ''; // Reset TechnicianFee
		$data['TechnicianFeeComments'] = ''; // Reset TechnicianFeeComments
		$data['ExpediteFee'] = '0'; // Reset ExpediteFee
		$data['AbortFee'] = '0'; // Reset AbortFee
		$data['AfterHoursFee'] = '0'; // Reset AfterHoursFee
		$data['OtherFSFee'] = ''; // Reset OtherFSFee
		$data['TechMiles'] = ''; // Reset TechMiles
		$data['PricingRuleApplied'] = ''; // Reset PricingRuleApplied
		$data['AbortFeeAmount'] = '0'; // Reset AbortFeeAmount
		$data['PricingRan'] = '0'; // Reset PricingRan
		$data['General1'] = ''; // Reset General1
		$data['General2'] = ''; // Reset General2
		$data['General3'] = ''; // Reset General3
		$data['General4'] = ''; // Reset General4
		$data['General5'] = ''; // Reset General5
		$data['General6'] = ''; // Reset General6
		$data['General7'] = ''; // Reset General7
		$data['General8'] = ''; // Reset General8
		$data['General9'] = ''; // Reset General9
		$data['General10'] = ''; // Reset General10
		return true;
	}

	private function getDataFromXML($xml)
	{
	    $result = array();
		$dom = new DOMDocument("1.0");
		$dom->loadXML($xml);
		$nodes = $dom->getElementsByTagName('WO')->item(0)->childNodes;

		foreach ($nodes as $node)
		{
			$result[$node->nodeName] = $node->nodeValue;
		}

		return $result;
	}

	private function getDataFromXMLGeneric($xml)
	{
	    $result = array();
		$dom = new DOMDocument("1.0");
		$dom->loadXML($xml);

		$result = $this->nodeToArray($dom->documentElement);

		return $result;
	}

	private function nodeToArray($node) {
		if ($node->hasChildNodes()) {
			$result = array();
			$children = $node->childNodes;
			foreach ($children as $child) {
				$childVal = $this->nodeToArray($child);
				if ($child->nodeName == "#text")
					$result = $childVal;
				else
					$result[$child->nodeName] = $childVal;
			}
			return $result;
		}
		return $node->nodeValue;
	}

/*	private function getDataFromXML($xml)
	{
	    if (is_string($xml)) {
	    	$xml = simplexml_load_string($xml);
	    }
	    $result = array();
		foreach ($xml->parameter as $key=>$parameter) {
			$f_name = trim($parameter['fieldName'],"`'");
		    $result["$f_name"] = trim($parameter['fieldValue'], "`'");
		}
		return $result;
	}*/

	private function numberWeekDays($startDate, $endDate) {
			$startDateTS = strtotime($startDate);
			$endDateTS = strtotime($endDate);

			$startDayOfWeek = date("w", $startDateTS);
			$endDayOfWeek = date("w", $endDateTS);

			$daysToNextSunday = 7 - $startDayOfWeek;
			$daysToPreviousSunday = $endDayOfWeek;
			$nextSundayTS = strtotime("+$daysToNextSunday days", $startDateTS);
			$previousSundayTS = strtotime("-$daysToPreviousSunday days", $endDateTS);

			$numWeekDays = 0;

			if ($nextSundayTS < $endDateTS) {
				// date range is more than one week
				$startWeekNumWeekDays = 5 - ($startDayOfWeek - 1 <= -1 ? 0 : $startDayOfWeek - 1);
				$endWeekNumWeekDays = ($endDayOfWeek == 6 ? 5 : $endDayOfWeek);

				$numDaysBetweenFirstAndLastWeek = ($previousSundayTS - $nextSundayTS) / 60 / 60 / 24 / 7 * 5;

				$numWeekDays = $startWeekNumWeekDays + $endWeekNumWeekDays + $numDaysBetweenFirstAndLastWeek;
			}
			else {
				$startWeekNumWeekDays = 5 - ($startDayOfWeek - 1 <= -1 ? 0 : $startDayOfWeek - 1);
				$endWeekNumWeekDaysNotIncluded = 5 - ($endDayOfWeek == 6 ? 5 : $endDayOfWeek);
				$numWeekDays = $startWeekNumWeekDays - $endWeekNumWeekDaysNotIncluded;
			}
			return $numWeekDays;
	}

	private function calculateSourceByDate($startDateTS, $endDateTS, $dateEntered, $numWeekDays) {
		$sourceByDateTS = $endDateTS; // use Project Start date by default

		if ($numWeekDays > 5 && $numWeekDays < 16) {
			// use Date record created
			$sourceByDateTS = strtotime(date("m/d/Y", strtotime($dateEntered)));

			// Can't count start day so subtract, initally start with Project Date
			$sourceByDateTS = strtotime("-1 day", $sourceByDateTS);
		}

		if ($numWeekDays < 6)
			$sourceByDateTS = strtotime(date("m/d/Y", strtotime($dateEntered)));
		else if ($numWeekDays > 5 && $numWeekDays < 16) {
			for ($i = 0; $i <= 5; ++$i) {
				$dayNum = date("w", $sourceByDateTS);
				if ($dayNum == 0 || $dayNum == 6)
					--$i;
				$sourceByDateTS = strtotime("+1 days", $sourceByDateTS);
			}
		}
		else {
			$sourceByDateTS = strtotime("-1 days", $sourceByDateTS);
			for ($i = 10; $i >= 1; $i--) {
				$dayNum = date("w", $sourceByDateTS);
				if ($dayNum == 0 || $dayNum == 6)
					++$i;
				$sourceByDateTS = strtotime("-1 days", $sourceByDateTS);

			}
		}

		return $sourceByDateTS;
	}



	/**
	 * Function for getting Projects for current usre
	 *
	 * @param string $user
	 * @param string $pass
	 * @param array $project_ids
	 * @param string $sort
	 * @return API_Response
	 */
	public function getProjectsList($user, $pass, $filters, $sort, $offset = 0, $limit = 0, &$count)
	{
        $logger = Core_Api_Logger::getInstance();
        $logger->setUrl('getProjectsList');
        $logger->setLogin($user);
        $logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');

        $errors = Core_Api_Error::getInstance();

        $authData = array('login'=>$user, 'password'=>$pass);
        $user = new Core_Api_AdminUser();
        $user->checkAuthentication($authData);
        if (!$user->isAuthenticate()) return API_Response::fail();

        //13798
        $db = Core_Database::getInstance();
        $companyQuery = $db->select();
        $companyQuery->from(array('cl'=>Core_Database::TABLE_CLIENTS),
                array('Company_ID','CompanyName')
        );
                
        //13992        
        $companyQuery->joinLeft(array('clex'=>"clients_ext"),
                'clex.ClientId = cl.ClientID',
                array('FSAccountManagerId','ClientFSCSDId'=>'FSClientServiceDirectorId')
        );   
        //end 13992
        
        
        $companyQuery->joinLeft(array('am'=>'operations_staff'),
                'am.id = clex.FSAccountManagerId',
                array('AMName'=>'name')
        );
        $companyQuery->joinLeft(array('clicsd'=>'operations_staff'),
                'clicsd.id = clex.FSClientServiceDirectorId',
                array('CliCSDName'=>'name')
        );
                     
        $companyQuery->where("cl.Company_ID IS NOT NULL");
        $companyQuery->where("TRIM(cl.Company_ID) <> ''");
        $companyQuery->group("cl.Company_ID");
            
        
		$db = Core_Database::getInstance(); 
		$select = $db->select()->from(array('p'=>Core_Database::TABLE_PROJECTS),
            array('*')
        );

        //13992
        $select->join(array('cl'=>$companyQuery),
                'cl.Company_ID = p.Project_Company_ID',
                    array('Company_ID','CompanyName','AMName','CliCSDName')
        );
        //end 13992
                                    
        $select->joinLeft(array('st'=>'wo_tag_items'),
            'st.id = p.ServiceTypeId',
            array('ServiceTypeName'=>'wo_tag_item_name')
        );
        
        $select->joinLeft(array('et'=>'wo_tag_items'),
            'et.id = p.EntityTypeId',
            array('EntityTypeName'=>'wo_tag_item_name')
        );

        $select->joinLeft(array('csd'=>'operations_staff'),
            'csd.id = p.FSClientServiceDirectorId',
            array('CSDName'=>'name')
        );
        //13939
        //$select->where("p.ServiceTypeId is null OR p.ServiceTypeId=0  OR p.ServiceTypeId IN (56,57,58,59,60)");  
        
        //end 13798
        
		// apply filters
		if (is_array($filters) ) {
		    if (isset($filters['companyId']) ) {                
		        $select->where('p.Project_Company_ID=?', $filters['companyId']);
		    }

		    if (isset($filters['hideInactive']) && strtolower($filters['hideInactive']) == 'true') {
		        $select->where('p.Active = ?', 1);
		    }

		    if (!empty($filters['letter'])) {
		        if ($filters['letter'] == '#') {
                    // first letter is numeric
		            $select->where("?", new Zend_Db_Expr("ORD(Project_Name) >= ORD(0) AND ORD(Project_Name) <= ORD(9)") );
		        } else {
		            $select->where("p.Project_Name LIKE ?", "{$filters['letter']}%");
		        }
		    }

		    if (!empty($filters['assignedPricingRule'])) {
				$select->where("IFNULL(p.PricingRuleID,0) != 0");
		    }

		}
        $select->distinct();
		if (!empty($sort) ) {
            $select->order($sort);                
		}

		if ($offset != 0 || $limit != 0) {
			$select->limit($limit, $offset);
		}
        
		$result = new Core_Filter_Result($select);
		if ($count !== null) $count = count($result);

		$err = $errors->getErrors();
		if (!empty($err))
			return API_Response::fail();
        
        //13798
        $records = $result->toArray();        
        //end 13798
		return API_Response::success($this->makeProjectsArray($records));
	}
    
     
    /**
	 * Function for getting Clients
	 *
	 * @param string $user
	 * @param string $pass
	 * @param array $clients_ids
	 * @param string $sort
	 * @return API_Response
	 */
	public function getClientsList($login, $hash, $filters, $sort, $offset = 0, $limit = 0, &$count)
	{
        $errors = Core_Api_Error::getInstance();

        $authData = array('login'=>$login, 'password'=>$hash);
        $user = new Core_Api_AdminUser();
        $user->checkAuthentication($authData);
        if (!$user->isAuthenticate()) return API_Response::fail();

        $logger = Core_Api_Logger::getInstance();
        $logger->setUrl('getClientsList');
        $logger->setLogin($login);
        $logger->addLog(LOGGER_ERROR_NO, null, 'db request');

      
		$db = Core_Database::getInstance();
		$select = $db->select()->from(Core_Database::TABLE_CLIENTS);

		// apply filters
		if (is_array($filters) ) {
		    if (isset($filters['ClientID']) )
		        $select->where("ClientID=?", $filters['ClientID']);
            if (!empty($filters['Company_ID']) )
		        $select->where("Company_ID = '".trim($filters['Company_ID'])."'");
            if (isset($filters['CompanyName']) )
		        $select->where("CompanyName LIKE '%".trim($filters['CompanyName'])."%'");
            if (isset($filters['ContactName']) )
		        $select->where("ContactName LIKE '%".trim($filters['ContactName'])."%'");
            if (isset($filters['ContactPhone1']) )
		        $select->where("ContactPhone1 LIKE '%".trim($filters['ContactPhone1'])."%'");
            if (isset($filters['ContactPhone2']) )
		        $select->where("ContactPhone2 LIKE '%".trim($filters['ContactPhone2'])."%'");
            if (isset($filters['Email1']) )
		        $select->where("Email1 LIKE '%".trim($filters['Email1'])."%'");
            if (isset($filters['Email2']) )
		        $select->where("Email2 LIKE '%".trim($filters['Email2'])."%'");
            if (isset($filters['WebsiteURL']) )
		        $select->where("WebsiteURL LIKE '%".trim($filters['WebsiteURL'])."%'");
            if (isset($filters['UserName']) )
		        $select->where("UserName LIKE '%".trim($filters['UserName'])."%'");
            if (isset($filters['Password']) )
		        $select->where("Password LIKE '%".trim($filters['Password'])."%'");
            if (isset($filters['UserType']) )
		        $select->where("UserType LIKE '%".trim($filters['UserType'])."%'");
            if (isset($filters['Admin']) )
		        $select->where("Admin =?", (int)$filters['Admin']);
            if (isset($filters['AcceptTerms']) )
		        $select->where("AcceptTerms =?", (int)$filters['AcceptTerms']);
            if (isset($filters['AccountEnabled']) )
		        $select->where("AccountEnabled =?", (int)$filters['AccountEnabled']);
            if (!empty($filters['assignedPricingRule']))
                        $select->where("IFNULL(PricingRuleID,0) != 0");
            //13901
            //$filters['logindateGreater'] = '2013-05-08'; (example)
            //$filters['logindateLess'] = '2013-05-09'; (example)
            if(!empty($filters['logindateGreater'])){
                $logindateGreater = $filters['logindateGreater'];
                $logindateGreater .= " 00:00:01";                
                $select->where("LastLoginDate >= '".$logindateGreater."'");
            }
            if(!empty($filters['logindateLess'])){
                $logindateLess = $filters['logindateLess'];
                $logindateLess .= " 23:59:59";
                $select->where("LastLoginDate <= '".$logindateLess."'");
            }
            //end 13901            
		}
		if (!empty($sort) )
	    	$select->order($sort);
        
        //13901    
        $select_count = clone $select;
        $allRecords = Core_Database::fetchAll($select_count);
        if ($count !== null) {
            $count = count($allRecords);   
        }
        //end 13901
        

		if ($offset != 0 || $limit != 0)
			$select->limit($limit, $offset);
        //echo $select; exit;
		$result = new Core_Filter_Result($select);

		$err = $errors->getErrors();
		if (!empty($err))
			return API_Response::fail();

		return API_Response::success($this->makeClientsArray($result->toArray()));
	}
	
	public function createISO($login, $hash, $data) {
        $errors = Core_Api_Error::getInstance();

        $authData = array('login'=>$login, 'password'=>$hash);
        $user = new Core_Api_AdminUser();
        $user->checkAuthentication($authData);
        if (!$user->isAuthenticate()) return API_Response::fail();

        $logger = Core_Api_Logger::getInstance();
		
        $factory = new Core_Validation_Factory(Core_Validation_Factory::VALIDATION_TYPE_ISO_CREATE);

        $validationFields = $data;
        $Country = Core_Country::getCountryInfoByCode($validationFields["Country"]);
        if(!empty($Country))
        {
            if($Country["ZipRequired"] != "1" && trim($validationFields["Zip"]) == "")
            {
                $validationFields["Zip"] = "12345";
            } 
        }
        if(($validationFields["Country"] != 'CA' 
                || $validationFields["Country"] != 'US'))
        {
            $validationFields["Contact_Phone"] = "123-234-2324";
        }    
	$validation = $factory->getValidationObject($validationFields);

        if ($validation->validate() === false) {
            $errors->addErrors('6', $validation->getErrors());
			return API_Response::fail();
        }

		$iso = new Core_ISO();
        foreach( $data as $f=>$v ) {
            $iso->$f = $v;
        }

        $result = $iso->save();

		$err = $errors->getErrors();
  		if (!empty($err))
			return API_Response::fail();

        return API_Response::success(array($result));
	}
	
    /**
	 * Function for getting Clients
	 *
	 * @param string $user
	 * @param string $pass
	 * @param array $clients_ids
	 * @param string $sort
	 * @return API_Response
	 */
	public function getISOsList($login, $hash, $filters, $sort, $offset = 0, $limit = 0, &$count)
	{
        $errors = Core_Api_Error::getInstance();

        $authData = array('login'=>$login, 'password'=>$hash);
        $user = new Core_Api_AdminUser();
        $user->checkAuthentication($authData);
        if (!$user->isAuthenticate()) return API_Response::fail();
      
		$db = Core_Database::getInstance();
		$select = $db->select()->from(Core_Database::TABLE_ISO);

		// apply filters
		if (is_array($filters) ) {
		    if (isset($filters['Client_ID']) )
		        $select->where("Client_ID=?", $filters['Client_ID']);
            if (!empty($filters['Contact_Name']) )
		        $select->where($db->quoteInto("Contact_Name LIKE ?", '%' . $filters['Contact_Name'] . '%'));
		}

		if (!empty($sort) )
	    	$select->order($sort);

		if ($offset != 0 || $limit != 0)
			$select->limit($limit, $offset);
        //echo $select; exit;
		$result = new Core_Filter_Result($select);
		if ($count !== null) $count = count($result);

		$err = $errors->getErrors();
		if (!empty($err))
			return API_Response::fail();

		return API_Response::success($result->toArray());
	}

    public function updateISO($login, $hash, $id, $data){        
        $errors = Core_Api_Error::getInstance();

        $authData = array('login'=>$login, 'password'=>$hash);
        $user = new Core_Api_AdminUser();
        $user->checkAuthentication($authData);
        
        if (!$user->isAuthenticate()) return API_Response::fail();

        $iso = new Core_ISO();
        $iso->load($id);

        foreach($data as $field=>$val){
            if($val != $iso->$field) $iso->$field = $val;
        }

        $factory = new Core_Validation_Factory(Core_Validation_Factory::VALIDATION_TYPE_ISO_UPDATE);
        $validationFields = $data;

        $Country = Core_Country::getCountryInfoByCode($validationFields["Country"]);
        if(!empty($Country))
        {
            if($Country["ZipRequired"] != "1" && trim($validationFields["Zip"]) == "")
            {
                $validationFields["Zip"] = "12345";
            } 
        }
        if(($validationFields["Country"] != 'CA' 
                || $validationFields["Country"] != 'US'))
        {
            $validationFields["Contact_Phone"] = "123-234-2324";
        } 
	$validation = $factory->getValidationObject($validationFields);

        if ($validation->validate() === false) {
            $errors->addErrors('6', $validation->getErrors());
			return API_Response::fail();
        }
        try{
              $iso->State= $data['State'];
        }
        catch(Exception $e)
        {

        }
        $result = $iso->save();

        $err = $errors->getErrors();
  		if (!empty($err))
			return API_Response::fail();

        return API_Response::success(array($result));
    }

    //--- 13622
    public function getCompanyMasterList($user, $pass)
    {
        $logger = Core_Api_Logger::getInstance();
        $logger->setUrl('getCompanyMasterList');
        $logger->setLogin($user);
        $logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');

        $errors = Core_Api_Error::getInstance();

        $authData = array('login'=>$user, 'password'=>$pass);
        $user = new Core_Api_AdminUser();
        $user->checkAuthentication($authData);
        if (!$user->isAuthenticate()) return API_Response::fail();
        
        $db = Core_Database::getInstance();        
        $select = $db->select();
        $select->from(Core_Database::TABLE_CLIENTS,
                    array('CompanyName','Company_ID')
        );
        $select->where("AccountEnabled = 1");
        $select->where("MarkDelete = 0");
        $select->where("Company_ID IS NOT NULL");
        $select->where("trim( Company_ID ) <> ''");
        $select->group("Company_ID");
        $select->order("CompanyName ASC");
        $records = Core_Database::fetchAll($select); 
        
        $apiUser = new Core_Api_User();
        $apiTag = new Core_Api_WoTagClass();
        if(!empty($records) && count($records)>0)
        {
            for($i=0;$i<count($records);$i++)
            {
                $companyID = $records[$i]['Company_ID'];
                $apiTag->updateSystemStatus($companyID);
                $repClientID = $apiUser->getRepClientIDforCompany($companyID);
                $clientTagsInfo = $this->getClientTagsInfo($repClientID);                
                foreach($clientTagsInfo as $k=>$v)
                {
                    $records[$i][$k] = $v;
                }
            }
        }        
        return $records; 
    }

    //--- 13622
    public function updateCompanyTagsInfo($companyID,$tagFields,$adminUserName)
    {
        $apiTag = new Core_Api_WoTagClass();
        $apiUser = new Core_Api_User();  
        $repClientID = $apiUser->getRepClientIDforCompany($companyID);      
        $clientIDs = $apiUser->getClientIDs_InCompany($companyID);
        $clientIDArray = $apiUser->getClientIDArray_InCompany($companyID);
        if(empty($clientIDs)) return;
        
        //--- insert if empty
        foreach($clientIDArray as $clientID)
        {
            //--- does client ext info exist?
            $db = Core_Database::getInstance();  
            $select = $db->select();
            $select->from('clients_ext');
            $select->where("ClientId = '$clientID'");
            $records = Core_Database::fetchAll($select);
            if(empty($records) && count($records)==0)
            {
                $insertFields = array("ClientId"=>$clientID,
                                    "FSClientServiceDirectorId"=>0,
                                    "FSAccountManagerId"=>0
                                    );                                
                $result = Core_Database::insert('clients_ext',$insertFields);
            }
        }
        //--- rep-client tag values
        $repClientTags = $apiTag->getClientTagValues($repClientID);
        
        //--- update tags 
        $updateFields = array();
        foreach($tagFields as $k=>$v)
        {
            if(in_array($k,$this->TagCodeIdArray))
            {
                $updateFields[$k]=$v;
            }
        }
        
        if(count($updateFields)>0)
        {
            $criteria = "ClientId IN ($clientIDs)";
            $result = Core_Database::update("clients_ext",$updateFields,$criteria);
        }        
        //--- write to log
        $logs = array();
        foreach($updateFields as $k=>$v)
        {
            $repTag = $repClientTags[$k];
            $tagCodeID = $repTag['TagCodeId'];
            if(empty($repTag['TagItemId']) && empty($v)){                
            }else  if( $repTag['TagItemId'] != $v ) {
                $tagItemName =  $apiTag->getTagItemName_ByTagItemId($tagCodeID,$v);
                $from = empty($repTag['TagItemName'])?"":"from: ".$repTag['TagItemName'];
                $logs[] = "Change ".$repTag['TagCodeName']." $from <br/> To: ".$tagItemName;
            }            
        }        
        
        if(count($logs) > 0) 
        {
            $message = "Edited Client Profile:<br/>";
            foreach($logs as $log) $message.=$log."<br/>";
            
            $result = Core_Database::insert("cliprofile_action_log",
                                    array("clientid"=>$repClientID,
                                    "username"=>$adminUserName,
                                    "logdate"=>date("Y-m-d H:i:s"),
                                    "action"=>$message,
                                    "company_id"=>$companyID,
                                    "from_site"=>'admin' 
                                    )
                 );         
        }
    }
    
    //--- 13622
    public function getClientTagsInfo($clientID)
    {
        //--- does client ext info exist?
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from('clients_ext');
        $select->where("ClientId = '$clientID'");
        $records = Core_Database::fetchAll($select);
        if(empty($records) && count($records)==0)
        {
            $insertFields = array("ClientId"=>$clientID,
                                "FSClientServiceDirectorId"=>0,
                                "FSAccountManagerId"=>0
                                );                                
            $result = Core_Database::insert('clients_ext',$insertFields);            
        }

        //--- get data        
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from(array('ce'=>'clients_ext'),array('*'));
        
        //FSClientServiceDirectorId
        $select->joinLeft(array('csd'=>'operations_staff'),
                'csd.id=ce.FSClientServiceDirectorId',
                array('CSD_Name'=>'name')
        );                
        //FSAccountManagerId
        $select->joinLeft(array('am'=>'operations_staff'),
                'am.id=ce.FSAccountManagerId',
                array('AM_Name'=>'name')
        );
        //ClientStatusId
        $select->joinLeft(array('cl_status'=>'wo_tag_items'),
                'cl_status.id=ce.ClientStatusId',
                array('ClientStatus_Name'=>'wo_tag_item_name')
        );
        //ClientRelationshipId
        $select->joinLeft(array('cl_relship'=>'wo_tag_items'),
                'cl_relship.id=ce.ClientRelationshipId',
                array('ClientRelationship_Name'=>'wo_tag_item_name')
        );
        //SegmentId     
        $select->joinLeft(array('segment'=>'wo_tag_items'),
                'segment.id=ce.SegmentId',
                array('Segment_Name'=>'wo_tag_item_name')
        );
        //OriginalSellerId          
        $select->joinLeft(array('OriginalSeller'=>'wo_tag_items'),
                'OriginalSeller.id=ce.OriginalSellerId',
                array('OriginalSeller_Name'=>'wo_tag_item_name')
        );
        //ExecutiveId          
        $select->joinLeft(array('Executive'=>'wo_tag_items'),
                'Executive.id=ce.ExecutiveId',
                array('Executive_Name'=>'wo_tag_item_name')
        );
        //EntityTypeId          
        $select->joinLeft(array('EntityType'=>'wo_tag_items'),
                'EntityType.id=ce.EntityTypeId',
                array('EntityType_Name'=>'wo_tag_item_name')
        );
        //ServiceTypeId               
        $select->joinLeft(array('ServiceType'=>'wo_tag_items'),
                'ServiceType.id=ce.ServiceTypeId',
                array('ServiceType_Name'=>'wo_tag_item_name')
        );
        //SystemStatusId               
        $select->joinLeft(array('SystemStatus'=>'wo_tag_items'),
                'SystemStatus.id=ce.SystemStatusId',
                array('SystemStatus_Name'=>'wo_tag_item_name')
        );
        $select->where("ce.ClientId = ?", $clientID);
        $records = Core_Database::fetchAll($select);
        return $records[0];
    }

    //--- 13622
    public function getWorkOrderTagsInfo($winNum)
    {
        //--- get CompanyID
        $db = Core_Database::getInstance();
        $query = $db->select();
        $query->from(Core_Database::TABLE_WORK_ORDERS,array('WIN_NUM','Company_ID'));
        $query->where("WIN_NUM = '$winNum'");   
        $records =  $db->fetchAll($query);
        if(empty($records) && count($records)==0)  {
            return null;
        }
        $companyID = $records[0]['Company_ID'];
        $apiUser = new Core_Api_User();            
        $repClientID = $apiUser->getRepClientIDforCompany($companyID);
        $clientTagsInfo = $this->getClientTagsInfo($repClientID);  
        /*CSD_Name, FSClientServiceDirectorId, FSAccountManagerId, AM_Name   */
        $FSClientServiceDirectorId = $clientTagsInfo['FSClientServiceDirectorId'];
        $CSD_Name = $clientTagsInfo['CSD_Name'];
        $FSAccountManagerId = $clientTagsInfo['FSAccountManagerId'];
        $AM_Name = $clientTagsInfo['AM_Name'];

        //--- get win tags
        $db = Core_Database::getInstance();
        $query = $db->select();
        $query->from(array('woaf'=>'work_orders_additional_fields'),
            array('WINNUM','ServiceTypeId','EntityTypeId','FSClientServiceDirectorId','FSAccountManagerId')
        );
        $query->join(array('w'=>Core_Database::TABLE_WORK_ORDERS),
            'w.WIN_NUM = woaf.WINNUM',
            array('Company_ID')
        );
        //FSClientServiceDirectorId
        $query->joinLeft(array('csd'=>'operations_staff'),
                'csd.id=woaf.FSClientServiceDirectorId',
                array('CSD_Name'=>'name')
        );                
        //FSAccountManagerId
        $query->joinLeft(array('am'=>'operations_staff'),
                'am.id=woaf.FSAccountManagerId',
                array('AM_Name'=>'name')
        );
        //EntityTypeId          
        $query->joinLeft(array('EntityType'=>'wo_tag_items'),
                'EntityType.id=woaf.EntityTypeId',
                array('EntityType_Name'=>'wo_tag_item_name')
        );
        //ServiceTypeId               
        $query->joinLeft(array('ServiceType'=>'wo_tag_items'),
                'ServiceType.id=woaf.ServiceTypeId',
                array('ServiceType_Name'=>'wo_tag_item_name')
        );
        
        $query->where("woaf.WINNUM = '$winNum'");   
       
                  
        $records =  $db->fetchAll($query);
        if(empty($records) && count($records)==0) 
        {
            
            return array('WINNUM'=>$winNum,'ServiceTypeId'=>0,'EntityTypeId'=>0,'FSClientServiceDirectorId'=>$FSClientServiceDirectorId,'FSAccountManagerId'=>$FSAccountManagerId,'CSD_Name'=>$CSD_Name,'AM_Name'=>$AM_Name,'EntityType_Name'=>'','ServiceType_Name'=>'');
        } 
        else 
        {
            $rec = $records[0];
            //$rec['FSClientServiceDirectorId']=$FSClientServiceDirectorId;
            //$rec['CSD_Name']=$CSD_Name;
            $rec['FSAccountManagerId']=$FSAccountManagerId;
            $rec['AM_Name']=$AM_Name;
            return $rec;   
        }
    }
    
    //--- 13622
    public function getClientActivityLog($filters,$forCount=0,$sort='',$limit=0,$offset=0)
    {
        $logFields = array('logdate','username','action','clientid');
        $clientFields = array('Company_ID');
        $sortFields = array('logdate','Company_ID','username','action');
        $filterFields = array('Company_ID','username','logdateGreater','logdateLess');
        
        if (empty($sort)) $sort = 'logdate DESC';
        if(!empty($forCount))
        {
            $logFields = array('Num'=>'(Count(*))');
            $clientFields = array();
        }
        
        $apiUser = new Core_Api_User();
        
        $db = Core_Database::getInstance();
        $query = $db->select();
        $query->from(array('log'=>'cliprofile_action_log'),$logFields);
        
        $query->joinLeft(array('cl'=>Core_Database::TABLE_CLIENTS),
                'cl.ClientID = log.clientid',
                $clientFields
        );
        foreach($filters as $searchKey=>$searchValue)
        {
            //--- Company
            if($searchKey=="Company_ID") {
                if(!empty($searchValue)) {
                    $clientIDArray = $apiUser->getClientIDArray_InCompany($searchValue);
                    if(!empty($clientIDArray) && count($clientIDArray) > 0){
                        $query->where("log.clientid IN (?)",$clientIDArray);                         
                    }
                }
            }
            //--- username
            if($searchKey=="username") {
                if(!empty($searchValue)) {
                    $query->where("log.username = '$searchValue'");                                   
                }
            }
            //--- logdateGreater
            if($searchKey=="logdateGreater") {
                if(!empty($searchValue)) {
                    $query->where("log.logdate >= '$searchValue'");
                }
            }
            //--- logdateLess
            if($searchKey=="logdateLess") {
                if(!empty($searchValue)) {
                    $query->where("log.logdate <= '$searchValue'",$clientIDArray);                   
                }
            }
        }//end of for
        $query->order($sort);
        if(!empty($limit))
        {
            $query->limit($limit,$offset);
        }
        $query->distinct();
        $records =  $db->fetchAll($query);
        
        if(!empty($forCount))
        {
             if(empty($records) && count($records)==0) return 0;
             else return $records[0]['Num'];
        }        
        else 
        {
            if(empty($records) && count($records)==0) {
                return null;   
            }
            else {
                return $records;
            }    
        }
    }
    //13798
    public static function getCompanyInfoArray()
    {   
        //$cache = Core_Cache_Manager::factory();
        //$cacheKey = Core_Cache_Manager::makeKey('CompanyInfoArray');
        //$CompanyInfoArray = $cache->load($cacheKey));        
        if (self::$CompanyInfoArray==null || count(self::$CompanyInfoArray)==0) {
            $db = Core_Database::getInstance();
            $query = $db->select();
            $query->from(array('cl'=>Core_Database::TABLE_CLIENTS),
                    array('Company_ID','CompanyName')
            );
                    
            $query->joinLeft(array('clex'=>"clients_ext"),
                    'clex.ClientId = cl.ClientID',
                    array('FSAccountManagerId')
            );   
            
            $query->joinLeft(array('am'=>'operations_staff'),
                    'am.id = clex.FSAccountManagerId',
                    array('AMName'=>'name')
            );
                         
            $query->where("cl.Company_ID IS NOT NULL");
            $query->where("TRIM(cl.Company_ID) <> ''");
            $query->group("cl.Company_ID");
            $records =  $db->fetchAll($query);
            
            self::$CompanyInfoArray = array();
            foreach($records as $rec){
                 $companyID = $rec['Company_ID'];
                 self::$CompanyInfoArray[$companyID]=$rec;
            }
            //$cache->save($CompanyInfoArray, $cacheKey, array('CompanyInfoArray'), LIFETIME_1DAY);
        }
        return self::$CompanyInfoArray;
    }
     //--- 13870 
    public function createDefaultProject($CompanyId,$csdId,$entityTypeId,$serviceTypeId)
    {
        	$db = Core_Database::getInstance();
		$companyList = $db->fetchCol("SELECT DISTINCT Company_ID FROM " . Core_Database::TABLE_CLIENTS . " WHERE Company_ID = ". $db->quoteInto("?", $CompanyId) . " AND NOT EXISTS (SELECT Project_ID FROM " . Core_Database::TABLE_PROJECTS . " WHERE Project_Company_ID = Company_ID AND Project_Name = 'Default Project')");
		
                if ($companyList && sizeof($companyList) > 0) {	
                        $fieldvals = array();                      
                        $fieldvals["FSClientServiceDirectorId"] = $csdId;
                        $fieldvals["EntityTypeId"] = $entityTypeId;
                        $fieldvals["ServiceTypeId"] = $serviceTypeId;
                        $fieldvals["Project_Name"] = "Default Project";
                        $fieldvals["Active"] = 1;
                        $fieldvals["CreatedBy"] = "fs_system";
                        $fieldvals["From_Email"] = "no-replies@fieldsolutions.us";
                        $fieldvals["To_Email"] = "no-replies@fieldsolutions.us";
                        $fieldvals["WO_Category_ID"] = 26;
                        $fieldvals["Headline"] ="General Assignment";
                        $fieldvals["Project_Company_ID"] = $CompanyId;
                        $fieldvals["AutoBlastOnPublish"] = 1;
                        $db->insert(Core_Database::TABLE_PROJECTS,$fieldvals) ;              
		}
    }
    // 576
    public function getCompaniesNotInActive($user, $pass, $fields = null, $sort = null, $group = null)
	{
            $logger = Core_Api_Logger::getInstance();
            $logger->setUrl('getProjects');
            $logger->setLogin($user);
            $logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');

            $errors = Core_Api_Error::getInstance();

            $authData = array('login'=>$user, 'password'=>$pass);
            $user = new Core_Api_AdminUser();
            $user->checkAuthentication($authData);
            if (!$user->isAuthenticate()) return API_Response::fail();

            $fieldsList = (!empty($fields)) ? $fields : Core_Database::getFieldList(Core_Database::TABLE_CLIENTS);

            $db = Core_Database::getInstance();
            $select = $db->select();

            $select->from(array('cl'=>Core_Database::TABLE_CLIENTS), $fieldsList);
			//
			$select->joinLeft(array('clex'=>"clients_ext"),
                'clex.ClientId = cl.ClientID',
                array('ClientStatusId'));
			 $select->where("ClientStatusId !='8'");
			//

            if(!empty($sort)) $select->order(trim($sort));
            if(!empty($group)) $select->group(trim($group));

            $result = Core_Database::fetchAll($select);
            $err = $errors->getErrors();
            if (!empty($err)) return API_Response::fail();
            return API_Response::success($this->makeCompaniesArray($result));
	}
}
