<?php
class Core_Api_FSExpertClass
{
    public function getFSExpertListForTech($techID,$sort='')
    {
        if(empty($techID)) {return; }
         $db = Core_Database::getInstance();  
         $select = $db->select();
         $select->from(array('et'=>'fsexpert_techs'),
                array('Id','FSExpertId','TechId','Date')
         );
         $select->join(array('e'=>'fsexperts'),'e.Id = et.FSExpertId',
                array('Code','Title','Content','FSExpertEnable','Abbreviation')
         );
         $select->where("et.TechId = '$techID'");
         if(!empty($sort)){
            $select->order($sort);
         }
         $records = Core_Database::fetchAll($select);  
         if(empty($records) || count($records)==0)  {
             return null;
         } else {
             $retArray = array();
             foreach($records as $rec){
                 $key = $rec['FSExpertId'];
                 $retArray[$key] = $rec;
                 //$retArray[] = $rec;
             }
             return $retArray;
         }
    }
    
    /*
    $filters-key
    TechId: 
    FSExpertId: 
    Category_ID:
    FSExpertCode:
    
    $filters = array('TechId'=>$techID,'FSExpertId'=>$FSExpertId);    
    */
    public function getFSExpertTechRecord($filters){
         $db = Core_Database::getInstance();  
         $select = $db->select();
         $select->from(array('et'=>'fsexpert_techs'),
                array('Id','FSExpertId','TechId','Date')
         );
         $select->join(array('e'=>'fsexperts'),'e.Id = et.FSExpertId',
                array('Code','Title','Content','FSExpertEnable')
         );
         $select->join(array('cat'=>Core_Database::TABLE_WO_CATEGORIES),
                'cat.Abbr = e.Code',
                array('Category_ID','Category','Abbr','TechSelfRatingColumn')
         );
         
         if(!empty($filters['Id'])){
            $select->where("et.Id = ? ",$filters['Id']);
         }
         if(!empty($filters['TechId'])){
            $select->where("et.TechId = ? ",$filters['TechId']);
         }
         if(!empty($filters['FSExpertId'])){
            $select->where("et.FSExpertId = ? ",$filters['FSExpertId']);
         } 
         if(!empty($filters['Category_ID']))
         {
            $select->where("cat.Category_ID = ?",$filters['Category_ID']);
         }         
         if(!empty($filters['FSExpertCode']))
         {
            $select->where("e.Code = ?",$filters['FSExpertCode']);
         }         
         $records = Core_Database::fetchAll($select);  
         if(empty($records) || count($records)==0)  {
             return null;
         } else {
             return $records[0];
         }
    }
    
    //--------------------------------------------
    /*
    $arrayData = array(
        array('TechId'=>15527,'FSExpertId'=>4,'HasFSExpert'=>0),
        array('TechId'=>15527,'FSExpertId'=>5,'HasFSExpert'=>1)
    )
    */
    public function saveFSExperts($arrayData)
    {
        if(empty($arrayData) || count($arrayData) ==0){
            return null;
        }
        /* [0] => Array
        (
            [TechId] => 12731
            [FSExpertId] => 1
            [HasFSExpert] => 1
        )
        */
        //print_r($arrayData);die();
        foreach($arrayData as $data){
            $techID = $data['TechId'];
            $FSExpertId = $data['FSExpertId'];
            $hasFSExpert = $data['HasFSExpert'];
            $filters = array('TechId'=>$techID,'FSExpertId'=>$FSExpertId);
            $FSExpertTechInfo = $this->getFSExpertTechRecord($filters);
            if(!empty($FSExpertTechInfo) && count($FSExpertTechInfo)>0){
                if($hasFSExpert==0){
                    $criteria = "TechId = '$techID' AND FSExpertId = '$FSExpertId'";
                    $db = Core_Database::getInstance();
                    $result = $db->delete('fsexpert_techs',$criteria);
                }                
            } else {
                if($hasFSExpert==1){
                    $fields = array('FSExpertId'=>$FSExpertId,
                                    'TechId'=>$techID,
                                    'Date' => date('Y-m-d H:i:s')
                    );
                    $result = Core_Database::insert("fsexpert_techs",$fields);  
                }
            }            
        }        
    }
    //--------------------------------------------
    /*
    $hasFSExpert: 1, 0
    */
    public function saveTechFSExpert($techID,$FSExpertId,$hasFSExpert)
    {
         $filters = array('TechId'=>$techID,'FSExpertId'=>$FSExpertId);
         $FSExpertTechInfo = $this->getFSExpertTechRecord($filters);
         if(!empty($FSExpertTechInfo)){
             if($hasFSExpert==0){
                 $criteria = "TechId = '$techID' AND FSExpertId = '$FSExpertId'";
                 $db = Core_Database::getInstance();
                 $result = $db->delete('fsexpert_techs',$criteria);
             }                
         } else {
             if($hasFSExpert==1){
                 $fields = array('FSExpertId'=>$FSExpertId,
                                'TechId'=>$techID,
                                'Date' => date('Y-m-d H:i:s')
                 );
                 $result = Core_Database::insert("fsexpert_techs",$fields);  
             }
         } 
    }    

    //--------------------------------------------
    public function saveTechHasFSExpert($techID,$FSExpertId)
    {
         $db = Core_Database::getInstance();  
         $select = $db->select();
         $select->from('fsexpert_techs',array('FSExpertId','TechId'));
         $select->where("TechId = ? ",$techID);
         $select->where("FSExpertId = ? ",$FSExpertId);
         $records = Core_Database::fetchAll($select);  
         if(empty($records) || count($records)==0){
             $fields = array('FSExpertId'=>$FSExpertId,
                            'TechId'=>$techID,
                            'Date' => date('Y-m-d H:i:s')
             );
             $result = Core_Database::insert("fsexpert_techs",$fields);  
         } 
         return $result;
    }    
    
    //--------------------------------------------
    /*
    $filters -> keys:
    - FSExpertCode
    - FSExpertId
    - Category_ID 
    - TechSelfRatingColumn: Example for value: electronicsSelfRating 
    - FSExpertCodeArray:
    */
    public function getTechIDArray_hasFSExpert($filters)
    {
         $db = Core_Database::getInstance();  
         $select = $db->select();
         $select->from(array('et'=>'fsexpert_techs'),
                array('Id','FSExpertId','TechId','Date')
         );
         $select->join(array('e'=>'fsexperts'),'e.Id = et.FSExpertId',
                array('Code','Title','Content')
         );
         $select->join(array('c'=>'wo_categories'),'c.Abbr = e.Code',
                array('Category_ID','TechSelfRatingColumn')
         );
         
         if(!empty($filters['FSExpertCode'])){             
            $select->where("e.Code = ? ",$filters['FSExpertCode']);
         }
         if(!empty($filters['FSExpertCodeArray'])){                 
            foreach($filters['FSExpertCodeArray'] as $FSExpertCode){
                $select->where("e.Code = ? ",$FSExpertCode);                
            }
         }         
         if(!empty($filters['FSExpertId'])){
            $select->where("et.FSExpertId = ? ",$filters['FSExpertId']);
         }
         if(!empty($filters['TechSelfRatingColumn'])){
            $select->where("c.TechSelfRatingColumn = ? ",$filters['TechSelfRatingColumn']);
         }         
         if(!empty($filters['Category_ID'])){
            $select->where("c.Category_ID = ? ",$filters['Category_ID']);
         }         
         $select->group("et.TechId");
         //print_r($select->__toString());
         $records = Core_Database::fetchAll($select);  
         $retArray = array();
         if(!empty($records) && count($records)>0){
            foreach($records as $rec){
                $retArray[] = $rec['TechId'];
            }
         }
         return $retArray;
    }
    //-----------------------------------------------
    /*
    $CatSelfRatingName: (Ex.) electronics
    $yesno: 1,0
    */
    public function getTechIDArray_HasOrNotFSExpert($CatSelfRatingName,$yesno)
    {
         if($yesno==1){
             $TechSelfRatingColumn = $CatSelfRatingName.'SelfRating';
             $filters = array('TechSelfRatingColumn'=>$TechSelfRatingColumn);
             return $this->getTechIDArray_hasFSExpert($filters);
         } else { //$yesno==0
             $TechSelfRatingColumn = $CatSelfRatingName.'SelfRating';
             $filters = array('TechSelfRatingColumn'=>$TechSelfRatingColumn);
             $hasExpert_techIDArray = $this->getTechIDArray_hasFSExpert($filters);
             
             $db = Core_Database::getInstance();  
             $select = $db->select();
             $select->from('TechBankInfo',array('TechID'));
             if(!empty($hasExpert_techIDArray) && count($hasExpert_techIDArray)>0){
                $select->where('TechId NOT IN (?)',$hasExpert_techIDArray);
             }             
             $records = Core_Database::fetchAll($select);  
             $retArray = array();
             if(!empty($records) && count($records)>0){
                foreach($records as $rec){
                    $retArray[] = $rec['TechID'];
                }
             }
             return $retArray;
         }
    }
    
    //-----------------------------------------------
    public function getFSExperts($enable=1,$filters=null){
         $db = Core_Database::getInstance();  
         $select = $db->select();
         $select->from('fsexperts');
         if($enable==1 || $enable==0){
             $select->where("FSExpertEnable = '$enable'");
         }
         $records = Core_Database::fetchAll($select);  
         if(empty($records) || count($records)==0)  {
             return null;
         } else {
             return $records;
         }
    }
    
    //-----------------------------------------------
    public function getFSExperts_ByCodes($FSExpertCodeArray){
         if(empty($FSExpertCodeArray)) return null;
         
         $db = Core_Database::getInstance();  
         $select = $db->select();
         $select->from(array('exp'=>'fsexperts'),
                array('FSExpertId'=>'Id','Code')
         );
         $select->join(array('cat'=>Core_Database::TABLE_WO_CATEGORIES),
                'cat.Abbr = exp.Code',
                array('Category_ID','TechSelfRatingColumn')
         );
         $select->where("exp.Code IN (?)",$FSExpertCodeArray);
         $records = Core_Database::fetchAll($select);  
         
         $ret = array();
         if(empty($records) || count($records)==0)  {
             return null;
         } else {
             foreach($records as $rec){
                 $code = $rec['Code'];
                 $ret[$code] = $rec;
             }
             return $ret;
         }
    }

    //-----------------------------------------------
    /*
    $filters-keys:
        - Category_ID: 
        - TechSelfRatingColumn:
        - FSExpertEnable:
        - Code
    */
    public function getFSExpert($filters)
    {
         $db = Core_Database::getInstance();  
         $select = $db->select();
         $select->from(array('exp'=>'fsexperts'),
                array('FSExpertId'=>'Id','Code','Title','Content','FSExpertEnable'));
         $select->join(array('cat'=>Core_Database::TABLE_WO_CATEGORIES),
                'cat.Abbr = exp.Code',
                array('Category_ID','Category','Abbr','TechSelfRatingColumn')
         );
         if(!empty($filters['Category_ID']))
         {
            $select->where("cat.Category_ID = ?",$filters['Category_ID']);
         }         
         if(!empty($filters['TechSelfRatingColumn']))
         {
            $select->where("cat.TechSelfRatingColumn = ?",$filters['TechSelfRatingColumn']);
         }         
         if(!empty($filters['FSExpertEnable']))
         {
            $select->where("exp.FSExpertEnable = ?",$filters['FSExpertEnable']);
         }         
         if(!empty($filters['Code']))
         {
            $select->where("exp.Code = ?",$filters['Code']);
         }         
         
         $records = Core_Database::fetchAll($select);  
         if(empty($records) || count($records)==0)  {
             return null;
         } else {
             return $records[0];
         }                
    }        
    
    public function getFSExpert_ByCatID($catID)
    {
        if(empty($catID)){
            return null;
        } else {
            $filters = array('Category_ID'=>$catID);
            return $this->getFSExpert($filters);
        }
    }
    /*
    $techSelfRatingColumn = 'electronicsSelfRating' (ex.)
    */
    public function isFSExpertEnable_TechSelfRatingColumn($techSelfRatingColumn){
        $filters = array('TechSelfRatingColumn'=>$techSelfRatingColumn,
            'FSExpertEnable'=>1
        );
        $FSExpert = $this->getFSExpert($filters);
        if(!empty($FSExpert) && count($FSExpert) > 0){
            return true;
        } else {
            return false;
        }
    }
    
    public function applyAllFSExpertRules_ForTech($techID)
    {
        $FSExpertCodeArray = $this->getFSExpertCodeArray();
        foreach($FSExpertCodeArray as $FSExpertCode)
        {
            $this->applyFSExpertRules_ForTech($techID,$FSExpertCode);
        }
    }
        
    public function applyFSExpertRules_ForTech($techID,$FSExpertCode)
    {
        $filters = array('Code'=>$FSExpertCode);
        $FSExpertInfo = $this->getFSExpert($filters);
        $catID = $FSExpertInfo['Category_ID'];
        $techSelfRatingColumn = $FSExpertInfo['TechSelfRatingColumn'];
        $FSExpertId = $FSExpertInfo['FSExpertId'];
        
        $countWOsCompletedInSkill = $this->countWOsCompletedInSkill_ForTech($techID,$catID);
        $skillSeftRating = $this->getSkillSelfRating($techID,$techSelfRatingColumn);
        
        $FSExpertRule = $this->getFSExpertRule($FSExpertCode);
        $minWOsCompletedInSkill = $FSExpertRule['Min_WOsCompletedInSkill'];
        $minSkillSeftRating = $FSExpertRule['Min_SkillSeftRating'];
        
        $normalCases = array('ATM','CCTV','Desktop','EnterpriseStorage','Kiosk','Printers','ServrHW','Wireless');
        
        if($FSExpertCode=='CAT5'){
            //special case
            /*AND Filled out Cabling Experience and Tools sections of Tech Profile (checked at least 1 box in each section), OR Preferred by BBS, CTS (if preferred, don't need to meet other requirements)
*/
            $isPreferredByBBS_CTS = $this->isPreferredByBBS_CTS($techID);
            $atLeastOneSkill = $this->atLeastOneSkillInEachSection($techID);
            if($countWOsCompletedInSkill >=$minWOsCompletedInSkill && $skillSeftRating >=$minSkillSeftRating && ($isPreferredByBBS_CTS || $atLeastOneSkill))
            {
               $this->saveTechFSExpert($techID,$FSExpertId,1);            
            }             
        } else if($FSExpertCode=='Copiers'){
            //special case
            /*OR passed 'Copier Certification'*/
            $hasCopier = $this->hasCopierCert($techID);
            if(($countWOsCompletedInSkill >=$minWOsCompletedInSkill && $skillSeftRating >=$minSkillSeftRating) || ($hasCopier))
            {
               $this->saveTechFSExpert($techID,$FSExpertId,1);            
            }                                     
        } else if($FSExpertCode=='Signage'){            
            //special case
            /*AND >=4 Self-Rating for Desktop/Laptop and >=4 Self-Rating for CAT5 Cabling
*/
            $DesktopSeftRating = $this->getSkillSelfRating($techID,'desktopSelfRating');
            $CAT5SeftRating = $this->getSkillSelfRating($techID,'cablingSelfRating');
            if($countWOsCompletedInSkill >= $minWOsCompletedInSkill && $skillSeftRating >=$minSkillSeftRating && $DesktopSeftRating >= 4 && $CAT5SeftRating >= 4)
            {
               $this->saveTechFSExpert($techID,$FSExpertId,1);            
            }             
        } else if($FSExpertCode=='DSL'){
            //special case
            /*AND >=4 Self-Rating for Telephony (Non-VoIP) and >=4 Self-Rating for CAT5 Cabling, AND Filled out Cabling Experience and Tools sections of Tech Profile (checked at least 1 box in each section)
*/
            $TelephonySeftRating = $this->getSkillSelfRating($techID,'telephonySelfRating');
            $CAT5SeftRating = $this->getSkillSelfRating($techID,'cablingSelfRating');
            $atLeastOneSkill = $this->atLeastOneSkillInEachSection($techID);
            if($countWOsCompletedInSkill >= $minWOsCompletedInSkill && $skillSeftRating >=$minSkillSeftRating && $TelephonySeftRating >= 4 && $CAT5SeftRating >= 4 && $atLeastOneSkill)
            {
               $this->saveTechFSExpert($techID,$FSExpertId,1);            
            }             
        } else if($FSExpertCode=='LowVolt'){
            //special case
            /*AND Filled out Cabling Experience and Tools sections of Tech Profile (checked at least 1 box in each section)
*/
            $atLeastOneSkill = $this->atLeastOneSkillInEachSection($techID);
            if($countWOsCompletedInSkill >= $minWOsCompletedInSkill && $skillSeftRating >=$minSkillSeftRating && $atLeastOneSkill)
            {
               $this->saveTechFSExpert($techID,$FSExpertId,1);            
            }             
            
        } else if($FSExpertCode=='POS'){
            //special case
            /*OR tech has an FLS ID#*/
            $hasFLSID = $this->hasFLSID($techID);
            if(($countWOsCompletedInSkill >= $minWOsCompletedInSkill && $skillSeftRating >=$minSkillSeftRating) || ($hasFLSID))
            {
               $this->saveTechFSExpert($techID,$FSExpertId,1);            
            }                                     
        } else if($FSExpertCode=='Routers'){
            //special case
            /*AND >=4 Self-Rating for CAT5 Cabling*/            
            $CAT5SeftRating = $this->getSkillSelfRating($techID,'cablingSelfRating');
            if($countWOsCompletedInSkill >= $minWOsCompletedInSkill && $skillSeftRating >=$minSkillSeftRating && $CAT5SeftRating >= 4)
            {
               $this->saveTechFSExpert($techID,$FSExpertId,1);            
            }                         
        } else if($FSExpertCode=='Tele'){
            //special case
            /*AND >=4 Self-Rating for CAT5 Cabling, AND Filled out Cabling AND Telephony Experience and Tools sections of Tech Profile (checked at least 1 box in each section)
*/
            $CAT5SeftRating = $this->getSkillSelfRating($techID,'cablingSelfRating');
            $atLeastOneSkill = $this->atLeastOneSkillInEachSection($techID);
            if($countWOsCompletedInSkill >= $minWOsCompletedInSkill && $skillSeftRating >=$minSkillSeftRating && $CAT5SeftRating >= 4 && $atLeastOneSkill)
            {
               $this->saveTechFSExpert($techID,$FSExpertId,1);            
            }             
        } else if($FSExpertCode=='VoIP'){
            //special case
            /*AND >=4 Self-Rating for CAT5 Cabling, AND Filled out Cabling  Experience and Tools sections of Tech Profile (checked at least 1 box in each section)
*/
            $CAT5SeftRating = $this->getSkillSelfRating($techID,'cablingSelfRating');
            $atLeastOneSkill = $this->atLeastOneSkillInEachSection($techID);
            if($countWOsCompletedInSkill >= $minWOsCompletedInSkill && $skillSeftRating >=$minSkillSeftRating && $CAT5SeftRating >= 4 && $atLeastOneSkill)
            {
               $this->saveTechFSExpert($techID,$FSExpertId,1);            
            }             
        } else {
            //for nolmal cases:
            if($countWOsCompletedInSkill >= $minWOsCompletedInSkill && $skillSeftRating >=$minSkillSeftRating)
            {
               $this->saveTechFSExpert($techID,$FSExpertId,1);            
            }             
        }

    }
    
    public function countWOsCompletedInSkill_ForTech($techID,$catID)
    {
         $db = Core_Database::getInstance();  
         $select = $db->select();
         $select->from(Core_Database::TABLE_WORK_ORDERS,
                        array('NUM'=>'Count(*)')
                );
         $select->where("Tech_ID = '$techID'");       
         $select->where("WO_Category_ID = '$catID'");       
         $select->where("Approved = 1");       
         $records = Core_Database::fetchAll($select);  
         $count = 0;
         if(!empty($records) && count($records)>0){
             $count = $records[0]['NUM'];
         }
         return $count;
    }
    
    public function getSkillSelfRating($techID,$techSelfRatingColumn)
    {
         $db = Core_Database::getInstance();  
         $select = $db->select();
         $select->from(Core_Database::TABLE_TECH_BANK_INFO,
                    array('TechID',"$techSelfRatingColumn")
         );
         $select->where("TechID = '$techID'");       
         $records = Core_Database::fetchAll($select);  
         $ret = 0;
         if(!empty($records) && count($records)>0){
             $ret = $records[0]["$techSelfRatingColumn"];
         }
         return $ret;        
    }
    
    public function getFSExpertCodeArray()
    {
         $db = Core_Database::getInstance();  
         $select = $db->select();
         $select->from("fsexperts",array('Code'));
         $select->where("FSExpertEnable = 1");
         //FSExpertEnable
         $records = Core_Database::fetchAll($select);  
         $retArray = array();
         foreach($records as $rec){
             $retArray[] = $rec['Code'];
         }
         return $retArray;
    }
    
    public function applyFSExpertRule_ForAllTech($FSExpertCode)
    {
        $filters = array('Code'=>$FSExpertCode);
        $FSExpertInfo = $this->getFSExpert($filters);
        $catID = $FSExpertInfo['Category_ID'];
        $techSelfRatingColumn = $FSExpertInfo['TechSelfRatingColumn'];
        $FSExpertId = $FSExpertInfo['FSExpertId'];
        
        $FSExpertRule = $this->getFSExpertRule($FSExpertCode);
        $minWOsCompletedInSkill = $FSExpertRule['Min_WOsCompletedInSkill'];
        $minSkillSeftRating = $FSExpertRule['Min_SkillSeftRating'];
        
        $normalCases = array('ATM','CCTV','Desktop','EnterpriseStorage','Kiosk','Printers','ServrHW','Wireless');
        
        $techArray_1 = $this->getTechIDArray_ForGreaterNumCompletedWO_ByCatID($catID,$minWOsCompletedInSkill);
        $techArray_2 = $this->getTechIDArray_ForGreaterSkillSelfRating($techSelfRatingColumn,$minSkillSeftRating);
        
        $techArray = array();
        
        //$techArray = array_intersect($techArray_1,$techArray_2);
            
        if($FSExpertCode=='CAT5'){
            //special case
             /*AND Filled out Cabling Experience and Tools sections of Tech Profile (checked at least 1 box in each section), OR Preferred by BBS, CTS (if preferred, don't need to meet other requirements)
*/
            $Array_isPreferredByBBS_CTS = $this->getTechIDArray_isPreferredByBBS_CTS();
            $Array_atLeastOneSkill = $this->getTechIDArray_atLeastOneSkillInEachSection();                    
            $techArray = array_merge($Array_isPreferredByBBS_CTS,$Array_atLeastOneSkill);
            $techArray = array_intersect($techArray,$techArray_1,$techArray_2);
        } else if($FSExpertCode=='Copiers'){
            //special case
            /*OR passed 'Copier Certification'*/
            $Array_hasCopier = $this->getTechIDArray_hasCopierCert();
            $techArray = array_intersect($techArray_1,$techArray_2);
            $techArray = array_merge($techArray,$Array_hasCopier);
            
        } else if($FSExpertCode=='Signage'){
            //special case
            /*AND >=4 Self-Rating for Desktop/Laptop and >=4 Self-Rating for CAT5 Cabling*/
            $Array_Desktop = $this->getTechIDArray_ForGreaterSkillSelfRating('desktopSelfRating',4);
            $Array_CAT5 = $this->getTechIDArray_ForGreaterSkillSelfRating('cablingSelfRating',4);
            $techArray = array_intersect($techArray_1,$techArray_2,$Array_Desktop,$Array_CAT5);            
        } else if($FSExpertCode=='DSL'){
            //special case
            /*AND >=4 Self-Rating for Telephony (Non-VoIP) and >=4 Self-Rating for CAT5 Cabling, AND Filled out Cabling Experience and Tools sections of Tech Profile (checked at least 1 box in each section)*/
            $Array_Telephony = $this->getTechIDArray_ForGreaterSkillSelfRating('telephonySelfRating',4);
            $Array_CAT5 = $this->getTechIDArray_ForGreaterSkillSelfRating('cablingSelfRating',4);
            $Array_atLeastOneSkill = $this->getTechIDArray_atLeastOneSkillInEachSection(); 
            $techArray = array_intersect($techArray_1,$techArray_2,$Array_Telephony,
                                            $Array_CAT5,$Array_atLeastOneSkill);
        } else if($FSExpertCode=='LowVolt'){
            //special case
            /* AND Filled out Cabling Experience and Tools sections of Tech Profile (checked at least 1 box in each section) */
            $Array_atLeastOneSkill = $this->getTechIDArray_atLeastOneSkillInEachSection(); 
            $techArray = array_intersect($techArray_1,$techArray_2,$Array_atLeastOneSkill);
        } else if($FSExpertCode=='POS'){            
            //special case
            /*OR tech has an FLS ID#*/
            $Array_hasFLSID = $this->getTechIDArray_hasFLSID();
            $techArray = array_intersect($techArray_1,$techArray_2);
            $techArray = array_merge($techArray,$Array_hasFLSID);            
        } else if($FSExpertCode=='Routers'){
            //special case
            /*AND >=4 Self-Rating for CAT5 Cabling*/            
            $Array_CAT5 = $this->getTechIDArray_ForGreaterSkillSelfRating('cablingSelfRating',4);
            $techArray = array_intersect($techArray_1,$techArray_2,$Array_CAT5);
        } else if($FSExpertCode=='Tele'){
            //special case
            /*AND >=4 Self-Rating for CAT5 Cabling, AND Filled out Cabling AND Telephony Experience and Tools sections of Tech Profile (checked at least 1 box in each section)
*/
            $Array_CAT5 = $this->getTechIDArray_ForGreaterSkillSelfRating('cablingSelfRating',4);
            $Array_atLeastOneSkill = $this->getTechIDArray_atLeastOneSkillInEachSection();
            $techArray = array_intersect($techArray_1,$techArray_2,$Array_CAT5,$Array_atLeastOneSkill);
        } else if($FSExpertCode=='VoIP'){
            //special case
            /*AND >=4 Self-Rating for CAT5 Cabling, AND Filled out Cabling  Experience and Tools sections of Tech Profile (checked at least 1 box in each section)
*/
            $Array_CAT5 = $this->getTechIDArray_ForGreaterSkillSelfRating('cablingSelfRating',4);
            $Array_atLeastOneSkill = $this->getTechIDArray_atLeastOneSkillInEachSection();
            $techArray = array_intersect($techArray_1,$techArray_2,$Array_CAT5,$Array_atLeastOneSkill);
        } else {                        
            //for nolmal cases:
            $techArray = array_intersect($techArray_1,$techArray_2);
        }

        if(!empty($techArray) && count($techArray) > 0) {
            $filters=array('FSExpertCode'=>$FSExpertCode);            
            $techIdsHaveFSExperts = $this->getTechIDArray_hasFSExpert($filters);
            $techNeedAddTag = array_diff($techArray,$techIdsHaveFSExperts);
            if(!empty($techNeedAddTag) && count($techNeedAddTag) > 0)
            {
                foreach($techNeedAddTag as $techID){
                     $fields = array('FSExpertId'=>$FSExpertId,
                            'TechId'=>$techID,
                            'Date' => date('Y-m-d H:i:s')
                            );
                     $result = Core_Database::insert("fsexpert_techs",$fields);  
                }
            }
        }            
    }
    
    
    public function getTechIDArray_ForGreaterNumCompletedWO_ByCatID($catID,$greaterNumCompletedWO)
    {
         $db = Core_Database::getInstance();  
/*         $select = $db->select();
         $select->from(Core_Database::TABLE_WORK_ORDERS,
                        array('Tech_ID','NUM'=>'Count(*)')
                );
         $select->where("Tech_ID IS NOT NULL"); 
         $select->where("Approved = 1"); 
         $select->where("WO_Category_ID = '$catID'"); 
         $select->group("Tech_ID");
         $select->having("Count(*) >= '$greaterNumCompletedWO'");         
         $records = Core_Database::fetchAll($select);  
         
         $techIDArray = array();
         if(!empty($records) && count($records)>0){
             foreach($records as $rec)
             {
                 $techIDArray[] = $rec['Tech_ID'];
             }
         }*/
		 
		$NumWOSelect = $db->select();
		$NumWOSelect->from(array("twcc" => "tech_wo_category_count"), array("Tech_ID"))
			->where("WO_Category_ID = ?", $catID)
			->where("completed >= ?", $greaterNumCompletedWO);
		$techIDArray = $db->fetchCol($NumWOSelect);

		return $techIDArray;
    }

    public function getTechIDArray_ForGreaterNumCompletedWO($FSExpertCodeArray,$FSExpertRules,$FSExpertInfos)
    {
         $db = Core_Database::getInstance(); 
         $ret = array();
         foreach($FSExpertCodeArray as $FSExpertCode){
             $catID = $FSExpertInfos[$FSExpertCode]['Category_ID'];
             $greaterNumCompletedWO = $FSExpertRules[$FSExpertCode]['Min_WOsCompletedInSkill'];			 
             
/*             $select = $db->select();
             $select->from(Core_Database::TABLE_WORK_ORDERS,
                            array('Tech_ID','NUM'=>'Count(*)')
                    );
             $select->where("Tech_ID IS NOT NULL"); 
             $select->where("Approved = 1");          
             $select->where("WO_Category_ID = '$catID'");          
             $select->group("Tech_ID");
             $select->having("Count(*) >= '$greaterNumCompletedWO'");         
             $records = Core_Database::fetchAll($select);  
             
             $techIDArray = array();
             if(!empty($records) && count($records)>0){
                 foreach($records as $rec)
                 {
                     $techIDArray[] = $rec['Tech_ID'];
                 }
             }  */
			 
			 $techIDArray = $this->getTechIDArray_ForGreaterNumCompletedWO_ByCatID($catID, $greaterNumCompletedWO);
             if(count($ret)>0){
                $ret = array_intersect($ret,$techIDArray);
				if (count($ret) == 0) break;
             } else {
                $ret =  $techIDArray;
             }
         }
         return $ret;
    }
    
    public function getTechIDArray_ForGreaterSkillSelfRating($techSelfRatingColumn,$greaterSkillSelfRating)
    {
         $db = Core_Database::getInstance();  
         $select = $db->select();
         $select->from(Core_Database::TABLE_TECH_BANK_INFO,
                    array('TechID',"$techSelfRatingColumn")
         );
         $select->where("TechID IS NOT NULL");       
         $select->where("$techSelfRatingColumn >= '$greaterSkillSelfRating'");       
         $records = Core_Database::fetchAll($select);  
         
         $techIDArray = array();
         if(!empty($records) && count($records)>0){
             foreach($records as $rec)
             {
                 $techIDArray[] = $rec['TechID'];
             }
         }
         return $techIDArray;
    }
    
     public function getTechIDArray_ForGreaterSkillSelfRating_ForMultiCodes($FSExpertCodeArray,$FSExpertRules,$FSExpertInfos)
    {
         $db = Core_Database::getInstance();  
         $select = $db->select();
         $select->from(Core_Database::TABLE_TECH_BANK_INFO,
                    array('TechID')
         );
         
         $select->where("TechID IS NOT NULL");       
         foreach($FSExpertCodeArray as $FSExpertCode){
             $techSelfRatingColumn = $FSExpertInfos[$FSExpertCode]['TechSelfRatingColumn'];
             $greaterSkillSelfRating = $FSExpertRules[$FSExpertCode]['Min_SkillSeftRating'];
             $select->where("$techSelfRatingColumn >= '$greaterSkillSelfRating'");       
         }         
         
         $records = Core_Database::fetchAll($select);  
         
         $techIDArray = array();
         if(!empty($records) && count($records)>0){
             foreach($records as $rec)
             {
                 $techIDArray[] = $rec['TechID'];
             }
         }
         return $techIDArray;
    }

    
    
    public function getFSExpertRule($FSExpertCode)
    {
        $WOsCompletedInSkill = 0;
        $skillSeftRating = 0;
        if($FSExpertCode=='ATM')
        {
            $WOsCompletedInSkill = 5; $skillSeftRating = 4;
        } else if($FSExpertCode=='CAT5'){
            $WOsCompletedInSkill = 5; $skillSeftRating = 4;
        } else if($FSExpertCode=='CCTV'){
            $WOsCompletedInSkill = 3; $skillSeftRating = 4;
        } else if($FSExpertCode=='Copiers'){
            $WOsCompletedInSkill = 3; $skillSeftRating = 4;
        } else if($FSExpertCode=='Desktop'){
            $WOsCompletedInSkill = 5; $skillSeftRating = 4;
        } else if($FSExpertCode=='Signage'){
            $WOsCompletedInSkill = 5; $skillSeftRating = 4;
        } else if($FSExpertCode=='DSL'){
            $WOsCompletedInSkill = 3; $skillSeftRating = 4;
        } else if($FSExpertCode=='EnterpriseStorage'){
            $WOsCompletedInSkill = 3; $skillSeftRating = 4;           
        } else if($FSExpertCode=='Kiosk'){
            $WOsCompletedInSkill = 3; $skillSeftRating = 4;
        } else if($FSExpertCode=='LowVolt'){
            $WOsCompletedInSkill = 3; $skillSeftRating = 4;
        } else if($FSExpertCode=='POS'){
            $WOsCompletedInSkill = 5; $skillSeftRating = 4;
        } else if($FSExpertCode=='Printers'){
            $WOsCompletedInSkill = 5; $skillSeftRating = 4;
        } else if($FSExpertCode=='Routers'){
            $WOsCompletedInSkill = 5; $skillSeftRating = 4;
        } else if($FSExpertCode=='ServrHW'){
            $WOsCompletedInSkill = 3; $skillSeftRating = 4;
        } else if($FSExpertCode=='Tele'){
            $WOsCompletedInSkill = 5; $skillSeftRating = 4;
        } else if($FSExpertCode=='VoIP'){
            $WOsCompletedInSkill = 5; $skillSeftRating = 4;
        } else if($FSExpertCode=='Wireless'){
            $WOsCompletedInSkill = 3; $skillSeftRating = 4;
        }
        return array('Min_WOsCompletedInSkill'=>$WOsCompletedInSkill,
            'Min_SkillSeftRating'=>$skillSeftRating
        );
    }

    public function getFSExpertRuleForMultiCodes($FSExpertCodeArray){
        $ret = array();
        foreach($FSExpertCodeArray as $FSExpertCode){
            $ret[$FSExpertCode]=$this->getFSExpertRule($FSExpertCode);
        }
        return $ret;
    }
    
    public function atLeastOneSkillInEachSection($techID)
    {        
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from(array('tbi'=>Core_Database::TABLE_TECH_BANK_INFO), 
                array("TechID")
        );
        $select->join(array('te'=>'tech_equipment'),
                'te.TechID = tbi.TechID',
                array()
        );
        $select->join(array("ts"=>"tech_skill"),
                'ts.TechID = tbi.TechID',
                 array()                
        );
        $select->where("tbi.Ladder = 1 OR (tbi.Tools IS NOT NULL AND tbi.Tools <> '') ");
        $select->where('tbi.TechID = ?',$techID);
        $select->group("tbi.TechID");
        
        $tbiRes = Core_Database::fetchAll($select);        
        if(!empty($tbiRes) && count($tbiRes) > 0) {
            return true;
        } else {
            return false;
        }        
    }

    public function getTechIDArray_atLeastOneSkillInEachSection()
    {        
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from(array('tbi'=>Core_Database::TABLE_TECH_BANK_INFO), 
                array("TechID")
        );
        $select->join(array('te'=>'tech_equipment'),
                'te.TechID = tbi.TechID',
                array()
        );
        $select->join(array("ts"=>"tech_skill"),
                'ts.TechID = tbi.TechID',
                 array()                
        );
        $select->where("tbi.Ladder = 1 OR (tbi.Tools IS NOT NULL AND tbi.Tools <> '') ");
        $select->group("tbi.TechID");
        $tbiRes = Core_Database::fetchAll($select);

        $TechIDArray = array();
        if(!empty($tbiRes) && count($tbiRes)>0){
            foreach($tbiRes as $rec){
                $TechIDArray[] = $rec['TechID'];
            }
        }        
        return $TechIDArray;
    }

    
    public function isPreferredByBBS_CTS($techID)
    {
        // PreferLevel CompanyID
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from("client_preferred_techs");        
        $select->where('Tech_ID = ?',$techID);
        $select->where('PreferLevel = 1');
        $select->where("CompanyID = 'BBS' OR CompanyID = 'CTS'");
        $records = Core_Database::fetchAll($select);
        
        if(!empty($records) && count($records) > 0){
            return true;
        } else {
            return false;
        }
    }

    public function getTechIDArray_isPreferredByBBS_CTS()
    {
        // PreferLevel CompanyID Tech_ID
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from("client_preferred_techs");        
        $select->where('PreferLevel = 1');
        $select->where("CompanyID = 'BBS' OR CompanyID = 'CTS'");
        $records = Core_Database::fetchAll($select);
        
        $retArray = array();
        if(!empty($records) && count($records)>0){
            foreach($records as $rec){
                $retArray[] = $rec['Tech_ID'];
            }
        }
        return $retArray;
    }
    
    public function hasCopierCert($techID)
    {
        //tech_id     ass_id     ispassed
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from("tech_ass");        
        $select->where('tech_id = ?',$techID);
        $select->where('ass_id = 1');
        $select->where("ispassed = 1");
        $records = Core_Database::fetchAll($select);
        if(!empty($records) && count($records) > 0){
            return true;
        } else {
            return false;
        }
    }

    public function getTechIDArray_hasCopierCert()
    {
        //tech_id     ass_id     ispassed
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from("tech_ass",array('tech_id'));        
        $select->where('ass_id = 1');
        $select->where("ispassed = 1");
        $records = Core_Database::fetchAll($select);

        $retArray = array();
        if(!empty($records) && count($records)>0){
            foreach($records as $rec){
                $retArray[] = $rec['tech_id'];
            }
        }
        return $retArray;
    }
    
    public function hasFLSID($techID)
    {
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from(Core_Database::TABLE_TECH_BANK_INFO,array('TechID','FLSID'));        
        $select->where('TechID = ?',$techID);
        $select->where("FLSID IS NOT NULL AND FLSID <> '' ");
        $records = Core_Database::fetchAll($select);
        if(!empty($records) && count($records) > 0){
            return true;
        } else {
            return false;
        }        
    }
    
    public function getTechIDArray_hasFLSID()
    {
        $db = Core_Database::getInstance();  
        $select = $db->select();
        $select->from(Core_Database::TABLE_TECH_BANK_INFO,array('TechID','FLSID'));        
        $select->where("FLSID IS NOT NULL AND FLSID <> '' ");
        $records = Core_Database::fetchAll($select);

        $retArray = array();
        if(!empty($records) && count($records)>0){
            foreach($records as $rec){
                $retArray[] = $rec['TechID'];
            }
        }
        return $retArray;
    }
}
