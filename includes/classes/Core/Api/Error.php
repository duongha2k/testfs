<?php
class Core_Api_Error
{
	static private $instance = NULL;
	private $errors = array();
	private $index = array();
	private $standardErrors = array(1 => 'External SOAP error',
	                                2 => 'Required fields are not set',
		                            3 => 'Authenticate is failed',
		                            4 => 'Not HTTPS protocol used',
		                            5 => 'Not enough permissions',
		                            6 => 'Validating error',
		                            7 => 'SQL error',
                                            8 => 'Restrict');

	private function __construct(){}

	private function __clone(){}

	/**
	 * Singleton
	 * @return Core_Api_Error
	 */
	public static function getInstance()
	{
		if (self::$instance == NULL)
			self::$instance = new Core_Api_Error();

		return self::$instance;
	}

    /**
     * hasErrors
     *
     * @access public
     * @return boolean
     */
    public function hasErrors()
    {
        return (bool) sizeof($this->errors);
    }

	private function makeStandardErrors()
	{
		/*$this->standardErrors[1] = 'External SOAP error';
		$this->standardErrors[2] = 'Required fields are not set';
		$this->standardErrors[3] = 'Authenticate is failed';*/
	}

	/**
	 * Enter description here...
	 *
	 * @param int $errorId
	 * @param string $description
	 */
	public function addError($errorId, $description= null)
	{
		//$this->makeStandardErrors();
		$error['description'] = $description;
		$error['error_id'] = $errorId;
		$error['id'] = $this->standardErrors[$errorId];
		$this->errors[] = $error;

//		$logger = Core_Api_Logger::getInstance();
//		$logger->addLog(LOGGER_ERROR_YES, $errorId, $description);
	}

	/**
	 * Enter description here...
	 *
	 * @param int $errorId
	 * @param array $errors
	 */
	public function addErrors($errorId, $errors)
	{
		//$this->makeStandardErrors();
		foreach ($errors as $val) {
			$error['description'] = $val;
    		$error['error_id'] = $errorId;
    		$error['id'] = $this->standardErrors[$errorId];
    		$this->errors[] = $error;
		}

//		$logger = Core_Api_Logger::getInstance();
//		$logger->addLog(LOGGER_ERROR_YES, $errorId, $this->standardErrors[$errorId]);
	}

	/**
	 * Enter description here...
	 *
	 * @return string
	 */
	public function getErrors()
	{
		if (!empty($this->errors)) {
			return $this->makeXML();
		} else {
			return '';
		}
	}

	public function getErrorArray() {
		return $this->errors;
	}

	/**
	 * Enter description here...
	 *
	 * @return string
	 */
	private function makeXML()
	{
		$dom = new DOMDocument("1.0");

	    $root = $dom->createElement('errors');
	    foreach ($this->errors as $key=>$er) {
		    $id = $dom->createAttribute('id');
		    $id->appendChild($dom->createTextNode($er['error_id']));

		    $type = $dom->createElement('type');
		    $type->appendChild($dom->createTextNode($er['id']));

		    $description = $dom->createElement('description');
		    $description->appendChild($dom->createTextNode($er['description']));

		    $error = $dom->createElement('error');
		    $error->appendChild($id);
		    $error->appendChild($type);
		    $error->appendChild($description);

		    $root->appendChild($error);
	    }
	    $dom->appendChild($root);

		return $dom->saveXML();
	}

	/**
	 * Return text description of errors
	 * @return string
	 */
	public function toString()
	{
		$result = '';
		foreach ($this->errors as $error) {
			$result .= '[' . $error['id'] . '] ' . $error['description'] . "\n";
		}
		return $result;
	}

	public function __toString()
	{
		return $this->toString();
	}
	
	
	public function resetErrors()
	{
	    $this->errors = array();
	}
}
