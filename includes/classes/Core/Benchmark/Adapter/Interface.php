<?php 
interface Core_Benchmark_Adapter_Interface
{
    public function setData(array $data);
    public function result();
}

