<?php
class Core_Benchmark_Adapter_File implements Core_Benchmark_Adapter_Interface
{
    private $_data  = array();
    private $_handler;
    private $_file;

    /**
     * __construct
     * 
     * @param string $file  File name
     * @access public
     * @return void
     */
    public function __construct($file = null)
    {
        if ($file === null)
            $this->_file = APPLICATION_PATH.'/../var/benchmark/results.log';
        else
            $this->_file = $file;

        if ( !file_exists($this->_file) ) {
            $dir = dirname($this->_file);
            if ( file_exists($dir) && is_writable($dir) ) {
                ;
            } else if ( !file_exists($dir) ) {
                if ( ! @mkdir($dir, 0777, true) ) {
                    //  error create dir
                    return;
                }
            } else if ( !is_writable($dir) ) {
                // error directory is not writable
                return;
            }
        }

        if ( function_exists('bzopen') && file_exists($this->_file) && is_readable($this->_file) && filesize($this->_file)/1024/1024 > 10 ) {
            //  If file greater than 10Mb, pack it and remove source
            if ( false !== ($arch = bzopen($this->_file.'.'.(date('mdYHi')).'.bz2', "w")) && false !== ($handle = fopen($this->_file, "r")) ) {
                while ( !feof($handle) ) {
                    $string = fread($handle, 4096);
                    bzwrite($arch, $string, strlen($string));
                }
                bzclose($arch);
                fclose($handle);
                unlink($this->_file);
            }
        }

        $this->_handler = fopen($this->_file, "a");
    }
    /**
     * __destruct
     * 
     * @access public
     * @return void
     */
    public function __destruct()
    {
        if ( is_resource($this->_handler) )
            fclose($this->_handler);
    }
    /**
     * setData
     * 
     * @param array $data Points
     * @access public
     * @return Core_Benchmark_Adapter_File
     */
    public function setData(array $data)
    {
        $this->_data = $data;
        return $this;
    }
    /**
     * result  
     * 
     * @access public
     * @return void
     */
    public function result()
    {
        if ( !is_resource($this->_handler) )
            return;

        flock($this->_handler, LOCK_EX);

        fwrite($this->_handler, sprintf("\n+-| date: %s |------------------------------------------------------------------+\n", date("m/d/Y H:i:s")));
        fwrite($this->_handler, "|   time,s   |   delta,s  |                             messages                                 |\n");
        fwrite($this->_handler, "+------------+------------+----------------------------------------------------------------------+\n");

        if ( sizeof($this->_data) ) {
            $_f = explode(' ', $this->_data[0]['point']);
            $first = $_f[0] + $_f[1];
            fwrite($this->_handler, sprintf("|%12.8f|%12.8f| %-69s|\n", 0.0, 0.0, $this->_data[0]['message']));

            for ( $i=1; $i < sizeof($this->_data); ++$i ) {

                $_p = explode(' ', $this->_data[$i-1]['point']);
                $prev = $_p[0] + $_p[1];
                $_c = explode(' ', $this->_data[$i]['point']);
                $current = $_c[0] + $_c[1];

                fwrite($this->_handler, sprintf("|%12.8f|%12.8f| %-69s|\n",
                    $current - $first,          //  execution time, s
                    $current - $prev,           //  delta, s
                    $this->_data[$i]['message'])
                );
            }
        }

        fwrite($this->_handler, "+------------+------------+----------------------------------------------------------------------+\n");

        fflush($this->_handler);
        flock($this->_handler, LOCK_UN);
    }
}

