<?php
class Core_Benchmark_Adapter_Factory
{
    /**
     * create  
     * 
     * @param string|Core_Benchmark_Adapter_Interface $adapter 
     * @static
     * @access public
     * @return Core_Benchmark_Adapter_Interface
     */
    static public function create($adapter)
    {
        if ( empty($adapter) )
            throw new Zend_Exception('Adapter can\'t be empty');

        if ( $adapter instanceof Core_Benchmark_Adapter_Interface )
            return $adpter;
        if ( is_string($adapter) ) {
            $adapter = 'Core_Benchmark_Adapter_'.ucfirst(trim($adapter));
            try {
                return new $adapter();
            } catch ( Exception $e ) {
                throw new Zend_Exception('Class "'.$adapter.'" not found.');
            }
        }
    }
}

