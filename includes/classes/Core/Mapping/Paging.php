<?php

class Core_Mapping_Paging
{
    private $curPage;
    private $totalCount;
    private $perPage;
    private $companyId;

    public function setCompanyId($val)
    {
        $this->companyId = $val;
    }
    
    public function setCurPage($val)
    {
        $this->curPage = $val;
    }
    
    public function setTotalCount($val)
    {
    
        $this->totalCount = $val;
        
    }
    
    public function setPerPage($val)
    {
        $this->perPage = $val;
    }
    
    public function getTechPaging()
    { 
        $buf = floor($this->totalCount / $this->perPage);
        $pagesCount = $buf == $this->totalCount/$this->perPage ? $buf : $buf+1;
        
        if ($pagesCount > 1) {
            if ($this->curPage <= 6) {
                $left = 1;
                if ($pagesCount > 9) {
                    $right = 9;
                } else {
                    $right = $pagesCount;
                }
            } elseif ($pagesCount-$this->curPage <= 6) {
                $left = $pagesCount-9;
                $right = $pagesCount;
            } else {
                $left = $this->curPage-4;
                $right = $this->curPage+4;
            }
        
            if ($this->curPage != 1) {
                $paging = "<span onClick=\"techCangePage(1); processTechData('".$this->companyId."');\" style='padding: 3px;cursor: pointer;'><b>first</b></span>";
                $paging .= "<span onClick=\"techCangePage(".($this->curPage-1)."); processTechData('".$this->companyId."');\" style='padding: 3px;cursor: pointer;'><b><</b></span>";
            }
            for ($i=$left; $i <= $right; $i++) {
                if ($i == $this->curPage) {
                	$paging .= "<span onClick=\"techCangePage($i); processTechData('".$this->companyId."');\" style='padding: 3px;cursor: pointer;'>(<b>$i</b>/$pagesCount)</span>";
                } else {
                    $paging .= "<span onClick=\"techCangePage($i); processTechData('".$this->companyId."');\" style='padding: 3px;cursor: pointer;'>$i</span>";
                }
            }
            
            if ($this->curPage != $pagesCount){
                $paging .= "<span onClick=\"techCangePage(".($this->curPage+1)."); processTechData('".$this->companyId."');\" style='padding: 3px;cursor: pointer;'><b>></b></span>";
                $paging .= "<span onClick=\"techCangePage($pagesCount); processTechData('".$this->companyId."');\" style='padding: 3px;cursor: pointer;'><b>last</b></span>";
            }
        } else {
            $paging = '';
        }
        return $paging;
    }

    public function getWoPaging()
    { 
        $buf = floor($this->totalCount / $this->perPage);
        $pagesCount = $buf == $this->totalCount/$this->perPage ? $buf : $buf+1;
        
        if ($pagesCount > 1) {
            if ($this->curPage <= 6) {
                $left = 1;
                if ($pagesCount > 9) {
                    $right = 9;
                } else {
                    $right = $pagesCount;
                }
            } elseif ($pagesCount-$this->curPage <= 6) {
                $left = $pagesCount-9;
                $right = $pagesCount;
            } else {
                $left = $this->curPage-4;
                $right = $this->curPage+4;
            }
        
            if ($this->curPage != 1) {
                $paging = "<span onClick=\"woCangePage(".($this->curPage-1)."); processWoData('".$this->companyId."');\" style='padding: 3px;cursor: pointer;'><b><</b></span>";
                $paging = "<span onClick=\"woCangePage(1); processWoData('".$this->companyId."');\" style='padding: 3px;cursor: pointer;'><b>first</b></span>";
            }
            for ($i=$left; $i <= $right; $i++) {
                if ($i == $this->curPage) {
                	$paging .= "<span onClick=\"woCangePage($i); processWoData('".$this->companyId."');\" style='padding: 3px;cursor: pointer;'>(<b>$i</b>/$pagesCount)</span>";
                } else {
                    $paging .= "<span onClick=\"woCangePage($i); processWoData('".$this->companyId."');\" style='padding: 3px;cursor: pointer;'>$i</span>";
                }
            }
            
            if ($this->curPage != $pagesCount) {
                $paging .= "<span onClick=\"woCangePage(".($this->curPage+1)."); processWoData('".$this->companyId."');\" style='padding: 3px;cursor: pointer;'><b>></b></span>";
                $paging .= "<span onClick=\"woCangePage($pagesCount); processWoData('".$this->companyId."');\" style='padding: 3px;cursor: pointer;'><b>last</b></span>";
            }
        } else {
            $paging = '';
        }
        return $paging;
    }

    public function getAssignWoPaging()
    { 
        $buf = floor($this->totalCount / $this->perPage);
        $pagesCount = $buf == $this->totalCount/$this->perPage ? $buf : $buf+1;
        
        if ($pagesCount > 1) {
            if ($this->curPage <= 6) {
                $left = 1;
                if ($pagesCount > 9) {
                    $right = 9;
                } else {
                    $right = $pagesCount;
                }
            } elseif ($pagesCount-$this->curPage <= 6) {
                $left = $pagesCount-9;
                $right = $pagesCount;
            } else {
                $left = $this->curPage-4;
                $right = $this->curPage+4;
            }
        
            if ($this->curPage != 1) {
                $paging = "<span onClick=\"assignWoCangePage(".($this->curPage-1)."); processAssignWoData('".$this->companyId."');\" style='padding: 3px;cursor: pointer;'><b><</b></span>";
                $paging = "<span onClick=\"assignWoCangePage(1); processAssignWoData('".$this->companyId."');\" style='padding: 3px;cursor: pointer;'><b>first</b></span>";
            }
            for ($i=$left; $i <= $right; $i++) {
                if ($i == $this->curPage) {
                	$paging .= "<span onClick=\"assignWoCangePage($i); processAssignWoData('".$this->companyId."');\" style='padding: 3px;cursor: pointer;'>(<b>$i</b>/$pagesCount)</span>";
                } else {
                    $paging .= "<span onClick=\"assignWoCangePage($i); processAssignWoData('".$this->companyId."');\" style='padding: 3px;cursor: pointer;'>$i</span>";
                }
            }
            
            if ($this->curPage != $pagesCount) {
                $paging .= "<span onClick=\"assignWoCangePage(".($this->curPage+1)."); processAssignWoData('".$this->companyId."');\" style='padding: 3px;cursor: pointer;'><b>></b></span>";
                $paging .= "<span onClick=\"assignWoCangePage($pagesCount); processAssignWoData('".$this->companyId."');\" style='padding: 3px;cursor: pointer;'><b>last</b></span>";
            }
        } else {
            $paging = '';
        }
        return $paging;
    }
    public function getAssignTechPaging()
    { 
        $buf = floor($this->totalCount / $this->perPage);
        $pagesCount = $buf == $this->totalCount/$this->perPage ? $buf : $buf+1;
        
        if ($pagesCount > 1) {
            if ($this->curPage <= 6) {
                $left = 1;
                if ($pagesCount > 9) {
                    $right = 9;
                } else {
                    $right = $pagesCount;
                }
            } elseif ($pagesCount-$this->curPage <= 6) {
                $left = $pagesCount-9;
                $right = $pagesCount;
            } else {
                $left = $this->curPage-4;
                $right = $this->curPage+4;
            }
        
            if ($this->curPage != 1) {
                $paging = "<span onClick=\"techCangePage(1); processAssignTechData('".$this->companyId."');\" style='padding: 3px;cursor: pointer;'><b>first</b></span>";
                $paging .= "<span onClick=\"techCangePage(".($this->curPage-1)."); processAssignTechData('".$this->companyId."');\" style='padding: 3px;cursor: pointer;'><b><</b></span>";
            }
            for ($i=$left; $i <= $right; $i++) {
                if ($i == $this->curPage) {
                	$paging .= "<span onClick=\"techCangePage($i); processAssignTechData('".$this->companyId."');\" style='padding: 3px;cursor: pointer;'>(<b>$i</b>/$pagesCount)</span>";
                } else {
                    $paging .= "<span onClick=\"techCangePage($i); processAssignTechData('".$this->companyId."');\" style='padding: 3px;cursor: pointer;'>$i</span>";
                }
            }
            
            if ($this->curPage != $pagesCount){
                $paging .= "<span onClick=\"techCangePage(".($this->curPage+1)."); processAssignTechData('".$this->companyId."');\" style='padding: 3px;cursor: pointer;'><b>></b></span>";
                $paging .= "<span onClick=\"techCangePage($pagesCount); processAssignTechData('".$this->companyId."');\" style='padding: 3px;cursor: pointer;'><b>last</b></span>";
            }
        } else {
            $paging = '';
        }
        return $paging;
    }
}