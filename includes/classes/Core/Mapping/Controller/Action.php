<?php
class Core_Mapping_Controller_Action extends Zend_Controller_Action
{
    protected $_login;
    protected $_password;

    public function init()
    {
        $request = $this->getRequest();
        /* Index Controller view scripts paths */
        $this->view->addScriptPath(MAPPING_MODULE_PATH.'views/full');

        //  Check authentication
        $auth = new Zend_Session_Namespace('Auth_User');
        $this->_login    = $auth->login;
        $this->_password = $auth->password;
        $this->_userType = $auth->userType;
        unset($auth);
        if ( !$this->_login || !$this->_password ) {
            throw new Core_Auth_Exception('Authentication required');
        }

    }
    
}

