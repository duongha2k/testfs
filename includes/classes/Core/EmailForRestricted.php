<?php

class Core_EmailForRestricted {

    protected $companyId;
    protected $companyName;
    protected $toMail;
    protected $toName;
    protected $username;
    protected $restrictedtype;
    protected $wostatus;
    protected $wonum;

    function __construct()
    {
        $this->mail = new Core_Mail();
    }

    public function setCompanyId($companyId)
    {
        $this->companyId = $companyId;
    }

    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName;
    }

    public function settoMail($toMail)
    {
        $this->toMail = $toMail;
    }

    public function settoName($toName)
    {
        $this->toName = $toName;
    }

    public function setusername($username)
    {
        $this->username = $username;
    }
    
    //1:    Full
    //2:    Partial
    //3:    auto 21days
    //4:    Full open
    //5:    auto 21days full open
    public function setrestrictedtype($restrictedtype)
    {
        $this->restrictedtype = $restrictedtype;
    }
    
    public function setwostatus($wostatus)
    {
        $this->wostatus = $wostatus;
    }
    
    public function setwonum($wonum)
    {
        $this->wonum = $wonum;
    }
    
    public function sendMail()
    {
        $FullCompanyName = $this->companyName;
        $CompanyCode = $this->companyId;
        $username = $this->username;
        $FromEmail="no-replies@fieldsolutions.com";
        $FormName="FieldSolutions   ";
        $ToEmail=$this->toMail;
        $ToName = $this->toName;
        $Subject="Site Access Restricted: ".$CompanyCode;
        $restrictedstr = "";
        if($this->restrictedtype==1)
        {
            $restrictedstr = " have had their site access restricted by ".$username.".";
            //Users from [FullCompanyName] ([CompanyCode])
        }
        else if($this->restrictedtype==2)
        {
            $restrictedstr = " have had their site access partially restricted by ".$username.".";
            //Users from [FullCompanyName] ([CompanyCode])
        }
        else if($this->restrictedtype==3)
        {
            $restrictedstr = " have had their site access restricted by the 21-Day Idle Account Restriction Tool.";
            //Users from [FullCompanyName] ([CompanyCode])
        }
        else if($this->restrictedtype==4)
        {
            $Subject="Site Restriction Lifted: ".$CompanyCode;
            $restrictedstr = " have had their site access restriction lifted manually by ".$username.".";
            //Users from [FullCompanyName] ([CompanyCode])
        }
        else if($this->restrictedtype==5)
        {
            $Subject="Site Restriction Lifted: ".$CompanyCode;
            $restrictedstr = " had their site access restriction lifted automatically when ".$username." ";
            if($this->wostatus=="work done")
            {
                $restrictedstr.= $this->wostatus." WIN# ".$this->wonum.".";
            }
            else
            {
                $restrictedstr.= "marked WIN# ".$this->wonum." as Work Done.";
            }
        }
    
        $message = "Hello,
        
        Users from ".$FullCompanyName." (".$CompanyCode.") ".$restrictedstr."
        

        Thank you,
        
        Your FieldSolutions Team";

        $this->mail->setBodyText($message);
        $this->mail->setFromEmail($FromEmail);
        $this->mail->setFromName($FormName);
        $this->mail->setToEmail($ToEmail);
        $this->mail->setToName($ToName);
        $this->mail->setSubject($Subject);
        $this->mail->send();
    }
}