<?php
class Core_Country extends Core_LookupTable {
	
	public function __construct($country = NULL) {
		$this->aliasTableName = 'alias_country';
		$alias = $this->getCodeByAlias($country);
		if (!empty($alias)) $country = $alias;
		self::getByCode();
		parent::__construct($country);
	}
		
	public static function getByCode() {
        $cache = Core_Cache_Manager::factory();
        $cacheKey = Core_Cache_Manager::makeKey('countriesListById');
        
        if ( false === ($result = $cache->load($cacheKey)) ) {
            $fieldsList = array('Code','Name');

            $db = Core_Database::getInstance();
            $select = $db->select();

            $select->from(Core_Database::TABLE_COUNTRIES, $fieldsList);
			
            $result = $db->fetchPairs($select);

            $cache->save($result, $cacheKey, array('countriesListById'), LIFETIME_1DAY);
        }
		
		self::$lookupTable = $result;
		return $result;
	}
    
    public static function getCountryInfoByCode($countryCode)
    { 
        $db = Core_Database::getInstance();
        $select = $db->select();
        $select->from(Core_Database::TABLE_COUNTRIES);
		$_country = new Core_Country($countryCode);
		$country = $_country->getCode();
		if (!empty($country)) $countryCode = $country;
        $select->where("Code = ?", $countryCode);
        $results = Core_Database::fetchAll($select);   
        if(!empty($results)) return $results[0];
        else return null;
    }
}