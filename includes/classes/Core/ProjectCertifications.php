<?php
class Core_ProjectCertifications extends Core_TableRelatedItem {	
	public function __construct() {
		parent::__construct(Core_Database::TABLE_PROJECT_CERTIFICATION, "Project_ID", "certification_id");
	}
	
	public function getProjectCertification($techIds) {
		if (is_numeric($techIds)) $techIds = array($techIds);
		if (!is_array($techIds)) return false;
		return $this->getItemsWithRelatedIds($techIds);
	}
	
	public function getProjectWithCertification($skillIds) {
		if (is_numeric($skillIds)) $skillIds = array($skillIds);
		if (!is_array($skillIds)) return false;
		return $this->getRelatedIdsWithItems($skillIds);
	}

	public function getProjectWithCertificationAny($skillIds) {
		if (is_numeric($skillIds)) $techIds = array($skillIds);
		if (!is_array($skillIds)) return false;
		return $this->getRelatedIdsWithItems($skillIds, false);
	}
	
	public static function getMapping() {
		$cache = Core_Cache_Manager::factory();
		$mapping = $cache->load('projectCertificationsMapping');
		if ($mapping) return $mapping;
		
		$db = Core_Database::getInstance();
		$select = $db->select();
		$select->from(Core_Database::TABLE_CERTIFICATIONS, array('id', 'name'));
		$mapping = $db->fetchPairs($select);
		$cache->save($mapping, 'projectCertificationsMapping');
		
		return $mapping;
	}
}