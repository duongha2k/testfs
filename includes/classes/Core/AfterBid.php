<?php
// quick wrapper for auto assign checking
//require_once(dirname(__FILE__) . "/../../../htdocs/clients/autoassign/includes/AutoAssign.php");

class Core_AfterBid {
	private $params;

	function __construct($params = null) {
		$this->params = $params;
	}

	public function run() {
		if (empty($this->params) || !is_array($this->params)) return;
		$wo = $this->params[6];
		$tech = $this->params[7];
		if ($wo->Company_ID == 'FLS' && strtolower($tech->getFLSstatus()) == 'registered') {
			$flsEmail = new Core_EmailForNoFlsIdBid();
			$flsEmail->setTech($this->params[1]);
			$flsEmail->sendMail();
		}
		unset($this->params[6]);
		unset($this->params[7]);
		Core_BackgroundProcess::run("/usr/bin/php -f " . WEBSITE_LIB_PATH . "/afterBid.php", $this->params);
	}
}
