<?php
class Core_TechLanguages
{
    
    //--- 13535
    public function getLanguages()
    {
	
        $db = Core_Database::getInstance();        
        $select = $db->select();
        $select->from('languages');
        $select->order("displayorder ASC");

        $records = Core_Database::fetchAll($select);
        if(empty($records)) return null;
        else return $records;                
    }
    
    public function getTechLanguages($techID)
    {
        //--- get all language
        $languages = $this->getLanguages();        
        
        //--- tech language 
        $db = Core_Database::getInstance();        
        $select = $db->select();
        $select->from('tech_languages');
        $select->where("TechID='$techID'");
        $techLanguages =  Core_Database::fetchAll($select);
        
        //--- add properties
        for($i=0;$i<count($languages);$i++)
        {
            $langID = $languages[$i]['id'];
            $languages[$i]['TechID'] = '';
            $languages[$i]['SpeakerLevel'] = ''; 
            
            if(!empty($techLanguages))
            {
                foreach($techLanguages as $techLang)
                {
                    if($techLang['LangID']==$langID)
                    {
                        $languages[$i]['TechID'] = $techLang['TechID'];
                        $languages[$i]['SpeakerLevel'] = $techLang['SpeakerLevel'];
                        break;
                    }                    
                }
            }
        }
        //--- return
        return $languages;
    }
    
    public function existTechLanguage($techID,$langID)
    {
        $db = Core_Database::getInstance();        
        $select = $db->select();
        $select->from('tech_languages');
        $select->where("TechID = '$techID'");
        $select->where("LangID = '$langID'");
        $records = Core_Database::fetchAll($select);
        if(empty($records)) return false;
        else return true;        
    }
    
    /*
    * $speakerLevel = 0: No language skill for this tech.
    *               = 1: Competent Speaker
    *               = 2: Fluent Speaker
    */
    public function saveTechLanguage($techID,$langID,$speakerLevel)
    {
        if($speakerLevel==0)
        {
            $criteria = "TechID='$techID' AND LangID='$langID'";
            $db = Core_Database::getInstance();
            $result = $db->delete('tech_languages',$criteria);            
        }
        else
        {
            if(!$this->existTechLanguage($techID,$langID))
            {
                $fields = array('TechID'=>$techID,'LangID'=>$langID,'SpeakerLevel'=>$speakerLevel);
                $result = Core_Database::insert("tech_languages",$fields);            
            }
            else
            {
                $criteria = "TechID='$techID' AND LangID='$langID'";
                $fields = array('SpeakerLevel'=>$speakerLevel);
                $result = Core_Database::update("tech_languages",$fields,$criteria);
            }        
            return $result;
        }
    }
    /*
    * $rating   = 0: Any
    *           = 1: Competent Speaker
    *           = 2: Fluent Speaker
    * return : array contain TechIDs
    */
    public function findTechIDs_ByLangAndRating($langID,$rating)
    {
        $db = Core_Database::getInstance();        
        $select = $db->select();
        $select->from('tech_languages');        
        $select->where("LangID = '$langID'");
        if(!empty($rating))
        {
            $select->where("SpeakerLevel = '$rating'");
        }
        $records = Core_Database::fetchAll($select);
        if(empty($records)) 
        {
            return null;            
        }
        else 
        {
            $techs = array();
            foreach($records as $rec)
            {
                $techs[]=$rec['TechID'];
            }            
        }
        return $techs;        
    }
    
}
  

