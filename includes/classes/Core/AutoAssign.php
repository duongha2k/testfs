<?php
class Core_AutoAssign {
	public static function getTimeRemaining($win_num) {
		$db = Core_Database::getInstance();
		$select = $db->select();
		$select->from(Core_Database::TABLE_P2T_TIMER, array(new Zend_Db_Expr('TIMESTAMPDIFF(MINUTE, NOW(), Scheduled)')))
			->where('WIN_NUM = ?', $win_num);
		$result = $db->fetchOne($select);
		if (!$result || $result <= 0) $result = 0;
		return $result;
	}
	
	public static function clearTimer($win_num) {
		// clears old already ran timers and removes the one requested to clear
		$db = Core_Database::getInstance();
		$db->delete(Core_Database::TABLE_P2T_TIMER, "WIN_NUM = $win_num OR (Ran IS NOT NULL AND NOW() > DATE_ADD(Ran, INTERVAL 14 DAY))");
	}
	
	public static function setTimer($win_num, $delay) {
		$db = Core_Database::getInstance();
		self::clearTimer($win_num);
		$db->insert(Core_Database::TABLE_P2T_TIMER, array('WIN_NUM' => $win_num, 'Added' => new Zend_Db_Expr('NOW()'), 'Scheduled' => new Zend_Db_Expr("DATE_ADD(DATE_FORMAT(NOW(), '%Y-%m-%d %H:%i:00'),INTERVAL $delay MINUTE)")));
	}
	
	public static function getTimerExpired() {
		$db = Core_Database::getInstance();
		$select = $db->select();
		$select->from(Core_Database::TABLE_P2T_TIMER, array('WIN_NUM'))
			->where('Ran IS NULL')
			->where('Scheduled <= NOW()');
		return $db->fetchCol($select);
	}
	
	public static function setTimerRan($win_nums) {
		if (!is_array($win_nums)) return false;
		$db = Core_Database::getInstance();
		$db->update(Core_Database::TABLE_P2T_TIMER, array('Ran' => new Zend_Db_Expr('NOW()')), array('WIN_NUM IN (?)' => $win_nums));
	}
	
	private static function execute($wins) {
		// call background process to execute single wo
		if (empty($wins)) return;
		$path = WEBSITE_LIB_PATH . "/../clients/autoassign/includes/executeAutoAssign.php";
//		$path = realpath($path);
		foreach ($wins as $win) {
			Core_BackgroundProcess::run("/usr/bin/php -f  $path", array($win));
		}
	}
	
	private static function executeSequentially($wins) {
		// call background process to execute batch of wo in sequence
		if (empty($wins)) return;
		$db = Core_Database::getInstance();
		$batchId = uniqid();
		$path = WEBSITE_LIB_PATH . "/../clients/autoassign/includes/executeAutoAssign.php";
//		$path = realpath($path);
		$db->update(Core_Database::TABLE_P2T_TIMER, array('BatchId' => $batchId), array('WIN_NUM IN (?)' => $wins));
		Core_BackgroundProcess::run("/usr/bin/php -f  $path", array('batch', $batchId));
	}
	
	public static function processTimers() {
		// run by cron to grab all expired timers, run auto assign for work orders in paralell for work orders where bidding techs haven't bidded on any other also expired work orders
		// all other work orders assigned one at a time
		$wins = self::getTimerExpired();
		if (empty($wins)) return;
		self::setTimerRan($wins);
		$db = Core_Database::getInstance();
		$select = $db->select();
		$select->from(Core_Database::TABLE_WORK_ORDER_BIDS, array('TechID'))
			->where('WorkOrderID IN (?)', $wins)
			->group('TechID')
			->having('COUNT(TechID) > 1');
		$techsWithMultipleBids = $db->fetchCol($select);
		
		// split list of wins into ones with and without dependencies
		$winsWithOutDependency = array();
		if (!empty($techsWithMultipleBids)) {
			$select = $db->select();
			$select->from(Core_Database::TABLE_WORK_ORDER_BIDS, array('WorkOrderID'))
				->where('WorkOrderID IN (?)', $wins)
				->where('TechID IN (?)', $techsWithMultipleBids)
				->group('WorkOrderID')
				->order('COUNT(TechID)');
			$winsWithDependency = $db->fetchCol($select);
			if (!empty($winsWithDependency)) {
				foreach ($wins as $id) {
					if (in_array($id, $winsWithDependency)) continue;
					$winsWithOutDependency[] = $id;
				}
			}
			else {
				$winsWithDependency = array();
				$winsWithOutDependency = $wins;
			}
		}
		else {
			$winsWithDependency = array();
			$winsWithOutDependency = $wins;
		}
		
		// run non-dependent work in paralell
		
		self::execute($winsWithOutDependency);
		
		// run dependent work sequentially
		
		self::executeSequentially($winsWithDependency);
	}	
}
