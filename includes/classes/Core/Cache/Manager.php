<?php
class Core_Cache_Manager
{    private static $cache = null;

    private function __construct()
    {
    }

    /**
     * Returns the object of cache (memcached or standard ZEND_CACHE)
     * 
     * @return Zend_Cache_Frontend_Class
     */
    public static function factory()
    {        
        if (self::$cache == null) {
            // If memcached is enabled
            if ( CACHE_USE_MEMCACHED ) {
                self::$cache = self::getMemcached();
            } else {
                // Use standard ZEND_CACHE                
		self::$cache = self::getFile();
            }
        }

        return self::$cache;
    }

    private static function getFile()
    {        
        $frontend = array(
            'lifetime' => CACHE_LIFE_TIME,
            'cache_id_prefix' => 'fs',
            'automatic_serialization' => true
        );

        $backend = array(
            'cache_dir' => CACHE_DIR_FOR_FILES
        );

        return Zend_Cache::factory('Core', 'File', $frontend, $backend);
    }

    private static function getMemcached()
    {
        $frontend = new Zend_Cache_Core(
    	array(
    	    'lifetime' => CACHE_LIFE_TIME,
    		'caching' => true,
    		'cache_id_prefix' => 'fs',
    		'write_control' => true,
    		'automatic_serialization' => true,
    		'ignore_user_abort' => true
    	) );


        $backend = new Zend_Cache_Backend_Memcached(
        	array(
        		'servers' => array( array(
        		'host' => CACHE_MEMCACHED_HOST,
        		'port' => CACHE_MEMCACHED_PORT,
        		'persistent' => CACHE_MEMCACHED_PERSISTENT
        		) ),
        		'compression' => false
        ) );

        return Zend_Cache::factory($frontend, $backend);
    }

    /**
     * makeKey
     *
     * Make cache story key
     * 
     * @param string ... 
     * @static
     * @access public
     * @return void
     */
    static public function makeKey()
    {
        $key = '';
        foreach ( func_get_args() as $arg ) {
            $key .= preg_replace('/[^\w]+/', '_', trim($arg)).'_';
        }
        $key = trim($key, '_');
        if ( empty($key) )
            throw new Zend_Exception('Cache key can\'t be empty');
        return $key;
    }
}
