<?php
class Core_EmailToWorkOrder_Processor_fattcsv extends Core_EmailToWorkOrder_Processor {	
	protected function parseEmail() {
		// parses email msg and returns array(API_WorkOrder wo, String company id) containing parsed info
		// return false indicates error / no company
		// overwrite to parse other formats
		$message = $this->message;
		if ($message->isMultipart()) {
			foreach (new RecursiveIteratorIterator($message) as $part) {
				try {
					$type = strtok($part->contentType, ';');
//					echo "$type\n";
					switch ($type){
						case 'application/octet-stream':
						case 'text/csv':
							$messageCsv = $part;
							break;
						case 'text/plain':
						case 'text/html':
							$messageMain = $part;
							break;
					}
				} catch (Zend_Mail_Exception $e) {
					// ignore
				}
			}
		}


		$attachment = $this->convertEncoding($messageCsv);
		$body = $this->convertEncoding($messageMain);
		$Company = strstr($body, 'Company:');
		if (empty($Company)) $Company = "";
		else $Company = trim(str_replace("Company:", "", $Company));
		$f = tmpfile();
		fwrite($f,$attachment);
		fseek($f,0);
		$columns = fgetcsv($f);
		$wos = array();
		$additionalWOFields = array();
		while ($row = fgetcsv($f)) {
			if (empty($row[0])) continue;
			$wo = new API_WorkOrder();
			foreach ($row as $idx => $val) {
				// go through csv columns
				if (!empty($val)) $val = trim($val);
				switch ($columns[$idx]) {
					case 'ID':
						$wo->Tech_ID = $val;
//						$wo->Tech_ID = 19806;
						break;
					case 'Code':
						$additionalWOFields["Flex_Pay_Code"] = $val;
						break;
					case 'Amount':
						$wo->calculatedTechHrs = $val;
						break;
					case 'Reg Rate':
					case 'OT Rate':
						if (empty($wo->Tech_Bid_Amount))
							$wo->Tech_Bid_Amount = ltrim($val, "$");
						break;
					case 'Amount Type':
						$wo->Amount_Per = 'Hour';
						break;
					case 'Project':
						$wo->Project_ID = $val;
//						$wo->Project_ID = 5902;
						break;
					case 'Week Ending':
						try {
							$endDate = new Zend_Date($val, 'YYYYMMdd');
							$wo->EndDate = $endDate->toString('MM/dd/YYYY');
						}
						catch (Exception $e) {
						}
						try {
							$startDate = new Zend_Date($val, 'YYYYMMdd');
							$startDate->sub(13, Zend_Date::DAY);
							$wo->StartDate = $startDate->toString('MM/dd/YYYY');
						}
						catch (Exception $e) {
						}
						break;
				}
			}
			if (empty($wo->Project_ID) || empty($wo->Tech_ID)) continue;
			
			$db = Core_Database::getInstance();
			$select = $db->select();
			$select->from(array("ts" => "tech_site"), array("SiteNumber"))
                        	->where("techid = ?", $wo->Tech_ID)
                        	->where("Project_ID = ?", $wo->Project_ID)
				->limit(1);
			$siteNumber = $db->fetchOne($select);
			$site = new Core_Site();
			$site->load($siteNumber, $wo->Project_ID, "FATT");
			$wo->SiteName = $site->SiteName;
			$wo->Site_Contact_Name = $site->Site_Contact_Name;
			$wo->Address = $site->Address;
			$wo->Zipcode = $site->City;
			$wo->State = $site->State;
			$wo->Zipcode = $site->Zipcode;
			$wo->SitePhone = $site->SitePhone;
			$wo->SiteFax = $site->SiteFax;
			$wo->SiteEmail = $site->SiteEmail;
			$wo->Country = $site->Country;
			$wo_id = new Zend_Date();
			$uniqueFileIdentifier = substr(md5($attachment), -4);
			$wo->WO_ID = $wo_id->toString('YYYYMMdd') . "$uniqueFileIdentifier-" .  (count($wos) + 1);
			$wo->Approved = 1;
			$wo->PayMax  = $wo->Tech_Bid_Amount;
			foreach ($wo as $i=>$val) {
				if (empty($wo->$i)) unset($wo->$i);
			}
			$wos[] = $wo;
		}
		fclose($f);
		return array($wos, $Company, $additionalWOFields, array());
	}
}
