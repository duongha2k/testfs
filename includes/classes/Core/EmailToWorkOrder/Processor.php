<?php
class Core_EmailToWorkOrder_Processor {
	protected $message;
	public function __construct($msg) {
		$this->message = $msg;
	}
	
	protected function convertHtml($nodes) {
		$msg = "";
		$msg .= $this->convertChild($nodes);
//		echo $msg;
		return $msg;
	}
		
	protected function convertChild($child) {
		$msg = "";
		foreach ($child as $c) {
			if ($c->hasChildNodes()) $msg .= $this->convertChild($c->childNodes);
			else {
				switch ($c->nodeName) {
					case 'br':
						$msg .= "\n";
						break;
					case 'p':
						$msg .= "\n" . $c->nodeValue . "\n";
						break;
					default:
						$msg .= $c->nodeValue;
				}
			}
		}
		return $msg;
	}
	
	protected function processWorkOrder($wos, $Company, $additionalWOFields, $extraFields) {
		// create or update wo using parsed info from email
		$db = Core_Database::getInstance();
		$select = $db->select();
		$select->from(array("g" => "wo_gateway_auth"), array("ClientID"))
			->join(array("c" => "clients"), "g.ClientID = c.ClientID", array("Company_ID", "Username", "Password"))
			->where("token = ?", $Company)->limit(1);

		$cred = $db->fetchAll($select);
		if (empty($cred)) return false;
		$cred = $cred[0];
		if (md5($cred["ClientID"] . "^FIELD_EMAIL_GATEWAY!") != $Company) return false;

		if (is_object($wos)) $wos = array($wos);

		$results = array();
		$c = new Core_Api_Class;
		$w = new Core_Api_WosClass;

		foreach ($wos as $idx=>$wo) {
			$filter = new API_WorkOrderFilter;
			$filter->WO_ID = $wo->WO_ID;
			$filter->Company_ID = $cred["Company_ID"];
			unset($wo->Company);
	//		getWorkOrders($user, $pass, $fields, $timeStampFrom, $timeStampTo, $timeStampType, $filters, $offset = 0, $limit = 0, &$countRows = NULL, $sort = "")
			$res = $c->getWorkOrders($cred["Username"], $cred["Password"], "", null, null, null, $filter);
			if (!$res->success || sizeof($res->data) == 0) {
				$doApprove = false;
				if ($wo->Approved == 1) {
					$doApprove = true;
					$wo->Approved = NULL;
				}
				$apiRes = $c->createWorkOrder($cred["Username"], $cred["Password"], $wo->ShowTechs == 1, $wo, true);
				if ($doApprove && $apiRes->success) {
					$wo->Approved = 1;
					$apiRes = $c->updateWorkOrder($cred["Username"], $cred["Password"], $apiRes->data[0]->WIN_NUM, $wo, true);
				}
			}
			else if (!empty($res->data[0]->WIN_NUM)) {
				$apiRes = $c->updateWorkOrder($cred["Username"], $cred["Password"], $res->data[0]->WIN_NUM, $wo, true);
			}
			else
				$apiRes = false;
			if ($apiRes && !empty($additionalWOFields)) {
				$c->updateWorkOrderAdditionalFields($apiRes->data[0]->WIN_NUM, $additionalWOFields);

			}
			if ($apiRes && !empty($extraFields)) {
				// save visit info for base visit
				$w->saveBaseWOVisit($apiRes->data[0]->WIN_NUM,$extraFields['StartType'],
                                        !empty($wo->StartDate) ? date_format(new DateTime($wo->StartDate), "Y-m-d") : NULL,$wo->StartTime,
                                        !empty($wo->EndDate) ? date_format(new DateTime($wo->EndDate), "Y-m-d") : NULL,$wo->EndTime,
                                        $extraFields['EstimatedDuration'],$extraFields['TechArrivalInstructions']);
/*				$w->saveBaseWOVisit($apiRes->data[0]->WIN_NUM,$extraFields['StartType'],
					date_format(new DateTime($wo->StartDate), "Y-m-d"),$wo->StartTime,
					date_format(new DateTime($wo->EndDate), "Y-m-d"),$wo->EndTime,$extraFields['EstimatedDuration'],$extraFields['TechArrivalInstructions']);*/
			}

			$results[] = $apiRes;
		}
		return $results;
	}
	
	protected function convertEncoding($message) {
		$msg = $message->getContent();
		try {
			switch ($message->contentTransferEncoding) {
				case 'base64':
					$msg = base64_decode($msg);
					break;
				case 'quoted-printable':
					$msg = quoted_printable_decode($msg);
					break;
			}
		} catch (Exception $e) {}
		return $msg;
	}
	
	protected function parseEmail() {
		// parses email msg and returns array(API_WorkOrder wo, String company id) containing parsed info
		// return false indicates error / no company
		// overwrite to parse other formats
		$msg = $this->convertEncoding($this->message);
		$fieldMapping = array(
			"Company" => "Company",
			"Client Work Order ID #" => "WO_ID",
			"Project" => "Project_Name",
			"Client Purchase Order #" => "PO",
			"Call Type" => "Type_ID",
			"Headline" => "Headline",
			"Start Type" => "StartType",
			"Est. Duration" => "EstimatedDuration",
			"Arrival Instructions" => "TechArrivalInstructions",
	//		"Category" => "WO_Category_ID",
			"Work Start Date" => "StartDate",
			"Work End Date" => "EndDate",
			"Work Start Time" => "StartTime",
			"Work End Time" => "EndTime",
			"Max Client Offer" => "PayMax",
			"per" => "Amount_Per",
			"Qty Devices" => "Qty_Devices",
			"Tech ID" => "Tech_ID",
			"Agreed Amount" => "Tech_Bid_Amount",
			"Trip Charge" => "TripCharge",
			"Publish" => "ShowTechs",
			"Region" => "Region",
			"Duration" => "Duration",
			"Site Name" => "SiteName",
			"Site Number" => "SiteNumber",
			"Site Address" => "Address",
			"Site Contact Name" => "Site_Contact_Name",
			"Site Fax" => "SiteFax",
			"Site Email" => "SiteEmail",
			"City" => "City",
			"State" => "State",
			"Zipcode" => "Zipcode",
			"Country" => "Country",
			"Site Phone" => "SitePhone",
			"Work Description" => "Description",
			"Tech Requirements and Tools Required" => "Requirements",
			"Special Instructions" => "SpecialInstructions",
			"Special Instructions (viewable only by the assigned tech)" => "Ship_Contact_Info",
			"General Info 1" => "General1",
			"General Info 2" => "General2",
			"General Info 3" => "General3",
			"General Info 4" => "General4",
			"General Info 5" => "General5",
			"General Info 6" => "General6",
			"General Info 7" => "General7",
			"General Info 8" => "General8",
			"General Info 9" => "General9",
			"General Info 10" => "General10"
		);

		$validColumns = array_keys($fieldMapping);
		preg_match_all("/(?:^(?:(" . implode("|", $validColumns) . "):\s)(.*))|(.*)/im", $msg, $matches);

		$b = array();
		$wo = new API_WorkOrder;
		$additionalWOFields = array();
		$extraFields = array();
		if (!empty($matches[1]) && !empty($matches[3])) {
			$fields = array();
			$lastField = null;
			foreach ($matches[1] as $k => $v) {
				if (empty($v)) {
					if ($k > 0 && !empty($matches[1][$k - 1]))
						$lastField = $k - 1;
					if ($lastField != null && empty($matches[1][$k - 1]) && empty($matches[4][$k - 1])) {
						$matches[3][$lastField] .= "\n" . $matches[4][$k];
						$f = $matches[1][$lastField];
						$val = trim($matches[3][$lastField]);
						$b[$f] = $val;
						$wo->$f = $val;
						if (!property_exists($wo, $f))
							 $extraFields[$f] = str_replace("\n", " ", $val);
					}
					continue;
				}
				$matches[1][$k] = $fieldMapping[$v];
				$matches[3][$k] = trim($matches[3][$k]);
				switch ($matches[1][$k]) {
					case 'Type_ID':
						$matches[3][$k] = $matches[3][$k] == 'Install' ? 1 : 2;
						break;
					case 'ShowTechs':
						$matches[3][$k] = strtolower($matches[3][$k]) == 'yes' ? 1 : 0;
						break;
					case 'PayMax':
						$matches[3][$k] = str_replace("$", "", $matches[3][$k]);
						break;
					case 'StartType':
						$extraFields['StartType'] = $matches[3][$k] == 'Window' ? 2 : 1;
						break;
					default:
						if (!property_exists($wo, $matches[1][$k]))
							$extraFields[$matches[1][$k]] = $matches[3][$k];
						break;
				}
				$f = $matches[1][$k];
				$b[$f] = trim($matches[3][$k]);
				$wo->$f = trim($matches[3][$k]);
				if ($f == "Company") {
					$Company = $wo->$f == NULL ? ltrim(rtrim($matches[3][$k])) : $wo->$f;
				}

			}
		}
		return array($wo, $Company, $additionalWOFields, $extraFields);
	}

	public function run() {
		$message = $this->message;
		$result = $this->parseEmail();
		$wo = $result[0];
		$Company = $result[1];
		$additionalWOFields = $result[2];
		$extraFields = $result[3];

		if (empty($Company)) return false;
		// get user credentials and create wo
//		$Company = "03022dcbbf30335cd5f5861242416f6b";
		//$wo->Project_Name = "Skynet-2";
		$apiRes = $this->processWorkOrder($wo, $Company, $additionalWOFields, $extraFields);
//		var_dump($apiRes);
		
		$contactMail = new Core_Mail();
		$contactMail->setFromName("Email To Work Order");
		$contactMail->setFromEmail("no-replies@fieldsolutions.com");
		$contactMail->setToEmail("tngo@fieldsolutions.com");
		$contactMail->setSubject("Email To WO Ran");
		$contactMail->setBodyText(print_r($wo,true) . "\n" . print_r($apiRes, true));

		$contactMail->send();

		return true;
	}
}
