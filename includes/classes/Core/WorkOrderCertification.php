<?php
class Core_WorkOrderCertification implements Core_ObjectFromArray {
	protected $WIN_NUM;
	protected $certification_id;
	protected static $mapping;
	protected $row;
	
	public function __construct($WIN_NUM = NULL, $certification_id = NULL) {
		$this->WIN_NUM = $WIN_NUM;
		$this->certification_id = $certification_id;
		self::init();
	}
	
	private function load() {
		$db = Core_Database::getInstance();
		$table = new Zend_Db_Table(Core_Database::TABLE_WORK_ORDER_CERTIFICATION);
		$this->row = $table->fetchRow($table->select(Zend_Db_Table::SELECT_WITH_FROM_PART)
			->where("WIN_NUM = ?", $this->WIN_NUM)
			->where("certification_id = ?", $this->certification_id));
	}
	
	public function loadRow() {
		$this->load();
		return $this->row;
	}
	
	private function prepareRow() {
		$this->load();
		$db = Core_Database::getInstance();
		$table = new Zend_Db_Table(Core_Database::TABLE_WORK_ORDER_CERTIFICATION);
		if (empty($this->row)) $this->row = $table->createRow();
		$this->row->WIN_NUM = $this->WIN_NUM;
		$this->row->certification_id = $this->certification_id;
	}

	public function save() {
		$this->prepareRow();
		try {
			$this->row->save();
		}
		catch (Exception $e) {
		}
	}
	
	public function delete() {
		if (empty($this->row)) $this->load();
		if (empty($this->row)) return false;
		try {
			$this->row->delete();
		}
		catch (Exception $e) {
		}
	}

	public static function init() {
		if (!empty(self::$mapping)) return;
		self::$mapping = Core_WorkOrderCertifications::getMapping();
	}
	
	public function getWIN_NUM() {
		return $this->WIN_NUM;
	}

	public function getCertificationId() {
		return $this->certification_id;
	}
		
	public static function objectFromArray($array) {
		@$win_num = $array["WIN_NUM"];
		@$certification_id = $array["certification_id"];
		$me = new self($win_num, $certification_id);
		return $me;
	}
    
        public function getRow()
        {
            $db = Core_Database::getInstance();
            $query = $db->select();        
            $query->from(Core_Database::TABLE_WORK_ORDER_CERTIFICATION);                
            $query->where("WIN_NUM = ?", $this->WIN_NUM);
            $query->where("certification_id = ?", $this->certification_id);        
            $result = Core_Database::fetchAll($query);

            if(empty($result)) return null;
            else return $result[0];
        }

        public function loadByWIN() {
                $db = Core_Database::getInstance();
                $table = new Zend_Db_Table(Core_Database::TABLE_WORK_ORDER_CERTIFICATION);
                return $table->fetchAll($table->select(Zend_Db_Table::SELECT_WITH_FROM_PART)
                        ->where("WIN_NUM = ?", $this->WIN_NUM));
        }
        public function deleteByWIN() {
            $data = $this->loadByWIN();
            if (empty($data)) return false;
            try {
                foreach($data as $item){
                    $item->delete();
                }
            }
            catch (Exception $e) {
            }
        }
        public function saveExt() {
            $this->deleteByWIN();
            try {
                $this->save();
            }
            catch (Exception $e) {
            }
        }
        public function getRowByWIN()
        {
            $db = Core_Database::getInstance();
            $query = $db->select();        
            $query->from(Core_Database::TABLE_WORK_ORDER_CERTIFICATION);                
            $query->where("WIN_NUM = ?", $this->WIN_NUM);           
            $result = Core_Database::fetchAll($query);

            if(empty($result)) return null;
            else return $result;
        }
}