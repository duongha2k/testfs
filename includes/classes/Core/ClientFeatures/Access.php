<?php
class Core_ClientFeatures_Access {
	public static function getAccessListForClientId($id) {
		$db = Core_Database::getInstance();
		$select = $db->select();
		$select->from('client_feature_access', array('feature_id'))
			->join('client_features', 'id = feature_id', array('name'))
			->where('ClientID = ?', $id);
		return $db->fetchAll($select);
    }
}