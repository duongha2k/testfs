<?php

class Core_WorkOrder
{
    const TABLE_NAME = Core_Database::TABLE_WORK_ORDERS;

    private $data = array();
    private $errors = array();
    private $fields = '';

    function __construct($wo = null)
    {
        if (!empty($wo)) {
		if (is_array($wo)) {
			$this->data = $wo;
		}
		else
	        	$this->setData((int)$wo);
        }
    }

    private function setData($id)
    {
        $fields = Core_Database::getFieldList(self::TABLE_NAME);
        $db = Core_Database::getInstance();
        $select = $db->select()->from(self::TABLE_NAME, $fields)->where('WIN_NUM=?', array($id));
        $techData = Core_Database::fetchAll($select);
        if (empty($techData[0])) {
            $this->addError('WIN# '.$id.' not exists');
        	return false;
        }
        foreach ($techData[0] as $k=>$v) {
        	$this->data["$k"] = trim($v, "`'");
        }
        return true;
    }

    public function __call($name, $params)
    {
        if (substr($name,0,3) == 'get') {
            $fieldName = substr($name,3);
            if (isset($this->data["$fieldName"])) {
            	return $this->data[$fieldName];
            } else {
                //throw new Exception();
            }
        }
        /*
        if (substr($name,0,3) == 'set') {
            $fieldName = $this->data[substr($name,3,0)];
            $this->data[$fieldName] = $params[0];
        }
        */
    }

    public function save()
    {

    }

    private function addError($descr)
    {
        $this->errors[] = $descr;
    }

    public function getErrors()
    {
        return empty($this->errors)?null:$this->errors;
    }
	
	public static function workOrderExists($winNum, $companyId = null) {
		$db = Core_Database::getInstance();
		$select = $db->select();
		$select->from(Core_Database::TABLE_WORK_ORDERS, "WIN_NUM")
			->where("WIN_NUM IN (?)", $winNum);
					
		if (!empty($companyId))
			$select->where("Company_ID = ?", $companyId);

		$result = $db->fetchOne($select);
		
		return ($result && sizeof($result) > 0);
	}
}
