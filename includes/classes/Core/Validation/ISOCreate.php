<?php
/**
 * Core_Validation_ISOCreate Class
 */
class Core_Validation_ISOCreate extends Core_Validation_Abstract
{
    public function validate() {
        $this->requredFields[] = 'Company_Name';
        $this->requredFields[] = 'Address_1';
        $this->requredFields[] = 'City';
        
		if ($this->data['Country'] == 'US' || $this->data['Country'] == 'CA' || $this->data['Country'] == 'MX')
	        $this->requredFields[] = 'State';
        $this->requredFields[] = 'Zip';
        $this->requredFields[] = 'Country';
        $this->requredFields[] = 'Contact_Name';
        $this->requredFields[] = 'Contact_Phone';
        $this->requredFields[] = 'Contact_Email';

        if (!empty($this->data['Contact_Phone']) && ($this->data['Country'] == 'US' || $this->data['Country'] == 'CA'))
            $this->fieldsMustEqual["Contact_Phone"]  = $this->globalPattern['PHONE_WITH_OPTIONAL_EXTENSION'];
        if (!empty($this->data['Contact_Email']))
            $this->fieldsMustEqual["Contact_Email"] = $this->globalPattern['EMAIL_PATTERN'];

		if(!parent::checkRequredFields())
            return false;
		if(!parent::checkMastEqualFields())
            return false;

        $er = parent::getErrors();
        if (!empty($er)) return false;
        return true;
    }
}