<?php

class Core_Validation_Update extends Core_Validation_Abstract
{

    public function validate()
    {
		if ($this->type == Core_Validation_Abstract::TYPE_TECH) {
            /**
             * $this->data['CheckedIn'] === True if Tech make Check In and not mark WO as Completed
             * Step #3 on Tech WO Details Page
             */
            if ( strtolower($this->data['CheckedIn']) === 'true' && strtolower($this->data['TechMarkedComplete']) !== 'true' ) {
                if(!empty($this->data['Date_In']))
                {
                $this->requredFields[] = 'Date_In';
                }
                
                if(!empty($this->data['Date_Out']))
                {
				$this->requredFields[] = 'Date_Out';
                }
                
                if(!empty($this->data['Time_In']))
                {
				$this->requredFields[] = 'Time_In';
                }
                
                if(!empty($this->data['Time_Out']))
                {
				$this->requredFields[] = 'Time_Out';
                }
				$this->fieldsMustEqual["Date_Out"]  = $this->globalPattern["DATE_PATTERN"];
				$this->fieldsMustEqual["Date_In"]   = $this->globalPattern["DATE_PATTERN"];
				$this->fieldsMustEqual["Time_In"]   = $this->globalPattern["TIME_PATTERN"];
				$this->fieldsMustEqual["Time_Out"]  = $this->globalPattern["TIME_PATTERN"];
            } else if ( strtolower($this->data['TechMarkedComplete']) === 'true' ) {
				$this->requredFields[] = 'Date_In';
				$this->requredFields[] = 'Date_Out';
				$this->requredFields[] = 'Time_In';
				$this->requredFields[] = 'Time_Out';
				$this->requredFields[] = 'TechComments';
				$this->fieldsMustEqual["Time_In"]  = $this->globalPattern["TIME_PATTERN"];
				$this->fieldsMustEqual["Time_Out"]    = $this->globalPattern["TIME_PATTERN"];
			}
            if ( strtolower($this->data['ContactHD']) === 'true' ) {
//                $this->requredFields[] = 'WorkOrderReviewed';
//                $this->requredFields[] = 'TechCheckedIn_24hrs';
//                $this->fieldsMustEqual['WorkOrderReviewed'] = 'True';
//                $this->fieldsMustEqual['TechCheckedIn_24hrs'] = 'True';
            }
            /** multifiles fix
             * @author Alex Scherba
             */
            /*for ( $i = 1; $i <= 3; ++$i ) {
                if ( !empty($this->data["Pic{$i}_FromTech"]) ) {
                    $this->fieldsMustEqual["Pic{$i}_FromTech"] = $this->globalPattern['FILE_NAME'];
                }
            }*/
			
            if (strtolower($this->data['Unexpected_Steps']) === 'true') {
                $this->requredFields[] = 'Unexpected_Desc';
                $this->requredFields[] = 'AskedBy';
                $this->requredFields[] = 'AskedBy_Name';
            }
		} else {
			if (array_key_exists("isProjectAutoAssign", $this->data) && $this->data['isProjectAutoAssign'] == 'True') {
				$this->requredFields[] = 'StartTime';
				$this->requredFields[] = 'EndTime';
				$this->requredFields[] = 'StartDate';
				$this->requredFields[] = 'EndDate';
				$this->requredFields[] = 'City';
                                //041
                                $Requirecountry = array("US"=>"US","CA"=>"CA","MX"=>"MX");
                                if(array_key_exists($this->data['Country'], $Requirecountry))
                                {
				$this->requredFields[] = 'State';
			}
                                //end 041
			}

			$this->requredFields[] = 'Zipcode';
			if ($this->data['Tech_ID']>0) {
				$this->fieldsMustEqual["Tech_Bid_Amount"] = $this->globalPattern["MONEY_GREATER_THAN_ZERO"];
				$this->requredFields[] = 'Tech_Bid_Amount';
			} else {
//				$this->fieldsMustEqual["BackOut_Tech"] = $this->globalPattern['EMPTY_OR_ZERO'];
//				$this->fieldsMustEqual["NoShow_Tech"]  = $this->globalPattern['EMPTY_OR_ZERO'];
			}

			if (array_key_exists("Amount_Per", $this->data) && $this->data['Amount_Per'] == 'Device') {
				$this->requredFields[] = 'Qty_Devices';
				$this->fieldsMustEqual["Qty_Devices"] = $this->globalPattern["WHOLE_NUMBER_GREATER_THAN_ZERO"];
			}

			if ($this->data['TechMarkedComplete'] == "True") {
				$this->requredFields[] = 'Tech_ID';
			}

			if ($this->data['TripCharge']>0) {
				//$this->requredFields[] = 'Add_Pay_Reason';
			}

			if ($this->data['MileageReimbursement']>0) {
				$this->requredFields[] = 'Add_Pay_AuthBy';
				//$this->requredFields[] = 'Add_Pay_Reason';
			}

			if ($this->data['Additional_Pay_Amount']>0) {
				$this->requredFields[] = 'Add_Pay_AuthBy';
				//$this->requredFields[] = 'Add_Pay_Reason';
			}

			if ($this->data['Deactivated']=='True') {
	//			$this->requredFields[] = 'Deactivated_Reason';
				$this->requredFields[] = 'DeactivationCode';
				if (!empty($this->data['DeactivationCode'])) {
					parent::checkValidFieldNameCaseSensitive(Core_Database::TABLE_DEACTIVATION_CODES, 'Reason', 'DeactivationCode');
				}
			}

			if ($this->data['Approved']=="True") {
				$this->requredFields[] = 'Tech_ID';
				$this->requredFields[] = 'Tech_Bid_Amount';
	//			$this->requredFields[] = 'PayAmount';

				$this->fieldsMustEqual["Tech_ID"] = $this->globalPattern["WHOLE_NUMBER_GREATER_THAN_ZERO"];
				$this->fieldsMustEqual["Tech_Bid_Amount"] = $this->globalPattern["MONEY_GREATER_THAN_ZERO"];
				$this->fieldsMustEqual["PayAmount"] = $this->globalPattern["MONEY_ZERO_OK"];
			}

			if ((!empty($this->data['ShowTechs']) && $this->data['ShowTechs']=="True") ||
				   (!empty($this->data['Tech_ID']) && $this->data['Tech_ID']>0)) {
				$this->fieldsMustEqual["PayMax"]     = $this->globalPattern["MONEY_GREATER_THAN_ZERO"];
				$this->fieldsMustEqual["Amount_Per"] = array("Site", "Hour", "Device", "Visit", "visit");
				$this->requredFields[] = 'PayMax';
				$this->requredFields[] = 'Amount_Per';
			}

			$this->fieldsMustEqual["Type_ID"]    = array("1", "2", "3");
			$this->fieldsMustEqual["StartTime"]  = $this->globalPattern["TIME_PATTERN"];
			$this->fieldsMustEqual["EndTime"]    = $this->globalPattern["TIME_PATTERN"];
			if ($this->data['Country'] == 'US' || $this->data['Country'] == 'CA') {
				$this->fieldsMustEqual["SitePhone"]  = $this->globalPattern["SITE_PHONE_STRICT_WITH_OPTIONAL_EXTENSION"];
				$this->fieldsMustEqual["ProjectManagerPhone"]      = $this->globalPattern["PHONE_WITH_OPTIONAL_EXTENSION"];
				$this->fieldsMustEqual["ResourceCoordinatorPhone"] = $this->globalPattern["PHONE_WITH_OPTIONAL_EXTENSION"];
				$this->fieldsMustEqual["EmergencyPhone"]           = $this->globalPattern["PHONE_WITHOUT_EXTENSION"];
				$this->fieldsMustEqual["TechnicalSupportPhone"]    = $this->globalPattern["PHONE_WITH_OPTIONAL_EXTENSION"];
				$this->fieldsMustEqual["CheckInOutNumber"]         = $this->globalPattern["PHONE_WITH_OPTIONAL_EXTENSION"];
			}
			$this->fieldsMustEqual["Zipcode"]    = $this->globalPattern["ZIP_PATTERN"];

			$this->requredFields[] = 'Headline';
			$this->requredFields[] = 'WO_Category_ID';
			$this->requredFields[] = 'Type_ID';
			$this->requredFields[] = 'WorkOrderOwner';

			$db = Core_Database::getInstance();

			if (empty($this->data['Country'])) $this->data['Country'] = 'US';
			else {
				$c = new Core_Country($this->data['Country']);
				if (!$c->isValid()) $this->addError('Country is not from the list of available values');
			}
			if (!empty($this->data['State'])) {
//				parent::checkValidFieldName(Core_Database::TABLE_STATES_LIST, 'Abbreviation', 'State', $db->quoteInto("Country = ?",this->data['Country']));
                                //041
                                $Requirecountry = array("US"=>"US","CA"=>"CA","MX"=>"MX");
                                if(array_key_exists($this->data['Country'], $Requirecountry))
                                {
				$s = new Core_State($this->data['State'], $this->data['Country']);
				if (!$s->isValid()) $this->addError('State is not from the list of available values');
			}
                                //end 041

			}

			if (!empty($this->data['Shipped_By'])) {
				parent::checkValidFieldName(Core_Database::TABLE_SHIPPING_CARRIERS, 'Shipping_Carrier', 'Shipped_By');
			}
		}

        if (!parent::checkRequredFields()) {
            return false;
        }

		if (!parent::checkMastEqualFields()) {
		    return false;
		}

		$this->validateGeneralFields();

		$er = parent::getErrors();
        if (!empty($er)) {
        	return false;
        }
        return true;
    }
}
