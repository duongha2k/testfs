<?php
/**
 * Core_Validation_ClientUpdate Class
 * @author Alex Scherba
 */
class Core_Validation_ClientUpdate extends Core_Validation_Abstract
{
    public function validate()
    {
        if ($this->type == Core_Validation_Abstract::TYPE_CLIENT) {
            $this->requredFields[] = 'ContactName';
            //$this->requredFields[] = 'CompanyName';
            $this->requredFields[] = 'ContactPhone1';
            $this->requredFields[] = 'Email1';
            $this->requredFields[] = 'Password';
        }

        if (!empty($this->data['Password']))
            $this->fieldsMustEqual["Password"] = $this->globalPattern['PASSWORD_8_TO_15_CHAR_WITH_AT_LEAST_ONE_LETTER_AND_ONE_NUMBER'];
		$validatePhone = $this->data['Country'] == 'US' || $this->data['Country'] == 'CA';
		if ($validatePhone) {
			if (!empty($this->data['ContactPhone1']))
				$this->fieldsMustEqual["ContactPhone1"]  = $this->globalPattern['PHONE_WITH_OPTIONAL_EXTENSION'];
			if (!empty($this->data['ContactPhone2']))
				$this->fieldsMustEqual["ContactPhone2"]  = $this->globalPattern['PHONE_WITH_OPTIONAL_EXTENSION'];
		}
        if (!empty($this->data['Email1']))
            $this->fieldsMustEqual["Email1"] = $this->globalPattern['EMAIL_PATTERN'];
        if (!empty($this->data['Email2']))
            $this->fieldsMustEqual["Email2"] = $this->globalPattern['EMAIL_PATTERN'];
        if (!empty($this->data['BusinessZip']))
            $this->fieldsMustEqual["BusinessZip"] = $this->globalPattern['ZIP_PATTERN'];
        if (!empty($this->data['BillingType']))
            $this->fieldsMustEqual["BillingType"] = array("Single Zip Access", "All Zip Access", "Premium");
        if (!empty($this->data['UserType']))
            $this->fieldsMustEqual["UserType"] = array("Manager", "Install Desk", "Staff", "Admin");


		if(!parent::checkRequredFields())
            return false;
		if(!parent::checkMastEqualFields())
            return false;
        if(!empty($this->data['UserName']) && !parent::checkUniqueField(Core_Database::TABLE_CLIENTS, 'UserName'))
            return false;
        
        $er = parent::getErrors();
        if (!empty($er)) return false;
        return true;
    }
}
