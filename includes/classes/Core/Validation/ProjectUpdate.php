<?php
/**
 * Core_Validation_ProjectUpdate Class
 * @author Alex Scherba
 */
class Core_Validation_ProjectUpdate extends Core_Validation_Abstract
{
    public function validate()
    {
        $this->requredFields[] = 'Project_Name';
        $this->requredFields[] = 'WO_Category_ID';
        $this->requredFields[] = 'Headline';
        $this->requredFields[] = 'From_Email';
        $this->requredFields[] = 'To_Email';

        if (!empty($this->data['Project_Manager_Phone']))
            $this->fieldsMustEqual["Project_Manager_Phone"] = $this->globalPattern['PHONE_WITH_OPTIONAL_EXTENSION'];
        if (!empty($this->data['Resource_Coord_Phone']))
            $this->fieldsMustEqual["Resource_Coord_Phone"]  = $this->globalPattern['PHONE_WITH_OPTIONAL_EXTENSION'];
        if (!empty($this->data['TechnicalSupportPhone']))
            $this->fieldsMustEqual["TechnicalSupportPhone"] = $this->globalPattern['PHONE_WITH_OPTIONAL_EXTENSION'];
        if (!empty($this->data['CheckInOutNumber']))
            $this->fieldsMustEqual["CheckInOutNumber"]       = $this->globalPattern['PHONE_WITH_OPTIONAL_EXTENSION'];

        if (!empty($this->data['Project_Manager_Email']))
            $this->fieldsMustEqual["Project_Manager_Email"] = $this->globalPattern['EMAIL_PATTERN'];
        if (!empty($this->data['Resource_Coord_Email']))
            $this->fieldsMustEqual["Resource_Coord_Email"]  = $this->globalPattern['EMAIL_PATTERN'];
        //691
        if (!empty($this->data['ACSNotifiPOC_Phone']))
            $this->fieldsMustEqual["ACSNotifiPOC_Phone"]        = $this->globalPattern['PHONE_WITHOUT_EXTENSION'];
        if (!empty($this->data['ACSNotifiPOC_Email']))
        {
            $this->data['ACSNotifiPOC_Email'] = str_replace(' ','',$this->data['ACSNotifiPOC_Email']);
            $this->fieldsMustEqual["ACSNotifiPOC_Email"]        = $this->globalPattern['EMAIL_PATTERN_COMMA_SEPARATED'];
        }
        //end 691
        
        //732
        if (!empty($this->data['EmergencyPhone']))
            $this->fieldsMustEqual["EmergencyPhone"]        = $this->globalPattern['PHONE_WITHOUT_EXTENSION'];
        if (!empty($this->data['EmergencyEmail']))
            $this->fieldsMustEqual["EmergencyEmail"]        = $this->globalPattern['EMAIL_PATTERN'];
        if (!empty($this->data['SiteContactReminderCallHours']))
            $this->fieldsMustEqual["SiteContactReminderCallHours"]        = $this->globalPattern['WHOLE_NUMBER_GREATER_THAN_ZERO'];
        //end 732
        
        if (!empty($this->data['TechnicalSupportEmail']))
            $this->fieldsMustEqual["TechnicalSupportEmail"] = $this->globalPattern['EMAIL_PATTERN'];
        if (!empty($this->data['CheckInOutEmail']))
            $this->fieldsMustEqual["CheckInOutEmail"]       = $this->globalPattern['EMAIL_PATTERN'];

        if (!empty($this->data['RecruitmentFromEmail']))
            $this->fieldsMustEqual["RecruitmentFromEmail"]  = $this->globalPattern['EMAIL_PATTERN'];
        if (!empty($this->data['From_Email']))
            $this->fieldsMustEqual["From_Email"]            = $this->globalPattern['EMAIL_PATTERN'];
        if (!empty($this->data['To_Email']))
            $this->fieldsMustEqual["To_Email"]              = $this->globalPattern['EMAIL_PATTERN_COMMA_SEPARATED'];
//        if (!empty($this->data['AssignmentNotificationEmail']))  --- appears to not match behavior of bid notification and work don notification --- they currently support comma separated list of addresses - #13345
//            $this->fieldsMustEqual["AssignmentNotificationEmail"] = $this->globalPattern['EMAIL_PATTERN_COMMA_SEPARATED'];
        if (!empty($this->data['Client_Email']))
            $this->fieldsMustEqual["Client_Email"]          = $this->globalPattern['EMAIL_PATTERN'];

        if (!empty($this->data['Amount_Per']) && $this->data['Amount_Per']!='Select Type')
            $this->fieldsMustEqual["Amount_Per"] = array("Site", "Hour", "Device","Visit");

        if ($this->data['ReminderAll']==1 || $this->data['ReminderAll']=="True"){
            $this->requredFields[] = 'Project_Manager';
            $this->requredFields[] = 'Project_Manager_Phone';
            $this->requredFields[] = 'Project_Manager_Email';
            //691
            $this->requredFields[] = 'ACSNotifiPOC';
            $this->requredFields[] = 'ACSNotifiPOC_Phone';
            $this->requredFields[] = 'ACSNotifiPOC_Email';
            //end 691
        }
        //732
        if ($this->data['SiteContactReminderCall']==1 || $this->data['SiteContactReminderCall']=="True"){
            $this->requredFields[] = 'ACSNotifiPOC';
            $this->requredFields[] = 'ACSNotifiPOC_Phone';
            $this->requredFields[] = 'ACSNotifiPOC_Email';
            $this->requredFields[] = 'SiteContactReminderCallHours';
        }
        //end 732

        if(isset($this->data['StartTime']) && !empty($this->data['StartTime']))
        {
            $this->fieldsMustEqual["StartTime"]  = $this->globalPattern["TIME_PATTERN"];
        }  
        if(isset($this->data['EndTime']) && !empty($this->data['EndTime']))
        {
            $this->fieldsMustEqual["EndTime"]  = $this->globalPattern["TIME_PATTERN"];
        } 
        
        if (array_key_exists("isProjectAutoAssign", $this->data) && ($this->data['isProjectAutoAssign'] == 'True' || $this->data['isProjectAutoAssign'] == 1)) {
			//$this->requredFields[] = 'StartTime';
        }

		parent::checkRequredFields();
		parent::checkMastEqualFields();
        $er = parent::getErrors();
        if (!empty($er)) {
        	return false;
        }
        return true;
    }
}
