<?php
class Core_Validation_WoVisit extends Core_Validation_Abstract
{
     public function validate()
     {
     	//--- check required fields
         if (array_key_exists("isProjectAutoAssign", $this->data) && $this->data['isProjectAutoAssign'] == 1) {
            $this->requredFields[] = 'StartTime';
            $this->requredFields[] = 'EndTime';
            $this->requredFields[] = 'StartDate';
            $this->requredFields[] = 'EndDate';
         }
         //13969
         if($this->data['RequiredStartDateTime']==1){
            $this->requredFields[] = 'StartDate';
            $this->requredFields[] = 'StartTime';
         }
         if($this->data['RequiredEndDateTime']==1){
            $this->requredFields[] = 'EndDate';
            $this->requredFields[] = 'EndTime';
         }         
         //end 13969
         
         //881
//         if (array_key_exists("OptionCheckInOut", $this->data) && $this->data['OptionCheckInOut'] == 2) 
//         {
//            $this->requredFields[] = 'CORCheckInDate';
//            $this->requredFields[] = 'CORCheckInTime';
//            $this->requredFields[] = 'CORCheckOutDate';
//            $this->requredFields[] = 'CORCheckOutTime';             
//         }
         //end 881
         
         
         if($this->type==self::TYPE_TECH)
         {
            $this->requredFields[] = 'TechCheckInDate';
            $this->requredFields[] = 'TechCheckInTime';
            $this->requredFields[] = 'TechCheckOutDate';
            $this->requredFields[] = 'TechCheckOutTime';                          
         }

         foreach (array (
			"StartTime" => "Start Time",
			"EndTime" => "End Time",
			"CORCheckInTime" => "Check In",
			"CORCheckOutTime" => "Check Out",
			"TechCheckInTime" => "Check In",
			"TechCheckOutTime" => "Check Out"
		) as $key => $description) {
			if (isset ($this->data[$key])) {
				if (!empty ($this->data[$key])
					&& preg_match("/(0?\d|1[0-2]):(0\d|[0-5]\d) (AM|PM)/i", $this->data[$key]) != 1)
				{
					$this->addError($description . " formatting is not H:MM AM/PM");
					//return false;
				}
			}
		}

		foreach (array (
			"StartDate" => "Start Date",
			"EndDate" => "End Date",
			"CORCheckInDate" => "Check In",
			"CORCheckOutDate" => "Check Out",
			"TechCheckInDate" => "Check In",
			"TechCheckOutDate" => "Check Out"
		) as $key => $description) {
			if (isset ($this->data[$key])) {
				$date = $this->data[$key];
				if (preg_match ("/^\d{4}-\d{1,2}-\d{1,2}$/", $date) == 1) {
					$date = date_format (new DateTime($date), "m/d/Y");
				}
				if (!empty ($date)
					&& preg_match("/^\d{1,2}\/\d{1,2}\/\d{4}$/", $date) != 1)
				{
					$this->addError($description . " formatting is not MM/DD/YYYY");
					//return false;
				}
			}
		}

		if (!$this->checkRequiredFields()) {
			return false;
		}         

		//--- startDate must be less then endDate
         if (!empty($this->data['StartDate']) && !empty($this->data['EndDate']))
         {
         	$startDateTime = date_format(new DateTime($this->data['StartDate']), "Y-m-d");
            $endDateTime = date_format(new DateTime($this->data['EndDate']), "Y-m-d");

            if (!empty($this->data['StartTime']) && !empty($this->data['EndTime'])) {
                $startDateTime .= ' '.$woVisitFieldValues['StartTime'];
                $endDateTime .= ' '.$woVisitFieldValues['EndTime'];
            }

            $startDT = strtotime($startDateTime); 
            
            $endDT = strtotime($endDateTime ? $endDateTime : "2100-01-01 00:00:00");
            
            //$test2 = ",startDT: $startDT , endDT: $endDT";

            if($endDT < $startDT)
            {
                 $this->addError('Start Date/Time must be before End Date/Time.');
                 return false;
            } 
         }

         return true;
     }
     
    protected function checkRequiredFields()
    {
        foreach ($this->requredFields as $field) {
            if (empty($this->data[$field])) {             
                $this->addError($this->getFieldDesc($field).' empty but required');
            }
        }
        return !empty($this->errors) ? false : true;
    }
    
    protected function getFieldDesc($field)
    {
        $ret = $field;
        if ($field=='CORCheckInDate') $ret = 'Client Over-Ride Check-In Date';
        else if($field=='CORCheckInTime') $ret = 'Client Over-Ride Check-In Time';
        else if($field=='CORCheckOutDate') $ret = 'Client Over-Ride Check-Out Date';
        else if($field=='CORCheckOutTime') $ret = 'Client Over-Ride Check-Out Time';
        else if($field=='TechCheckInDate') $ret = 'Technician Check In Date';
        else if($field=='TechCheckInTime') $ret = 'Technician Check In Time';
        else if($field=='TechCheckOutDate') $ret = 'Technician Check Out Date';
        else if($field=='TechCheckOutTime') $ret = 'Technician Check Out Time';
        else if($field=='StartDate') $ret = 'Start Date';
        else if($field=='StartTime') $ret = 'Start Time';
        else if($field=='EndDate') $ret = 'End Date';
        else if($field=='EndTime') $ret = 'End Time';
        return $ret;        
    }
}
 
