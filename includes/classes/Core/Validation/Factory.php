<?php
/**
 * Enter description here...
 *
 */

class Core_Validation_Factory
{
    const VALIDATION_TYPE_CREATE         = 'create';
    const VALIDATION_TYPE_UPDATE         = 'update';
	const VALIDATION_TYPE_TECH_UPDATE    = 'techupdate';
    const VALIDATION_TYPE_PROJECT_UPDATE = 'prjupdate';
    const VALIDATION_TYPE_PROJECT_CREATE = 'prjcreate';
    const VALIDATION_TYPE_CLIENT_UPDATE  = 'clientupdate';
    const VALIDATION_TYPE_CLIENT_CREATE  = 'clientcreate';
    const VALIDATION_TYPE_CLIENT_PROFILE_UPDATE  = 'clientprofileupdate';
    const VALIDATION_TYPE_CLIENT_USER_UPDATE  = 'clientuserupdate';
    const VALIDATION_TYPE_CLIENT_USER_CREATE  = 'clientusercreate';
    const VALIDATION_TYPE_SITE_CREATE  = 'sitecreate';
    const VALIDATION_TYPE_SITE_UPDATE  = 'siteupdate';
    const VALIDATION_TYPE_ISO_CREATE  = 'isocreate';
    const VALIDATION_TYPE_ISO_UPDATE  = 'isoupdate';

    private $type = '';
    
    function __construct($type)
    {
        $this->type = $type;
    }
    
    public function getValidationObject($data)
    {
        switch ($this->type)
        {
            case self::VALIDATION_TYPE_CREATE:
                return new Core_Validation_Create($data);
                break;
            case self::VALIDATION_TYPE_UPDATE:
                return new Core_Validation_Update($data, Core_Validation_Abstract::TYPE_CLIENT);
                break;
            case self::VALIDATION_TYPE_TECH_UPDATE:
                return new Core_Validation_Update($data, Core_Validation_Abstract::TYPE_TECH);
                break;
            case self::VALIDATION_TYPE_PROJECT_UPDATE:
                return new Core_Validation_ProjectUpdate($data);
                break;
            case self::VALIDATION_TYPE_PROJECT_CREATE:
                return new Core_Validation_ProjectCreate($data);
                break;
            case self::VALIDATION_TYPE_CLIENT_UPDATE:
                return new Core_Validation_ClientUpdate($data,Core_Validation_Abstract::TYPE_ADMIN);
                break;
            case self::VALIDATION_TYPE_CLIENT_CREATE:
                return new Core_Validation_ClientCreate($data,Core_Validation_Abstract::TYPE_ADMIN);
                break;
            case self::VALIDATION_TYPE_CLIENT_USER_UPDATE:
                return new Core_Validation_ClientUpdate($data, Core_Validation_Abstract::TYPE_CLIENT);
                break;
            case self::VALIDATION_TYPE_CLIENT_USER_CREATE:
                return new Core_Validation_ClientCreate($data, Core_Validation_Abstract::TYPE_CLIENT);
                break;
            case self::VALIDATION_TYPE_SITE_UPDATE:
                return new Core_Validation_SiteCreate($data, Core_Validation_Abstract::TYPE_CLIENT);
                break;
            case self::VALIDATION_TYPE_SITE_CREATE:
                return new Core_Validation_SiteCreate($data, Core_Validation_Abstract::TYPE_CLIENT);
                break;
            case self::VALIDATION_TYPE_ISO_UPDATE:
                return new Core_Validation_ISOCreate($data, Core_Validation_Abstract::TYPE_CLIENT);
                break;
            case self::VALIDATION_TYPE_ISO_CREATE:
                return new Core_Validation_ISOCreate($data, Core_Validation_Abstract::TYPE_CLIENT);
                break;
            //13970    
            case self::VALIDATION_TYPE_CLIENT_PROFILE_UPDATE:
                return new Core_Validation_ClientProfileUpdate($data, Core_Validation_Abstract::TYPE_CLIENT);
                break;    
            default:
                return null;
                break;
        }
    }
}

