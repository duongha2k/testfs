<?php
/**
 * Core_Validation_SiteCreate Class
 */
class Core_Validation_SiteCreate extends Core_Validation_Abstract
{
    public function validate() {        
        $this->requredFields[] = 'SiteNumber';
		$validatePhone = $this->data['Country'] == 'US' || $this->data['Country'] == 'CA';
		if ($validatePhone) {
			if (!empty($this->data['SitePhone']))
				$this->fieldsMustEqual["SitePhone"]  = $this->globalPattern['SITE_PHONE_STRICT_WITH_OPTIONAL_EXTENSION'];
			if (!empty($this->data['SiteFax']))
				$this->fieldsMustEqual["SiteFax"] = $this->globalPattern['SITE_PHONE_STRICT_WITH_OPTIONAL_EXTENSION'];
		}
        if (!empty($this->data['SiteEmail']))
            $this->fieldsMustEqual["SiteEmail"] = $this->globalPattern['EMAIL_PATTERN'];

		if(!parent::checkRequredFields())
            return false;
		if(!parent::checkMastEqualFields())
            return false;

        $er = parent::getErrors();
        if (!empty($er)) return false;
        return true;
    }
}