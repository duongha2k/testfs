<?php

class Core_Validation_Create extends Core_Validation_Abstract
{
    public function validate()
    {
        if (array_key_exists("isProjectAutoAssign", $this->data) && ($this->data['isProjectAutoAssign'] == 'True' || $this->data['isProjectAutoAssign'] == 1)) {
			
        	//IF this is copy WO, don't make start date and start time required
        	if(!strpos($this->data["WO_ID"],"COPY")){
        		$this->requredFields[] = 'StartTime';
        		$this->requredFields[] = 'StartDate';
        		$this->requredFields[] = 'EndTime';
        		$this->requredFields[] = 'EndDate';
			}
        	
        	$this->requredFields[] = 'City';
                //041
                $Requirecountry = array("US"=>"US","CA"=>"CA","MX"=>"MX");
                if(array_key_exists($this->data['Country'], $Requirecountry))
                {
                    $this->requredFields[] = 'State';
                }
        	//end 041
        }
       	//$this->requredFields[] = 'Zipcode';
        if (!empty($this->data['Tech_ID']) && $this->data['Tech_ID']>0) {
        	$this->fieldsMustEqual["Tech_Bid_Amount"] = $this->globalPattern["MONEY_GREATER_THAN_ZERO"];
        	$this->requredFields[] = 'Tech_Bid_Amount';
        } else {
            $this->fieldsMustEqual["BackOut_Tech"] = $this->globalPattern['EMPTY_OR_ZERO'];
		    $this->fieldsMustEqual["NoShow_Tech"]  = $this->globalPattern['EMPTY_OR_ZERO'];
        }

        //--- 13543
        if (array_key_exists("Amount_Per", $this->data) && ($this->data['Amount_Per'] == 'Device')) {
        	$this->fieldsMustEqual["Qty_Devices"] = $this->globalPattern["WHOLE_NUMBER_GREATER_THAN_ZERO"];
        	$this->requredFields[] = 'Qty_Devices';
        }
        
        if (array_key_exists("Amount_Per", $this->data) && ($this->data['Amount_Per'] == 'Visit')) {
            $this->fieldsMustEqual["Qty_Visits"] = $this->globalPattern["WHOLE_NUMBER_GREATER_THAN_ZERO"];
            $this->requredFields[] = 'Qty_Visits';
        }
        
        //---
        if (!empty($this->data['TripCharge']) && $this->data['TripCharge']>0) {
			//$this->requredFields[] = 'Add_Pay_Reason';
        }

        if (!empty($this->data['MileageReimbursement']) && $this->data['MileageReimbursement']>0) {
			$this->requredFields[] = 'Add_Pay_AuthBy';
			//$this->requredFields[] = 'Add_Pay_Reason';
        }

        if (!empty($this->data['Additional_Pay_Amount']) && $this->data['Additional_Pay_Amount']>0) {
			$this->requredFields[] = 'Add_Pay_AuthBy';
			//$this->requredFields[] = 'Add_Pay_Reason';
        }

        if (!empty($this->data['Deactivated']) && $this->data['Deactivated']=='True') {
			$this->requredFields[] = 'DeactivationCode';
			if (!empty($this->data['DeactivationCode'])) {
				parent::checkValidFieldNameCaseSensitive(TABLE_DEACTIVATION_CODES, 'Reason', 'DeactivationCode');
			}
//			$this->requredFields[] = 'Deactivated_Reason';
        }

        if (!empty($this->data['Approved']) && $this->data['Approved']=="True") {
			$this->requredFields[] = 'Tech_ID';
			$this->requredFields[] = 'Tech_Bid_Amount';
//			$this->requredFields[] = 'PayAmount';

			$this->fieldsMustEqual["Tech_ID"] = $this->globalPattern["WHOLE_NUMBER_GREATER_THAN_ZERO"];
			$this->fieldsMustEqual["Tech_Bid_Amount"] = $this->globalPattern["MONEY_GREATER_THAN_ZERO"];
			$this->fieldsMustEqual["PayAmount"] = $this->globalPattern["MONEY_ZERO_OK"];
	    }

	if ((!empty($this->data['ShowTechs']) && $this->data['ShowTechs']=="True") ||
           (!empty($this->data['Tech_ID']) && $this->data['Tech_ID']>0)) {
		$this->fieldsMustEqual["PayMax"]     = $this->globalPattern["MONEY_GREATER_THAN_ZERO"];
		$this->fieldsMustEqual["Amount_Per"] = array("Site", "Hour", "Device","Visit");
		$this->requredFields[] = 'PayMax';
		$this->requredFields[] = 'Amount_Per';
	}

		//$this->fieldsMustEqual["Approved"]   = array("0", "False");;//13985 Remove
		$this->fieldsMustEqual["Invoiced"]   = array("0", "False");;
		$this->fieldsMustEqual["TechPaid"]   = array("0", "False");;
		$this->fieldsMustEqual["Type_ID"]    = array("1", "2", "3");
		$this->fieldsMustEqual["StartTime"]  = $this->globalPattern["TIME_PATTERN"];
		$this->fieldsMustEqual["EndTime"]    = $this->globalPattern["TIME_PATTERN"];
		if ($this->data['Country'] == 'US' || $this->data['Country'] == 'CA') {
			$this->fieldsMustEqual["SitePhone"]  = $this->globalPattern["SITE_PHONE_STRICT_WITH_OPTIONAL_EXTENSION"];
			$this->fieldsMustEqual["ProjectManagerPhone"]      = $this->globalPattern["PHONE_WITH_OPTIONAL_EXTENSION"];
			$this->fieldsMustEqual["ResourceCoordinatorPhone"] = $this->globalPattern["PHONE_WITH_OPTIONAL_EXTENSION"];
			$this->fieldsMustEqual["EmergencyPhone"]           = $this->globalPattern["PHONE_WITHOUT_EXTENSION"];
			$this->fieldsMustEqual["TechnicalSupportPhone"]    = $this->globalPattern["PHONE_WITH_OPTIONAL_EXTENSION"];
			$this->fieldsMustEqual["CheckInOutNumber"]         = $this->globalPattern["PHONE_WITH_OPTIONAL_EXTENSION"];
		}

                $this->requredFields[] = 'Project_ID';
		$this->requredFields[] = 'WO_ID';
		$this->requredFields[] = 'WO_Category_ID';
		$this->requredFields[] = 'Headline';
                $this->requredFields[] = 'Country';
                $this->requredFields[] = 'Zipcode';
                $this->fieldsMustEqual["Zipcode"]    = $this->globalPattern["ZIP_PATTERN"];
		$this->requredFields[] = 'Company_ID';
		$this->requredFields[] = 'Type_ID';
		$this->requredFields[] = 'WorkOrderOwner';

		if (!parent::checkRequredFields()) {
            return false;
        }

		if (!parent::checkMastEqualFields()) {
		    return false;
		}

		parent::checkUniqueField(Core_Database::TABLE_WORK_ORDERS, 'WO_ID');
        //parent::twoFieldValidate(TABLE_WO_CATEGORIES, 'Category', 'Category_ID', 'WO_Category', 'WO_Category_ID');

		//if (!empty($this->data['Project_ID']))
		//	parent::twoFieldValidate(TABLE_CLIENT_PROJECTS, 'Project_ID', 'Project_Name', 'Project_ID', 'Project_Name');

		if (empty($this->data['Country'])) $this->data['Country'] = 'US';
		else {
			$c = new Core_Country($this->data['Country']);
			if (!$c->isValid()) $this->addError('Country is not from the list of available values');
		}
		if (!empty($this->data['State'])) {
//			parent::checkValidFieldName(Core_Database::TABLE_STATES_LIST, 'Abbreviation', 'State', $db->quoteInto("Country = ?",this->data['Country']));
			$s = new Core_State($this->data['State'], $this->data['Country']);
			if (!$s->isValid()) $this->addError('State is not from the list of available values');
		}

        if (!empty($this->data['Shipped_By']))
        	parent::checkValidFieldName(Core_Database::TABLE_SHIPPING_CARRIERS, 'Shipping_Carrier', 'Shipped_By');

        $this->validateGeneralFields();

        $er = parent::getErrors();
        if (!empty($er)) {
        	return false;
        }
        return true;
    }
}
