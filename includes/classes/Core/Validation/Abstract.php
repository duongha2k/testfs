<?php

abstract class Core_Validation_Abstract
{
	const TYPE_CLIENT = 0;
	const TYPE_TECH = 1;
    const TYPE_ADMIN = 2;

	protected $data = array();
    protected $globalPattern = array();
    protected $fieldsMustEqual = array();
    protected $requredFields = array();

    protected $errors = array();
	protected $type = self::TYPE_CLIENT;

    function __construct($data, $type = self::TYPE_CLIENT)
    {
		$this->type = $type;
        $this->data = $data;

		$this->globalPattern["EMPTY_OR_ZERO"] = new patternMatch("/^(0{1})$/", "empty or 0", true);
//		$this->globalPattern["EMPTY_OR_ZERO"] = new patternMatch("/^(0{1})$/", "empty or 0", true);
//		$this->globalPattern["GREATER_THAN_ZERO"] = new patternMatch("/^(\d+[^0])$/", "Greater Than Zero", false);
		$this->globalPattern["MONEY_GREATER_THAN_ZERO"] = new patternMatch("/^(?(?!0[.]00)(\d+)[.](\d{2}))$/", "0.00 and Greater Than Zero", false);
		$this->globalPattern["MONEY_ZERO_OK"] = new patternMatch("/^(\d+)[.](\d{2})$/", "0.00", false);
		$this->globalPattern["WHOLE_NUMBER_GREATER_THAN_ZERO"] = new patternMatch("/^(?!0+)(\d+)$/", "Whole Number Greater Than Zero", false);
		$this->globalPattern["AMOUNT_PER_DEVICE"] = new patternMatch("/^Device$/", "'Device'", false);
		$this->globalPattern["PHONE_WITH_OPTIONAL_EXTENSION"] = new patternMatch("/^([(]?(\d{3})[)]?[^0-9]*(\d{3})[^0-9]*(\d{4})|[(]?(\d{3})[)]?[^0-9]*(\d{3})[^0-9]*(\d{4})[^0-9]+([0-9]*)[^0-9]*([0-9]*))$/", "###-###-#### or ###-###-#### x ####", TRUE);
		$this->globalPattern["PHONE_WITHOUT_EXTENSION"] = new patternMatch("/^[(]?(\d{3})[)]?[^0-9]*(\d{3})[^0-9]*(\d{4})$/", "###-###-####", TRUE);
		$this->globalPattern["SITE_PHONE_STRICT_WITH_OPTIONAL_EXTENSION"] = new patternMatch("/^([(](\d{3})[)]\s(\d{3})[-](\d{4})|[(](\d{3})[)]\s(\d{3})[-](\d{4})[^0-9]+[0-9]*)$/", "(###) ###-#### or (###) ###-#### x ####", true);
		$this->globalPattern["BOOLEAN_TRUE"] = new patternMatch("/^1$/", "equal to '1'", false);
		$this->globalPattern["TIME_PATTERN"] = new patternMatch("/^(\d{1,2}):(\d{2})\s(AM|PM)$/", "H:MM AM/PM", true);
		$this->globalPattern["ZIP_PATTERN"] = new patternMatch("/^.{4,}$/", "5 or more characters", true);
        $this->globalPattern['FILE_NAME']   = new patternMatch('/^[^\+\\\<\>\:\"\*\?\|]{1,255}$/', 'File name should contain legal characters only and not more then 255 characters', false);
        $this->globalPattern['DATE_PATTERN'] = new patternMatch('/(0?[1-9]|1[0-2])\/([0-2]?[0-9]|3[01])\/(20)?[1-9]\d/', 'Entered Date is incorrect. You should set Date by "MM/DD/YY" pattern. E.g. 9/22/10 or 01/01/2011');
        $this->globalPattern["EMAIL_PATTERN"] = new patternMatch("/^[A-Za-z0-9_\.\-]+@[A-Za-z0-9\.\-]+\.[A-Za-z]{2,4}$/", " e-mail", true);
        $this->globalPattern["EMAIL_PATTERN_COMMA_SEPARATED"] = new patternMatch("/^[A-Za-z0-9_\.\-]+@[A-Za-z0-9\.\-]+\.[A-Za-z]{2,4}(,[A-Za-z0-9_\.\-]+@[A-Za-z0-9\.\-]+\.[A-Za-z]{2,4})*$/", " e-mail", true);
		$this->globalPattern["PASSWORD_8_TO_15_CHAR_WITH_AT_LEAST_ONE_LETTER_AND_ONE_NUMBER"] = new patternMatch("/^(?=.*[A-Z]|[a-z])(?=.*\d)(.{8,15})$/", "between 8-15 characters and contain at least one letter and one number.", false);
    }

    abstract function validate();

    protected function checkMastEqualFields()
    {
        $flag = true;
        
        foreach ($this->data as $key=>$val) {
        	$res = $this->checkEqual($key, $val);

        	if (!$res) {
        		if (is_object($this->fieldsMustEqual[$key])) {
                            
                            if (array_key_exists("Amount_Per", $this->data) && $this->data['Amount_Per'] == 'Visit' && $key=='Qty_Devices') 
                            {
                                $this->addError('# of Visit must be '.$this->fieldsMustEqual[$key]->getDescription());
                            }
                            else
                            {
        			$this->addError($key.' must be '.$this->fieldsMustEqual[$key]->getDescription());
                            }
                                
        		} elseif (is_array($this->fieldsMustEqual[$key])) {
        	       $this->addError($key." value ($val) not from list");
        		} else {
        	       $this->addError($key.' parameter value is not correct');
        	    }
        	    $flag = false;
        	}
        }
        return $flag;
    }

    protected function checkRequredFields()
    {
        foreach ($this->requredFields as $field) {
        	if (empty($this->data[$field])) {
                    if (array_key_exists("Amount_Per", $this->data) && $this->data['Amount_Per'] == 'Visit' && $field=='Qty_Devices') 
                            {
                                $this->addError('# of Visit empty but required');
                            }
                            else
                            {
                                $this->addError($field.' empty but required');
                            }
        	}
        }
       	return !empty($this->errors) ? false : true;
    }

    protected function addError($description)
    {
        $this->errors[] = $description;
    }

    protected function checkUniqueField($tableName, $fieldName)
    {
		if (empty($this->data[$fieldName])) {
			return false;
		}

		$db = Core_Database::getInstance();
		$select = $db->select();

		$select->from($tableName, "$fieldName");
		$select->where("$fieldName = ?", $this->data[$fieldName]);

		if (!empty($this->data["Company_ID"]))
			$select->where("Company_ID = ?", $this->data["Company_ID"]);

		$results = Core_Database::fetchAll($select);

		if ($results) {
			$this->addError("$fieldName already exist");
			return false;
		}
		return true;
    }

    public function getErrors()
    {
        return $this->errors;
    }


    private function checkEqual($name, $value)
    {
		$checkRequired = array_key_exists($name, $this->fieldsMustEqual);
        $value = trim($value, '\'');
		if ($checkRequired && is_object($this->fieldsMustEqual[$name])) {
			return $this->fieldsMustEqual[$name]->checkValue($value);
		} elseif ($checkRequired && is_array($this->fieldsMustEqual[$name])) {
			return in_array($value, $this->fieldsMustEqual[$name]);
		} elseif ($checkRequired) {
			return ($value == $this->fieldsMustEqual[$name]);
		}

		return true;
	}

	protected function checkValidFieldName($tableName, $fieldName1, $fieldName2)
	{
		if (is_numeric($this->data[$fieldName2])) {
			$compareSign = ' = ';
		} else {
			$compareSign = ' LIKE ';
		}

		$db = Core_Database::getInstance();
		$select = $db->select();

		$select->from($tableName, "$fieldName1");
		$select->where("$fieldName1 $compareSign ?", $this->data[$fieldName2]);

		$results = Core_Database::fetchAll($select);

		if (!empty($results[0])) {
			return true;
		}
		$this->addError($fieldName2.' is not from the list of available values');
		return false;
	}

	protected function checkValidFieldNameCaseSensitive($tableName, $fieldName1, $fieldName2)
	{
		if (is_numeric($this->data[$fieldName2])) {
            $compareSign = ' = ';
        } else {
            $compareSign = ' LIKE ';
        }

		$db = Core_Database::getInstance();
		$select = $db->select();

		$select->from($tableName, "$fieldName1");
		$select->where("$fieldName1 $compareSign ?", $this->data[$fieldName2]);

		$results = Core_Database::fetchAll($select);

		if (!empty($results[0]) && $results[0][$fieldName1] == $this->data[$fieldName2]) {
			return true;
		}
		$this->addError($fieldName2.' is not from the list of available values');
		return false;
	}

	protected function twoFieldValidate($tableName, $fieldName1, $fieldName2, $v1, $v2)
	{
		$db = Core_Database::getInstance();
		$select = $db->select();

		$select->from(Core_Database::TABLE_WO_CATEGORIES, "$fieldName1,$fieldName2");

		$results = Core_Database::fetchAll($select);

		$map = array();
		foreach ($results as $val) {
			$map[$val[$fieldName2]] = $val[$fieldName1];
		}

		if (array_key_exists($this->data[$v2], $map)) {
        		$this->data[$v1] = $map[$this->data[$v2]];
		} else {
			return false;
		}
	}

	protected function validateGeneralFields()
	{
		for ($i = 1; $i <= 10; $i++) {
			$fieldName = "General" . $i;
			if (isset($this->data[$fieldName]) && strlen($this->data[$fieldName]) > 255) {
				$this->addError('Maximum length of General Information ' . $i . ' field is 255 characters');
			}
		}
	}
}

class patternMatch
{
	private $pattern;
	private $description;
	private $matchBlank;
	public function patternMatch($p, $d, $matchBlank = TRUE) {
		$this->pattern = $p;
		$this->description = $d;
		$this->matchBlank = $matchBlank;
	}
	public function getDescription() {
		return $this->description;
	}
	public function checkValue($value) {

		return (($this->matchBlank && $value == "") || preg_match($this->pattern, $value) == 1);
	}
}
