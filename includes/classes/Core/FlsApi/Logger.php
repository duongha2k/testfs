<?php
class Core_FlsApi_Logger extends Core_Api_Logger
{   
	static private $instance = NULL;
	private $logId     = null;
	private $logUrl    = null;
	private $companyId = null;
	private $login     = null;
    
	private function __construct(){}
    
	private function __clone(){}

     public static function getInstance()
     {
          if (self::$instance == NULL)
               self::$instance = new Core_FlsApi_Logger();
              
          return self::$instance;
     }
    
     public function addLog($error, $errorId=null, $description=null, $resultCode=null)
     {
//          $db = Zend_Registry::get('DB');
         
          $ip  = $_SERVER['REMOTE_ADDR'];
//          $date = date('Y-m-d h:i:s', time());

		  date_default_timezone_set('US/Central');
          $date = date('m-d-Y H:i:s', time());
         
          $row = array (
              'url'             => $this->logUrl,
              'result_code'     => $resultCode,
              'ip'              => $ip,
              'time_requested'  => $date,
              'client_username' => $this->login,
              'error'           => $error,
              'error_id'        => $errorId,
              'error_description'     => $description
          );
       
        if ($this->companyId !== null) {
            $row['company_id'] = $this->companyId;  
        }

//          $table = 'logger';

          $fieldsList = implode(",",array_keys($row));
          $fieldsData = "'" . implode("','",$row). "'";

// $fieldsList = "url, result_code, ip, time_requested, client_username, company_id, error, error_id, error_description";
// $fieldsData = "'', 'TB_UNID=>TEST', '192.168.100.107', '09-01-2009 05:42:07', 'gbailey', 'BW', 'No', '0', 'This was a test'";
 $result = Core_Caspio::caspioInsert("API_Log", $fieldsList, $fieldsData);
    
/*
          if (empty($this->logId)) {              
               $rows_affected = $db->insert($table, $row);
               $result = Core_Caspio::caspioInsert("API_Log", $fieldsList, $fieldsData);
                 $row['error_description'] .= $result[0];  
                    $fieldsData = "'" . implode("','",$row). "'";
                    $result = Core_Caspio::caspioInsert("API_Log", $fieldsList, $fieldsData);
               $this->logId = '266';
          } else {         
               $where = $db->quoteInto('id = ?', $this->logId);
               $rows_affected = $db->update($table, $row, $where);

               $criteria = 'id = $this->logId';
               $row['error_description'] .= $criteria;  
               $fieldsData = "'" . implode("','",$row). "'";
               $result = Core_Caspio::caspioInsert("API_Log", $fieldsList, $fieldsData);
//               $result = Core_Caspio::caspioUpdate("API_Log", $fieldsList, $fieldsData, $criteria);
          }
*/
     }    
}
