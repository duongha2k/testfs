<?php

class Core_FlsApi_User extends Core_Api_User
{	
	/**
	 * Check up user information
	 *
	 * @param array $authData
	 * @return bool
	 */
	public function checkAuthentication($authData)
	{
		$errors = Core_Api_Error::getInstance();
		
		$this->login = Core_Caspio::caspioEscape($authData['login']);
		$this->password = Core_Caspio::caspioEscape($authData['password']);
		
		if (empty($this->login) || empty($this->password)) {
			$errors->addError(2, 'Login or password is empty but required');
			$this->authenticate = false;
			return false;
		}
		
		if (!$this->checkProtocol()) {
			throw new SoapFault("jeff", "failing in check protocol");
			$this->authenticate = false;
			return false;
		}
		
		$criteria = 'Username = \''.$this->login.'\''
		          . ' AND Password = \''.$this->password.'\'';
		
		$user = Core_Caspio::caspioSelectAdv(TABLE_FLS_USERS_ADMIN, 'AdminID', $criteria,'');
		
		if (!empty($user)) {
			$this->authenticate = true;
			$this->companyId = "FLS";
			$this->companyName = "FLS";
		} else {
			$er = $errors->getErrors();
			if (empty($er)) {
				$errors->addError(3, 'Login or password is not correct');
			}
			$this->authenticate = false;
		}
		
		return $this->authenticate;
	}
}