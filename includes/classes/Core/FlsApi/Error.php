<?php
class Core_FlsApi_Error extends Core_Api_Error
{
	public static function getInstance()
	{
		if (self::$instance == NULL)
			self::$instance = new Core_FlsApi_Error();

		return self::$instance;
	}
}