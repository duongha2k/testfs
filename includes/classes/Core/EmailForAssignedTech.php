<?php

class Core_EmailForAssignedTech
{

    protected $tech;
    protected $wo;
    protected $project;
    protected $companyId;
    protected $companyName;
    protected $mail;

    protected $curWO;
    protected $Reasigned;

    function __construct()
    {
        $this->mail = new Core_Mail();
    }
    
    public function setWOData($wo) {
        $this->curWO = $wo;
    }    

    public function setTech($tech)
    {
        if (is_object($tech)) {
        	$this->tech = $tech;
        } else {
            $this->tech = new Core_Tech((int)$tech);
        }
    }
    
    public function setWorkOrder($workOrder)
    {
        $this->wo = (int)$workOrder;
    }
    
    public function setProject($project)
    {
        if (is_object($project)) {
        	$this->project = $project;
        } else {
            $this->project = new Core_Project((int)$project);
            $this->project->setFields('Project_Name,From_Email,Resource_Coordinator,P2TNotificationEmail,AssignmentNotificationEmail,NotifyOnAssign,NotifyOnAutoAssign');
            $this->project->setData();
        }
    }
    
    public function setCompanyId($companyId)
    {
        $this->companyId = $companyId;
    }
    
    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName;
    }
    
    public function setReasigned($value)
    {
        $this->Reasigned = $value;
    }

    public function sendMail()
    {
	$techID = $this->tech->getTechID();
//        $FirstName = $this->tech->getFirstName();
//        $LastName  = $this->tech->getLastName();
        
        $api = new Core_Api_TechClass;
        $techProperties = $api->getProperties($techID);

        $FirstName = $techProperties['FirstName']; 
        $LastName  = $techProperties['LastName'];
        $PrimaryEmail = $this->tech->getPrimaryEmail();
        //$PrimaryEmail = 'alexander.scherba@warecorp.com';
        $TechAddress1 = $this->tech->getAddress1();
        $TechAddress2 = $this->tech->getAddress2();
        $TechCity     = $this->tech->getCity();
        $TechState    = $this->tech->getState();
        $TechZipCode  = $this->tech->getZipCode();
        $TechPrimaryPhone   = $this->tech->getPrimaryPhone();
        $TechSecondaryPhone = $this->tech->getSecondaryPhone();

       	$projectName = $this->project->getProject_Name();

	$tbUNID = $this->wo;

	$curWO = $this->curWO;

	$startDate = "";
	$startTime = "";
	$city = "";
	$headline = "";

        $WosClass = new Core_Api_WosClass();
                                    
                                    
	if ($curWO) {
		$startDate = $curWO["StartDate"];
		$startTime = $curWO["StartTime"];
		$city = $curWO["City"];
		$state = $curWO["State"];
		$headline = $curWO["Headline"];
		$siteName = $curWO["SiteName"];
		$siteAddress = $curWO["Address"];
		$siteZip = $curWO["Zipcode"];
		$isProjectAutoAssign = $curWO["isProjectAutoAssign"];
		$workOrderID = $curWO["WO_ID"];

		$projectManager = $curWO["ProjectManagerName"];
		$projectManagerEmail = $curWO["ProjectManagerEmail"];
		$PcntDeduct = $curWO["PcntDeduct"];
                $PcntDeductPercent = $curWO["PcntDeductPercent"];
		$endDate = $curWO["EndDate"];
		$endTime = $curWO["EndTime"];

		$startRange = $curWO["StartRange"];
		$duration = $curWO["Duration"];
		$tech_Bid_Amount = $curWO["Tech_Bid_Amount"];
                $bid_comments = $curWO["bid_comments"];
		$amountPer = $curWO["Amount_Per"];
		$qtyDevices = $curWO["Qty_Devices"];
		$qtyDevices = ($amountPer == "Device" ? "($qtyDevices device(s))" : "");
		$projectManagerPhone = $curWO["ProjectManagerPhone"];
                $SiteContactName = $curWO["Site_Contact_Name"];
                $Address = $curWO["Address"];
                $SitePhone = $curWO["SitePhone"];
                $Description = $curWO["Description"];
                $Requirements = $curWO["Requirements"];
                $SpecialInstructions = $curWO["SpecialInstructions"];
                $Ship_Contact_Info = $curWO["Ship_Contact_Info"];
                
                
	}
        $Core_Api_WosClass = new Core_Api_WosClass();
        $WOVisitList = $Core_Api_WosClass->GetWOVisitList($tbUNID);
//        print_r("<pre>");
//        
//        print_r($WOVisitList);
//        print_r("</pre>");
        //334
	//$deductMsg = $PcntDeduct == "True" || $PcntDeduct == 1 ? "A 10% service fee will be deducted from your final total payment amount from this work order." : "NO service fee is being deducted from this work order.";
        if (empty($PcntDeductPercent) || $PcntDeductPercent == 0)
        {
            $PcntDeductPercentMsg = "No service fee will be deducted from your final total payment amount from this work order.";
        } else if ($PcntDeductPercent == 1)
        {
            $PcntDeductPercentMsg = "You will be paid outside of the FieldSolutions Platform for this work order.";
        } else
        {
            $PcntDeductPercentMsg = "A " . ($PcntDeductPercent * 100) . "% service fee will be deducted from your final total payment amount from this work order.";
        }
        
        $BidInfo = $WosClass->getLastBidInfo($tbUNID,$techID);
        $message = "Dear $FirstName $LastName,
        
	Congratulations, your application for FieldSolutions work was accepted. You have been assigned:
        
        WIN#: $tbUNID
        Client Work Order ID#: $workOrderID 
        Work: $headline 
        Project: $projectName
        Site Address: $Address
        City, State, Zip: $city, $state, $siteZip
        Site Contact Name: $SiteContactName
        Site Phone Number: $SitePhone ";

        if(!empty($WOVisitList))
        {
            $i = 1;
            foreach($WOVisitList as $visititem)
            {
                if($visititem["StartTypeID"] == 1)
                    $visitStartType = "Firm Start Time";
                else
                    $visitStartType = "Start Time Window";
                $visitStartDate = !empty($visititem['StartDate'])? date_format(new DateTime($visititem['StartDate']),"m/d/Y"):"";
                $visitStartTime = $visititem['StartTime'];
                $visitEndDate = !empty($visititem['EndDate'])? date_format(new DateTime($visititem['StartDate']),"m/d/Y"):""; 
                $visitEndTime = $visititem['EndTime'];
                $EstimatedDuration = $Core_Api_WosClass->getDurationDesc($visititem["EstimatedDuration"]); 
                $TechArrivalInstructions = $visititem['TechArrivalInstructions'];
                $message .= "

        Visit #$i: 
        Start Type: $visitStartType
        Work Start: $visitStartDate $visitStartTime
        Work End: $visitEndDate  $visitEndTime
        Duration: $EstimatedDuration
        Arrival Instructions: $TechArrivalInstructions";
                $i++;
            }
        }

	$message .="
        
	Pay: $tech_Bid_Amount PER $amountPer $qtyDevices		
	$PcntDeductPercentMsg
        ";
        if(!empty($BidInfo))
        {
        $message .="Bid Amount: ". $BidInfo['BidAmount']." PER $amountPer $qtyDevices
        Comments: ".$BidInfo['Comments'];
        }
        else
        {
            $message .="Bid Amount: 
        Comments: ";
        }
	$message .="
	THIS IS A COMMITTED ASSIGNMENT OF WORK TO YOU.

        Work Description
        ";
        $message = nl2br($message);
        $apicom = new Core_Api_CommonClass();
        if ($apicom->isStringHTML($Description))
        {
            $message .= $Description;
        } else
        {
            $message .= nl2br($Description);
        }
        
        $message .= "Tech Requirements and Tools<br/>";
        if ($apicom->isStringHTML($Requirements))
        {
            $message .= $Requirements;
        } else
        {
            $message .= nl2br($Requirements);
        }

        $message .= "Special Instructions<br/>";
        if ($apicom->isStringHTML($SpecialInstructions))
        {
            $message .= $SpecialInstructions;
        } else
        {
            $message .= nl2br($SpecialInstructions);
        }
        if ($apicom->isStringHTML($Ship_Contact_Info))
        {
            $message .= $Ship_Contact_Info;
        } else
        {
            $message .= nl2br($Ship_Contact_Info);
        }
	$message1 = "
	If for any reason you are no longer available, please contact $projectManager (phone: $projectManagerPhone email: $projectManagerEmail) immediately.  
	CONFIRM YOUR ACCEPTANCE OF THE WORK ORDER TODAY.
        Click this link to CONFIRM YOUR ASSIGNMENT (<a href='https://www.fieldsolutions.com/techs/wosDetails.php?id=$tbUNID&v=$techID'>https://www.fieldsolutions.com/techs/wosDetails.php?id=$tbUNID&v=$techID</a>) to WIN# $tbUNID. You may be required to login to your account if you are not already.

        Step 1: Review the work order details in the SECTION 1 of the work order.
        Step 2: CONFIRM your ACCEPTANCE of this work order by clicking the 'Confirm Now' button in the upper right corner of the work order.
        Step 3: Then, 24 hours before the work order begins, click the 'Confirm Now' button in the upper right corner of the work order to confirm that you will on-site the next day.
        Step 4: To REVIEW and PRINT project documentation, click the Documents icon in the Work Order Tools section of the work order.
        Step 5: To PRINT Sign-Off Sheet or Full Work Order (as required by the client), click the Print icon in the Work Order Tools section of the work order.

        As a requirement for this Work Order, you will receive one or more automated Reminder Calls. When you receive a Reminder Call, press 1 on your phone's key pad to confirm that you will be on site as scheduled. If you do not answer, the system will attempt to contact you again. 

        We appreciate your professional commitment to quality work for our clients. 
        Your FieldSolutions Team
	";
       	$message .= nl2br($message1);
       	
    	$wo = new Core_WorkOrder($tbUNID);
        
        //736
        $result = $Core_Api_WosClass->getSystemGeneratedEmailAddress_ByWinNum($tbUNID);
        $vFromEmail = $result["SystemGeneratedEmailFrom"];
        if ($vFromEmail == ""){
            $vFromEmail = "Projects@fieldsolutions.com";
		}
        //End 736
	

        $this->mail->setBodyText($message);
        $this->mail->setFromEmail($vFromEmail);
        $this->mail->setFromName($this->project->getResource_Coordinator());
        $this->mail->setToEmail($PrimaryEmail);
        $this->mail->setToName($FirstName.' '.$LastName);
        $this->mail->setSubject("Work Order Assigned: WIN# $tbUNID - [$headline]");

        $aipSchedulefile = new Core_Api_Class();
        if($this->Reasigned)
        {
            $mySchedulefile = $aipSchedulefile->getAttachFileScheduleForWOAssignedToAnotherTech($tbUNID);
        }
        else
        {
            $mySchedulefile = $aipSchedulefile->getAttachFileScheduleForWOAssigned($tbUNID);
        }
//        print_r($mySchedulefile);
        $mySchedulefile = file_get_contents($mySchedulefile);
/*		if (!empty($mySchedulefile)) {
			$att = new Zend_Mime_Part($mySchedulefile);
			$att->type = 'application/octet-stream';
			$att->disposition = Zend_Mime::DISPOSITION_ATTACHMENT;
			$att->encoding = Zend_Mime::ENCODING_BASE64;
			$att->filename = $tbUNID.'_schedule.ics';
			$this->mail->addAttachments($att);
		}*/
		$this->mail->setTech($this->tech);
		$this->mail->isTechEmail(true);
        $this->mail->send();
        //$aipSchedulefile->deleteFile($mySchedulefile);
	// Send SMS if needed
	if ($isProjectAutoAssign == "True") {
		$smsSender = new Core_SMS();

		$smsMsg = "RUSH: You have been awarded WIN# $tbUNID $startTime $startDate $siteAddress $city $siteZip Go online to accept.";

		$smsSender->setToSMSWithTechIDList(array($techID));
		$smsSender->setFromName($this->project->getResource_Coordinator());
		$smsSender->setFromEmail($sysGenEmailFrom);
		$smsSender->setToName($FirstName.' '.$LastName);
		$smsSender->setSubject("WIN# $tbUNID: RUSH: You have been awarded");
		$smsSender->setBodyText($smsMsg);
		$smsSender->setTech($this->tech);
		$smsSender->isTechEmail(true);
		$smsSender->send();
	}

	$pID = $this->project->getId();

	$woAssignedEmailTo = $wo->getWorkAssignedEmailTo();
	$NotifyOnAssign = $this->project->getNotifyOnAssign() || !empty($woAssignedEmailTo);
	$NotifyOnAutoAssign = $this->project->getNotifyOnAutoAssign();
	$P2TNotificationEmail = ($wo->getP2TNotificationEmailTo() != "" && $wo->getP2TNotificationEmailTo() != NULL) ? $wo->getP2TNotificationEmailTo() : $this->project->getP2TNotificationEmail();
	$AssignmentNotificationEmail = ($wo->getWorkAssignedEmailTo() != "" && $wo->getWorkAssignedEmailTo() != NULL) ? $wo->getWorkAssignedEmailTo() : $this->project->getAssignmentNotificationEmail();
	
	//overwrite any emails that have data in the future info table
	/*
	$future = new Core_FutureWorkOrderInfo();
	$futureData = $future->find($this->project->getCreatedBy)->toArray();
	if(!empty($futureData)){
		if(!empty($futureData['WorkAssignedEmailTo']))$AssignmentNotificationEmail = $futureData['WorkAssignedEmailTo'];
		if(!empty($futureData['P2TNotificationEmailTo']))$P2TNotificationEmail = $futureData['P2TNotificationEmail'];
	}
	*/
	if ($NotifyOnAutoAssign == "True" || $NotifyOnAssign == "True") {
		$eList1 = $NotifyOnAssign == "True" ? $AssignmentNotificationEmail : "";
		$eList2 = $NotifyOnAutoAssign == "True" ? $P2TNotificationEmail : "";

		$recpList = array();
		if ($eList1 != "")
			$recpList[] = $eList1;
		if ($eList2 != "")
			$recpList[] = $eList2;

		$recp = implode(",", $recpList);

		if ($recp != "") {
			//$content = "This note to is to notify you that work order ($workOrderID / WIN# $tbUNID) has been assigned to ($techID / $FirstName $LastName).  The event is scheduled for $startDate at $startTime.\n\n";
			$content = "This note is to notify you that a Work Order has been assigned.\n\n";
			$content .= "WIN#: $tbUNID\n";
			$content .= "Client WO ID#: $workOrderID\n";
			$content .= "Project: {$curWO["Project_Name"]}\n";
			$content .= "Headline: $headline\n";
			$content .= "Region: {$curWO["Region"]}\n";
			$content .= "Route: {$curWO["Route"]}\n";
			$content .= "Site: $siteName\n";
			$content .= "$city, $state $siteZip\n";
			$content .= "Start Date/Time: $startDate $startTime\n";
			$content .= "End Date/Time: $endDate $endTime\n";
			$content .= "FS-Tech ID#: $techID\n";
			$content .= "Tech Name: $FirstName $LastName\n";
			$content .= "Tech Phone (Day): {$this->tech->getPrimaryPhone()}\n";
			$content .= "Tech Phone (Evening): {$this->tech->getSecondaryPhone()}\n";
			$content .= "Tech Email: {$this->tech->getPrimaryEmail()}\n\n";
			$content .= "Thank you,\n"; 
			$content .= "Your FieldSolutions Team\n";

			$P2TLabel = $isProjectAutoAssign == "True" ? "RUSH:" : "";
			$notifySender = new Core_Mail();
			$notifySender->setFromName("FieldSolutions");
			$notifySender->setFromEmail("nobody@fieldsolutions.com");
			$notifySender->setToEmail($recp);
			$notifySender->setSubject("WIN# $tbUNID: $P2TLabel Work Order ($workOrderID) has been Assigned");
			$notifySender->setBodyText($content);
			$notifySender->send();
		}
	}

    $db = Core_Database::getInstance();
    $query = $db->select();
    $query->from(Core_Database::TABLE_WORK_ORDER_BIDS, array('TechID','Bid_Date','BidAmount'));
    $query->where('WorkOrderID = ?', $tbUNID);
    $query->where('TechID <> ?', $techID);
    $outBidded = Core_Database::fetchAll($query);
    
//    if ( !$result ) $outBidded = API_Response::fail();
//    else $outBidded = API_Response::success($this->makeApplicantsArray($result));

	if (!empty($outBidded)) {
		$techList = array();
		foreach ($outBidded as $bid) {
			$techList[] = $bid["TechID"];
		}
		$db = Core_Database::getInstance();
		$outbidTech = $db->select()->from(Core_Database::TABLE_TECH_BANK_INFO, array("TechID", "Firstname", "Lastname", "PrimaryEmail"))->where("TechID IN (?)", $techList)->query()->fetchAll();
		$outbidTechInfo = array();
		foreach ($outbidTech as $k => $row) {
			$outbidTechInfo[$row["TechID"]] = $row;
		}

		foreach ($outBidded as $bid) {
			// construct email

			// Get data from array and set values for email
			$techID = $bid["TechID"];
			$FirstName = $outbidTechInfo[$techID]["Firstname"];
			$LastName = $outbidTechInfo[$techID]["Lastname"];
			$sendToEmail = $outbidTechInfo[$techID]["PrimaryEmail"];
			$bidDate = $bid["Bid_Date"];
			$bidAmount = $bid["BidAmount"];
				$message = "
	Dear $FirstName $LastName,

	Thank you for your bid on WIN# $tbUNID for $headline that was scheduled for $startDate $startTime located $city, $state. This work order has been assigned to another technician. (Important: the name ID# for the work order has been changed to WIN#)

	Three things you can do to win more work:
		1.  Add more details to your profile :  add more skills, certifications and a resume to your profile
		2.  Build your track record in Field Solutions; bid a few jobs at a  lower  price to get some work history and strong ratings 
		3.  Bid a lower price:  think about your bids, maybe bid a few $ lower than the client's \"offer\" to get noticed.

	Also, please enroll in the Fujitsu FLS program through FieldSolutions to see more work orders.

	Thank you and good hunting!
	Field Solutions Technician Support Team at support@fieldsolutions.com
	";

		        $this->mail = new Core_Mail();
		        $this->mail->setBodyText($message);
		        $this->mail->setFromEmail("no-replies@fieldsolutions.us");
		        $this->mail->setFromName("Field Solutions");
		        $this->mail->setToEmail($sendToEmail);
		        $this->mail->setToName($FirstName.' '.$LastName);
		        $this->mail->setSubject("WIN# $tbUNID: Thanks for bidding; FS Work Order Assigned to Another Tech");
                //3296
                $mySchedulefileTAT = $aipSchedulefile->getAttachFileScheduleForWOAssignedToAnotherTech($tbUNID);
                $mySchedulefileTAT = file_get_contents($mySchedulefileTAT);
/*				if (!empty($mySchedulefileTAT)) {
					$attTAT = new Zend_Mime_Part($mySchedulefileTAT);
					$attTAT->type = 'application/octet-stream';
					$attTAT->disposition = Zend_Mime::DISPOSITION_ATTACHMENT;
					$attTAT->encoding = Zend_Mime::ENCODING_BASE64;
					$attTAT->filename = $tbUNID . '_schedule.ics';
					$this->mail->addAttachments($attTAT);
				}*/
				$this->mail->setTech($this->tech);
				$this->mail->isTechEmail(true);
 		        $this->mail->send();
		}
	}
   }

}
