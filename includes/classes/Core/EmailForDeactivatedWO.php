<?php

class Core_EmailForDeactivatedWO
{

    protected $tech;
    protected $wo;
    protected $project;
    protected $companyId;
    protected $companyName;
    protected $mail;

    protected $curWO;

    function __construct()
    {
        $this->mail = new Core_Mail();
    }
    
    public function setWOData($wo) {
        $this->curWO = $wo;
    }    

    public function setTech($tech)
    {
        if (is_object($tech)) {
        	$this->tech = $tech;
        } else {
            $this->tech = new Core_Tech((int)$tech);
        }
    }
    
    public function setWorkOrder($workOrder)
    {
        $this->wo = (int)$workOrder;
    }
    
    public function setProject($project)
    {
        if (is_object($project)) {
        	$this->project = $project;
        } else {
            $this->project = new Core_Project((int)$project);
            $this->project->setFields('Project_Name,From_Email,Resource_Coordinator');
            $this->project->setData();
        }
    }
    
    public function setCompanyId($companyId)
    {
        $this->companyId = $companyId;
    }
    
    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName;
    }
    
    public function sendMail()
    {
                
		$curWO = $this->curWO;
		//736
                $wosClass = new Core_Api_WosClass;
                $result = $wosClass->getSystemGeneratedEmailAddress_ByWinNum($this->wo);
                $vFromEmail = $result["SystemGeneratedEmailFrom"];
                if ($vFromEmail == ""){
                    $vFromEmail = "Projects@fieldsolutions.com";
                }
                //End 736
		
		if (!empty($curWO["ResourceCoordinatorName"])) {
			$rc = $curWO["ResourceCoordinatorName"];
			$rcPhone = $curWO["ResourceCoordinatorPhone"];
		}
		else {
			$rc = $this->project->getResource_Coordinator();
			$rcPhone = $this->project->getResource_Coord_Phone();
		}
				
		$message = "
	This is notification that the below work order to which you had been assigned has been deactivated by the client:
	 
	Win#: {$this->wo}
	Client Work Order #: {$curWO["WO_ID"]}
	Start Date: $startDate {$curWO["StartDate"]}
	Site Name: {$curWO["SiteName"]}
	Site Number: {$curWO["SiteNum"]}
	City, State, ZipCode: {$curWO["City"]}, {$curWO["State"]}, {$curWO["ZipCode"]}
	Headline: {$curWO["Headline"]}
	 
	Client Contact Info:
	Coordinator:  $rc
	Telephone:  $rcPhone
	 
	This work order is no longer available to be viewed in the Field Solutions system. If you have any concerns, please call the client directly.
	
	Thank you,
	The Field Solutions Team";
		
		$this->mail->setBodyText($message);
        //$this->mail->setFromEmail($this->project->getFrom_Email());
        $this->mail->setFromEmail($vFromEmail);
         
        $this->mail->setFromName($this->project->getProject_Manager());
        $this->mail->setToEmail($this->tech->getPrimaryEmail());
        $this->mail->setToName($this->tech->getFirstName().' '.$this->tech->getLastName());
        $this->mail->setSubject("WIN# {$this->wo} - Deactivated By Client");
        $aipSchedulefile = new Core_Api_Class();
        $mySchedulefile = $aipSchedulefile->getAttachFileScheduleForWODeactivatedToAnotherTech($this->wo);
        $mySchedulefile = file_get_contents($mySchedulefile);
/*		if (!empty($mySchedulefile)) {
			$att = new Zend_Mime_Part($mySchedulefile);
			$att->type = 'application/octet-stream';
			$att->disposition = Zend_Mime::DISPOSITION_ATTACHMENT;
			$att->encoding = Zend_Mime::ENCODING_BASE64;
			$att->filename = $this->wo . '_schedule.ics';
			$this->mail->addAttachments($att);
		}*/
		$this->mail->setTech($this->tech);
		$this->mail->isTechEmail(true);
        $this->mail->send();
	}

}
