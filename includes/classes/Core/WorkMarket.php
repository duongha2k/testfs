<?php
class Core_WorkMarket
{
	const API_USER = "auto-assign";
	const API_PASSWORD = "aut0assign210";
	const WM_API_NAME = "API API";
    protected $wm_assignment_id;
	
	protected static $wm = NULL;
	protected static $mode = WorkMarket_Api_Class::MODE_TEST;
	
	const WM_TOKEN = "5l7IuedcT1ctNAMCWh8v";
	const WM_SECRET = "DK9glQ09Le19qyODi7njZzoPljCbpdt0dEDXrqGC";
	
	const WM_TOKEN_PROD = "E44POkCcO0LdjuejLAxV";
	const WM_SECRET_PROD = "nqr1MmxiNhEAaFIulqqgxRSyQoHApcgr3n6Y5SRl";
	
	const WM_TEMPATE_FIRST_BIDDER = "1264396193";
	const WM_TEMPATE_AT_EXPIRE = "1210972233";
	
	const WM_TEMPATE_FIRST_BIDDER_PROD = "1378944179";
	const WM_TEMPATE_AT_EXPIRE_PROD = "1007522600";
	
	protected $win_num;
	protected $assignment_id;
	
	protected $currentUserSession;
	protected $groupIDMap;
	
	protected static $template_first_bidder;
	protected static $template_at_expire;

	public function __construct($win_num, $assignment_id = NULL) {
		self::connectWM();
		$this->win_num = $win_num;
		if (empty($assignment_id))
			try {
				$this->assignment_id = $this->getAssignmentIdWithWinNum($win_num);
			} catch (Exception $e) {}
		else
			$this->assignment_id = $assignment_id;
	}

	public static function connectWM() {
		$cfgSite = Zend_Registry::get('CFG_SITE');
		$site_name_sub_domain = $cfgSite->get("site")->get("site_name_sub_domain");
		if (empty($site_name_sub_domain))$site_name_sub_domain = "www";
		if ($site_name_sub_domain == 'www' || $site_name_sub_domain == 'api')
			self::$mode = WorkMarket_Api_Class::MODE_PROD;
		if (is_null(self::$wm)) {
			if (self::$mode == WorkMarket_Api_Class::MODE_PROD) {
				$token = self::WM_TOKEN_PROD;
				$secret = self::WM_SECRET_PROD;
				self::$template_first_bidder = self::WM_TEMPATE_FIRST_BIDDER_PROD;
				self::$template_at_expire = self::WM_TEMPATE_AT_EXPIRE_PROD;
			}
			else {
				$token = self::WM_TOKEN;
				$secret = self::WM_SECRET;
				self::$template_first_bidder = self::WM_TEMPATE_FIRST_BIDDER;
				self::$template_at_expire = self::WM_TEMPATE_AT_EXPIRE;
			}
			self::$wm = new WorkMarket_Api_Class($token, $secret, self::$mode);
			self::$wm->connect();
		}
	}
	
	public function startWMAssignment() {
		/* 
		- set Group visibility according to skill category
		- fill Support / Contact with RC info, or EC, or PM
		- Default Location Type Commerical
		- Set pay amount as net (ie. PayMax * (100-PcntDeduct))
		- Set First Tech to Accept according to FS-Rush
			- Yes if first bidder
			- No if lowest upon expiration
		*/
		if (!empty($this->assignment_id))
				$this->stopWMAssignment(true, 0.00, 'FS republish'); // void or cancel assignment
		
		$fswo = $this->getFSWorkOrder(array(											
			'ResourceCoordinatorEmail',
			'EmergencyEmail',
			'ProjectManagerEmail',
			'PayMax',
			'PcntDeduct',
			'PcntDeductPercent',
			'Amount_Per',
			'Qty_Visits',
			'Qty_Devices',
			'isWorkOrdersFirstBidder',
			'WO_Category_ID',
			'StartDate',
			'StartTime',
			'EndDate',
			'EndTime',
			'Address',
			'City',
			'State',
			'Zipcode',
			'Headline',
			'Description',
			'SiteName',
			'SiteNumber',
			'Site_Contact_Name',
			'SitePhone',
			'SiteEmail',
			'SpecialInstructions',
			'Duration',
			'Requirements'
		));
		$wo = new WorkMarket_Assignment();
		
//		$wo->scheduled_start = strtotime($fswo->StartDate . ' ' . $fswo->StartTime);
		$wo->scheduled_start_date = date('Y/m/d h:i A', strtotime($fswo->StartDate . ' ' . $fswo->StartTime));
//		$wo->scheduled_end = strtotime($fswo->EndDate . ' ' . $fswo->EndTime);
		$wo->scheduled_end_date = date('Y/m/d h:i A', strtotime($fswo->EndDate . ' ' . $fswo->EndTime));
		$wo->location_address1 = $fswo->Address;
		$wo->location_city = $fswo->City;
		$wo->location_state = $fswo->State;
		$wo->location_zip = $fswo->Zipcode;
		$wo->location_country = 'USA';
		$wo->title = $fswo->Headline;
		$wo->description = $fswo->Description . "\n\n";
		$wo->description .= $fswo->Requirements . "\n\n";
		$wo->instructions = $fswo->SpecialInstructions;
		$wo->private_instructions = 0;
		$wo->location_name = $fswo->SiteName;
		$wo->location_number = $fswo->SiteNumber;
		$wo->location_contact = array(array(
			'first_name' => $fswo->Site_Contact_Name,
			'email' =>  $fswo->SiteEmail,
			'phone' => $fswo->SitePhone
		));

		$wo->send_to_groups = array($this->getWMGroupIDWithFSSkillCat($fswo->WO_Category_ID)); // group id based on fs skill category
		$wo->support_contact_email = $fswo->ResourceCoordinatorEmail;
		if (empty($wo->support_contact_email))
			$wo->support_contact_email = $fswo->EmergencyEmail;
		if (empty($wo->support_contact_email))
			$wo->support_contact_email = $fswo->ProjectManagerEmail;
			
		$wo->template_id = $fswo->isWorkOrdersFirstBidder == 1 ? self::$template_first_bidder : self::$template_at_expire;
		$wo->send_radius = 50; // mile radius to send
		$wo->location_type = 1; // Default Location Type to Commerical
		
		$deduct = empty($fswo->PcntDeduct) ? 1.0 : (1.0 - $fswo->PcntDeductPercent);
		switch ($fswo->Amount_Per) {
			case 'Hour':
				$wo->pricing_max_number_of_hours = $fswo->Duration;
				$wo->pricing_type = 'per_hour';
				$wo->pricing_per_hour_price = $deduct * $fswo->PayMax;
				break;
			case 'Device':
			case 'Visit':
				$wo->pricing_type = 'per_unit';
				$wo->pricing_per_unit_price = $deduct * $fswo->PayMax;
				$wo->pricing_max_number_of_units = $fswo->Amount_Per == 'Device' ? $fswo->Qty_Devices : $fswo->Qty_Visits;
				break;
			default:
				$wo->pricing_type = 'flat';
				$wo->pricing_flat_price = $deduct * $fswo->PayMax;
				break;
		}
		
		$result = self::$wm->assignmentCreate($wo);
		$this->assignment_id = $result->response->id;
		$this->setAssignmentId($this->assignment_id);
		return $this->assignment_id;
	}
	
	public static function pollWMAssignmentUpdated() {
		// check for any updated assignments and pull in their updates to FS
		self::connectWM();
		$date = new Zend_Date();
		$lastPollTime = self::getLastAssignmentPoll();
		$lastPollTime -= 60;
		$assignmentsList = self::$wm->assignmentListUpdated($lastPollTime);
		$assignmentsList = $assignmentsList->response;
		$total_results = $assignmentsList->total_results;
		$count = $assignmentsList->count;
		$last_row = $count;
		$rows = $assignmentsList->data;
		self::setLastAssignmentPoll($date, $total_results);
		if ($total_results > $count) {
			// get the rest of the modified assignments due to wm limit
			while ($last_row < $total_results) {
				$start = $last_row + 1;
				$assignmentsList = self::$wm->assignmentListUpdated($lastPollTime, $start);
				$assignmentsList = $assignmentsList->response;
				$rowsNew = $assignmentsList->data;
				$rows = array_merge($rows, $rowsNew);
				$countNewRows = count($rowsNew);
				$last_row += $countNewRows;
				if ($countNewRows == 0) break;
			}
		}
		$assignmentIds = array();
		foreach ($rows as $row) {
			$assignmentIds[] = $row->id;
		}
		$wins = self::getWinNumWithAssignmentIds($assignmentIds);
		if (empty($wins)) return;
		foreach ($rows as $row) {
			if (!array_key_exists($row->id, $wins)) continue;
			$win = $wins[$row->id];
			$labels = $row->labels;
			switch ($row->status) {
				case 'sent':
				case 'active':
//				case 'exception':
				case 'inprogress':
				case 'complete':
					// pull info for these statuses
					$assignment = self::$wm->assignmentGet($row->id);
					// process each modified assignment
					$wmClass = new self($win, $row->id);
					if ($row->status == 'sent')
						$wmClass->handleCounterOffer($assignment);
					if ($row->status == 'active' || $row->status == 'inprogress' || $row->status == 'complete' /*|| $row->status == 'exception'*/) {
						$techID = $wmClass->pullResourceInfo($assignment);
						$wmClass->readyWorkToRunThroughWM($assignment);
						$wmClass->pullCheckInOut($assignment);
						$wmClass->pullQuestions($assignment, $techID);
						$wmClass->pullNotes($assignment);
					}
					break;
				default:
					// ignore others
			}
		}		
	}
	
	protected static function getProcessed($table, $ids, $assignment_id = NULL) {
		if (empty($ids)) return array();
		if (!is_array($ids)) $ids = array($ids);
		$db = Core_Database::getInstance();
		$db->delete($table, "NOW() > DATE_ADD(datetime, INTERVAL 180 DAY)"); // remove old rows
		$select = $db->select();
		$select->from($table, array('id'))->where('id IN (?)', $ids);
		if (!empty($assignment_id))
			$select->where('WMAssignmentID = ?', $assignment_id);
		return $db->fetchCol($select);
	}
	
	protected static function setProcessed($table, $id, $assignment_id) {
		$db = Core_Database::getInstance();
		try {
			$db->insert($table, array(
				'id' => $id,
				'datetime' => new Zend_Db_Expr('NOW()'),
				'WMAssignmentID' => $assignment_id
			));
		} catch (Exception $e) {}
	}
	
	protected static function getOfferProcessed($ids) {
		return self::getProcessed('workmarket_offer_processed', $ids);
	}

	protected static function setOfferProcessed($id, $assignment_id) {
		self::setProcessed('workmarket_offer_processed', $id, $assignment_id);
	}

	protected static function getNoteProcessed($ids, $assignment_id) {
		return self::getProcessed('workmarket_note_processed', $ids, $assignment_id);
	}

	protected static function setNoteProcessed($id, $assignment_id) {
		self::setProcessed('workmarket_note_processed', $id, $assignment_id);
	}

	protected static function getQuestionProcessed($ids) {
		return self::getProcessed('workmarket_question_processed', $ids);
	}

	protected static function setQuestionProcessed($id, $assignment_id) {
		self::setProcessed('workmarket_question_processed', $id, $assignment_id);
	}

	protected static function getLastAssignmentPoll() {
		$db = Core_Database::getInstance();
		$db->delete('workmarket_poll', "NOW() > DATE_ADD(datetime, INTERVAL 14 DAY)"); // remove old rows
		$select = $db->select();
		$select->from('workmarket_poll', array('datetimeunix'))->order('id DESC')->limit(1);
		$result = $db->fetchOne($select);
		if (empty($result)) $result = strtotime('-1 hour');
		return $result;
	}

	protected static function setLastAssignmentPoll(Zend_Date $date, $total = 0) {
		// sets date of last poll using zend date object
		$db = Core_Database::getInstance();
		$db->insert('workmarket_poll', array(
			'datetime' => new Zend_Db_Expr("NOW()"),
			'datetimeunix' => $date->get(Zend_Date::TIMESTAMP),
			'modifiedCount' => $total
		));
	}

	public function acceptOffer($assignment = NULL, $WMID) {
		$assignment = $this->getAssignment($assignment);
		$pending_offers = $assignment->pending_offers;
		foreach ($pending_offers as $offer) {
			$id = $offer->id;
			$resource = $offer->resource;
			if ($resource->id != $WMID) continue;
			@$pricing = $offer->pricing;
			if (!empty($pricing->flat_price)) { 
				$bidAmount =  $pricing->flat_price;
			}
			else if (!empty($pricing->per_unit_price)) {
				$bidAmount =  $pricing->per_unit_price;
			}
			else if (!empty($pricing->per_hour_price)) {
				$bidAmount =  $pricing->per_hour_price;
			}
			if (empty($bidAmount)) {
				if (!empty($assignment->pricing->flat_price))
					$bidAmount = $assignment->pricing->flat_price;
				else if (!empty($assignment->pricing->per_hour_price))
					$bidAmount = $assignment->pricing->per_hour_price;
				else if (!empty($assignment->pricing->per_unit_price))
					$bidAmount = $assignment->pricing->per_unit_price;
			}

			$result = self::$wm->assignmentOffersAccept($id);
			$this->readyWorkToRunThroughWMBG();
			return $bidAmount;
		}
		return false;
	}

	public function handleCounterOffer($assignment = NULL) {
		/*
			- CounterOffer
			- New amount goes into Bid Amount
			- New time goes into Comments
			- Offer expiration goes into comments (maybe new field)
			- Do not auto assign WM CounterOffers
		*/
		$assignment = $this->getAssignment($assignment);
		$pending_offers = $assignment->pending_offers;
		@$this->currentUserSession = $_SESSION['UserObj']; // save session data
		$techClass = new Core_Api_TechClass;
		$idList = array();
		foreach ($pending_offers as $offer) {
			$idList[] = $offer->id;
		}
		
		$processedList = self::getOfferProcessed($idList);
		if (empty($processedList)) $processedList = array();
		$fswo = $this->getFSWorkOrder(
			array(
				'PcntDeduct',
				'PcntDeductPercent'
			)
		);
		$deduct = empty($fswo->PcntDeduct) ? 1.0 : (1.0 - $fswo->PcntDeductPercent);
		
		foreach ($pending_offers as $offer) {
			$id = $offer->id;
			if (in_array($id, $processedList)) continue; // skip already processed
			$resource = $offer->resource;
			@$note = $offer->note;
			@$scheduling = $offer->scheduling;
			@$pricing = $offer->pricing;
			if (!empty($pricing->flat_price)) { 
				$bidAmount =  $pricing->flat_price / $deduct;
				$note .= ' bid as flat rate';
			}
			else if (!empty($pricing->per_unit_price)) {
				$bidAmount =  $pricing->per_unit_price / $deduct;
				$note .= ' bid as per unit rate';
			}
			else if (!empty($pricing->per_hour_price)) {
				$bidAmount =  $pricing->per_hour_price / $deduct;
				$note .= ' bid as per hour rate';
			}
			if (empty($note)) $note = '';
			if (!empty($scheduling->request_scheduled_time)) {
				$date = $this->getAssignmentLocalTime($assignment, $scheduling->request_scheduled_time, "MM/dd/YYYY hh:mm a");
				$note = $note . ' Requested start date/time: ' . $date;
			}
			else if (!empty($scheduling)) {
				$dateStart = $this->getAssignmentLocalTime($assignment, $scheduling->request_window_start, "MM/dd/YYYY hh:mm a");
				$dateTo = $this->getAssignmentLocalTime($assignment, $scheduling->request_scheduled_end, "MM/dd/YYYY hh:mm a");
				$note = $note . ' Requested start window: ' . $dateStart . ' to ' . $dateTo;
			}
			if (!empty($offer->expires_on)) {
				$dateExpires = $this->getAssignmentLocalTime($assignment, $offer->expires_on, "MM/dd/YYYY hh:mm a");
				$note = $note . ' Bid expires: ' . $dateExpires;
			}
			$authData = Core_Api_TechUser::getTechLoginByWMID($resource->id);
			if (empty($authData)) {
				$techID = $this->saveResourceInfo($resource);
				$authData = Core_Api_TechUser::getTechLoginByWMID($techID['WMID']);
				$techID = $techID['TechID'];
			}

			if (empty($bidAmount)) {
				if (!empty($assignment->pricing->flat_price))
					$bidAmount = $assignment->pricing->flat_price;
				else if (!empty($assignment->pricing->per_hour_price))
					$bidAmount = $assignment->pricing->per_hour_price;
				else if (!empty($assignment->pricing->per_unit_price))
					$bidAmount = $assignment->pricing->per_unit_price;
			}

			$techClass->createBid($authData['login'], $authData['password'], $this->win_num, $bidAmount, $note); // make bid in FS
			self::setOfferProcessed($id, $this->assignment_id); // mark offer as processed
		}
		@$_SESSION['UserObj'] = $this->currentUserSession; // restore session data
	}
		
	public function getAssignmentIdWithWinNum($win_num) {
		if (!is_numeric($win_num)) {
			throw new Exception('Invalid WIN# passed');
		}
		// lookup wm assignment id
		$woClass = new Core_Api_Class;
		$additionalFields = $woClass->getWorkOrderAdditionalFields($win_num);
       
		$WMAssignmentID = $additionalFields['WMAssignmentID'];
		if (empty($WMAssignmentID)) {
			//throw new Exception('Unable to find WIN#');
		}
		return $WMAssignmentID;
	}

	public function setAssignmentId($WMAssignmentID) {
		if (empty($this->win_num) && !is_null($WMAssignmentID)) {
			throw new Exception('Unable to find WIN#');
		}
		$api = new Core_Api_Class;
		$api->updateWorkOrderAdditionalFields($this->win_num, array('WMAssignmentID' => $WMAssignmentID));
		return $WMAssignmentID;
	}

	public static function getWinNumWithAssignmentIds($assignmentIds) {
		if (!is_array($assignmentIds)) {
			throw new Exception('Invalid assignment id# passed');
		}
		if (empty($assignmentIds)) return array();
		// lookup wm assignment id
		$db = Core_Database::getInstance();		
		$select = $db->select();
		$select->from('work_orders_additional_fields', array('WMAssignmentID', 'WINNUM'))->where('WMAssignmentID IN (?)', $assignmentIds);
		return $db->fetchPairs($select);
	}

	public function prepareToAssignInFS() {
		// stops FS-Rush and prepare work order to be assigned in FS
		/*
			- When WO assigned in FS, check WM
				- If no VOID in WM
				- If yes shadow box already assigned or kill auto assign	
		*/
		$result = $this->stopWMAssignment();
		return $result;
	}
	
	public function readyWorkToRunThroughWMBG() {
		$params = array($this->win_num, $this->assignment_id);
		Core_BackgroundProcess::run("/usr/bin/php -f " . WEBSITE_LIB_PATH . "/readyWorkToRunThroughWM.php", $params);
	}
	
	public function pullResourceInfo($assignment = NULL) {
		$assignment = $this->getAssignment($assignment);
		$active_resource = $assignment->active_resource;
		$techID = $this->saveResourceInfo($active_resource);
		return $techID['TechID'];
	}
	
	public function pullCheckInOut($assignment = NULL) {
		$assignment = $this->getAssignment($assignment);
		$active_resource = $assignment->active_resource;
		$payment = $assignment->payment;
		$checkInOut = $active_resource->check_in_out[0];
		$checked_in = $checkInOut->checked_in_on;
		$checked_out = $checkInOut->checked_out_on;
		$resolution = $assignment->resolution; // closing comment
		
		@$this->currentUserSession = $_SESSION['UserObj']; // save session data		
		$client = new Core_Api_TechClassPublic;
		$wo = new API_TechWorkOrder();
		if (!empty($checked_in)) {
			// set checkout
			$dateTime = $this->getAssignmentLocalTime($assignment, $checked_in);
			$wo->Date_In = $dateTime->toString("MM/dd/YYYY");
			$wo->Time_In = $dateTime->toString("hh:mm a");
		}
		if (!empty($checked_out)) {
			// set checkout
			$dateTime = $this->getAssignmentLocalTime($assignment, $checked_in);
			$wo->Date_Out = $dateTime->toString("MM/dd/YYYY");
			$wo->Time_Out = $dateTime->toString("hh:mm a");
		}
		if ($assignment->status == "complete") {
			$wo->calculatedTechHrs = $payment->hours_worked;
			$wo->TechMarkedComplete = true;
			$wo->TechComments = $resolution;
		}
		$authData = Core_Api_TechUser::getTechLoginByWMID($active_resource->id);
		$client->updateWorkOrder($authData['login'],$authData['password'],$this->win_num, $wo);
		// pull attachments from resource
		$this->pullResourceAttachment($assignment, $authData);
		@$_SESSION['UserObj'] = $this->currentUserSession; // restore session data
	}
	
	public function pullResourceAttachment($assignment = NULL, $authData = NULL) {
		$assignment = $this->getAssignment($assignment);
		$attachments = $assignment->attachments;
		if (empty($attachments)) return; // no attachments
		$attachmentsTech = self::$wm->assignmentAttachmentList($assignment->id, true);
		$attachmentsTech = $attachmentsTech->response;
		$techUploads = array();
		foreach ($attachmentsTech as $attachment) {
			$techUploads[] = $attachment->uuid;
		}
				
		$techClass = new Core_Api_TechClass;
		foreach ($attachments as $attachment) {
			$uri = $attachment->relative_uri;
			$uri = explode('/', $uri);
			$id = $uri[2];
			if (!in_array($id, $techUploads)) continue; // skip non tech uploads
			$coreFiles = new Core_Files();
			$processed = $coreFiles->getProcessedWMFileID(array($id));
			if (!empty($processed)) continue;
			$file = self::$wm->assignmentAttachmentGet($id);
			$file = $file->response;
			$fData = base64_decode($file->attachment);
			$techClass->uploadFileAWS( $authData['login'], $authData['password'], $this->win_num, $file->filename, $fData, $file->description, $id);
//			self::setAttachmentProcessed($id, $this->assignment_id); // mark offer as processed
		}
		
	}
	
	public function pullNotes($assignment = NULL) {
		// pull questions from assignment and place in FS Q&A
		$assignment = $this->getAssignment($assignment);
		$resource = $assignment->active_resource;
		$wosClass = new Core_Api_WosClass;
		$authData = Core_Api_TechUser::getTechLoginByWMID($resource->id);
		$notes = $assignment->notes;
		$idList = array();
		foreach ($notes as $note) {
			if ($note->created_by == self::WM_API_NAME) continue; // ignore notes created by api
			$idList[] = $note->date;
		}
		$processed = $this->getNoteProcessed($idList, $assignment->id);
		foreach ($notes as $note) {
			if ($note->created_by == self::WM_API_NAME) continue; // ignore notes created by api
			if (in_array($note->date, $processed)) continue; // skip processed notes
			$wosClass->addTechQuestion($this->win_num, $authData['login'], $resource->first_name . ' ' . $resource->last_name, $authData['TechID'], $note->text);
			$this->setNoteProcessed($note->date, $assignment->id);
		}
	}

	public function pullQuestions($assignment = NULL, $techID = NULL) {
		// pull questions from assignment and place in FS Q&A
		$assignment = $this->getAssignment($assignment);
		$resource = $assignment->active_resource;
		if (!empty($resource->id))
			$authData = Core_Api_TechUser::getTechLoginByWMID($resource->id);
		else {
			$authData = array('login' => '');
			$resource->first_name = '';
			$resource->last_name = '';
			$techID = '';
		}
		$questions = $assignment->questions;
		$wosClass = new Core_Api_WosClass;
		$idList = array();
		foreach ($questions as $question) {
			$idList[] = $question['id'];
		}
		if (empty($idList))
			$processed = array();
		else
			$processed = self::getQuestionProcessed($idList);
		foreach ($questions as $question) {
			$id = $question['id'];
			if (in_array($id, $processed)) continue;
//			$authData = Core_Api_TechUser::getTechLoginByWMID($resource->id);
			$wosClass->addTechQuestion($this->win_num, $authData['login'], $resource->first_name . ' ' . $resource->last_name, $techID, $question['question']);
			self::setQuestionProcessed($id, $assignment->id);
		}
	}
	
	public function pushQuestion($message) {
		// push FS Q&A from work order and place in WM as Note
		if (empty($this->assignment_id))
			{
               
                $assignment_id = $this->getAssignmentIdWithWinNum($this->win_num);
               // print_r("test123_assignment_id");print_r($assignment_id);die();  
            }
		else
			{
                
                $assignment_id = $this->assignment_id;
            }
		if (empty($assignment_id)) return false;

//		if (!empty($question_id))
//			$result = self::$wm->assignmentQuestionAnswer($question_id, $message);
		$result = self::$wm->assignmentNotesAdd($assignment_id, $message);
		return $result;
        
	}
		
	public function readyWorkToRunThroughWM($assignment = NULL) {
		// pulls assignment information into work order, Mark FS wo accepted, add Note to WM with Special Instructions for assigned tech only, attach docs 
		// add special instructions as notes
		$assignment = $this->getAssignment($assignment);
		$wo = $this->getFSWorkOrder(
			array(
				'Ship_Contact_Info', 
				'Status',
				'ProjectManagerName',
				'ProjectManagerPhone',
				'ProjectManagerEmail',
				'ResourceCoordinatorName',
				'ResourceCoordinatorPhone',
				'ResourceCoordinatorEmail',
				'EmergencyName',
				'EmergencyPhone',
				'EmergencyEmail',
				'TechnicalSupportName',
				'TechnicalSupportPhone',
				'TechnicalSupportEmail',
				'CheckInOutName',
				'CheckInOutNumber',
				'CheckInOutEmail',
				'PcntDeduct',
				'PcntDeductPercent'
			)
		);
		if ($wo->Status != 'published') return;

		$techID = $this->pullResourceInfo($assignment);
		// assign work order in FS
		$api = new Core_Api_AdminClass;

		$deduct = empty($wo->PcntDeduct) ? 1.0 : (1.0 - $wo->PcntDeductPercent);
		if (empty($deduct))
			$deduct = 1.0;
		if (!empty($assignment->pricing->flat_price))
			$bidAmount = $assignment->pricing->flat_price / $deduct;
		else if (!empty($assignment->pricing->per_hour_price))
			$bidAmount = $assignment->pricing->per_hour_price / $deduct;
		else if (!empty($assignment->pricing->per_unit_price))
			$bidAmount = $assignment->pricing->per_unit_price / $deduct;

		$apiwo = new API_AdminWorkOrder;
		$apiwo->Tech_ID = $techID;
		$apiwo->Tech_Bid_Amount = $bidAmount;
		$apiwo->WorkOrderReviewed = true;

		$r = $api->updateWorkOrder(self::API_USER, self::API_PASSWORD, $this->win_num, $apiwo, true);
		
		$notes = "Special Instructions: " . strip_tags($wo->Ship_Contact_Info) . "\n\n";
		$notes .= "Contact Information: \n";
		$notes .= "Project Manager: {$wo->ProjectManagerName} {$wo->ProjectManagerPhone} {$wo->ProjectManagerEmail}\n";
		$notes .= "Resource Coordinator: {$wo->ResourceCoordinatorName} {$wo->ResourceCoordinatorPhone} {$wo->ResourceCoordinatorEmail}\n";
		$notes .= "Emergency Contact: {$wo->EmergencyName} {$wo->EmergencyPhone} {$wo->EmergencyEmail}\n";
		$notes .= "Technical Help: {$wo->TechnicalSupportName} {$wo->TechnicalSupportPhone} {$wo->TechnicalSupportEmail}\n";
		$notes .= "Check-in/Check-Out: {$wo->CheckInOutName} {$wo->CheckInOutNumber} {$wo->CheckInOutEmail}\n";
		self::$wm->assignmentNotesAdd($this->assignment_id, $notes);
		
		$this->attachAllFilesToWMAssignment();
	}
	
	protected function saveResourceInfo($active_resource, $checkTechExists = true) {
		// create / update FS Tech
		if (empty($active_resource->id)) return NULL;
		$primaryPhone = empty($active_resource->phone_numbers[0]) ? '' : $active_resource->phone_numbers[0]->phone;
		$secondaryPhone = empty($active_resource->phone_numbers[1]) ? '' : $active_resource->phone_numbers[1]->phone;
		
		$data = array(
			'WMID' => $active_resource->id,
			'FName' => $active_resource->first_name,
			'LName' => $active_resource->last_name,
			'FirstName' => $active_resource->first_name,
			'LastName' => $active_resource->last_name,
			'primaryEmail' => $active_resource->email,
			'PrimaryEmail' => $active_resource->email,
			'PrimaryPhone' => $primaryPhone,
			'SecondaryPhone' => $secondaryPhone,
			'primaryAddress' => $active_resource->address->address_1,
			'primaryAddress2' => $active_resource->address->address_2,
			'Address1' => $active_resource->address->address_1,
			'Address2' => $active_resource->address->address_2,
			'city' => $active_resource->address->city,
			'state' => $active_resource->address->state,
			'zip' => $active_resource->address->zip,
			'City' => $active_resource->address->city,
			'State' => $active_resource->address->state,
			'Zip' => $active_resource->address->zip,
			'country' => 'US',
			'Country' => 'US'
		);
		if ($checkTechExists) {
			$techID = Core_Tech::getTechIDWithWMID($data['WMID'], $data['primaryEmail']);
			if (!empty($techID)) {
				$data['techID'] = $techID['TechID'];
				$data['WMID'] = $techID['WMID'];
			}
		}
		if (empty($techID)) {
			$myID = Core_Tech::registerWMTech($data);
			$techID = array('TechID' => $myID, 'WMID' => $data['WMID']);
		}
		else {
			Core_Tech::updateTechInfo($data);
		}
		return $techID;
	}
	
	public function attachAllFilesToWMAssignment() {
		$files = new Core_Files();
		$projectFiles = $files->getFileIDsByWinsAndType($this->win_num, Core_Files::WOS_PROJECT_FILE);
		$woFiles = $files->getFileIDsByWinsAndType($this->win_num, Core_Files::WOS_FILE);
		$signoffFiles = $files->getFileIDsByWinsAndType($this->win_num, Core_Files::SIGNOFF_FILE);
		
		foreach ($projectFiles as $file) {
			if (!empty($file['WMFileID'])) continue;
			$getter = new Core_Api_ProjectClass();
			$data = $getter->getFile($file['path']);
			self::attachFileToWMAssignment($this->assignment_id, basename($file['path']), $data, $file['description']);
		}
		foreach ($woFiles as $file) {
			if (!empty($file['WMFileID'])) continue;
			$data = API_WorkOrder::getFile($file['path']);
			self::attachFileToWMAssignment($this->assignment_id, basename($file['path']), $data, $file['description']);
		}
		foreach ($signoffFiles as $file) {
			if (!empty($file['WMFileID'])) continue;
			$data = API_WorkOrder::getFile($file['path']);
			self::attachFileToWMAssignment($this->assignment_id, basename($file['path']), $data, $file['description']);
		}
	}
	
	public static function attachFileToWMAssignment($id, $filename, $data, $description) {
		self::connectWM();
		$result = self::$wm->assignmentAttachmentAdd($id, $filename, $data, $description);
		return $result->response->uuid;
	}
	
	public static function removeFileFromWMAssignment($uuid) {
		self::connectWM();
		$result = self::$wm->assignmentAttachmentRemove($uuid);
		return $result->response->successful;
	}
	
	public function stopWMAssignment($allowCancel = false, $pay = 0.00, $reason = '') {
		// Stops FS-Rush and voids WM assignment. Pulls in assignment info if already assigned in WM. Returns false if work order already assigned in WM, or true if assignment in WM was voided
		/*
			- When WO assigned in FS, check WM
				- If no VOID in WM
				- If yes shadow box already assigned or kill auto assign	
		*/
		// check if assignment is assigned in WM. Returns true when WM has not assigned assignment, false we WM has assigned assignment. Only assign in FS when return is true
		$result = $this->voidAssignment($allowCancel, $pay, $reason);
		// cancel autoassign
		
		if (!$result)
			// assignment assigned in WM. Pull in assignment info from WM
			$this->readyWorkToRunThroughWMBG();
			
		Core_AutoAssign::clearTimer($this->win_num);
		
		return $result; // WM assignment was voided
	}
	
	public function voidAssignment($allowCancel = false, $pay = 0.00, $reason = '') {
		// voids assignment in WM
		if (empty($this->assignment_id)) return true; // no assignment id so there's no assignment in WM
		$assignment = $this->getAssignment();
		if ($assignment->status != 'draft' && $assignment->status != 'sent' && $assignment->status != 'void') {
			if (!$allowCancel)
				return false; // assignment is not sent or voided which means it was assigned
			else {
				$result = self::$wm->assignmentCancel($this->assignment_id, $pay, $reason);
				if (!$result->response->successful) return false;
				$this->setAssignmentId(NULL);
				return true;
			}
		}
		if ($assignment->status != 'void') {
			// try to void
			$result = self::$wm->assignmentVoid($this->assignment_id);
			if (!$result->response->successful) return false; // unable to void, assume it's assigned
		}
		$this->setAssignmentId(NULL);
		return true;
	}
	
	public function hasOffers() {
		if (empty($this->assignment_id)) return false;
		$assignment = $this->getAssignment();
		return !empty($assignment->pending_offers);
	}

	public function hasOffersWithCounters() {
		if (empty($this->assignment_id)) return false;
		$assignment = $this->getAssignment();
		$result = false;
		foreach ($assignment->pending_offers as $offer) {
			if (!empty($offer->scheduling) || !empty($offer->pricing )) {
				$result = true;
				break;
			}
		}
		return $result;
	}

	public function rejectForPayment($missingComments = NULL) {
		if (empty($missingComments)) {
			$fswo = $this->getFSWorkOrder(array(
				'MissingComments'
			));
			$missingComments = $fswo->MissingComments;
		}
		self::$wm->assignmentRejectForPayment($this->assignment_id, $missingComments);
	}

	public function approveForPayment() {
		self::$wm->assignmentApproveForPayment($this->assignment_id);
	}

	public function deactivateAssignment($pay = 0.00, $reason = '') {
		if (empty($missingComments)) {
			$fswo = $this->getFSWorkOrder(array(
				'Deactivated_Reason',
				'Net_Pay_Amount'
			));
			$pay = $fswo['Net_Pay_Amount'];
			$reason = $fswo->Deactivated_Reason;
		}
		self::$wm->assignmentCancel($this->assignment_id, $pay, $reason);
	}
	
	protected function getFSWorkOrder($fields = NULL) {
		$api = new Core_Api_AdminClass;
		$fields = implode(",", $fields);
		$filters = new API_AdminWorkOrderFilter();
		$filters->TB_UNID = $this->win_num;
		$wo = $api->getWorkOrders(self::API_USER, self::API_PASSWORD, $fields, NULL, NULL, NULL, $filters);
		$wo = $wo->data[0];
		
		return $wo;
	}
	
	protected function getWMGroupIDMapping() {
		if (!empty($this->groupIDMap)) return $this->groupIDMap;
		$db = Core_Database::getInstance();
		$select = $db->select();
		$select->from(Core_Database::TABLE_WO_CATEGORIES, array('Category_ID', 'WMGroupID'));
		$this->groupIDMap = $db->fetchPairs($select);		
	}
	
	protected function getWMGroupIDWithFSSkillCat($id) {
		$this->getWMGroupIDMapping();
		return $this->groupIDMap[$id];
//		return 1973;
	}

	protected function getAssignment($assignment = NULL) {
		if (empty($assignment))
			$assignment = self::$wm->assignmentGet($this->assignment_id);
		if (isset($assignment->response))
			$assignment = $assignment->response;
		return $assignment;
	}
	
	protected function getAssignmentLocalTime($assignment = NULL, $unixTs, $format = NULL) {
		$assignment = $this->getAssignment($assignment);
		try {
			$dateTime = new Zend_Date($unixTs, Zend_Date::TIMESTAMP);
			$dateTime->setTimeZone($assignment->time_zone);
			if (!empty($format))
				$dateTime = $dateTime->toString($format);
		}
		catch (Exception $e) {
			if (!empty($format))
				$dateTime = '';
			else
				$dateTime = new Zend_Date($unixTs, Zend_Date::TIMESTAMP);
		}
		return $dateTime;
	}
}
