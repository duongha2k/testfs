<?php

class Core_EmailForCompleteByTech
{
    private $wo;
    private $project;
    private $mail;

    function __construct()
    {
        $this->mail = new Core_Mail();
    }
        
    public function setWorkOrder($workOrder)
    {
        if (is_object($workOrder)) {
        	$this->wo = $workOrder;
        } else {
//            $this->wo = new Core_WorkOrder((int)$workOrder);
        }
    }

    public function setProject($project)
    {
		$this->project = $project;
    }

            
    public function sendMail()
    {
		$wo = $this->wo;
		$prj_id = $wo->Project_ID ? $wo->Project_ID : '';
		$project = $this->project;

		$toEmail = $project->WorkDoneNotificationEmails;
    $workDoneEmailTo = trim($wo->WorkDoneEmailTo);
    if($workDoneEmailTo != "" && $workDoneEmailTo != NULL){
         $toEmail = $workDoneEmailTo;
    }
    if($project->NotifyOnWorkDone=='1'){
          $projectNotifyOnWorkDone = true;
    }else{
          $projectNotifyOnWorkDone = false;
    }
		//if (!empty($project->Project_ID) && $wo->Company_ID=='NCR' && ($projectNotifyOnWorkDone==true || $workDoneEmailTo!=''))
    if (!empty($project->Project_ID)  && ($projectNotifyOnWorkDone==true || $workDoneEmailTo!=''))
    {
			$din = $wo->Date_In;
			$dout = $wo->Date_Out;
			try {
				$dateIn = new Zend_Date($din, Zend_Date::ISO_8601);
				$dateOut = new Zend_Date($dout, Zend_Date::ISO_8601);
				$din = $dateIn->toString('MM/dd/yyyy');
				$dout = $dateIn->toString('MM/dd/yyyy');
				$din .= " " .$wo->Time_In;
				$dout .= " " .$wo->Time_Out;
			}
			catch (Exception $e) {
				$din = $wo->Date_In." ".$wo->Time_In;
				$dout = $wo->Date_Out." ".$wo->Time_Out;
			}
      
       //14054
      $vsList = '';
      $i = 1;
      $hours = 0.0; 
      $ttHours = '0 hour 0 min';
      
      $apiWosClass = new Core_Api_WosClass();  
      $vsLists = $apiWosClass->GetAllWOVisitList($wo->WIN_NUM);
      if(!empty($vsLists)){
        foreach($vsLists as $item){
             if($item['OptionCheckInOut']==1){
                  //visits Tech
                 $vsStartDate = $item['TechCheckInDate'];
                 $vsStartDate = new Zend_Date($vsStartDate, Zend_Date::ISO_8601); 
                 $vsStartDate = $vsStartDate->toString('MM/dd/yyyy');    
                 $vsStartDate = $vsStartDate.' '.$item['TechCheckInTime']; 
                 
                 $vsEndDate = $item['TechCheckOutDate'];
                 $vsEndDate = new Zend_Date($vsEndDate, Zend_Date::ISO_8601);  
                 $vsEndDate = $vsEndDate->toString('MM/dd/yyyy');    
                 $vsEndDate = $vsEndDate.' '.$item['TechCheckOutTime']; 
                 
                 $vsTotal = $item['DisplayTechTotalHours'];
                 
                 $hours = $hours + $item['TechTotalHours']; 
                 
             }else{
                  //visits Client
                 $vsStartDate = $item['CORCheckInDate'];
                 $vsStartDate = new Zend_Date($vsStartDate, Zend_Date::ISO_8601); 
                 $vsStartDate = $vsStartDate->toString('MM/dd/yyyy');    
                 $vsStartDate = $vsStartDate.' '.$item['CORCheckInTime']; 
                 
                 $vsEndDate = $item['CORCheckOutDate'];
                 $vsEndDate = new Zend_Date($vsEndDate, Zend_Date::ISO_8601);  
                 $vsEndDate = $vsEndDate->toString('MM/dd/yyyy');    
                 $vsEndDate = $vsEndDate.' '.$item['CORCheckOutTime']; 
                 
                 $vsTotal = $item['DisplayCORTotalHours'];
                 
                 $hours = $hours + $item['CORTotalHours']; 
             }
             $vsList .='
                        Visit #'.$i.'
                        Start Date/Time: '.$vsStartDate.'
                        End Date/Time: '.$vsEndDate.'
                        Calculated Hours: '.$vsTotal;
             $i = $i +1;
        }
         $ttHours = $apiWosClass->getDurationDesc($hours,true); 
      }
      
			$mail = new Core_Mail();
			$message = "FS-Tech ID# ".$wo->Tech_ID." ".$wo->Tech_FName." ".$wo->Tech_LName." has marked a Work Order as Work Done:

			WIN#: ".$wo->WIN_NUM."
			Client WO ID#: ".$wo->WO_ID."
			Project: ".$wo->Project_Name."
			Headline: ".$wo->Headline."
			Region: ".$wo->Region."
			Route: ".$wo->Route."
			Site: ".$wo->SiteName."
			".$wo->City.", ".$wo->State." ".$wo->Zipcode."
			
      $vsList
      
      Calculated Total Hours: ".$ttHours."
      Tech Closing Comments: ".$wo->TechComments."

			Please approve this Work Order for payment when you are satisfied that it is complete.
      
			Thank you,
			Your FieldSolutions Team
			";
       //End 14054 
       
			//overwrite project work done email with future table info
			/*
			$future = new Core_FutureWorkOrderInfo();
			$futureData = $future->find($project->CreatedBy);
			if(count($futureData) > 0){
				$row = $futureData->toArray();
				if(!empty($row[0]['WorkDoneEmailTo'])) $toEmail = $row[0]['WorkDoneEmailTo'];
			}
			*/ 
			$mail->setBodyText($message);
			$mail->setFromName("FieldSolutions");
			$mail->setFromEmail('nobody@fieldsolutions.com');
			$mail->setToEmail($toEmail);
			$mail->setSubject("WIN# ".$wo->WIN_NUM.": Work Order marked as Complete by Technician");
			$mail->send();
		}		
    }
}
