<?php
class Core_EmailToWorkOrder {
	private $mail;
	public function __construct() {
		$dbInfo = Zend_Registry::get("DB_Info");
		$isTesting = $dbInfo["dbname"] !== "gbailey_technicianbureau";
		$this->mail = new Zend_Mail_Storage_Imap(array(
			'host' => 'imap.emailsrvr.com',
			'user' => $isTesting ? 'wo.test_gateway@fieldsolutions.com' : 'wo_gateway@fieldsolutions.com',
			'password' => $isTesting ? '6BE2BCsE' : '82bmVBXA',
			'ssl' => 'TLS')
		);
	}
	
	protected function getLastRead() {
		$db = Core_Database::getInstance();
		$select = $db->select();
		$select->from("wo_gateway_read", array("msg_id"))->order("id DESC")->limit(1);
		return $db->fetchOne($select);
	}
	
	protected function setLastRead($msg_id) {
		$db = Core_Database::getInstance();
		$db->insert("wo_gateway_read", array("msg_id" => $msg_id, "read_date" => new Zend_Db_Expr("NOW()")));
	}

	public function process() {
		$mId = $this->getLastRead();
		if (empty($mId)) $mId = 1;
		$count = $this->mail->countMessages();
//		$mId = 97;
//		$count = 97;
		for ($mId = $mId; $mId <= $count; ++$mId) {
			$message = $this->mail->getMessage($mId);
//			echo "Mail from '{$message->from}': {$message->subject} {$message->contentType}\n";
			$this->setLastRead($mId);
			if ($message->hasFlag(Zend_Mail_Storage::FLAG_SEEN)) continue;
//			echo $message->getContent() . "\n";
			$this->mail->setFlags($mId, array(Zend_Mail_Storage::FLAG_SEEN));
			$form = strstr($message->subject, 'form:');
			if (empty($form)) $form = "";
			$form = trim(str_replace("form:", "", $form));
			if (!empty($form)) {
				try {
					// try to match form
					$form = strtolower($form);
					$className = "Core_EmailToWorkOrder_Processor_" . $form;
					@$ex = class_exists($className);
					$processor = new $className($message);
				}
				catch (Exception $e) {
					$processor = new Core_EmailToWorkOrder_Processor($message);
				}
			}
			else
				$processor = new Core_EmailToWorkOrder_Processor($message);
			$result = $processor->run();
		}
	}
}
