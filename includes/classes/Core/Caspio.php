<?php

class Core_Caspio
{
   const OBJECT_FAULT = "{Object reference not set to an instance of an object.}";
    /**
     * API access to caspio
     */

    private static $wsdl;
    private static $name;
    private static $profile;
    private static $password;
    private static $soapClient;

    private static $config;
    private static $initialized = false;

    private static $testMode = false;
    private static $testAllowedTechs = '';

    /**
     * Init Caspio API
     */
    public static function init()
    {
    	if (!self::$initialized) {
/*	        if ( defined( 'CACHE_USE_MEMCACHED' ) ) {
	            $cache = Core_Cache_Manager::factory();

	            $capioCfg = $cache->load("cfg_caspio_xml");
	            if (empty($caspioCfg)) {
	                $capioCfg = new Zend_Config_Xml(DOCUMENT_CONFIG . '/cfg.caspio.xml', 'caspio');
	                $cache->save($capioCfg, 'cfg_caspio_xml', array(), '3600');
	            }
	        } else {
	            $capioCfg = new Zend_Config_Xml(DOCUMENT_CONFIG . '/cfg.caspio.xml', 'caspio');
	        }

		    self::$wsdl     = $capioCfg->get('wsdl');
	        self::$name     = $capioCfg->get('name');
	        self::$profile  = $capioCfg->get('profile');
	        self::$password = $capioCfg->get('password');
	        try {
        		self::$soapClient = new Zend_Soap_Client(self::$wsdl, array('soap_version' => '1'));
	        	if ($capioCfg->logRequests) {
	        		self::$soapClient = new Core_Caspio_Log(self::$soapClient, $capioCfg->logFile);
	        	}
	        } catch (Zend_Soap_Client_Exception $e) {
	        	$errors = Core_Api_Error::getInstance();
	        	$errors->addError(1, $e->getMessage());
	        }

	        if (isset($capioCfg->testMode) && $capioCfg->testMode) {
	        	self::$testMode = true;
	        	self::$testAllowedTechs = trim($capioCfg->testAllowedTechs);
	        }*/

    		self::$initialized = true;
    	}
    }

    public static function caspioSelectAdv($tableName, $fieldList, $criteria, $sortBy, $assoc = TRUE, $raw = FALSE)
    {
    	//self::checkAllowedTable($tableName);
		$tableName = self::getMysqlTableFromCaspio($tableName);
		if (empty($tableName)) return false;
		$design = $res = Core_Database::getFieldListType($tableName);
		$db = Core_Database::getInstance();
		$criteria = !empty($criteria) ? "WHERE $criteria" : "";
		$sortBy = !empty($sortBy) ? "ORDER BY $sortBy" : "";
		if (is_array($fieldList)) {
			$fieldList = implode(",", $fieldList);
		}
		$fieldList = str_replace(array('ISNULL', 'GETDATE()'), array('IFNULL', 'NOW()'), $fieldList);
		self::notify("SELECT $fieldList FROM " . $tableName . " $criteria $sortBy");
		try {
			$res = $db->query("SELECT $fieldList FROM " . $tableName . " $criteria $sortBy");
			$res = $res->fetchAll();
		} catch (Exception $e) {
			self::notify("SELECT $fieldList FROM " . $tableName . " $criteria $sortBy" . print_r($e, true));
			$res = null;
		}
		$result = array();
		if ($res && !empty($res)) {
			foreach ($res as $i=>$row) {
				foreach ($row as $col=>&$val) {
					if ($val == NULL) 
						$val = 'NULL';
					else {
						switch (@$design[$col]) {
							case 'varchar':
							case 'text':
								$val = "`$val`";
								break;
							case 'tinyint':
								$val = $val == 1 ? "True" : "False";
								break;
							case 'datetime':
								if ($val == "0000-00-00 00:00:00") $val = 'NULL';
								else {
									try {
										$ut = new Zend_Date($val, Zend_Date::ISO_8601);
										if (!empty($ut) && $val != "0000-00-00 00:00:00")
											$val = $ut->toString("M/dd/YYYY hh:m:s a");
									} catch (Exception $e) {
									}

								}
								break;
						}
					}
				}
				$result[$i] = implode("#|^", $row);
			}
			if ($raw) return $result;
				return $assoc == TRUE ? self::parseCaspioResponse($result, $fieldList) : self::parseCaspioResponseIndex($result);
		}
		return $result;
    }

    public static function caspioSelectWithFieldListMap($tableName, $fieldListMap, $criteria, $sortBy)
    {
    	self::checkAllowedTable($tableName);
		if (empty($fieldListMap)) return null;
		$fieldList = array_values($fieldListMap);
		$resul = self::caspioSelectAdv($tableName, $fieldList, $criteria, $sortBy, false, true);
		if (!empty($resul[0])) {
			return self::parseCaspioResponseFieldListMap($resul, $fieldListMap);
		}
        return null;
    }

    /**
     * caspioDownloadFile
     *
     * @param mixed $fileName
     * @static
     * @access public
     * @return void
     */
    public static function caspioDownloadFile( $fileName)
    {
        try {
            return self::$soapClient->DownloadFileByName(self::$name, self::$profile, self::$password, $fileName);
        } catch ( SoapFault $e ) {
            $errors = Core_Api_Error::getInstance();
            $exceptionConverter = new Core_Caspio_ExceptionConverter($e->faultstring);
            $errors->addError(1, $exceptionConverter->process());
        }
        return null;
    }
    /**
     * caspioUploadFile
     *
     * @param string $fname
     * @param string $fdata
     * @param string $mime
     * @param string|int $folderId  Folder full path or -1 for root folder
     * @param boolean $overwrite Does need overwrite exists file
     * @static
     * @access public
     * @return void
     */
    static public function caspioUploadFile( $fname, $fdata, $mime, $folderId = -1, $overwrite = false ) {
        try {
            self::$soapClient->UploadFile(self::$name, self::$profile, self::$password, $fname, $fdata, $mime, $folderId, $overwrite);
        } catch ( SoapFault $e ) {
            $errors = Core_Api_Error::getInstance();
            $exceptionConverter = new Core_Caspio_ExceptionConverter($e->faultstring);
            $errors->addError(1, $exceptionConverter->process());
        }
        return null;
    }
    /**
     * caspioListFiles
     *
     * @param int $folderID
     * @param bool $subfolders
     * @static
     * @access public
     * @return false|string XML
     */
    static public function caspioListFiles( $folderID = -1, $subfolders = false )
    {
        try {
            return self::$soapClient->ListFiles(self::$name, self::$profile, self::$password, $folderID, $subfolders);
        } catch ( SoapFault $e ) {
            $errors = Core_Api_Error::getInstance();
            $exceptionConverter = new Core_Caspio_ExceptionConverter($e->faultstring);
            $errors->addError(1, $exceptionConverter->process());
        }
        return false;
    }
    /**
     * caspioDeleteFile
     *
     * @param int $fileID
     * @static
     * @access public
     * @return void
     */
    static public function caspioDeleteFile( $fileID )
    {
        if ( !is_int($fileID) || $fileID < 1 ) {
            $errors = Core_Api_Error::getInstance();
            $errors->addError(6, 'FileID should be an unsigned integer');
            return null;
        }

        try {
            self::$soapClient->DeleteFile(self::$name, self::$profile, self::$password, $fileID);
        } catch ( SoapFault $e ) {
            $errors = Core_Api_Error::getInstance();
            $exceptionConverter = new Core_Caspio_ExceptionConverter($e->faultstring);
            $errors->addError(1, $exceptionConverter->process());
        }
        return null;
    }

    public static function caspioInsert($tableName, $fieldList, $valueList)
    {
    	self::checkAllowedTable($tableName);
		if (empty($fieldList) || empty($valueList)) return false;
		$tableName = self::getMysqlTableFromCaspio($tableName);
		if (empty($tableName)) return false;
		if (!is_array($valueList)) {
			$valueList = explode(",", $valueList);
		}
		if (!is_array($fieldList)) {
			$fieldList = explode(",", $fieldList);
		}
		foreach ($fieldList as $key => &$val) {
			$val = trim($val);
		}
		
		foreach ($valueList as $key => &$val) {
			if ($val === NULL || $val === "") {
				$val = new Zend_Db_Expr("DEFAULT");
			} elseif ($val == 'GETDATE()'){
				$val = new Zend_Db_Expr("NOW()");
			} else {
				$val = stripslashes(trim($val, "'"));
			}
		}
		
		$insert = array_combine($fieldList, $valueList);
		$db = Core_Database::getInstance();
		self::notify("INSERT INTO ". $tableName . print_r($insert, true));
		Core_Database::insert($tableName, $insert);
		return $db->lastInsertId($tableName);
    }

    public static function caspioUpdate($tableName, $fieldList, $valueList, $criteria, $skipSync = FALSE)  {
    	self::checkAllowedTable($tableName);

		$tableName = self::getMysqlTableFromCaspio($tableName);
		if (!is_array($valueList)) {
			$valueList = explode(",", $valueList);
		}
//		$autoformat = new Core_Data_Autoformat($valueList);
//		$valueList = $autoformat->process();
		foreach ($valueList as $key => &$val) {
			if ($val === NULL || $val === "") {
				$val = new Zend_Db_Expr("DEFAULT");
			} elseif ($val == 'GETDATE()'){
				$val = new Zend_Db_Expr("NOW()");
			} else {
				$val = stripslashes(trim($val, "'"));
			}
		}
		$update = array_combine($fieldList, $valueList);
		
		self::notify("UPDATE ". $tableName . print_r($update, true) . " " . $criteria);
		$result = Core_Database::update($tableName, $update, $criteria);

		return $result;
    }

    public static function caspioDelete($tableName, $criteria = NULL)  {
    	self::checkAllowedTable($tableName);
		$tableName = self::getMysqlTableFromCaspio($tableName);
		if (empty($tableName)) return false;
    	if ($criteria === NULL) return false;
		$db = Core_Database::getInstance();
		return $db->delete($tableName, $criteria);
    }

    public static function caspioProximitySearchByCoordinatesRaw(
        $tableName,                 //  ObjectName
        $isView,                    //  IsView
        $latitude,                  //  Latitude
        $longitude,                 //  Longitude
        $refLatitudeField,          //  LatitudeField
        $refLongitudeField,         //  LongitudeField
        $proximityCriteria,         //  ProximityCriteria
        $units,                     //  Units
        $fieldList,                 //  FieldList
        $additionalCriteria,        //  AdditionalCriteria
        $orderBy,                   //  OrderBy
        $includeCalculatedDistance, //  IncludeCalculatedDistance
        $fieldDelimiter = "|",
        $stringDelimiter = "`",
        $catchErrors = TRUE
    ) {
    	self::checkAllowedTable($tableName);
		$tableName = self::getMysqlTableFromCaspio($tableName);
		$db = Core_Database::getInstance();
		$select = $db->select();
		if (is_string($fieldList)) {
			$fieldList = explode(",", $fieldList);
		}
		$select->from($tableName, $fieldList);
		if (!empty($orderBy)) {
			$sortBy = explode(",", $orderBy);
			foreach ($sortBy as $sort) {
				$sort = str_replace("calculated_distance", "Distance", $sort);
				$select->order(trim($sort));
			}
		}
		$proximityCriteria = str_replace(array("<", "=", " "), array("", "", ""), $proximityCriteria);
		$select = Core_Database::whereProximity($select, $latitude, $longitude, $refLatitudeField, $refLongitudeField, $proximityCriteria, "Distance");

		if (!empty($additionalCriteria)) {
			if (stripos(ltrim($additionalCriteria), 'and ') === 0) $additionalCriteria = 'TechID = TechID ' . $additionalCriteria; // workaround for leading and
			$select->where($additionalCriteria);
		}
		$result = $db->fetchAll($select);
            if ( $includeCalculatedDistance ) {
                if(is_array($fieldList))
                    $fieldList['Distance'] = 'Distance';
                else
                    $fieldList .= ',Distance';
            }

			return $result;

            /*if (!empty($result[0])) {
                if(is_array($fieldList))
                    return self::parseCaspioResponseFieldListMap($result, $fieldList);
                return self::parseCaspioResponse($result, $fieldList);
            } else {
                return null;
            }
        return null;*/
    }

    public static function caspioGetTableDesign($tableName)
    {
    	self::checkAllowedTable($tableName);
		
		$res = Core_Database::getFieldListType($tableName);
		
		$tres = array();
		foreach ($res as $name=>$type) {
			switch ($type) {
				CASE 'text':
					$type = 'LongText';
					break;
				CASE 'varchar':
					$type = 'ShortText';
					break;
				CASE 'datetime':
					$type = 'Date/Time';
					break;					
				CASE 'decimal':
				CASE 'int':
					$type = 'Number';
					break;
				CASE 'tinyint':
					$type = 'Yes/No';
					break;
				default:
					break;
			}
			$tres[] = "$name,$type,False,";
		}
		
		return $res;
		
    }

    public static function parseCaspioResponseFieldListMap(&$response, $fieldListMap)
    {
	$result = array();
	$fieldListMap = array_keys($fieldListMap);
	foreach ($response as $key=>&$record) {
		$record = explode("#|^", $record);
		$resRec = array();
		foreach ($record as $k2 => $field) {
			if (!array_key_exists($k2, $fieldListMap)) continue;
			$buf = trim($field, "`'");
			if ($buf == "NULL")
				$buf = null;
			$resRec[$fieldListMap[$k2]] = $buf;

		}
		$record = null;
		$result[] = $resRec;
	}
	return $result;
    }

    private static function parseCaspioResponseIndex($response)
    {
	$result = array();
	foreach ($response as $key=>$record) {
		$record = explode("#|^", $record);
		foreach ($record as $k2=>$field) {
			$record[$k2] = trim($field, "`'");
		}
		$result[] = $record;
	}
	return $result;
    }

    private static function parseCaspioResponse($response, $fieldList)
    {
        $result = array();
        if (empty($fieldList) || $fieldList == '*') {
            return null;
        }

        $fieldList = explode(",", $fieldList);
        foreach ($response as $key=>$record) {
            $record = explode("#|^", $record);
            foreach ($fieldList as $k=>$field) {
                $field = trim($field, "`'");
                $buf = @trim($record[$k], "`'");
                if ($buf == "NULL")
                    $buf = null;
                $result[trim($key)][trim($field)] =    $buf;
            }
        }
        return $result;
    }

    public static function caspioEscape($str)
    {
		$db = Core_Database::getInstance();
		$str = $db->quoteInto("?", $str);
		return trim($str, "'");
    }

    public static function getTableFieldsList($tableName)
    {
    	self::checkAllowedTable($tableName);
    	$columns = self::caspioGetTableDesign($tableName);
        if (empty($columns[0]))
            return false;

        $fieldList = array();

        foreach ($columns as $info) {
            $fieldInfo = explode(",",$info);
            $fieldList[] = trim($fieldInfo[0]);
        }

        $fieldList = implode(',', $fieldList);

        return $fieldList;
    }

	/**
	 * In test mode function modify update criteria
	 * @param string $tableName teble name
	 * @param string $criteria criteria
	 * @return string return result criteria

	 */
    private static function modifyTestCriteria($tableName, $criteria)
    {
    	if (!self::$testMode) return $criteria;
    	if ($tableName == 'TR_Master_List') {
    		if (!self::$testAllowedTechs) return $criteria . ' AND 1=0'; //List of allowed techs is empty, prevent other techs from update
    		return $criteria .= ' AND TechID IN (' . self::$testAllowedTechs . ')';
    	}
    	return $criteria;
    }

    /**
     * In test mode function checks if table is allowed for using. If table isn't allowed then throw exception.
     * @param string $tableName table name
     * @throws Core_Exception
     */
    public static function checkAllowedTable($tableName)
    {
    	//if (!self::$testMode) return;
    	if ($tableName == "Work_Orders") throw new Core_Exception("Trying to use Caspio Work_Orders table");
    }
	
	private static function getMysqlTableFromCaspio($table) {
		switch ($table) {
			case TABLE_CLIENT_LIST:
				return Core_Database::TABLE_CLIENTS;
				break;
			case TABLE_DEACTIVATION_CODES:
				return Core_Database::TABLE_DEACTIVATION_CODES;
				break;		
			case TABLE_WO_CATEGORIES:
				return Core_Database::TABLE_WO_CATEGORIES;
				break;		
			case TABLE_MASTER_LIST:
				return Core_Database::TABLE_TECH_BANK_INFO;
				break;		
			case TABLE_STATES_LIST:
				return Core_Database::TABLE_STATES_LIST;
				break;		
			case TABLE_SHIPPING_CARRIERS:
				return Core_Database::TABLE_SHIPPING_CARRIERS;
				break;		
			case ISO:
				return Core_Database::TABLE_ISO;
				break;		
		}
		self::notify("unknown table $table");
		return "";
	}
	private static function notify($msg) {
			$stack = debug_backtrace();
			$msg .=  "stack --- " . print_r($stack, true);
			$contactMail = new Core_Mail();
			$contactMail->setFromName("FieldSolutions");
			$contactMail->setFromEmail("no-replies@fieldsolutions.com");
			$contactMail->setToEmail("tngo@fieldsolutions.com");
			$contactMail->setSubject("Caspio Call");
			$contactMail->setBodyText($msg);

//			$contactMail->send();
	}
}
