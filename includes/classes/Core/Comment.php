<?php
class Core_Comment extends Zend_Db_Table_Row_Abstract {
	private $username;

	public function _insert() {
		$this->_save();
	}

	public function _save() {
		if (empty($this->id))
			$this->date = new Zend_Db_Expr('NOW()');
		$this->dateChanged = new Zend_Db_Expr('NOW()');
		
		if (!empty($this->id)) {
			$h = new Core_CommentsHistory;
			$h->log($this->id, Core_CommentsHistory::ACTION_EDIT, $this->username);
		}
		
//		parent::save();
	}

	public function _delete() {
		$h = new Core_CommentsHistory;
		$h->log($this->id, Core_CommentsHistory::ACTION_DELETE, $this->username);
//		parent::delete();
//		var_dump($this->id);die();
	}

	public function getCountHelpful() {
		return null;
//		return $this['cntHelpful'];
	}

	public function getCountHelpfulMe() {
		return null;
//		return $this['cntHelpfulMe'];
	}

	public function getCountHelpfulMeTotal() {
		return null;
//		return $this['cntHelpfulMeTotal'];
	}

	public function getCountHelpfulTotal() {
		return null;
//		return $this['cntHelpfulTotal'];
	}

	public function getSkillCategory() {
//		return null;
		return $this['skillCategory'];
	}

	public function getHeadline() {
		return $this['headline'];
	}

	public function getJobRating() {
		return $this['jobRating'];
	}

	public function setUsername($username) {
		$this->username = $username;
	}
}
