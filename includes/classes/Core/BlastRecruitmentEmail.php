<?php

class Core_BlastRecruitmentEmail
{

    const RECRUITMENT_BLAST_FROM_EMAIL_DEFAULT = "no-replies@fieldsolutions.us";    

    private $wo;
    private $companyId;
    private $mail;
    private $comments;
    private $distance = '50';
    private $copySelf = false;
    private $alternateFromEmail;
    private $user;
	private $importId;

    function __construct()
    {
        $this->mail = new Core_Mail();
    }
    
    public function setDistance($value)
    {
        $this->distance = $value;
    }
    public function setComments($value)
    {
        $this->comments = $value;
    }
    public function setCopySelf($value)
    {
        $this->copySelf = $value;
    }
    public function setCompanyId($companyId)
    {
        $this->companyId = $companyId;
    }
    public function setAlternateFromEmail($alternateFromEmail)
    {
        $this->alternateFromEmail = $alternateFromEmail;
    }
        
    public function setWorkOrder($workOrder)
    {
        if (is_object($workOrder)) {
        	$this->wo = $workOrder;
        } else {
            $this->wo = new Core_WorkOrder((int)$workOrder);
        }
    }

    public function setUser($user) {
	$this->user = $user;
    }
    
    public function setImportId($id) {
		$this->importId = $id;
		if (empty($this->importId)) $this->importId = "";
    }

	public function sendMail()
    {
 
		if (defined('IMPORTID') && empty($this->importId)) $this->importId = IMPORTID;
		$this->blastRecruitmentEmailMainSiteBG(array($this->wo->getWIN_NUM()), $this->companyId, $this->comments, $this->distance, $this->copySelf, false, false, $this->alternateFromEmail, $this->user, $this->importId);
		return;
	}
			
	private function blastRecruitmentEmailMainSiteBG($woList, $companyID, $comments = "", $distance = "50", $copySelf = FALSE, $manualBlast = FALSE, $returnBadZip = FALSE, $alternateFromEmail = "", $user = "", $importId = "") {
		$woList = array("idList" => $woList);
		$woList = serialize($woList);
		$copySelf = serialize($copySelf);
		$manualBlast = serialize($manualBlast);
		$returnBadZip = serialize($returnBadZip);
		//error_log("(/usr/bin/php -f  " . WEBSITE_LIB_PATH . "/blastRecruitmentEmailMainSiteBGWithCriteria.php " . escapeshellarg($woList) . " " . escapeshellarg($companyID) . " " . escapeshellarg($comments) . " " . escapeshellarg($distance) . " " . escapeshellarg($copySelf) . " " . escapeshellarg($manualBlast) . " " . escapeshellarg($returnBadZip) . " " . escapeshellarg($alternateFromEmail) . " " . escapeshellarg($user) . ") > /dev/null 2>&1 &");
		
		exec("(/usr/bin/php -f  " . WEBSITE_LIB_PATH . "/blastRecruitmentEmailMainSiteBGWithCriteria.php " . escapeshellarg($woList) . " " . escapeshellarg($companyID) . " " . escapeshellarg($comments) . " " . escapeshellarg($distance) . " " . escapeshellarg($copySelf) . " " . escapeshellarg($manualBlast) . " " . escapeshellarg($returnBadZip) . " " . escapeshellarg($alternateFromEmail) . " " . escapeshellarg($user) . " " . escapeshellarg($importId) . ") > /dev/null 2>&1 &");
	}
}
