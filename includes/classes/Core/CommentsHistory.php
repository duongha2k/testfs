<?php
class Core_CommentsHistory extends Zend_Db_Table_Abstract {
	const ACTION_EDIT = 0;
	const ACTION_DELETE = 1;
	protected $_name = 'comments_history';
	
	public function log($id, $action, $username) {
		$c = new Core_Comments;
		$oldComment = $c->getComment($id);
		$originalComment = empty($oldComment) ? "" : $oldComment->comment;
		$log = $this->createRow();
		$log->action = $action;
		$log->originalComment = $originalComment;
		$log->date = new Zend_Db_Expr('NOW()');
		$log->comment_id = $id;
		$log->username = $username;
		$log->save();
	}
}
