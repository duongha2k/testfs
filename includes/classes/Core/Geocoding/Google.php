<?php
class Core_Geocoding_Google
{
    private $mapKey;
	private $mapClientId;
    private $address;
    private $city;
    private $zip;
	private $state;
    private $xmlResult;
    private $simpleXml;
    private $coordinates = NULL;
	private $apiKey = 'MeS76SbstaylVoIk-CCk2-EZMCA=';
	private $standardOffset; // offset from UTC
	private $DST; // does location observe daylight savings time
	private $timezone;

    public function __construct()
    {
        $cfgSite = Zend_Registry::get('CFG_SITE');
		$site = $cfgSite->get('site');
        $this->mapKey = $site->get('google_map_key');
		$this->mapClientId = $site->get('google_map_client_id');
    }

    public function setAddress($val)
    {
        $this->address = $val;
    }

    public function setZip($val)
    {
        $this->zip = $val;
    }

    public function setCity($val)
    {
        $this->city = $val;
    }

    public function setState($val)
    {
        $this->state = $val;
    }
     public function setTechSource($val)
    {
        $this->TechSource = $val;
    }

    public function getLat()
    {
        $this->getCoordinates();

        if (!empty($this->coordinates[1])) {
            return $this->coordinates[1];
        }
    }

    public function getLon()
    {
        $this->getCoordinates();

        if (!empty($this->coordinates[0])) {
            return $this->coordinates[0];
        }
    }
	
	public function getStandardOffset() {
		return $this->standardOffset;
	}
	
	public function getDST() {
		return $this->DST;
	}

	private function addressParts() {
		$address = array();
		if (!empty($this->address)) $address[] = urlencode($this->address);
		if (!empty($this->city)) $address[] = urlencode($this->city);
		if (!empty($this->state)) $address[] = urlencode($this->state);
		if (!empty($this->zip)) $address[] = urlencode($this->zip);
		return $address;
	}
	
	private function getSignature($url) {
		// pass url minus domain
		$sig = hash_hmac('sha1', $url, base64_decode(strtr($this->apiKey, '-_', '+/')), true);
		$sig = strtr(base64_encode($sig), '+/', '-_');
		return $sig;
	}

    private function process()
    {
//        $url = 'http://maps.google.com/maps/geo?q='.$this->address.$this->city.$this->zip.'&output=xml&key='.$this->mapKey;
//		$url = 'http://maps.google.com/maps/geo?q='.implode(urlencode(','), $this->addressParts()).'&output=xml&sensor=false&client='.$this->mapClientId;
		$domain = 'https://maps.googleapis.com';
		$url = '/maps/api/geocode/xml?address='.implode(urlencode(','), $this->addressParts()).'&sensor=false&client='.$this->mapClientId;

		$sig = $this->getSignature($url);

		$url = $domain . $url . "&signature=$sig";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_FAILONERROR, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1); // return into a variable
        curl_setopt($ch, CURLOPT_TIMEOUT, 10); // times out after 4s
        $result = utf8_encode(curl_exec($ch)); // run the whole process
        curl_close($ch);
        
        $this->xmlResult = $result;
        $this->simpleXml = @simplexml_load_string($result);
    }
    
    private function getZipFromTable()
    {
		$db = Core_Database::getInstance();
		$res = $db->fetchRow("SELECT Latitude, Longitude, StandardOffset, DST FROM zipcodes WHERE ZIPCode = " . $db->quoteInto("?", $this->zip) . " LIMIT 1");
        if (!empty($res)) {
        	$this->coordinates[0] = $res['Longitude'];
        	$this->coordinates[1] = $res['Latitude'];
			$this->standardOffset = $res['StandardOffset'];
			$this->DST = $res['DST'];
        }
        return true;
    }
	
	public function getTimezone($lat = NULL, $long = NULL) {
		if (empty($lat) || empty($long)) {
			if (is_array($this->coordinates)) {
				$lat = $this->coordinates[1];
				$long = $this->coordinates[0];
			}
			if (empty($lat) || empty($long)) return false;
		}
		$timestamp = strtotime('07/01/' . date('Y')); // use time in middle of daylight savings time to see if timezone uses DST
		$domain = 'https://maps.googleapis.com';
		$url = '/maps/api/timezone/json?location=' . $lat . ',' . $long . '&timestamp=' . $timestamp . '&sensor=false&client='.$this->mapClientId;
		
		$sig = $this->getSignature($url);

		$url = $domain . $url . "&signature=$sig";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_FAILONERROR, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1); // return into a variable
        curl_setopt($ch, CURLOPT_TIMEOUT, 10); // times out after 4s
        $result = utf8_encode(curl_exec($ch)); // run the whole process
        curl_close($ch);
        
		$json = json_decode($result);
		if ($json->status != 'OK') return false;
		return array(
			'standardOffset' => $json->rawOffset / 3600.0, // convert to hrs
			'dstOffset' => $json->dstOffset / 3600.0,
			'timeZoneId' => $json->timeZoneId,
			'timeZoneName' => $json->timeZoneName
		);
	}

    public function getCoordinates($timezoneLookup = TRUE)
    {
        if (empty($this->simpleXml) && $this->coordinates === NULL) {
			if (!empty($this->zip)) {
				// check if zipcode is in zipcodes table
				if ($timezoneLookup) $this->getZipFromTable();
				if (!is_array($this->coordinates) && $timezoneLookup) {
					$geocodeZipOnly = new self();
					$geocodeZipOnly->setZip($this->zip);
					$coordinatesZipOnly = $geocodeZipOnly->getCoordinates(FALSE); // lookup coordinate of zipcode only
					// lookup timezone
					$this->timezone = $geocodeZipOnly->getTimezone();
					$this->standardOffset = $this->timezone['standardOffset'];
					$this->DST = $this->timezone['dstOffset'] > 0;
					// add the zipcode to zipcodes table with coordinates
					$db = Core_Database::getInstance();
					try {
						$db->insert('zipcodes', array(
							'ZIPCode' => $this->zip,
							'Latitude' => $geocodeZipOnly->getLat(),
							'Longitude' => $geocodeZipOnly->getLon(),
							'StandardOffset' => $this->standardOffset,
							'DST' => ($this->DST ? 1 : 0)
						));
					} catch (Exception $e) {
						// ignore if for some reason another process inserted before us
					}
				}
				$this->coordinates = NULL;
			}
            $this->process();
        }

        if ($this->coordinates === NULL) {
            if (!empty($this->simpleXml->result->geometry->location)) {
                $coordinates  = $this->simpleXml->result->geometry->location;
                $this->coordinates = array($coordinates->lng->__toString(), $coordinates->lat->__toString());
            }	
            if (empty($this->coordinates[0]) || empty($this->coordinates[1])) {
            	$this->getZipFromTable();
            }
        }
        return $this->coordinates;
    }

    
    /**
     * Calculate distance beetween coord
     * @param float $Aa
     * @param float $Ba
     * @param float $Ca
     * @param float $Da
     * @return float
     */
    public static function distance($Aa, $Ba, $Ca, $Da){ 

        $input = array($Aa, $Ba, $Ca, $Da); 
     
        $A = $Aa/57.29577951; 
        $B = $Ba/57.29577951; 
        $C = $Ca/57.29577951; 
        $D = $Da/57.29577951; 
        //convert all to radians: degree/57.29577951 
     
        if ($A == $C && $B == $D ){ 
            $dist = 0; 
        } 
        else if ( (sin($A)* sin($C)+ cos($A)* cos($C)* cos($B-$D)) > 1){ 
            $dist = 3963.1* acos(1);// solved a prob I ran into.  I haven't fully analyzed it yet    
        } 
        else{ 
            $dist = 3963.1* acos(sin($A)*sin($C)+ cos($A)* cos($C)* cos($B-$D)); 
        } 
        return ($dist); 
    } 

    
    /**
     * Check zip by canadian
     * 
     * @param string $zip
     * @return boolean
     */
    public static function isCanadianZip($zip)
    {
        return preg_match('/([A-Za-z])(\d)([A-Za-z])([ ]+)?(\d)([A-Za-z])(\d)/', trim($zip) ) ? true : false;
    }
    
}