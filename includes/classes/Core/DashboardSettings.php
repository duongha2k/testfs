<?php
class Core_DashboardSettings extends Core_DashboardSettings_Abstract
{	
	const FILTER_NONE = 0;
	const FILTER_PROJECT_MY = 1;
	const FILTER_PROJECT_SELECT = 2;
	const FILTER_WORK_ORDER = 3;
	
	const SORT_DEFAULT = 0;
	const SORT_RECENT = 1;
	const SORT_DEFAULT_MY = 2;
	
	const SORT_DIR_ASC = 0;
	const SORT_DIR_DESC = 1;	
	
	const VIEW_FULL = 0;
	const VIEW_LITE = 1;
	
	protected $_name = 'dashboard_settings';
	protected $_primary = 'client_id';
	
	public static function saveSettings($client_id, $params) {
		// save dashboard settings
		if (!is_numeric($client_id)) return false;
		if (!is_array($params)) return false;
		
		$me = self::getInstance();
		$row = $me->find($client_id);
		if (!$row || count($row) == 0)
			$row = $me->createRow();
		else
			$row = $row->getRow(0);
		$row->client_id = $client_id;
		foreach ($params as $field=>$value) {
			$row->$field = $value;
		}
		$row->save();
	}
}
