<?php

class Core_Mail {

    protected $bodyText;
    protected $fromEmail;
    protected $fromName;
    protected $toEmail;
    protected $toName;
    protected $subject;
    protected $Attachments;
    protected $tech;
    protected $isTechEmail = false;
    protected $isreportmail;
    protected $isemptyattach;
    //14110
    protected $ccEmail;

    //end 14110

    public function setBodyText($value)
    {
        $this->bodyText = $value;
    }

    public function setFromEmail($value)
    {
        $this->fromEmail = $value;
    }

    public function setFromName($value)
    {
        $this->fromName = $value;
    }

    public function setToEmail($value)
    {
        $this->toEmail = $value;
    }

    public function setToName($value)
    {
        $this->toName = $value;
    }

    public function setreportmail($value)
    {
        $this->isreportmail = $value;
    }

    public function setemptyattach($value)
    {
        $this->isemptyattach = $value;
    }

    public function setSubject($value)
    {
        $this->subject = $value;
    }

    public function setTech(Core_Tech $tech)
    {
        $this->tech = $tech;
    }

    public function isTechEmail($flag)
    {
        $this->isTechEmail = $flag === true;
    }

    public function addAttachments($value)
    {
        if (empty($this->Attachments))
            $this->Attachments = array();
        try
        {
            $fName = $value->filename;
            $description = $value->filename;
            $files = new Core_File();
            $value->encoding = Zend_Mime::ENCODING_BASE64;
            $files->uploadFile($value->filename, base64_decode($value->getContent()), S3_CLIENTS_DOCS_DIR);

            $this->Attachments[$value->filename] = array('url' => $files->getS3Url($value->filename, S3_CLIENTS_DOCS_DIR), 'description' => $value->description);
        } catch (Exception $e)
        {
            // unable to attach for some reason
        }
    }

    //14110
    public function setCcEmail($value)
    {
        $this->ccEmail = $value;
    }

    //end 14110

    public function send($forceHTML = false)
    {
		if ($this->isTechEmail && (empty($this->tech) || $this->tech->isWMTech)) return false;
        if (!file_exists(WEBSITE_LIB_PATH . "/smtpBG.php"))
        {
            $mail = new Zend_Mail();
            $mail->setBodyText($this->bodyText);
            $mail->setFrom($this->fromEmail, $this->fromName);
            $mail->addTo($this->toEmail, $this->toName);
            $mail->setSubject($this->subject);
            $mail->send();
        } else
        {
            // very quick hack
            $apicom = new Core_Api_CommonClass();
            $isHtml = !$forceHTML ? $apicom->isStringHTML($this->bodyText) : true;
            if (!empty($this->Attachments))
            {
                // append links to end of body
                if ($isHtml)
                {
                    $this->bodyText .= '<br/><br/>Attachments:<br/>';
                } else
                {
                    $this->bodyText .= "\n\nAttachments:\n";
                }
                foreach ($this->Attachments as $fName => $file)
                {
                    $url = $file['url'];
                    if ($isHtml)
                    {
                        $this->bodyText .= "<a href='$url'>$fName</a>";
                        if (!empty($file['description']))
                            $this->bodyText .= " -- " . nl2br($file['description']);
                        $this->bodyText .= "<br/>";
                    }
                    else
                    {
                        $this->bodyText .= "$fName: $url";
                        if (!empty($file['description']))
                            $this->bodyText .= " -- " . $file['description'];
                        $this->bodyText .= "\n";
                    }
                }
            }

            if ($this->isreportmail)
            {
                if ($this->isemptyattach)
                {
                    if ($isHtml)
                    {
                        $this->bodyText .= '<br/><br/>Attachments:<br/>';
                    } else
                    {
                        $this->bodyText .= "\n\nAttachments:\n";
                    }
                    $this->bodyText .= "There were no results for the selected filters";
                }

                if ($this->fromEmail == "FS-ReportWriter@fieldsolutions.com")
                {
                    if ($isHtml)
                    {
                        $this->bodyText .= '<br/><br/>FS-ReportWriter@fieldsolutions.com is an unmonitored mailbox. Please contact your FieldSolutions Support Contact for assistance.<br/>';
                    } else
                    {
                        $this->bodyText .= "\n\nFS-ReportWriter@fieldsolutions.com is an unmonitored mailbox. Please contact your FieldSolutions Support Contact for assistance.\n";
                    }
                }
            }

            $eList = explode(",", $this->toEmail);
            //14110
            $ccList = explode(",", $this->ccEmail);

            if ($isHtml)
            {
                @$this->smtpMailLogReceivedBG($this->fromName, $this->fromEmail, $eList, $this->subject, $this->bodyText, $this->bodyText, "Work Order API", "", $ccList);
            } else
            {
                @$this->smtpMailLogReceivedBG($this->fromName, $this->fromEmail, $eList, $this->subject, $this->bodyText, nl2br($this->bodyText), "Work Order API", "", $ccList);
            }
            //end 14100
        }
    }

    private function smtpMailLogReceivedBG($fromName, $fromEmail, $eList, $subject, $txt, $html, $caller, $delimiter, $ccList)
    {
        // Sends email in background, every 5 seconds. Use for large # receipients
        // eList is an array
        $eList = array_chunk($eList, 1000);

        //14110
        $ccList = array_chunk($ccList, 1000);
        //end 14110

        $delay = 0;
        foreach ($eList as $list)
        {
            //14110
            $chunk = implode(",", str_replace($delimiter, "", $list));
            $ccChunk = implode(",", str_replace($delimiter, "", $ccList));
            exec("(/usr/bin/php -f  " . WEBSITE_LIB_PATH . "/smtpBG.php " . escapeshellarg($fromName) . " " . escapeshellarg($fromEmail) . " " . escapeshellarg($chunk) . " " . escapeshellarg($subject) . " " . escapeshellarg($txt) . " " . escapeshellarg($html) . " " . escapeshellarg($caller) . " " . escapeshellarg($delay) . " " . escapeshellarg($ccChunk) . ") > /dev/null 2>&1 &");
            $delay += 5;
            //end 14110
        }
    }

}
