<?php
class Core_WorkOrder_OutboundSync_MDS extends Core_WorkOrder_OutboundSync_Abstract
{
	protected $outbound;
	protected $user = 'fieldsolutions';
	protected $pass = '92VuZ4i18';
    function __construct()
    {
		parent::__construct();
		$dir = dirname(__FILE__);
		$this->outbound = new SimOutBound;
	}

    public function syncWithWorkOrder($winNum, $currWO, $updatedWO, $context, $user) {
		$ts = date('Y-m-d h:i A');
		$status = self::determineStatus($currWO, $updatedWO);
		if ($status == NULL) return;
		$$fsWoXML = array();
		$woid = htmlspecialchars($updatedWO['WO_ID'], ENT_QUOTES, 'UTF-8');
		switch ($status) {
			case self::STATUS_ASSIGNED:
				$fsWoXML[] = '<FSWorkOrder WIN="' . $winNum . '" WOID="' . $woid . '" UpDateType="TechAssigned" TechID="' . $updatedWO['Tech_ID'] . '" TechName="' . htmlspecialchars($updatedWO['Tech_FName'], ENT_QUOTES, 'UTF-8') . ' ' . htmlspecialchars($updatedWO['Tech_LName'], ENT_QUOTES, 'UTF-8') . '" TechPhone="' . $updatedWO['TechPhone'] . '" DateTime="' . $ts . '"></FSWorkOrder>';
				break;
			case self::STATUS_UNASSIGNED:
				$fsWoXML[] = '<FSWorkOrder WIN="' . $winNum . '" WOID="' . $woid . '" UpDateType="TechUnAssigned" TechID="" TechName="" TechPhone="" DateTime="' . $ts . '"></FSWorkOrder>';
				break;
			case self::STATUS_WORK_DONE:
				$fsWoXML[] = '<FSWorkOrder WIN="' . $winNum . '" WOID="' . $woid . '" UpDateType="WorkOrderCompletedArrival" Status="Complete" DateTime="' . $updatedWO['Date_In'] . ' ' . $updatedWO['Time_In'] . '"></FSWorkOrder>';
				$fsWoXML[] = '<FSWorkOrder WIN="' . $winNum . '" WOID="' . $woid . '" UpDateType="WorkOrderCompletedDeparture" Status="Complete" DateTime="' . $updatedWO['Date_Out'] . ' ' . $updatedWO['Time_Out'] . '"></FSWorkOrder>';
				$fsWoXML[] = '<FSWorkOrder WIN="' . $winNum . '" WOID="' . $woid . '" UpDateType="WorkOrderCompletedNotes" Notes="' . htmlspecialchars($updatedWO['TechComments'], ENT_QUOTES, 'UTF-8') . '" DateTime="' . $ts . '"></FSWorkOrder>';
				break;
			case self::STATUS_APPROVED:
				$db = Core_Database::getInstance();
				$select = $db->select();
		                $select->from(Core_Database::TABLE_WORK_ORDERS, array('PayAmount'))
		                        ->where('WIN_NUM = ?', $winNum);
		                $payamount = $db->fetchOne($select);
				$fsWoXML[] = '<FSWorkOrder WIN="' . $winNum . '" WOID="' . $woid . '" UpDateType="FinalCost" Amount="' . $payamount . '"></FSWorkOrder>';
				break;
			default:
				return;
		}
		
		$xml = new Core_WorkOrder_OutboundSync_Note("XML", $fsWoXML, Core_WorkOrder_OutboundSync_Note::AUDIENCE_ALL);

		$this->sync($winNum, $xml, $status, $context, $user);
	}
	
	public function sync($winNum, $notes = NULL, $status = NULL, $context = NULL, $user = NULL) {
		$ts = date('Y-m-d h:i A');	
		$xml = "";
		if ($notes == NULL && $status != self::STATUS_WO_FILE_ATTACHED) return;
		$info = $notes->getDescription();
		switch ($status) {
			case self::STATUS_ASSIGNED_CONFIRMED:
				// Assigned and Confirmed 1 hr
				$xml = '<FSWorkOrder WIN="' . $winNum . '" WOID="' . $info['WO_ID'] . '" UpDateType="WorkOrderConfirmed" DateTime="' . $ts . '"></FSWorkOrder>';
				break;
			case self::STATUS_ASSIGNED_CHECKED_IN:
				// Assigned and Checked In
				$xml = '<FSWorkOrder WIN="' . $winNum . '" WOID="' . $info['WO_ID'] . '" UpDateType="TechArrivedOnSite" DateTime="' . $ts . '"></FSWorkOrder>';
				break;
			case self::STATUS_ASSIGNED_CHECKED_OUT:
				// Assigned and Checked Out				
				$xml = '<FSWorkOrder WIN="' . $winNum . '" WOID="' . $info['WO_ID'] . '" UpDateType="TechLeftSite" DateTime="' . $ts . '"></FSWorkOrder>';
				break;
			case self::STATUS_WO_FILE_ATTACHED:
				// Assigned and Checked In
				$fileArray = $notes->getDescription();
				$db = Core_Database::getInstance();
				$select = $db->select();
				$select->from(Core_Database::TABLE_FILES, array('id', 'path', 'description'))
					->where('id IN (?) AND WIN_NUM ='.$winNum, $fileArray);
				$info = $db->fetchAll($select);
				if (!$info || sizeof($info) == 0) return;
				$fInfo = array();
				foreach ($info as $val) {
					$fInfo[$val['id']] = array('filename' => $val['path'], 'description' => $val['description']);
				}
				$xmlArray = array();
				foreach ($fileArray as $fid) {
					if (!array_key_exists($fid, $fInfo)) continue;
					$token = md5($winNum . 'offSite#Access' . $fid);
					$link = 'http://www.fieldsolutions.com/widgets/dashboard/do/file-wos-documents-download/?win='. $winNum . '&id=' . $fid . '&token=' . $token;
					$xmlArray[] = '<ImageLink Link="' . htmlspecialchars($link, ENT_QUOTES, 'UTF-8') . '" FileName="' . htmlspecialchars($fInfo[$fid]['filename'], ENT_QUOTES, 'UTF-8') . '" FileDesc="' . htmlspecialchars($fInfo[$fid]['description'], ENT_QUOTES, 'UTF-8') . '"></ImageLink>';
				}
				$woid = $notes->getSubject();
				if (sizeof($xmlArray) > 0) $xml = "<FSWorkOrder WIN=\"$winNum\" WOID=\"" . htmlspecialchars($woid, ENT_QUOTES, 'UTF-8') . "\" UpDateType=\"WorkOrderImages\"><ImageLinks>" . implode("\n", $xmlArray) . "</ImageLinks></FSWorkOrder>";
				else $xml = "";
				break;
			default:
				if ($notes == NULL) return;
//				$xml = implode("\n", $info);
				foreach ($info as $x) {
					$this->outbound->workOrderStatusUpdate($x);
				}
				return;
		}
		$this->outbound->workOrderStatusUpdate($xml);
	}
}

class SimOutBound {
	protected $outbound;
	function __construct() {
	}
		
	function workOrderStatusUpdate($params) {
		if (empty($params)) return false;
//		$url = "http://test.fieldsolutions.com/eptest.php";

		$url = "http://edi.mfsclarity.com/EDI/XMLIn.aspx?Process=FieldSolutions";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url); // set url to post to
		curl_setopt($ch, CURLOPT_FAILONERROR, 0);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);// allow redirects
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // return into a variable
		curl_setopt($ch, CURLOPT_TIMEOUT, 15);
		curl_setopt($ch, CURLOPT_POST, 0); // set POST method
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: text/xml'));
		$xml = "<?xml version=\"1.0\"?>$params";
		curl_setopt($ch, CURLOPT_POSTFIELDS, $xml); // set POST method
//		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		$result = curl_exec($ch); // run the whole process
		
		/*try {
			$fp = fopen("/home/p3559/ob.txt", "a");
			fwrite($fp, date('c') . "---- $params\n\n");
			fclose($fp);
		} catch (Exception $e) {}*/

		return "Success";
	}
}
