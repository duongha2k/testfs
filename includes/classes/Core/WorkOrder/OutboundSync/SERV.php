<?php
class Core_WorkOrder_OutboundSync_SERV extends Core_WorkOrder_OutboundSync_Abstract
{
	protected $outbound;
    function __construct()
    {
		parent::__construct();
		$dir = dirname(__FILE__);
		$this->outbound = new SimOutBound;
	}

    public function syncWithWorkOrder($winNum, $currWO, $updatedWO, $context, $user) {
		$status = self::determineStatus($currWO, $updatedWO);
		if ($status == NULL && 
			$currWO['Tech_Bid_Amount'] == $updatedWO['Tech_Bid_Amount'] &&
			$context != Core_WorkOrder_OutboundSync_Abstract::CONTEXT_TECH
		) return;
		if ($context == Core_WorkOrder_OutboundSync_Abstract::CONTEXT_TECH && $updatedWO['Status'] == 'assigned') {
			if (!empty($updatedWO['Time_In'])) {
				$currWO['Date_In'] = NULL;
				$currWO['Time_In'] = NULL;
			}
			if (!empty($updatedWO['Time_Out'])) {
				$currWO['Date_Out'] = NULL;
				$currWO['Time_Out'] = NULL;
			}
		}
//		else return;
		$this->sync($winNum, array($currWO, $updatedWO, $winNum), $status, $context, $user);
	}
	
	public function sync($winNum, $notes = NULL, $status = NULL, $context = NULL, $user = NULL) {
		if (is_array($notes)) {
			$curWO = $notes[0];
			$updatedWO = $notes[1];
		}
		else {
			$class = new Core_Api_Class;
			$updatedWO = $class->getWorkOrdersWithWinNum($winNum, NULL, "");
			$updatedWO = $updatedWO[0];
			$curWO = $updatedWO;
			$curWO['lastModDate'] = NULL;
			$curWO['lastModBy'] = NULL;
			switch ($status) {
			case self::STATUS_ASSIGNED_CONFIRMED:
				// Assigned and Confirmed 1 hr
				break;
			case self::STATUS_ASSIGNED_CHECKED_IN:
				// Assigned and Checked In
				$curWO['Date_In'] = NULL;
				$curWO['Time_In'] = NULL;
				return;
				break;
			case self::STATUS_ASSIGNED_CHECKED_OUT:
				// Assigned and Checked Out
				$curWO['Date_Out'] = NULL;
				$curWO['Time_Out'] = NULL;
				$curWO['TechComments'] = NULL;
				break;
			case self::STATUS_WO_FILE_ATTACHED:
				// Assigned and Checked In
				break;
			}
		}

		$result = array($curWO, $updatedWO, $winNum);
		$this->outbound->workOrderStatusUpdate($result);
	}
}

class SimOutBound {
	protected $outbound;
	function __construct() {
	}
		
	function workOrderStatusUpdate($params) {
		if (empty($params)) return false;
/*		$bufArr = array('StartDate','StartTime','Address','City','State','Zipcode','PayAmount',
                            'SpecialInstructions','BaseTechPay','OutofScope_Amount','TripCharge','MileageReimbursement',
                            'MaterialsReimbursement','AbortFeeAmount','Additional_Pay_Amount','Add_Pay_Reason','lastModBy');

		$woChangedMail = new Core_Mail_WoStatus_Change();
		$curWO = $params[0];
		$updatedFields = $params[1];
		$winNum = $params[2];
		$woChangedMail->assign('TB_UNID', $winNum);
		$project = $updatedFields["Project_ID"];
		$projectInfo = new Core_Project((int)$project);
                               $projectInfo->setData();
                        $woChangedMail->setProject($projectInfo);
            	foreach ($curWO as $key=>$val) {
                	if (empty($curWO[$key]) || $val == $updatedFields[$key]) continue;
                    	if (in_array($key,$bufArr)) {
                        	$woChangedMail->assign($key, $updatedFields[$key]);
                    	}
                }
	$woChangedMail->assign('WorkOrderNo', $updatedFields['WO_ID']);
	$woChangedMail->assign('projectName', $updatedFields['Project_Name']);
            $woChangedMail->assign('TB_UNID', $winNum);
		$woChangedMail->send(true, "fsupdates@servright.com");*/

		$curWO = $params[0];
		$updatedFields = $params[1];
		$winNum = $params[2];

$dom = new DOMDocument("1.0", "UTF-8");
$root = $dom->createElement("WorkOrderUpdatedFields");
$dom->appendChild($root);
$projectName = empty($updatedFields['Project_Name']) ? $curWO['Project_Name'] : $updatedFields['Project_Name'];

		$root->appendChild(new DOMElement('WIN_NUM', $winNum));
		$root->appendChild(new DOMElement('WO_ID', $updatedFields['WO_ID']));
            	foreach ($updatedFields as $key=>$val) {
//			echo ("$key => $val {$curWO[$key]}\n");
                	if ($val == $curWO[$key]) continue;
		        $root->appendChild(new DOMElement($key, $val));
                }
$r = $dom->saveXML();


$contactMail = new Core_Mail();
                        $contactMail->setFromName("FieldSolutions");
                        $contactMail->setFromEmail("no-replies@fieldsolutions.com");
                        $contactMail->setToName("");
                        $contactMail->setToEmail("fsupdates@servright.com");
                        $contactMail->setSubject("Project: $projectName - WorkOrder# {$updatedFields['WO_ID']} has been changed");
                        $contactMail->setBodyText($r);

                        $contactMail->send();


		return "Success";
	}
}
