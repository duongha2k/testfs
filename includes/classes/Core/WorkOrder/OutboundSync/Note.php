<?php
class Core_WorkOrder_OutboundSync_Note
{
	
	const AUDIENCE_ALL = 1;
	const AUDIENCE_CLIENT = 2;
	
	protected $subject;
	protected $description;
	protected $audience;
	
	function __construct($subj, $descr, $audience)
    {
		$this->subject = $subj;
		$this->description = $descr;
		$this->audience = $audience;
	}
	
	public function getSubject() {
		return $this->subject;
	}
	public function getDescription() {
		return $this->description;
	}
	public function getAudience() {
		return $this->audience;
	}
}
