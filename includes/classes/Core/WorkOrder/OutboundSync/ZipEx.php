<?php
class Core_WorkOrder_OutboundSync_ZipEx extends Core_WorkOrder_OutboundSync_Abstract
{
	protected $outbound;
	protected $user = 'fieldsolutions';
	protected $pass = '92VuZ4i18';
    function __construct()
    {
		parent::__construct();
		$dir = dirname(__FILE__);
/*		$this->outbound = new SoapClient("$dir/WorkOrderService.xml", array(
				'classmap' => array(
									'workOrderStatusParam' => 'Core_OutboundSync_ZipEx_WorkOrderStatusParam',
									'workOrderNoteParam' => 'Core_OutboundSync_ZipEx_WorkOrderNoteParam'
									)
			)
		);*/
		$this->outbound = new SimOutBound;
	}

    public function syncWithWorkOrder($winNum, $currWO, $updatedWO, $context, $user) {
		$newWO = $currWO == NULL;
		$statusChanged = $newWO || $currWO['Status'] != $updatedWO['Status'];
		$status = NULL;
		
		// Determine status change
		if ($statusChanged && $updatedWO['Status'] == WO_STATE_CREATED) {
			// Created
			$status = self::STATUS_CREATED;
		}
		else if ($updatedWO['Status'] == WO_STATE_ASSIGNED && ($newWO || $currWO['WorkOrderReviewed'] == 'False' || $currWO['WorkOrderReviewed'] == 0) && ($updatedWO['WorkOrderReviewed'] == 'True' || $updatedWO['WorkOrderReviewed'] == 1)) { 
			// Assigned and Reviewed
			$status = self::STATUS_ASSIGNED_REVIEWED;
		}
		else if ($updatedWO['Status'] == WO_STATE_ASSIGNED && empty($currWO['Time_In']) && !empty($updatedWO['Time_In']) ) { 
			// Assigned and Checked In
			$status = self::STATUS_ASSIGNED_CHECKED_IN;
		}
		else if ($statusChanged && $updatedWO['Status'] == WO_STATE_WORK_DONE) { 
			// Work Done
			$status = self::STATUS_WORK_DONE;
		}
		else if ($statusChanged && $updatedWO['Status'] == WO_STATE_APPROVED) { 
			// Approved
			$status = self::STATUS_APPROVED;
		}
		else if ($statusChanged && $updatedWO['Status'] == WO_STATE_COMPLETED) { 
			// Completed
			$status = self::STATUS_COMPLETED;
		}
		else if ($statusChanged && $updatedWO['Status'] == WO_STATE_DEACTIVATED) { 
			// Deactivated
			$status = self::STATUS_DEACTIVATED;
		}
		
		// Build notes
		$changedFields = array();
		$clientOnlyFields = array();
		
		foreach ($updatedWO as $k => $v) {
			if ($k == 'Requirements') continue;
			if ($newWO || (array_key_exists($k, $currWO ) && $currWO[$k] == $v)) continue;
			$vis = $this->getFieldVisibility($k);
			if ($vis == self::FIELD_VISIBILITY_INTERNAL) continue;
			if ($vis == self::FIELD_VISIBILITY_ALL)
				$changedFields[$k] = $v;
			else if ($vis == self::FIELD_VISIBILITY_CLIENT)
				$clientOnlyFields[$k] = $v;
		}
		
		$noteDescr = "";
		$noteDescrClientOnly = "";
		$notes = NULL;
		$notesClientOnly = NULL;
		
		foreach ($changedFields as $k => $v) {
			$noteDescr .= "$k changed to '$v'\n";
		}
		
		foreach ($clientOnlyFields as $k => $v) {
			$noteDescrClientOnly .= "$k changed to '$v'\n";
		}

		if (!empty($noteDescr))
			$notes = new Core_WorkOrder_OutboundSync_Note("Work order updated", $noteDescr, Core_WorkOrder_OutboundSync_Note::AUDIENCE_ALL);

		if (!empty($noteDescrClientOnly))
			$notesClientOnly = new Core_WorkOrder_OutboundSync_Note("Work order updated", $noteDescrClientOnly, Core_WorkOrder_OutboundSync_Note::AUDIENCE_CLIENT);

		$this->sync($winNum, $notes, $status, $context, $user);
		
		if (!empty($noteDescrClientOnly))
			$this->sync($winNum, $notesClientOnly, NULL, $context, $user);
	}
	
	public function sync($winNum, $notes = NULL, $status = NULL, $context, $user) {
		$ts = date('Y-m-d');
		$woStatus = new Core_WorkOrder_OutboundSync_ZipEx_WorkOrderStatusParam;
		$woStatus->workOrderId = $winNum;
		$woStatus->statusDate = $ts;
		$woStatus->statusResponsibleParty = $user;
/*		switch ($status) {
			case self::STATUS_CREATED:
				// Created
				$woStatus->statusCode = 35;
				$woStatus->subCode = 13;
				break;
			case self::STATUS_ASSIGNED_REVIEWED:
				// Assigned and Reviewed
				$woStatus->statusCode = 3;
				$woStatus->subCode = 71;
				break;
			case self::STATUS_ASSIGNED_CHECKED_IN:
				// Assigned and Checked In
				$woStatus->statusCode = 84;
				$woStatus->subCode = 75;
				break;
			case self::STATUS_WORK_DONE:
				// Assigned and Checked In
				$woStatus->statusCode = 4;
				$woStatus->subCode = 85;
				break;
			case self::STATUS_APPROVED:
				// Assigned and Checked In
				$woStatus->statusCode = 4;
				$woStatus->subCode = 86;
				break;
			case self::STATUS_COMPLETED:
				// Completed
				$woStatus->statusCode = 4;
				$woStatus->subCode = 5;
				break;
			case self::STATUS_DEACTIVATED:
				// Deactivated
				$woStatus->statusCode = 6;
				$woStatus->subCode = 7;
				break;
		}*/
		switch ($status) {
			case self::STATUS_CREATED:
				// Created
				$woStatus->statusCode = 13;
				$woStatus->subCode = 35;
				break;
			case self::STATUS_ASSIGNED_REVIEWED:
				// Assigned and Reviewed
				$woStatus->statusCode = 71;
				$woStatus->subCode = 3;
				break;
			case self::STATUS_ASSIGNED_CHECKED_IN:
				// Assigned and Checked In
				$woStatus->statusCode = 75;
				$woStatus->subCode = 84;
				break;
			case self::STATUS_WORK_DONE:
				// Assigned and Checked In
				$woStatus->statusCode = 85;
				$woStatus->subCode = 4;
				break;
			case self::STATUS_APPROVED:
				// Assigned and Checked In
				$woStatus->statusCode = 86;
				$woStatus->subCode = 4;
				break;
			case self::STATUS_COMPLETED:
				// Completed
				$woStatus->statusCode = 5;
				$woStatus->subCode = 4;
				break;
			case self::STATUS_DEACTIVATED:
				// Deactivated
				$woStatus->statusCode = 7;
				$woStatus->subCode = 6;
				break;
		}
		if ($status !== NULL) {
			$params = new stdClass;
			$params->username = $this->user;
			$params->password = $this->pass;
			$params->woStatusParam = $woStatus;
			$this->outbound->workOrderStatusUpdate($params);
		}
		if ($notes !== NULL) {
			$notesFinal = new Core_WorkOrder_OutboundSync_ZipEx_WorkOrderNoteParam;
			$notesFinal->noteAudience = $notes->getAudience() == Core_WorkOrder_OutboundSync_Note::AUDIENCE_ALL ? 'All' : 'Private';
			$notesFinal->noteCreationDate = $ts;
			$notesFinal->noteCreator = $user;
			$notesFinal->noteDescription = $notes->getDescription();
			$notesFinal->noteExternalId = 0;
			$notesFinal->noteSubject = $notes->getSubject() . " -- by " . $context;
			$notesFinal->workOrderId = $winNum;
			$params = new stdClass;
			$params->username = $this->user;
			$params->password = $this->pass;
			$params->woNoteParam = $notesFinal;
			$this->outbound->workOrderNotesAppend($params);
		}
	}
}

class SimOutBound {
	protected $outbound;
	function __construct() {
		$dir = dirname(__FILE__);
//		$this->outbound = new SoapClient("$dir/ZipEx/WorkOrderService.xml", array('trace' => true,
		$this->outbound = new SoapClient("http://connect.zipinstallation.com:8080/service/api/soap/v1/WorkOrderService?wsdl", array('trace' => true,
				'classmap' => array(
									'workOrderStatusParam' => 'Core_OutboundSync_ZipEx_WorkOrderStatusParam',
									'workOrderNoteParam' => 'Core_OutboundSync_ZipEx_WorkOrderNoteParam'
									)
			)
		);
	}
	
	function workOrderNotesAppend($params) {
//		$fp = fopen("/home/p3559/ob.txt", "a");
		$notifyEmail = new Core_Mail();
		$notifyEmail->setFromName("ZipEx Outbound Sync");
		$notifyEmail->setFromEmail("nobody@fieldsolutions.com");
		$notifyEmail->setToName("Trung Ngo");
		$notifyEmail->setToEmail("tngo@fieldsolutions.com");
		$notifyEmail->setSubject("Notes Append");
		$msg = "";
		try {
			$res = $this->outbound->workOrderNotesAppend($params);
		} catch (Exception $e) {
			$msg .= print_r($e, true);
		}
		$req = $this->outbound->__getLastRequest();
		$res = $this->outbound->__getLastResponse();
//		fwrite($fp, date('c') . " --- Note Append ---\n" . print_r($params, true) . " Req = $req Result = '$res'\n --- End Note Append --- \n\n");
//		fclose($fp);
		@$msg .= date('c') . " --- Note Append ---\n" . print_r($params, true) . " Req = $req Result = '$res'\n --- End Note Append --- \n\n";
		$notifyEmail->setBodyText($msg);
//		$notifyEmail->send();
		return "Success";
	}
	function workOrderStatusUpdate($params) {
//		$fp = fopen("/home/p3559/ob.txt", "a");
		$notifyEmail = new Core_Mail();
		$notifyEmail->setFromName("ZipEx Outbound Sync");
		$notifyEmail->setFromEmail("nobody@fieldsolutions.com");
		$notifyEmail->setToName("Trung Ngo");
		$notifyEmail->setToEmail("tngo@fieldsolutions.com");
		$notifyEmail->setSubject("Status Update");
		$msg = "";
		try {
			$res = $this->outbound->workOrderStatusUpdate($params);
		} catch (Exception $e) {
			$msg .= print_r($e, true);
		}
		$req = $this->outbound->__getLastRequest();
		$res = $this->outbound->__getLastResponse();
//		fwrite($fp, date('c') . " --- Status Update ---\n" . print_r($params, true) . " Req = $req Result = '$res'\n --- End Status Update --- \n\n");
		@$msg .= date('c') . " --- Status Update ---\n" . print_r($params, true) . " Req = $req Result = '$res'\n --- End Status Update --- \n\n";
		$notifyEmail->setBodyText($msg);
//		$notifyEmail->send();
//		fclose($fp);
		return "Success";
	}
}
