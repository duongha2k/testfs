<?php
abstract class Core_WorkOrder_OutboundSync_Abstract
{
	const FIELD_VISIBILITY_NOT_FOUND = 0;
	const FIELD_VISIBILITY_ALL = 1;
	const FIELD_VISIBILITY_CLIENT = 2;
	const FIELD_VISIBILITY_INTERNAL = 3;
	
	const STATUS_CREATED = 0;
	const STATUS_ASSIGNED = 1;
	const STATUS_ASSIGNED_REVIEWED = 2;
	const STATUS_ASSIGNED_CONFIRMED = 3;
	const STATUS_ASSIGNED_CHECKED_IN = 4;
	const STATUS_WORK_DONE = 5;
	const STATUS_APPROVED = 6;
	const STATUS_COMPLETED = 7;
	const STATUS_DEACTIVATED = 8;
	const STATUS_UNASSIGNED = 9;
	const STATUS_WO_FILE_ATTACHED = 10;
	const STATUS_ASSIGNED_CHECKED_OUT = 11;
	
	const CONTEXT_CLIENT = 'client';
	const CONTEXT_TECH = 'tech';
	const CONTEXT_ADMIN = 'admin';

	protected $internalFields;

    function __construct()
    {
		$this->internalFields = array('FS_INTERNALCOMMENTS');
    }

    abstract public function syncWithWorkOrder($winNum, $currWO, $updatedWO, $context, $user);
    abstract public function sync($winNum, $notes, $status, $context, $user);
	
	protected function getFieldVisibility($field) {
		$techExists = property_exists("API_TechWorkOrder", $field);
		$clientExists = property_exists("API_WorkOrder", $field);
		
		if (!$clientExists && !$techExists) return self::FIELD_VISIBILITY_NOT_FOUND;
		if ($techExists) return self::FIELD_VISIBILITY_ALL;
		if (in_array(strtoupper($field), $this->internalFields)) return self::FIELD_VISIBILITY_INTERNAL;
		
		return self::FIELD_VISIBILITY_CLIENT;
	}
	
	protected static function determineStatus($currWO, $updatedWO) {
		$newWO = $currWO == NULL;
		$statusChanged = $newWO || $currWO['Status'] != $updatedWO['Status'];
		$status = NULL;

		// Determine status change
		if ($newWO && $updatedWO['Status'] == WO_STATE_CREATED) {
			// Created
			$status = self::STATUS_CREATED;
		}
		else if ($statusChanged && empty($updatedWO['Tech_ID']) && $currWO['Tech_ID'] != $updatedWO['Tech_ID']) {
			// Unassigned
			$status = self::STATUS_UNASSIGNED;
		}
		else if ($statusChanged && $updatedWO['Status'] == WO_STATE_ASSIGNED && ($updatedWO['WorkOrderReviewed'] == 'False' || $updatedWO['WorkOrderReviewed'] == 0)) { 
			// Assigned
			$status = self::STATUS_ASSIGNED;
		}
		else if ($updatedWO['Status'] == WO_STATE_ASSIGNED && ($newWO || $currWO['WorkOrderReviewed'] == 'False' || $currWO['WorkOrderReviewed'] == 0) && ($updatedWO['WorkOrderReviewed'] == 'True' || $updatedWO['WorkOrderReviewed'] == 1)) { 
			// Assigned and Reviewed
			$status = self::STATUS_ASSIGNED_REVIEWED;
		}
		else if ($updatedWO['Status'] == WO_STATE_ASSIGNED && empty($currWO['Time_In']) && !empty($updatedWO['Time_In']) ) { 
			// Assigned and Checked In
			$status = self::STATUS_ASSIGNED_CHECKED_IN;
		}
		else if ($statusChanged && $updatedWO['Status'] == WO_STATE_WORK_DONE) { 
			// Work Done
			$status = self::STATUS_WORK_DONE;
		}
		else if ($statusChanged && $updatedWO['Status'] == WO_STATE_APPROVED) { 
			// Approved
			$status = self::STATUS_APPROVED;
		}
		else if ($statusChanged && $updatedWO['Status'] == WO_STATE_COMPLETED) { 
			// Completed
			$status = self::STATUS_COMPLETED;
		}
		else if ($statusChanged && $updatedWO['Status'] == WO_STATE_DEACTIVATED) { 
			// Deactivated
			$status = self::STATUS_DEACTIVATED;
		}
		
		return $status;
	}
}
