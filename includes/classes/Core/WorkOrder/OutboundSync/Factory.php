<?php
class Core_WorkOrder_OutboundSync_Factory
{
    private function __construct() {		
	}
	
	public static function build($company) {
//		if ($company == 'CBD') $company = 'ZipEx';
		$className = "Core_WorkOrder_OutboundSync_" . $company;
		@$ex = class_exists($className);
		if (!$ex) return false;
		return new $className;
	}
}