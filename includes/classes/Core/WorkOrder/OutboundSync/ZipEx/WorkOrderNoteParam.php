<?php
class Core_WorkOrder_OutboundSync_ZipEx_WorkOrderNoteParam {
	var $noteAudience;
	var $noteCreationDate;
	var $noteCreator;
	var $noteDescription;
	var $noteExternalId;
	var $noteSubject;
	var $workOrderId;
}