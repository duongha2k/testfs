<?php
class Core_WorkOrder_OutboundSync_ZipEx_WorkOrderStatusParam {
	var $statusCode;
	var $statusDate;
	var $statusResponsibleParty;
	var $subCode;
	var $workOrderId;
}