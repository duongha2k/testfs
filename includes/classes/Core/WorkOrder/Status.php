<?php
class Core_WorkOrder_Status {
			// Created - ShowTechs = '0' AND IFNULL(Tech_ID, 0) = 0 AND Deactivated = '0' 
			// Published - ShowTechs = '1' AND IFNULL(Tech_ID, 0) = 0 AND Deactivated = '0' AND IFNULL(FLS_ID,'') = ''
			// Assigned - IFNULL(Tech_ID, 0) != 0 AND Deactivated = '0' AND TechMarkedComplete = '0' AND IFNULL(MissingComments,'') = ''
			// Work Done - TechMarkedComplete = '1' AND Approved = '0' AND TechPaid = '0' AND Invoiced = '0' AND Deactivated = '0'
			// Approved - Approved = '1' AND Invoiced = '0' AND Deactivated = '0'
			// Incomplete - TechMarkedComplete = '0' AND Approved = '0' AND TechPaid = '0' AND Invoiced = '0' AND IFNULL(MissingComments,'') = '' AND Deactivated = '0'
			// In Accounting - Invoiced = '1' AND TechPaid = '0' AND Deactivated = '0'
			// Completed - TechPaid = '1' AND Deactivated = '0'
			// All - Deactivated = '0'
			// Deactivated - Deactivated = '1'
	
	// cache status query
	private static $stateQuery = null;
	private static $stateQueryName = null;

	private static $lifecycleStates = null;
	private static $customStates = null;
	private static $allStates = null;

	const LIFECYCLE_STATE = 0;
	const CUSTOM_STATE = 1;

	private static $idToNameMap;
	private static $nameToIdMap;
	private static $currentId = 0;

	private static function init() {
			if (self::$lifecycleStates !== null) return;
			
			self::$lifecycleStates = array();
			self::$customStates = array();

			self::stateDefine(WO_STATE_CREATED, "ShowTechs = 0 AND IFNULL(Tech_ID, 0) = 0 AND Deactivated = 0 AND Approved = 0");
			self::stateDefine(WO_STATE_PUBLISHED, "ShowTechs = 1 AND IFNULL(Tech_ID, 0) = 0 AND Deactivated = 0 AND IFNULL(FLS_ID,'') = ''");
			self::stateDefine(WO_STATE_ASSIGNED, "IFNULL(Tech_ID, 0) != 0 AND Deactivated = 0 AND TechMarkedComplete = 0 AND IFNULL(MissingComments,'') = ''");
			self::stateDefine(WO_STATE_WORK_DONE, "TechMarkedComplete = 1 AND Approved = 0 AND TechPaid = 0 AND Invoiced = 0 AND Deactivated = 0");
			self::stateDefine(WO_STATE_INCOMPLETE, "TechMarkedComplete = 0 AND Approved = 0 AND TechPaid = 0 AND Invoiced = 0 AND IFNULL(MissingComments,'') != '' AND Deactivated = 0");
			self::stateDefine(WO_STATE_APPROVED, "Approved = 1 AND Invoiced = 0 AND Deactivated = 0");
			self::stateDefine(WO_STATE_IN_ACCOUNTING, "Invoiced = 1 AND TechPaid = 0 AND Deactivated = 0");
			self::stateDefine(WO_STATE_COMPLETED, "TechPaid = 1 AND Deactivated = 0");
			self::stateDefine(WO_STATE_DEACTIVATED, "Deactivated = 1");

			self::stateDefine(WO_STATE_ALL_ACTIVE, "Deactivated = 0", self::CUSTOM_STATE);
			self::stateDefine(WO_STATE_COMPLETED_TAB, "Approved = 1 AND Deactivated = 0", self::CUSTOM_STATE);
			self::stateDefine(WO_STATE_TECH_PAID_TAB, "Invoiced = 1 AND Deactivated = 0", self::CUSTOM_STATE);
            self::stateDefine("open", "Status = 'assigned' OR Status = 'published' OR Status = 'work done' OR Status = 'incomplete' ",self::CUSTOM_STATE);
            self::stateDefine("close", "(Approved = 1) AND (Deactivated = 0)",self::CUSTOM_STATE);

			self::$allStates = array_merge(self::$lifecycleStates, self::$customStates);
	}
	
	private static function stateDefine($name, $filter, $type = self::LIFECYCLE_STATE) {
		$id = self::$currentId;
		++self::$currentId;
		self::$idToNameMap[$id] = $name;
		self::$nameToIdMap[$name] = $id;
		$query = WorkOrderStateQuery::define($id, $name, $filter);
		if ($type == self::LIFECYCLE_STATE)
			self::$lifecycleStates[$name] = $query;
		else
			self::$customStates[$name] = $query;
	}
	
	public static function getStatusQuery() {
		self::init();
		// return sql query for getting the life cycle state of a work order as id
		if (self::$stateQuery !== null) return self::$stateQuery;
		$query = "(CASE ";
		foreach (self::$lifecycleStates as $state) {
			$query .= "WHEN ({$state->getFilter()}) THEN {$state->getId()} ";
		}
		$query .= "ELSE -1 END)"; // else case should not happen
		self::$stateQuery = $query;
		return $query;
	}

	public static function getStatusQueryAsName() {
		self::init();
		// return sql query for getting the life cycle state of a work order as name
		if (self::$stateQueryName !== null) return self::$stateQueryName;
		$query = "(CASE ";
		foreach (self::$lifecycleStates as $state) {
			$query .= "WHEN ({$state->getFilter()}) THEN '{$state->getName()}' ";
		}
		$query .= "ELSE '' END)"; // else case should not happen
		self::$stateQueryName = $query;
		return $query;
	}
	
	public static function getStateConditionsQueryWithName($name) {
		self::init();
		// return where clause for a status / use to find WOs with a particular life cycle or custom status
		return array_key_exists($name, self::$customStates) ? self::$allStates[$name]->getFilter() : "";
	}

	public static function getNameWithId($id) {
		return array_key_exists($id, self::$idToNameMap) ? self::$idToNameMap[$id] : "";		
	}

	public static function getIdWithName($name) {
		return array_key_exists($name, self::$nameToIdMap) ? self::$nameToIdMap[$name] : "";		
	}
	
	public static function getStatusWithWorkOrder($curWO) {
		$ShowTechsFalse = !isset($curWO["ShowTechs"]) || $curWO["ShowTechs"] === "False";
                $DeactivatedFalse = !isset($curWO["Deactivated"]) || $curWO["Deactivated"] === "False";
                $TechMarkedCompleteFalse = !isset($curWO["TechMarkedComplete"]) || $curWO["TechMarkedComplete"] === "False";
                $ApprovedFalse = !isset($curWO["Approved"]) || $curWO["Approved"] === "False";
                $TechPaidFalse = !isset($curWO["TechPaid"]) || $curWO["TechPaid"] === "False";
                $InvoicedFalse = !isset($curWO["Invoiced"]) || $curWO["Invoiced"] === "False";
		if ($ShowTechsFalse && 
			empty($curWO["Tech_ID"]) &&
			empty($curWO["FLS_ID"]) &&
			$DeactivatedFalse &&
			$ApprovedFalse) return WO_STATE_CREATED;
		else if ($curWO["ShowTechs"] === "True" &&
			empty($curWO["Tech_ID"]) &&
			empty($curWO["FLS_ID"]) &&
			$DeactivatedFalse) return WO_STATE_PUBLISHED;
                else if ($curWO["Approved"] === "True" && 
                        $TechMarkedCompleteFalse &&			
			$DeactivatedFalse) return WO_STATE_APPROVED;//13985
		else if ((!empty($curWO["Tech_ID"]) ||
			!empty($curWO["FLS_ID"])) &&
			$DeactivatedFalse &&
			$TechMarkedCompleteFalse &&
			empty($curWO["MissingComments"])) return WO_STATE_ASSIGNED;
		else if ($curWO["TechMarkedComplete"] === "True" &&
			$ApprovedFalse &&
			$TechPaidFalse &&
			$InvoicedFalse &&
			$DeactivatedFalse) return WO_STATE_WORK_DONE;
		else if ($TechMarkedCompleteFalse &&
			$ApprovedFalse &&
			$TechPaidFalse &&
			$InvoicedFalse &&
			!empty($curWO["MissingComments"]) &&
			$DeactivatedFalse) return WO_STATE_INCOMPLETE;
		else if ($curWO["Approved"] === "True" &&
			$InvoicedFalse &&
			$DeactivatedFalse) return WO_STATE_APPROVED;
		else if ($curWO["Invoiced"] === "True" &&
			$TechPaidFalse &&
			$DeactivatedFalse) return WO_STATE_IN_ACCOUNTING;
		else if ($curWO["TechPaid"] === "True" &&
			$DeactivatedFalse) return WO_STATE_COMPLETED;
		else if ($curWO["Deactivated"] === "True") return WO_STATE_DEACTIVATED;
		else return "";		
	}
}

class WorkOrderStateQuery {
	protected $id;
	protected $name;
	protected $filter;

	private function __construct() {}

	public static function define($id, $name, $filter) {
		$state = new self();
		$state->id = $id;
		$state->name = $name;
		$state->filter = $filter;
		return $state;
	}

	public function getId() {
		return $this->id;
	}

	public function getName() {
		return $this->name;
	}

	public function getFilter() {
		return $this->filter;
	}	
}
?>
