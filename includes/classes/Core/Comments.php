<?php
class Core_Comments extends Zend_Db_Table_Abstract {
	const TYPE_PRIVATE = 0;
	const TYPE_CLIENT = 1;
	const TYPE_PUBLIC = 2;

	const CLASSIFICATION_FAVORABLE = 0;
	const CLASSIFICATION_NEUTRAL = 1;
	const CLASSIFICATION_UNFAVORABLE = 2;
	
	protected $_name = 'comments';
	protected $_rowClass = 'Core_Comment';
	
	public function getTechComments($techID, $client, $sort = NULL) {
		if (empty($techID)) return array();
		$select = $this->basicSelectWithJoins($client, $sort);
		$select->where('WIN_NUM IS NULL')
			->where('TechID = ?', $techID);
		return $this->fetchAll($select);
	}

	public function getWorkOrderComments($techID, $client, $winNum = NULL, $sort = NULL) {
		if (empty($techID) && empty($winNum)) return array();
		$select = $this->basicSelectWithJoins($client, $sort);
		if (!empty($techID))
			$select->where('TechID = ?', $techID);
		$select->join(array("w" => Core_Database::TABLE_WORK_ORDERS), 'c.WIN_NUM = w.WIN_NUM', array('skillCategory' => 'WO_Category', 'headline' => 'Headline', 'jobRating' => 'SATPerformance'));
		if (!empty($winNum))
			$select->where('c.WIN_NUM = ?', $winNum);
		else 
			$select->where('c.WIN_NUM IS NOT NULL');
            
            
			return $this->fetchAll($select);
	}

	public function getComment($id, $client) {
//		if (empty($client)) return NULL;
		if (empty($id)) return NULL;
		$db = Core_Database::getInstance();
		$select = $this->basicSelect();
		$select->where('id = ?', $id);
		if (!empty($client)) {
			$companyID = empty($client) ? '' : $client->getContextCompanyId();
			$select->where('type = ' . self::TYPE_PUBLIC . ' OR Company_ID = ? OR ' . $db->quoteInto('clientID = ?', $client->getClientId()), $companyID);
		}
		$select->limit(1);
		$ret = $this->fetchRow($select);
		return $ret;
	}

	protected function basicSelectWithJoins(Core_Api_User $client, $order = NULL) {
        $clientID = $client->getClientId();    
        $companyID = $client->getCompanyId();   
        $classApi = new Core_Api_Class();
        $isGPM = $classApi->isGPM($clientID);
        
		$db = Core_Database::getInstance();
		$select = $this->basicSelect();
		$select->setIntegrityCheck(false);
        if($isGPM)
        {
			$select->from(array("c" => $this->_name), array('*'));
            $select->join(array("cli"=>Core_Database::TABLE_CLIENTS),
                        'cli.ClientID=c.clientID',
                        array('ClientCompanyID'=>'Company_ID','UserName')
            );
			$select->group('c.id');
        }
        else
        {
            $select->from(array("c" => $this->_name), array('*'));
            $select->join(array("cli"=>Core_Database::TABLE_CLIENTS),
                        'cli.ClientID=c.clientID',
                        array('ClientCompanyID'=>'Company_ID','UserName')
            );
            $select->where("c.type=2 OR (c.type=0 AND c.clientID='$clientID') OR (c.type=1 AND cli.Company_ID='$companyID')");
            $select->group('c.id');            
        }
        
	if (!empty($order)) {
	    foreach ($order as $o) {
		$select->order($o);
	    }
	}
	else
        {
	    $select->order('date desc');
        }
		return $select;
	}

	protected function basicSelect() {
		$select = $this->select();
		return $select;
	}

	public function add($comment, $client, $techId, $winNum = NULL, $type = self::TYPE_PRIVATE, $class = self::CLASSIFICATION_FAVORABLE) {
		if (empty($client)) return false;
		$companyID = empty($client) ? '' : $client->getContextCompanyId();
		$clientId = $client->getClientId();
		$newComment = $this->createRow();
		$newComment->comment = $comment;
		$newComment->clientID = $clientId;
		$newComment->Company_ID = $companyID;
		$newComment->TechID = $techId;
		$newComment->WIN_NUM = $winNum;
		$newComment->type = $type;
		$newComment->classification = $class;
		$newComment->save();
		return true;
	}

	public function edit($id, $client, $comment, $type = NULL, $class = NULL) {
		$c = $this->getComment($id, $client);
		if (empty($c)) return false;
		$c->comment = $comment;
		if ($type !== NULL)
			$c->type = $type;
		if ($class !== NULL)
			$c->classification = $class;
		$c->save();
		return true;
	}

	public function deleteById($id, $client) {
		if (empty($client)) return false;
		$username = $client->getLogin();
		$c = $this->getComment($id, $client);
		if (empty($c)) return false;
		$c->setUsername($username);
		$c->delete();
		return true;
	}

	public function reportAbuse($id, $client) {
		if (empty($client)) return false;
		$c = $this->getComment($id, $client);
		if (empty($c)) return false;
		
		$tech = new API_Tech();
		$tech->lookupID($c->TechID, null, true);
				
		$reportedBy = $client->getContactName() . " (" . $client->getLogin() . ")";
		$techCommentedOn = $tech->Firstname . " " . $tech->Lastname . " (" . $tech->TechID . ")";
		$commentType = self::getType($c->type);
		$commentDate = $c->date;
		$comment = $c->comment;
		
		$msg = "Reported by: $reportedBy\n";
		$msg .= "Tech: $techCommentedOn\n";
		$msg .= "Comment Type: $commentType\n";
		$msg .= "Comment Date: $commentDate\n";
		$msg .= "Comment: $comment\n";
		$mail = new Core_Mail();
		$mail->setFromName("Comments Report Abuse");
		$mail->setFromEmail("no-replies@fieldsolutions.com");
		$mail->setToName("");
		$mail->setToEmail("support@fieldsolutions.com");
		$mail->setSubject("Reported Abuse");
		$mail->setBodyText($msg);

		$mail->send();

		$c->reportAbuseByClientID = $id;
		$c->save();

		return true;
	}
	
	public function clearAbuse($id, $client) {
		$c = $this->getComment($id, $client);
		if (empty($c)) return false;
		$c->reportAbuseByClientID = new Zend_Db_Expr('NULL');
		$c->save();
		return true;
	}
	
	protected function getClient($id) {
		$client = new Core_Api_User;
		$user = $client->loadById($id);
		if (!$user) return false;
		return $user;
	}
	
	public static function getType($type) {
		switch ($type) {
			case self::TYPE_PRIVATE:
				return 'Private';
				break;
			case self::TYPE_CLIENT:
				return 'Company';
				break;
			case self::TYPE_PUBLIC:
				return 'Public';
				break;
			default:
				return 'Unknown';
		}
		return 'Unknown';
	}
    
    //13970
    public static function getNumComments_ForTech_andNonGPMClient($techID,
            $clientID,$companyID)
    {
        $db = Core_Database::getInstance();
        $select = $db->select();
        $select->from(array('c'=>'comments'),array("Count(*) AS NumComments"));
        $select->join(array('cli'=>'clients'),
                'cli.ClientID=c.clientID ',
                array()
        );
        $select->where("TechID = '$techID'");
        $select->where("c.type=2 OR (c.type=0 AND c.clientID='$clientID') OR (c.type=1 AND cli.Company_ID='$companyID') ");
        $records = Core_Database::fetchAll($select);
        
        $numComments = 0;
        if(!empty($records) && count($records)>0){
            $numComments = $records[0]['NumComments'];
        }
        return $numComments;        
    }
    
    //13970
    public static function getNumAllComments_forTech($techID)
    {
        $db = Core_Database::getInstance();
        $select = $db->select();
        $select->from('comments',array("Count(*) AS NumComments"));
        $select->where("TechID = '$techID'");
        $select->where("clientID IS NOT NULL");
        $records = Core_Database::fetchAll($select);
        $numComments = 0;
        if(!empty($records) && count($records)>0){
            $numComments = $records[0]['NumComments'];
        }
        return $numComments;        
    }
    
    //13970
    public function addComment($comment, $clientID, $techID, $companyID, $winNum = NULL, $type = self::TYPE_CLIENT, $class = self::CLASSIFICATION_FAVORABLE,$GeneratedByFSSystem=1) 
    {            
        $newComment = $this->createRow();
        $newComment->comment = $comment;
        $newComment->clientID = $clientID;
        $newComment->Company_ID = $companyID;
        $newComment->TechID = $techID;
        if(!empty($winNum)){
            $newComment->WIN_NUM = $winNum;            
        }
        $newComment->type = $type;
        $newComment->classification = $class;
        $newComment->GeneratedByFSSystem = $GeneratedByFSSystem;
        $newComment->date = date("Y-m-d H:i:s");
        $newComment->dateChanged = date("Y-m-d H:i:s");
        $newComment->save();
        return true;
    }
}
