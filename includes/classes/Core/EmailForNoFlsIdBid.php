<?php

class Core_EmailForNoFlsIdBid {
    private $tech;
    private $wo;
    private $project;
    private $companyId;
    private $companyName;
    private $mail;

    function __construct()
    {
        $this->mail = new Core_Mail();
    }
    
    public function setTech($tech)
    {
        if (is_object($tech)) {
        	$this->tech = $tech;
        } else {
            $this->tech = new Core_Tech((int)$tech);
        }
    }
        
    public function sendMail()
    {
//    	$FirstName = $this->tech->getFirstName();
//    	$LastName = $this->tech->getLastName();
//    	$sendToEmail = $this->tech->getPrimaryEmail();
        $techID = $this->tech->getTechID();
        $api = new Core_Api_TechClass;
        $techProperties = $api->getProperties($techID);

        $FirstName = $techProperties['FirstName']; 
        $LastName  = $techProperties['LastName'];
    	$sendToEmail = $this->tech->getPrimaryEmail();
    
    
    	$eList = $sendToEmail;
    	$vSubject = "Required FLS Online Training";	

    	$messageold = "Dear $FirstName $LastName,\n\n
You recently applied for one or more open work orders at the Technician
Bureau for our FLS client. Please be advised that in order to be eligible
to run calls and/or work any projects for FLS, you will need a Badge and
two FLS ID numbers. If you have no Badge and/or FLS ID numbers, it
indicates that you have not yet been through the required FLS online
training or you just recently went through the training, submitted the test
with a passing score and are currently waiting to be activated and/or
issued your FLS Badge and two FLS ID numbers.\n
\n 
If you have not yet been through the FLS online training to have a Badge
sent to you and two FLS ID numbers issued in your name, you will need
do so via the link below in order to be eligible for FLS. Thank you.\n
\n
Online training for FLS starts here: http://www.flsupport.com/3.html\n
\n
Regards,\n
\n
FieldSolutions\n
Web: www.fieldsolutions.com\n";

        $message = "Dear $FirstName $LastName,\n\n
You recently applied for one or more open work orders on FieldSolutions 
for our Fujitsu client. Please be advised that in order to be eligible 
to run calls and/or work any projects for Fujitsu, you will need to pass 
their certification test and be assigned an FLS ID #. If you do not have 
an FLS ID # that indicates you have not yet passed the FLS training or 
you have recently passed and are currently waiting to be activated and
issued an ID #.\n
\n 
If you have not yet been through the FLS online training please log in 
and click on the Fujitsu FLS link found either on the left side under 
Certifications or click on My Profile, Section 6: FS-TrustedTech (Credentials) 
and click the link for FLS. This will open the certification test and provide 
links to study guides and additional information.\n
\n 
Thank you, \n
\n 
Your FieldSolutions Team\n";

    	$this->mail->setBodyText($message);
        $this->mail->setFromEmail('support@fieldsolutions.com');
        $this->mail->setFromName('FLS Support');
        $this->mail->setToEmail($eList);
        $this->mail->setSubject('Required Fujitsu Online Training');
        
		$this->mail->setTech($this->tech);
		$this->mail->isTechEmail(true);
        $this->mail->send();            		
    }
}