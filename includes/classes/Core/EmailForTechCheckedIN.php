<?php

class Core_EmailForTechCheckedIN
{
    private $mail;

    function __construct()
    {
        $this->mail = new Core_Mail();
    }
            
    public function sendMail($win,$checkInDateTime='')
    {
        if (isset($win))
        {
            $apiclass = new Core_Api_Class();
            $apiProjectClass = new Core_Api_ProjectClass();
            
            $wo = $apiclass->getWorkOrdersWithWinNum($win, '', 'WIN_NUM', '');
            $wo = $wo[0];

            $owner = $wo['WorkOrderOwner'];
            $projectID =  $wo['Project_ID'];
            
            //--- get info from 'my-settings'
            $future = new Core_FutureWorkOrderInfo;
            $row = $future->find($owner)->toArray();
            $futureInfo = $row[0];
            //--- get info from project
            $projectInfo = $apiProjectClass->getProjectById($projectID);
            //--- 
            $mySetting_Email = '';
            if(!empty($futureInfo)){
                $mySetting_Email = $futureInfo['TechCheckedInEmailTo'];    
            }
            $receiveEmail = '';
            $prj_Email = '';
            $AllowCustomCommSettings=0;                        
            if(!empty($projectInfo)){
                $receiveEmail = $projectInfo['ReceiveTechCheckedInEmail'];
                $prj_Email = $projectInfo['TechCheckedInEmailTo'];
                $AllowCustomCommSettings = $projectInfo['AllowCustomCommSettings'];
            }
            
            if (!empty($wo) && !empty($receiveEmail))
            {
                $to='';
                if(!empty($mySetting_Email) && !empty($AllowCustomCommSettings)){
                    $to = $mySetting_Email;        
                } else {
                    $to = $prj_Email;        
                }

                if (!empty($to))
                {
                    $StartDate = "";
                    if(!empty($wo['StartDate']) && $wo['StartDate'] !="0000-00-00")  {
                        $strToTime = strtotime($wo['StartDate']);
                        if($strToTime != -1)
                            $StartDate = date("m/d/Y",$strToTime);
                    }
                    $Date_In = "";
                    if(!empty($wo['Date_In']) && $wo['Date_In'] !="0000-00-00") {
                        $strToTime = strtotime($wo['Date_In']);
                        if($strToTime != -1)
                            $Date_In = date("m/d/Y",$strToTime);                
                    }
                    
                    $fromName = "Tech Checked In - FieldSolutions";
                    $fromEmail = "no-replies@fieldsolutions.com";
                    $subject = "WIN# ".$wo['WIN_NUM'].": Work Order ID [".$wo['WO_ID']."]: Tech CHECKED IN";   
                             
                    $message = "<br/>";
                    $message .= "<p>The technician assigned to WIN# ".$wo['WIN_NUM']." Client WO ID# ".$wo['WO_ID']." has checked in.</p>";
                    $message .="<p>Project: " . $wo['Project_Name'] . "<br/>";
                    $message .="Headline: " . $wo['Headline'] . "<br/>";
                    $message .="Region: " . $wo['Region'] . "<br/>";
                    $message .="Route: " . $wo['Route'] . "<br/>";
                    $message .="Site Name: " . $wo['SiteName'] . "<br/>";
                    $message .="Site #: " . $wo['SiteNumber'] . "<br/>";
                    $message .="Site Address: " . $wo['Address'] . "<br/> " . $wo['City'] . ", " . $wo['State'] . " " . $wo['Zipcode'] . "<br/>";
                    $message .="Start Date/Time: " . $StartDate . " " . $wo['StartTime'] . "<br/>";
                    if(!empty($checkInDateTime)) {
                        $message .="Check In Time: " . $checkInDateTime . "<br/>";
                    } else {
                        $message .="Check In Time: " . $Date_In . " " . $wo['Time_In'] . "<br/>";
                    }
                    $message .="FS-Tech ID#: " . $wo['Tech_ID'] . "<br/>";
                    $message .="Tech Name: " . $wo['Tech_FName'] . " " . $wo['Tech_LName'] . "<br/>";
                    //868
                    $techInfo = Core_Tech::getProfile($wo['Tech_ID'], true, API_Tech::MODE_TECH);
                    $PrimaryPhone = $techInfo['PrimaryPhone'];
                    $SecondaryPhone = $techInfo['SecondaryPhone'];
                    $message .="Tech Phone (Day): " . $PrimaryPhone . "<br/>";
                    $message .="Tech Phone (Evening): " . $SecondaryPhone . "<br/>";
                    $message .="Tech Email: " . $wo['TechEmail'] . "</p>";
                    //end 868
                    
                    $message .="<p>Thank you,<br/>";
                    $message .="Your FieldSolutions Team</p>";
                    $message .= "<br/>";
                    
                    //echo("<br/>to: $to");echo("<br/>fromName: $fromName");echo("<br/>fromEmail: $fromEmail");echo("<br/>subject: $subject");print_r($message);//test
                    $mail = new Core_Mail();                
                    $mail->setBodyText($message);
                    $mail->setFromName($fromName);
                    $mail->setFromEmail($fromEmail);
                    $mail->setToEmail($to);
                    $mail->setSubject($subject);
                    $mail->send();
                }// end of  if (!empty($to))
            }
        }        
    }//end of if (isset($win))
}
