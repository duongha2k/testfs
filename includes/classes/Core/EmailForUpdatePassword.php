<?php

class Core_EmailForUpdatePassword {

    protected $tomail;
    protected $toname;

    function __construct()
    {
        $this->mail = new Core_Mail();
    }

    public function setToMail($tomail)
    {
        $this->tomail = $tomail;
    }

    public function setToName($toname)
    {
        $this->toname = $toname;
    }

    public function sendMailtoAdmin($adminUserName)
    {
        $AdminUser = new Core_Api_AdminUser();
        $AdminInfo = $AdminUser->getAdminInfo($adminUserName);
        $FromName = "FieldSolutions Support";
        $FromEmail = "support@fieldsolutions.com";
        $ToName = $AdminInfo['Fname'] .' ' . $AdminInfo['Lname'];
        $ToEmail = $AdminInfo['Email'];
        
        $subject = "FieldSolutions Password Updated &ndash; Admin";
        $message = "<p>Your FieldSolutions Log In password has been changed. </p><p> If you are not aware of your password being changed recently, please contact Support at <a hreft='mailto:support@fieldsolutions.com'>support@fieldsolutions.com</a> </p>
            
        <p>Thank You, Your FieldSolutions Team</p>";

        $this->sendmail($FromName, $FromEmail, $ToName, $ToEmail, $subject, $message);
    }

    public function sendMailtoClientNotUpdatePassword()
    {
        $FromEmail = "support@fieldsolutions.com";
        $FromName = "FieldSolutions Support";
        $subject = "Time to Refresh Your FieldSolutions Password";
        $message = "<p>For your company&rsquo;s security, FieldSolutions is now requiring periodic password updates.</p><p> Please log in at <a href='http://www.fieldsolutions.com'>http://www.fieldsolutions.com/</a> and follow the system prompts to refresh your password. </p>
�      <p>
        Thank you for your attention to this matter,
�
        Your FieldSolutions Team</p>
        
        <img src='http://".$_SERVER["SERVER_NAME"]."/wp/wp-content/themes/fieldsolutions/img/fieldSolutionsLogo.jpg'>
        ";

        $this->sendmail($FromName, $FromEmail, $this->toname, $this->tomail, $subject, $message);
    }

    public function sendMailtoClient($clientID)
    {
        $Class = new Core_Api_Class();
        $clientinfo = $Class->getClientByID($clientID);

        $FromName = "FieldSolutions Support";
        $FromEmail = "support@fieldsolutions.com";
        $ToName = $clientinfo['ContactName'];
        $ToEmail = $clientinfo['Email1'];
        $subject = "FieldSolutions Password Updated";
        $message = "<p>Your FieldSolutions Log In password has been changed. </p><p> If you are not aware of your password being changed recently, or are unable to log in to FieldSolutions, please contact your FieldSolutions Client Services Director (for Contact Information, visit the Client Profile tab of your Settings page) or Support at <a hreft='mailto:support@fieldsolutions.com'>support@fieldsolutions.com</a>. </p> 
�       <p>
        Thank you,
�
        Your FieldSolutions Team</p>
        
        <img src='http://".$_SERVER["SERVER_NAME"]."/wp/wp-content/themes/fieldsolutions/img/fieldSolutionsLogo.jpg'>
        ";

        $this->sendmail($FromName, $FromEmail, $ToName, $ToEmail, $subject, $message);
    }

    public function sendMailtoTech($techID)
    {
        $WosClass = new Core_Api_WosClass();
        $TechInfo = $WosClass->getTechInfo($techID);

        $FromName = "FieldSolutions Support";
        $FromEmail = "support@fieldsolutions.com";
        $ToName = $TechInfo['FirstName'] .' ' . $TechInfo['LastName'];
        $ToEmail = $TechInfo['PrimaryEmail'];
        
        $subject = "FieldSolutions Password Updated";
        $message = "<p>Your FieldSolutions Log In password has been changed. </p><p> If you are not aware of your password being changed recently or are unable to log-in, please contact the FieldSolutions Technician Resource Center at <a hreft='mailto:support@fieldsolutions.com'>support@fieldsolutions.com</a>. </p>
�     <p>
        Thank you,
�
        Your FieldSolutions Team</p>
        
        <img src='http://".$_SERVER["SERVER_NAME"]."/wp/wp-content/themes/fieldsolutions/img/fieldSolutionsLogo.jpg'>
        ";
        $this->sendmail($FromName, $FromEmail, $ToName, $ToEmail, $subject, $message);
    }

    public function sendmail($FromName, $FromEmail, $ToName, $ToMail, $subject, $message)
    {
        $this->mail->setFromName($FromName);
        $this->mail->setFromEmail($FromEmail);
        $this->mail->setToName($ToName);
        $this->mail->setToEmail($ToMail);
        $this->mail->setSubject($subject);
        $this->mail->setBodyText($message);
        $this->mail->send();
    }

}

