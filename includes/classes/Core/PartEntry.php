<?php
// class to create / get Part Entries
class Core_PartEntry
{
	const ACTION_CREATE = 1;
	const ACTION_UPDATE = 2;
	const ACTION_DELETE = 3;
	protected static $testMode = false;
	protected $data = array();
	
	function __construct($entry = null, $winNum = null, $company = null, $tech = null)
	{
		if (!empty($entry)) {
			if (is_array($entry) && array_key_exists(0, $entry)) {
				if (is_array($entry[0]))
					$this->data = $entry;
				else if (is_numeric($entry[0]))
					$this->setData($entry, $winNum, $company, $tech);
			}
		}
		else if (is_numeric($winNum)) {
			$this->setData(null, $winNum, $company, $tech);
		}
	}

	private function setData($ids, $winNum, $company = null, $tech = null)
	{
//		$fields = Core_Database::getFieldList(Core_Database::TABLE_PARTS);
		if (empty($winNum)) return false;
		$db = Core_Database::getInstance();
		$select = $db->select()
			->from(array('w' => Core_Database::TABLE_WORK_ORDERS), array() )
			->join(array('pe' => Core_Database::TABLE_PART_ENTRIES), (self::$testMode ? "(`w`.WIN_NUM - 1000000)" : "`w`.WIN_NUM") . " = `pe`.WinNum", "Consumable")
			->join(array('p' => Core_Database::TABLE_PARTS), "`p`.PartEntryID = `pe`.id")
			->where('`w`.`WIN_NUM` = ?', array($winNum))
			->where('`pe`.`Deleted` = 0')
			//->where('`p`.IsNew = 1 OR `pe`.Consumable = 0')
			->order(array('PartEntryID ASC', 'IsNew DESC'));

		if (!empty($ids))
			$select->where('`p`.`PartEntryID` IN (?)', $ids);
		if (!empty($company))
			$select->where('`w`.Company_ID = ?', array($company));
		if (!empty($tech))
			$select->where('`w`.Tech_ID = ?', array($tech));

		$entry = Core_Database::fetchAll($select);

		$this->data = $entry;

		return true;
	}

	protected static function actionPart($data, $partEntryID, $action) {
		$errors = Core_Api_Error::getInstance();

		if (!is_numeric($partEntryID)) return;
		if ($data instanceof API_NewPart)
			$isNew = 1;
		else if ($data instanceof API_ReturnPart) 
			$isNew = 0;
		else 
			return false;
		
		$data->ETA = empty($data->ETA) ? NULL : $data->ETA;

		$values = array();
		foreach ($data as $field => $val) {
			if ($val == NULL) continue;
			$values[$field] = $val;
		}
		
		$values["IsNew"] = $isNew;
		
		$db = Core_Database::getInstance();

		switch ($action) {
			case self::ACTION_UPDATE:
				$crit = $db->quoteInto("PartEntryID = ?", $partEntryID) . " AND " . $db->quoteInto("IsNew = ?", $isNew);
				$result = Core_Database::update(Core_Database::TABLE_PARTS, $values, $crit);
				if ($result == 0) {
					$select = $db->select();
					$select->from(Core_Database::TABLE_PARTS, array("id"))
                                                ->where($crit);
                                        $result = Core_Database::fetchAll($select);
					if (is_array($result) && sizeof($result) == 0) {
						$values["PartEntryID"] = $partEntryID;
						$result = Core_Database::insert(Core_Database::TABLE_PARTS, $values);
					}
				}
				break;
			default:
				// create New Part
				$values["PartEntryID"] = $partEntryID;
				$result = Core_Database::insert(Core_Database::TABLE_PARTS, $values);
				break;
		}

		$err = $errors->getErrors();
		if (!empty($err))
			return FALSE;

		return TRUE;
	}

	protected static function action(API_PartEntry $entry, $winNum, $action) {
		if (!is_numeric($winNum)) return false;
		if (!Core_WorkOrder::workOrderExists($winNum)) return false;
		$errors = Core_Api_Error::getInstance();
		$consumable = $entry->Consumable === true || $entry->Consumable == "True";
		$newPart = null;
		// validating parts
		if (!empty($entry->newPart) && $entry->newPart instanceof API_NewPart) {
			$newPart = $entry->newPart;
   		}
		$returnPart = null;
		if (!empty($entry->returnPart) && $entry->returnPart instanceof API_ReturnPart) {
			$returnPart = $entry->returnPart;
		}

		$db = Core_Database::getInstance();
		$partEntryID = $entry->id;

		switch ($action) {
			case self::ACTION_DELETE:
				$crit = "Deleted = 0 AND " . $db->quoteInto("WinNum = ?", $winNum) . " AND " . $db->quoteInto("id = ?", $partEntryID);
				$result = Core_Database::update(Core_Database::TABLE_PART_ENTRIES, array("Deleted" => 1), $crit);
				if ($result == 0) {
					$select = $db->select();
					$select->from(Core_Database::TABLE_PART_ENTRIES, array("id"))
						->where($crit);
					$result = Core_Database::fetchAll($select);
					if (is_array($result) && sizeof($result) == 0) {
						$errors->addError(7, "WinNum $winNum or Part ID $partEntryID does not exist");
						return false;
					}
				}
				else {
					// update part count
					self::updatePartCount($winNum);
					return true;
				}
				break;
			case self::ACTION_UPDATE:
				$crit = "Deleted = 0 AND " . $db->quoteInto("WinNum = ?", $winNum) . " AND " . $db->quoteInto("id = ?", $partEntryID);
				$result = Core_Database::update(Core_Database::TABLE_PART_ENTRIES, array("Consumable" => $consumable),$crit);

				if ($result == 0) {
					$select = $db->select();
					$select->from(Core_Database::TABLE_PART_ENTRIES, array("id"))
						->where($crit);
					$result = Core_Database::fetchAll($select);
					if (is_array($result) && sizeof($result) == 0) {
						$errors->addError(2, "PartEntryID $partEntryID does not exist");
						return false;
					}
				}
				break;
			default:
				// create Part Entry
				$result = Core_Database::insert(Core_Database::TABLE_PART_ENTRIES, array("WinNum" => $winNum, "Consumable" => $consumable));
				$partEntryID = $db->lastInsertId(Core_Database::TABLE_PART_ENTRIES);
				break;
		}

		$err = $errors->getErrors();
		if (!empty($err))
			return false;

		$failed = false;

		if ($action != self::ACTION_DELETE) {
			if ($newPart != NULL) {
				$newPart->PartEntryID = $partEntryID;
				$failed = !(self::actionPart($newPart, $partEntryID, $action));
			}
			if ($returnPart != NULL) {
				// create Return Part
				$returnPart->PartEntryID = $partEntryID;
				$failed = !(self::actionPart($returnPart, $partEntryID, $action));
			}
		}

		// update part count
		self::updatePartCount($winNum);

		if ($action == self::ACTION_CREATE)
			return $partEntryID;

		return !$failed;
	}

	public static function create(API_PartEntry $entry, $winNum) {
		return self::action($entry, $winNum, self::ACTION_CREATE);
	}

	public static function update(API_PartEntry $entry, $winNum) {
		return self::action($entry, $winNum, self::ACTION_UPDATE);
	}

	public static function delete(API_PartEntry $entry, $winNum) {
		return self::action($entry, $winNum, self::ACTION_DELETE);
	}
	
	protected function updatePartCount($winNum) {
		if (!is_numeric($winNum)) return false;
		$winNum = (self::$testMode ? $winNum - 1000000 : $winNum);
		$db = Core_Database::getInstance();
		$fieldMap = array(
//			"NewCount" => "TOP 1 (SELECT COUNT(id) FROM " . TABLE_PARTS . " WHERE IsNew = '1' AND PartEntryID IN ((SELECT id FROM " . TABLE_PART_ENTRIES. " WHERE WinNum = '$winNum' AND Deleted = '0')))", 
			"NewCount" => "COUNT((CASE WHEN `p`.IsNew = 1 THEN `pe`.id ELSE NULL END))", 
//		"ReturnCount"=> "(SELECT COUNT(id) FROM " . TABLE_PARTS . " WHERE IsNew = '0' AND PartEntryID IN ((SELECT id FROM " . TABLE_PART_ENTRIES . " WHERE WinNum = '$winNum' AND Deleted = '0' AND Consumable = '0')))");
			"ReturnCount"=> "COUNT((CASE WHEN `p`.IsNew = 0 THEN `pe`.id ELSE NULL END))");
		$select = $db->select()
				->from(array('pe' => Core_Database::TABLE_PART_ENTRIES), $fieldMap)
				->join(array('p' => Core_Database::TABLE_PARTS), "`p`.PartEntryID = `pe`.id", array())
				->where('`p`.IsNew = 1 OR `pe`.Consumable = 0')
				->where("WinNum = ? AND Deleted = 0", array($winNum));

		//echo $select;

		$result = Core_Database::fetchAll($select);

		if ($result && $result[0]) {
			$newCount = $result[0]["NewCount"];
			$returnCount = $result[0]["ReturnCount"];
			$winNum = (self::$testMode ? $winNum + 1000000 : $winNum);
			try {
				$result = $db->update(Core_Database::TABLE_WORK_ORDERS, array("NewPartCount" => $newCount,"ReturnPartCount" => $returnCount), "WIN_NUM = $winNum");
			} catch (Exception $e) {
				$result = false;
			}
		}

		return $result;
	}
	
	public function toAPI_PartEntry() {
		if (empty($this->data[0])) return array();
		$parts = array();
		$entryList = array();

		foreach ($this->data as $key=>$value) {
			if (!array_key_exists($value['PartEntryID'], $entryList)) {
				// New Part Entry
				$entryList[$value['PartEntryID']] = array();
			}
			$entryList[$value['PartEntryID']][] = $value;
		}

		foreach ($entryList as $key=>$value) {
			foreach ($value as $k => &$part) {
				foreach ($part as $p => $v) {
					if ($p == "IsNew" || $p == "Consumable") {
						$part[$p] = $part[$p] == 0 ? "False" : "True";
					}
				}
			}
			if (isset($value[1]["Consumable"])) unset($value[1]["Consumable"]);
			$part = API_PartEntry::partEntryWithResult($value);
			$parts[] = $part;
		}
		return $parts;
	}
}
