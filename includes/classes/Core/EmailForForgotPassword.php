<?php

class Core_EmailForForgotPassword
{

    private $type;
    private $name;
    private $code;

    function __construct()
    {
        $this->mail = new Core_Mail();
    }

    public function setClientType($type)
    {
        $this->type = $type;
    }

    public function setContactName($name)
    {
        $this->contactName = $name;
    }

    public function setCode($code)
    {
        $this->code = $code;
    }

    public function sendMail($to)
    {
        $vFromName = "FieldSolutions";
	    $vFromEmail = "no-replies@fieldsolutions.us";
        $eList = $to;
    	$caller = "fsForgotPassword";
    	$vSubject = "FieldSolutions Password Retrieval";

		$message = "
            Dear {$this->contactName}, \n\n
            Please follow this link to reset your password. The link will expire tonight at midnight CST. \n\n
            \n\n
            http://www.fieldsolutions.com/misc/pw_validate.php?email={$to}&code={$this->code}&type={$this->type} \n\n
            \n\n
            Thank you, \n\n
            Your FieldSolutions Team
        ";

    	$this->mail->setBodyText($message);
        $this->mail->setFromEmail($vFromEmail);
        $this->mail->setFromName($vFromName);
        $this->mail->setToEmail($eList);
        $this->mail->setToName($this->contactName);
        $this->mail->setSubject($vSubject);

        $this->mail->send();
    }
}