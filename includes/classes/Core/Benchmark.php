<?php
class Core_Benchmark
{
    static private $_instance = null;

    private $_points    = array();
    private $_do_bench  = true;
    private $_adapter   = 'File';

    /**
     * getInstance  
     * 
     * @param string $message
     * @static
     * @access public
     * @return void
     */
    static public function getInstance($message = null)
    {
        if ( self::$_instance === null ) {
            self::$_instance = new self($message);
        }

        return self::$_instance;
    }
    /**
     * __construct  
     * 
     * @param string $message
     * @access private
     * @return void
     */
    private function __construct($message)
    {
        $this->_do_bench = defined('APPLICATION_ENV') && APPLICATION_ENV !== 'production';
        $this->point(($message === NULL) ? 'Start benchmark' : $message);
    }
    /**
     * __destruct  
     * 
     * @access public
     * @return void
     */
    public function __destruct()
    {
        self::$_instance = null;
        if ( !$this->_do_bench )
            return;

        $this->point('End benchmark');
        $this->makeResult();
    }
    /**
     * __clone  
     * 
     * @access public
     * @return void
     * @depricated
     */
    public function __clone()
    {
        trigger_error("This object is 'Singleton' and you can't clone it");
    }
    /**
     * point  
     * 
     * @param string $message 
     * @access public
     * @return Core_Benchmark
     */
    public function point($message = 'Message is empty')
    {
        if ( !$this->_do_bench )
            return;

        $this->_points[] = array('message' => $message, 'point' => microtime(false));
        return $this;
    }
    /**
     * makeResult
     * 
     * @access public
     * @return void
     */
    public function makeResult()
    {
        if ( !$this->_do_bench ) return;

        Core_Benchmark_Adapter_Factory::create($this->_adapter)
            ->setData($this->_points)
            ->result();
    }
    /**
     * setAdapter
     * 
     * @param Core_Benchmark_Adapter_Interface|string $adapter
     * @access public
     * @return void
     */
    public function setAdapter($adapter)
    {
        if ( empty($adapter) ) {
            $adapter = 'File';
        }
        $this->adapter = $adapter;
        return $this;
    }
}

