<?php
class Core_IVR_Redial
{
	/**
	 * New call. Create entry in ivr_status or if it exists
	 * increase RedialCount.
	 * @param int $winNum WIN_NUM of work order
	 * @param string $callType call type
	 */
	public static function newCallAttempt($winNum, $callType)
	{
		$db = Zend_Registry::get('DB');

		$sql = "INSERT ivr_status(WIN_NUM, CallType, LastCalledTime, RedialCount) VALUES (?,?,NOW(),1)"
		. " ON DUPLICATE KEY UPDATE LastCalledTime=NOW(), RedialCount=RedialCount+1";

		$db->query($sql, array($winNum, $callType));
	}

	/**
	 * Mark ivr_status entry as success.
	 * @param int $winNum WIN_NUM of work order
	 * @param string $callType call type
	 */
	public static function markSuccess($winNum, $callType)
	{
		$db = Zend_Registry::get('DB');

		$sql = "UPDATE ivr_status SET Success=1 WHERE WIN_NUM=? AND CallType=?";
		$db->query($sql, array($winNum, $callType));
	}

	/**
	 * Reset ACS calls.
	 * @param int $winNum WIN_NUM of work order
	 */
	public static function resetCalls($winNum)
	{
		$db = Zend_Registry::get('DB');

		$db->delete('ivr_status', $db->quoteInto('WIN_NUM=?', $winNum));
	}
}
