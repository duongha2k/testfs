<?php
class Core_IVR_Log
{
	/**
	 * Log a call
	 */
	public static function create($woUNID, $techID, $callType, $parentCall = 0)
	{
		Core_IVR_Redial::newCallAttempt($woUNID, $callType);
		$db = Zend_Registry::get('DB');
		$result = $db->insert(Core_Database::TABLE_IVR_LOG, array(
			'WOUNID' => $woUNID,
			'TechID' => $techID,
			'CallType' => $callType,
			'CalledTime' => new Zend_Db_Expr('UTC_TIMESTAMP()'),
			'Result' => 'Pending',
			'CallLengthSeconds' => 0,
			'ParentCall' => $parentCall
		));
		
		if (!$result) return false;
		$id = $db->lastInsertId(Core_Database::TABLE_WORK_ORDERS);
		
		if ($parentCall == 0) {
			// update ParentCall for parent
			self::update($id, 'Pending', 0, $techID, '', $id);
		}
		
		return $id;
	}

	/**
	 * update log
	 */
	public static function update($logID, $result, $length, $techID = "", $WOUNID = "", $parentCall = "")
	{
		$db = Zend_Registry::get('DB');
		$data = array(
			'Result' => $result,
			'CallLengthSeconds' => $length
		);
		
		if ($techID != '')
			$data['TechID'] = $techID;
		if ($WOUNID != '')
			$data['WOUNID'] = $WOUNID;
		if ($parentCall != '')
			$data['ParentCall'] = $parentCall;			
			
		$db->update(Core_Database::TABLE_IVR_LOG, $data, $db->quoteInto('id = ?', $logID));

		if (strpos($result, 'Success') !== 0) return true;
		
		$select = $db->select();
		$select->from(Core_Database::TABLE_IVR_LOG, array('WOUNID', 'CallType'))->where('id = ?', $logID);
		$call = $db->fetchAll($select);
		$WOUNID = $call[0]['WOUNID'];
		$callType = $call[0]['CallType'];
		
		Core_IVR_Redial::markSuccess($WOUNID, $callType);
		
		return true;
	}

	/**
	 * Reset ACS calls.
	 * @param int $winNum WIN_NUM of work order
	 */
	public static function resetCalls($winNum)
	{
		$db = Zend_Registry::get('DB');
		$data = array(
			'Old' => 1,
		);
		
		$result = $db->update(Core_Database::TABLE_IVR_LOG, $data, $db->quoteInto('WOUNID = ?', $winNum));
	}
}
