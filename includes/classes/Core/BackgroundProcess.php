<?php
class Core_BackgroundProcess {
	public static function run($cmd, $params = null) {
		$cmd = $cmd . " ";
		if (!empty($params) && is_array($params)) {
			foreach ($params as $v) {
				$cmd .= " " . escapeshellarg($v);
			}
		}
		$cmd = "(" . $cmd . ") > /dev/null 2>&1 &";
		exec($cmd);
	}
}
