<?php
class Core_Caspio_ExceptionConverter
{
    private $message;
    // Used for columnValidate
    private $invalidColumn = null;
    
    public function __construct($message = null)
    {
        if ($message !== null) {
            $this->message = $message;
        }
        return $this;
    }
    
    public function process()
    {
        if (strpos($this->message, 'SQL Server error') !== false) {
            if (strpos($this->message, 'Violation of UNIQUE KEY constraint') !== false) {
                return $this->uniqueKey();
            }  elseif (strpos($this->message, 'Invalid column name') !== false) {
                return $this->invalidColumn();
            }  
        }
                
        $patterns = array(
                    '#^{|}$#',
                    '#SQL Server error(:)?\s*#');
        $replace = array(
                            '');
        $this->message = preg_replace($patterns, $replace, $this->message);
        
        return $this->message;
    } 
    
    private function uniqueKey()
    {
        if (preg_match('#(?:Violation of UNIQUE KEY constraint).+?(\'.+?\').+?#i', $this->message, $match)) {
            if (!empty($match[1])) {
                $match[1] = trim($match[1], '\'');
                $field = preg_replace('#^\w+\.#', '', $match[1]);
                $result = 'The duplicate value for the field  "'.$field.'"'; 
                return $result;    
            }            
        }
        return $this->message;   
    } 
    
    private function invalidColumn()
    {
        if (preg_match('#(?:Invalid column name).\s*(\'.+?\')#i', $this->message, $match)) {
            if (!empty($match[1])) {
                $match[1] = trim($match[1], '\'');
                $result = 'Invalid field name "'.$match[1].'"';
                return $result;     
            }
        }
        
        return $this->message;
    }
    
    /**
    * Validate if provided columns exists in the table
    * @param $table string table name
    * @param $fields array fields to validate
    * @return bool 
    */
    public function columnValidate($table, array $fields)
    {
        $fieldsInTable = Core_Database::getFieldList($table);     
        foreach ($fields as &$val) {
            if (!in_array($val, $fieldsInTable)) {
                $this->invalidColumn = $val;
                return false;
            }    
        }
        return true;
    }
    
    public function getNotValidColumn()
    {
        return $this->invalidColumn;
    }  
}