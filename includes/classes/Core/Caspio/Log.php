<?php
class Core_Caspio_Log {
	private $caspioService;
	private $logFile;

	public function __construct($caspioService, $logFile)
	{
		$this->caspioService = $caspioService;
		$this->logFile = $logFile;
	}

	public function __call($name, $arguments)
	{
		$before = microtime(true);
		$return = call_user_func_array(array($this->caspioService, $name), $arguments);
		$time = microtime(true) - $before;

		$h = fopen($this->logFile, 'a');
		fwrite($h, date('Y-m-d H:i:s', $before) . " Caspio function $name\nArguments:\n" . var_export($arguments, true) . "\nTime: $time miliseconds\n");
		fclose($h);
		return $return;
	}
}
