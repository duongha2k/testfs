<?php

class Core_TimeStamp
{
    /**
     * Timestamps processing
     */

    public static function createTimeStamp($woID, $userName, $Description, $companyID, $clientName, $projectName, $customerName, $TechID, $TechName) {
		$db = Core_Database::getInstance();
		try {
			$dateTimeStamp = date("Y-m-d H:i:s");
			$result = $db->insert(Core_Database::TABLE_TIMESTAMPS, array(
																		 "WIN_NUM" => $woID,
																		 "DateTime_Stamp" => $dateTimeStamp,
																		 "Username" => $userName,
																		 "Description" => $Description,
																		 "Customer_Name" => $customerName,
																		 "Tech_ID" => $TechID,
																		 "Tech_Name" => $TechName
																		 ));
		} catch (Exception $e) {
		}
	}

	public static function createWOACSTimeStamp($id, $companyId) {
		if (!is_numeric($id)) return;

		$lib_path = realpath(dirname(__FILE__) . "/../../../") . "/htdocs/library/";
		exec("(/usr/bin/php -f " . $lib_path . "woACSTimeStampBG.php " . escapeshellarg($id) . ") > /dev/null 2>&1 &");
	}

    public static function createProjectTimeStamp($prj_id,$user,$pass) {
        $prjObj = new Core_Api_ProjectClass();
        $prj    = $prjObj->getProjects($user, $pass, $prj_id);
        if(!$prj->success || empty($prj->data[0])) return;
        $prj    = $prj->data[0];
        if ( strrpos($user, '|')) {
            $user = substr($user,0,strrpos($user, '|'));
        }
		$fieldList = array("DateChanged", "Project_ID", "Project_Name", "UserName", "ReminderAll",
            "ReminderAcceptance", "Reminder24Hr", "Reminder1Hr", "CheckInCall", "CheckOutCall",
            "ReminderNotMarkComplete", "ReminderIncomplete", "Company_ID", "SMSBlast", "ApplyTo", "PcntDeduct",'ReminderCustomHr','ReminderCustomHrChecked','ReminderCustomHr_2','ReminderCustomHrChecked_2','ReminderCustomHr_3','ReminderCustomHrChecked_3');
        $valuesList = array(new Zend_Db_Expr('NOW()'), $prj_id, $prj->Project_Name, $user, $prj->ReminderAll,
            $prj->ReminderAcceptance,$prj->Reminder24Hr,$prj->Reminder1Hr,$prj->CheckInCall,$prj->CheckOutCall,
            $prj->ReminderNotMarkComplete,$prj->ReminderIncomplete,$prj->Project_Company_ID,$prj->SMSBlast,$prj->ACSApplyTo,$prj->PcntDeduct,$prj->ReminderCustomHr,$prj->ReminderCustomHrChecked,$prj->ReminderCustomHr_2,$prj->ReminderCustomHrChecked_2,$prj->ReminderCustomHr_3,$prj->ReminderCustomHrChecked_3);

		$toInsert = array_combine($fieldList, $valuesList);
            
        if (!empty($toInsert)) {
            $notNull = array("ReminderAll", "ReminderAcceptance", "Reminder24Hr", "Reminder1Hr", "CheckInCall", "CheckOutCall", "ReminderNotMarkComplete", "ReminderIncomplete", "SMSBlast", "ApplyTo", "PcntDeduct");
            foreach ($notNull as $key) {
                if (is_null($toInsert[$key]) ) {
                    $toInsert[$key] = 0;
                }
            }
            $db = Zend_Registry::get('DB');
		    $db->insert('ProjectACSChangeLog', $toInsert);
		}
        
		return array("projectName" => $prj->Project_Name, "reminderAll" => $prj->ReminderAll, "reminderAcceptance" => $prj->ReminderAcceptance, "reminder24Hr" => $prj->Reminder24Hr, "reminder1Hr" => $prj->Reminder1Hr, "checkInCall" => $prj->CheckInCall, "checkOutCall" => $prj->CheckOutCall, "reminderNotMarkComplete" => $prj->ReminderNotMarkComplete, "reminderIncomplete" => $prj->ReminderIncomplete, "SMSBlast" => $prj->SMSBlast, "applyTo" => $prj->ACSApplyTo, "pcntDeduct" => $prj->PcntDeduct );		
    }

}
