<?php

class Core_Image
{
    /**
     * Map file extensions to image type
     * @var array
     */
    static private $_fileExtToType = array(
        'bmp' => 'bmp',
        'jpeg' => 'jpeg',
        'jpg' => 'jpeg',
        'jpe' => 'jpeg',
        'gif' => 'gif',
        'png' => 'png',
        'tif' => 'tiff',
        'tiff' => 'tiff'
    
    );
    
    /**
     * Map image type to mime type
     * @var array
     */
    static private $_imgTypeToMime = array(
        'bmp' => 'image/bmp',
        'jpeg' => 'image/jpeg',
        'gif' => 'image/gif',
        'png' => 'image/png',
        'tiff' => 'image/tiff'
    );
    
    
    /**
     * Get image type by file name
     * 
     * @param string $filePath
     * @return multitype:string |NULL
     */
    static public function getImageTypeByFileName($filePath)
    {
        $info = pathinfo($filePath);
        
        if (!empty($info['extension'])) {
            if (isset(self::$_fileExtToType[mb_strtolower($info['extension']) ] ) ) {
                return self::$_fileExtToType[mb_strtolower($info['extension']) ];
            }
        }
        return null;
    }   
    
    
    /**
     * Get mime name by image type
     * 
     * @param string $fileType
     * @return multitype:string |NULL
     */
    static public function getMimeByImgType($fileType)
    {
        if (isset(self::$_imgTypeToMime[$fileType]) ) {
            return self::$_imgTypeToMime[$fileType];
        } else {
            return null;
        }
    }
    
    
    /**
     * Check. Is image?
     * 
     * @param string $filePath
     * @return boolean
     */
    static public function isImage($filePath)
    {
        return (self::getImageTypeByFileName($filePath) === null) ? false : true;        
    }
    
}