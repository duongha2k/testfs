<?php
class Core_FSPlusOne
{
   
   const WORK_ORDER_APPROVED = 1;
   const WORK_ORDER_SUPERIOR_RATING = 2;
   const WORK_ORDER_NO_SHOW_NO_CALL = 3;
   const WORK_ORDER_BACK_OUT_CALL = 4;
   const ENROLLMENT_BONUS = 5;
   const CERT_COMP_TIA = 6;
   const CERT_BACKGROUND_CHECK = 7;
   const FS_MARKETING_REGISTRATION = 8;
   const REF_CLIENT_PURCHASE_10_WO = 9;
  
   
	public static function isEligible($fsPlusOneID){
    	if(empty($fsPlusOneID))
    		return false;
    		
    	$fsID = preg_replace("/[^0-9]/", '', $fsPlusOneID);
    	
    	if($fsPlusOneID[0] == "T"){
    		$userType = "tech";
    		
    		$db = Core_Database::getInstance();
    		$select = $db->select()
				->from(Core_Database::TABLE_TECH_BANK_INFO, array("TechID", "W9", "AcceptTerms", "Deactivated", "LastName", "FSPlusOneReg"))
				->where('TechID = "'.$fsID.'"');
				
			$result = Core_Database::fetchAll($select);

			$returnMessage = array();
		
    		if ($result && $result[0]) {
			
			/*
			if($result[0]["W9"] != "1")
				$returnMessage['errors'][] = "No W9 on File";
			*/
			
				if(strtoupper($result[0]["AcceptTerms"]) != "YES" || $result[0]["Deactivated"] != "0")
					$returnMessage['errors'][] = "Not in good standing with Independent Contractor Agreement Accepted";
				
				
    			if(empty($returnMessage['errors'])){
    				$returnMessage['usertype'] = $userType;
    				$returnMessage['FS_ID'] = $result[0]['TechID'];
    				$returnMessage['userLastName'] = $result[0]['LastName'];
    				$returnMessage['W9'] = $result[0]['W9'];
					return $returnMessage;
				}else{
					return $returnMessage;
				}
			}
    	}else{
    		$userType = "client";
    	}
    }
  

    public static function registerTech($fsPlusOneID){
    	
   		if(empty($fsPlusOneID))
    		return false;
    	
		$isEligible = self::isEligible($fsPlusOneID);
    	$date = date("Y-m-d H:i:s");
    	 
		$returnMessage = array();
		
		if(empty($isEligible['errors'])){
			try {
				//$db = Core_Database::getInstance();
				//$result = $db->update(Core_Database::TABLE_TECH_BANK_INFO, array("FSPlusOneReg" => true,"FSPlusOneDateReg" => $date), "TechID = '".$isEligible["FS_ID"]."'");
				
				
				//Use Caspio Update. It will update record in caspio and then update record in mysql
				$result = Core_Caspio::caspioUpdate("TR_Master_List", array("FSPlusOneReg","FSPlusOneDateReg"), array("1", $date), "TechID = '".$isEligible["FS_ID"]."'");
				if($result){
					self::applyPoints(self::ENROLLMENT_BONUS, $isEligible['FS_ID'], $isEligible['userLastName'],  $isEligible['usertype']);
					$returnMessage['success'][] = $result;
				}else{
					error_log("FSPLUSONE SIGNUP ERROR: ".$result)
;				}
			} catch (Exception $e) {
				$returnMessage['errors'][] = $e;
			}
		}else{
			$returnMessage = $isEligible;
			return $returnMessage;
		}
    }
    
    public static function applyPoints($action, $fsID, $userLastName, $userType, $WIN_NUM = ""){
    	if(empty($fsID) || empty($action))
    		return false;
    	
    	$db = Core_Database::getInstance();
    	
    	$select = $db->select()
    			->from("TechBankInfo", array("FSPlusOneReg", "LastName"))
    			->where("TechID = '".$fsID."'");
    				
    	$result = Core_Database::fetchAll($select);
    	
    	if($result && $result[0]){
    		if($result[0]['FSPlusOneReg'] != "1")
    			return false;
    	}
    	
    	$userLastName = $result[0]['LastName'];
    	
    	if($userType == "tech"){
    		$fsPlusOneID = "T".$fsID.$userLastName;
    	}
    	
    	$isEligible = self::isEligible($fsPlusOneID);
    	
    	if(empty($isEligible['errors'])){
    		//get point amount
    		$select = $db->select()
    				->from("FSPlusOneActions", "points")
    				->where("id = '".$action."'");
    				
    		$result = Core_Database::fetchAll($select);
    		
    		if($result && $result[0]){
    			$pointAmount = $result[0]["points"];
    		}
    				
    		switch($action){
    		
    			case self::WORK_ORDER_APPROVED:
    			case self::WORK_ORDER_SUPERIOR_RATING:
    			case self::WORK_ORDER_NO_SHOW_NO_CALL:
    			case self::WORK_ORDER_BACK_OUT_CALL:
    				if(empty($WIN_NUM))
    					return false;
    				
    				$select = $db->select()
    						->from("FSPlusOnePoints", "id")
    						->where('FS_ID = "'.$fsID.'"')
    						->where('actionID = "'.$action.'"')
    						->where('WIN_NUM = "'.$WIN_NUM.'"');
    				
    				$result = Core_Database::fetchAll($select);
    				
    				if($result && $result[0]){
    					return false;
    				}else{
    					$date = date("Y-m-d H:i:s");
    					$result = Core_Database::insert("FSPlusOnePoints", array("FSPlusOneID" => $fsPlusOneID, "actionID" => $action, "WIN_NUM" => $WIN_NUM, "points" => $pointAmount, "applied"=> "0", "FS_ID" => $isEligible['FS_ID'], "usertype" => "tech", "W9" => $isEligible['W9'],  "dateCreated" => $date, "dateUpdated" => $date));
    				}
    				return $result;
    				break;
    			
    			case self::ENROLLMENT_BONUS:
    				$select = $db->select()
    					->from("FSPlusOnePoints", "id")
    					->where('FSPlusOneID = "'.$fsPlusOneID.'"')
    					->where('actionID = "'.$action.'"');
    				
    				$result = Core_Database::fetchAll($select);
    				
    				if($result && $result[0]){
    					return false;
    				}else{
    					$date = date("Y-m-d H:i:s");
    					$result = Core_Database::insert("FSPlusOnePoints", array("FSPlusOneID" => $fsPlusOneID, "actionID" => $action, "points" => $pointAmount, "applied"=> "0", "FS_ID" => $isEligible['FS_ID'], "usertype" => "tech", "W9" => $isEligible['W9'],  "dateCreated" => $date, "dateUpdated" => $date));
    				}
    				return $result;
    				break;
    			
    			case self::CERT_COMP_TIA:
    			case self::CERT_BACKGROUND_CHECK:
    			case self::FS_MARKETING_REGISTRATION:
    			case self::REF_CLIENT_PURCHASE_10_WO:
				default:
/*    				$select = $db->select()
    					->from("FSPlusOnePoints", "id")
    					->where('FSPlusOneID = "'.$fsPlusOneID.'"')
    					->where('actionID = "'.$action.'"');
    				
    				$result = Core_Database::fetchAll($select);
    				
    				if($result && $result[0]){
    					return false;
    				}else{*/
    					$date = date("Y-m-d H:i:s");
    					$result = Core_Database::insert("FSPlusOnePoints", array("FSPlusOneID" => $fsPlusOneID, "actionID" => $action, "points" => $pointAmount, "applied"=> "0", "FS_ID" => $isEligible['FS_ID'], "usertype" => "tech", "W9" => $isEligible['W9'],  "dateCreated" => $date, "dateUpdated" => $date));
//    				}
    				return $result;
    				break;
    		}	
    	}
    }
    
    public static function getTechPointSummary($techID){
    	if(empty($techID))
    		return false;
    		
    	$db = Core_Database::getInstance();
    		
    	$select = $db->select()
    			->from("FSPlusOnePoints", array("points", "applied", "actionID"))
    			->where("FS_ID = '".$techID."'")
    			->where("usertype = 'tech'");
    				
    	$result = Core_Database::fetchAll($select);
    	
    	$returnArray = array();
    	
    	if($result && $result[0]){
    		foreach($result as $r){
    			if($r["applied"] == "0" /*|| $r['points'][0] == "-"*/){
    				$pointsCurrent[] = $r['points'];
    			}
    			
    			if($r['actionID'] != NULL && $r['actionID'] != "")$pointsAll[] = $r['points'];
    		}
    		
    		$returnArray['currentPoints'] = array_sum($pointsCurrent);
    		$returnArray['allPoints'] = array_sum($pointsAll);
    		return $returnArray;
    	}else{
    		return false;
    	}
    }
    
    public static function getActionsPoints(){
    	$db = Core_Database::getInstance();
    		
    	$select = $db->select()->from("FSPlusOneActions", array("id", "action", "points", "usertype"))->where("id > '5'");
    	$result = Core_Database::fetchAll($select);
    	
    	if($result && $result[0]){
    		return $result;
    	}else{
    		return false;
    	}
	
    }
    
    
}