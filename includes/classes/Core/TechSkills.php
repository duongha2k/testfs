<?php
class Core_TechSkills extends Core_TableRelatedItem {	
	public function __construct() {
		parent::__construct(Core_Database::TABLE_TECH_SKILL, "TechID", "skill_id");
	}
	
	public function getTechSkills($techIds) {
		if (is_numeric($techIds)) $techIds = array($techIds);
		if (!is_array($techIds)) return false;
		return $this->getItemsWithRelatedIds($techIds);
	}
	
	public function getTechsWithSkills($skillIds) {
		if (is_numeric($skillIds)) $techIds = array($skillIds);
		if (!is_array($skillIds)) return false;
		return $this->getRelatedIdsWithItems($skillIds);
	}

	public function getTechsWithSkillsAny($skillIds) {
		if (is_numeric($skillIds)) $techIds = array($skillIds);
		if (!is_array($skillIds)) return false;
		return $this->getRelatedIdsWithItems($skillIds, false);
	}
	
	public static function getMapping() {
		$cache = Core_Cache_Manager::factory();
		$mapping = $cache->load('techSkillsMapping');
		if ($mapping) return $mapping;
		
		$db = Core_Database::getInstance();
		$select = $db->select();
		$select->from('skills', array('id', 'name'));
		$mapping = $db->fetchPairs($select);
		$cache->save($mapping, 'techSkillsMapping');
		
		return $mapping;
	}
}