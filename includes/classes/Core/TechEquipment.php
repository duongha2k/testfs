<?php
class Core_TechEquipment extends Core_TableRelatedItem {
	public function __construct() {
		parent::__construct(Core_Database::TABLE_TECH_EQUIPMENT, "TechID", "equipment_id");
	}
	
	public function getTechEquipment($techIds) {
		if (is_numeric($techIds)) $techIds = array($techIds);
		if (!is_array($techIds)) return false;
		return $this->getItemsWithRelatedIds($techIds);
	}
	
	public function getTechsWithEquipment($skillIds) {
		if (is_numeric($skillIds)) $techIds = array($skillIds);
		if (!is_array($skillIds)) return false;
		return $this->getRelatedIdsWithItems($skillIds);
	}

	public function getTechsWithEquipmentAny($skillIds) {
		if (is_numeric($skillIds)) $techIds = array($skillIds);
		if (!is_array($skillIds)) return false;
		return $this->getRelatedIdsWithItems($skillIds, false);
	}
	
	public static function getMapping() {
		$cache = Core_Cache_Manager::factory();
		$mapping = $cache->load('techEquipmentMapping');
		if ($mapping) return $mapping;
		
		$db = Core_Database::getInstance();
		$select = $db->select();
		$select->from('equipment', array('id', 'name'));
		$mapping = $db->fetchPairs($select);
		$cache->save($mapping, 'techEquipmentMapping');
		
		return $mapping;
	}
}