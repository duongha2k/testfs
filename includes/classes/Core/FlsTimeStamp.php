<?php

class Core_FlsTimeStamp
{
    /**
     * Timestamps processing
     */

    public static function createTimeStamp($woID, $userName, $Description, $companyID, $clientName, $projectName, $customerName, $TechID, $TechName) {
//		date_default_timezone_set("America/Chicago");
		$dateTimeStamp = date("Y-m-d H:i:s");
		$woID        = Core_Caspio::caspioEscape($woID);
		$userName    = Core_Caspio::caspioEscape($userName);
		$Description = Core_Caspio::caspioEscape($Description);
		$companyID   = Core_Caspio::caspioEscape($companyID);
		$clientName  = Core_Caspio::caspioEscape($clientName);
		
    	$fieldList = "WO_UNID, DateTime_Stamp, Username, Description, Company_ID, Client_Name, Project_Name, Customer_Name, TechID, Tech_Name";
		$valueList = "'$woID', '$dateTimeStamp', '$userName', '$Description', '$companyID', '$clientName', '$projectName', '$customerName', '$TechID', '$TechName'";
		$result = Core_Caspio::caspioInsert(TABLE_FLS_WORK_ORDER_TIMESTAMPS, $fieldList, $valueList);
	}
	
/*	function createWOACSTimeStamp($id, $companyId) {
		if (!is_numeric($id)) return;
		$wo = new Core_WorkOrder($id);

		$reminderAll = $wo->getReminderAll() == "True" ? 1 : 0;
		$reminderAcceptance = $wo->getReminderAll() == "True" ? 1 : 0;
		$reminder24Hr = $wo->getReminderAll() == "True" ? 1 : 0;
		$reminder1Hr = $wo->getReminderAll() == "True" ? 1 : 0;
		$checkInCall = $wo->getReminderAll() == "True" ? 1 : 0;
		$checkOutCall = $wo->getReminderAll() == "True" ? 1 : 0;
		$reminderNotMarkComplete = $wo->getReminderAll() == "True" ? 1 : 0;
		$reminderIncomplete = $wo->getReminderAll() == "True" ? 1 : 0;
		$companyID = $companyId;
		$SMSBlast = $wo->getReminderAll() == "True" ? 1 : 0;
		
		$fieldList = "DateChanged, WO_UNID, ReminderAll, ReminderAcceptance, Reminder24Hr, Reminder1Hr, CheckInCall, CheckOutCall, ReminderNotMarkComplete, ReminderIncomplete, SMSBlast";
		$valueList = "GETDATE(), '$id', '$reminderAll', '$reminderAcceptance', '$reminder24Hr', '$reminder1Hr', '$checkInCall', '$checkOutCall', '$reminderNotMarkComplete', '$reminderIncomplete', '$SMSBlast'";
		$result = Core_Caspio::caspioInsert("WOACSChangeLog", $fieldList, $valueList);
	}*/
}