<?php
class Core_Site {
	protected static $table;
	protected static $fieldsList;
	protected $id;
	protected $SiteNumber;
	protected $SiteName;
	protected $Site_Contact_Name;
	protected $Address;
	protected $City;
	protected $State;
	protected $Zipcode;
	protected $Country;
	protected $SitePhone;
	protected $SiteFax;
	protected $SiteEmail;
	protected $Latitude;
	protected $Longitude;
	protected $Project_ID;
    protected $Status_ID;

	protected $row;

	public function __construct() {
		$this->init();
		$this->existing = false;
	}

	protected function init() {
		Zend_Db_Table::setDefaultAdapter(Core_Database::getInstance());
		self::$table = new Zend_Db_Table(Core_Database::TABLE_SITES);
		self::$fieldsList = get_object_vars($this);
		unset(self::$fieldsList['row']);
		unset(self::$fieldsList['id']);
		self::$fieldsList = array_keys(self::$fieldsList);
	}

	public function __get($name) {
		if (!property_exists($this, $name)) return NULL;
		return $this->$name;
	}

	public function __set($name, $value) {
		if (!property_exists($this, $name)) return;
		switch ($name) {
			case  'Country':
				if (empty($this->Country)) $this->Country = 'US';
				$c = new Core_Country($value);
				$this->$name = $c->getCode();
				if (!empty($this->State)) {
					$s = new Core_State($this->State, $this->Country);
					$this->State = $s->getCode();
				}
				break;
			case 'State':
				if (!empty($this->Country)) {
					$s = new Core_State($value, $this->Country);
					$this->$name = $s->getCode();
				}
				else
					$this->$name = $value;
				break;
			default:
				$this->$name = $value;
				break;
		}
	}

	public function load($siteNumber, $projectId, $company) {
		$row = $this->getRow($siteNumber, $projectId, $company);
		if (!$row) return false;
		$site = $row->toArray();
		if (!$site || sizeof($site) == 0) return false;
		foreach ($site as $field => $value) {
			if (!property_exists($this, $field)) continue;
			$this->$field = $value;
		}
		$this->row = $row;
		return true;
	}

	private function prepareRow() {
		foreach (self::$fieldsList as $field) {
			$this->row->$field = $this->$field;
		}
		if (is_null($this->Country)) $this->Country = 'US';
	}

	private function getRow($siteNumber, $projectId, $company) {
		$t = Core_Database::TABLE_SITES;
		$row = self::$table->fetchRow(self::$table->select(Zend_Db_Table::SELECT_WITH_FROM_PART)->setIntegrityCheck(false)
			->join(array('p' => Core_Database::TABLE_PROJECTS), "`p`.Project_ID = `$t`.Project_ID")
			->where("`$t`.SiteNumber = ?", $siteNumber)
			->where("`$t`.Project_ID = ?", $projectId)
			->where('`p`.Project_Company_ID = ?', $company));
		return $row;
	}

	public function toArray() {
		$ret = array();
		foreach (self::$fieldsList as $field) {
			$ret[$field] = $this->$field;
		}
		$ret['id'] = $this->id;
		return $ret;
	}

	public function save($company) {
		if (is_null($this->SiteNumber) || empty($this->Project_ID)) return false;
		if (!$this->row) $this->row = self::$table->createRow();
		if (empty($this->Latitude) || empty($this->Longitude)) {
		$geocode = new Core_Geocoding_Google();
		$geocode->setAddress($this->Address);
		$geocode->setZip($this->Zip);
		$geocode->setCity($this->City);
		$geocode->setState($this->State);
		$this->Latitude = $geocode->getLat();
		$this->Longitude = $geocode->getLon();
		}
		$this->prepareRow();
		try {
			$this->row->save();
		} catch (Exception $e) {
			// site exists
			$this->row = $this->getRow($this->SiteNumber, $this->Project_ID, $company);
			if (!$this->row) return false;
			$this->prepareRow();
			$this->row->save();
		}
	}
	
	public function delete() {
		if (is_null($this->SiteNumber) || empty($this->Project_ID)) return false;
		if (!$this->row) return false;
		$this->row->delete();
}
	
	public static function getSiteCount($company, $project = NULL) {
		$db = Core_Database::getInstance();
		$select = $db->select()->from(array('s' => Core_Database::TABLE_SITES), array('Project_ID' => 'Project_ID', 'count' => new Zend_Db_Expr('COUNT(`s`.id)')))->join(array('p' => Core_Database::TABLE_PROJECTS), '`s`.Project_ID = `p`.Project_ID', array());
        $select->where("`p`.Project_Company_ID=?", $company)
			->group('Project_ID');
		if (!empty($project))
			$select->where('`s`.Project_ID = ?', $project);
		$result = $db->fetchAll($select);
		$count = array();
		if (!$result || sizeof($result) == 0) return $count;
		foreach ($result as $proj) {
			$count[$proj['Project_ID']] = $proj['count'];
}
		return $count;
	}
}
