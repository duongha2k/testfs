<?php

abstract class Core_FlsValidation_Abstract extends Core_Validation_Abstract
{    
    private $errors = array();

    function __construct($data)
    {
        $this->data = $data;
        
		$this->globalPattern["NOT_BLANK"] = new patternMatch("/\S+?/", "Not Blank", false);
		$this->globalPattern["EMPTY_OR_ZERO"] = new patternMatch("/^(0{1})$/", "empty or 0", true);
//		$this->globalPattern["GREATER_THAN_ZERO"] = new patternMatch("/^(\d+[^0])$/", "Greater Than Zero", false);
		$this->globalPattern["MONEY_GREATER_THAN_ZERO"] = new patternMatch("/^(?(?!0[.]00)(\d+)[.](\d{2}))$/", "0.00 and Greater Than Zero", false);
		$this->globalPattern["WHOLE_NUMBER_GREATER_THAN_ZERO"] = new patternMatch("/^(?!0+)(\d+)$/", "Whole Number Greater Than Zero", false);
		$this->globalPattern["AMOUNT_PER_DEVICE"] = new patternMatch("/^Device$/", "'Device'", false);
		$this->globalPattern["PHONE_WITH_OPTIONAL_EXTENSION"] = new patternMatch("/^([(]?(\d{3})[)]?[^0-9]*(\d{3})[^0-9]*(\d{4})|[(]?(\d{3})[)]?[^0-9]*(\d{3})[^0-9]*(\d{4})[^0-9]+([0-9]*)[^0-9]*([0-9]*))$/", "###-###-#### or ###-###-#### x ####", TRUE);
		$this->globalPattern["PHONE_WITHOUT_EXTENSION"] = new patternMatch("/^[(]?(\d{3})[)]?[^0-9]*(\d{3})[^0-9]*(\d{4})$/", "###-###-####", TRUE);
		$this->globalPattern["SITE_PHONE_STRICT_WITH_OPTIONAL_EXTENSION"] = new patternMatch("/^([(](\d{3})[)]\s(\d{3})[-](\d{4})|[(](\d{3})[)]\s(\d{3})[-](\d{4})[^0-9]+[0-9]*)$/", "(###) ###-#### or (###) ###-#### x ####", true);
		$this->globalPattern["BOOLEAN_TRUE"] = new patternMatch("/^1$/", "equal to '1'", false);
		$this->globalPattern["TIME_PATTERN"] = new patternMatch("/^(\d{1,2}):(\d{2})\s(AM|PM)$/", "H:MM AM/PM", true);
		$this->globalPattern["ZIP_PATTERN"] = new patternMatch("/^.{4,}$/", "5 or more characters", true);
    }
}