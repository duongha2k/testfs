<?php

class Core_FlsValidation_Update extends Core_FlsValidation_Abstract 
{
    
    public function validate()
    {
		$this->fieldsMustEqual["Lead"] = array("False", "True");
		$this->fieldsMustEqual["Assist"] = array("False", "True");
		$this->fieldsMustEqual["CallType"] = array("Service Call", "Install");
		$this->fieldsMustEqual["Zip"] = $this->globalPattern["ZIP_PATTERN"];
		$this->fieldsMustEqual["FLSComments"] = $this->globalPattern["NOT_BLANK"];

//       	$this->requredFields[] = "PayAmt";
       	$this->requredFields[] = "Duration";
       	$this->requredFields[] = "FLSComments";
		
        if (!parent::checkRequredFields()) {
            return false;
        }
        
		if (!parent::checkMastEqualFields()) {
		    return false;
		}
		
		/*if (!empty($this->data['Project_ID'])) {
			parent::checkValidFieldName(TABLE_CLIENT_PROJECTS, 'Project_ID', 'Project_ID');
		}
		if (!empty($this->data['Project_Name'])) {
			parent::checkValidFieldName(TABLE_CLIENT_PROJECTS, 'Project_Name', 'Project_Name');
		}        
		if (!empty($this->data['WO_Category'])) {
			parent::checkValidFieldName(TABLE_WO_CATEGORIES, 'Category', 'WO_Category');
		}
        
        parent::checkValidFieldName(TABLE_WO_CATEGORIES, 'Category_ID', 'WO_Category_ID');
        */
        
        if (!empty($this->data['State'])) {
        	parent::checkValidFieldName(TABLE_STATES_LIST, 'Abbreviation', 'State');
        }
        if (!empty($this->data['Shipped_By'])) {
        	parent::checkValidFieldName(TABLE_SHIPPING_CARRIERS, 'Shipping_Carrier', 'Shipped_By');
        }
        
        $er = parent::getErrors();
        if (!empty($er)) {
        	return false;
        }
        return true;
    }
}