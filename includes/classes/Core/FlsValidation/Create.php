<?php

class Core_FlsValidation_Create extends Core_FlsValidation_Abstract 
{
    public function validate()
    {
		$this->fieldsMustEqual["Paid"] = "No";
		$this->fieldsMustEqual["PayApprove"] = "False";
		$this->fieldsMustEqual["Lead"] = array("False", "True");
		$this->fieldsMustEqual["Assist"] = array("False", "True");
		$this->fieldsMustEqual["CallType"] = array("Service Call", "Install", "");
		$this->fieldsMustEqual["Zip"] = $this->globalPattern["ZIP_PATTERN"];
		$this->fieldsMustEqual["Addedby"] = $this->globalPattern["NOT_BLANK"];
		$this->fieldsMustEqual["FLSComments"] = $this->globalPattern["NOT_BLANK"];

		$this->requredFields[] = "Addedby";
//       	$this->requredFields[] = "PayAmt";
       	$this->requredFields[] = "Duration";
       	$this->requredFields[] = "FLSComments";
								        				
        if (!parent::checkRequredFields()) {
            return false;
        }
        
		if (!parent::checkMastEqualFields()) {
		    return false;
		}
		
        if (!empty($this->data['State']))
        	parent::checkValidFieldName(TABLE_STATES_LIST, 'Abbreviation', 'State');
                
        $er = parent::getErrors();
        if (!empty($er)) {
        	return false;
        }
        return true;
    }
}