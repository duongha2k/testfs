<?php
/**
 * Enter description here...
 *
 */

class Core_FlsValidation_Factory
{
    const VALIDATION_TYPE_CREATE = 'create';    
    const VALIDATION_TYPE_UPDATE = 'update';    

    private $type = '';
    
    function __construct($type)
    {
        $this->type = $type;
    }
    
    public function getValidationObject($data)
    {
        switch ($this->type)
        {
            case self::VALIDATION_TYPE_CREATE:
                return new Core_FlsValidation_Create($data);
                break;
                
            case self::VALIDATION_TYPE_UPDATE:
                return new Core_FlsValidation_Update($data);
                break;
            
            default:
                return null;
                break;
        }
    }
}
