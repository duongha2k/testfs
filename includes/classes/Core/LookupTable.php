<?php
abstract class Core_LookupTable {
	protected $code;
	protected $name;
	protected $isValid;
	
	protected static $lookupTable = array();
	protected $aliasTable = NULL;
	protected $aliasTableName = NULL;
		
	public function __construct($lookup = NULL, $caseSensitive = false) {
		$this->isValid = false;
		$table = self::$lookupTable;
		if ($caseSensitive) {
		if (array_key_exists($lookup, $table)) {
			// lookup by code
			$this->code = $lookup;
			$this->name = $table[$lookup];
			$this->isValid = true;
		}
		else {
			$table = array_flip($table);
			if (array_key_exists($lookup, $table)) {
				$this->code = $table[$lookup];
				$this->name = $lookup;
				$this->isValid = true;
			}
		}
	}
		else {
			$lookupCmp = strtolower($lookup);
			foreach ($table as $k=>$v) {
				if ($this->isValid || ($lookupCmp != strtolower($k) && $lookupCmp != strtolower($v))) continue;
				$this->code = $k;
				$this->name = $v;
				$this->isValid = true;					
			}
		}
	}
		
	//public abstract static function getByCode();
	
	public function getAlias() {
		if (empty($this->aliasTableName)) return;
        $db = Core_Database::getInstance();
        $select = $db->select();
        $select->from($this->aliasTableName, array('alias', 'key'));
        $this->aliasTable = $db->fetchPairs($select);
		return $this->aliasTable;
	}
	
	public function getCodeByAlias($alias) {
		if (empty($this->aliasTableName)) return NULL;
		$cache = Core_Cache_Manager::factory();
		$key = $this->aliasTableName . 'ListAlias';
		$cacheKey = Core_Cache_Manager::makeKey($key);
		if ( is_null($this->aliasTable)) {
			$resultAlias = $this->getAlias();
			$cache->save($resultAlias, $cacheKey, array($key), LIFETIME_1DAY);
			$this->aliasTable = $resultAlias;
		}
//		var_dump($cache->load("alias_stateListAlias"));
		if (empty($alias)) return NULL;
		
		if (!empty($this->aliasTable[$alias])) return $this->aliasTable[$alias];
		return NULL;
	}
	
	public function getCode() {
		return $this->code;
	}

	public function getName() {
		return $this->name;
	}
	
	public function isValid() {
		return $this->isValid;
	}
}