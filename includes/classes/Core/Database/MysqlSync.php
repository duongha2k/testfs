<?php
//require_once(realpath(dirname(__FILE__) . "/../../../") . "/modules/tables.php");

class Core_Database_MysqlSync {
	
	const MODE_NORMAL = 1;
	const MODE_TECH_ITEMS = 2;

	protected static $mysqlTableToDBMapping = array(
		//Project is ported to mysql. Disabled. TABLE_CLIENT_PROJECTS => array("table" => Core_Database::TABLE_PROJECTS, "mapping" => array()),
		//TABLE_CLIENT_LIST => array("table" => Core_Database::TABLE_CLIENTS, "mapping" => array()),
		//Core_Database::TABLE_TECH_BANK_INFO => array("table" => TABLE_MASTER_LIST, "mapping" => array(), "mode" => self::MODE_NORMAL),
		//Core_Database::TABLE_TECH_EQUIPMENT => array("table" => 'Tech_Cabling_Skills', "mapping" => array(), "mode" => self::MODE_TECH_ITEMS),
		//Core_Database::TABLE_TECH_SKILL => array("table" => 'Tech_Telephony_Skills', "mapping" => array(), "mode" => self::MODE_TECH_ITEMS)
	);

/*	public static function syncBG($tableMysql, $idColumnMysql, $idList, $delete = false) {
		$idList = serialize($idList);
		$delete = serialize($delete);
		exec("(/usr/bin/php -f " . realpath(dirname(__FILE__) . "/../../../../") . "/htdocs/library/caspioSyncBG.php " . escapeshellarg($tableMysql) . " " . escapeshellarg($idColumnMysql) . " " . escapeshellarg($idList) . " " . escapeshellarg($delete) . ") > /dev/null 2>&1 &");
//		self::sync($tableMysql, $idColumnMysql, $idList, $delete);
	}*/

	public static function sync($tableMysql, $idColumnMysql, $idList, $delete = false) {

		$dbInfo = Zend_Registry::get("DB_Info");
		if ($dbInfo["dbname"] !== "gbailey_technicianbureau")
			return false; // don't sync in testing environment

		if (is_numeric($idList)) $idList = array($idList);
		if (!is_array($idList)) return false;
		if (!array_key_exists($tableMysql, self::$mysqlTableToDBMapping)) return false;
		$tableInfo = self::$mysqlTableToDBMapping[$tableMysql];
		$tableDB = $tableInfo["table"];
		$mapping = $tableInfo["mapping"];
		$mode = $tableInfo["mode"];
		$mysqlColumns = array_combine(Core_Database::getFieldList($tableMysql), Core_Database::getFieldListType($tableMysql));
		$tableInfoCaspio = Core_Caspio::caspioGetTableDesign($tableDB);

		$dbColumns = array();
		foreach ($tableInfoCaspio as $info) {
			$columnInfo = explode(",",$info);
			$dbColumns[$columnInfo[0]] = $columnInfo[1];
		}

		$idColumnDB = array_key_exists($idColumnMysql, $mapping) ? $mapping[$idColumnMysql] : $idColumnMysql;

		$fieldListMap = array_keys($mysqlColumns);
		$fieldListMap = array_combine($fieldListMap, $fieldListMap);

		$db = Core_Database::getInstance();

		if ($delete) {
/*			foreach ($idList as $k=>$v) {
				$idList[$k] = $db->quote($v);
			}
			$w = array("$idColumnDB IN (" . implode(",", $idList) . ")");
			try {
				$db->delete($tableDB, $w);
			} catch (Exception $e) {}
			return true;*/
		}

		$select = $db->select();
		if ($mode == self::MODE_NORMAL) {
			$select->from($tableMysql, $fieldListMap)->where("$idColumnDB IN (?)", $idList);
			$rowsToCopy = $db->fetchAll($select);
		}
		else {
			$mappingFieldName = array(
				'AtLeast40HrsLast6Months' => '40HrsLast6Months',
				'AvayaGateway' => 'AyayaGateway',
				'AvayaGateway6mo' => 'AyayaGateway6mo',
				'FishTape50' => '50.',
				'HaveTruck' => 'TruckCarryLaddersCable',
				'LowVoltageLicensed' => 'PossessAllLicenses',
				'NortelIBMS6mo' => 'NortelBMS6mo',
				'MItel' => 'Mitel',
				'MItel6mo' => 'Mitel6mo'
			);
			switch ($tableMysql) {
				case Core_Database::TABLE_TECH_EQUIPMENT:
					$search = new Core_TechEquipment();
					$rowsToCopy = $search->getTechEquipment($idList);
					$mapping = Core_TechEquipment::getMapping();
					break;
				case Core_Database::TABLE_TECH_SKILL:
					$search = new Core_TechSkills();
					$rowsToCopy = $search->getTechSkills($idList);
					$mapping = Core_TechSkills::getMapping();
					break;
				default:
					return false;
			}
			$mappingFlip = array_flip($mapping);
			foreach ($mappingFieldName as $caspio => $mysql) {
				if (!array_key_exists($mysql, $mappingFlip)) continue;
				$id = $mappingFlip[$mysql];
				$mapping[$id] = $caspio;
			}
		}
//		var_dump($rowsToCopy);
//		var_dump($mapping);
//		var_dump($dbColumns);
		

		if (empty($rowsToCopy) || sizeof($rowsToCopy) == 0) return true;
		foreach ($rowsToCopy as $index => $wo) {
			$realWO = array();
			foreach ($wo as $key=>$val) {
				if ($mode == self::MODE_NORMAL) {
					$mappedColumn = array_key_exists($key, $mapping) ? $mapping[$key] : $key;
					if (is_numeric($key) || !array_key_exists($mappedColumn, $dbColumns)) {
						unset($wo[$key]);
						continue;
					}

					if ($val === "NULL") {
						$wo[$key] = NULL;
					}
					else if (array_key_exists($key, $mysqlColumns)) {
						switch ($mysqlColumns[$key]) {
							case "datetime":
								$ut = strtotime($val);
								if ($ut && $val != "0000-00-00 00:00:00")
									$wo[$key] = date("Y-m-d H:i:s", $ut);
								else $wo[$key] = NULL;
								break;
							case "tinyint":
								if ($val === "True") $wo[$key] = 1;
								if ($val === "False") $wo[$key] = 0;
								break;
						}
					}
					$realWO[$mappedColumn] = $wo[$key];
				}
				else {
//					print "$val -- " . $mapping[$val] . "\n";
					if (!array_key_exists($val, $mapping) || !array_key_exists($mapping[$val], $dbColumns)) continue;
					$realWO[$mapping[$val]] = 1;
				}
			}

			if ($mode == self::MODE_TECH_ITEMS) {
				foreach ($dbColumns as $id => $name) {
					if ($id == "UNID" || $id == "TechID") continue;
					if (array_key_exists($id, $realWO)) continue;
					$realWO[$id] = 0;
				}
				$realWO[$idColumnMysql] = $index;
			}
//			$woList[$index] = $realWO;

			// insert / update to caspio
//			die();
			if ($mode == self::MODE_NORMAL) {
				$max = 0;
				do {
					$r = Core_Caspio::caspioSelectWithFieldListMap($tableDB, array('TechID' => 'TechID'), "TechID = {$realWO[$idColumnDB]}", "");
					if (empty($r)) {
						if (empty($max)) {
							$max = Core_Caspio::caspioSelectWithFieldListMap($tableDB, array('mymax' => 'MAX(TechID)'), "", "");
							$max = $max[0]['mymax'];
							if ($max > $realWO[$idColumnDB]) {
								$mail = new Core_Mail();
								$mail->setBodyText("MysqlSync error TechID: {$realWO[$idColumnDB]}");
								$mail->setFromName("FieldSolutions");
								$mail->setFromEmail('no-replies@fieldsolutions.com');
								$mail->setToEmail("tngo@fieldsolutions.com");
								$mail->setSubject("MysqlSync Error");
								$mail->send();
								return false;
							}
						}
						//echo "Create $tableDB {$realWO[$idColumnDB]}\n";
						$n = Core_Caspio::caspioInsert($tableDB, array("Username"), array(uniqid()));
					}
				} while (empty($r));
			}
			try {
				if ($mode == self::MODE_NORMAL) throw new Exception("Tech ID already exists");
				Core_Caspio::caspioInsert($tableDB, array_keys($realWO), array_values($realWO));
				$errors = Core_Api_Error::getInstance();
				$err = $errors->getErrors();
				if (!empty($err)) throw new Exception("Tech ID already exists");
/*				echo "Inserted: ";
				var_dump($realWO);
				echo "\n";*/
			} catch (Exception $e) {
				try {
					$w = "$idColumnDB = " . Core_Caspio::caspioEscape($realWO[$idColumnDB]);
					unset($realWO[$idColumnDB]);
					Core_Caspio::caspioUpdate($tableDB, array_keys($realWO), array_values($realWO), $w);
/*					echo "Updated: ";
					var_dump($realWO);
					var_dump($w);
					echo "\n";*/
/*					die();*/
				} catch (Exception $e) {
					error_log($e);
				}
			}
		}
		
		if ($tableMysql == Core_Database::TABLE_TECH_BANK_INFO) {
			self::sync(Core_Database::TABLE_TECH_EQUIPMENT, $idColumnMysql, $idList);
			self::sync(Core_Database::TABLE_TECH_SKILL, $idColumnMysql, $idList);
		}

		//Clean projects cache
/*		if ($tableMysql == TABLE_CLIENT_PROJECTS)
			Core_Cache_Manager::factory()->clean('all', array('prj'));*/

		return true;
	}
}

