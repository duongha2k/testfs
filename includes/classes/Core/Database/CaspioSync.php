<?php
//require_once(realpath(dirname(__FILE__) . "/../../") . "/modules/tables.php");

class Core_Database_CaspioSync {

	protected static $caspioTableToDBMapping = array(
		//Project is ported to mysql. Disabled. TABLE_CLIENT_PROJECTS => array("table" => Core_Database::TABLE_PROJECTS, "mapping" => array()),
		//TABLE_CLIENT_LIST => array("table" => Core_Database::TABLE_CLIENTS, "mapping" => array()),
		//TABLE_ADMINS  => array("table" => Core_Database::TABLE_ADMINS, "mapping" => array()),
		//TABLE_MASTER_LIST => array("table" => Core_Database::TABLE_TECH_BANK_INFO, "mapping" => array()),
		//TABLE_CUSTOMERS => array("table" => Core_Database::TABLE_CUSTOMERS, "mapping" => array()),
		//TABLE_STAFF => array("table" => Core_Database::TABLE_STAFF, "mapping" => array()),
		//TABLE_CLIENT_DENIED_TECHS => array("table" => Core_Database::TABLE_CLIENT_DENIED_TECHS, "mapping" => array()),
		//TABLE_ISO => array("table" => Core_Database::TABLE_ISO, "mapping" => array())
	);

	public static function syncBG($tableCaspio, $idColumnCaspio, $idList, $delete = false) {
		$idList = serialize($idList);
		$delete = serialize($delete);
		exec("(/usr/bin/php -f " . realpath(dirname(__FILE__) . "/../../../../") . "/htdocs/library/caspioSyncBG.php " . escapeshellarg($tableCaspio) . " " . escapeshellarg($idColumnCaspio) . " " . escapeshellarg($idList) . " " . escapeshellarg($delete) . ") > /dev/null 2>&1 &");
//		self::sync($tableCaspio, $idColumnCaspio, $idList, $delete);
	}

	public static function sync($tableCaspio, $idColumnCaspio, $idList, $delete = false) {
		if (is_numeric($idList)) $idList = array($idList);
		if (!is_array($idList)) return false;
		if (!array_key_exists($tableCaspio, self::$caspioTableToDBMapping)) return false;
		$tableInfo = self::$caspioTableToDBMapping[$tableCaspio];
		$tableDB = $tableInfo["table"];
		$mapping = $tableInfo["mapping"];

		$tableInfoCaspio = Core_Caspio::caspioGetTableDesign($tableCaspio);

		$caspioColumns = array();
		foreach ($tableInfoCaspio as $info) {
			$columnInfo = explode(",",$info);
			$caspioColumns[$columnInfo[0]] = $columnInfo[1];
		}

		$dbColumns = Core_Database::getFieldList($tableDB);
		$dbColumns = array_flip($dbColumns);

		$idColumnDB = array_key_exists($idColumnCaspio, $mapping) ? $mapping[$idColumnCaspio] : $idColumnCaspio;

		$fieldListMap = array_keys($caspioColumns);
		$fieldListMap = array_combine($fieldListMap, $fieldListMap);

		$db = Core_Database::getInstance();

		if ($delete) {
			foreach ($idList as $k=>$v) {
				$idList[$k] = $db->quote($v);
			}
			$w = array("$idColumnDB IN (" . implode(",", $idList) . ")");
			try {
				$db->delete($tableDB, $w);
/*				echo "Deleted: ";
				var_dump($w);
				echo "\n";*/
			} catch (Exception $e) {}
			return true;
		}

		$rowsToCopy = Core_Caspio::caspioSelectWithFieldListMap($tableCaspio, $fieldListMap, "$idColumnDB IN (" . implode(",", $idList) . ")", "");

		foreach ($rowsToCopy as $index => $wo) {
			$realWO = array();
			foreach ($wo as $key=>$val) {
				$mappedColumn = array_key_exists($key, $mapping) ? $mapping[$key] : $key;
				if (is_numeric($key) || !array_key_exists($mappedColumn, $dbColumns)) {
					unset($wo[$key]);
					continue;
				}
				if ($val === "NULL") {
					$wo[$key] = "NULL";
				}
				else if (array_key_exists($key, $caspioColumns)) {
					switch ($caspioColumns[$key]) {
						case "Date/Time":
							$ut = strtotime($val);
							if ($ut)
								$wo[$key] = date("Y-m-d H:i:s", $ut);
							break;
						case "Yes/No":
							if ($val === "True") $wo[$key] = 1;
							if ($val === "False") $wo[$key] = 0;
							break;
					}
				}
				$realWO[$mappedColumn] = $wo[$key];
			}

//			$woList[$index] = $realWO;

			// insert / update to caspio


			try {
				$db->insert($tableDB, $realWO);
/*				echo "Inserted: ";
				var_dump($realWO);
				echo "\n";*/
			} catch (Exception $e) {
				try {
					$w = array("$idColumnDB = " . $db->quote($realWO[$idColumnDB]));
					unset($realWO[$idColumnDB]);
					$db->update($tableDB, $realWO, $w);
/*					echo "Updated: ";
					var_dump($w);
					echo "\n";
					die();*/
				} catch (Exception $e) {
					print_r($e);
					error_log($e);
				}
	                }

		}

		//Clean projects cache
		if ($tableCaspio == TABLE_CLIENT_PROJECTS)
			Core_Cache_Manager::factory()->clean('all', array('prj'));

		try {
//			mail("tngo@fieldsolutions.com", "CaspioSync", print_r(debug_backtrace(), true));
		} catch (Exception $e) {}

		return true;
	}
}

