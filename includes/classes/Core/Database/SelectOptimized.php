<?php
require_once "Zend/Db/Select.php";
class Core_Database_SelectOptimized extends Zend_Db_Select {

    protected $indexHints = array();

    public function indexHint($hints) {
	$h = !is_array($hints);
	if (!is_array($hints)) return $this;
	$this->indexHints = array_merge($this->indexHints, $hints);
	return $this;
    }

    protected function _renderFrom($sql)
    {
        /*
         * If no table specified, use RDBMS-dependent solution
         * for table-less query.  e.g. DUAL in Oracle.
         */
        if (empty($this->_parts[self::FROM])) {
            $this->_parts[self::FROM] = $this->_getDummyTable();
        }

        $from = array();

        foreach ($this->_parts[self::FROM] as $correlationName => $table) {
            $tmp = '';

            // Add join clause (if applicable)
            if (! empty($from)) {
                $tmp .= ' ' . strtoupper($table['joinType']) . ' ';
            }

            $tmp .= $this->_getQuotedSchema($table['schema']);
            $tmp .= $this->_getQuotedTable($table['tableName'], $correlationName);

		if (array_key_exists($correlationName, $this->indexHints))
			$tmp .= " " . $this->indexHints[$correlationName];
		else if (array_key_exists($table['tableName'], $this->indexHints))
			$tmp .= " " . $this->indexHints[$table['tableName']];

            // Add join conditions (if applicable)
            if (!empty($from) && ! empty($table['joinCondition'])) {
                $tmp .= ' ' . self::SQL_ON . ' ' . $table['joinCondition'];
            }

            // Add the table name and condition add to the list
            $from[] = $tmp;
        }

        // Add the list of all joins
        if (!empty($from)) {
            $sql .= ' ' . self::SQL_FROM . ' ' . implode("\n", $from);
        }

        return $sql;
    }
}
