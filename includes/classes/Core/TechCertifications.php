<?php
class Core_TechCertifications extends Core_TableRelatedItem {
	public function __construct() {
		parent::__construct(Core_Database::TABLE_TECH_CERTIFICATION, "TechID", "certification_id", 
			array('date', 'number', 'dateExpired'),
			array('dateExpired IS NULL OR dateExpired <= NOW()'),
			"Core_TechCertification"
		);
	}
		
	public function getTechCertification($techIds) {
		if (is_numeric($techIds)) $techIds = array($techIds);
		if (!is_array($techIds)) return false;
		return $this->getItemsWithRelatedIds($techIds);
	}
	
	public function getTechsWithCertification($skillIds, $dateGreater = NULL, $dateLess = NULL) {
		$db = Core_Database::getInstance();
		if (is_numeric($skillIds)) $techIds = array($skillIds);
		if (!is_array($skillIds)) return false;
		$dateCriteria = array();
		if (!empty($dateGreater)) {
			foreach ($dateGreater as $id=>$crit) {
				$dateCriteria[] = "(" . $db->quoteInto("certification_id != ?", $id) . " OR " . $db->quoteInto("date >= ?", $crit) . ")";
	}
		}
		if (!empty($dateLess)) {
			foreach ($dateLess as $id=>$crit) {
				$dateCriteria[] = "(" . $db->quoteInto("certification_id != ?", $id) . " OR " . $db->quoteInto("date <= ?", $crit) . ")";
			}
		}
		if (sizeof($dateCriteria) == 0) $dateCriteria = NULL;
		return $this->getRelatedIdsWithItems($skillIds, true, $dateCriteria);
	}

	public function getTechsWithCertificationAny($skillIds) {
		if (is_numeric($skillIds)) $techIds = array($skillIds);
		if (!is_array($skillIds)) return false;
		return $this->getRelatedIdsWithItems($skillIds, false);
	}
	
	public static function getMapping() {
		$cache = Core_Cache_Manager::factory();
		$mapping = $cache->load('techCertificationMapping');
		if ($mapping) return $mapping;
		
		$db = Core_Database::getInstance();
		$select = $db->select();
		$select->from('certifications', array('id', 'name', 'label'));

		$dbInfo = Zend_Registry::get("DB_Info");
		if ($dbInfo["dbname"] == "gbailey_technicianbureau")
			$select->where("hide = 0");

		$info = $db->fetchAll($select);
		$mapping = array();
		foreach ($info as $val) {
			$mapping[$val['id']] = $val;
		}
		$cache->save($mapping, 'techCertificationMapping');
		
		return $mapping;
	}

    public static function getTechIdsArray_ByCertId($certId)
    {
    	$db = Core_Database::getInstance();    
        $select = $db->select();
        $select->from(Core_Database::TABLE_TECH_CERTIFICATION);
        $select->where("certification_id = ?",$certId);     
        $techs = $db->fetchAll($select);     
        if(empty($techs)) return null;
        $result = array();
        foreach ($techs as $t) {
            $result[] = $t['TechID'];
        }
        return $result;        
    }
    
    //--- 13535
    public static function  getTechIdsArray_WithAnyCertId($certIdsArray)
    {
        $db = Core_Database::getInstance();    
        $select = $db->select();
        $select->from(Core_Database::TABLE_TECH_CERTIFICATION);
        $select->where("certification_id IN (?)",$certIdsArray);     
        $techs = $db->fetchAll($select);     
        if(empty($techs)) return null;
        $result = array();
        foreach ($techs as $t) {
            $result[] = $t['TechID'];
        }
        return $result;                
    }
    
    public static function getDeVryTechIdsArray()
    {
        return Core_TechCertifications::getTechIdsArray_ByCertId(2);
    }
    
    public static function getTBCTechIdsArray()
    {
        return Core_TechCertifications::getTechIdsArray_ByCertId(3);
    }

    public static function getBlueRibbonTechIdsArray()
    {
        return Core_TechCertifications::getTechIdsArray_ByCertId(4);
    }

    public static function getTechIdsArray_ByComputersPlusTestFail()
    {
        $db = Core_Database::getInstance();    
        $select = $db->select();
        $select->from("ComputersPlus_Test",array('tech_id',"maxscore"=>"(MAX(score))"));
        $select->group("tech_id");
        $select->having("MAX(score) < 100");
        $select->distinct();
        $techs = $db->fetchAll($select);     
        
        if(empty($techs)) return null;
        $result = array();
        foreach ($techs as $t) {
            $result[] = $t['tech_id'];
        }
        return $result;        
    }    

    public function saveWentToBrotherSite($techID,$WentToBrotherSite)
    {
        $criteria = "TechID='$techID'";
        $updatedFields = array('WentToBrotherSite'=>$WentToBrotherSite);
        $result = Core_Database::update("tech_ext",$updatedFields,$criteria);
        return $result;
    }
    public function getWentToBrotherSite($techID)
    {
        $db = Core_Database::getInstance();    
        $select = $db->select();
        $select->from("tech_ext",array('WentToBrotherSite'));
        $select->where("TechID='$techID'");
        $records = $db->fetchAll($select);     
        if(!empty($records)) return $records[0]['WentToBrotherSite'];
        else return 0;
    }
    //--- 13440
    public static function getTechIDArray_ByCertIdAndNum($certId,$number)
    {//TechID
        $db = Core_Database::getInstance();    
        $select = $db->select();
        $select->from(Core_Database::TABLE_TECH_CERTIFICATION);
        $select->where("certification_id = '$certId'");   
        $select->where("number = '$number'");   
        $techs = $db->fetchAll($select);     
        if(empty($techs)) return null;
        else return $techs[0];
        $result = array();
        foreach ($techs as $t) {
            $result[] = $t['TechID'];
        }
        return $result;        
    }
    
}
