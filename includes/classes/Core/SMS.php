<?php
class Core_SMS extends Core_Mail {
    /* updated the query to omit users who have an 'sms' setting = 1 ( indicating they've opted out of SMS messages for work */
	private static function getSMSEmailFromTechID($techIDList) {
		$sms_domain = "ISNULL((SELECT TOP 1 SMS_Domain FROM Cell_Carrier WHERE CellProvider = Carrier), '')";
		if (!is_array($techIDList) || sizeof($techIDList) == 0) return array();
//		$emailList = Core_Caspio::caspioSelectAdv(TABLE_MASTER_LIST, "SMS_Number, $sms_domain", "TechID IN ('" . implode("','", $techIDList) . "') AND AllowText = '1' AND AcceptTerms = 'Yes' AND $sms_domain != ''", "");
                $db = Core_Database::getInstance();
                $smsSelect = $db->select()->from(array("tbi" => Core_Database::TABLE_TECH_BANK_INFO), array("SMS_Number"))
                ->joinLeft(array("carrier" =>"cell_carriers"), "tbi.CellProvider = carrier.id", array("sms_domain"))
                ->where('TechID IN(?)', $techIDList)
                ->where("AllowText = '1'")
                ->where("AcceptTerms = 'Yes'")
                ->where("Deactivated !='1'");

                $emailList = Core_Database::fetchAll($smsSelect);

		if (sizeof($emailList) == 0) return array();
		$retList = array();
		foreach ($emailList as $info) {
			$tech = array_values($info);
			$phone = $tech[0];
			$domain = $tech[1];
			if (empty($domain) || empty($phone)) continue;
			
			$phone = filter_var($phone, FILTER_SANITIZE_NUMBER_INT);
			$phone = str_replace(array("-", ".", " ", "(", ")"), array("", "", "", "", ""), $phone);
			
			if ($phone != "" && $domain != "")
				$retList[] = $phone . $domain;
		}
		return $retList;
	}
		
	public function setToSMSWithTechIDList($techIDList) {
		$smsRecipientList = implode(",", Core_SMS::getSMSEmailFromTechID($techIDList));
		self::setToEmail($smsRecipientList);
	}
	
	public function setToSMS($smsEmails) {
		self::setToEmail($smsEmails);
	}
	
	public function send() {
		if ($this->toEmail != "")
			parent::send();
	}

}
?>