<?php
/* class to determine if company is tech deduct enabled */
class Core_TechDeduct {
	public static function isEnabledForCompanyID($companyId) {
		if (empty($companyId)) return false;
		$db = Core_Database::getInstance();
		$select = $db->select();

		$select->from(Core_Database::TABLE_CLIENTS_TECH_DEDUCT, array("id"))
			->where("Company_ID = ?", $companyId);
		$isEnabled = Core_Database::fetchAll($select);
		return $isEnabled && $isEnabled[0]['id'];
	}
	
	public static function getPercentage($companyId, $projectId = 0) {
		if (empty($companyId)) return false;
		if (empty($projectId)) $projectId = 0;
		$db = Core_Database::getInstance();
		$select = $db->select();
		
		$select->from(array("c" => Core_Database::TABLE_CLIENTS_TECH_DEDUCT), array("percent" => "(CASE WHEN p.PcntDeductPercent IS NULL OR p.PcntDeductPercent = 0 THEN c.PcntDeductPercent ELSE p.PcntDeductPercent END)"))
		->joinLeft(array("p" => Core_Database::TABLE_PROJECTS), "p.Project_Company_ID = c.Company_ID AND Project_ID = " . $db->quoteInto("?", $projectId), "")
		->where("Company_ID = ?", $companyId);
		$percent = $db->fetchOne($select);
		if (empty($percent)) $percent = 0.10;
		// truncate float to percentage
		$percent = floor((float)$percent * 100) / 100;		
		return $percent;
	}	
}
