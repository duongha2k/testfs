<?php
    class Core_BRT {

        // Your API Key
        const API_KEY = 'b83511791d9ca7cdcddfc09f18f69cf87255e3f8';
        // Your API Secret
        const API_SECRET = '!FielDSolutions!';
        // Your API Company Name (no spaces use '_')
        const API_COMPANY = 'field_solutions';
        // Sandbox POST URL
        const POST_URL = 'https://capi.blueribbontechs.com';
        // Your Client ID
        const CLIENT_ID = 10192;
        const TOKEN = '22412979fa61fbeafc72d76a5f88819df05068d9';
		
		private $result = "";
		private $_key;
		private $_secret;
		private $_api_url;
		private $_company;
		private $_client_id;
		private $_token;
		private $_last_matched_tech_info; // info of last tech from doesTechMatch method

		public function __construct() {
            $this->_key = self::API_KEY;
            $this->_secret = self::API_SECRET;
            $this->_api_url = self::POST_URL;
            $this->_company = self::API_COMPANY;
            $this->_client_id = self::CLIENT_ID;
            $this->_token = self::TOKEN;
/*            if (! is_null ( $input_params )) {
                $dec_params = json_decode ( $input_params );
                foreach ( $dec_params as $key => $value ) {
                    if ($key == 'brt_request') {
                        $this->request ( $value );
                        break;
                    }
                }

            }*/
        }
		
		public function sync() {
			// request BRT tech dump and sync with FS tech records
			$db = Core_Database::getInstance();
			$this->request('dump');
			$result = $this->getResult();
			if ($result) {
				foreach ($result as $techID=>$info) {
					if (empty($info->last)) continue;
					if (!$this->doesTechMatch($techID, $info->last)) continue;
					// mark as BRT 
					$brtCert = new Core_TechCertification($techID, 4, $info->brt_id, date('Y-m-d'));
					$cert = $brtCert->loadRow();
					if (empty($cert)) {
						$brtCert->save();
					}
					
					if ($info->drug == 1) {
						// drug test
						$drugDate = $this->formatDate($info->drug_date);
						if (!$drugDate) {
/*							echo "BRT sync unable to parse date '{$info->drug_date}' for $techID\n";
							error_log("BRT sync unable to parse date '{$info->drug_date}' for $techID");*/
							continue;
						}
						$update = array('DrugPassed' => 1, 'DrugTest_Pass' => 'Pass', 'DatePassDrug' => $drugDate);
						$where = $db->quoteInto("TechID = ?", $techID) . " AND " . $db->quoteInto("(DatePassDrug IS NULL OR DatePassDrug < ?)", $drugDate);
//						echo "** ACTION UPDATE DRUG TEST TECH ID $techID: " . print_r($update, true) . "($where) \n";
						
						$result = $db->update(Core_Database::TABLE_TECH_BANK_INFO, $update, $where);
//						if (!$result) echo "\n** BRT data older, tech not updated\n";
					}
					if ($info->background == 1) {
						// background check
						$bgDate = $this->formatDate($info->background_date);
						if (!$bgDate) {
/*							echo "BRT sync unable to parse date '{$info->background_date}' for $techID\n";
							error_log("BRT sync unable to parse date '{$info->background_date}' for $techID");*/
							continue;
						}
						$tier = stripos($info->background_type, "gold") !== FALSE ? 1 : 0;
						$update = array();
						$update["Bg_Test_ResultsDate_Lite"] = $bgDate;
						$update["Bg_Test_Pass_Lite"] = 'Pass';
						$where = $db->quoteInto("TechID = ?", $techID) . " AND ";
						if ($tier == 1) {
							$update["BG_Test_ResultsDate_Full"] = $bgDate;
							$update["Bg_Test_Pass_Full"] = 'Pass';
							$where .=  $db->quoteInto("(BG_Test_ResultsDate_Full IS NULL OR BG_Test_ResultsDate_Full < ?)", $bgDate);
						}
						else
							$where .=  $db->quoteInto("(Bg_Test_ResultsDate_Lite IS NULL OR Bg_Test_ResultsDate_Lite < ?)", $bgDate);
														
//						echo "** ACTION UPDATE BG CHECK TECH ID $techID: " . print_r($update, true) . " ($where) \n";
						$result = $db->update(Core_Database::TABLE_TECH_BANK_INFO, $update, $where);
//						if (!$result) echo "\n** BRT data older, tech not updated\n";
					}
					if ($info->boot_camp == 1) {
						// boot camp
						$bootCampCert = new Core_TechCertification($techID, 3, NULL, date('Y-m-d'));
						$cert = $bootCampCert->loadRow();
						if (empty($cert)) {
							$bootCampCert->save();
					}
						$bootCampCert = new Core_TechCertification($techID, 43, NULL, NULL);
						$cert = $bootCampCert->loadRow();
						if (!empty($cert)) {
							// remove boot camp plus
							$bootCampCert->delete();
						}
					}
					else if ($info->boot_camp == 2) {
						// boot camp plus
						$bootCampCert = new Core_TechCertification($techID, 3, NULL, NULL);
						$cert = $bootCampCert->loadRow();
						if (!empty($cert)) {
							// remove regular boot camp
							$bootCampCert->delete();
						}
						$bootCampCert = new Core_TechCertification($techID, 43, NULL, date('Y-m-d'));
						$cert = $bootCampCert->loadRow();
						if (empty($cert)) {
							$bootCampCert->save();
						}
					}
					if ($info->fls == 1) {
						$db = Core_Database::getInstance();
						$flsDate = $this->formatDate($info->fls_date);
//						echo "** FLS CHECK {$this->_last_matched_tech_info['FLSID']} $techID\n";
						if (!empty($this->_last_matched_tech_info['FLSID'])) continue;
						$upInfo = array(
							'FLSstatus' => 'Trained',
							'FLStitle' => 'Backup',
							'FLSCSP_Rec' => 1,
							'FLSCSP_RecDate' => $flsDate,
							'FLSID' => 'FL' . $techID
						);
						$db->update(Core_Database::TABLE_TECH_BANK_INFO, $upInfo, $db->quoteInto("TechID = ?", $techID));
						$insInfo = array(
							'tech_id' => $techID,
							'date_taken' => $flsDate,
							'score' => 100
						);
						$db->insert('FLS_Test', $insInfo);
//						echo "** ACTION FLS CHECK TECH ID $techID\n";
					}
//					echo "\n\n **** END ****\n\n";
				}
			}
		}
		
		private function doesTechMatch($techID, $name) {
			// match tech's lastname and tech id to guard against typo of tech id
			$db = Core_Database::getInstance();
			if (empty($techID)) return false;
			$select = $db->select();
			$select->from(Core_Database::TABLE_TECH_BANK_INFO, array(
				'TechID',
				'FLSID',
				'FLSCSP_Rec'
			));
			$select->where("TechID = ?", $techID);
			$lastname = explode(" ", $name);
			if (empty($lastname))
				$lastname = $name;
			else
				$lastname = $lastname[count($lastname) - 1];
			$select->where("Lastname LIKE ?", "%$lastname%");
			if (!empty($name)) {
				$result = $db->fetchAll($select);
				if (!empty($result))
					$this->_last_matched_tech_info = $result[0];
				$result = !empty($result);
			}
			else
				$result = false;
			if (!$result) {
				// log error
				$this->logTechMismatch($techID, $name);
			}
			return $result;
		}

        private function request($request_type) {

            if (empty ( $request_type ) || $request_type == '') {
                $output = json_encode ( array (
                    'brt_api_error' => 'failed to read request type, is it set?', 'status' => 'failure', 'client_id' => $this->_client_id 
                ) );
//                header ( "HTTP/1.0 403 FAILURE" );
				$this->result = $output;
                return false;
            }

            // This is the request array value from brt_client_test.php

            switch ($request_type) {

                // Start Authentication
                case 'greet' :
                    $fields = array (
                        'api_key' => $this->_key, 'api_secret' => $this->_secret, 'api_company' => $this->_company, 'request' => 'greet', 'client_id' => $this->_client_id 
                    );
                    break;

                    // Request Umbrella Status

                case 'dump' :
                    $fields = array (
                        'api_key' => $this->_key, 'api_secret' => $this->_secret, 'api_company' => $this->_company, 'request' => 'm_result', 'client_id' => $this->_client_id, 'token' => $this->_token 
                    );
                    break;

                default :
                    $output = json_encode ( array (
                        'brt_api_error' => 'invalid request type passed', 'status' => 'failure', 'client_id' => $this->_client_id 
                    ) );
//                    header ( "HTTP/1.0 403 FAILURE" );
					$this->result = $output;
                    return false;

            }

            if (array_key_exists ( 'api_key', $fields ) && array_key_exists ( 'api_secret', $fields ) && array_key_exists ( 'api_company', $fields )) {
                $ch = curl_init ();
                curl_setopt ( $ch, CURLOPT_HEADER, 0 );
                curl_setopt ( $ch, CURLOPT_VERBOSE, 0 );
                curl_setopt ( $ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible;)" );
                curl_setopt ( $ch, CURLOPT_URL, $this->_api_url );
				curl_setopt ( $ch, CURLOPT_SSLVERSION, 3);
                curl_setopt ( $ch, CURLOPT_POST, true );
                curl_setopt ( $ch, CURLOPT_SSL_VERIFYPEER, false );
                curl_setopt ( $ch, CURLOPT_SSL_VERIFYHOST, false );
                curl_setopt ( $ch, CURLOPT_POSTFIELDS, $fields );
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                $result = curl_exec ( $ch );
                $info = curl_getinfo ( $ch );
                curl_close ( $ch );
				$this->result = $result;

                if (array_key_exists ( 'http_code', $info ) && $info ['http_code'] == 200) {
                    return true;
                } else {
                    return false;
                }
            } else {
                $output = json_encode ( array (
                    'brt_api_error' => 'failed to read all request params.', 'status' => 'failure' 
                ) );
//                header ( "HTTP/1.0 403 FAILURE" );
				$this->result = $output;
                return false;
            }

        }

        public function __destruct() {
            unset ( $this );
            unset ( $fields );
            unset ( $info );
        }
		
		public function getResult() {
			return json_decode($this->result);
		}
		
		public function logTechMismatch($techID, $name) {
			$db = Core_Database::getInstance();
			$db->insert('brt_mismatch', array('TechID' => $techID, 'Name' => $name, 'Date' => new Zend_Db_Expr('NOW()')));
		}
		
		private function formatDate($date) {
			try {
				$drugDateRaw = str_replace("-", "/", $date);
				$objDateNow = new Zend_Date($date, "MM-dd-yyyy hh:mm:ss a");
				$date = $objDateNow->toString('yyyy-MM-dd HH:mm:ss');
			}
			catch (Exception $e) {
				$date = false;
			}
			return $date;
		}
    }

?>
