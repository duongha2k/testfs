<?php

class Core_EmailForPublishedForeignCountryWO
{

    private $userFullName;
    private $wo;
    private $project;
    private $mail;

    function __construct()
    {
        $this->mail = new Core_Mail();
    }
        
    public function setWorkOrder($workOrder)
    {
        if (is_object($workOrder)) {
        	$this->wo = $workOrder;
        } else {
            $this->wo = new Core_WorkOrder((int)$workOrder);
        }
    }

    public function setUserFullName($user)
    {
		$this->userFullName = $user;
    }

    public function sendMail()
    {
    	$WIN_NUM = $this->wo->getWIN_NUM();
		$Country = $this->wo->getCountry();

		$Company = $this->wo->getCompany_Name();

	    $vFromEmail = "support@fieldsolutions.com";
		
		$Country = strtoupper($Country);
		
		switch ($Country) {
			case 'MX':
				$eList = "mexico@fieldsolutions.com";
				break;
			case 'BR':
				$eList = "brazil@fieldsolutions.com";
				break;
			case 'BS':
				$eList = "bahamas@fieldsolutions.com";
				break;
			case 'KY':
				$eList = "cayman@fieldsolutions.com";
				break;
			case 'UK':
				$eList = "england@fieldsolutions.com";
				break;
			case 'US':
			case 'CA':
				return;
				break;
			default:
				return;
		}
		
		$WO_ID = $this->wo->getWO_ID();
		$Project = $this->wo->getProject_Name();
		$Headline = $this->wo->getHeadline();
		$today = new Zend_Date();
		$today = $today->toString('MM/dd/YYYY');
		$StartDate = $this->wo->getStartDate();
		if (!empty($StartDate)) {
			try {
				$StartDate = new Zend_Date($StartDate, 'YYYY-MM-dd');
				$StartDate = $StartDate->toString('MM/dd/YYYY');
			}
			catch (Exception $e) {
			}
		}
		$StartTime = $this->wo->getStartTime();
		$PayMax = $this->wo->getPayMax();
		$Per = $this->wo->getAmount_Per();
		$SiteName = $this->wo->getSiteName();
		$Address = $this->wo->getAddress();
		$City = $this->wo->getCity();
		$State = $this->wo->getState();
		$Zipcode = $this->wo->getZipcode();
		
    	$caller = "EmailForPublishedForeignWO";
    	$vSubject = "WIN# $WIN_NUM Published in $Country";
		

    	$message = "Dear $Country Ops Support Team,

A Work Order has been published in $Country by {$this->userFullName} from $Company. 

Company: $Company

Client Name: {$this->userFullName}

WIN#: $WIN_NUM

Client WO ID: $WO_ID

Project: $Project

Headline: $Headline

Date Published: $today

Start Date: $StartDate

Start Time: $StartTime

Pay: $PayMax PER $Per

Site Name: $SiteName

Address: Address

City: $City

State/Province/Island: $State

Zip/Postal Code: $Zipcode

Country: $Country";

    	$this->mail->setBodyText($message);
        $this->mail->setFromEmail($vFromEmail);
//        $this->mail->setFromName($vFromName);
        $this->mail->setToEmail($eList);
        $this->mail->setToName($caller);
        $this->mail->setSubject($vSubject);
        
        $this->mail->send();
                		
    }
}