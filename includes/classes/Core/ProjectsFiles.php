<?php
class Core_ProjectsFiles extends Zend_Db_Table_Abstract
{
    protected $_name = 'projects_files';

    /**
     * remove files by IDs
     * @param <int> $win
     * @param <array> $files
     * @return bollean
     * @author Alex Scherba
     */
    public function removeFiles($prj_id, $files){
        if(empty($files) || !is_array($files)) return false;
        $select = $this->select()
                ->from($this, array('path', 'WMFileID'))
                ->where('id IN (?) AND Project_ID ='.$prj_id, $files);
        $filesNames = $this->fetchAll($select);
        $where = $this->getAdapter()->quoteInto('id IN (?) AND Project_ID ='.$prj_id, $files);
        $del = $this->delete($where);
        $files = new Core_File();
        if($del) {
            foreach ($fileNames as $item) {
				if (empty($item['WMFileID'])) continue;
				Core_WorkMarket::removeFileFromWMAssignment($item['WMFileID']);
            }
            return $files->removeFiles($filesNames, S3_PROJECTS_DOCS_DIR);
		}
        return false;
    }
}
?>
