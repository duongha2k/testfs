<?php
class Core_ProjectUsersAllowedCreateWO extends Zend_Db_Table_Abstract
// if no user listed for a project, all users can create wo
// otherwise only users listed are allowed
{
	protected $_name = 'projects_users_allowed_createwo';
	protected $_primary = array("Project_ID", "UserName");
	
	private static function getInstance() {
		$db = Core_Database::getInstance();
		return new self(array('db'=>$db));
	}
	
	private static function getSelect() {
		$me = self::getInstance();
		$select = $me->select();
		$select->from(array("ua" => Core_Database::TABLE_PROJECTS_USERS_ALLOWED_CREATEWO), array('Project_ID', 'UserName'))
			->join(array("p" => Core_Database::TABLE_PROJECTS), "p.Project_ID = ua.Project_ID", array('CreatedBy', 'Project_Name', 'Owner', 'Project_Company_ID'))
			->join(array("c" => Core_Database::TABLE_CLIENTS), "c.UserName = ua.UserName", array('ContactName'))
		->setIntegrityCheck(false);
		return $select;
	}
	
	private static function clearProjectCache() {
		$cache = Core_Cache_Manager::factory();
		$cache->clean(
			Zend_Cache::CLEANING_MODE_MATCHING_TAG,
			array('prj')
		);
	}
	
	public static function isUserAllowedForProjectID($user, $projectID) {
		try {
			$userObj = new Core_Api_User;
			$userObj->lookupUsername($user);
			$userObj->loadById($userObj->ClientID);
			$pm = $userObj->isPM();
			// client user is pm
			if ($pm == 1) return true;
		} catch (Excption $e) {
			// user not found
		}		
		$allowedList = self::getAllowedUsersForProjectID($projectID);
		return empty($allowedList) || in_array($user, $allowedList);
	}
	
	public static function getAllowedUsersForProjectID($projectID) {
		// get list of allowed users
		if (empty($projectID)) return false;
		$me = self::getInstance();
		$select = $me->select();
		$select->from($me, array('UserName'))->where('Project_ID = ?', $projectID);
		$rows = $me->fetchAll($select);
		$users = array();
		foreach ($rows as $row) {
			$users[] = $row['UserName'];
		}
		return $users;
	}

	public static function addUsersForProjectID($projectID, $users) {
		// allow all users in $users to create work order for given project id where $users is array of usernames
		if (empty($projectID) || empty($users) || !is_array($users)) return false;
		$me = self::getInstance();
		foreach ($users as $user) {
			try {
				$me->insert(array('Project_ID' => $projectID, 'UserName' => $user));
			} catch (Exception $e) {
				// ignore duplicates
			}
		}
		self::clearProjectCache();
		return true;
	}
	
	public static function removeUsersForProjectID($projectID, $users) {
		// deny all users in $users from creating work order for given project id unless a project has no restrictions
		if (empty($projectID) || empty($users) || !is_array($users)) return false;
		$db = Core_Database::getInstance();
		$me = self::getInstance();
		foreach ($users as $user) {
			try {
				$me->delete($db->quoteInto("Project_ID = ?",$projectID) . " AND " . $db->quoteInto("UserName = ?", $user));
			} catch (Exception $e) {
			// ignore duplicates
			}
		}
		self::clearProjectCache();
		return true;
	}
		
	public static function removeAllUsersForProjectID($projectID) {
		// remove all user restrictions for project
		if (empty($projectID)) return false;
		$db = Core_Database::getInstance();
		$me = self::getInstance();
		if (!is_array($projectID))
			$me->delete($db->quoteInto('Project_ID = ?', $projectID));
		else
			$me->delete($db->quoteInto('Project_ID IN (?)', $projectID));
		self::clearProjectCache();
		return true;
	}

	public static function removeAllRestrictionsForAllProjectsOwner($user, $company_id = NULL) {
		// remove all user restrictions for all user created project
		if (empty($user)) return false;
		$db = Core_Database::getInstance();
		$me = self::getInstance();
		$projects = self::getRestrictionsForAllProjectsOwner($user, $company_id);
		$projects = array_keys($projects);
		self::removeAllUsersForProjectID($projects);
		self::clearProjectCache();
		return true;
	}

	public static function getRestrictionsForAllProjectsOwner($user, $company_id = NULL) {
		// list all projects created by user and inside each project lists user allowed to create wo for that project
		// project name is returned in Project_Name index
		$me = self::getInstance();
		$select = self::getSelect();
		$select->where('Owner = ? OR (Owner IS NULL AND p.CreatedBy = ?)', $user, $user);
		if (!empty($company_id))
			$select->where('Project_Company_ID = ?', $company_id);
		$result = $me->fetchAll($select);
		$projects = array();
		foreach ($result as $row) {
			$projects[$row['Project_ID']]['Project_Company_ID'] = $row['Project_Company_ID'];
			$projects[$row['Project_ID']]['Project_Name'] = $row['Project_Name'];
			$projects[$row['Project_ID']][] = array('UserName' => $row['UserName'], 'ContactName' => $row['ContactName']);
		}
		return $projects;
	}
	
	public static function getUsersAllowedForCompany($company_id) {
		// lists all users currently allowed to create wo in any project for the company
		$me = self::getInstance();
		$select = self::getSelect();
		$select->where('Project_Company_ID = ?', $company_id);
		$result = $me->fetchAll($select);
		$users = array();
		foreach ($result as $row) {
			$users[] = $row['UserName'];
		}
	}
	
	public static function projectSelectAllowedForUser($user, $company_id, $fieldsList = NULL, $allowProjects = NULL) {
		// return a select to show only projects a user is allowed to create wo for
		$db = Core_Database::getInstance();
		$select = $db->select();
		if (empty($fieldsList)) $fieldsList = array();
		$select->from(array("p" => Core_Database::TABLE_PROJECTS), $fieldsList);
		$allowProjectQuery = "";
		if (!empty($allowProjects) && is_array($allowProjects)) {
			$allowProjectQuery = $db->quoteInto("OR p.Project_ID IN (?)", $allowProjects);
		}
		$select->joinLeft(array("ua" => Core_Database::TABLE_PROJECTS_USERS_ALLOWED_CREATEWO), 
                                "p.Project_ID = ua.Project_ID", 
                                array('UserName'))
			->where("ua.UserName = ? OR ua.UserName IS NULL $allowProjectQuery", $user)
			->where("p.Project_Company_ID = ?", $company_id);
		return $select;
	}
}
?>
