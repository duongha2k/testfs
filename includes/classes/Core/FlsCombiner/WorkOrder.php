<?php

class Core_FlsCombiner_WorkOrder extends Core_Combiner_WorkOrder
{    
    public function combine()
    {        
        if (!empty($this->workOrderData['Project_ID'])) {
        	$this->processProjectData(Core_Caspio::caspioEscape((int)$this->workOrderData['Project_ID']));
        }
		else if (!empty($this->workOrderData['ProjectName'])) {
        	$this->processProjectData(Core_Caspio::caspioEscape($this->workOrderData['ProjectName']));
		}

        if (!empty($this->workOrderData['FLSTechID'])) {
        	$this->processTechData(Core_Caspio::caspioEscape((int)$this->workOrderData['FLSTechID']));
        }

        $this->excludeFields();
        $this->formatData();
        
        if (!empty($this->errors)) {
        	return false;
        }
        return true;
    }
    
    private function processProjectData($projectId)
    {
        $fieldsList = "Default_Amount,ProjectName";
        $criteria = "ProjectNo = $projectId";
		if (!is_numeric($projectId)) {
	        $fieldsList = "Default_Amount,ProjectNo";
	        $criteria = "ProjectName = '$projectId'";
		}
        $projectData = Core_Caspio::caspioSelectAdv(TABLE_FLS_PROJECTS, $fieldsList, $criteria, '');

        if (!empty($projectData[0])) {
            $woToProjectFieldMap = array("Default_Amount" => "PayAmt", "ProjectNo" => "Project_ID");
			
        	foreach ($projectData[0] as $key=>$val) {
        	    $buf = $val=="NULL" ? '' : $val;
        	    if (key_exists($key, $woToProjectFieldMap)) {
        	    	if (empty($this->workOrderData[$woToProjectFieldMap[$key]]) && !empty($buf)) {
        		        $this->workOrderData[$woToProjectFieldMap[$key]] = $buf;
        		    }
        	    } else {
        	        if (empty($this->workOrderData[$key]) && !empty($buf)) {
        		        $this->workOrderData[$key] = $buf;
        		    }
        	    }
        	}
        	return true;
        } else {
            $this->addError('There is no such Project');
        }
    }
	
    private function processTechData($techId)
    {
        $fieldsList = 'FirstName,LastName,PrimaryEmail,PrimaryPhone,No_Shows,Back_Outs,ShipTo_Address1,TechID';
        $techData = Core_Caspio::caspioSelectAdv(TABLE_MASTER_LIST, $fieldsList, 'FLSID = '.$techId, '');

        if (!empty($techData[0])) {
        	$this->workOrderData['TechFName']        = $techData[0]['FirstName'];
        	$this->workOrderData['TechLName']        = $techData[0]['LastName'];
        	$this->workOrderData['TechEmail']         = $techData[0]['PrimaryEmail'];
        	$this->workOrderData['TechPhone']         = $techData[0]['PrimaryPhone'];
        	$this->workOrderData['TB_ID']         = $techData[0]['TechID'];
        	$this->workOrderData['TechName']        = $techData[0]['FirstName'] . " " . $techData[0]['LastName'];
			
        	return true;
        } else {
            $this->addError('There is no such Tech');
        }
    }
        
    private function formatData()
    {
		// FORMAT ZIP IF ONLY 4 CHAR (FLS)
		if (key_exists("Zip", $this->workOrderData)) {
			$getZipcode = $this->workOrderData["Zip"];
			if(strlen($getZipcode)==6){
				$getZipcode=trim($getZipcode, "'");
				$this->workOrderData["Zip"]="0".$getZipcode;
			}
		}
		//PayAmt (FLS)
		if (key_exists("PayAmt", $this->workOrderData)) {
			$PayAmt = trim($this->workOrderData["PayAmt"],"'");
			$formatted_PayAmt = number_format($PayAmt, 2);
			$this->workOrderData["PayAmt"] = $PayAmt;
		}
		
		return true;
    }
    
    private function excludeFields()
    {
        $notSettable = array('WorkOrderID');
        foreach ($this->workOrderData as $k=>$v) {
		
            if (in_array($k, $notSettable)) {
            	unset($this->workOrderData[$k]);
            }
        }
    }
}